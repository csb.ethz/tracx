.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Demo scripts
============

.. note::
	All examples presented here are available from the ``tracx_demo_data`` repository. You will find the matlab scripts as  well as the actual demo data.

.. code-block:: bash
	
    $ git clone https://gitlab.com/csb.ethz/tracx_demo_data.git
    $ git clone --recurse-submodules https://gitlab.com/csb.ethz/tracx.git

Minimal working example
-----------------------

.. literalinclude:: examples/run_tracker_example_minimal.m

Starting from generic segmentation (asymetrical)
-------------------------------------------------------
Example for symmetrical cell division with *S.cerevisae* data.

|TRACX| can be used with results from any segmentation algorithm given labeled single page segmentation masks in tif file format. If your masks come in a multipage format please use the `GUI.TracXFileConverter` first.

.. literalinclude:: examples/tracx_yeast_scerevisiae_demo_track_generic_segmentation.m

Starting from CellX segmentation  (asymmetrical)
-----------------------------------------------------
Example for symmetrical cell division with *S.cerevisae* data.

.. literalinclude:: examples/tracx_yeast_scerevisiae_demo_track_cellx_segmentation.m

Starting from CellX segmentation  (symmetrical)
-----------------------------------------------------
Example for symmetrical cell division with *S.pombe* data.

.. literalinclude:: examples/tracx_yeast_spombe_demo_track_cellx_segmentation.m

Load from |TRACX|  project
------------------------------------------

.. literalinclude:: examples/tracx_demo_load_from_tracx_project.m

