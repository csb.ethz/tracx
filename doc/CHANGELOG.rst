.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Changelog
----------------

All notable changes to |TRACX| will be documented in this file. Please read and follow the instructions of  :ref:`how_to_contribute`.



[1.0.0] - 2022-07-07
^^^^^^^^^^^^^^^^^

**Added** 

- Cross platform specific project files. Note MATLAB Compiler project files hard code absolute paths therefore the project files are only given as 'what is required to compile' and should be updated for the file location you are using.

**Fixed**

- Missing resources for cross platform deployment.

  

[0.9.0] - 2022-07-01
^^^^^^^^^^^^^^^^^

**Fixed**

- `getNewickFormat` to work for both asymmetrical and symmetrical division.

- `generateLineageTreeObject` memory pre allocation and row index. 

- `setBirthToDivPhase` prevention of re-evaluation & re-assessment of cell cycle phase of assessed mothers.

- Typos & Documentation

  

[0.8.0] - 2022-06-15
^^^^^^^^^^^^^^^^^

**Fixed**

- Control image writing

- `runReQuantification` and `prepareDataFromSegmentationMask`

- Dependencies

  

[0.7.0] - 2022-06-10
^^^^^^^^^^^^^^^^^

**Fixed**

- Default `videoWriter` profile cross platform

- `maxWorkers` for `saveTrackerProjectImages` handling

- Cross platform paths for `tracx_demo_data`

  

[0.6.0] - 2022-06-03
^^^^^^^^^^^^^^^^^

**Removed** 

- Deprecated code



[0.5.0] - 2022-04-13
^^^^^^^^^^^^^^^^^

**Added** 

- Add version information to GUI.

**Fixed**
- Missing icon resource
- Error handling for Constrain input dialog.



[0.4.0] - 2022-02-24
^^^^^^^^^^^^^^^^^

**Added** 

- Add input validation for `prepareDataFromSegmentationMask`.

- Add user friendly error messages for `prepareDataFromSegmentationMask`.

- Add introduction to GUI page of the documentation.

- Add tracker parameter to separate documentation page.

  

**Fixed**

- Fix `fluoTags` error in GUI.

- Fix typos in Documentation.

  

[0.3.0] - 2021-12-13
^^^^^^^^^^^^^^^^^

**Fixed**

- Corner case of empty neighborhood.
- Max number of default workers in demo scripts and control image generation based on actual availability.
- Set the `neighbourhoodSearchRadius` based on the `trackerEndFrame` for a better estimate.



[0.2.0] - 2021-10-19
^^^^^^^^^^^^^^^^^

**Added** 

- GUI; channel name display for constrain data by fluorescence.
- Documentation now describes how to set bud neck marker in GUI (how to choose the correct channel).

**Fixed**

- GUI; new default for mask `*.mat` instead of `*.tif` (`CellX` input)
- GUI; max workers set by default to 12. If fewer are available it checks what the system allows and uses that.
- GUI; export tracker result table button.



[0.1.0] - 2021-10-7
^^^^^^^^^^^^^^^^

**Added** 

- Added Changelog to project.

**Fixed**

- Documentation

