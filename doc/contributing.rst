.. |TRACX| replace:: **Trac**\ :superscript:`x`\

.. _how_to_contribute:

How to contribute
----------------------------

Thanks for taking the time to contribute to the open source  |TRACX| toolbox! 

.. image:: TrackerLogo.png
   :width: 300
   :align: center

Please follow the Documentation, Style and Changelog Guide to make sure that all your great work will work flawlessly and also be properly documented.

.. include:: doc_guide.rst

.. include:: style_guide.rst

.. include:: changelog_guide.rst