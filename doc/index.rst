﻿.. Trac :superscript:`X` documentation master file, created by
   sphinx-quickstart on Thu Jun  4 15:00:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree:: 
   :hidden:
   :maxdepth: 3
   :caption: Home:

.. toctree:: 
   :hidden:
   :caption: Examples:
	
   demo
   start
   gui
   parameters
   output

.. toctree::
   :hidden:
   :maxdepth: 3
   :caption: API reference:

   api

   CHANGELOG
   contributing
   contributors
   contact
   cite
   license_link

.. image:: TrackerLogo.png
   :width: 300
   :align: center

.. _CellX: https://gitlab.com/csb.ethz/CellX

.. _Cellpose: https://github.com/MouseLand/cellpose

.. |TRACX| replace:: **Trac**\ :superscript:`x`\

|TRACX|- a toolbox for tracking non-motile 2D cell cultures
===========================================================

.. image:: https://img.shields.io/badge/build-passing-brightgreen
   :target: https://img.shields.io/badge/build-passing-brightgreen
   :alt: Travis CI build status

.. image::  https://img.shields.io/badge/version-v1.0.0-blue
   :target: https://img.shields.io/badge/version-v1.0.0-blue

.. image::  https://img.shields.io/badge/platform-windows%20%7C%20macos%20%7C%20linux-lightgrey
   :target: https://img.shields.io/badge/platform-windows%20%7C%20macos%20%7C%20linux-lightgrey

.. image:: https://img.shields.io/badge/License-BSD--3-brightgreen
        :target: https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
        :alt: License

.. image:: https://readthedocs.org/projects/tracx/badge/?version=latest
        :target: https://tracx.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

|TRACX| is a MATLAB generic toolbox for cell tracking from various microscopy image modalities such as Bright-field (BF), phase contrast (PhC) or fluorescence (FL) with an automated track quality assessment in 
absence of a ground truth. It allows to track cells of any shape independent of the cell type. Its modular design allows to use a labeled segmentation mask from any segmentation algorithm along with the raw images! It
offers a variety of control and plotting options as well as the generation of animated movies. Additionally there are methods to reconstruct the genealogy for asymmetrical and symmetrically dividing cells.

|TRACX| allows users to define their own project specifications in xml format, making the toolbox suitable for automating the batch processing of large experiments with thousands of cells and images. 


Why using |TRACX|?
==================

* The only tool giving you an estimate how good tracking was without the need of a manually curated ground truth.
* Can be used independently from a segmentation software. Segmentation algorithms are often tuned for a specific image modality and cell type, therefore keeping segmentation and tracking seperate allows for more flexibility.
* Frame by frame tracking allows for live interaction with running experiments (feed back microscopy)

.. raw:: html

	<video width="640" height="480" controls>
	  <source src="TS_SPombe_4_R2.mp4" type="video/mp4">
	Your browser does not support the video tag.
	</video>

Installation
============

If you run |TRACX| from source you need to have a fully licensed version of MATLAB (for GUI >R2020a) including the following toolboxes:

- Image Processing Toolbox
- Signal Processing Toolbox
- Statistics and Machine Learning Toolbox
- Parallel Computing Toolbox
- Fuzzy Logic Toolbox (optional)
- Mapping Toolbox (optional)

**Installation with git:**

Clone this repo including the submodules and add it to your MATLAB path. 

.. code-block:: bash

    $ git clone --recurse-submodules https://gitlab.com/csb.ethz/tracx.git

Then navigate to the folder you cloned the repo and then past the following code to your MATLAB terminal. This will add |TRACX| to the MATLAB path and makes its functionalities available to you.

.. code-block:: ruby

	addpath(genpath('tracx'));

**Manual download of the repository:**

Download this repo, unzip it and add it to your MATLAB path.  Replace `C:\PATH\TO\` with the actual path on your filesystem where you downloaded |TRACX|  to and run the following code in your MATLAB terminal. This will add |TRACX| to the MATLAB path and makes its functionalities available to you.

.. code-block:: ruby

	addpath(genpath('C:\PATH\TO\tracxdevel-master\tracxdevel-master'));

.. note::
	An error might occur depending on your platform and MATLAB version used in combination with your internet proxy / certificates. "Error using urlreadwrite (line 98) Error downloading URL. Your network connection may be down or your proxy settings improperly configured." A potential fix is described here: https://ch.mathworks.com/matlabcentral/answers/92506-how-can-i-configure-matlab-to-allow-access-to-self-signed-https-servers



Usage
=====

Start by first generating a new |TRACX| instance. See :ref:`extended usage` for more details.

.. code-block:: ruby

	Tracker = TracX.Tracker(); 

.. note::
	You can assign it to any other variable name of your liking instead of calling it `Tracker`. We will call it `Tracker` for simplicity throughout the documentation and demo scripts. 
	
Optionally you can also start the GUI (graphical user interface). See :ref:`gui` for details.

.. code-block:: ruby

	TracX.GUI.TracXGUI(); 

.. note::
	The |TRACX| GUI requires MATLAB R2020a or newer. 



Running |TRACX| for the first time will automatically check and install missing dependencies.

.. code-block:: ruby

    INITIALIZE: Welcome to TracX!
    CHECK: Image Processing Toolbox, Signal Processing Toolbox, Parallel Computing Toolbox
    DONE: Checking required Matlab Toolbox
    CHECK: Full installation of external dependencies.
    INFO: Download started
    INFO: Download successfull https://gitlab.com/csb.ethz/cellxmaskinterface/-/archive/CellXMaskInterfaceTracX/cellxmaskinterface-CellXMaskInterfaceTracX.zip
    INFO: Download started
    INFO: Download successfull https://gitlab.com/csb.ethz/MatlabProgressBar/-/archive/MatlabProgressBarTracX/MatlabProgressBar-MatlabProgressBarTracX.zip
    INFO: Download started
    INFO: Download successfull https://github.com/siyideng/texttoimage/archive/refs/heads/master.zip
    INFO: Download started
    INFO: Download successfull https://gitlab.com/csb.ethz/matlab-tree/-/archive/tracx-tree/matlab-tree-tracx-tree.zip
    DONE: All dependencies successfully installed.
    INFO: INFO: TracX version 1.0.0 is ready.



Create a new |TRACX| project
----------------------------

Before creating a new |TRACX| project you need:



- A set of **brightfield or phase contrast or fluorescence microscopy images** in the form of TIF files, one file per timepoint, position, channel, etc. These raw images  (ideally 16 bit) will be used for the |TRACX| fingerprints. 

- A corresponding set of **segmentation masks** (segmented cells or nuclei). The segmentation masks need to be generated by third-party software (e.g. CellX_ or Cellpose_ or similar), as |TRACX| *does not include a segmentation functionality*. Labeled masks are preferred over binary masks. These files should also be provided as TIF files and have the same image dimensions as the raw images.
- **Optional:** Any number of **additional fluorescence channels** that will then be quantified by the algorithm, provided in the same file format and image dimensions.

*File names:*

Ideally, the file names take the format ``ChannelName_positionXXYYZZZ_timeTTTT.tif``, e.g. ``Brightfield_position0101000_time0001.tif``. `Position` refers to the x and y component of the well position and to the z-plane; it can be set to ``0101000`` as a default, if this does not apply to your experiment. The GUI contains a name parser interface that can be used to transform your original file names to the |TRACX| format using MATLAB® regular expressions, if they do not yet conform to this format (see extended guide below).

*Specific requirements for 3D-data (3D + time):*

The file format should be in the form of one z-stack per timepoint and channel, as a multi-page TIF file containing the z-planes. If all z-planes are provided as separate files, or all planes and timepoints are provided as one single frame, these can be transformed in the GUI. However, the channels need to be separate.

If the above requirements are fulfilled you can create a new |TRACX| project as follows:

.. code-block:: ruby

	% [String] Name of the project / experiment.
	projectName = 'myProject';
	
	% [String] File name identifier of raw images used 
	% 		   to calculate the fingerprint (i.e. Brightfield).
	fileIdentifierFingerprintImages = 'BF'; 
	
	% [String] Well position identifier if multi well 
	% 		   experiment was performed to use.
	fileIdentifierWellPositionFingerprint = []; 
	
	% [String] File identifier of fluorescent images to be used
	% 		   for asymmetrical lineage reconstruction.
	fileIdentifierCellLineage = 'mKate'; 
	
	% [Array, x, y, w, h] Image crop coordinates, in case only part
	% 		   of an image was segmented (CellX segmentation only).
	imageCropCoordinateArray = [480 445 383 323]; 
	
	% [Int] Max number of frames to track.
	movieLength = 30; 
	
	% [String] Image directory to be used for the current project.
	imageDir = 'C:\\myProject\\rawImages'
	
	% [String] Directory containing the segmentation results for the current project.
	segmentationResultDir = 'C:\\myProject\\CellXResults'
	
	% [String] Cell division type. Either asymmetrical or symmetrical.
	cellDivisionType = 'asym';

.. code-block:: ruby

	Tracker.createNewTrackingProject(...
		projectName, imageDir, segmentationResultDir, ...
		fileIdentifierFingerprintImages, ...
		fileIdentifierWellPositionFingerprint, ...
		fileIdentifierCellLineage, ...
		imageCropCoordinateArray, movieLength, cellDivisionType);


Start tracking
--------------

.. code-block:: ruby

	Tracker.runTracker()


Post tracking
-------------

.. code-block:: ruby

	% Save tracking result control images
	Tracker.saveTrackerProjectControlImages()
	
	% Save current state of the tracker instance
	Tracker.saveCurrentTrackerState() 
	
	% Export the project configuration
	Tracker.saveTrackingProject()
	
	% Save tracking results as csv table
	Tracker.saveTrackerResultsAsTable()

Further help / troubleshooting
------------------------------

For further help, such as how to convert your data into the proper format or how to define the |TRACX| parameters, please read the respective sections in :ref:`extended usage` or :ref:`gui`. 

The API reference of the |TRACX| classes and methods can also provide further help in respect to which input and which output is expected as well as a short description about a function's purpose.

License
=======

|TRACX| is licensed under BSD-3 clause. Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group. For details :ref:`license`.

Contributing
============

The authors as well as the 3rd parties libraries we appreciate and use of |TRACX| are listed under :ref:`contributors` . 

If you like to contribute yourselves please read the section :ref:`how_to_contribute`.
