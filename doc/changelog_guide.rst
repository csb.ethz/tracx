.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Changelog guide
-------------------

The Changelog of |TRACX| contains a curated, chronologically ordered list of notable changes for each version of |TRACX| to make it easier for users and contributors to see what changes have been made. This is especially important in terms of reproducibility of Biomedical analysis.

We follow the guidelines described `here <https://keepachangelog.com/en/1.0.0/>`_  in detail:


**Keep in mind that:**

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable (i.e. used in git tags).
- The latest version comes first.
- The release date of each version is displayed. We use the isodata format `ISO 8601 <https://www.iso.org/iso-8601-date-and-time-format.html>`_.  ``YYYY-MM-DD``

**Label changes as follows:**

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

**We stick to Semantic versioning**

The version number is therefore structured as MAJOR.MINOR.PATCH, with::

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

More details `here <https://semver.org/spec/v2.0.0.html>`_