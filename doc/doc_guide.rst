.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Documentation guide
-------------------

This guideline is modified for |TRACX| taken from `here <https://github.com/opencobra/cobratoolbox/blob/master/docs/source/guides/documentationGuide.rst>`_.

In order to enable the automatic documentation generation, the header of
a ``MATLAB`` function has to be formatted properly. The automatic
documentation extracts the commented header of the function (commented
lines between the function’s signature and the first line of code) based
on keywords and blocks.

**Rule 1**: There should be 1 free space between the percentage sign ``%``
and the text. For instance, a correctly formatted line in the header
reads:

.. code-block:: matlab

    % this is correct

A line in the header that is formatted as follows is **ignored**:

.. code-block:: matlab

    %this is not correct (note the space)

**Rule 2**: After the last line of the header, leave one empty line
before the body of the function.

.. code-block:: matlab

    % This is the end of the header of the function
    
    x = 5;  % the body of the function begins after one empty line

Importantly, **do not** put a comment above the first line of code, but
include the comment inline:

.. code-block:: matlab

    % This is the end of the header of the function
    
    % the body of the function begins after one empty line
    x = 5;

**Rule 3**: A line in the header that includes ``..`` after the
percentage sign will be **ignored** in the documentation:

.. code-block:: matlab

    % .. this line will be ignored

Function signature
~~~~~~~~~~~~~~~~~~

The function signature must be correctly formatted. Leave a space after
every comma and before and after the equal sign ``=``. A correctly
formatted function signature reads:

.. code-block:: matlab

    function [output1, output2, output3] = someFunction(input1, input2, input3) % good practice

A function signature that is not formatted properly throws an **error**
during the automatic documentation generation:

.. code-block:: matlab

    function [ output1,output2,output3 ]=someFunction( input1,input2,input3 ) % bad practice

Function description
~~~~~~~~~~~~~~~~~~~~

The description of the function is a brief explanation of the purpose of
the function. Start with a capital letter function name. The description of the function may extend over several
lines. However, try to keep the explanations as brief as possible.

.. code-block:: matlab

    function [output1, output2, output3] = someFunction(input1, input2, input3)
    % SOMEFUNCTION. This is a description of the function that helps to understand how the function works
    % Here the description continues, then we leave an empty comment line
    %

Keywords
~~~~~~~~

The automatic documentation software relies on keywords to properly
format the documented function. A keyword also defines the start of a
block with the header of a function. Main keywords include:

- ``Args:``: block with input argument(s)
- ``Returns:`` block with output argument(s).
- ``Example:``: block with example code (formatted ``MATLAB`` syntax)
- ``.. note::``: highlighted box with text
- ``:Authors:``: list with author(s)

Each of them must be followed by non-empty lines and should be separated
from the next block by an empty line.

All keywords are optional. For instance, if a function does not have any
input arguments, the keyword ``Args:`` can be omitted.

Any line of the block must be indented by 4 spaces after the comment
sign ``%``:

.. code-block:: matlab

    % Args:
    %    input1 (:obj:`object`):     Description of input1
    %    input2 (:obj:`int`):        Description of input2
    % input3:    Description <-- this is bad practice
    %
    % Returns
    % -------
    %    output1: :obj:`bool` <-- no brackets needed  
    %								Description of output1 <-- start on next line

If the indentation differs, there will be an error.

.. rubric:: Keyword ``Example:``

In the block starting with the keyword ``Example:``, the function’s
signature must be given in order to show how the function should be
used. It is important to leave one empty line before the keyword
``Example:``, after the keyword, and after the function’s signature.

.. code-block:: matlab

    % the end of the description
    %
    % Example:
    %
    %    [output1, output2, output3] = someFunction(input1, input2, input3)
    %
    % here the other section can begin

.. rubric:: Keyword ``Args:`` and ``Returns``

The arguments declared in the blocks: ``Args:``, ``Example:``
``Authors:`` must be followed by a colon ``:`` before
the argument description is provided. Except ``Returns`` this needs to be followed by a line with dashes.

The indentation between the argument (with colon) and the description
should be at least 4 spaces, so that all argument descriptions are
aligned.

.. code-block:: matlab

    % Args:
    %    input1 (:obj:`int`):     Description of input1 <-- good practice
    %    input2 (:obj:`int`)     No colon <-- bad practice
    %    input3 (:obj:`int`): Not enough distance (4+ spaces) <-- bad practice
     %   input4: No info on datatype <-- bad practice
    %
    % Returns
    % -------
    %    longerNameOutput:    
    %						  Description of longerNameOutput after 4 spaces
    %    output1:             
    %	                      Description begins at the same place as the longest argument <-- good practice
    %    output2:    
    %                  Description begins too soon <-- bad practice

For a ``structure`` argument, it is possible to list its fields. An
empty line is added after the ``structure`` argument. Then, in the next
line, the field is written aligned with the description of the
``structure`` **plus 2 extra spaces**.

The field is listed beginning as ``* .field - description`` (note the
space between ``*`` and ``.``). It is not necessary to leave an empty
line after listing fields and writing the next argument. The following
illustrates how to list a structure with its fields:

.. code-block:: matlab

    % Returns
    % -------
    %    output:    output argument with fields:
    %
    %                 * .field1 - first field of the structure.
    %               * .field2 - no indent <-- bad practice
    %                 * .field3 - multi-line comment must begin always
    %                   where the text of the first line begins <-- good practice
    %                 * .field4 - multi-line comment where
    %                 the text in line 2 begins too soon <-- bad practice
    %    next:      next argument can be added without empty line

It is also possible to replace ``*`` with a numbered list. You can use
numbers followed by a dot (e.g., ``1.``) instead of ``* .``.

.. code-block:: matlab

    % Args:
    %     varargin (:obj:`str varchar`):
    %
    %        * **opt input 1** (:obj:`float`): Description of optional argument1.
    %        * **opt input 2** (:obj:`float`): Description of optional argument 2. Note do not linebreak!

.. rubric:: Keyword ``Example:``

A common usage example can be included in the ``Example:`` block. Code
included in this block will be formatted as ``MATLAB`` formatted code.
Leave one empty line before the keyword ``Example:``, after the keyword,
and after the properly indented (4 spaces) code snippet.

.. code-block:: matlab

    % the previous block ends here
    %
    % Example:
    %
    %    result = someFunction(input1, input2)
    %    %additional comment if necessary
    %
    % another block begins here

.. rubric:: Keyword ``..note::``

Important information, such as common errors, can be included in the
block that starts with the keyword ``..note::``. A ``..note::`` block is
formatted in a dedicated and highlighted box in the documentation.

Leave one empty line before the keyword ``..note::``, after the keyword,
and after the properly indented text (indent of 4 spaces). The keyword needs to start with two dots followed by two colons after the name.

Normally formatted text can be left at the with one space after the
comment sign. An example of a ``..note::`` block reads:

.. code-block:: matlab

    %
    % ..note::
    %
    %    This is a note that contains a important information.
    %    It will be clearly visible in the documentation online.
    %
    % This is an additional final comment that can be added and that is
    % only relevant to the code itself

.. rubric:: Keyword ``:Authors:``.

In the ``:Authors:`` block, the author(s) that have written or
contributed to the function are listed. Authors are shown in the
documentation. For multiple authers use a list with a dash.

.. code-block:: matlab

    %
    % :Authors: - Name, date, additional information if needed
    %     Author Name - Short description
    

    x = 5;  % here the body of the function begins

If there are 2 or more authors, format as follows:

.. code-block:: matlab

    %
    % :Authors:
    %   
    %     - Author Name - Short description
    %     - Author2 Name - Short description

    x = 5;  % here the body of the function begins

Example
~~~~~~~

A complete example of a function is provided here. Please remember that
colons, indentations, and keywords are important to guarantee pretty
formatting.

.. code-block:: matlab

    function [output1, output2] = someFunction(input1, input2, input3, varargin)
    % SOMEFUNCTION. This is a description of the function that helps understand how the function works
    % Here the description continues, then we leave an empty comment line
    %
    % Example:
    %
    %    [output1, output2] = someFunction(input1, input2, input3, input4)
    %
    % Args:
    %    input1 (:obj:`int`):     Description of input1
    %    input2 (:obj:`str`):     Description of input2
    %    varargin (:obj:`str varchar`):
    %
    %        * **opt input 1** (:obj:`float`): Description of optional argument1.
    %        * **opt input 1** (:obj:`float`): Description of optional argument1.
    %
    % Returns
    % -------
    %    output1 :obj:`unit8` NxM                               
    %                                          Description of output
    %    output2 :obj:`unit8` NxM                               
    %                                          Description of output
    %
    % Example:
    %
    %    % this could be an example that can be copied from the documentation to MATLAB
    %    [output1, output2] = someFunction(11, '22', structure, [1;2])
    %    % without optional values
    %    output1 = someFunction(11, '22')
    %
    % ..note::
    %
    %    This is a very important information to be highlighted
    %
    % This is a final comment that cannot be in the description but can be useful
    %
    % :Authors: 
    %    Name - some information

    x = 5;  % here the body of the function begins