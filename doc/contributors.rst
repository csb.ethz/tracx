.. |TRACX| replace:: **Trac**\ :superscript:`x`\

.. _contributors:

Contributors
============

.. image:: https://avatars.githubusercontent.com/u/16665588?v=4
   :target: https://github.com/cunyap
   :width: 100

.. raw:: html

    <a href="https://avatars.githubusercontent.com/u/16665588?v=4"><img 		class="img-circle"></a>
    <p><a href="https://github.com/cunyap">Andreas P. Cuny</a></p>


**All authors of the**  |TRACX|  **Toolbox:** 

- Andreas P. Cuny
- Tomas Kuendig
- Aaron Ponti
- Joerg Stelling

3rd parties libraries
-------------------------------


The authors appreciate and use the following 3rd parties libraries:

- `CellXMaskInterface <https://gitlab.com/csb.ethz/cellxmaskinterface>`_  written by Andreas P. Cuny fork of `CellX <https://gitlab.com/csb.ethz/CellX>`_, under ``BSD 3-Clause License``.
- `matlabProgressBar <https://github.com/JAAdrian/MatlabProgressBar>`_, written by  Jens-Alrik Adrian;  under ``BSD 3-Clause License``.
- `tree <https://github.com/tinevez/matlab-tree>`_, written by Jean-Yves Tinevez; under ``BSD 2-Clause License``.
- `lapjv <https://ch.mathworks.com/matlabcentral/fileexchange/26836-lapjv-jonker-volgenant-algorithm-for-linear-assignment-problem-v3-0?s_tid=srchtitle>`_, written by Yi Cao;  under ``BSD 3-Clause License``
- `ROFdenoise <https://github.com/grocid/rofdenoise>`_,  written by Philippe Magiera & Carl Löndahl, 2008; under ``BSD 3-Clause License``
- `printTimeLeft <https://ch.mathworks.com/matlabcentral/fileexchange/8076-ascii-progress-bar?s_tid=prof_contriblnk>`_,  written by Nicolas Le Roux; ``WTFPL License``
- `natsort <https://ch.mathworks.com/matlabcentral/fileexchange/47434-natural-order-filename-sort?s_tid=srchtitle>`_,  written by Stephen Cobeldick, 2012-2021; ``BSD 3-Clause License``
- `natsortfiles <https://ch.mathworks.com/matlabcentral/fileexchange/47434-natural-order-filename-sort?s_tid=srchtitle>`_,  written by Stephen Cobeldick, 2012-2021; ``BSD 3-Clause License``
- `mirt_dctn <https://ch.mathworks.com/matlabcentral/fileexchange/24050-multidimensional-discrete-cosine-transform-dct?s_tid=srchtitle>`_, written by Andriy Myronenko;  ``BSD 2-Clause License``
- `sigmf <https://ch.mathworks.com/matlabcentral/fileexchange/47171-constrained-fuzzy-model-identification-files-for-fuzzy-modeling-and-identification-toolbox?s_tid=FX_rc2_behav>`_, written by Janos Abonyi;  ``BSD 2-Clause License``
- `PlayBar <https://ch.mathworks.com/matlabcentral/fileexchange/70402-playbar?s_tid=srchtitle>`_, written by Collin Pecora;  ``BSD 3-Clause License``
- `CROIEditor <https://ch.mathworks.com/matlabcentral/fileexchange/31388-multi-roi-mask-editor-class>`_, written by Jonas Reber;  ``BSD 2-Clause License``


Matlab dependencies
---------------------------------

- Image Processing (for i/o)
- Signal Processing Toolbox (for i.e resample)
- Statistics and Machine Learning Toolbox (for i.e pdist)
- Mapping Toolbox (for i.e changem)
- Parallel Computing toolbox (for i.e parfor)
