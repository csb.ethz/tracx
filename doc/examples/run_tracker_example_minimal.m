%% Make sure paths are set correctly
run('setup_environment.m')

%% Configure a new tracking project
projectName = 'TracX_Yeast_SCerevisiae_Experiment'; % Project name
fileIdentifierFingerprintImages = 'BFdivide'; % Image identifier for Brigthfield images;
fileIdentifierWellPositionFingerprint = []; % Well position identifier if multiwell experiment.
fileIdentifierCellLineage = 'mKate'; % Image identifier for the Cell Lineage reconstruction (i.e bud neck marker).
imageCropCoordinateArray = [73 71 179 174]; % Empty if no crop has been applied in CellX, add CellX cropRegionBoundaries coordinates otherwise (from CellX_SCerevisiae_Parameter.xml).
movieLength = 30; % Number of timepoints to track 
cellsFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'CellXSegmentation', 'mKO_mKate'); % Path to segmentation results (CellX Style).
imagesFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'RawImages'); % Path to raw images.
cellDivisionType = 'asym'; % Cell division type.

% Create a tracker instance and a new project
Tracker = TracX.Tracker();
Tracker.createNewTrackingProject(projectName, imagesFilePath, ...
        cellsFilePath, fileIdentifierFingerprintImages, ...
        fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...
        imageCropCoordinateArray, movieLength, cellDivisionType);
Tracker.revertSegmentationImageCrop() % Reverts the potential image crop applied by CellX to map the cell coordinates to the full image.

%% Test default tracking parameters
% Optionally tune tracking parameters for better results
Tracker.configuration.ParameterConfiguration.setMaxTrackFrameSkipping(4);
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeDecrease(0.6);
% Tracker.configuration.ParameterConfiguration.setMeanCellDiameter(35);
% Tracker.configuration.ParameterConfiguration.setMaxCellCenterDisplacement(25)
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)

% Dry run to test the tracking parameters
% Tracker.testTrackingParameters([1,10]) % Track from frame 1 to frame 10 for testing.

%% Run the tracker
Tracker.runTracker()

%% Save the tracking results
Tracker.saveCurrentTrackerState() % Saves the tracker state as mat file (to continue work anytime later)
Tracker.saveTrackingProject() % Saves the tracking project.
Tracker.saveTrackerResultsAsTable() % Saves the tracking results as one column seperated table for further analysis.
Tracker.saveTrackerProjectControlImages('isParallel', false, 'maxWorkers', 12) % Save additional control images to inspect the sucess of the tracking.