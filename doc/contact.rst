.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Contact
=======
.. raw:: html

    <br>

For questions regarding |TRACX| Toolbox, please contact the lead developer:

`Andreas P. Cuny`_,
	
.. raw:: html

	Computational Systems Biology, <br>
	Departement of Biosystems Science and Engineering (D-BSSE), <br>
	ETH Zurich.

.. _Andreas P. Cuny: https://bsse.ethz.ch/department/people/detail-person.MTg0ODUw.TGlzdC8yNjY5LDEwNjI4NTM0MDk=.html