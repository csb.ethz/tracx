.. |TRACX| replace:: **Trac**\ :superscript:`x`\

Cite
====
.. raw:: html

If you are using |TRACX| for your scientific work please cite our paper:

	Cuny, A. P., Ponti, A., Kuendig, T., Rudolf, F., & Stelling J. (2021). Cell region fingerprints enable highly precise single-cell tracking and lineage reconstruction. BioRxiv,. https://doi.org/10.1101/2021.10.26.465883

or this repository using its DOI as follows:

	https://doi.org/10.1101/ (update once available)

.. note::
	This DOI will resolve to the first version of |TRACX|.

.. code-block:: bibtex

	@article{cuny2021,
		author    = {Andreas P. Cuny, Aaron Ponti, Tomas Kuendig, Fabian Rudolf and Joerg Stelling},
		title     = {Cell region fingerprints enable highly precise single-cell tracking and lineage reconstruction},
		journal   = {BioRxiv},
		year      = {2021},
		doi       = {https://doi.org/10.1101/2021.10.26.465883}
		}

