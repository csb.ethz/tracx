%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = mapCrop2RawCoordinates(this, imageCropCoordinateArray )
% MAPCROP2RAWCOORDINATES Maps ProjectConfiguration.imageCropCoordinateArray
% coordinates to original raw image coordinates.
%
% Args:
%    this (:obj:`object`):                         :class:`TrackerData` instance.      
%    imageCropCoordinateArray (:obj:`array`, 1x4): :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`  
%
% Returns
% -------
%    void :obj:`-`                               
%                                             
% :Authors:
%    Andreas P. Cuny - initial implementation

this.setFieldArray('cell_center_x', (this.getFieldArray('cell_center_x') ...
    + imageCropCoordinateArray(1)));
this.setFieldArray('cell_center_y', (this.getFieldArray('cell_center_y') ...
    + imageCropCoordinateArray(2)));

end
