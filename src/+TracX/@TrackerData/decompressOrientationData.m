%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function decompressOrientationData(this)
% DECOMPRESSORIENTATIONDATA Decompresses the orientation vector into from
% a single number.
%
% Args:
%    this (:obj:`object`):                   :class:`TrackerData` instance.      
%
% Returns
% -------
%    void :obj:`-`                               
%                                             
% :Authors:
%    Thomas Kuendig - initial implementation

this.SegmentationData.cell_orientation = this.io.orientationDecompressor(this.SegmentationData.cell_orientation);

end