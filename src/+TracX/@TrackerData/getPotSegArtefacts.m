%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [uuidToDel, trToDelFrame, ciToDel, segArtefactsL] = ...
    getPotSegArtefacts(this, varargin)
% GETPOTSEGARTEFACTS Determines potential segmentation artefacts based on a
% fluorescent channel and returns the uuid, track and cell indices to
% delete.
%
% Args:
%    this (:obj:`object`):               :class:`TrackerData` instance.       
%    varargin (:obj:`str varchar`):
%
%        * **channel** (:obj:`int`):     Channel number :class:`~+TracX.@QuantificationData.QuantificationData.fluo_id`  of the fluorescent channel to use. Defaults to 1.
%        * **stdFactor** (:obj:`float`): Factor standard deviation. Defaults to 2.
%
% Returns
% -------
%    uuidToDel :obj:`array` 
%                                      Array of uuids of segmentation
%                                      objects to delete.
%    trToDelFrame :obj:`array` 
%                                      Array of frames of segmentation
%                                      objects to delete.
%    ciToDel :obj:`array` 
%                                      Array of cell indices of segmentation
%                                      objects to delete.
%    segArtefactsL :obj:`array` 
%                                      Array of positive segmentation artefacts.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
defaultChannel = 1;
defaultStdFactor = 2;
ids = unique(this.QuantificationData.fluo_id);
names = unique(this.QuantificationData.fluo_name);
validChannel = @(x) any(ismember(x, ids)) || any(cell2mat(strfind(names, x)));
validStdFactor = @(x) isnumeric(x) && x > 0;
addRequired(p, 'this', @isobject);
addOptional(p, 'channel', defaultChannel, validChannel);
addOptional(p, 'stdFactor', defaultStdFactor, validStdFactor);
parse(p, this, varargin{:});

uuidSArray = this.getFieldArray('uuid');
frameArray = this.getFieldArray('cell_frame');
cellArray = this.getFieldArray('cell_index');
backgroundMeanArray = this.QuantificationData.getFieldNameForFluoChannel(...
    'fluo_background_mean', p.Results.channel);
backgroundStdArray = this.QuantificationData.getFieldNameForFluoChannel(...
    'fluo_background_std', p.Results.channel);
cellQ75Array = this.QuantificationData.getFieldNameForFluoChannel(...
    'fluo_bright_q75', p.Results.channel);

segArtefactsL = (cellQ75Array - backgroundMeanArray) <= (...
    backgroundStdArray * p.Results.stdFactor);
uuidToDel = uuidSArray(segArtefactsL);
trToDelFrame = frameArray(segArtefactsL);
ciToDel = cellArray(segArtefactsL);

end
