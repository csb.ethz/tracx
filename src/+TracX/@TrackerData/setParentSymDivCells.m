%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function setParentSymDivCells( this, startFrame, endFrame, imageFrameNumberArray, ...
    segmentationResultDir, trackerResultMaskFileArray)
% SETPARENTSYMDIVCELLS Determines the relationship
% between daughter and mother cell of symmetricaly dividing cells.
%
% Args:
%    this (:obj:`object`):                    :class:`TrackerData` instance.      
%    startFrame (:obj:`int`):                 Number of the first frame of
%                                             the project where to start the
%                                             mother daughter assignment.
%    endFrame (:obj:`int`):                   Number of the last frame of the
%                                             project where to end the mother
%                                             daughter assignment.
%    imageFrameNumberArray (:obj:`array`):    Array with all image frame
%                                             numbers for current project.
%                                             This function evaluates
%                                             this array in the range
%                                             startFrame to endFrame.
%    segmentationResultDir (:obj:`str`):      Path to segmentation results.
%    trackerResultMaskFileArray (:obj:`array`): Array with tracker result
%                                             mask filenames.
%
% Returns
% -------
%    void :obj:`-`                               
%                                             Saves track_parent and
%                                             cell_pole_age information into
%                                             :class:`SegmentationData`
%                                             directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get data for needed field names from TrackerData.SegmentationData for the
% first project image frame.
fieldNameArray = {'track_index', 'cell_center_x', 'cell_center_y', 'cell_center_z', ...
    'cell_filtered_dif_x', 'cell_filtered_dif_y', 'cell_filtered_dif_z', ...
    'cell_majoraxis', 'cell_pole1_x' 'cell_pole1_y', 'cell_pole2_x', ...
    'cell_pole2_y', 'cell_pole1_age', 'cell_pole2_age', 'track_parent' };
oldFrameData = this.getTemporaryDataStruct(fieldNameArray, startFrame );

frameArray = imageFrameNumberArray(startFrame:endFrame);

% Total frame number
nFrames = length(frameArray);

f = 1;
while f <= nFrames
    frameNumber = frameArray(f);
    
    maskDaughterFrame = this.io.readSegmentationMask(fullfile( ...
        segmentationResultDir, ...
        trackerResultMaskFileArray{frameNumber}));
    if frameNumber > 1
        maskParentFrame = this.io.readSegmentationMask(fullfile( ...
            segmentationResultDir, ...
            trackerResultMaskFileArray{frameNumber-1}));
    else
        maskParentFrame = maskDaughterFrame;
    end
    
    newFrameData = this.getTemporaryDataStruct(fieldNameArray, frameNumber );
    
    % Find track differences between frames
    % @todo If mis segmented for several frames (holes, missing) we potentialy
    % wrongly assign this parent too early
    parentCandidateArrayTemp = setdiff(oldFrameData.track_index, ...
        newFrameData.track_index);
    parentCandidateArrayTempIdx = ismember(oldFrameData.track_index, ...
        parentCandidateArrayTemp);
    parentCandidateArray = oldFrameData.track_index(parentCandidateArrayTempIdx);
    if ~isempty(parentCandidateArray)
        % Get coordinates of cell_poles for all cells
        % NOTE: This can be improved by looking at cells only in the
        % neighbourhood of the ParentCandidates
        daughterPoleCoordX = [newFrameData.cell_pole1_x - ...
            newFrameData.cell_filtered_dif_x; newFrameData.cell_pole2_x - ...
            newFrameData.cell_filtered_dif_x];
        daughterPoleCoordY = [newFrameData.cell_pole1_y - ...
            newFrameData.cell_filtered_dif_y; newFrameData.cell_pole2_y - ...
            newFrameData.cell_filtered_dif_y];
        
        for i = 1:numel(parentCandidateArray)
            iParentCandidate = parentCandidateArray(i);
            parentCellCenterCoordX = oldFrameData.cell_center_x(...
                oldFrameData.track_index == iParentCandidate);
            parentCellCenterCoordY = oldFrameData.cell_center_y(...
                oldFrameData.track_index == iParentCandidate);
            
            candPairID = this.getMaskBasedDPairs(maskParentFrame, ...
                maskDaughterFrame, iParentCandidate);
            
            xyDist = pdist2([parentCellCenterCoordX, parentCellCenterCoordY], ...
                [daughterPoleCoordX, daughterPoleCoordY]);
            cellLUT = [1:length(newFrameData.cell_pole1_x), ...
                1:length(newFrameData.cell_pole1_x)];
            
            % Sort to find the two closest candidates:
            [xyDistS, xyDistIdx] = sort(xyDist);
            % Two closest to previous end:
            
            dID = [newFrameData.track_index; newFrameData.track_index]';
            %newFrameData.track_index(cellLUT(xyDistIdx(1)))
            if candPairID ~= -1
                candPoleDist = xyDist(ismember(dID, candPairID));
                candPoleIdx = find(ismember(dID, candPairID));
                [closestPoleDist, closestPoleDistIdx] = mink(candPoleDist, 2);
                xyDistIdx = candPoleIdx(closestPoleDistIdx);
                
                % Two closest to previous end:
                if ~isempty(xyDistIdx)
                    daughterCandidateXYDist = xyDist(xyDistIdx(1:2));
                
                % Calc distance of daughter candidates to parent. Should be close
                % to the parent cell center. Use majoraxis as distance measure to
                % determine max allowed pole distances
                for n = 1:numel(daughterCandidateXYDist)
                    % Check if closest coordinates found are within reasonable range
                    if ~(daughterCandidateXYDist(n) > oldFrameData.cell_majoraxis(...
                            oldFrameData.track_index == iParentCandidate)/2)
                        
                        % Determine the new born cell pole
                        % Set track_parent for closest candidates, which is now a
                        % valid daughter cell
                        newFrameData.track_parent(cellLUT(xyDistIdx(n))) = ...
                            oldFrameData.track_index(...
                            oldFrameData.track_index == iParentCandidate);
                        
                        if xyDistIdx(n) > length(newFrameData.cell_pole1_x)
                            % Set new pole age to 1, NA for initial 'old' pole.
                            newFrameData.cell_pole2_age(cellLUT(xyDistIdx(n))) = 1; % [ 1, x]
                        else
                            % New Pole age
                            newFrameData.cell_pole1_age(cellLUT(xyDistIdx(n))) = 1; % [ x, 1]
                        end
                        
                        % Get the pole age for the older pole from the oldframe
                        % (There determine first which one is closer and get the
                        % age of this one)
                        parentPoleCoordArray = [...
                            oldFrameData.cell_pole1_x(...
                            oldFrameData.track_index == iParentCandidate), ...
                            oldFrameData.cell_pole1_y(...
                            oldFrameData.track_index == iParentCandidate); ...
                            oldFrameData.cell_pole2_x(...
                            oldFrameData.track_index == iParentCandidate), ...
                            oldFrameData.cell_pole2_y(...
                            oldFrameData.track_index == iParentCandidate)];
                        
                        if xyDistIdx(n) > length(newFrameData.cell_pole1_x)
                            parentPoleDist = pdist2([daughterPoleCoordX(...
                                cellLUT(xyDistIdx(n))),...
                                daughterPoleCoordY(cellLUT(xyDistIdx(n)))], ...
                                parentPoleCoordArray);
                        else
                            parentPoleDist = pdist2([daughterPoleCoordX(...
                                cellLUT(xyDistIdx(n)) + length(...
                                newFrameData.cell_pole1_x)),...
                                daughterPoleCoordY(cellLUT(xyDistIdx(n)) + ...
                                length(newFrameData.cell_pole1_x))], ...
                                parentPoleCoordArray);
                        end
                        parentPoleIdx = parentPoleDist == min(parentPoleDist);
                        
                        if parentPoleIdx(1) == 1
                            if xyDistIdx(n) > length(newFrameData.cell_pole1_x)
                                newFrameData.cell_pole1_age(cellLUT(xyDistIdx(n))) = ...
                                    oldFrameData.cell_pole1_age(...
                                    oldFrameData.track_index == iParentCandidate) + 1;
                            else
                                newFrameData.cell_pole2_age(cellLUT(xyDistIdx(n))) = ...
                                    oldFrameData.cell_pole2_age(...
                                    oldFrameData.track_index == iParentCandidate) + 1;
                            end
                        else
                            if xyDistIdx(n) > length(newFrameData.cell_pole1_x)
                                newFrameData.cell_pole1_age(cellLUT(xyDistIdx(n))) = ...
                                    oldFrameData.cell_pole2_age(...
                                    oldFrameData.track_index == iParentCandidate) + 1;
                            else
                                newFrameData.cell_pole2_age(cellLUT(xyDistIdx(n))) = ...
                                    oldFrameData.cell_pole2_age(...
                                    oldFrameData.track_index == iParentCandidate) + 1;
                            end
                        end
                        newFrameData.cell_pole2_age;
                    end
                end
                % else % Not a pair
                %    xyDistIdx
                end
            end
        end
    end
    
    % better subset data such that, all index ~= 0 get selected for the
    % track_index and not the cell parents,
    daughterCellArray = newFrameData.track_index(newFrameData.track_parent ~= 0);
    for k = 1:numel(daughterCellArray)
        iDaughterCellTrackIndex = daughterCellArray(k);
        
        parentTrackIdx = newFrameData.track_parent(newFrameData.track_index == iDaughterCellTrackIndex);
        poleAge1 = newFrameData.cell_pole1_age(newFrameData.track_index == iDaughterCellTrackIndex);
        poleAge2 = newFrameData.cell_pole2_age(newFrameData.track_index == iDaughterCellTrackIndex);
        
        % Instead of looping over all frames find only frames belonging to the
        % daughter cells
        daughterCellTrackIndexFrameArray = this.getFieldArrayForTrackIndex('cell_frame', iDaughterCellTrackIndex);
        for l = 1:numel(daughterCellTrackIndexFrameArray)
            iDaughterCellTrackIndexFrame = daughterCellTrackIndexFrameArray(l);
            % Allow this assignment only one time (yet not sure if that is
            % the best way. Problem if a track stopps due to
            % misssegmentation but re appears later again and a cell pole
            % is in its neighbourhood this pole and cell gets assigned
            % wrongly. Tracker with tracker verification should eliminate
            % that problem)
            if this.getFieldArrayForTrackIndexForFrame('track_parent', ...
                    iDaughterCellTrackIndexFrame, iDaughterCellTrackIndex) == 0
                this.setFieldnameValuesForTracksOnFrame('track_parent', ...
                    iDaughterCellTrackIndexFrame, iDaughterCellTrackIndex, parentTrackIdx)
                this.setFieldnameValuesForTracksOnFrame('cell_pole1_age', ...
                    iDaughterCellTrackIndexFrame, iDaughterCellTrackIndex, poleAge1)
                this.setFieldnameValuesForTracksOnFrame('cell_pole2_age', ...
                    iDaughterCellTrackIndexFrame, iDaughterCellTrackIndex, poleAge2)
            end
        end
    end
    
    % Update oldFrameData
    oldFrameData.track_index = newFrameData.track_index;
    oldFrameData.cell_center_x = newFrameData.cell_center_x;
    oldFrameData.cell_center_y = newFrameData.cell_center_y;
    oldFrameData.cell_filtered_dif_x = newFrameData.cell_filtered_dif_x;
    oldFrameData.cell_filtered_dif_y = newFrameData.cell_filtered_dif_y;
    oldFrameData.cell_majoraxis = newFrameData.cell_majoraxis;
    oldFrameData.cell_pole1_x = newFrameData.cell_pole1_x;
    oldFrameData.cell_pole2_x = newFrameData.cell_pole2_x;
    oldFrameData.cell_pole1_y = newFrameData.cell_pole1_y;
    oldFrameData.cell_pole2_y = newFrameData.cell_pole2_y;
    oldFrameData.cell_pole1_age = newFrameData.cell_pole1_age;
    oldFrameData.cell_pole2_age = newFrameData.cell_pole2_age;
    oldFrameData.track_parent = newFrameData.track_parent;
    
    f = f + 1;
    
end
end

