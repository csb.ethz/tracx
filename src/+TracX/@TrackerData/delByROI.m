%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = delByROI(this, roi, neighbourhoodSearchRadius)
% DELETEBYROI Deletes segmentation objects outside a region of interest
% (ROI). 
% 
% Args:
%    this (:obj:`obj`):                        :class:`TrackerData` instance.
%    roi (:obj:`array`):                       Region of interest defined 
%                                              by polygon vertices to keep
%                                              data for tracking.
%    neighbourhoodSearchRadius (:obj:`int`):   Neighbourhood radius. Defaults to 
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
%
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`TrackerData` object
%                                               directly and deletes selected
%                                               data outside ROI.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
validROI = @(x) isnumeric(x); 
validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
addRequired(p, 'this', @isobject);
addRequired(p, 'roi', validROI);
addRequired(p, 'neighbourhoodSearchRadius', validNeighbourhoodSearchRadius);
parse(p, this, roi, neighbourhoodSearchRadius);


pH = images.roi.Polygon('Position', p.Results.roi);
insideIdx = pH.inROI(this.getFieldArray('cell_center_x'), ...
        this.getFieldArray('cell_center_y'));
uuid = this.getFieldArray('uuid');

this.utils.printToConsole('DELETE: cell indices:')

d = uuid(~insideIdx)';
fac = ceil(size(d, 2)/10);
dd = nan(10*fac, 1);
dd(1:numel(d)) = d;
ddd = reshape(dd', 10, [])';
for k = 1:size(ddd, 1)
    this.utils.printToConsole(sprintf('DELETE: %s', regexprep(num2str(ddd(k, ...
        ~isnan(ddd(k,:)))),'\s+',', ')))
end

% Delete
this.deleteData(uuid(~insideIdx), p.Results.neighbourhoodSearchRadius)

this.utils.printToConsole('DELETE: Done')
end
