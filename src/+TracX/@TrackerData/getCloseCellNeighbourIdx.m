%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  closeNeighboursIdx  = getCloseCellNeighbourIdx(...
    xCoordinateArray, yCoordinateArray, neighbourhoodSearchRadius, varargin)
% GETCLOSECELLNEIGHBOURIDX Returns a square logical matrix of size
% length(x) where as 1 corresponds to a neighbour within the
% :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`.
%
% Args:
%    ignoredArg (:obj:`obj`):                  :class:`TrackerData` instance.
%    xCoordinateArray (:obj:`array`, kx1):     Array with x coordinates
%    yCoordinateArray (:obj:`array`, kx1):     Array with y coordinates
%    neighbourhoodSearchRadius:  [Int]         Radius in pixel within an other
%                                              point defined by the coordinate
%                                              arrays x and y is considered to
%                                              be a neighbour of (x_i,y_i).
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% 
% Returns
% -------
%     closeNeighboursIdx: :obj:`array` kxk  
%                                              Square logical matrix selecting the
%                                              neighbours within 
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if ~isempty(xCoordinateArray)
    if ~isempty(varargin)
        coordinateArray = [xCoordinateArray, yCoordinateArray, varargin{1}];
    else
        coordinateArray = [xCoordinateArray, yCoordinateArray];
    end
    cellDist = pdist(coordinateArray) <= neighbourhoodSearchRadius;
    closeNeighboursIdx = squareform(cellDist);
else
    closeNeighboursIdx = [];
end

end

