%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ sortIdx ] = getSortIdxForTrackIndices( this, varargin )
% GETSORTIDXFORTRACKINDICES Returns an array of indices to sort 
% track_index on each frame in ascending (default) or descending order. 
%
% Args:
%    this (:obj:`object`):             :class:`TrackerData` instance.       
%    varargin (:obj:`str varchar`):
%
%        * **direction** (:obj:`str`): Sorting direction. Defaults to ascending 'a'. Otherwise 'd'.
%
% Returns
% -------
%    sortIdx :obj:`array` 
%                                      Sorting indices for ordering  
%                                      arrays.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultSortDirection = 'a';
validSortDirectionKeys = {'a', 'd'};

p = inputParser;
p.addRequired('this', @isobject);
p.addOptional('direction', defaultSortDirection, @(x) ...
    any(validatestring(x, validSortDirectionKeys)));
parse(p, this, varargin{:})

frames = this.getFieldArray('cell_frame');
framesU = unique(frames);
tracks = this.getFieldArray('track_index');
tracksS = nan(size(tracks));
sortIdx = nan(size(tracks));

for fn = framesU'
    currIdx  = frames == fn;
    tracksS(currIdx) = sort(tracks(currIdx), p.Results.direction);
    [~, currSortIdx] = ismember(tracksS(currIdx), tracks(currIdx));
    lastIdx = find(~isnan(sortIdx), 1, 'last');
    if ~isempty(lastIdx)
        sortIdx(currIdx) = currSortIdx + lastIdx;
    else
        sortIdx(currIdx) = currSortIdx;
    end
   
end

%[~, sortIdx] = ismember(tracksS, tracks);

end