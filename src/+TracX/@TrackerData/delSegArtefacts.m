%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = delSegArtefacts(this, neighbourhoodSearchRadius, varargin)
% DELSEGARTEFACTS Deletes potential segmentation artefacts (segmented
% objects which are no cells based the autofluorescence.
% 
% Args:
%    this (:obj:`obj`):                        :class:`TrackerData` instance.
%    neighbourhoodSearchRadius (:obj:`int`):   Neighbourhood radius. Defaults to 
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
%    varargin (:obj:`str varchar`):
%
%        * **channel** (:obj:`int`):     Channel number :class:`~+TracX.@QuantificationData.QuantificationData.fluo_id`  of the fluorescent channel  to use for segmentation artefact detection. Defaults to 1.
%        * **stdFactor** (:obj:`float`): Factor standard deviation. Defaults to 2.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`TrackerData` object
%                                               directly and deletes selected
%                                               data.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
defaultChannel = 1;
defaultStdFactor = 2;
ids = unique(this.QuantificationData.fluo_id);
names = unique(this.QuantificationData.fluo_name);
validChannel = @(x) any(ismember(x, ids)) || any(cell2mat(strfind(names, x)));
validStdFactor = @(x) isnumeric(x) && x > 0; 
validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
addRequired(p, 'this', @isobject);
addRequired(p, 'neighbourhoodSearchRadius', validNeighbourhoodSearchRadius);
addOptional(p, 'channel', defaultChannel, validChannel);
addOptional(p, 'stdFactor', defaultStdFactor, validStdFactor);
parse(p, this, neighbourhoodSearchRadius, varargin{:});

this.utils.printToConsole('DELETE: Segmentation artefacts')

[dataIDToDelete, ~, ~] = this.getPotSegArtefacts(p.Results.channel, ...
    p.Results.stdFactor);

this.utils.printToConsole('DELETE: cell indices:')

d = dataIDToDelete';
fac = ceil(size(d, 2)/10);
dd = nan(10*fac, 1);
dd(1:numel(d)) = d;
ddd = reshape(dd', 10, [])';
for k = 1:size(ddd, 1)
    this.utils.printToConsole(sprintf('DELETE: %s', regexprep(num2str(ddd(k, ...
        ~isnan(ddd(k,:)))),'\s+',', ')))
end

this.deleteData( dataIDToDelete, p.Results.neighbourhoodSearchRadius)

this.utils.printToConsole('DELETE: Done')
end
