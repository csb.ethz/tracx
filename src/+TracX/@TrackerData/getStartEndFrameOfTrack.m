%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ startFrameArray, endFrameArray ] = getStartEndFrameOfTrack(~, ...
    cellFrameArray, trackIndexArray)
% GETSTARTENDFRAMEOFTRACK Determines for each cell track with a
% track index its first and last occuring image frame.
%
% Args:
%    ignoredArg (:obj:`object`):                   :class:`TrackerData` instance.     
%    cellFrameArray (:obj:`array`, kx1):           Array of image frame
%                                                  numbers for each segmentet cell.
%                                                  :class:`~+TracX.@SegmentationData.SegmentationData.cell_frame` 
%    trackIndexArray (:obj:`array`, kx1):          Array of track indicies.
%                                                  :class:`~+TracX.@SegmentationData.SegmentationData.track_index` 
%    trackFingerprintDistanceArray (:obj:`array`): Array of track fingerprint
%                                                  distances for trackIndexArray
%    trackAssignmentFractionArray (:obj:`array`):  Array of track assignment
%                                                  fractions for trackIndexArray
%    fingerprintDistThreshold (:obj:`float`):      Fingerprint distance
%                                                  threshold defined by
%                                                  :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintDistThreshold` 
%
% Returns
% -------
%    startFrameArray :obj:`array` kx1
%                                                  Array with frame number
%                                                  where track i first occured.
%    endFrameArray :obj:`array` kx1
%                                                  Array with frame number
%                                                  where track i last occured.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

uniqueTrackIndexArray = unique(trackIndexArray);
nTracks = numel(uniqueTrackIndexArray);
startFrameArray = nan(numel(trackIndexArray), 1);
endFrameArray = nan(numel(trackIndexArray), 1);
for iTrack = 1:nTracks
    trackOccurance = cellFrameArray(trackIndexArray == uniqueTrackIndexArray(iTrack));
    startFrameArray(ismember(trackIndexArray, uniqueTrackIndexArray(iTrack)))...
        = min(trackOccurance);
    endFrameArray(ismember(trackIndexArray, uniqueTrackIndexArray(iTrack)))...
        = max(trackOccurance);
end
end