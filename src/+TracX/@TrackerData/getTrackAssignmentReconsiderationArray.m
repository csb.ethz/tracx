%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function trackAssignmentReconsiderationArray  = getTrackAssignmentReconsiderationArray( ...
    ~, trackIndexArray, trackFingerprintDistanceArray, ...
    trackAssignmentFractionArray, fingerprintDistThreshold)
% GETTRACKASSIGNMENTRECONSIDERATIONARRAY Returns an array with track
% indicies where the track assignment has to be re-considered as the
% fingerprint distance is above the threshold given.
%
% Args:
%    ignoredArg (:obj:`object`):                   :class:`TrackerData` instance.       
%    trackIndexArray (:obj:`array`):               Array of track indicies.
%                                                  :class:`~+TracX.@SegmentationData.SegmentationData.track_index` 
%    trackFingerprintDistanceArray (:obj:`array`): Array of track fingerprint
%                                                  distances for trackIndexArray
%    trackAssignmentFractionArray (:obj:`array`):  Array of track assignment
%                                                  fractions for trackIndexArray
%    fingerprintDistThreshold (:obj:`float`):      Fingerprint distance
%                                                  threshold defined by
%                                                  :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintDistThreshold` 
%
% Returns
% -------
%    trackAssignmentReconsiderationArray :obj:`array` kx1
%                                                  Array with track indicies
%                                                  to reconsider the assignment.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get the fingerprint distances between assigned tracks
% (NewFrame vs OldFrame)
if iscell(trackFingerprintDistanceArray)
    minDistArray = cellfun(@(x) min(x), trackFingerprintDistanceArray);
else
    minDistArray = trackFingerprintDistanceArray;
end

useFFP = true;
if useFFP == true
    aboveThresIdx = trackAssignmentFractionArray > fingerprintDistThreshold; %0;
else
    % Get index of the fingerprints above the fingerprintDisThreshold
    aboveThresIdx = (minDistArray > fingerprintDistThreshold);
end

% Get track_indicies of those above the threshold
trackAssignmentReconsiderationArray  = trackIndexArray(aboveThresIdx);

end