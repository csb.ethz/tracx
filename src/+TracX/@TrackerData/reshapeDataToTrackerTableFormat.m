%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function this = reshapeDataToTrackerTableFormat( this, ...
    rawData, currentFrameFieldNameArray)
% RESHAPEDATATOTRACKERTABLEFORMAT Reshapes the imput data
% table i.e. CellX results table into the correct width.
% CellX adds the quantification of multiple fluorophores as
% additional columns to the data table which makes it hard to
% deal with this kind of data of various colum length. Therefore
% we reshape the data into a thight colum format specifing
% potential multiple fluorophores as additional row identified
% by a id (fluo.id) and the name as string in (fluo.name).
%
% Args:
%    this (:obj:`object`):                      :class:`TrackerData` instance.      
%    rawData (:obj:`array`):                    Imported raw data from
%                                               segmentation software
%                                               i.e CellX in single text
%                                               file per image frame
%                                               format.
%    currentFrameFieldNameArray (:obj:`array`): Array with field name of
%                                               cellXImportedRawData
%                                               (i.e. cell_frame,
%                                               cell_area).
%
% Returns
% -------
%    void :obj:`-`                               
%                                             Returns :class:`TrackerData`
%
% :Authors:
%    Andreas P. Cuny - initial implementation

NTempFieldNames = numel(currentFrameFieldNameArray);
prefix = cell(numel(currentFrameFieldNameArray),1);
for i = 1:numel(currentFrameFieldNameArray)
    tmp = strsplit(currentFrameFieldNameArray{i}{1}, '_');
    prefix{i} = tmp{1};
    
    if ~isprop(this.SegmentationData,char(currentFrameFieldNameArray{i}))
        addprop(this.SegmentationData, char(currentFrameFieldNameArray{i}));
        this.SegmentationData.(char(currentFrameFieldNameArray{i})) = [];
    end
end
prefixUnique = unique(prefix);


cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));
cellIdx = cellfun(cellfind('cell'),prefixUnique);
trackIdx = cellfun(cellfind('track'),prefixUnique);
fluoIdx = ~or(cellIdx, trackIdx);
fluoPrefix = prefixUnique(fluoIdx);

assignin('base','FluoPrefix', fluoPrefix)

fluoTableIdx =  1:numel(fluoPrefix);
IDs = repmat(fluoTableIdx,1,size(rawData,1))';
this.QuantificationData.fluo_id = [this.QuantificationData.fluo_id; IDs];
fluoNames = repmat(fluoPrefix',1,size(rawData,1))';
this.QuantificationData.fluo_name = [this.QuantificationData.fluo_name; fluoNames];

if isempty(this.SegmentationData.uuid) % isempty(this.QuantificationData.uuid)
    uuid = 1;
    currentIDs = (uuid:size(rawData,1))';
    insert = repmat(currentIDs,1,size(fluoPrefix,1))';
    this.QuantificationData.setUUID(insert(:))
else
    currentIDs = ((max(this.SegmentationData.uuid) + 1) : ...
        (max(this.SegmentationData.uuid) + ...
        size(rawData,1)))';
    %currentIDs = ((max(this.QuantificationData.uuid) + 1) : ...
    %    (max(this.QuantificationData.uuid) + ...
    %    size(cellXImportedRawData,1)))';
    insert = repmat(currentIDs,1,size(fluoPrefix,1))';
    this.QuantificationData.setUUID([this.QuantificationData.uuid;insert(:)])
end
% Set FluoPrefix to 1 if there was no quantification performed
if isempty(this.QuantificationData.fluo_id )
    fluoPrefix = 1;
end

FluoData = struct();
for tn = 1:NTempFieldNames
    %RepPattern = zeros(numel(FluoPrefix),1);
    currName = strsplit(char(currentFrameFieldNameArray{tn}), '_');
    if strcmp(currName{1}, 'cell')
        repPattern = zeros(numel(prefixUnique(cellIdx)),1);
        %RepPattern(:) = 1;
        insertIdx = repmat(repPattern,size(rawData,1),1);
        currName{1} = 'cell';
        currFieldName = strjoin(currName, '_');
        % if isempty(this.SegmentationData.(char(CurrFieldName)))
        tempArray = nan(size(rawData,1) * ...
            numel(prefixUnique(cellIdx)),1);
        % else
        %    TempArray = this.SegmentationData.(char(CurrFieldName));
        % end
        if strcmp(currFieldName, 'cell_frame')
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData,1),1);
            tempArray(logical(insertIdx)) = repelem(...
                rawData(:,tn), ...
                numel(prefixUnique(cellIdx)));
        elseif strcmp(currFieldName, 'cell_index')
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData,1),1);
            tempArray(logical(insertIdx)) = repelem(...
                rawData(:, tn), ...
                numel(prefixUnique(cellIdx)));
        else
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData, 1), 1);
            tempArray(logical(insertIdx)) = rawData(:,tn);
        end
        this.SegmentationData.(char(currFieldName)) = ...
            [this.SegmentationData.(char(currFieldName));...
            tempArray];
    elseif strcmp(currName{1}, 'track')
        repPattern = zeros(numel(prefixUnique(trackIdx)),1);
        %RepPattern(:) = 1;
        insertIdx = repmat(repPattern,size(rawData,1),1);
        currName{1} = 'track';
        currFieldName = strjoin(currName, '_');
        % if isempty(this.SegmentationData.(char(CurrFieldName)))
        tempArray = nan(size(rawData,1) * ...
            numel(prefixUnique(trackIdx)),1);
        % else
        %    TempArray = this.SegmentationData.(char(CurrFieldName));
        % end
        if strcmp(currFieldName, 'cell_frame')
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData,1),1);
            tempArray(logical(insertIdx)) = repelem(...
                rawData(:,tn), ...
                numel(prefixUnique(trackIdx)));
        elseif strcmp(currFieldName, 'cell_index')
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData,1),1);
            tempArray(logical(insertIdx)) = repelem(...
                rawData(:, tn), ...
                numel(prefixUnique(trackIdx)));
        else
            repPattern(:) = 1;
            insertIdx = repmat(repPattern,size(...
                rawData, 1), 1);
            tempArray(logical(insertIdx)) = rawData(:,tn);
        end
        this.SegmentationData.(char(currFieldName)) = ...
            [this.SegmentationData.(char(currFieldName));...
            tempArray];
    else
        repPattern = zeros(numel(fluoPrefix),1);
        currFluoIdx = find(cellfun(cellfind(currName{1}),fluoPrefix));
        repPattern(currFluoIdx) =  1;
        insertIdx = repmat(repPattern,size(rawData,1),1);
        currName{1} = 'fluo';
        currFieldName = strjoin(currName, '_');
        
        if ~isfield(FluoData, (currFieldName))
            FluoData.(currFieldName) = nan(size(...
                rawData,1) * ...
                numel(fluoPrefix),1);
        end
        tempArray =  FluoData.(currFieldName);
        tempArray(logical(insertIdx)) = rawData(:,tn);
        FluoData.(currFieldName) = tempArray;
    end
end
if ~ isempty(fieldnames(FluoData))
    FluoFieldNames = fieldnames(this.QuantificationData);
    for n = 4:numel(FluoFieldNames)
        if isfield(FluoData, FluoFieldNames{n})
            this.QuantificationData.(FluoFieldNames{n}) = [...
                this.QuantificationData.(FluoFieldNames{n}); ...
                FluoData.(FluoFieldNames{n})];
        end
    end
    remFields = setdiff(fieldnames(FluoData), FluoFieldNames);
    for n = 1:numel(remFields)
        if contains(remFields{1}, {'fluo_bright_median', 'fluo_cell_median', ...
                'fluo_mem_median', 'fluo_nuc_median'})
            res = strsplit(remFields{n}, '_median');
            name = sprintf('%s_q50', res{1});
            this.QuantificationData.(name) = [...
                this.QuantificationData.(name); ...
                FluoData.(remFields{n})];
        else
            if ~isprop(this.QuantificationData, remFields{n})
                % Add non standard quantification fields
                this.QuantificationData.addprop(remFields{n});
            end
            this.QuantificationData.(remFields{n}) = [...
                this.QuantificationData.(remFields{n}); ...
                FluoData.(remFields{n})];
        end
    end
end
this.SegmentationData.setUUID([this.SegmentationData.uuid; currentIDs])

end