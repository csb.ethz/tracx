%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [distLocM, distM] = getMotionCorrNeighbours(this, frameNumber, ...
    neighbourhoodSearchRadius)
% GETMOTIONCORRNEIGHBOURS Computes the eucledian distance among consecutive
% image frames correcting for potential motion. Returns the neighbourhood
% below the neighbourhoodSearchRadius as locial matrix as well as the
% actual distances
%
% Args:
%  this (:obj:`obj`):                      :class:`TrackerData` instance. 
%  frameNumber (:obj:`int`):               Frame number of the consecutive frame
%  neighbourhoodSearchRadius (:obj:`int`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
%
% Returns
% -------
%  distLocM :obj:`array`                 
%                                           Eucledian distance among segmented objects on consecutive image frames below distance threshold
%  distM :obj:`array`                    
%                                           Eucledian distance among segmented objects on consecutive image frames
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

if frameNumber - 1 < min(this.getFieldArray('cell_frame'))
    oldFrameNumber = min(this.getFieldArray('cell_frame'));
else
    oldFrameNumber = frameNumber - 1;
end

xo = this.getFieldArrayForFrame('cell_center_x', oldFrameNumber);
yo = this.getFieldArrayForFrame('cell_center_y', oldFrameNumber);
xof = this.getFieldArrayForFrame('cell_filtered_dif_x', oldFrameNumber);
xof(isnan(xof)) = 0;
yof = this.getFieldArrayForFrame('cell_filtered_dif_y', oldFrameNumber);
yof(isnan(yof)) = 0;
xn = this.getFieldArrayForFrame('cell_center_x', frameNumber);
yn = this.getFieldArrayForFrame('cell_center_y', frameNumber);

distM = pdist2([xo+xof, yo+yof], [xn, yn],  'euclidean');
distLocM = distM < neighbourhoodSearchRadius;

end