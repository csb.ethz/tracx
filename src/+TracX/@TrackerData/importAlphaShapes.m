%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function importAlphaShapes(this, resultDir)
% IMPORTALPHASHAPES Function to import alpha shapes from cells*.mat
% files from the segmentation results directory.
%
% Args:
%    this (:obj:`object`):                            :class:`TrackerData` instance.       
%    resultDir (:obj:`array`):                         Segmentation results folder :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir` 
%
% Returns
% -------
%    void :obj:`-` 
%                                                      Acts on :class:`TrackerData` instance.
%
% :Authors:
%    Thomas Kuendig - initial implementation

    numOfResults = numel(dir(fullfile(resultDir,["cells*.mat"])));
    counter = 1;
            pBarT = this.utils.progressBar(numOfResults, ...
                'Title', 'IMPORTING ALPHASHAPES: Progress' ...
                );
%     if ~isprop(this.SegmentationData, 'cell_alphashape')
%         addprop(this.SegmentationData, 'cell_alphashape');
%     end
    if ~isprop(this.SegmentationData, 'cell_alphashape_red')
        addprop(this.SegmentationData, 'cell_alphashape_red');
    end    
        
    for f = 1:numOfResults
        load(fullfile(resultDir,["cells_" + f + ".mat"]))
        if ~isfield(data, 'alphashape')
            %this.SpatialData.cell_alphashape{counter} = {};
            this.SegmentationData.cell_alphashape_red{counter} = {};
            counter = counter + 1;
        else
            numberOfNewElements = numel(data.alphashape);
            for c = 1:numberOfNewElements
                %this.SpatialData.cell_alphashape{counter} = ...
                %    data.alphashape{c};
                this.SegmentationData.cell_alphashape_red{counter} = ...
                    data.alphashape_red{c};
                counter = counter + 1;
            end
        end
        pBarT.step([], [], []);
        %this.utils.printToConsole(["Imported " + "cells_" + f + ".mat"]);
    end
    this.SegmentationData.cell_alphashape_red = ...
        this.SegmentationData.cell_alphashape_red(...
        ~cellfun('isempty',this.SegmentationData.cell_alphashape_red));
    pBarT.release();
end