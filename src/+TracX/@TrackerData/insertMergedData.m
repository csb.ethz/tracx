%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = insertMergedData(this, data, tracks2Merge)
% INSERTMERGEDDATA Inserts the data after merging the masks of the bud with
% its assigned mother until detected division and re segmentation and
% quantification into :class:`TrackerData`.
%
% Args:
%    this (:obj:`object`):             :class:`TrackerData` instance.       
%    data (:obj:`array` ):             Data array
%    tracks2Merge (:obj:`array`):      Array of track indices to be merged.
%
% Returns
% -------
%    void :obj:`-` 
%
% .. note:: 
%
%    We only want to update relevant segmentation fields that changed
%    but all quantification fields except uuid (key into SegmentationData),
%    fluo_id and fluo_name as they stay the same.
%    'cell_index' has no interpretation anymore as it refers to the
%    initial segmentation masks. the 'final_track_mask' use the track_index as
%    id. Other fields such as cell_diff_x, fingerpint, costs are outdated and
%    could could only be updated by rerunning the full tracker +
%    lineage_reconstruction from the 'final_track_mask' which would be largely
%    redundanant and unnecessary costly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

newData = [data.SegmentationData.cell_frame, data.SegmentationData.cell_index];
[idx,~] = ismember(tracks2Merge(:,1:2), newData, 'rows');
tracks2MergeC = tracks2Merge(idx,[1:2,4]);
[~, ind] = unique(tracks2MergeC(:, 3), 'rows');
tracks2MergeCMatched = tracks2MergeC(ind, :);
[~, val] = ismember(newData, tracks2MergeCMatched(:, 1:2), 'rows');
tracks2MergeCMatchedSort = tracks2MergeCMatched(val, :);

% Update SegmentationData
% replIdx = ismember(this.SegmentationData.uuid, tracks2Merge(:,4));
replIdx = ismember(this.SegmentationData.uuid, tracks2MergeCMatchedSort(:,3));
uuidOld = this.SegmentationData.uuid(replIdx);
uuidNew = tracks2MergeCMatchedSort(:,3);
[~, reLocIdx] = ismember(uuidOld, uuidNew);
fieldNames2Replace = {'cell_center_x', 'cell_center_y', 'cell_majoraxis', ...
    'cell_minoraxis', 'cell_orientation', 'cell_area', 'cell_volume', ...
    'cell_perimeter', 'cell_eccentricity', 'cell_fractionOfGoodMembranePixels', ...
    'cell_mem_area', 'cell_mem_volume', 'cell_nuc_radius', 'cell_nuc_area'};

for name = fieldNames2Replace
    this.SegmentationData.(char(name))(replIdx) = ...
        data.SegmentationData.(char(name))(reLocIdx);
end

% Update QuantificationData
tracks2MergeCMatchedSortU = [tracks2MergeCMatchedSort, data.SegmentationData.uuid];
[~,locQ] = ismember(data.QuantificationData.uuid, tracks2MergeCMatchedSortU(:, 4));
mappedOldUuid = tracks2MergeCMatchedSortU(locQ, 3);
fieldNames = fieldnames(this.QuantificationData);
delIdx = contains(fieldNames, {'uuid', 'fluo_id', 'fluo_name'});
fieldNames(delIdx) = [];
replIdx = ismember(this.QuantificationData.uuid, tracks2MergeCMatchedSort(:,3));
uuidOld = this.QuantificationData.uuid(replIdx);
[~, reLocIdx] = ismember(uuidOld, mappedOldUuid);

for name = fieldNames'
    this.QuantificationData.(char(name))(replIdx) = ...
        data.QuantificationData.(char(name))(reLocIdx);
end

end