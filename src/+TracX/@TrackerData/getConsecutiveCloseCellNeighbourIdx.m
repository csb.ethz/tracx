%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [joinedNeighbours, joinedNeighboursWMotion, oldNeighbours, newNeighbours] = ...
    getConsecutiveCloseCellNeighbourIdx(xO, xOF, yO, yOF, trO, xN, yN, trN, ...
    neighbourhoodSearchRadius)
% GETCONSECUTIVECLOSECELLNEIGHBOURIDX Returns the neighbourhood indices for
% consecutive image frames. It returns the joined neighbourhood for
% segmentation objects existing on both image frames, for segmentation
% objects only on the first and second frame respectively. The shape of the
% matrix is first times second frame in the order of the track index of the
% segmentation objects on the respective frames.
%
% Args:      
%    xO (:obj:`array`, 1xN):                   Array with x coordinates from oldFrame.
%    xOF (:obj:`array`, 1xN):                  Array with filtered x coordinates from oldFrame.
%    yO (:obj:`array`, 1xN):                   Array with y coordinates from oldFrame.
%    yOF (:obj:`array`, 1xN):                  Array with filtered y coordinates from oldFrame.
%    trO (:obj:`array`, 1xN):                  Array with track indices from oldFrame.
%    xN (:obj:`array`, 1xN):                   Array with x coordinates from newFrame.
%    yN (:obj:`array`, 1xN):                   Array with y coordinates from newFrame.
%    trN (:obj:`array`, 1xN):                  Array with track indices from newFrame.
%    neighbourhoodSearchRadius (:obj:`int`):   :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius` 
%
% Returns
% -------
%    joinedNeighbours :obj:`array` 
%                                             Array with joined
%                                             neighbourhood between old and
%                                             new frame.
%    joinedNeighboursWMotion :obj:`array` 
%                                             Array with joined
%                                             neighbourhood including motion
%                                             between old and new frame.
%    oldNeighbours :obj:`array` 
%                                             Array with neighbourhood 
%                                             of oldFrame.
%    newNeighbours :obj:`array` 
%                                             Array with neighbourhood 
%                                             of newFrame.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

lOldFrame = numel(trO);
lNewFrame = numel(trN);

dOldFrame = pdist([xO, yO]) <= neighbourhoodSearchRadius;
dOldFrameIdx = squareform(dOldFrame);

dNewFrame = pdist([xN, yN]) <= neighbourhoodSearchRadius;
dNewFrameIdx = squareform(dNewFrame);

joinedNeighbours = pdist2([xO, yO], [xN,yN]) <= neighbourhoodSearchRadius;
joinedNeighboursWMotion = pdist2([xO+xOF, yO+yOF], [xN, yN], 'euclidean') ...
    <= neighbourhoodSearchRadius;

id = zeros(lOldFrame, lNewFrame);
r = find(ismember(trO, trN));
c = find(ismember(trN, trO));
if ~or(isempty(r), isempty(c))
    idx = sub2ind(size(id), r, c);
    % Note: add diag to have itself assignment as well.
    id(idx) = 1;
end

if isempty(dOldFrameIdx)
    oldNeighbours = logical(dOldFrameIdx);
else
    oldNeighbours = logical(dOldFrameIdx * id); 
end

%%
id2 = zeros(lOldFrame, lNewFrame);
r = find(ismember(trO, trN));
c = find(ismember(trN, trO));
if ~or(isempty(r), isempty(c))
    idx = sub2ind(size(id2), r, c);
    % Note: add diag to have itself assignment as well.
    id2(idx) = 1;
end

newNeighbours = logical(id2 * dNewFrameIdx); 

end