%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function currAlphaShapes = importAlphaShapesForFrame(~, frame, ...
    resultDir, reducedShapes)
% IMPORTALPHASHAPESFORFRAME Function to import alpha shapes from cells*.mat
% files for a specific frame.
%
% Args:
%    ignoredArg (:obj:`object`):                       :class:`TrackerData` instance.       
%    frame (:obj:`int`):                               Frame number
%    resultDir (:obj:`array`):                         Segmentation results folder :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir` 
%    reducedShapes (:obj:`bool`):                      Flag if reduced alpha shapes should be loaded.
%
% Returns
% -------
%    current_alphashapes :obj:`array` 
%                                                      Imported alphashape
%                                                      for current frame.
%
% :Authors:
%    Thomas Kuendig - initial implementation

load(fullfile(resultDir, ["cells_" + frame + ".mat"]))

if ~isfield(data, 'alphashape')
    currAlphaShapes = {};
else
    if reducedShapes
        currAlphaShapes = data.alphashape_red;
    else
        currAlphaShapes = data.alphashape;
    end
end

% this.utils.printToConsole(["Imported " + "cells_" + f + ".mat"]);

end