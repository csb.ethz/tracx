%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ uuidsOutsideMask, outsideMaskIdx ] = getUUIDOutsideROI(this, ...
    imgSize, roi )
% GETUUIDOUTSIDEROI Returns data uuids for cells on an image
% outside a region of interest (roi). Roi can be either an offset value 
% from the image border or an impoly object.
%
% Args:
%    this (:obj:`object`):             :class:`TrackerData` instance.       
%    imgSize (:obj:`array`, 1x2):      Size of image.
%    roi (:obj:`array`):               Image border offset or a 
%                                      impoly object
%
% Returns
% -------
%    uuidsOutsideMask :obj:`array` kx1
%                                       Array with data uuids outside 
%                                       of the roi masked image
%                                       region.
%    outsideMaskIdx :obj:`array` kx1
%                                       Indices array for data outside 
%                                       of the roi masked image
%                                       region.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

rmin = 0;
rmax = imgSize(1); 
cmin = 0;
cmax = imgSize(2);

if isa(roi,'double') && numel(all(size(roi))) == 1
    % Create mask from image border offset
    offset = roi;
    xCoords = [cmin + offset, cmax - offset, cmax - offset, cmin + offset];
    yCoords = [rmin + offset, rmin + offset, rmax - offset, rmax - offset];
    
    mask = poly2mask(xCoords, yCoords, imgSize(1), imgSize(2));
else
    % Create mask from polygon roi
    mask = createMask(roi); % roi needs to be an impoly object
end

if any(imgSize>200)
    mask = imresize(mask, 1/(imgSize(1)/200));
end
maskIdx = find(mask == 1);

[maskRow, maskCol] = ind2sub([size(mask, 1), size(mask, 2)], maskIdx);

x = this.getFieldArray('cell_center_x');
y = this.getFieldArray('cell_center_y');

if any(imgSize(1)>200)
   x = round(x .* (1/(imgSize(1)/200)));
   y = round(y .* (1/(imgSize(1)/200)));
end
uuids = this.getFieldArray('uuid');

outsideMaskIdx = ~inpolygon(x, y, maskCol, maskRow);
uuidsOutsideMask = uuids(outsideMaskIdx);

end
%% Add resizing to say 200x200! to speed up things