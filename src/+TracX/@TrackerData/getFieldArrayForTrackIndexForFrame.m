%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ selectedFieldnameValueArray ] = ...
    getFieldArrayForTrackIndexForFrame(this, fieldName, ...
    frameNumber, selectedTrackArray, varargin)
% GETFIELARRAYFORTRACKINDEXFORFRAME Returns an array with
% values for a particular fieldname, frame and selected track.
%
% Args:
%    this (:obj:`object`):                      :class:`TrackerData` instance.
%    fieldName (:obj:`str`):                    Fieldname where to get
%                                               values from.
%    frameNumber (:obj:`str`):                  Frame number for which
%                                               one wants to get the
%                                               fieldname values.
%    selectedTrackArray (:obj:`array`, kx1):    Array of track indicies
%                                               for which one wants to
%                                               get the values for the
%                                               fieldname.
%    varargin (:obj:`str varchar`):
%
%        * **fluoID** (:obj:`str`):             Fluorescent channel id :class:`~+TracX.@QuantificationData.QuantificationData.fluo_id`.
%
% Returns
% -------
%    selectedFieldnameValueArray: :obj:`array`
%                                               Returns the value array
%                                               for the selected track
%                                               indicies for the given
%                                               fieldName on the given
%                                               frame.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

tracksOnCurrentFrame = this.SegmentationData.track_index(...
        this.SegmentationData.cell_frame == frameNumber);
uuidOnCurrentFrame = this.SegmentationData.uuid(...
        this.SegmentationData.cell_frame == frameNumber);
    
if any(ismember(fieldnames(this.SegmentationData), fieldName))
    if ~ isempty(this.SegmentationData.(fieldName))
        fieldValueOnCurrentFrame = this.SegmentationData.(fieldName)(...
            this.SegmentationData.cell_frame == frameNumber);
        [idx, loc] = ismember(tracksOnCurrentFrame, selectedTrackArray);
        selectedFieldnameValueArrayTmp = fieldValueOnCurrentFrame(idx);
        
        selectedFieldnameValueArray = nan(size(selectedTrackArray));
        % Reorder result that it matches the order of the input
        %     selectedFieldnameValueArray = selectedFieldnameValueArray(...
        %         loc(loc>=1));
        selectedFieldnameValueArray(loc(loc>=1)) = selectedFieldnameValueArrayTmp;
    else
        selectedFieldnameValueArray = nan;
    end
elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
    fluoID = this.QuantificationData.fluo_id(ismember(...
        this.QuantificationData.uuid, uuidOnCurrentFrame));
    array = this.QuantificationData.(fieldName)(ismember(...
        this.QuantificationData.uuid, uuidOnCurrentFrame));
    if isempty(varargin)
        array = array(fluoID == 1);
    else
        array = array(fluoID == varargin{1});
    end
    
    [idx, loc] = ismember(tracksOnCurrentFrame, selectedTrackArray);
    selectedFieldnameValueArrayTmp = array(idx);
        
    selectedFieldnameValueArray = nan(size(selectedTrackArray));
    % Reorder result that it matches the order of the input
    %     selectedFieldnameValueArray = selectedFieldnameValueArray(...
    %         loc(loc>=1));
    selectedFieldnameValueArray(loc(loc>=1)) = selectedFieldnameValueArrayTmp;
else
    selectedFieldnameValueArray = [];
    this.utils.printToConsole('ERROR: Unknown field name') 
end
end