%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [candPairID] = getMaskBasedDPairs(~, maskParentFrame, maskDaughterFrame, ...
    parentCandidate)
% GETMASKBASEDDPAIRS Based on tracked segmentation masks the most likely
% daughter pair track indices for a parent candidate will be returned.
% Otherwise -1. Based onthe candidates with the highest intersection over
% the union score.
%
% Args:
%    this (:obj:`object`):                    :class:`TrackerData` instance.
%    maskParentFrame (:obj:`double`):         Parent segmentation masks frame.
%    maskDaughterFrame (:obj:`double`):       Daughter segmentation masks frame.
%    parentCanditate (:obj:`int`):            Parent candidate track index.
%
% Returns
% -------
%    candPairID: :obj:`array`
%                                                     Array of the daughter   
%                                                     candidates.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

parentMaskIdx = maskParentFrame == parentCandidate;
intersectingDID = unique(maskDaughterFrame .* (parentMaskIdx & maskDaughterFrame));

% Remove background ID == 0
intersectingDID = intersectingDID(2:end); 

% Evaluate all possible daughter pair combinations
candDPairs = nchoosek(intersectingDID, 2);
[iouScore] = deal(size(candDPairs, 1));
for k = 1:size(candDPairs, 1)
   u = nnz(ismember(maskDaughterFrame, candDPairs(k, :)) | parentMaskIdx);
   if u > 0
        d = nnz(ismember(maskDaughterFrame, candDPairs(k, :)) & parentMaskIdx) / u;
   else
        d = 0;
   end 
   iouScore(k) = d;
end
if sum(iouScore == max(iouScore)) == 1
    if iouScore(iouScore == max(iouScore)) == 0
        candPairID = -1; % No pair
    else
        candPairID = candDPairs(iouScore == max(iouScore), :);
    end
else
    candPairID = -1; % Either no overlap or no unique candidate
end
end