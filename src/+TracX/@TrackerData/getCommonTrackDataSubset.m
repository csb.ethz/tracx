%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ oldFrameSubset, newFrameSubset ] = getCommonTrackDataSubset(~, ...
    oldFrame, newFrame, varargin )
% GETCOMMONTRACKDATASUBSET Subsets oldFrame and newFrame data
% struct for common track indicies and returns the position for both
% frames sorted.
%
% Args:
%    ignoredArg (:obj:`obj`):       :class:`TrackerData` instance.
%    oldFrame (:obj:`obj`):         Old frame instance
%    newFrame (:obj:`obj`):         New frame instance
%    varargin (:obj:`str varchar`):
%
%        * **newTrackIndex** (:obj:`array`):   Newly assigned track indicies for newFrame
%
% Returns
% -------
%     oldFrameSubset: :obj:`array` kx2         
%                                              Data subset for only x and y position of
%                                              the oldFrame.
%     newFrameSubset: :obj:`array` kx2         
%                                              Data subset for only x and y position of
%                                              the newFrame.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if isempty(varargin)
    newTrackIndex = newFrame.track_index;
else
    newTrackIndex = varargin{1};
end
    commonLocIdxOldFrame = ismember(oldFrame.track_index, ...
        newTrackIndex);
    commonLocIdxNewFrame = ismember(newTrackIndex, ...
        oldFrame.track_index);

[matchingTracksOld, matchingTracksOldSortIdx] = sort(oldFrame.track_index(...
    commonLocIdxOldFrame));
[matchingTracksNew, matchingTracksNewSortIdx] = sort(newTrackIndex(...
    commonLocIdxNewFrame));

if ~isempty(commonLocIdxOldFrame) && any(commonLocIdxOldFrame)
    matchingOldFrame(:,1) = oldFrame.cell_center_x(commonLocIdxOldFrame);
    matchingOldFrame(:,2) = oldFrame.cell_center_y(commonLocIdxOldFrame);
    matchingOldFrame(:,3) = oldFrame.cell_center_z(commonLocIdxOldFrame);
    matchingNewFrame(:,1) = newFrame.cell_center_x(commonLocIdxNewFrame);
    matchingNewFrame(:,2) = newFrame.cell_center_y(commonLocIdxNewFrame);
    matchingNewFrame(:,3) = newFrame.cell_center_z(commonLocIdxNewFrame);

    % Subset of all matching track indicies between two image frames. Contains
    % also potential wrong assignments due to large cell movements
    oldFrameSubset = [matchingTracksOld matchingOldFrame(...
        matchingTracksOldSortIdx, :)];
    newFrameSubset = [matchingTracksNew matchingNewFrame(...
        matchingTracksNewSortIdx, :)];
else
    oldFrameSubset = [];
    newFrameSubset = [];
end
end