%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = deleteData( this, dataIDToDelete, neighbourhoodSearchRadius)
% DELETEDATA Deletes data elements (segmented objects). I.e cells as
% they are outside of a  ROI, to close to the image border or should be 
% excluded due togating.
%
% Args:
%    this (:obj:`obj`):                        :class:`TrackerData` instance.
%    dataIDToDelete (:obj:`array`, 1xk):       Array with uuids of cells                                                        
%                                              to be deleted.
%    neighbourhoodSearchRadius (:obj:`int`):   Neighbourhood radius. Defaults to 
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
%
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`TrackerData` object
%                                               directly and deletes selected
%                                               data.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Delete segmentation data
uuidArray = this.getFieldArray('uuid');
toDeleteIdx = ismember(uuidArray, dataIDToDelete);
fieldNamesS = fieldnames(this.SegmentationData);
NFieldNames = numel(fieldNamesS);
for k = 1:NFieldNames
    if ~isempty(this.SegmentationData.(fieldNamesS{k})) && ~strcmp((...
            fieldNamesS{k}), 'cell_close_neighbour') && ~strcmp((...
            fieldNamesS{k}), 'uuid')
        this.SegmentationData.(fieldNamesS{k})(toDeleteIdx) = [];
    elseif strcmp((fieldNamesS{k}), 'uuid')
        this.SegmentationData.setUUID(this.SegmentationData. ...
            (fieldNamesS{k})(~toDeleteIdx))
    end
end

% Delete quantification data
uuidArray = this.QuantificationData.uuid;
toDeleteIdx = ismember(uuidArray, dataIDToDelete);
fieldNamesQ = fieldnames(this.QuantificationData);
NFieldNames = numel(fieldNamesQ);
for k = 1:NFieldNames
    if ~isempty(this.QuantificationData.(fieldNamesQ{k})) && ...
        ~strcmp((fieldNamesQ{k}), 'uuid')
        this.QuantificationData.(fieldNamesQ{k})(toDeleteIdx) = [];
    elseif strcmp((fieldNamesQ{k}), 'uuid')
        this.QuantificationData.setUUID(this.QuantificationData.(...
            fieldNamesQ{k})(~toDeleteIdx));
    end
end

% Recalculate neighbourhood
frameArray = unique(this.getFieldArray('cell_frame'))';
for frame = frameArray
   neighbourIdx = this.getCloseCellNeighbourIdx(this.getFieldArrayForFrame(...
       'cell_center_x', frame), this.getFieldArrayForFrame('cell_center_y', ...
       frame), neighbourhoodSearchRadius);
   this.SegmentationData.cell_close_neighbour(frame).Indicies = neighbourIdx;
end

end
