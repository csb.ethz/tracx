%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = delByFeature(this, feature, threshold, neighbourhoodSearchRadius)
% DELBYFEATURE Delete cell segmentations exceeding the feature threshold
% value from tracking.
%
% Args:
%    this (:obj:`obj`):                        :class:`TrackerData` instance.
%    feature (:obj:`str`):                     Any segmentation feature to
%                                              use to threshold the data before
%                                              tracking. :class:`~+TracX.@SegmentationData.SegmentationData`
%    threshold (:obj:`float`):                 Threshold. Feature values
%                                              below the threshold are selected.
%    neighbourhoodSearchRadius (:obj:`int`):   Neighbourhood radius. Defaults to 
%                                              :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`TrackerData` object
%                                               directly and deletes selected
%                                               data below threshold.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
validFieldNames = [fieldnames(this.SegmentationData); fieldnames(this.QuantificationData)];
validFeature =  @(x) any(validatestring(x, validFieldNames)); 
validThreshold = @(x) isnumeric(x); 
validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
addRequired(p, 'this', @isobject);
addRequired(p, 'feature', validFeature);
addRequired(p, 'threshold', validThreshold);
addRequired(p, 'neighbourhoodSearchRadius', validNeighbourhoodSearchRadius);
parse(p, this, feature, threshold, neighbourhoodSearchRadius);

featureVal = this.getFieldArray(p.Results.feature);
uuid = this.getFieldArray('uuid');

this.utils.printToConsole('DELETE: cell indices:')

d = uuid(featureVal<p.Results.threshold)';
fac = ceil(size(d, 2)/10);
dd = nan(10*fac, 1);
dd(1:numel(d)) = d;
ddd = reshape(dd', 10, [])';
for k = 1:size(ddd, 1)
    this.utils.printToConsole(sprintf('DELETE: %s', regexprep(num2str(ddd(k, ...
        ~isnan(ddd(k,:)))),'\s+',', ')))
end

% Delete the data
this.deleteData(uuid(featureVal<p.Results.threshold), p.Results.neighbourhoodSearchRadius)

this.utils.printToConsole('DELETE: Done')

end