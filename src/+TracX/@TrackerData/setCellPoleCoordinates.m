%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ this ] = setCellPoleCoordinates( this )
% SETCELLPOLECOORDINATES Calculates the two cell poles
% coordinates (x,y) from cell major axis and cell orientation given
% the cell center coordinate array for rod shaped segmentation
% objects.
%
% Args:
%    this (:obj:`object`):                    :class:`TrackerData` instance.      
%
% Returns
% -------
%    void :obj:`-`                               
%                                             Calculated field cell_poles with
%                                             coordinates for cell poles of symetrically
%                                             dividing cells saved to :class:`TrackerData`.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

pole1X = this.SegmentationData.cell_pole1_x;
pole1Y = this.SegmentationData.cell_pole1_y;
pole2X = this.SegmentationData.cell_pole2_x;
pole2Y = this.SegmentationData.cell_pole2_y;

% Calculate coordinate deviation in x and y direction
dy = sind(abs(this.SegmentationData.cell_orientation)) .* ...
    (this.SegmentationData.cell_majoraxis / 2);
% True if cell_center_x and y is realy set in the cell middle.
dx = cosd(abs(this.SegmentationData.cell_orientation)) .* ...
    (this.SegmentationData.cell_majoraxis / 2);

% Calculate pole coordinates
% for angles 0 - 90
cond1 = this.SegmentationData.cell_orientation >0;%<= 0;
pole1X(cond1, 1) = this.SegmentationData.cell_center_x(cond1) + dx(cond1);
pole1Y(cond1, 1) = this.SegmentationData.cell_center_y(cond1) + dy(cond1);
pole2X(cond1, 1) = this.SegmentationData.cell_center_x(cond1) - dx(cond1);
pole2Y(cond1, 1) = this.SegmentationData.cell_center_y(cond1) - dy(cond1);
% for angles 0 - -90
cond2 = this.SegmentationData.cell_orientation <=0; %> 0;
pole1X(cond2, 1) = this.SegmentationData.cell_center_x(cond2) + dx(cond2);
pole1Y(cond2, 1) = this.SegmentationData.cell_center_y(cond2) - dy(cond2);
pole2X(cond2, 1) = this.SegmentationData.cell_center_x(cond2) - dx(cond2);
pole2Y(cond2, 1) = this.SegmentationData.cell_center_y(cond2) + dy(cond2);

% fill coordinate arrays into data struct
% this.SegmentationData.cell_pole1_x = pole1X;
% this.SegmentationData.cell_pole1_y = pole2Y;
% this.SegmentationData.cell_pole2_x = pole2X;
% this.SegmentationData.cell_pole2_y = pole1Y;
this.SegmentationData.cell_pole1_x(cond1) = pole1X(cond1);
this.SegmentationData.cell_pole1_y(cond1) = pole2Y(cond1);
this.SegmentationData.cell_pole2_x(cond1) = pole2X(cond1);
this.SegmentationData.cell_pole2_y(cond1) = pole1Y(cond1);
% Mirror poles due to negative orientation to be consistent
this.SegmentationData.cell_pole1_x(cond2) = pole2X(cond2);
this.SegmentationData.cell_pole1_y(cond2) = pole1Y(cond2);
this.SegmentationData.cell_pole2_x(cond2) = pole1X(cond2);
this.SegmentationData.cell_pole2_y(cond2) = pole2Y(cond2);

end