%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ colormap ] = getFieldArrayColorMap(this, varargin)
% GETFIELDARRAYCOLORMAP Returns a colormap for the properties/ field arrays.
% Special colormaps are returned for 'track_index', 'track_parent'. If
% currTrack is given, all other tracks will be colored in grey.
%
% Args:
%    this (:obj:`obj`):                        :class:`TrackerData` instance.
%    varargin (:obj:`str varchar`):
%
%        * **property** (:obj:`str`):          Property of :class:`~+TracX.@SegmentationData.SegmentationData` for which colormap shold be returned. Defaults to :class:`~+TracX.@SegmentationData.SegmentationData.track_index`.
%        * **currTrack** (:obj:`int`):         Current track index if single track should be highlighted. Defaults to 0.
%
% Returns
% -------
%     colormap: (:obj:`array`) kx3        
%                                              Returns the colormap
%
% :Authors:
%    - Thomas Kuendig - initial implementation in GUI
%    - Andreas P. Cuny - implementation in TracX core.

defaultProperty = 'track_index';
defaultCurrentTrack = 0;
validFieldNames = [fieldnames(this.SegmentationData); ...
    fieldnames(this.QuantificationData)];
validTracks = this.getFieldArray('track_index');

p = inputParser;
p.addRequired('this', @isobject);
p.addOptional('property', defaultProperty, ...
    @(x) any(validatestring(x, validFieldNames)))
p.addOptional('currTrack', defaultCurrentTrack, ...
    @(x) any([0, validTracks]))
parse(p, this, varargin{:})

property = p.Results.property;
currTrack = p.Results.currTrack;
uniqueTracks = unique(this.getFieldArray('track_index'))';

if strcmp(property, 'track_index')
    cMap = hsv(length(uniqueTracks));
    colormap = cMap(this.getFieldArray('track_index'), :);
elseif strcmp(property,'track_parent')
    colormap = this.utils.getRecursiveTrackParentColoring(0, [0.5 0.5 0.5], ...
        zeros(length(this.getFieldArray('track_index')), 3), ...
        this.getFieldArray('track_index'), ...
        this.getFieldArray('track_parent'));
else
    cMap = jet(64);
    values = this.getFieldArray(current_property);
    LuT = linspace(min(values) ,max(values), 64);
    colormap = interp1(LuT, cMap, values);
end
colormap(isnan(colormap)) = 0;

if currTrack ~= 0
    currentTracks = this.getFieldArray('track_index') ~= currTrack;
    colormap(currentTracks, 1) = 0.5;
    colormap(currentTracks, 2) = 0.5;
    colormap(currentTracks, 3) = 0.5;
end
end