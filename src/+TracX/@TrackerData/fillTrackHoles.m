%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function fillTrackHoles(this, imageFrameNumberArray, ...
    fingerprintHalfWindowSideLength, ...
    fingerprintMaxConsideredFrequencies, imageDir, ...
    imageFingerprintFileArray, imageCropCoordinateArray, ...
    neighbourhoodSearchRadius)
% FILLTRACKHOLES is supposed to create a virtual segmentation
% object for image frames where a certain track index is
% missing but the image frame before and after still contain
% the object (cell). This holes (in time) exist when a cell
% was not segmented (detected) on a particular image frame
% but does exist.
%
% Args:
%    this (:obj:`obj`):                                :class:`TrackerData` instance.
%    imageFrameNumberArray (:obj:`array`, kx1):        Array of all existing image
%                                                      frames for the given experiment.
%    fingerprintHalfWindowSideLength (:obj:`int`):     :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintHalfWindowSideLength`
%    fingerprintMaxConsideredFrequencies (:obj:`int`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintMaxConsideredFrequencies`
%    imageDir (:obj:`str`):                            :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageDir`
%    imageFingerprintFileArray (:obj:`array`):         :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageFingerprintFileArray`
%    imageCropCoordinateArray (:obj:`array`, 1x4):     :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`
%    neighbourhoodSearchRadius (:obj:`int`):           Neighbourhood radius. Defaults to 
%                                                      :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% Returns
% -------
%     void: (:obj:`-`)          
%                                                      Acts on :class:`TrackerData`.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

trackIndexArray = unique(this.SegmentationData.track_index);
for iTrack = 1:numel(trackIndexArray)
    frameTrackOccurance = ismember(imageFrameNumberArray, ...
        this.SegmentationData.cell_frame(...
        this.SegmentationData.track_index == trackIndexArray(...
        iTrack)));
    framesTrackAbsent = strfind(frameTrackOccurance, 0);
    framesTrackPresent = strfind(frameTrackOccurance, 1);
    fillIdx =  framesTrackAbsent < max(framesTrackPresent) & ...
        framesTrackAbsent > min(framesTrackPresent);
    
    absentFrames = framesTrackAbsent(fillIdx);
    for kFrame = 1:numel(absentFrames)
        prefFrame = absentFrames(kFrame) - 1;
        x = this.getFieldArrayForTrackIndexForFrame(...
            'cell_center_x', prefFrame, trackIndexArray(iTrack));
        y = this.getFieldArrayForTrackIndexForFrame(...
            'cell_center_y', prefFrame, trackIndexArray(iTrack));
        majorAxis = this.getFieldArrayForTrackIndexForFrame(...
            'cell_majoraxis', prefFrame, trackIndexArray(iTrack));
        minorAxis = this.getFieldArrayForTrackIndexForFrame(...
            'cell_minoraxis', prefFrame, trackIndexArray(iTrack));
        angle = this.getFieldArrayForTrackIndexForFrame(...
            'cell_orientation', prefFrame, trackIndexArray(iTrack));
        xx = this.getFieldArrayForFrame('cell_center_x', ...
            absentFrames(kFrame));
        yy = this.getFieldArrayForFrame('cell_center_y', ...
            absentFrames(kFrame));
        ti = this.getFieldArrayForFrame('track_index', ...
            absentFrames(kFrame));
        % Determine which cell centers are within the area
        % occupied of the track in the previous frame
        pointInsideEllipseArray = TracX.Functions. ...
            isPointInsideEllipse( xx, yy,  x, y, angle, ...
            majorAxis, minorAxis);
        % Results below 1 are inside the ellipse spanned by the
        % cell of interest
        trackWithinTrackArray = ti(pointInsideEllipseArray<1);
        % Here modify existing data by flagging and deleting and
        % by inserting for missing segmentation
        for lTrack = 1:numel(trackWithinTrackArray)
            this.setFieldnameValuesForTracksOnFrame(...
                'track_contained_in_track', absentFrames(kFrame), ...
                trackWithinTrackArray(lTrack), ...
                trackIndexArray(iTrack))
        end
        % Insert new cell
        this.insertNewData( absentFrames(kFrame), ...
            trackIndexArray(iTrack), ...
            fingerprintHalfWindowSideLength, ...
            fingerprintMaxConsideredFrequencies, imageDir, ...
            imageFingerprintFileArray, imageCropCoordinateArray, ...
            neighbourhoodSearchRadius)
        
    end
end
end