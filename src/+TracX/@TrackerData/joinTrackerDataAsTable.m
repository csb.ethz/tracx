%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function joinTrackerDataAsTable(this)
% JOINTRACKERDATAASTABLE column binds the fields of
% SegmentationData and QuantificationData in wide format.
%
% Args:
%    this (:obj:`object`):                   :class:`TrackerData` instance.      
%
% Returns
% -------
%    void :obj:`-`                               
%                                             
% :Authors:
%    Andreas P. Cuny - initial implementation

% Prepare segmentation and tracking data by removing fileds
% which can not be converted into the long format
segFieldNames = fieldnames(this.SegmentationData);
idx = ismember(segFieldNames, {'uuid', ...
    'cell_close_neighbour', ...
    'track_fingerprint_real'});
segFieldNames = segFieldNames(~idx);
nEntries = size( this.SegmentationData.cell_frame, 1);
for l = 1:numel(segFieldNames)
    if isempty(this.SegmentationData.(segFieldNames{l}))
        this.joinedTrackerData.(segFieldNames{l}) = nan(nEntries, 1);
    else
        this.joinedTrackerData.(segFieldNames{l}) = this. ...
            SegmentationData.(segFieldNames{l});
    end
end

% Prepare QuantificationData by renaming 'fluo' with the actual
% fluo_name
fluoNames = unique(this.QuantificationData.fluo_name);
fieldNames = fieldnames(this.QuantificationData);
rawFieldNames = fieldNames(4:end);
for k = 1:numel(fluoNames)
    fieldNamesOut = strrep(rawFieldNames,'fluo',fluoNames(k));
    for j = 1:numel(rawFieldNames)
        if ~isempty(this. ...
                QuantificationData.(rawFieldNames{j}))
            
            this.joinedTrackerData.(fieldNamesOut{j}) = this. ...
                QuantificationData.getFieldNameForFluoChannel(...
                rawFieldNames{j}, fluoNames{k});
        end
    end
end

% Convert the struct to table
this.joinedTrackerDataTable = struct2table(this.joinedTrackerData);

end