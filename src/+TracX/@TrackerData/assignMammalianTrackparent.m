%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [newFrameData, splits] = assignMammalianTrackparent(this,...
    newFrameData, assignment, parentCandidateArray, parentCandidateType,...
    daughterPairs, frame, printOutput)
% ASSIGNMAMMALIANTRACKPARENT Assignes the track parent for symmetrical cell
% division of convex shapes such as for mammalian cells.
%
% Args:
%    this (:obj:`object`):                   :class:`TrackerData` instance. 
%    newFrameData (:obj:`object`):           newFrame data 
%    assignment (:obj:`object`):             Assignment result
%    parentCandidateArray (:obj:`array`):    Array with parent candidates
%    parentCandidateType (:obj:`int`):       Type of parents (0 or 1).
%    daughterPairs (:obj:`array`):           Array with daughter pairs
%    frame (:obj:`int`):                     Image frame to be evaluated 
%    printOutput (:obj:`bool`):              Flag, if text output should be
%                                            displayed in the console.
%
% Returns
% -------
%    newFrameData :obj:`object`            
%                                            Parent set for daughter pair.
%    splits :obj:`object`                          
%                                            Array with potential splits
%                                            for continous track.
%                                             
% :Authors:
%    Thomas Kuendig - initial implementation

splits = zeros(1,length(this.SegmentationData.track_index));

assert(length(parentCandidateArray)==length(parentCandidateType));

for i = 1:length(assignment)
    parent = parentCandidateArray(i);
    daughters = daughterPairs(i,:);
    
    % identify splits
    if ismember(parent,daughters) && ~any(daughters == 0)
        aftersplit = this.SegmentationData.track_index == parent & ...
            this.SegmentationData.cell_frame >= frame;
        splits(find(aftersplit,1,'first')) = 1; 
    % identify joins
    elseif any(daughters == 0) && parentCandidateType(i) ~= 1
        aftersplit = this.SegmentationData.track_index == daughters(daughters ~= 0); 
         splits(find(aftersplit,1,'first')) = -parent; 
    end
    
    daughters = daughters(daughters ~= parent & daughters ~= 0);
    
    if printOutput && ~isempty(daughters)
        this.utils.printToConsole(sprintf("Track %3d is parent of %11s in frame %3d.",parent,strjoin(compose("%3d",daughters),' and '),frame));
    end
    
    for d = daughters
        daughterframes = this.SegmentationData.track_index==d;
        this.SegmentationData.track_parent(daughterframes) = ...
            repmat(parent,sum(daughterframes),1);
        daughterframes = newFrameData.track_index==d;
        newFrameData.track_parent(daughterframes) = ...
            repmat(parent,sum(daughterframes),1);
    end
end
end