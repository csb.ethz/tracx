%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TrackerData < handle
% TRACKERDATA Creates an object to store and manipulate data
% used in a Tracker project. The Data itself is stored in the objects
% SegmentationData and QuantificationData.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

    properties
        % Data
        SegmentationData % :class:`+TracX.@SegmentationData` stores SegmentationData and Tracking results
        QuantificationData % :class:`+TracX.@QuantificationData` stores fluorescence quantification data
        OldFrame % :class:`+TracX.@TemporaryTrackingData` instance holding the current image frame data.
        NewFrame % :class:`+TracX.@TemporaryTrackingData` instance holding the subsequent image frame data.
        VeryOld % :class:`+TracX.@TemporaryTrackingData` instance holding the previous image frame data of non assigned tracks.
        OldFrameEval % :class:`+TracX.@TemporaryTrackingData` instance holding the previous image frame data for evaluation.
        NewFrameEval % :class:`+TracX.@TemporaryTrackingData` instance holding the current image frame data for evaluation.
        VeryOldEval   % :class:`+TracX.@TemporaryTrackingData` instance object holding the of non assigned tracks after evaluation.
        TmpLinAss % :class:`+TracX.@TemporaryLineageAssignment` instance object holding data of lineage assignments.
       
        cellDivisionMarkerData  % Stores optional additional cell division marker segmentation and signal quantifiaction.
        numberOfTracks = [] % Highest used track index number
        
        joinedTrackerData = struct(); %> JoinedTrackerData is a struct holding tracking results along with :class:`+TracX.@SegmentationData` and :class:`+TracX.@QuantificationData` 
        joinedTrackerDataTable = []; %> JoinedTrackerData is a table holding tracking results along with :class:`+TracX.@SegmentationData` and :class:`+TracX.@QuantificationData` for export to disk.
        io % :class:`+TracX.@IO` instance implementing read/write methods
        utils % :class:`+TracX.@Utils` instance implementing utility methods
    end
    
    methods
        
        % Constructor
        function this = TrackerData()
            % TrackerData Constructs a TrackerData object to manipulate
            % and store segmentation and tracking data.
            %
            % Returns
            % -------
            %    void :obj:`-`    
            %
            this.SegmentationData = TracX.SegmentationData();
            this.QuantificationData = TracX.QuantificationData();
            this.OldFrame = TracX.TemporaryTrackingData();
            this.NewFrame = TracX.TemporaryTrackingData();
            this.VeryOld = TracX.TemporaryTrackingData();
            this.OldFrameEval = TracX.TemporaryTrackingData();
            this.NewFrameEval = TracX.TemporaryTrackingData();
            this.VeryOldEval = TracX.NonAssignedTracks();
            this.TmpLinAss = TracX.TemporaryLineageAssignment();
            this.utils = TracX.Utils();
            this.io = TracX.IO(this.utils);
        end
         
        function this = importData(this, pathName, fileNameArray,  ...
                maxNrOfFrames)
            % IMPORTDATA Imports segmentation data from single files.
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    pathName (:obj:`array`):    The path to where the data is
            %                                stored.
            %    fileNameArray (:obj:`int`): Cell array with the filename(s)
            %                                of the data to import.
            %    maxNrOfFrames (:obj:`int`): Max number of files to import
            %                                (if not whole experimental
            %                                data of a long time series
            %                                should be imported.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                :class:`TrackerData` instance
            %                                with imported data.
            
            if numel(fileNameArray) < maxNrOfFrames
                maxNrOfFrames = numel(fileNameArray);
            end
            for fn = 1:maxNrOfFrames
                
                % Read data frame by frame
                [ ~, dataMatrix, fieldNameArray] = this.io. ...
                    readSegmentationData( pathName, fileNameArray(fn));
                
                if all(dataMatrix{1}(2:end) == 0)
                    continue
                else
                    this.reshapeDataToTrackerTableFormat( dataMatrix{1}, ...
                        fieldNameArray{1});
                end
            end
            
            if isempty(this.SegmentationData.track_index)
                this.initializeTrackingFields();
            else
                % Data which can not be imported from text files gets
                % initialized here
                this.SegmentationData.track_fingerprint_real =  cell(...
                    numel(this.SegmentationData.track_index), 1);
            end
        end
        
        %
        %  ---- Getters ----
        %
        
        [ selectedFieldnameValueArray ] = ...
            getFieldArrayForTrackIndexForFrame(this, fieldName, ...
            frameNumber, selectedTrackArray, varargin)
        % GETFIELARRAYFORTRACKINDEXFORFRAME Returns an array with
        % values for a particular fieldname, frame and selected track.
        

        function nRows = getNumberOfRowsFromFrame(this, frameNumber)
            % GETNUMBEROFROWSFORFRAME Returns the number of rows in
            % the SegmentationData object for the selected frameNumber.
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):   Image frame number.
            %
            % Returns
            % -------
            %    nRows :obj:`int`
            %                                Number of rows in 
            %                                :class:`+TracX.@SegmentationData` 
            %                                for frameNumber.
            
            nRows = numel(this.SegmentationData.cell_frame(...
                this.SegmentationData.cell_frame == frameNumber));
        end
        
        function array = getFieldArrayForTrackIndex(this, fieldName, trackIndex, ...
                varargin)
            % GETFIELDARRAYFORTRACKINDEX Returns the data array for a
            % track and given fieldname of the  :class:`+TracX.@SegmentationData`
            % object.
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):     :class:`+TracX.@SegmentationData` field name.
            %    trackIndex (:obj:`int`):    A track_index from :class:`+TracX.@SegmentationData.SegmentationData.track_index`
            %    varargin (:obj:`str varchar`):
            %
            %        * **fluoID** (:obj:`int`): Fluo channel ID for which the fieldName data should be returned.
            %
            % Returns
            % -------
            %    array :obj:`array`
            %                                Array of data for a given track 
            %                                and fieldName.
            
            if any(ismember(fieldnames(this.SegmentationData), fieldName))
                array = this.SegmentationData.(fieldName)(...
                    this.SegmentationData.track_index == trackIndex);
            elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
                uuid = this.SegmentationData.uuid(...
                    this.SegmentationData.track_index == trackIndex);
                fluoID = this.QuantificationData.fluo_id(ismember(...
                    this.QuantificationData.uuid, uuid));
                array = this.QuantificationData.(fieldName)(ismember(...
                    this.QuantificationData.uuid, uuid));
                if isempty(varargin)
                    array = array(fluoID == 1);
                else
                    array = array(fluoID == varargin{1});
                end
            else
                array = [];
                this.utils.printToConsole('ERROR: Unknown field name')
            end
        end
               
        function ret = getConditionalFieldArray(this, fieldName, ...
                fieldNameCond, condition, varargin )
            % GETCONDITIONALFIELDARRAY Returns an array with
            % all the data for a given fieldName 
            % (:class:`+TracX.@SegmentationData` property)
            % matching a given condition for the fieldNameCon
            % (:class:`+TracX.@SegmentationData` property).
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):     String with fieldname where to
            %                                get values from.
            %    fieldNameCond (:obj:`str`): String with fieldname to match
            %                                the condition.
            %    condition (:obj:`array`):   Array with condition to match the
            %                                match with the fieldNameCond.
            %
            % Returns
            % -------
            %    ret :obj:`array`
            %                                Array of selected data.
            
            try
                if any(ismember(fieldnames(this.SegmentationData), fieldName))
                    ret = this.SegmentationData.(fieldName)(ismember(...
                    this.SegmentationData.(fieldNameCond), condition));
                elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
                    uuid = this.SegmentationData.uuid(ismember(...
                    this.SegmentationData.(fieldNameCond), condition));
                    fluoID = this.QuantificationData.fluo_id(ismember(...
                        this.QuantificationData.uuid, uuid));
                    array = this.QuantificationData.(fieldName)(ismember(...
                        this.QuantificationData.uuid, uuid));
                    if isempty(varargin)
                        ret = array(fluoID == 1);
                    else
                        ret = array(fluoID == varargin{1});
                    end
                else
                    ret = [];
                    this.utils.printToConsole('ERROR: Unknown field name')
                end
            catch ME
                error([ME.message])
            end
        end
        
        function ret = getFieldArrayForFrame(this, fieldName, ...
                frameNumber, varargin)
            % GETFIELDARRAYFORFRAME Returns an array with
            % all the data for a given fieldName (:class:`+TracX.@SegmentationData` property)
            % for a given frameNumber (image frame).
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):     String with fieldname where to
            %                                get values from.
            %    frameNumber (:obj:`int`):   Frame number for which the fieldName 
            %                                data should be returned.
            %    varargin (:obj:`str varchar`):
            %
            %        * **fluoID** (:obj:`int`): Fluo channel ID for which the fieldName data should be returned.
            %
            % Returns
            % -------
            %    ret :obj:`array`
            %                                Array of selected data.
            
            try
                if any(ismember(fieldnames(this.SegmentationData), fieldName))
                    array = this.SegmentationData.(fieldName);
                elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
                    uuid = this.SegmentationData.uuid;
                    fluoID = this.QuantificationData.fluo_id(ismember(...
                        this.QuantificationData.uuid, uuid));
                    array = this.QuantificationData.(fieldName)(ismember(...
                        this.QuantificationData.uuid, uuid));
                    if isempty(varargin)
                        array = array(fluoID == 1);
                    else
                        array = array(fluoID == varargin{1});
                    end
                else
                    array = [];
                end
                
                if ~isempty(array)
                    ret = array(this.SegmentationData.cell_frame == frameNumber);
                else
                    ret = [];
                    this.utils.printToConsole('ERROR: Unknown field name')
                end
            catch ME
                error([ME.message])
            end
        end
        
        function array = getFieldArrayForSubsetOnFrame(this, fieldName, ...
                frame, subset, varargin)
            % GETFIELDARRAYFORSUBSETONFRAME Returns an array with
            % the data for a given fieldName (:class:`+TracX.@SegmentationData` property) 
            % for a given frameNumber (image frame) and datasubset.
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):     String with fieldname where to
            %                                get values from.
            %    subset (:obj:`array`):      Logical array to subset selected data.
            %    varargin (:obj:`str varchar`):
            %
            %        * **fluoID** (:obj:`int`): Fluo channel ID for which the fieldName data should be returned.
            %
            % Returns
            % -------
            %    array :obj:`array`
            %                                Array of selected data.
                        
            if any(ismember(fieldnames(this.SegmentationData), fieldName))
                fieldArrayOnCurrentFrame = this.SegmentationData.(fieldName)(...
                    this.SegmentationData.cell_frame == frame);
                array = fieldArrayOnCurrentFrame(subset);
            elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
                uuid = this.SegmentationData.uuid(...
                    this.SegmentationData.cell_frame == frame);
                fluoID = this.QuantificationData.fluo_id(ismember(...
                    this.QuantificationData.uuid, uuid));
                array = this.QuantificationData.(fieldName)(ismember(...
                    this.QuantificationData.uuid, uuid));
                if isempty(varargin)
                    array = array(fluoID == 1);
                else
                    array = array(fluoID == varargin{1});
                end
                array = array(subset);
            else
                array = [];
                this.utils.printToConsole('ERROR: Unknown field name')
            end
        end
        
        function array = getFieldArray(this, fieldName, varargin)
            % GETFIELDARRAY Returns an array with
            % all the data for any fieldName (:class:`+TracX.@SegmentationData` property)
            % for all frameNumbers (image frames).
            %
            % Args:
            %    this (:obj:`object`):       :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):     String with fieldname where to
            %                                get values from.
            %    varargin (:obj:`str varchar`):
            %
            %        * **fluoID** (:obj:`int`): Fluo channel ID for which the fieldName data should be returned.
            %
            % Returns
            % -------
            %    array :obj:`array`
            %                                Array of selected data.
                        
            if any(ismember(fieldnames(this.SegmentationData), fieldName))
                array = this.SegmentationData.(fieldName);
            elseif any(ismember(fieldnames(this.QuantificationData), fieldName))
                uuid = this.SegmentationData.uuid;
                fluoID = this.QuantificationData.fluo_id(ismember(...
                    this.QuantificationData.uuid, uuid));
                array = this.QuantificationData.(fieldName)(ismember(...
                    this.QuantificationData.uuid, uuid));
                if isempty(varargin)
                    array = array(fluoID == 1);
                else
                    array = array(fluoID == varargin{1});
                end
            else
                array = [];
                this.utils.printToConsole('ERROR: Unknown field name')
             end
            
        end
        
        function [ dataStruct ] = getTemporaryDataStruct(this, fieldNameArray, imageFrame )
            % GETTEMPORARYDATASTRUCT Returns a struct of a subset of
            % :class:`+TracX.@SegmentationData` based on field names and a given
            % frame number.
            %
            % Args:
            %    this (:obj:`object`):          :class:`TrackerData` instance.
            %    fieldNameArray (:obj:`array`): Cell array with valid fieldnames for
            %                                   :class:`+TracX.@SegmentationData`
            %                                   that should be returned.
            %    imageFrame (:obj:`int`):       Image frame for which data should be
            %                                   returned.
            %
            % Returns
            % -------
            %    dataStruct :obj:`array`
            %                                   Data struct for given imageFrame
            %                                   number and all field names in
            %                                   fieldNameArray.
            dataStruct = struct();
            
            for iFieldName = 1:length(fieldNameArray)
                dataStruct.(fieldNameArray{iFieldName}) = this.getFieldArrayForFrame(...
                    fieldNameArray{iFieldName}, imageFrame );
            end
        end
        
        [distLocM, distM ] = getMotionCorrNeighbours(this, frameNumber, ...
            neighbourhoodSearchRadius)
        % GETMOTIONCORRNEIGHBOURS Computes the eucledian distance among consecutive
        % image frames correcting for potential motion. Returns the neighbourhood
        % below the neighbourhoodSearchRadius as locial matrix as well as the
        % actual distances
        
        
        
        %
        % ---- Setters ----
        %
        
        function setFieldnameValuesForTracksOnFrame(this, fieldName, ...
                frameNumber, trackIndexArray, fieldDataArray)
            % SETFIELDARRAYFORTRACKSONFRAME Sets data
            % for a particular fieldname, frame and selected track.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):         String for the fieldName to
            %                                    set new data.
            %    frameNumber (:obj:`int`):       Frame number for which one
            %                                    wants to set the
            %                                    fieldName data.
            %    trackIndexArray (:obj:`array`): Array of track indicies for which one
            %                                    wants to set new data into the fieldName
            %                                    data array.
            %    fieldDataArray (:obj:`array`):  Data array for the trackIndexArray
            %                                    for the given fieldName on 
            %                                    the given frame.
            %
            % Returns
            % -------
            %    void :obj:`-`
            %                                    Sets data to :class:`TrackerData`
            
            if isempty(fieldDataArray)
                TracX.Tracker.printToConsole('fieldDataArray is empty')
            else
                
                tracksOnCurrentFrame = this.SegmentationData.track_index(...
                    this.SegmentationData.cell_frame == frameNumber);
                if ~isempty(this.SegmentationData.(fieldName))
                    fieldValueOnCurrentFrame = this.SegmentationData.(fieldName)(...
                        this.SegmentationData.cell_frame == frameNumber);
                    fieldValueOnCurrentFrame(ismember(tracksOnCurrentFrame, ...
                        trackIndexArray)) = fieldDataArray;
                    this.setFieldArrayForFrame(fieldName, frameNumber, ...
                        fieldValueOnCurrentFrame )
                    %setFieldArrayForFrame(this, fieldName, frameNumber, ...
                    %    fieldValueOnCurrentFrame )
                end
            end
        end
        
        function setFieldArrayForFrame(this, fieldName, frameNumber,...
                fieldDataArray )
            % SETFIELDARRAYFORFRAME Sets data for a particular fieldname
            % in the SegmentationData object of TrackerData.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):         String for the fieldName to
            %                                    set new data.
            %    frameNumber (:obj:`int`):       Frame number for which one
            %                                    wants to set the
            %                                    fieldName data.
            %    fieldDataArray (:obj:`array`):  Data array to add to fieldName.
            %
            % Returns
            % -------
            %    void :obj:`-`
            %                                    Sets data to :class:`TrackerData`
            
            this.SegmentationData.(fieldName)(...
                this.SegmentationData.cell_frame == frameNumber) = ...
                fieldDataArray;
        end

        function setFieldnameValueForTrack(this, fieldName, trackIndex, value)
            % SETFIELDNAMEVALUEFORTRACK Sets data for a particular
            % fieldname and track in the :class:`SegmentationData` object of 
            % :class:`TrackerData`.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):         String for the fieldName to
            %                                    set new data.
            %    trackIndex (:obj:`int`):        Track index of the data to be set.
            %    value (:obj:`float`):           Data value to add to fieldName
            %                                    for all trackIndex
            %                                    occurances.
            %
            % Returns
            % -------
            %    void :obj:`-`
            %                                    Sets data to :class:`TrackerData`
            
            fieldNameArray = this.SegmentationData.(fieldName)(...
                this.SegmentationData.track_index == trackIndex);
            
            if size(value, 1) == size(fieldNameArray, 1)
                newVal = value;
            elseif size(value, 2) == size(fieldNameArray, 1)
                newVal = value';
            else
                newVal = repmat(value, numel(fieldNameArray), 1);
            end
            
            this.SegmentationData.(fieldName)(...
                this.SegmentationData.track_index == trackIndex) = newVal;
                
        end
        
        function setFieldnameValueForTrackSubset(this, fieldName, trackIndex, ...
                frameArray, value)
            % SETFIELDNAMEVALUEFORTRACKSUBSET Sets data for a particular
            % fieldname and track for a given frame subset 
            % in the :class:`SegmentationData` object of :class:`TrackerData`.           
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):         String for the fieldName to
            %                                    set new data.
            %    trackIndex (:obj:`int`):        Track index of the data to be set.
            %    frameArray (:obj:`int`):        Frame array of the data to be set.
            %    value (:obj:`float`):           Data value to add to fieldName
            %                                    for all trackIndex occurances.
            %
            % Returns
            % -------
            %    void :obj:`-`
            %                                    Sets data to :class:`TrackerData`
            
            fieldNameArray = this.SegmentationData.(fieldName)(...
                this.SegmentationData.track_index == trackIndex);
            trackFrameArray = this.SegmentationData.('cell_frame')(...
                this.SegmentationData.track_index == trackIndex);
            
            newValue = value(ismember(frameArray, trackFrameArray));
            fieldNameArray(ismember(trackFrameArray, frameArray)) = newValue;
                        
            this.SegmentationData.(fieldName)(...
                this.SegmentationData.track_index == trackIndex) = fieldNameArray;
               
        end
        
        function setFieldArray(this, fieldName, fieldDataArray )
            % SETFIELDARRAY Sets data for a particular fieldname in
            % the :class:`SegmentationData` object of :class:`TrackerData`.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    fieldName (:obj:`str`):         String for the fieldName to
            %                                    set new data.
            %    fieldDataArray (:obj:`array`):  Data array to add to fieldName.
            %
            % Returns
            % -------
            %    void :obj:`-`
            %                                    Sets data to :class:`TrackerData`
            
            this.SegmentationData.(fieldName) = fieldDataArray;
        end
        
        [ this ] = setCellPoleCoordinates( this )
        % SETCELLPOLECOORDINATES Calculates the two cell poles
        % coordinates (x,y) from cell major axis and cell orientation given
        % the cell center coordinate array for rod shaped segmentation
        % objects.
        
        [ this ] = setParentSymDivCells( this, startFrame, endFrame, ...
            imageFrameNumberArray, segmentationResultDir, trackerResultMaskFileArray)
        % SETPARENTSYMDIVCELLS Determines the relationship
        % between daughter and mother cell of symmetricaly dividing cells.

        [ candPairID ] = getMaskBasedDPairs(~, maskParentFrame, maskDaughterFrame, ...
            parentCandidate)
        % GETMASKBASEDDPAIRS Based on tracked segmentation masks the most likely
        % daughter pair track indices for a parent candidate will be returned.

        
        [newFrameData, splits] = assignMammalianTrackparent(this, newFrameData, ...
            assignment, parentCandidateArray, parentCandidateType, ...
            daughterpairs, frame, printOutput)
        % ASSIGNMAMMALIANTRACKPARENT Assignes the track parent for symmetrical cell
        % division of convex shapes such as for mammalian cells.

        fillTrackHoles(this, imageFrameNumberArray, ...
            fingerprintHalfWindowSideLength, ...
            fingerprintMaxConsideredFrequencies, imageDir, ...
            imageFingerprintFileArray, imageCropCoordinateArray, ...
            neighbourhoodSearchRadius)
        % FILLTRACKHOLES is supposed to create a virtual segmentation
        % object for image frames where a certain track index is
        % missing but the image frame before and after still contain
        % the object (cell). This holes (in time) exist when a cell
        % was not segmented (detected) on a particular image frame
        % but does exist.
        
        function this = initializeLineageDataFields(this)
            % INITIALIZELINEAGEDATAFIELDS Initializes :class:`SegmentationData`
            % fields reqired for the lineage reconstruction.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    lineage related fields.
            
            Ncells = size(this.SegmentationData.uuid);
            this.SegmentationData.cell_pole1_x = nan(Ncells);
            this.SegmentationData.cell_pole1_y = nan(Ncells);
            this.SegmentationData.cell_pole2_x = nan(Ncells);
            this.SegmentationData.cell_pole2_y = nan(Ncells);
            this.SegmentationData.cell_pole1_age = nan(Ncells);
            this.SegmentationData.cell_pole2_age = nan(Ncells);
            this.SegmentationData.track_parent = zeros(Ncells);
            this.SegmentationData.track_parent_frame = zeros(Ncells);
            this.SegmentationData.track_parent_prob = zeros(Ncells);
            this.SegmentationData.track_parent_score = zeros(Ncells);
            this.SegmentationData.track_generation = nan(Ncells);
            this.SegmentationData.track_lineage_tree = nan(Ncells);
            this.SegmentationData.track_cell_cycle_phase = nan(Ncells);
            this.SegmentationData.track_budneck_total = nan(Ncells);
        end
        
        function this = initializeCellCloseNeighbours(this, neighbourRadius)
            % INITIALIZECELLCLOSENEIGHBOURS Initializes the cell
            % neighbourhood index matrix for all cells within neighbourRadius
            % related fields of the :class:`SegmentationData` object.           
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    neighbourRadius (:obj:`int`):   Distance in pixel to consider
            %                                    other cells to be a neighbouring
            %                                    cell.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized 
            %                                    :class:`~+TracX.@SegmentationData.SegmentationData.cell_close_neighbour` 
            %                                    index matrix.
            
            for iFrame = 1:max(this.SegmentationData.cell_frame)
                x = this.SegmentationData.cell_center_x(...
                    ismember(this.SegmentationData.cell_frame, iFrame));
                y = this.SegmentationData.cell_center_y(...
                    ismember(this.SegmentationData.cell_frame, iFrame));
                z = this.SegmentationData.cell_center_z(...
                    ismember(this.SegmentationData.cell_frame, iFrame));
                this.SegmentationData.cell_close_neighbour(iFrame).Indicies = ...
                    this.getCloseCellNeighbourIdx(x, y, neighbourRadius, z);
            end
            
        end
        
        function this = initializeTrackingFields(this)
            % INITIALIZETRACKINGFIELDS Initializes the to tracking
            % related fields of the :class:`SegmentationData` object.
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    tracking fields.
            
            % Initialize tracking fields globally
            Nrows = numel(this.SegmentationData.cell_frame);
            
            this.SegmentationData.track_fingerprint_real =  cell(Nrows, 1);
            this.SegmentationData.track_fingerprint_real_distance =  nan(Nrows, 1);
            this.SegmentationData.track_assignment_fraction = nan(Nrows, 1);
            this.SegmentationData.track_age =  zeros(numel(this.SegmentationData.cell_frame), 1);
            this.SegmentationData.track_index_qc = nan(Nrows, 1);
            this.SegmentationData.track_index_corrected = zeros(Nrows, 1);
            this.SegmentationData.track_contained_in_track = nan(Nrows, 1);
            this.SegmentationData.track_has_bud = zeros(Nrows, 1);
            this.SegmentationData.track_budneck_total = nan(Nrows, 1);
            
            names = {'track_index', 'cell_dif_x', 'cell_dif_y', 'cell_dif_z', ...
                'cell_filtered_dif_x', 'cell_filtered_dif_y', 'cell_filtered_dif_z', ...
                'track_assignment_fraction', ...
                'track_total_assignment_cost', 'track_position_assignment_cost', ...
                'track_area_assignment_cost', 'track_rotation_assignment_cost', ...
                'track_frame_skipping_cost'};
            for name = names
                this.SegmentationData.(char(name)) = nan(Nrows, 1);   
            end
            
        end
        
        function this = initializeOldFrame(this, frameNumber)
            % INITIALIZEOLDFRAME Initailizes the OldFrame property of the
            % Tracker with data from the :class:`TrackerData` object for the 
            % first image frame (frameNumber == 1).
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):       Image frame number.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    OldFrame.
            
            % startF = this.configuration.ProjectConfiguration.trackerStartFrame;
            ofl = length(this.getFieldArrayForFrame('cell_frame', frameNumber));
            
            this.OldFrame.addData((1:ofl).', (1:ofl).', ones(ofl,1),...
                this.getFieldArrayForFrame('cell_index', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_x', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_y', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_z', frameNumber), ...
                zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), ...
                zeros(ofl,1), zeros(ofl,1), ...
                this.getFieldArrayForFrame('cell_orientation', frameNumber), ...
                this.getFieldArrayForFrame('cell_area', frameNumber), ...
                this.SegmentationData.cell_close_neighbour(frameNumber).Indicies, ...
                zeros(ofl,1),...
                this.getFieldArrayForFrame('track_fingerprint_real', frameNumber),...
                this.getFieldArrayForFrame('track_fingerprint_real_distance', frameNumber),...
                this.getFieldArrayForFrame('track_assignment_fraction', frameNumber),...
                this.getFieldArrayForFrame('track_age', frameNumber));
            
        end
        
        function this = initializeOldFrameEval(this, frameNumber)
            % INITIALIZEOLDFRAMEEVAL Initailizes the OldFrameEval
            % property of the :class:`TrackerData` with data from the :class:`TrackerData`
            % object for the first given image frame (frameNumber).
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):       Image frame number.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    OldFrameEval.
            
            ofl = length(this.getFieldArrayForFrame('cell_frame', ...
                frameNumber ));
            
            this.OldFrameEval.addData(this.getFieldArrayForFrame(...
                'track_index', frameNumber), [], ones(ofl,1), [], ...
                this.getFieldArrayForFrame('cell_center_x', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_y', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_z', frameNumber), [], ...
                [], [], zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), this.getFieldArrayForFrame( ...
                'cell_orientation', frameNumber),this.getFieldArrayForFrame( ...
                'cell_area', frameNumber),  this.SegmentationData. ...
                cell_close_neighbour(frameNumber).Indicies, [], ...
                this.getFieldArrayForFrame('track_fingerprint_real', ...
                frameNumber), ...
                this.getFieldArrayForFrame('track_fingerprint_real_distance', ...
                frameNumber), this.getFieldArrayForFrame('track_assignment_fraction', ...
                frameNumber), []);
           
        end
        
        function this = initializeNewFrame(this, frameNumber)
            % INITIALIZENEWFRAME Initailizes the NewFrame property of the
            % Tracker with data from the :class:`TrackerData` object for the 
            % current image frame (frameNumber).
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):       Image frame number.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    NewFrame.
            
            % NewFrame needs no trackindex or age therefore it will be
            % initialized empty
            ofl = length(this.getFieldArrayForFrame('cell_frame', frameNumber ));
            
            this.NewFrame.addData([], [], [], ...
                this.getFieldArrayForFrame('cell_index', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_x', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_y',frameNumber), ...
                this.getFieldArrayForFrame('cell_center_z', frameNumber), ...
                zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), ...
                zeros(ofl,1), zeros(ofl,1), ...
                this.getFieldArrayForFrame('cell_orientation', frameNumber), ...
                this.getFieldArrayForFrame('cell_area', frameNumber), ...
                this.SegmentationData.cell_close_neighbour(frameNumber).Indicies, ...
                zeros(ofl,1),...
                this.getFieldArrayForFrame('track_fingerprint_real', frameNumber),...
                this.getFieldArrayForFrame('track_fingerprint_real_distance', frameNumber),...
                this.getFieldArrayForFrame('track_assignment_fraction', frameNumber),...
                this.getFieldArrayForFrame('track_age', frameNumber));
            
        end
        
        function this = initializeNewFrameEval(this, frameNumber)
            % INITIALIZENEWFRAMEEVAL Initailizes the NewFrameEval
            % property of the :class:`TrackerData` with data from the :class:`TrackerData`
            % object for the first given image frame (frameNumber).
            %
            % Args:
            %    this (:obj:`object`):           :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):       Image frame number.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                    :class:`TrackerData`
            %                                    object with initialized
            %                                    NewFrameEval.
            
            ofl = length(this.getFieldArrayForFrame('cell_frame', ...
                frameNumber ));
            
            this.NewFrameEval.addData(this.getFieldArrayForFrame(...
                'track_index', frameNumber), [], ones(ofl,1), [], ...
                this.getFieldArrayForFrame('cell_center_x', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_y', frameNumber), ...
                this.getFieldArrayForFrame('cell_center_z', frameNumber), [], ...
                [], [], zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), this.getFieldArrayForFrame( ...
                'cell_orientation', frameNumber),this.getFieldArrayForFrame( ...
                'cell_area', frameNumber), this.SegmentationData. ...
                cell_close_neighbour(frameNumber).Indicies, [], ...
                this.getFieldArrayForFrame('track_fingerprint_real', ...
                frameNumber), ...
                this.getFieldArrayForFrame('track_fingerprint_real_distance', ...
                frameNumber), this.getFieldArrayForFrame('track_assignment_fraction', ...
                frameNumber), []);
            
        end
        
        function this = InitializeVeryOld(this, assignmentRowIdx, ...
                maxTrackFrameSkipping)
            % INITIALIZEVERYOLD Initailizes the VeryOld property of the
            % Tracker with data from OldFrame.
            %
            % Args:
            %    this (:obj:`object`):               :class:`TrackerData` instance.
            %    assignmentRowIdx (:obj:`array`):    Array with assignment row
            %                                        indices.
            %    maxTrackFrameSkipping (:obj:`int`): Max tolerated
            %                                        frames a track is
            %                                        allowed to be missing.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                        :class:`TrackerData`
            %                                        object with initialized
            %                                        VeryOld.
            
            % Initialisation of VeryOld from OldFrame
            this.VeryOld.addData(this.OldFrame.track_index, ...
                this.OldFrame.track_index_qc, ...
                this.OldFrame.track_age, this.OldFrame.cell_index,...
                this.OldFrame.cell_center_x, ...
                this.OldFrame.cell_center_y, this.OldFrame.cell_center_z, this.OldFrame.cell_dif_x, ...
                this.OldFrame.cell_dif_y, this.OldFrame.cell_dif_z, ...
                this.OldFrame.cell_filtered_dif_x, this.OldFrame.cell_filtered_dif_y, ...
                this.OldFrame.cell_filtered_dif_z, this.OldFrame.cell_orientation, ...
                this.OldFrame.cell_area, this.OldFrame.cell_close_neighbour, ...
                this.OldFrame.cell_del, ...
                this.OldFrame.track_fingerprint_real, ...
                this.OldFrame.track_fingerprint_real_distance, ...
                this.OldFrame.track_assignment_fraction, ...
                this.OldFrame.track_fingerprint_age);
            
            fieldNames = fieldnames(this.VeryOld);
            iFieldName = 1;
            while iFieldName <= numel(fieldNames)
                kAssignment = numel(assignmentRowIdx);
                while kAssignment > 0
                    if size(this.VeryOld.(fieldNames{iFieldName}), 2) == 1
                        this.VeryOld.(fieldNames{iFieldName})(...
                            assignmentRowIdx(kAssignment)) = [];
                    else
                        if ~isempty(this.VeryOld.(fieldNames{iFieldName}))
                            this.VeryOld.(fieldNames{iFieldName})(...
                                assignmentRowIdx(kAssignment), :) = [];
                        end
                    end
                    kAssignment = kAssignment - 1;
                end
                iFieldName = iFieldName + 1;
            end
            
            % Remove too old old tracks (track_age above
            % maxTrackFrameSkipping threshold)
            tooOldTrackArray = (find(this.VeryOld.track_age > ...
                maxTrackFrameSkipping));
            iFieldName = 1;
            while iFieldName <= numel(fieldNames)
                kAssignment = numel(tooOldTrackArray);
                while kAssignment > 0
                    this.VeryOld.(fieldNames{iFieldName})(...
                        tooOldTrackArray(kAssignment)) = [];
                    kAssignment = kAssignment - 1;
                end
                iFieldName = iFieldName + 1;
            end
            
            if isempty(this.VeryOld.cell_close_neighbour)
                this.VeryOld.cell_close_neighbour = [];
            end
            
        end
        
        function saveTrackingAssignmentsToTrackerData(this, frameNumber, ...
                dataToSave)
            % SAVETRACKINGASSIGNMENTSTOTRACKERDATA Saves data formated as
            % struct to :class:`TrackerData`. Requires valid field names
            % matching the propery names of :class:`SegmentationData`
            %
            % Args:
            %    this (:obj:`object`):               :class:`TrackerData` instance.
            %    frameNumber (:obj:`int`):           Image frame number.
            %    dataToSave (:obj:`array`):          Struct with data to be
            %                                        saved.
            %
            % Returns
            % -------
            %    void :obj:`-`
            
            
%             dataProps = properties(this.SegmentationData);
%             propertiesToSave = {'track_index', 'cell_dif_x', 'cell_dif_y', ...
%                 'cell_filtered_dif_x', 'cell_filtered_dif_y', ...
%                 'track_total_assignment_cost', ...
%                 'track_position_assignment_cost', 'track_area_assignment_cost', ...
%                 'track_rotation_assignment_cost', 'track_frame_skipping_cost'};
%             for k = 1:numel(propertiesToSave)
%                 this.SegmentationData.(dataProps{strcmp(dataProps, ...
%                     propertiesToSave{k})}) = [...
%                     this.SegmentationData.(dataProps{strcmp(dataProps, ...
%                     propertiesToSave{k})}); ...
%                     dataToSave(:, k)];
%             end
              names = fieldnames(dataToSave);
              for name = names'
                 this.setFieldArrayForFrame(char(name), frameNumber, ...
                dataToSave.(char(name)));
              end
            
        end
        
        function updateTemporaryTrackingDataForNextFrameEvaluation(this, ...
                neighbourhoodSearchRadius)
            % UPDATETEMPORARYTRACKINGDATAFORNEXTFRAMEEVALUATION Updates the
            % temporary tracking data for the next frame assignment
            % evaluation by merging NewFrame and VeryOld data and moving it
            % to OldFrame.
            %
            % Args:
            %    this (:obj:`object`):               :class:`TrackerData` instance.
            %    neighbourhoodSearchRadius:  [Int]   Radius in pixel within an other
            %                                        point defined by the coordinate
            %                                        arrays x and y is considered to
            %                                        be a neighbour of (x_i,y_i).
            %                                        :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
            %
            % Returns
            % -------
            %    void :obj:`-`
            
            dataProps = properties(this.OldFrame);
            for k = 1:numel(dataProps)
                if strcmp(dataProps{k}, 'cell_close_neighbour') ~= 1
                    this.OldFrame.(dataProps{k}) = [this.NewFrame.(dataProps{k}); ...
                        this.VeryOld.(dataProps{k})];
                else
                    this.OldFrame.(dataProps{k}) = this. ...
                        getCloseCellNeighbourIdx(this.OldFrame.cell_center_x, ...
                        this.OldFrame.cell_center_y, neighbourhoodSearchRadius, ...
                        this.OldFrame.cell_center_z);
                end
            end
        end
        
        [ this ] = reshapeDataToTrackerTableFormat( this, ...
            cellXImportedRawData, currentFrameFieldNameArray)
        % RESHAPEDATATOTRACKERTABLEFORMAT Reshapes the imput data
        % table i.e. CellX results table into the correct width.
        % CellX adds the quantification of multiple fluorophores as
        % additional columns to the data table which makes it hard to
        % deal with this kind of data of various colum length. Therefore
        % we reshape the data into a thight colum format specifing
        % potential multiple fluorophores as additional row identified
        % by a id (fluo.id) and the name as string in (fluo.name).
        
        insertNewData(this, absentFrameArray, trackArray, ...
            fingerprintHalfWindowSideLength, ...
            fingerprintMaxConsideredFrequencies, ...
            imageDir, imageQCFiles, segmentationImageCrop, ...
            neighbourRadius)
        % INSERTNEWDATA Inserts a virtual object into
        % SegmentationData for a certain track in a certain image frame.
        % Due to miss- or missing segmentation on some frames the existing
        % data is wrong or missing and should not be used for tracking. But
        % that a cell trace continues to exist on all image frames a
        % virtual 'cell' with properties from the last existing valid
        % occurance wil be added.
       
        function  updateTrackIndicies(this, oldTrackIndiciesArray, ...
                newTrackIndiciesArray )
            % UPDATETRACKINDICIES Updates the track indicies globally
            % for the whole data structure based on a old and new track_index
            % array.
            %
            % Args:
            %    this (:obj:`object`):                 :class:`TrackerData` instance.
            %    oldTrackIndiciesArray (:obj:`array`): Array with old track
            %                                          indicies to be updated
            %    newTrackIndiciesArray (:obj:`array`): Array with new track
            %                                          indicies to update the
            %                                          old ones with.
            %
            % Returns
            % -------
            %    void (:obj:`-`):    
            
            locIdxNonEqual = find(~eq(oldTrackIndiciesArray, newTrackIndiciesArray));
            if ~isempty(locIdxNonEqual)
                for i = 1:numel(locIdxNonEqual)
                    iIndex = locIdxNonEqual(i);
                    trackToReplace = oldTrackIndiciesArray(iIndex);
                    locIdxData = this.SegmentationData.track_index == trackToReplace;
                    if numel(unique(locIdxData)) > 1
                        this.SegmentationData.track_index(locIdxData) = newTrackIndiciesArray(iIndex);
                    end
                    % sprintf('Replaced %d with %d', trackToReplace, newTrackIndiciesArray(iIndex))
                end
            end
            
        end
        
        joinTrackerDataAsTable(this)
        % JOINTRACKERDATAASTABLE column binds the fields of
        % SegmentationData and QuantificationData in wide format.
        
        function setFinalTrackIndicies( this )
            % SETFINALTRACKINDICIES Sets the final track indicies for the
            % project. Continous numbers are whole data structure based on 
            % a old and new track_index array.
            %
            % Args:
            %    this (:obj:`object`):        :class:`TrackerData` instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`):      
            
            trackIndicies = this.SegmentationData.track_index;
            uniqueTrackIndicies = unique(trackIndicies);
            uniqueTrackIndicies(isnan(uniqueTrackIndicies)) = [];
            [uniqueTrackIndiciesSorted, uniqueTrackIndiciesSortedIdx] = sort(uniqueTrackIndicies);
            trackIndiciesRelabeled = nan(max(uniqueTrackIndiciesSorted), 1);
            trackIndiciesRelabeled(uniqueTrackIndiciesSorted) = uniqueTrackIndiciesSortedIdx;
            this.SegmentationData.track_index(~isnan(trackIndicies)) = ...
                trackIndiciesRelabeled(trackIndicies(~isnan(trackIndicies))); % trackIndiciesRelabeled(trackIndicies);
            
        end
        
        function [ rgbWithMask ] = saveTrackerControlMaskImages(this, imageDir, ...
                segmentationResultDir, imageFingerprintFile, ...
                trackerResultMaskFiles, imageCropCoordinateArray,...
                frameNumber, alpha, path, filename)
            % SAVETRACKERCONTROLMASKIMAGES Saves tracker control mask
            % images.
            %
            % Args:
            %    this (:obj:`object`):                     :class:`TrackerData` instance.
            %    imageDir (:obj:`str`):                    :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageDir`
            %    segmentationResultDir (:obj:`str`):       :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
            %    imageFingerprintFile (:obj:`str`):        :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageFingerprintFileArray`
            %    trackerResultMaskFiles (:obj:`str`):      :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.trackerResultMaskFileArray`
            %    imageCropCoordinateArray (:obj:`array`):  :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`
            %    frameNUmber (:obj:`int`):                 Frame number.
            %    alpha (:obj:`int`):                       Alpha level of mask.
            %    path (:obj:`str`):                        Path to where to save the image to.
            %    filename (:obj:`str`):                    File name for the mask file.
            %
            % Returns
            % -------
            %    rgbWithMask (:obj:`array`):       Array with daughter track
            %                                        indices.
            
            track_index = this.getFieldArrayForFrame('track_index', frameNumber);
            cell_index = this.getFieldArrayForFrame('cell_index', frameNumber);
            track_contained_in_track = this.getFieldArrayForFrame(...
                'track_contained_in_track', frameNumber);
            
            maxUniqueTrackIndicies = max(unique(this.getFieldArray('track_index')));
            
            track_index(~isnan(track_contained_in_track)) = 0;
            
            % Load image and crop to same size as segmentation
            img = TracX.imageIO.readImageToGrayScale(fullfile(...
                imageDir, imageFingerprintFile{frameNumber}), ...
                imageCropCoordinateArray);
            img = TracX.imageGen.normalizeImage(img);
            
            % Load tracking masks
            mask = TracX.imageIO.readSegmentationMask(fullfile(...
                segmentationResultDir, trackerResultMaskFiles{frameNumber}));
            mask = TracX.imageGen.relabelMaskIndices(mask, track_index, ...
                cell_index);
            mask2 = label2rgb(mask, lines(maxUniqueTrackIndicies), 'k'); %jet(maxUniqueTrackIndicies)
            
            % Create RGB from greyscale image
            rgb = img(:,:,[1 1 1]);
            rgbWithMask = imlincomb(1, im2uint8(rgb), alpha, mask2, 'uint8');
            imwrite(rgbWithMask, fullfile(path, filename))
            
        end
        
        function daugtherTrack = getDaugterTrack(this, trackArray, frameNumber)
            % GETDAUGTERTRACK Returns daughter track index for a given
            % track on a given frame
            %
            % Args:
            %    this (:obj:`object`):               :class:`TrackerData` instance.
            %    trackArray (:obj:`array`):          Array of track indices
            %                                        to get daughters from.
            %    frameNUmber (:obj:`int`):           Frame number.
            %
            % Returns
            % -------
            %    daugtherTrack (:obj:`array`):       Array with daughter track
            %                                        indices.
            
            trackStartArray = this.getFieldArrayForTrackIndexForFrame(...
                'track_start_frame', frameNumber, trackArray);
            trackSizeArray = this.getFieldArrayForTrackIndexForFrame(...
                'cell_area', frameNumber, trackArray);
            
            if all(trackStartArray == min(trackStartArray))
                daugtherTrack = trackArray(trackSizeArray == min(trackSizeArray));
            else
                daugtherTrack = trackArray(...
                    trackSizeArray == min(trackSizeArray) & ...
                    max(trackStartArray) == trackStartArray);
            end
            
        end
        
        function [ this ] = clearAllData(this)
            % CLEARALLDATA Deletes all data from TrackerData
            %
            % Args:
            %    this (:obj:`object`):               :class:`TrackerData` instance.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                       :class:`TrackerData` instance
            %                                       with all data deleted.
            
            this.SegmentationData = TracX.SegmentationData();
            this.QuantificationData = TracX.QuantificationData();
            this.OldFrame = TracX.TemporaryTrackingData();
            this.NewFrame = TracX.TemporaryTrackingData();
            this.VeryOld = TracX.TemporaryTrackingData();
            this.OldFrameEval = TracX.TemporaryTrackingData();
            this.NewFrameEval = TracX.TemporaryTrackingData();
            this.VeryOldEval = TracX.NonAssignedTracks();
        end
        
        function this = clearTrackerData( this, neighbourRadius )
        % CLEARTRACKERDATA Clears all tracker data from previous
        % run and initialize tracking fields and neighbourhood for given
        % segmentation data.
        %
        % Args:
        %    this (:obj:`object`):               :class:`TrackerData` instance.
        %    neighbourhoodSearchRadius:  [Int]   Radius in pixel within an other
        %                                        point defined by the coordinate
        %                                        arrays x and y is considered to
        %                                        be a neighbour of (x_i,y_i).
        %                                        :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
        %
        % Returns
        % -------
        %    this :obj:`object`
        %                                       :class:`TrackerData` instance
        %                                       with tracking results
        %                                       deleted.
            
            allFieldNames = fieldnames(this.SegmentationData);
            namesToKeep = {'uuid', 'cell_frame', 'cell_index', 'cell_center_x', ...
                'cell_center_y', 'cell_center_z', 'cell_majoraxis', 'cell_minoraxis', ...
                'cell_orientation', 'cell_area', 'cell_volume', 'cell_perimeter', ...
                'cell_eccentricity', 'cell_fractionOfGoodMembranePixels', ...
                'cell_mem_area', 'cell_mem_volume', 'cell_nuc_radius', ...
                'cell_nuc_area', 'cell_close_neighbour'};
            cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));
            
            for iFieldName = allFieldNames'
                cellIdx = cellfun(cellfind(iFieldName), namesToKeep);
                if all(cellIdx == 0)
                    this.SegmentationData.(iFieldName{1}) = [];
                end               
            end
            
            this.SegmentationData.cell_close_neighbour = struct();
            this = initializeCellCloseNeighbours(this, neighbourRadius);
            this = initializeTrackingFields(this);
        end
        
        
        [ sortIdx ] = getSortIdxForTrackIndices( this, varargin )
        % GETSORTIDXFORTRACKINDICES Returns an array of indices to
        % sort track_index on each frame in ascending (default) or
        % descending order.
        
        % [ closeNeighboursIdx ] = getCloseCellNeighbourIdx(this, ...
        %     xCoordinateArray, yCoordinateArray, neighbourhoodSearchRadius)
        % GETCLOSECELLNEIGHBOURIDX Returns a square logical matrix
        % of size length(x) where as 1 corresponds to a neighbour within
        % the neighbourhoodSearchRadius.
        
        [ oldFrameSubset, newFrameSubset ] = getCommonTrackDataSubset(this, ...
            oldFrame, newFrame, varargin )
        % GETCOMMONTRACKDATASUBSET Subsets oldFrame and newFrame data
        % struct for common track indicies and returns the position for both
        % frames sorted.
        
        [ startFrameArray, endFrameArray ] = getStartEndFrameOfTrack(this, ...
            cellFrameArray, trackIndexArray)
        % GETSTARTENDFRAMEOFTRACK Determines for each cell track
        % with a track index its first and last occuring image frame.
        
        [ trackAssignmentReconsiderationArray ] = ...
            getTrackAssignmentReconsiderationArray(this, ...
            trackIndexArray, trackFingerprintDistanceArray, ...
			trackAssignmentFractionArray, ...
            fingerprintDistThreshold)
        % GETTRACKASSIGNMENTRECONSIDERATIONARRAY Returns an array with
        % track with track indicies where the track assignment has to be
        % re-considered as the fingerprint distance is above the threshold
        % given.
        
        [ uuidsOutsideMask, outsideMaskIdx ] = getUUIDOutsideROI(this, ...
            img, roi )
        % GETUUIDOUTSIDEROI Returns data uuids for cells on an image
        % outside a region of interest (roi). Roi can be either an offset
        % value from the image border or an impoly object.

        [ ] = deleteData( this, dataIDToDelete, neighbourhoodSearchRadius)
        % DELETEDATA Deletes data elements (segmented objects).
        % I.e cells asthey are outside of a  ROI, to close to the image
        % border or should be excluded due togating.
        
        [ ] = mapCrop2RawCoordinates(this, imageCropCoordinateArray )
        % MAPCROP2RAWCOORDINATES Maps ProjectConfiguration.imageCropCoordinateArray
        % coordinates to original raw image coordinates.
        
        [ ] = delByFeature(this, feature, threshold, neighbourhoodSearchRadius)
        % DELBYFEATURE Delete cell segmentations exceeding the feature threshold
        % value from tracking.
        
        [ ] = delSegArtefacts(this, neighbourhoodSearchRadius, varargin)
        % DELSEGARTEFACTS Deletes potential segmentation artefacts (segmented
        % objects which are no cells based the autofluorescence.
        
        [ uuidToDel, trToDelFrame, ciToDel, segArtefactsL ] = ...
            getPotSegArtefacts(this, varargin)
        % GETPOTSEGARTEFACTS Determines potential segmentation artefacts based on a
        % fluorescent channel and returns the uuid, track and cell indices to
        % delete.
        
        [ ] = insertMergedData(this, data, tracks2Merge)
        % INSERTMERGEDDATA Inserts the data after merging the masks of the bud with
        % its assigned mother until detected division and re segmentation and
        % quantification into TrackerData.
        
        [ colormap ] = getFieldArrayColorMap(this, property, currTrack)
        % GETFIELDARRAYCOLORMAP Returns a colormap for the properties / field arrays.
        % Special colormaps are returned for 'track_index', 'track_parent'. If
        % current_track is given, all other tracks will be colored in grey.
        
        importAlphaShapes(this, resultDir)
        % IMPORTALPHASHAPES Function to import alpha shapes from cells*.mat
        % files from the segmentation results directory.
        
        [ currAlphaShapes ] = importAlphaShapesForFrame(this, frame, ...
            resultDir, ReducedShapes)
        % IMPORTALPHASHAPESFORFRAME Function to import alpha shapes from cells*.mat
        % files for a specific frame.
        
        decompressOrientationData(this)
        % DECOMPRESSORIENTATIONDATA Decompresses the orientation vector into 
        % from a single number.
        
        
        
    end
    
    methods(Static)

        [ closeNeighboursIdx ] = getCloseCellNeighbourIdx(...
            xCoordinateArray, yCoordinateArray, neighbourhoodSearchRadius, ...
            varargin)
        % GETCLOSECELLNEIGHBOURIDX Returns a square logical matrix
        % of size length(x) where as 1 corresponds to a neighbour within
        % the neighbourhoodSearchRadius.
        
        [joinedNeighbours, joinedNeighboursWMotion, oldNeighbours, newNeighbours] = ...
            getConsecutiveCloseCellNeighbourIdx(xO, xOF, yO, yOF, trO, xN, yN, trN, ...
            neighbourhoodSearchRadius)
        % GETCONSECUTIVECLOSECELLNEIGHBOURIDX Returns the neighbourhood indices for
        % consecutive image frames. It returns the joined neighbourhood for
        % segmentation objects existing on both image frames, for segmentation
        % objects only on the first and second frame respectively.
    end
end