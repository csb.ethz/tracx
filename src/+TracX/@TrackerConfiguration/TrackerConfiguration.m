%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TrackerConfiguration < handle
% TRACKERCONFIGRURATION Creates an object to store and manipulate
% metadata used and defining a Tracker project. The metadata itself is
% stored in :class:`+TracX.@ParameterConfiguration` and :class:`+TracX.@ProjectConfiguration`.
%
% :Authors:
%    Andreas P. Cuny - initial implementation
    properties

        ParameterConfiguration % :class:`+TracX.@ParameterConfiguration` instance stores all the parameters needed for tracking.
        ProjectConfiguration % :class:`+TracX.@ProjectConfiguration` instance stores all information needed for a tracking project.
        io % :class:`+TracX.@IO` instance implementing read/write methods
        
    end
    
    methods
        
        % Constructor
        function obj = TrackerConfiguration()                 
        % TrackerConfiguration Constructs a :class:`TrackerConfiguration` 
        % object to manipulate and store tracking :class:`+TracX.@ParameterConfiguration`
        % and :class:`+TracX.@ProjectConfiguration` data.
        %
        % Args:
        %    -
        % Returns
        % -------
        %    this :obj:`object`:      
        %                                    Returns a :class:`TrackerConfiguration` instance.
        %                                    object instance with initialized
        %                                    :class:`+TracX.@ParameterConfiguration` (default
        %                                    values) and :class:`+TracX.@ProjectConfiguration`
        %                                    (empty) objects
        %
        % :Authors:
        %    Andreas P. Cuny - initial implementation
            obj.ParameterConfiguration = TracX.ParameterConfiguration();
            obj.ProjectConfiguration = TracX.ProjectConfiguration();
            obj.io = TracX.IO(TracX.Utils());  
        end
              
        function this = importTrackerConfiguration(this, file)
		% IMPORTTRACKERCONFIGURATION Reads a TracX configuration xml 
        % file and stores the parameters in the ParameterConfiguration 
        % object.
		%
        % Args:
        %    this (:obj:`object`):           :class:`TrackerConfiguration` instance.
        %    file (:obj:`str`):              Path to parameter xml file
        %
        % Returns
        % -------
        %    this: :obj:`object`
        %                                    Acts on :class:`TrackerConfiguration` object
        %                                    directly and returns TracX configured.
        %
            this = this.io.readFromXML(this, file);
        end
        
        function exportTrackerConfiguration(this, file)
		% EXPORTTRACKERCONFIGURATION Writes a TracX configuration 
        % xml file with all the parameters stored in the 
        % ParameterConfiguration and ProjectConfiguration object.
        %
        % Args:
        %    this (:obj:`object`):           :class:`TrackerConfiguration` instance.
        %    file (:obj:`str`):              Path to TracX configuration xml
        %                                    file and its name.
        %
        % Returns
        % -------
        %    void (:obj:`-`)                
        %                                    Writes to disk directly.
        %
            this.io.writeToXML(this, file);
        end
    end
end