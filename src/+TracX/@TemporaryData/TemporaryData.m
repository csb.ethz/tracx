%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TemporaryData
    % TEMPORARYDATA acts as temprorary data container for :class:`~+TracX.@TemporaryTrackingData.TemporaryTrackingData`. 
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
        
        OldFrame
        % :class:`TemporaryTrackingData` object holding the current image frame data.
        NewFrame
        % :class:`TemporaryTrackingData` object holding the subsequent image frame data.
        VeryOld
        % :class:`TemporaryTrackingData` object holding the previous image frame data of non assigned tracks.
    end
    
    methods
        
        % Constructor
        function obj = TemporaryData()
            % TEMPORARYDATA Constructs a :class:`TemporaryData` object instance
            % to keep teporary frame data during tracking.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                           Returns :class:`TemporaryData` instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            
            obj.OldFrame = TracX.TemporaryTrackingData();
            obj.NewFrame = TracX.TemporaryTrackingData();
            obj.VeryOld = TracX.TemporaryTrackingData();
        end
        
        function this = initializeOldFrame(this)
            % INITIALIZEOLDFRAME Initailizes the OldFrame property of the
            % Tracker with data from the :class:`TemporaryData` object for the first
            % image frame (frameNumber == 1).
            %
            % Args:
            %    this (:obj:`object`):  :class:`TemporaryData` instance
            %
            % Returns
            % -------
            %    this: :obj:`object`
            %                           Initializes the :class:`TemporaryData.OldFrame`
            %                           property and returns the :class:`TemporaryData`
            %                           instance parametrized.
            
            ofl = length(this.Data.getFieldArrayForFrame('cell_frame', 1 ));
            
            this.OldFrame.addData((1:ofl).', (1:ofl).', ones(ofl,1),...
                this.Data.getFieldArrayForFrame('cell_index', 1), ...
                this.Data.getFieldArrayForFrame('cell_center_x', 1), ...
                this.Data.getFieldArrayForFrame('cell_center_y', 1), ...
                zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), ...
                this.Data.getFieldArrayForFrame('cell_orientation', 1), ...
                this.Data.getFieldArrayForFrame('cell_area', 1), ...
                this.Data.SegmentationData.cell_close_neighbour(1).Indicies, ...
                zeros(ofl,1),...
                this.Data.getFieldArrayForFrame('track_fingerprint_real', 1),...
                this.Data.getFieldArrayForFrame('track_fingerprint_real_distance', 1),...
                this.Data.getFieldArrayForFrame('track_age', 1));
        end
    end
    
end
