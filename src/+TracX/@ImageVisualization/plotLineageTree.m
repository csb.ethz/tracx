%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ figureHandles ] = plotLineageTree(this, varargin)
% PLOTLINEAGETREE Plot a lineage tree for one or multiple root cells
% (i.e cells on the first frame). The tree is plotted to the console by
% default. With 'plotToFigure' set to true plots the tree to a figure.
%
% Args:
%    this (:obj:`obj`):                        :class:`ImageVisualization` object
%    varargin (:obj:`str varchar`):
%
%        * **rootIndicies** (:obj:`int`):      The lineage root; Any 'track_index' number.
%        * **divisionType** (:obj:`int`):      Cell division type. Set automatically but can be change.
%        * **plotToFigure** (:obj:`bool`):     Flag if lineage tree should be plotted to a figure. Defaults to false.
% 
% Returns
% -------
%  figureHandles (:obj:`object`)         
%                                              Prints the tree to the
%                                              command window.
%                                              Optionally the lineage tree 
%                                              is visualized in a figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation

defaultRootIndices = [];
defaultPlotToFigure = false;
if this.configuration.ProjectConfiguration.cellDivisionType == 1
    defaultDivisionType = 'asymmetrical';
else
    defaultDivisionType = 'symmetrical';
end
validRootIndices = {'>', 0, '<',max(this.data.getFieldArray('track_index'))};
validDivisionType = {'symmetrical', 'symmetric', 'Symmetrical', 'asymmetrical', ...
    'Asymmetrical', 'asymmetric', '-1', '0', '1'};

p = inputParser;
p.addRequired('this', @isobject);
p.addOptional('rootIndicies', defaultRootIndices, @(x)validateattributes(x, ...
    {'double'}, validRootIndices, 'Rankings'));
p.addParameter('divisionType', defaultDivisionType, @(x) any( ...
    validatestring(x, validDivisionType)))
p.addParameter('plotToFigure', defaultPlotToFigure, @(x)validateattributes(x, ...
    {'logical'}, {'scalar'}))
parse(p, this, varargin{:})

lineageTreeArray = this.lineage.generateLineageTreeObject('rootIndicies', ...
    p.Results.rootIndicies );

if ~isempty(p.Results.rootIndicies)
    rootsToPlot = p.Results.rootIndicies;
else
    rootsToPlot = find(~cellfun('isempty', lineageTreeArray))';
end
figureHandles = cell(numel(rootsToPlot), 1);
for iRoot = rootsToPlot
    
    if p.Results.plotToFigure
        trackNodes = cell2mat(lineageTreeArray{iRoot}.Node);
        if numel(trackNodes) == 1
            continue
        else
            h = figure('Visible', 'off');
            if any(strcmp(string(p.Results.divisionType), ...
                    {'asym', 'asymmetrical', 'asymmetric', 'Asymmetrical', '1'}))
                
                duration = TracX.ExternalDependencies.tree.tree(lineageTreeArray{iRoot}, 'clear');
                
                startFrameArray = this.data.getFieldArray('track_start_frame');
                trackIndexArray = this.data.getFieldArray('track_index');
                for i = 1:numel(trackNodes)
                    duration = duration.set(i, (5*unique(startFrameArray(...
                        trackIndexArray == trackNodes(i))) - unique(startFrameArray(...
                        trackIndexArray == iRoot))  )/60);
                end
                sh = subplot(1,1,1);
                lineageTreeArray{iRoot}.plot(duration, 'SymDiv', false, 'Parent', sh);
                sh.YDir = 'Reverse';
                sh.XGrid = 'on';
                figureHandles{iRoot} = h;
            else
                sh = subplot(1,1,1);
                lineageTreeArray{iRoot}.plot([], 'Parent', sh);
                sh.YDir = 'Reverse';
                figureHandles{iRoot} = h;
            end
        end
    else
        disp(lineageTreeArray{iRoot}.tostring);
    end
    
end

end