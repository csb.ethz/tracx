%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = plotFingerprintEvalSummary(this, pathToResults )
% PLOTFINGERPRINTEVALSUMMARY Plots fingerprint evaluation results as contour
% plots for varied parameters for resizing, freq bins and windowing.
% Additionally a plot visualizing the window on the real data is displayed.
%
% Args:
%    this (:obj:`obj`):                 :class:`ImageVisualization` object
%    pathToResults (:obj:`str`):        Full path to fingerprintEvaluation 
%                                       result file.
% 
% Returns
% -------
%    void (:obj:`-`)                              
%                                       Displays figures.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Import data
data = importdata(pathToResults);
resizingFactors = unique(data.data.Sheet1(:,6));

figure
for iResizeFactor = 1:numel(resizingFactors)
    idx = data.data.Sheet1(:,6) == resizingFactors(iResizeFactor);
    x = data.data.Sheet1(idx,4);
    y = data.data.Sheet1(idx,5);
    z = data.data.Sheet1(idx,8);
    % construct the interpolant function
    F = TriScatteredInterp(x,y,z);
    
    t = 0:1:60; % sample uniformly the surface for matrices (qx, qy, qz)
    [qx, qy] = meshgrid(t, t);
    qz = F(qx, qy);
    subplot(ceil(numel(resizingFactors)/3),3,2+iResizeFactor)
    [~, c] = contour(qx, qy, qz, 'ShowText','on');
    c.LineWidth = 3;
    xlim([14,61])
    ylim([3,31])
    hold on
    plot(x,y,'x', 'Color', [0.35, 0.35, 0.35]); hold off
    title(sprintf('Resizing factor %d', resizingFactors(iResizeFactor)))
    xlabel('windowVariation')
    ylabel('freqVariation')
    
end

figure;
windows = unique(data.data.Sheet1(:,4));
for iWindow = 1:numel(windows)
    subplot(ceil(numel(resizingFactors)/3),3,iWindow)
    imshow(this.imageProcessing.generateFingerprintControlImageFrame(10,...
        windows(iWindow), 5));
    title(sprintf('Window size %d', windows(iWindow)))
    
end

for iResizeFactor = 1:numel(resizingFactors)
    idx = data.data.Sheet1(:,6) == resizingFactors(iResizeFactor);
    x = data.data.Sheet1(idx,4);
    y = data.data.Sheet1(idx,5);
    z = data.data.Sheet1(idx,8);
    % construct the interpolant function
    F = TriScatteredInterp(x,y,z);
    
    t = 0:1:60; % sample uniformly the surface for matrices (qx, qy, qz)
    [qx, qy] = meshgrid(t, t);
    qz = F(qx, qy);
    figH = figure;
    [~, c] = contour(qx, qy, qz, 'ShowText','on', 'LineWidth',0.5);
    c.LineWidth = 2;
    c.LabelSpacing = 20;
    xlim([14,61])
    ylim([3,31])
    hold on
    plot(x,y,'x', 'Color', [0.35, 0.35, 0.35], 'MarkerSize', 2); hold off
    title(sprintf('Resizing factor %d', resizingFactors(iResizeFactor)))
    xlabel('windowVariation')
    ylabel('freqVariation')
    figH.PaperOrientation = 'landscape';
    figH.PaperUnits ='centimeters';
    figH.PaperPosition =[0 0 4.8 3.8];
     print(figH, fullfile(this.configuration.ProjectConfiguration.segmentationResultDir, ...
         'Evaluations', sprintf('%s_resizing_%d.pdf',...
         this.configuration.ProjectConfiguration.projectName, ...
         resizingFactors(iResizeFactor))), '-dpdf')
end

end
