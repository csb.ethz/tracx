%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ fig ] = plotSummaryTracksAboveFPThreshold(this, varargin )
% PLOTSUMMARYTRACKSABOVEFPTHRESHOLD Creates a summary plot track ids vs
% image frames with tracks and image frames above the fingerprint threshold
% marked. Gives an overview which linkages have to be inspected manually.
%
% Args:
%    this (:obj:`obj`):          :class:`ImageVisualization` object
%    varargin (:obj:`str varchar`):
%
%        * **threshold** (:obj:`str`):     CRF fraction threshold. Defaults to 0.
%
% Returns
% -------
%  void (:obj:`-`)         
%                                          Image is visualized in a figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
defaultThreshold = 0;
validThreshold = @(x) isnumeric(x) || x>=0 && x<=1;
addRequired(p, 'this', @isobject);
addOptional(p, 'threshold', defaultThreshold, validThreshold);
p.KeepUnmatched=true;
parse(p, this, varargin{:});

trackArray = unique(this.data.getFieldArray('track_index'));
trackArrayS = sort(trackArray, 'ascend')';
frameArray = unique(this.data.getFieldArray('cell_frame'));
data = nan(numel(frameArray), numel(trackArray));

for k = 1:numel(trackArrayS)
    currFra = this.data.getFieldArrayForTrackIndex('track_assignment_fraction', ...
        trackArrayS(k));
    currFrame = this.data.getFieldArrayForTrackIndex('cell_frame', ...
        trackArrayS(k));
    data(ismember(frameArray, currFrame), k) = currFra;
end
%%

[rAboveT, cAboveT] = find(data > p.Results.threshold == 1);
[rBelowT, cBelowT] = find(data == p.Results.threshold == 1);

%
dataAboveThreshold = [rAboveT, cAboveT];
dataBelowThreshold = [rBelowT, cBelowT];
% find limits
minx = min([dataAboveThreshold(:, 1); dataBelowThreshold(:, 1)]);
maxx = max([dataAboveThreshold(:, 1); dataBelowThreshold(:, 1)]);
miny = min([dataAboveThreshold(:, 2); dataBelowThreshold(:, 2)]);
maxy = max([dataAboveThreshold(:, 2); dataBelowThreshold(:, 2)]);

[N2, ~] = histc(dataBelowThreshold(:, 1),unique(dataBelowThreshold(:, 1)));
[N, ~] = histc(dataAboveThreshold(:, 1),unique(dataAboveThreshold(:, 1)));
[N3, ~] = histc(dataAboveThreshold(:, 2),unique(dataAboveThreshold(:, 2)));
[N4, ~] = histc(dataBelowThreshold(:, 2),unique(dataBelowThreshold(:, 2)));
t1 = zeros(maxx, 1);
t1(unique(dataAboveThreshold(:, 1))) = N;
t2 = zeros(maxx, 1);
t2(unique(dataBelowThreshold(:, 1))) = N2;
t3 = zeros(maxy, 1);
t3(unique(dataAboveThreshold(:, 2))) = N3;
t4 = zeros(maxy, 1);
t4(unique(dataBelowThreshold(:, 2))) = N4;


% make figure
fig =figure('PaperSize', [17 8.5]);
set(gcf,'color','w');
clf

ah1 = subplot(2, 2,  4);
bh2 = bar([t3,t4], 'stacked', 'EdgeColor','none');
set(bh2,{'FaceColor'},{[150/255, 100/255, 100/255]; [34/255, 152/255, 140/255]});
set(gca, 'XDir','reverse')
xlim([miny, maxy])
ylabel('Counts')
ah2 = subplot(2, 2, 1);
bh = bar([t1,t2], 'stacked', 'EdgeColor','none');
set(bh,{'FaceColor'},{[150/255, 100/255, 100/255]; [34/255, 152/255, 140/255]});
ylabel('Counts')

ah3 = subplot(2, 2, 3);
hold on
idLUTGT = data;
% %---- Style 1 Start ----
for k = 1:size(idLUTGT, 2)
    if ~isempty([min(find(~isnan(idLUTGT(:,k)))), max(find(~isnan(idLUTGT(:,k))))])
        if min(find(~isnan(idLUTGT(:,k))))-1 == max(find(~isnan(idLUTGT(:,k))))
            plot(min(find(~isnan(idLUTGT(:,k)))), ...
                max(find(~isnan(idLUTGT(:,k)))), 'o',...
                'MarkerFaceColor', [34/255, 152/255, 140/255], ...
                'MarkerEdgeColor', [34/255, 152/255, 140/255])
            hold on
        else
            line([min(find(~isnan(idLUTGT(:,k))))-1, ...
                max(find(~isnan(idLUTGT(:,k))))], [k,k], ...
                'Color', [34/255, 152/255, 140/255])
            hold on
        end
    end
end
plot(dataAboveThreshold(:, 1), dataAboveThreshold(:, 2), 'o', 'MarkerFaceColor', ...
    [150/255, 100/255, 100/255], ...
    'MarkerEdgeColor', [150/255, 100/255, 100/255], 'MarkerSize', 1.5)
%---- Style 2 End -
ylabel('Track indicies (-)')
xlabel('Frame number (-)')
linkaxes([ah3, ah2], 'x')
ah3.XLim = [minx, maxx+0.3];
ah3.YLim = [miny, maxy];
ah1.Box = 'off';
ah1.View = [90, 90];
ah1.XAxis.Visible = 'off';
ah1.Visible = 'on';
ah2.Visible = 'on';
ah2.Box = 'off';
ah2.View = [0, 90];
ah2.XAxis.Visible = 'off';
hFig    = gcf;
hAxes   = findobj(allchild(hFig), 'flat', 'Type', 'axes');
axesPos = get(hAxes, 'Position');
set(hAxes(3), 'Position', [0.7128, 0.0718, 0.2533, 0.5821]);
set(hAxes(1), 'Position', [0.0542, 0.0687, 0.6398, 0.5845]);
set(hAxes(2), 'Position', [0.0542, 0.7, 0.6398, 0.29]);

end
