%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef ImageVisualization
    % IMAGEVISUALIZATION Class handles all the image visualization
    % aspects of TracX. i.e. control images displayed to the user.
    %
    % :Authors:
    % 	Andreas P. Cuny - initial implementation
    
    properties
        
        data % :class:`+TracX.@TrackerData` instance stores all segmentation, quantification and lineage data.

        configuration % :class:`+TracX.@TrackerConfiguration` instance implementing configuration methods.

        imageProcessing % :class:`+TracX.@ImageProcessing` instance implementing image processing methods.

        lineage % :class:`+TracX.@Lineage` instance implementing lineage methods.
                
    end
    
    methods
        
        % Constructor
        function obj = ImageVisualization(data, configuration, ...
                imageProcessing, lineage)
        % Constructor of an :class:`ImageVisualization` object. Class handles 
        % all the image visualization aspects of TracX. i.e. control images 
        % displayed to the user.
        %
        % Args:
        %    data (:obj:`obj`):            :class:`TrackerData` instance
        %    configuration (:obj:`obj`):   :class:`TrackerConfiguration` instance
        %    imageProcessing (:obj:`obj`): :class:`ImageProcessing` instance
        %    lineage (:obj:`obj`):         :class:`Lineage` instance
        %
        % Returns
        % -------
        %  obj (:obj:`obj`)
        %                                :class:`ImageVisualization`
        %                                instance
        %
        % :Authors:
        % 	Andreas P. Cuny - initial implementation
            obj.data = data;
            obj.configuration = configuration;
            obj.imageProcessing = imageProcessing;
            obj.lineage = lineage;
        end
        
        % PLOTFINGERPRINTCONTROLIMAGEFRAME Displays a control
        % image where all cell tracks found on an image frame are labeled
        % with their track indicies overlayed onto a (Brightfield) image.
        % Additionally the fingerprint windows are displayed.
        [  ] = plotFingerprintControlImageFrame( this, fontSize, ...
            rectHalfSideLength, frameNumber)
        
        % PLOTPOLEAGECONTROLIMAGEFRAME Displays a control
        % image where for all cell tracks found on an image frame the cell
        % poles are labeled with their pole age overlayed onto a
        % (Brightfield) image.
        [ rgb ] = plotPoleAgeControlImageFrame( this, fontSize, ...
            frameNumber)
        
        % PLOTTRACKCONTROLIMAGEFRAME Displays a control
        % image where all cell tracks found on an image frame are labeled
        % with their track indicies overlayed onto a (Brightfield) image.
        [  ] = plotTrackControlImageFrame( this, fontSize, frameNumber, ...
            varargin)
        
        % PLOTLINEAGETREE Plot a lineage tree for one or multiple 
        % root cells (i.e cells on the first frame). The tree is plotted 
        % to the console by default. With 'plotToFigure' set to true plots
        % the tree to a figure.
        [figureHandle] = plotLineageTree(this, divisionType, varargin)
        
        % PLOTFPVSDELTADISTPLOT Displays a scatter plot figure with
        % fingerprint distance vs. delta distance plotted. Additional 
        % histograms show the distributions of the main cathegory along 
        % the respective axes.
        [ fh ] = plotFPVsDeltaDistPlot( ~, ...
            xFPDistAssigned, xFPDistAssignedToCompare, yDeltaDistAssigned, ...
            yDeltaDistToCompare, fpDistThreshold, lowerQuantile, varargin)
        
        % PLOTFINGERPRINTEVALSUMMARY Plots fingerprint evaluation 
        % results as contour plots for varied parameters for resizing, freq 
        % bins and windowing. Additionally a plot visualizing the window on
        % the real data is displayed.
        [ ] = plotFingerprintEvalSummary(this, pathToResults )
        
        % PLOTSUMMARYTRACKSABOVEFPTHRESHOLD Creates a summary plot 
        % track ids vs image frames with tracks and image frames above the
        % fingerprint threshold marked. Gives an overview which linkages 
        % have to be inspected manually.
        [ fig ] = plotSummaryTracksAboveFPThreshold(this, varargin)
        
        % PLOTANNOTATEDCONTROLIMAGEFRAME Displays a control
        % image where all data found on an image frame for a given property
        % are overlayed onto a (Brightfield) image at their cell position. Default
        % plotted property is 'track_index' and the image cropped to the segmented
        % window.
        [  ] = plotAnnotatedControlImageFrame( this, frameNumber, ...
            varargin)
        
        % PLOTDIFFERENCEALPHASHAPEVSREDUCEDALPHASHAPE Displays a control
        % image for the differences in alpha shapes vs reduced alpha shapes.
        plotDifferenceAlphaShapeVsReducedAlphaShape(this, cellIdx)
        
        % VISUALISE3DCELLS Function to visualize 3D cells / type of data. Optional
        % inputs alow for customization regarding track to display, colormap, image
        % limits, if only reduced alpha shapes should be used, if an image sould be
        % returned, if a movie should be generated, what the pixel per micron
        % factor is and if the image should be rotated.
        returnValue = visualise3DCells(this, varargin)
    end
    
end
