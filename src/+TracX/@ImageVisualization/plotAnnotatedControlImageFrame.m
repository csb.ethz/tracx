%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = plotAnnotatedControlImageFrame( this, frameNumber, ...
    varargin)
% PLOTANNOTATEDCONTROLIMAGEFRAME Displays a control
% image where all data found on an image frame for a given property
% are overlayed onto a (Brightfield) image at their cell position. Default 
% plotted property is 'track_index' and the image cropped to the segmented
% window.
%
% Args:
%    this (:obj:`obj`):                        :class:`ImageVisualization` object
%    frameNumber (:obj:`int`):                 Image frame number.
%    varargin (:obj:`str varchar`):
%
%        * **fieldName** (:obj:`str`):              Property to be displayed on the image. Defaults to 'track_index' can be any propery of :obj:`+TracX.SegmentationData`.
%        * **highlightFieldNameVal** (:obj:`int`):  Property value that should be highlighted. 
%        * **fontSize** (:obj:`str`):               Font size of property to be displayed.
%        * **removeContainedTracks** (:obj:`bool`): Flag, if contained tracks within tracks should be removed. Defaults to true.
%        * **figure** (:obj:`object`):              Figure handle of existing figure to plot result in.
% 
% Returns
% -------
%  void (:obj:`-`)         
%                                                   Image is visualized in figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation


p = inputParser;
defaultIsSegmentationCrop = false;
defaultFontSize = 14;
defaultFieldName = 'track_index';
defaultRemoveContainedTracks = true;
defaultHighlightFiledNameVal = [];
defaultFigureHandle = [];
validFontSize = @(x) isnumeric(x) || x>4;
validFieldNames = fieldnames(this.data.SegmentationData);
addRequired(p, 'this', @isobject);
addRequired(p, 'frameNumber', @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame);
addOptional(p, 'fieldName', defaultFieldName, ...
    @(x) any(validatestring(x, validFieldNames)));
addOptional(p, 'highlightFieldNameVal', defaultHighlightFiledNameVal, ...
    @(x) all(isnumeric(x)) && all(x>=1));
addOptional(p, 'fontSize', defaultFontSize, validFontSize);
addOptional(p, 'isSegmentationCrop', defaultIsSegmentationCrop, @islogical);
addOptional(p, 'removeContainedTracks', defaultRemoveContainedTracks, @islogical);
addOptional(p, 'figure', defaultFigureHandle, @(x) isgraphics(x, 'figure') ...
    || isgraphics(x, 'axes'));
p.KeepUnmatched=true;
parse(p, this, frameNumber, varargin{:});

labeledImg = this.imageProcessing.generateAnnotatedControlImageFrame(...
    frameNumber, varargin{:});

if p.Results.isSegmentationCrop == true
    labeledImg = TracX.Utils.imcropC(labeledImg, this.configuration. ...
        ProjectConfiguration.imageCropCoordinatePriorReversionArray);
end

if isempty(p.Results.figure)
    fig = figure;
    imshow(labeledImg, 'Border', 'tight', 'InitialMagnification', 'fit');
    truesize(fig);
    set(fig,'visible', 'on');
else
    fig = p.Results.figure;
    imshow(labeledImg, 'Border', 'tight', 'InitialMagnification', 'fit', 'Parent', fig);
end
end