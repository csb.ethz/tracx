%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = plotTrackControlImageFrame( this, frameNumber, ...
    varargin)
% PLOTTRACKCONTROLIMAGEFRAME Displays a control
% image where all cell tracks found on an image frame are labeled
% with their track indicies overlayed onto a (Brightfield) image.
%
% Args:
%    frameNumber (:obj:`int`):                     Image frame number.
%    varargin (:obj:`str varchar`):
%
%        * **fieldName** (:obj:`str`):              Property to be displayed on the image. Defaults to 'track_index' can be any propery of :obj:`+TracX.SegmentationData`.
%        * **highlightFieldNameVal** (:obj:`int`):  Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **fontSize** (:obj:`str`):               Font size of property to be displayed.
%        * **removeContainedTracks** (:obj:`bool`): If contained tracks within tracks should be removed. Defaults to true.
%        * **isMarker** (:obj:`bool`):              If markers should be displayed. Defaults to false.
%
% Returns
% -------
%  void (:obj:`-`)         
%                                                   Image is visualized in a figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation

labeledImg = this.imageProcessing.generateAnnotatedControlImageFrame(...
    frameNumber, varargin{:});

fig = figure;
imshow(labeledImg, 'Border', 'tight', 'InitialMagnification', 'fit');
truesize;
set(fig,'visible', 'on');

end