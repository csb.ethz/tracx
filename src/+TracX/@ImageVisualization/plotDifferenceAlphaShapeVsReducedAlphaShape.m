%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function plotDifferenceAlphaShapeVsReducedAlphaShape(this, cellIdxArray)
% PLOTDIFFERENCEALPHASHAPEVSREDUCEDALPHASHAPE Displays a control
% image for the differences in alpha shapes vs reduced alpha shapes.
%
% Args:
%    this (:obj:`obj`):                        :class:`ImageVisualization` instance.
%    cellIdxArray (:obj:`array`):              Array of cell indices to plot.
% 
% Returns
% -------
%  void (:obj:`-`)         
%                                                   Image is visualized in figure.
% :Authors:
% 	Tomas Kuendig - initial implementation

tiledlayout('flow');
for i = cellIdxArray
    h = nexttile; hold on
    plot(this.data.SpatialData.cell_alphashape{i},'FaceAlpha', 0)
    patch(this.data.SpatialData.cell_alphashape_red{i},'FaceAlpha',0, 'EdgeColor', 'red', 'LineWidth', 1)
    h.View = [16.6500   41.9018];
    h.DataAspectRatio = [1 1 1];
    h.Title.String = i;
end
end