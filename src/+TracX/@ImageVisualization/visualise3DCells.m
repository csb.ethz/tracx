%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function  returnValue = visualise3DCells(this, varargin)
% VISUALISE3DCELLS Function to visualize 3D cells / type of data. Optional
% inputs alow for customization regarding track to display, colormap, image
% limits, if only reduced alpha shapes should be used, if an image sould be
% returned, if a movie should be generated, what the pixel per micron
% factor is and if the image should be rotated.
%
% Args:
%    ignoredArg (:obj:`obj`):          :class:`ImageVisualization` object
%    varargin (:obj:`str varchar`):
%
%        * **PropertyToVisualise** (:obj:`str`):    Property to be displayed on the image. Defaults to 'track_index' can be any propery of :obj:`+TracX.SegmentationData`.
%        * **Track** (:obj:`int`):                  Track number which should be highlighed.
%        * **Frames** (:obj:`array`):               Image frames array
%        * **ColorMap** (:obj:`array` kx3):         Colormap to be used
%        * **DelayTime** (:obj:`float` 1x1):        Deleay time. Defaults to 0.3.
%        * **NameOnly** (:obj:`bool` 1x1):          Flag if name only. Defaults to false.
%        * **FileName** (:obj:`str` 1x1):           Filename of output file.
%        * **XLim** (:obj:`array`, 1x2):            Plot x limits.
%        * **YLim** (:obj:`array`, 1x2):            Plot y limits.
%        * **ZLim** (:obj:`array`, 1x2):            Plot z limits.
%        * **ReducedShapes** (:obj:`bool`):         Flag; if redunced alpha shapes should be used. Defaults to false.
%        * **ReturnImage** (:obj:`bool`):           Flag; if image should be returned. Defaults to false.
%        * **NumbersHidden** (:obj:`bool`):         Flag, if numbers should be shown. Defaults to false.
%        * **OutputType** (:obj:`str`):             Type of output. Defaults to 'Movie'.
%        * **PixelsPerMicrometer** (:obj:`float`):  Pixels per micrometer. Defaults to 0.
%        * **Rotation** (:obj:`bool`):              Flag, if rotation should be applied. Defaults to 0.
%        * **Colorbar** (:obj:`bool`):              Flag, if colobar should be shown. Defaults to false.
%
% Returns
% -------
%  returnValue (:obj:`array`)
%                                       Image is visualized in a figure.
% :Authors:
% 	Thomas Kuending - initial implementation

p = inputParser;
p.KeepUnmatched = true;
p.addParameter('PropertyToVisualise', 'track_index',@ischar);
p.addParameter('Track', 0);
p.addParameter('Frames', []);
p.addParameter('ColorMap', []);
p.addParameter('DelayTime',0.3);
p.addParameter('NameOnly',false);
p.addParameter('FileName',[]);
p.addParameter('View',[-10,36]);
p.addParameter('XLim', [0 this.configuration.ProjectConfiguration.imageWidth]);
p.addParameter('YLim', [0 this.configuration.ProjectConfiguration.imageHeight]);
p.addParameter('ZLim', [0 this.configuration.ProjectConfiguration.imageDepth * ...
    this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval]);
p.addParameter('ReducedShapes', 0);
p.addParameter('ReturnImage', 0);
p.addParameter('NumbersHidden', 0);
p.addParameter('OutputType','Movie');
p.addParameter('PixelsPerMicrometer',0);
p.addParameter('Rotation',0);
p.addParameter('Colorbar',0);
p.parse(varargin{:});

PropertyToVisualise = p.Results.PropertyToVisualise;
Track = p.Results.Track;
Frames = p.Results.Frames;
ColorMap = p.Results.ColorMap;
DelayTime = p.Results.DelayTime;
NameOnly = p.Results.NameOnly;
FileName = p.Results.FileName;
View = p.Results.View;
OutputType = p.Results.OutputType;
xdim = p.Results.XLim;
ydim = p.Results.YLim;
zdim = p.Results.ZLim;
ReducedShapes = p.Results.ReducedShapes;
ReturnImage = p.Results.ReturnImage;
NumbersHidden = p.Results.NumbersHidden;
PixelsPerMicrometer = p.Results.PixelsPerMicrometer;
Rotation = p.Results.Rotation;
Colorbar = p.Results.Colorbar;

resultdir = this.configuration.ProjectConfiguration.trackerResultsDir;
track_indices = this.data.SegmentationData.track_index;
cell_frames = this.data.SegmentationData.cell_frame;
track_indices(isnan(track_indices)) = max(track_indices)+1;

isTrackIndex = isequal(PropertyToVisualise,'track_index');
isFrameBoundaries =  ~isempty(Frames);

returnValue = 0;


propertyValues = this.data.SegmentationData.(PropertyToVisualise);

if ~NameOnly && isempty(ColorMap)
    
    if isFrameBoundaries
        propertyValues = propertyValues(cell_frames>=varargin{1}(1) & ...
            cell_frames<=varargin{1}(2));
    end
    
    if isTrackIndex
        uniqueTracks = unique(track_indices);
        uniqueTracks = uniqueTracks(~isnan(uniqueTracks));
        c = hsv(length(uniqueTracks));
        trackmap(uniqueTracks) = 1:length(uniqueTracks);
        ColorMap = c(trackmap(track_indices),:);
        if isequal(Colorbar,1)
            Colorbar = {c, [min(uniqueTracks), max(uniqueTracks)]};
        end
    elseif strcmp(PropertyToVisualise,'track_parent')
        rng(uint32(str2double(mod(str2double(sprintf('%d', ...
            this.configuration.ProjectConfiguration.projectName)),9999))));
        ColorMap = this.lineage.recursiveTrackParentColoring(0, [0.5 0.5 0.5],...
            zeros(length(this.data.SegmentationData.track_index),3), ...
            this.data.SegmentationData.track_index, ...
            this.data.SegmentationData.track_parent );
        Colorbar = 0;
    else
        c = jet(64);
        values = this.data.SegmentationData.(PropertyToVisualise);
        %values(isnan(values))=0;
        LuT = linspace(min(values),max(values),64);
        ColorMap = interp1(LuT,c,values);
        ColorMap(isnan(ColorMap)) = 0;
        if isequal(Colorbar,1)
            Colorbar = {c, [min(values), max(values)]};
        end
    end
    
    if Track ~= 0
        currentTracks = ~ismember(track_indices,Track);
        ColorMap(currentTracks,1) = 0.5;
        ColorMap(currentTracks,2) = 0.5;
        ColorMap(currentTracks,3) = 0.5;
    end
end

uniqueTimepoints = unique(cell_frames);
if isFrameBoundaries
    uniqueTimepoints = uniqueTimepoints(Frames(1):Frames(2));
end

if isempty(FileName) && (strcmpi(OutputType,'GIF') || strcmpi(OutputType,'PNG') || strcmpi(OutputType,'avi'))
    if Track == 0
        trackeridentifier = "all-tracks";
    else
        trackeridentifier = ["track-" + Track];
    end
    if strcmpi(OutputType,'PNG')
        frames = ["Frame"];
    else
        frames = ["Frames" + min(uniqueTimepoints) + "-" + max(uniqueTimepoints)] ;
    end
    if isequal(View, [-10,36])
        filetext = strjoin([this.configuration.ProjectConfiguration.projectName ...
            strrep(PropertyToVisualise,'_','-') ...
            trackeridentifier date frames], '_' );
    else
        filetext = strjoin([this.configuration.ProjectConfiguration.projectName ...
            strrep(PropertyToVisualise,'_','-') ...
            trackeridentifier ["View_" + sprintf("%.0f",View(1)) + "_" + sprintf("%.0f",(View(2)))] date frames], '_' );
    end
    filetext = [filetext + "." + string(OutputType)];
    filename = fullfile(resultdir,filetext);
    
    version = 2;
    while isfile(filename)
        if isequal(View, [-10,36])
            filetext = strjoin([this.configuration.ProjectConfiguration.projectName ...
                strrep(PropertyToVisualise,'_','-') ...
                trackeridentifier date  ["v"+version] frames], '_' );
        else
            filetext = strjoin([this.configuration.ProjectConfiguration.projectName ...
                strrep(PropertyToVisualise,'_','-') ...
                trackeridentifier ["View_" + sprintf("%.0f",View(1)) + "_" + sprintf("%.0f",(View(2)))] date  ["v"+version] frames], '_' );
        end
        filetext = [filetext + "." + string(OutputType)];
        filename = fullfile(resultdir,filetext);
        version = version + 1;
    end
    returnValue = filetext;
else
    filename = fullfile(resultdir,FileName);
    filetext = FileName;
    returnValue = filetext;
end


if ~NameOnly
    
    outputImages = cell(length(uniqueTimepoints),1);
    
    assert(~isequal(Colorbar,1),['It is currently not supported to input a' , ...
        ' predetermined Colormap and use the automatic colorbar generation ' , ...
        '(colorbar = 1). Please input a complete colorbar in the format {colorscale, valuelimits}', ...
        ' or delete the colormap argument']);
    
    parfor time = uniqueTimepoints'
        %subplot(gridSize,gridSize,time); hold on
        h = figure('Visible','off');
        hold on
        axis equal
        h.Children(1).XGrid = 'on';
        h.Children(1).YGrid = 'on';
        h.Children(1).ZGrid = 'on';
        h.Color = [1 1 1];
        
        figureTitle = [strrep(PropertyToVisualise,'_',' '), ' at timepoint ', num2str(time)];
        figureTitle(1) = upper(figureTitle(1));
        title(figureTitle);
        
        if PixelsPerMicrometer
            xlabel("x [\mu\itm\rm]");
            ylabel("y [\mu\itm\rm]");
            zlabel("z [\mu\itm\rm]");
        else
            xlabel("x");
            ylabel("y");
            zlabel("z");
        end
        
        %         colormap([(0:0.01:1)'  zeros(101,1) (1:-0.01:0)']);
        %         caxis([minProp maxProp]);
        view(View+Rotation*[0.3 0]*time);
        
        current_alphashapes = this.data.importAlphaShapesForFrame(time,resultdir, ReducedShapes);
        current_cells = find(cell_frames==time)';
        current_indices = this.data.getFieldArrayForFrame('cell_index',time);
        for i = 1:length(current_cells)
            if ReducedShapes
                patch(current_alphashapes{current_indices(i)}, 'EdgeColor',  ...
                    ColorMap(current_cells(i),:), ...
                    'FaceAlpha', 0.6, 'FaceColor',ColorMap(current_cells(i),:));
            else
                plot(current_alphashapes{current_indices(i)}, 'EdgeColor',  ...
                    ColorMap(current_cells(i),:), ...
                    'FaceAlpha', 0);
            end
            if ~NumbersHidden
                text(this.data.SegmentationData.cell_center_x(current_cells(i))+5, ...
                    this.data.SegmentationData.cell_center_y(current_cells(i))-20, ...
                    this.data.SegmentationData.cell_center_z(current_cells(i)), ...
                    num2str(track_indices(current_cells(i))), 'FontSize', 6);
            end
        end
        
        xlim(xdim);
        ylim(ydim);
        zlim(zdim);
        
        if PixelsPerMicrometer
            h.Children.XTickLabel = h.Children.XTick/PixelsPerMicrometer;
            h.Children.YTickLabel = h.Children.YTick/PixelsPerMicrometer;
            h.Children.ZTickLabel = h.Children.ZTick/PixelsPerMicrometer;
        end
        
        if ~isequal(Colorbar, 0)
            c = colorbar(h.Children(1), 'southoutside');
            h.Children(2).Colormap = Colorbar{1};
            h.Children(2).CLim = Colorbar{2};
            c.FontSize = 8;
        end
        
        drawnow
        % Capture the plot as an image
        frame = getframe(h);
        im = frame2im(frame);
        close(h);
        outputImages(time,:) = {im};
        this.lineage.utils.printToConsole(sprintf('Frame %d is plotted.', time));
    end
    
    outputImages = outputImages(~cellfun('isempty', outputImages));
    
    % Write to the GIF File
    if strcmpi(OutputType, 'GIF')
        pBarT = this.lineage.utils.progressBar(length(uniqueTimepoints), ...
            'Title', 'GIF EXPORT: Progress' ...
            );
        for i = 1:size(outputImages,1)
            [imind,cm] = rgb2ind(outputImages{i},256);
            if i == 1
                imwrite(imind,cm,filename, 'gif', 'Loopcount',inf);
            else
                imwrite(imind,cm,filename, 'gif', 'WriteMode', 'append', ...
                    'DelayTime', DelayTime);
            end
            pBarT.step([],[],[]);
        end
        pBarT.release();
        returnValue = 1;
        
    elseif strcmpi(OutputType, 'Grid')
        figure;
        montage(outputImages);
        returnValue = 1;
        
    elseif strcmpi(OutputType, 'Movie')
        outputMovie = cat(4,outputImages{:});
        if ~ReturnImage
            clear outputImages;
        end
        
        % Create a figure
        hFig = figure('Name', 'TracX :: Movie Player', 'NumberTitle', ...
            'off', 'Color', 'white');
        warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
        warning('off', 'MATLAB:class:PropUsingAtSyntax');
        javaFrame=get(hFig, 'javaframe');
        javaFrame.setFigureIcon(javax.swing.ImageIcon(fullfile(which('TrackerLogo.png'))));
        
        hImg = imshow(outputMovie(:,:,:,1));
        axis tight manual;
        ax = gca;
        ax.NextPlot = 'replaceChildren';
        % Create the PlayBar object
        playBar = TracX.ExternalDependencies.PlayBar(hFig, size(outputMovie, 4));
        % Create a listener to listen for the PlayBar's UpdateEvent event
        addlistener(playBar, 'UpdateEvent', @onUpdate);
        returnValue = 1;
        
    elseif strcmpi(OutputType, 'avi')
        v = VideoWriter(filename, 'Uncompressed AVI');
        v.FrameRate = 1/DelayTime;
        open(v);
        writeVideo(v,cat(4,outputImages{:}));
        close(v);
        returnValue = 1;
        
    elseif strcmpi(OutputType, 'png')
        parfor i = 1:size(outputImages,1)
            [p,n,e] = fileparts(filename);
            currentfilename = fullfile(p,[n,'_',sprintf('%03d',uniqueTimepoints(i)),e])
            [imind,cm] = rgb2ind(outputImages{i},256);
            imwrite(imind,cm,currentfilename,'png');
        end
        returnValue = 1;
    end
    
    if ReturnImage
        returnValue = outputImages;
    end
end

    function onUpdate( hSource, hEventData )
        j = hSource.getIndex;
        currFrame = outputMovie(:,:,:, j);
        hImg.CData = currFrame;
        drawnow();
    end

end