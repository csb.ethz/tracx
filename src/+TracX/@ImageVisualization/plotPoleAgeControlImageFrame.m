%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = plotPoleAgeControlImageFrame( this, fontSize, ...
    frameNumber)
% PLOTPOLEAGECONTROLIMAGEFRAME Displays a control
% image with the cell poles for all cell tracks found on an image frame 
% labeled with their pole age overlayed onto a (Brightfield) image. 
%
% Args:
%    this (:obj:`obj`):                 :class:`ImageVisualization` object
%    fontSize (:obj:`int`):             Font size of track indicies labels.
%    frameNumber (:obj:`int`):          Image frame number.
%
% Returns
% -------
%  void (:obj:`-`)         
%                                       Image is visualized in a figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation

labeledImg = this.imageProcessing.generatePoleAgeControlImageFrame(fontSize, ...
    frameNumber);

fig = figure;
imshow(labeledImg, 'Border', 'tight', 'InitialMagnification', 'fit');
truesize;
set(fig,'visible', 'on');

end