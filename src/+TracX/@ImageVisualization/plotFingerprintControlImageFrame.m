%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = plotFingerprintControlImageFrame( this, fontSize, ...
    rectHalfSideLength, frameNumber)
% PLOTFINGERPRINTCONTROLIMAGEFRAME Displays a control
% image where all cell tracks found on an image frame are labeled
% with their track indicies overlayed onto a (Brightfield) image. 
% Additionally the fingerprint windows are displayed.
%
% Args:
%    this (:obj:`obj`):                 :class:`ImageVisualization` object
%    fontSize (:obj:`int`):             Font size of track indicies labels.
%    rectHalfSideLength (:obj:`int`):   Half side length of rectangle around
%                                       cell centroids (i.e.
%                                       :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintWindowHalfSideLength`)
%    frameNumber (:obj:`int`):          Image frame number.
%
% Returns
% -------
%  void (:obj:`-`)         
%                                       Labeled image is visualized in figure.
% :Authors:
% 	Andreas P. Cuny - initial implementation

img = this.imageProcessing.generateFingerprintControlImageFrame(fontSize, ...
    rectHalfSideLength, frameNumber);

fig = figure;
imshow(img, 'Border', 'tight', 'InitialMagnification', 'fit');
truesize;
set(fig, 'visible', 'on');

end