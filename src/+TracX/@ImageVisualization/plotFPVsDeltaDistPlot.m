%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ fh ] = plotFPVsDeltaDistPlot( ~, ...
    xFPDistAssigned, xFPDistAssignedToCompare, yDeltaDistAssigned, ...
    yDeltaDistToCompare, fpDistThreshold, lowerQuantile, varargin)
% PLOTFPVSDELTADISTPLOT Displays a scatter plot figure with
% fingerprint distance vs. delta distance plotted. Additional histograms
% show the distributions of the main cathegory along the respective axes.
%
% Args:
%    ignoredArg (:obj:`object`):                  :class:`ImageVisualization` instance 
%    xFPDistAssigned (:obj:`array` 1xk):          Fingerprint distance of 
%                                                 all assignments         
%    xFPDistAssignedToCompare (:obj:`array` 1xj): Fingerprint distances of
%                                                 neighbouring cells for 
%                                                 each assigned cell.
%    yDeltaDistAssigned (:obj:`array` 1xk):       Delta distance of all
%                                                 assignments to the
%                                                 minimal FPdist of all
%                                                 possible assignments for
%                                                 each cell.
%    yDeltaDistToCompare (:obj:`array` 1xj):      Delta distance of all
%                                                 assignments to their 
%                                                 comparator (i.e. its direct
%                                                 neighbours or all other
%                                                 cells)
%    fpDistThreshold (:obj:`fluat` 1x1):          Fingerprint distance
%                                                 threshold.
%    lowerQuantile (:obj:`float` 1x1):            Lower quantile [0,1]
%                                                 used for fpDistThreshold
%                                                 determination.
%    varargin (:obj:`str varchar`):
%
%        * **lowXLim** (:obj:`float`):            Lower plot x limit.
%        * **upXLim** (:obj:`float`):             Upper plot x limit. 
%        * **lowYLim** (:obj:`float`):            Lower plot y limit.
%        * **upYLim** (:obj:`float`):             Upper plot y limit. 
%        * **plotThreshold** (:obj:`float`):      Flag; if threhsold should be plotted. Defaults to true.
%        * **plotTitle** (:obj:`float`):          Flag; if title should be plotted. Defaults to true.
%        * **markerFaceAlpha** (:obj:`float`):    The transparency of the scatter dots in the range [0,1]. Defaults to 0.2.
%        * **markerSize** (:obj:`float`):         The size of the scatter dots [>0]. Defaults to 32.
%
% Returns
% -------
%    fh: :obj:`object` 1xM
%                                                  Figure handle.  
%
% :Authors:
%    Andreas P. Cuny - initial implementation

parser = inputParser;
parser.addParameter('lowYLim', -1, @(x) isfloat(x) || isinteger(x));
parser.addParameter('lowXLim', 0, @(x) isfloat(x) || isinteger(x));
parser.addParameter('upXLim', 2.5, @(x) isfloat(x) || isinteger(x));
parser.addParameter('upYLim', 2.5, @(x) isfloat(x) || isinteger(x));
parser.addParameter('plotThreshold', true, @(x) islogical(x));
parser.addParameter('plotTitle', true, @(x) islogical(x));
parser.addParameter('markerFaceAlpha', 0.2, @(x) isfloat(x) || isinteger(x));
parser.addParameter('markerSize', 35, @(x) isfloat(x) || isinteger(x));

parser.parse(varargin{:});
lowXLim    = parser.Results.lowXLim;
lowYLim    = parser.Results.lowYLim;
upXLim    = parser.Results.upXLim;
upYLim    = parser.Results.upYLim;
plotThreshold = parser.Results.plotThreshold;
plotTitle = parser.Results.plotTitle;
markerFaceAlpha = parser.Results.markerFaceAlpha;
markerSize = parser.Results.markerSize;

% Definition of the colors used for scatter plots and histograms.
cMapAssigned = [34/255, 152/255, 140/255];
cMapOtherComparisons = [253/255, 190/255, 60/255];

% Plot figure
fh = figure('Position', [50, 50, 1000, 600]); % 1200, 680
set(gcf,'color','w');
clf
ah1 = subplot(2, 2,  4);
% h1 = histogram(yDeltaDistToCompare, 'Orientation','horizontal');
% h1.Normalization = 'probability';
% h1.BinWidth = 0.05;
% h1.FaceColor = cMapOtherComparisons;
h1 = histfit(yDeltaDistToCompare, ceil(numel(yDeltaDistToCompare)/100),'kernel');
h1(2).Color = cMapOtherComparisons;
h1(1).FaceColor = [1 1 1];
h1(1).EdgeColor = [1 1 1];
ha1 = area(h1(2).XData, h1(2).YData);
ha1.FaceColor = cMapOtherComparisons;
ha1.FaceAlpha = 0.5;
set(gca, 'view', [90, -90])
%ylabel('Counts')
set(gca, 'Layer', 'top')


% h1(2).Color = cMapOtherComparisons;
% h1(1).FaceColor = [1 1 1];
% h1(1).EdgeColor = [1 1 1];
% xlabel('Counts')

ah2 = subplot(2, 2, 1);
h2 = histfit(xFPDistAssigned, ceil(numel(yDeltaDistToCompare)/100),'kernel');
h2(2).Color = cMapAssigned;
h2(1).FaceColor = [1 1 1];
h2(1).EdgeColor = [1 1 1];
ha2 = area(h2(2).XData, h2(2).YData);
ha2.FaceColor = cMapAssigned;
ha2.FaceAlpha = 0.5;



%ylabel('Counts')
set(gca, 'Layer', 'top')

%h2 = histogram(xFPDistAssigned);
%h2.Normalization = 'probability';
%h2.BinWidth = 0.02;
%h2.FaceColor = cMapAssigned;

if plotTitle
    title(sprintf('FPdist threshold %0.4f for the %d%% quantile.', ...
        fpDistThreshold, lowerQuantile*100))
end

ah3 = subplot(2, 2, 3);
sh1 = scatter(xFPDistAssigned, yDeltaDistAssigned, markerSize, ...
    cMapAssigned, 'filled');
sh1.MarkerFaceAlpha = markerFaceAlpha;
sh1.MarkerEdgeAlpha = markerFaceAlpha;
set(gca, 'Layer', 'top')
%xlim([0, fixedXLim])
%ylim([0, fixedYLim])
hold on
sh2 = scatter(xFPDistAssignedToCompare, yDeltaDistToCompare, markerSize, ...
    cMapOtherComparisons, 'filled');
sh2.MarkerFaceAlpha = markerFaceAlpha;
sh2.MarkerEdgeAlpha = markerFaceAlpha;
xlabel('Fingerprint distance (a.u.)')
ylabel('Delta distance (a.u.)')
hold on
if plotThreshold
    ha = annotation('textarrow', 'String','Threshold');
    ha.Parent = gca;
    ha.X = [fpDistThreshold fpDistThreshold];
    if round(min([yDeltaDistToCompare; yDeltaDistAssigned]), 1) ~= 0
        ha.Y = [round(min([yDeltaDistToCompare; yDeltaDistAssigned]), 1)/2 0];
    else
        ha.Y = [-0.2 0];
    end
end

grid on
ah1.Box = 'off';
set(gca, 'Layer', 'top')
set(ah1,'XTickLabel',[]);
ah1.YAxis.Visible = 'off';
set(ah1,'XTick',[]);
ah2.Box = 'off';
ah2.XAxis.Visible = 'on';
set(ah2,'XTickLabel',[]);
set(gca, 'Layer', 'top')
ah2.YAxis.Visible = 'off';
set(ah2,'XTick',[]);
hFig    = gcf;
hAxes   = findobj(allchild(hFig), 'flat', 'Type', 'axes');
% set(hAxes(3), 'Position', [0.7128, 0.0718, 0.12, 0.5821]);
% set(hAxes(1), 'Position', [0.0542, 0.0687, 0.6398, 0.5845]);
% set(hAxes(2), 'Position', [0.0542, 0.68, 0.6398, 0.12]);
% set(hAxes(3), 'Position', [0.7128, 0.0718, 0.12, 0.5821]);
% set(hAxes(1), 'Position', [0.0542, 0.0687, 0.6398, 0.5845]);
% set(hAxes(2), 'Position', [0.0542, 0.68, 0.6398, 0.12]);
set(hAxes(3), 'Position', [0.85, 0.079, 0.14, 0.69]);
set(hAxes(1), 'Position', [0.0542, 0.079, 0.79, 0.69]);
set(hAxes(2), 'Position', [0.0542, 0.78, 0.79, 0.18]);

% Scatter plot
sXLimits = xlim(ah3);
sXLimits(1) = lowXLim;
sXLimits(2) = upXLim;
set(ah3, 'XLim', sXLimits);
sYLimits = ylim(ah3);
sYLimits(1) = lowYLim;
sYLimits(2) = upYLim;
set(ah3, 'YLim', sYLimits);

% Right hist
xlim(ah1);
ylim(ah1);
set(ah1, 'XLim', sYLimits);
% Top hist
xlim(ah2);
set(ah2, 'XLim', sXLimits);
ylim(ah2);


end

