%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef NonAssignedTracks < handle
    % NONASSIGNEDTRACKS This class holds the minimal required
    % information of the non assigned tracks after each round of tracking
    % improvement evaluation.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties (GetAccess=public, SetAccess=private)
        
        maxTrackFrameSkipping = [] % :class:`~+TracX.@ParameterConfiguration.ParameterConfigurationmaxTrackFrameSkipping`     
        track_index = [] % :class:`~+TracX.@SegmentationData.SegmentationData.track_index`       
        track_age = [] % :class:`~+TracX.@SegmentationData.SegmentationData.track_age`        
        cell_center_x = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_center_x`        
        cell_center_y = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_center_y`       
        cell_center_z = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_center_z`       
        cell_filtered_dif_x = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_filtered_dif_x`       
        cell_filtered_dif_y = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_filtered_dif_y`       
        cell_filtered_dif_z = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_filtered_dif_z`       
        cell_area = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_area`      
        cell_orientation = [] % :class:`~+TracX.@SegmentationData.SegmentationData.cell_orientation`
        track_fingerprint_real_distance = [] % :class:`~+TracX.@SegmentationData.SegmentationData.track_fingerprint_real_distance`
        track_fingerprint_real = [] % :class:`~+TracX.@SegmentationData.SegmentationData.track_fingerprint_real`
        track_assignment_fraction = [] % :class:`~+TracX.@SegmentationData.SegmentationData.track_assignment_fraction`
        
    end
    
    methods
        
        % Constructor
        function this = NonAssignedTracks()
            % NONASSIGNEDTRACKS Constructs a :class:`NonAssignedTracks` object instance
            % to keep teporary frame data during tracking.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                           Returns :class:`NonAssignedTracks` instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
        end
        
        function addCell(this, data, varargin)
            % ADDCELL Adds a cell to TracX.NonAssignedTracks instance
            % to keep teporary the data of non assigned tracks.
            %
            % Args:
            %    this (:obj:`object`):     :class:`NonAssignedTracks` instance.
            %    data (:obj:`array`):      Data array in order of properties
            %    varargin (:obj:`array`):  Optional input
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            %                             Adds data to :class:`NonAssignedTracks`
            %                             directly.
            if nargin == 2
                if ~isempty(data)
                    % Avoid douplicates. But update the values to the
                    % latest found position.
                    locIdx = ~ismember(data(:, 1), this.track_index);
                    
                    this.track_index = [this.track_index; data(locIdx, 1)];
                    this.cell_center_x = [this.cell_center_x; data(locIdx, ...
                        2)];
                    this.cell_center_y = [this.cell_center_y; data(locIdx, ...
                        3)];
                    this.cell_center_z = [this.cell_center_z; data(locIdx, ...
                        4)];
                    this.cell_filtered_dif_x = [this.cell_filtered_dif_x; ...
                        data(locIdx, 5)];
                    this.cell_filtered_dif_y = [this.cell_filtered_dif_y; ...
                        data(locIdx, 6)];
                    this.cell_filtered_dif_z = [this.cell_filtered_dif_z; ...
                        data(locIdx, 7)];
                    this.cell_area = [this.cell_area; data(locIdx, 8)];
                    this.cell_orientation = [this.cell_orientation; ...
                        data(locIdx, 9)];
                    this.track_age = [this.track_age; data(locIdx, 10) ]; ...
                        this.track_fingerprint_real_distance = ...
                        [this.track_fingerprint_real_distance; data(locIdx, 11)];
                    this.track_assignment_fraction = ...
                        [this.track_assignment_fraction; data(locIdx, 12)];
                end
                this.evaluateTrackAge()
            else
                disp(['Not enought input arguments; track_index, cell_area, ', ...
                    'cell_center_x, cell_center_y, cell_filtered_dif_x', ...
                    'cell_filtered_dif_y, cell_area', ...
                    'cell_orientation, age and track_fingerprint_real_distance', ...
                    'are needed']);
            end
        end
        
        function removeCell(this, track_index)
            % REMOVECELL Removes a cell from TracX.NonAssignedTracks
            % instance.
            %
            % Args:
            %    this (:obj:`object`):       :class:`NonAssignedTracks` instance.
            %    track_index (:obj:`array`): Track indices of data to be
            %                                removed
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            %                                 Removes data from :class:`NonAssignedTracks`
            %                                 directly.
            if nargin == 2
                idx = ismember(this.track_index, track_index);
                if ~isempty(idx)
                    this.track_index(idx) = [];
                    this.cell_center_x(idx) = [];
                    this.cell_center_y(idx) = [];
                    this.cell_center_z(idx) = [];
                    this.cell_filtered_dif_x(idx) = [];
                    this.cell_filtered_dif_y(idx) = [];
                    this.cell_filtered_dif_z(idx) = [];
                    this.cell_area(idx) = [];
                    this.cell_orientation(idx) = [];
                    this.track_age(idx) = [];
                    this.track_fingerprint_real_distance(idx) = [];
                    this.track_assignment_fraction(idx) = [];
                end
            else
                sprintf(['Not enought input arguments;' ...
                    'track_index is needed']);
            end
        end
        
        function evaluateTrackAge(this)
            % EVALUTATETRACKAGE Evaluates track age and removes cell above
            % maxTrackFrameSkipping threshold.
            %
            % Args:
            %    this (:obj:`object`):     :class:`NonAssignedTracks` instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            %                             Adds data to :class:`NonAssignedTracks`
            %                             directly.
            tooOldTracks = this.track_index( ...
                this.track_age > this.maxTrackFrameSkipping);
            this.removeCell(tooOldTracks)
        end
        
        %> @todo DEPRECIATED not used anymore
        function ret = getCells(this)
            ret = [this.track_index, this.cell_center_x, ...
                this.cell_center_y, this.cell_center_z, this.cell_filtered_dif_x, ...
                this.cell_filtered_dif_y, this.cell_filtered_dif_z, this.cell_area, ...
                this.cell_orientation, this.track_age, ...
                this.track_fingerprint_real_distance, ...
                this.track_assignment_fraction];
        end
        
        function ret = getCellStruct(this)
            % GETCELLSTRUCT Returns data as struct.
            %
            % Args:
            %    this (:obj:`object`):    :class:`NonAssignedTracks` instance.
            %
            % Returns
            % -------
            %    ret (:obj:`struct`)
            %                             Data from TracX.NonAssignedTracks
            %                             as formated struct.
            
            ret = struct('track_index', this.track_index, 'cell_center_x', ...
                this.cell_center_x, 'cell_center_y', this.cell_center_y, ...
                'cell_center_z', this.cell_center_z, ...
                'cell_filtered_dif_x', this.cell_filtered_dif_x, ...
                'cell_filtered_dif_y', this.cell_filtered_dif_y, ...
                'cell_filtered_dif_z', this.cell_filtered_dif_z, ...
                'cell_area', this.cell_area, 'cell_orientation', ...
                this.cell_orientation, 'track_age', this.track_age, ...
                'track_fingerprint_real_distance', ...
                this.track_fingerprint_real_distance, ...
                'track_assignment_fraction', ...
                this.track_assignment_fraction);
        end
        
        function merged = merge(~, structA, structB)
            % MERGE Merges two structs.
            %
            % Args:
            %    ignoredArg (:obj:`object`):  :class:`NonAssignedTracks` instance.
            %    structA (:obj:`struct`):     Struct A
            %    structB (:obj:`struct`):     Struct B
            %
            % Returns
            % -------
            %    merged (:obj:`struct`)
            %                                 Merged data
            
            merged = structA;
            if ~isempty(structB.track_index)
                sizeA = length(merged);
                f = fieldnames(structB);
                for i = 1:length(f)
                    merged(sizeA).(f{i}) = [structA.(f{i}); structB.(f{i})];
                end
                
            end
        end
        
        function setMaxTrackFrameSkipping(this, ...
                maxTrackFrameSkipping)
            % SETMAXTRACKFRAMESKIPPING Sets the max track frame skipping
            % threshold.
            %
            % Args:
            %    this (:obj:`object`):               :class:`NonAssignedTracks` instance.
            %    maxTrackFrameSkipping (:obj:`int`): Max track frame
            %                                        skipping threshold
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            %                                        Acts on :class:`NonAssignedTracks`
            %                                        directly
            
            this.maxTrackFrameSkipping = ...
                maxTrackFrameSkipping;
        end
        
        function increaseTrackAge(this)
            % INCREASETRACKAGE Increases track age by one.
            %
            % Args:
            %    this (:obj:`object`):               :class:`NonAssignedTracks` instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            %                                        Acts on :class:`NonAssignedTracks`
            %                                        directly
            
            this.track_age = this.track_age + 1;
            % Remove too old tracks directly
            this.evaluateTrackAge();
        end
    end
end