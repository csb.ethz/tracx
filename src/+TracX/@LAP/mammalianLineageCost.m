%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function cost = mammalianLineageCost(~, oldFrameData, newFrameData, ...
    daughterpairTracks, parentTracks, daughterpairTypes, parentTypes)
% MAMMALIANLINEAGECOST Cost matrix function for mammalian lineage
% reconstruction in 2 & 3D.
%
% Args:
%    ignoredArg (:obj:`object`):            :class:`LAP` instance.
%    oldFrameData (:obj:`object`):          oldFrame :class:`TemporaryTrackingData` instance.
%    newFrameData (:obj:`object`):          newFrame :class:`TemporaryTrackingData` instance.
%    daughterpairTracks (:obj:`int`, 1xK):  Array of daughterpair tracks
%    parentTracks (:obj:`int`, 1xJ):        Array of parent tracks
%    daughterpairTypes (:obj:`int`, 1x1):   Type of daughter pairs (I or II)
%    parentTypes (:obj:`int`, 1x1):         Type of parents (I or II)
%
% Returns
% -------
%    cost: :obj:`array` 
%                                           Cost matrix
%
% :Authors:
%    Thomas Kuendig - initial implementation

% tic
nDaughterpairs = size(daughterpairTracks,1);
nParents = length(parentTracks);

%cost = nan(NumDaughterpairs,NumParents);

[~, parentOldFrameIdx] = ismember(parentTracks,oldFrameData.track_index);
[~, daughterNewFramepairIdx] = ismember(daughterpairTracks,newFrameData.track_index);


% Area Cost: calculated from the difference between the sum of daughter
% cells' sizes and the parents size. Ideally they would match ~ cost = 0;
daughterpairAreas = sum(newFrameData.cell_area(daughterNewFramepairIdx'),1);
parentAreas = oldFrameData.cell_area(parentOldFrameIdx);

areaDiff = repmat(parentAreas,1,nDaughterpairs) ./  ...
    repmat(daughterpairAreas,nParents,1) - 1;
areaCost = areaDiff .^ 2 * 100;


% Dist Cost: calculated from the summed squared distance of both daughter to the parents.
% Ideally they would match ~ cost = 0;
parentX = oldFrameData.cell_center_x(parentOldFrameIdx) ...
   + oldFrameData.cell_filtered_dif_x(parentOldFrameIdx);
parentY = oldFrameData.cell_center_y(parentOldFrameIdx) ...
   + oldFrameData.cell_filtered_dif_y(parentOldFrameIdx);
parentZ = oldFrameData.cell_center_z(parentOldFrameIdx) ...
   + oldFrameData.cell_filtered_dif_z(parentOldFrameIdx);

daughtersX = newFrameData.cell_center_x(daughterNewFramepairIdx') ...
    + newFrameData.cell_filtered_dif_x(daughterNewFramepairIdx');
daughtersY = newFrameData.cell_center_y(daughterNewFramepairIdx') ...
    + newFrameData.cell_filtered_dif_y(daughterNewFramepairIdx');
daughtersZ = newFrameData.cell_center_z(daughterNewFramepairIdx') ...
    + newFrameData.cell_filtered_dif_z(daughterNewFramepairIdx');


distDaughter1 = sqrt( ( repmat(daughtersX(1,:),nParents,1) - repmat(parentX,1,nDaughterpairs) ) .^2 + ...
    ( repmat(daughtersY(1,:),nParents,1) - repmat(parentY,1,nDaughterpairs) ) .^2 + ...
    ( repmat(daughtersZ(1,:),nParents,1) - repmat(parentZ,1,nDaughterpairs) ) .^2 );

distDaughter2 = sqrt( ( repmat(daughtersX(2,:),nParents,1) - repmat(parentX,1,nDaughterpairs) ) .^2 + ...
    ( repmat(daughtersY(2,:),nParents,1) - repmat(parentY,1,nDaughterpairs) ) .^2 + ...
    ( repmat(daughtersZ(2,:),nParents,1) - repmat(parentZ,1,nDaughterpairs) ) .^2 );

distCost = (distDaughter1 + distDaughter2) .^ 2;


% if the daughter and/or the parent is a survivor cell but not the same
% track, they are discarded / cost = inf;
wrongSurvivorPenalty = zeros(nParents, nDaughterpairs);
survivorInvolved = repmat(daughterpairTypes, nParents,1) | ...
    repmat(parentTypes,1,nDaughterpairs);
parentIsNotDaughter = repmat(parentTracks,1,nDaughterpairs) ~= repmat(daughterpairTracks(:,2)',nParents,1) & ...
    repmat(parentTracks,1,nDaughterpairs) ~= repmat(daughterpairTracks(:,1)',nParents,1);
wrongSurvivorPenalty(survivorInvolved & parentIsNotDaughter) = Inf;

cost = (areaCost + distCost + wrongSurvivorPenalty)';

% % DEPRECIATED FUNCTION
% toc
% tic
% for i = 1:length(daughterpairTracks)
%     for j = 1:length(parentTracks)
% 
%         daughterpair = daughterpairTracks(i,:);
%         parent = parentTracks(j);
%         daughterpairtype = daughterpairtypes(i);
%         parenttype = parenttypes(j);
% 
%         if (daughterpairtype || parenttype) && ~ismember(parent,daughterpair)
%             cost2(i,j) = inf;
%             continue
%         end
% 
%         firstDaughterIdx = newFrameData.track_index == daughterpair(1);
%         secondDaughterIdx = newFrameData.track_index == daughterpair(2);
%         parentIdx = oldFrameData.track_index == parent;
% 
%         areadiff = oldFrameData.cell_area(parentIdx) ./ (newFrameData.cell_area(firstDaughterIdx) ...
%             + newFrameData.cell_area(secondDaughterIdx)) - 1;
%         areacost(i,j) = min(areadiff .^ 2 * 100);
% 
% 
%         distdiff = nan(3,2);
%         distdiff(1,1) = oldFrameData.cell_center_x(parentIdx) + oldFrameData.cell_filtered_dif_x(parentIdx)  - newFrameData.cell_center_x(firstDaughterIdx) - newFrameData.cell_filtered_dif_x(firstDaughterIdx) ;
%         distdiff(2,1) = oldFrameData.cell_center_y(parentIdx) + oldFrameData.cell_filtered_dif_y(parentIdx)  - newFrameData.cell_center_y(firstDaughterIdx) - newFrameData.cell_filtered_dif_y(firstDaughterIdx) ;
%         distdiff(3,1) = oldFrameData.cell_center_z(parentIdx) + oldFrameData.cell_filtered_dif_z(parentIdx)  - newFrameData.cell_center_z(firstDaughterIdx) - newFrameData.cell_filtered_dif_z(firstDaughterIdx) ;
% 
%         distdiff(1,2) = oldFrameData.cell_center_x(parentIdx) + oldFrameData.cell_filtered_dif_x(parentIdx)  - newFrameData.cell_center_x(secondDaughterIdx) - newFrameData.cell_filtered_dif_x(secondDaughterIdx);
%         distdiff(2,2) = oldFrameData.cell_center_y(parentIdx) + oldFrameData.cell_filtered_dif_y(parentIdx)  - newFrameData.cell_center_y(secondDaughterIdx) - newFrameData.cell_filtered_dif_y(secondDaughterIdx);
%         distdiff(3,2) = oldFrameData.cell_center_z(parentIdx) + oldFrameData.cell_filtered_dif_z(parentIdx)  - newFrameData.cell_center_z(secondDaughterIdx) - newFrameData.cell_filtered_dif_z(secondDaughterIdx);
% 
%         distcost(i,j) = norm(distdiff(:,1)') + norm(distdiff(:,2)');
% 
%     end
% end
%         cost = areacost + distcost;
%         toc
end