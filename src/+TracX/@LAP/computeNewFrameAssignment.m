%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ newTrackIndex, assignmentTotalCost, assignmentPositionCost, ...
    assignmentAreaCost, assignmentRotationCost, ...
    assignmentFrameSkippingCost] = computeNewFrameAssignment( this, ...
    oldFrame, newFrame, maxCellCenterDisplacement, ...
    maxMajorAxisRotation, maxTrackFrameSkipping, ...
    individualFunctionPenalty, maxCellSizeDecrease, averageCellSizeGrowth, ...
    maxCellSizeIncrease, usedFunctionsForCostMatrix, ...
    fingerprintDistThreshold, meanCellDiameter, is3DData)
% COMPUTENEWFRAMEASSIGNMENT Computes linear assignment and returns
% track indicies for the subsequent frame ('newFrame').
%
% Args:
%    this (:obj:`object`):                          :class:`LAP` instance.
%    oldFrame (:obj:`object`):                      oldFrame :class:`TemporaryTrackingData` instance.
%    newFrame (:obj:`object`):                      newFrame :class:`TemporaryTrackingData` instance.
%    maxCellCenterDisplacement (:obj:`int`, 1x1):   Maximal frame to frame
%                                                   segmentation centroid
%                                                   displacement
%    maxMajorAxisRotation (:obj:`float` 1x1):       Max allowed major axis
%                                                   rotation in degree.
%    maxTrackFrameSkipping (:obj:`int` 1x1):        Max number of frames a
%                                                   cell is allowed to be 
%                                                   missing from a track
%    individualFunctionPenalty (:obj:`int` 1x1):    Individual function penalty
%    maxCellSizeDecrease (:obj:`float` 1x1):        Maximal allowed frame 
%                                                   to frame cell size 
%                                                   decrease in percent.
%    averageCellSizeGrowth (:obj:`float` 1x1):      Average allowed frame 
%                                                   to frame cell size growth
%                                                   in percent.
%    maxCellSizeIncrease (:obj:`float` 1x1):        Maximal allowed frame 
%                                                   to frame cell size 
%                                                   increase in percent.
%    usedFunctionsForCostMatrix (:obj:`array` 1x4): Logical array to select 
%                                                   which of the four penalty  
%                                                   functions should be used 
%                                                   to build the cost matrix.
%    fingerprintDistThreshold (:obj:`float` 1x1):   CRF threshold
%    meanCellDiameter (:obj:`float` 1x1):           Mean segmentation  
%                                                   object diameter (cell   
%                                                   diameter).
%    is3DData (:obj:`bool`):                        Is data 3D. Defaults to 
%                                                   0 for 2D data. 
%
% Returns
% -------
%    newTrackIndex: :obj:`array`
%                                                New track indices array
%    totalCost: :obj:`float` IxJ                
%                                                Costmatrix with the sum
%                                                of cost matricies positionCost to
%                                                frameSkippingCost
%    positionCost: :obj:`float` IxJ              
%                                                Costmatrix for spatial
%                                                position.
%    areaCost: :obj:`float` IxJ                  
%                                                Costmatrix for size
%                                                difference (area).
%    rotationCost: :obj:`float` IxJ               
%                                                Costmatrix for rotation
%                                                difference (orientation).
%    frameSkippingCost: :obj:`float` IxJ      
%                                                Costmatrix for frame
%                                                skipping (segmented object
%                                                not present/detected on
%                                                current image frame).
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get length of the newFrame to preallocate the newTrackIndex array
lengthNewFrame = length(newFrame.cell_area);
newTrackIndex = zeros(lengthNewFrame, 1);

% Compute the costs
[totalCost, positionCost, areaCost, rotationCost, frameSkippingCost] = ...
    this.computeAssignmentCostMatrix(...
    oldFrame, newFrame, maxCellCenterDisplacement, ...
    maxMajorAxisRotation, ...
    individualFunctionPenalty, maxCellSizeDecrease, ...
    averageCellSizeGrowth, maxCellSizeIncrease, ...
    usedFunctionsForCostMatrix, meanCellDiameter, ...
    maxTrackFrameSkipping, 1, is3DData);

% Apply knowledge from fingerprint
[ totalCost] = this.fingperprintCostMatrixRefinement( newFrame, ...
    oldFrame, totalCost, fingerprintDistThreshold);

% Solve the assignment
[ ~ , assignmentColIdx, ~, assignmentTotalCost, ...
    assignmentPositionCost, assignmentAreaCost, assignmentRotationCost, ...
    assignmentFrameSkippingCost] = this.solveLAP( totalCost, ...
    positionCost, areaCost, rotationCost, frameSkippingCost);

validAssignmentsLocIdx = ismember(assignmentColIdx, ...
    1:numel(newFrame.cell_area));
newTrackIndex(assignmentColIdx(validAssignmentsLocIdx)) = ...
    oldFrame.track_index(validAssignmentsLocIdx);

% validTracks = oldFrame.track_index(~excludeFromAssignmentIdx);
% newTrackIndex(assignmentColIdx) = validTracks(assignmentRowIdx);
% Fill the non assigned entries with Id's from the newFrame
nonAssignedNewFrameTracks = newFrame.track_index(newTrackIndex == 0);
if all(ismember(nonAssignedNewFrameTracks, newTrackIndex) == 0)
    newTrackIndex(newTrackIndex == 0) = nonAssignedNewFrameTracks;
else
    nonUniqueIds = nonAssignedNewFrameTracks(ismember(...
        nonAssignedNewFrameTracks, newTrackIndex));
    for i = 1:numel(nonUniqueIds)
        nonAssignedNewFrameTracks(ismember(nonAssignedNewFrameTracks, ...
            nonUniqueIds(i))) = max(newFrame.track_index) + i;
    end
    newTrackIndex(newTrackIndex == 0) = nonAssignedNewFrameTracks;
end

end