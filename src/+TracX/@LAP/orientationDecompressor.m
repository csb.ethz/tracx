%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ vec ] = orientationDecompressor (~, tag) 
% ORIENTATIONDECOMPRESSOR Decompresses the linearized orientation vector to
% its 3D format.
%
% Args:
%    ignoredArg (:obj:`object`): :class:`LAP` instance.
%    tag (:obj:`array`):         Compressed and linearized orientation 3D vector        
%
% Returns
% -------
%    vec: :obj:`array` Kx3
%                                Decompressed orientation vector 
%
% :Authors:
%    Thomas Kuendig - initial implementation

vec=zeros(size(tag,1), size(tag,2), 3);
vec(:,:,1) = round(tag,-9)/1e14 - 1;
vec(:,:,2) = round(mod(tag,1e10),-4)/1e9 - 1;
vec(:,:,3) = round(mod(tag,1e5))/1e4 - 1;

end