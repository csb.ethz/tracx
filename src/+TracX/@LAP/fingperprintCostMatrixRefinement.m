%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ costMatrix] = fingperprintCostMatrixRefinement(~, newFrame, ...
    oldFrame, costMatrix, fingerprintDistThreshold)
% FINGERPRINTCOSTMATRIXREFINEMENT Refines the cost matrix by setting 
% costs for confirmed/trusted assignments (below fingerprint distance 
% threshold) to 0 and detection of invalid costMatrix rows (all costs 
% above max individual function penalty.
%
% Args:
%    ignoredArg (:obj:`object`):              :class:`LAP` instance.
%    oldFrame (:obj:`object`):                oldFrame :class:`TemporaryTrackingData` instance.
%    newFrame (:obj:`object`):                newFrame :class:`TemporaryTrackingData` instance.
%    costMatrix (:obj:`double`, NxM):         Cost matrix for assignments 
%                                             between old- and newFrame.
%    fingerprintDistThreshold (:obj:`float`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.fingerprintDistThreshold`
%
% Returns
% -------
%    costMatrix: :obj:`double` NxM
%                                             Refined cost matrix
%    excludeFromAssignmentIdx: :obj:`array` Nx1 
%                                             Logical indicies of rows to
%                                             exclude from costMatrix.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Set costs to 0 for fingerprint threshold passed assignments
[commonLocIdxOldFrame, oldInNewLocIdx] = ismember(oldFrame.track_index, ...
    newFrame.track_index);
commonRowsOld = 1:numel(oldFrame.track_index);
linIdx = sub2ind([numel(oldFrame.cell_area), numel(newFrame.cell_area)], ...
commonRowsOld(commonLocIdxOldFrame)', oldInNewLocIdx(commonLocIdxOldFrame));

useFFP = true;
if useFFP == true
     trustedAssignments = oldFrame.track_assignment_fraction <= fingerprintDistThreshold; %== 0;
else
    trustedAssignments = oldFrame.track_fingerprint_real_distance < ...
        fingerprintDistThreshold;
end

costMatrix(linIdx(trustedAssignments(commonLocIdxOldFrame))) = 0;

end
