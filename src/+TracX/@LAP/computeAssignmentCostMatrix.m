%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [totalCost, positionCost, areaCost, rotationCost, ...
    frameSkippingCost] = computeAssignmentCostMatrix(this, ...
    oldFrame, newFrame, maxCellCenterDisplacement, ...
    maxMajorAxisRotation, individualFunctionPenalty, maxCellSizeDecrease, ...
    averageCellSizeGrowth, maxCellSizeIncrease, ...
    usedFunctionsForCostMatrix, meanCellDiameter, ...
    maxTrackFrameSkipping, displacementWeightingMode, is3DData)
% COMPUTEASSIGNMENTCOSTMATRIX computes the individual
% assignement costs for spatial (position), size (area) and
% rotation (orientation) differences as well as frame skipping
% and returns the matrices for each cost individually (positionCost:
% frameSkippingCost) as well as the sum of all costs (totalCost).
%
% Args:
%    ignoredArg (:obj:`object`):                    :class:`LAP` instance.
%    oldFrame (:obj:`object`):                      oldFrame :class:`TemporaryTrackingData` instance
%    newFrame (:obj:`object`):                      newFrame :class:`TemporaryTrackingData` instance
%    maxCellCenterDisplacement (:obj:`int`, 1x1):   Maximal frame to frame
%                                                   segmentation centroid
%                                                   displacement
%    maxMajorAxisRotation (:obj:`float` 1x1):       Max allowed major axis
%                                                   rotation in degree.
%    individualFunctionPenalty (:obj:`int` 1x1):    Individual function penalty
%    maxCellSizeDecrease (:obj:`float` 1x1):        Maximal allowed frame 
%                                                   to frame cell size 
%                                                   decrease fraction.
%    averageCellSizeGrowth (:obj:`float` 1x1):      Average allowed frame 
%                                                   to frame cell size growth
%                                                   fraction.
%    maxCellSizeIncrease (:obj:`float` 1x1):        Maximal allowed frame 
%                                                   to frame cell size 
%                                                   increase fraction.
%    usedFunctionsForCostMatrix (:obj:`array` 1x4): Logical array to select 
%                                                   which of the four penalty  
%                                                   functions should be used 
%                                                   to build the cost matrix.
%    meanCellDiameter(:obj:`float` 1x1):            Mean segmentation  
%                                                   object diameter (cell   
%                                                   diameter).
%    maxTrackFrameSkipping (:obj:`int` 1x1):        Max number of frames a
%                                                   cell is allowed to be 
%                                                   missing from a track
%    displacementWeightingMode (:obj:`bool`):       For quadratic (default)  
%                                                   or 1 for linear  
%                                                   displacement cost 
%                                                   calculation.
%    is3DData (:obj:`bool`):                        Is data 3D. Defaults to 
%                                                   0 for 2D data. 
%
% Returns
% -------
%    totalCost: :obj:`float` IxJ                
%                                                Costmatrix with the sum
%                                                of cost matricies positionCost to
%                                                frameSkippingCost
%    positionCost: :obj:`float` IxJ              
%                                                Costmatrix for spatial
%                                                position.
%    areaCost: :obj:`float` IxJ                  
%                                                Costmatrix for size
%                                                difference (area).
%    rotationCost: :obj:`float` IxJ               
%                                                Costmatrix for rotation
%                                                difference (orientation).
%    frameSkippingCost: :obj:`float` IxJ      
%                                                Costmatrix for frame
%                                                skipping (segmented object
%                                                not present/detected on
%                                                current image frame).
%
% :Authors:
%    Markus Duerr    - initial implementation in R (weight_age)
% :Authors:
%    Urs Kuechler    - translation to Matlab (weight_age)
% :Authors:
%    Andreas P. Cuny - Re-implementation and new design for computation of individual cost functions with real physical meaning.
%

% Determine dimension of cost matrix
ro = length(oldFrame.cell_area); % dim of rows 
cn = length(newFrame.cell_area); % dim of cols 

% Calculate the absolute difference in x - y pixel position; in other words
% we calculate the expected position in the newFrame based on the position
% in the oldFrame using a by the cell neighbourhood filtered vector
% components
distanceX = repmat(oldFrame.cell_center_x + oldFrame.cell_filtered_dif_x .* ...
    oldFrame.track_age, 1, cn) - repmat(newFrame.cell_center_x.', ...
    ro, 1);
distanceY = repmat(oldFrame.cell_center_y + oldFrame.cell_filtered_dif_y .* ...
    oldFrame.track_age, 1, cn) - repmat(newFrame.cell_center_y.', ...
    ro, 1);
if is3DData
    distanceZ = repmat(oldFrame.cell_center_z + oldFrame.cell_filtered_dif_z .* ...
        oldFrame.track_age, 1, cn) - repmat(newFrame.cell_center_z.', ...
        ro, 1);
end

% Caclulate a motion depentent position cost correction. For non motile
% cell initial position costs is used where as for motile cells costs are
% lowered.
if is3DData
vecLengthOld = (sqrt(oldFrame.cell_filtered_dif_x.^2 + ...
    oldFrame.cell_filtered_dif_y.^2 + ...
    oldFrame.cell_filtered_dif_z.^2));
else
   vecLengthOld = (sqrt(oldFrame.cell_filtered_dif_x.^2 + ...
    oldFrame.cell_filtered_dif_y.^2)); 
end
vecLength = repmat(vecLengthOld, 1, cn);
motionDependentPosCostCorrection = 1 ./ ( 1 + (vecLength ./ ...
    meanCellDiameter));

% Calculate the relative difference in cell area
relativeAreaDifference = repmat(newFrame.cell_area.', ro, 1) ./ repmat(...
    oldFrame.cell_area, 1, cn);

% Calcualte the absolute difference in cell orientation
if is3DData
    orientationDifference =  this.computeOrientationDifferenceMatrix( ...
        repmat(this.orientationDecompressor(oldFrame.cell_orientation), 1, cn), ...
        repmat(this.orientationDecompressor(newFrame.cell_orientation.'), ro, 1));
else
    orientationDifference = repmat(oldFrame.cell_orientation, 1, cn) - ...
        repmat(newFrame.cell_orientation.', ro, 1);
end


% Calculation of the different costs
%
% calculation of the x - y displacement costs based on
% the predicted position
if displacementWeightingMode == 0 % quadratic function
    if is3DData
        positionCost = ( distanceX .^ 2 + distanceY .^ 2 + distanceZ .^ 2) .* (...
            individualFunctionPenalty / maxCellCenterDisplacement ^ 2 ) .* ...
            motionDependentPosCostCorrection;
    else
        positionCost = ( distanceX .^ 2 + distanceY .^ 2) .* (...
            individualFunctionPenalty / maxCellCenterDisplacement ^ 2 ) .* ...
            motionDependentPosCostCorrection;
    end
elseif displacementWeightingMode == 1 % linear function
    if is3DData
        positionCost = ( abs(distanceX)  + abs(distanceY) + abs(distanceZ) ) .* (...
            individualFunctionPenalty / maxCellCenterDisplacement ) .* ...
            motionDependentPosCostCorrection;
    else
        positionCost = ( abs(distanceX)  + abs(distanceY) ) .* (...
            individualFunctionPenalty / maxCellCenterDisplacement ) .* ...
            motionDependentPosCostCorrection;
    end
end
positionCost = [positionCost .* 1 repmat( individualFunctionPenalty, ro )];
positionCost = positionCost .* usedFunctionsForCostMatrix(1);

% Costs on area
areaCostMatrix = zeros( size( relativeAreaDifference ));
areaCostMatrix(relativeAreaDifference > ( 1 + averageCellSizeGrowth )) =  ...
    (- 1 *(maxCellSizeDecrease -( 1 + averageCellSizeGrowth ))) /...
    (maxCellSizeIncrease - ( 1 + averageCellSizeGrowth )).*...
    (relativeAreaDifference(relativeAreaDifference > ( 1 + ...
    averageCellSizeGrowth)) - ( 1 + averageCellSizeGrowth));
areaCostMatrix(relativeAreaDifference<=(1+averageCellSizeGrowth)) = ...
    -1 * (relativeAreaDifference(relativeAreaDifference <= ( 1 + ...
    averageCellSizeGrowth)) - ( 1 + averageCellSizeGrowth));
factor = individualFunctionPenalty / ( -1 * (maxCellSizeDecrease(...
    maxCellSizeDecrease <= ( 1 + averageCellSizeGrowth)) - ( 1 + ...
    averageCellSizeGrowth))) .^2;
areaCostMatrix = (areaCostMatrix .^2).* factor;
areaCost = [areaCostMatrix repmat(individualFunctionPenalty, ro)];
areaCost = areaCost .* usedFunctionsForCostMatrix(2);

% Cost for cell rotation
rotationCost = (sin(pi / 180 * orientationDifference )) .^2;
if maxMajorAxisRotation ~= 0
    rotationCost = rotationCost .* individualFunctionPenalty / (sin(pi / 180 ...
        .* maxMajorAxisRotation)) .^2;
end
rotationCost = [rotationCost .*1 repmat(individualFunctionPenalty, ro)];
rotationCost = rotationCost .* usedFunctionsForCostMatrix(3);

if maxTrackFrameSkipping == 0
    frameSkippingWeight = individualFunctionPenalty;
else
    frameSkippingWeight = individualFunctionPenalty / ...
        maxTrackFrameSkipping;
end
frameSkippingCost = frameSkippingWeight * repmat(oldFrame.track_age - 1, ...
    1, cn);
frameSkippingCost = [frameSkippingCost .*1 repmat(individualFunctionPenalty, ro)];
frameSkippingCost = frameSkippingCost .* usedFunctionsForCostMatrix(4);

% Sum of individual costs
totalCost = positionCost + areaCost + rotationCost + frameSkippingCost;

% LAPJV is faster with integers that float.
totalCost = round(totalCost);
end