%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ weightedTotalCosts, weightedDisplacementCost, weightedAreaCost] = ...
    computeNeighbourhoodMotionCostMatrix(~, oldFrame, newFrame, ...
    displacementOfAssignedTracks, maxExpectedMovement, cellCloseNeighbours, ...
    individualFunctionPenalty, maxCellSizeDecrease, averageCellSizeGrowth, ...
    maxCellSizeIncrease)
% COMPUTENEIGHBOURHOODMOTIONCOSTMATRIX Computes the individual
% assignement costs for spatial (position), size (area) differences
% and returns the matrices for each weighted cost component individually
% (weightedPositionCost, weightedAreaCost, weightedTotalCost). For
% computing the individual spatial differences, the individual
% neighbourhood is considered and the indivudial spatial differeces are
% compenstated by the minimal difference of its neighbourhood. For the
% area weight an averageCellSizeGrowth is assumed and the maximal cell
% size decrease and increase can be specified individually.
%
% Args:
%    ignoredArg (:obj:`object`):                       :class:`LAP` instance.
%    oldFrame (:obj:`object`):                         oldFrame :class:`TemporaryTrackingData` instance.
%    newFrame (:obj:`object`):                         newFrame :class:`TemporaryTrackingData` instance.
%    displacementOfAssignedTracks (:obj:`array`, Kx3): Displacement of assigned tracks in x,y,z
%    maxExpectedMovement (:obj:`float`):               Maximum expected segmented centroid movement
%    cellCloseNeighbours (:obj:`array`):               Array of cell close neighbours
%    individualFunctionPenalty (:obj:`int` 1x1):       Individual function penalty
%    maxCellSizeDecrease (:obj:`float` 1x1):           Maximal allowed frame 
%                                                      to frame cell size 
%                                                      decrease as fraction.
%    averageCellSizeGrowth (:obj:`float` 1x1):         Average allowed frame 
%                                                      to frame cell size growth
%                                                      as fraction.
%    maxCellSizeIncrease (:obj:`float` 1x1):           Maximal allowed frame 
%                                                      to frame cell size 
%                                                      increase as fraction.
% Returns
% -------
%    weightedTotalCosts: :obj:`float` nxm             
%                                                      Costmatrix with the sum
%                                                      of cost matrices weightedpositionCost
%                                                      to weightedAreaCost
%    weightedPositionCost: :obj:`float` nxm            
%                                                      Weighted cost matrix
%                                                      for spatial differences (position).
%    weightedAreaCost: :obj:`float` nxm           
%                                                      Weighted cost matrix
%                                                      for size differences (area).
%
% :Authors:
%    Ricard Delgado-Gonzalo - initial description of neighbourhood preserved motion https://doi.org/10.1109/ISBI.2010.5490288
% :Authors:
%    Cristian Vesari - Implementation of idea above in MATLAB http://dx.doi.org/10.1098/rsif.2016.0705
% :Authors:
%    Andreas P. Cuny - Neighbourhood preserved motion implementation for TracX.

% Determine dimension of cost matrix
nOldFrame = size(oldFrame.cell_center_x, 1);
nNewFrame = size(newFrame.cell_center_x, 1);

% Define max allowed cost
maxCostMatrix = zeros(nOldFrame, nOldFrame);
maxCostMatrix(:) = (3 * individualFunctionPenalty);

% Calculate the absolute difference in x - y pixel position
distanceX = repmat(newFrame.cell_center_x.', ...
    nOldFrame, 1) - repmat(oldFrame.cell_center_x, 1, nNewFrame);
distanceY =  repmat(newFrame.cell_center_y.', ...
    nOldFrame, 1) - repmat(oldFrame.cell_center_y, 1, nNewFrame);
distanceZ =  repmat(newFrame.cell_center_z.', ...
    nOldFrame, 1) - repmat(oldFrame.cell_center_z, 1, nNewFrame);

% Calculated a refined displacement corrected by the displacement of
% correctly assigned neighbouring cells
refinedDisplacement = nan(size(distanceX));
for iRow = 1:size(refinedDisplacement, 1)
    distXAssignments = displacementOfAssignedTracks(...
        cellCloseNeighbours(iRow,:), 1);
    distYAssignments = displacementOfAssignedTracks(...
        cellCloseNeighbours(iRow,:), 2);
    distZAssignments = displacementOfAssignedTracks(...
        cellCloseNeighbours(iRow,:), 3);
    if ~isempty(distXAssignments)
        resX = bsxfun(@minus, distanceX(iRow,:), distXAssignments);
        resY = bsxfun(@minus, distanceY(iRow,:), distYAssignments);
        resZ = bsxfun(@minus, distanceZ(iRow,:), distZAssignments);
        distancesX = resX.^2;
        distancesY = resY.^2;
        distancesZ = resZ.^2;
        distances = sqrt(distancesX + distancesY + distancesZ);
        if size(distances, 1) == 1
            refinedDisplacement(iRow, :) = distances / ...
                (maxExpectedMovement); % ~ 2.5 times the average cell diameter
        else
            refinedDisplacement(iRow, :) = sum(distances)/size(distances, 1) / ...
                (maxExpectedMovement); % ~ 2.5 times the average cell diameter
        end
    else
        resX = distanceX(iRow,:);
        resY = distanceY(iRow,:);
        resZ = distanceZ(iRow,:);
        distancesX = resX.^2;
        distancesY = resY.^2;
        distancesZ = resZ.^2;
        distances = sqrt(distancesX + distancesY + distancesZ);
        refinedDisplacement(iRow, :) = distances / ...
            (maxExpectedMovement); % ~ 2.5 times the average cell diameter
    end
end

% Compute area change
areaNewFrame = repmat(newFrame.cell_area.',nOldFrame, 1);
areaOldFrame = repmat(oldFrame.cell_area, 1, nNewFrame);

areaChange = 1 - min(areaOldFrame, areaNewFrame) ./ ...
    max(areaOldFrame, areaNewFrame);

% Weight on position change
posWeight = individualFunctionPenalty; 

% Weight on area change
areaChange = areaChange + 1;
areaWeight = zeros(size(areaChange));
areaWeight(areaChange > (1 + averageCellSizeGrowth)) =  ...
    (-1 * (maxCellSizeDecrease - ( 1 + averageCellSizeGrowth))) /...
    (maxCellSizeIncrease - ( 1 + averageCellSizeGrowth)).*...
    (areaChange(areaChange > ( 1 + averageCellSizeGrowth)) - ( 1 + ...
    averageCellSizeGrowth));
areaWeight(areaChange<=(1+averageCellSizeGrowth)) = ...
    -1 * (areaChange(areaChange <= (1 + averageCellSizeGrowth)) ...
    - (1 + averageCellSizeGrowth));
factor = individualFunctionPenalty / (-1* (maxCellSizeDecrease( ...
    maxCellSizeDecrease <= (1+ averageCellSizeGrowth))- (1+ ...
    averageCellSizeGrowth))).^2;
areaWeight = (areaWeight.^2).* factor;

% Apply weights on position and area change
weightedDisplacementCost = refinedDisplacement .* posWeight;
weightedAreaCost = areaChange .* areaWeight;

costMatrix = weightedDisplacementCost + weightedAreaCost;
weightedTotalCosts = [costMatrix, maxCostMatrix];

end