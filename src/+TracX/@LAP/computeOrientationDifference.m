%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function result = computeOrientationDifference(~, a, b)
% COMPUTEORIENTATIONDIFFERENCE Calculates the central angle between two 
% orientation vectors
%
% Args:
%    ignoredArg (:obj:`object`): :class:`LAP` instance.
%    a (:obj:`array`):           Orientation vector a
%    b (:obj:`array`):           Orientation vector b
%
% Returns
% -------
%    result: :obj:`double`
%                                Resulting orientation difference
%
% :Authors:
%    Thomas Kuendig - initial implementation

dummyA = [a(1,1,1), a(1,1,2), a(1,1,3)];
dummyB = [b(1,1,1), b(1,1,2), b(1,1,3)];
magnitudeOfCrossProduct = norm(cross(dummyA, dummyB));
dotProduct = dot(dummyA, dummyB);
result = abs(atan(magnitudeOfCrossProduct / dotProduct)*180/pi);

end