%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function resultMatrix = computeOrientationDifferenceMatrix(this, A, B)
% COMPUTEORIENTATIONDIFFERENCEMATRIX Computes the orientation difference
% matrix.
%
% Args:
%    this (:obj:`object`):       :class:`LAP` instance.
%    A (:obj:`array`):           Orientation matrix A
%    B (:obj:`array`):           Orientation matrix B
%
% Returns
% -------
%    resultMatrix: :obj:`double`
%                                Resulting matrix
%
% :Authors:
%    Thomas Kuendig - initial implementation

assert(isequal(size(A), size(B)), "Matrices not the same size")
resultMatrix = zeros(size(A,1), size(A,2));

for i = 1:size(A, 1)
    for j = 1:size(A, 2)
        resultMatrix(i, j) = this.computeOrientationDifference(A(i,j,:), ...
            B(i,j,:));
    end
end
end