%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ assignmentRowIdx, assignmentColIdx, assignmentLocIdx, ...
    assignmentCosts, varargout] = solveLAP(this, costMatrix, varargin )
% SOLVELAP Computes linear assignment and returns the assignments
% for the row and columns of the costMatrix (i.e. oldFrame and newFrame)
% and the linear assignement location indicies of the costMatrix
% directly.
%
% Args:
%    this (:obj:`object`):             :class:`LAP` instance.
%    costMatrix (:obj:`float`, IxJ):   costMatrix with the total cost of
%                                      all individual weighting functions.
%    varargin (:obj:`float`, IxJ):     (Optional) individual weighted 
%                                      costs matrix for i.e. position, area.
%
% Returns
% -------
%    assignmentRowIdx: :obj:`int` Ix1
%                                       Optimal row indicies
%    assignmentColIdx: :obj:`int` Ix1
%                                       Optimal column indicies
%    assignmentLocIdx: :obj:`int` Ix1 
%                                       Linear indicies for optimal row
%                                       and column indicies
%    assignmentCosts :obj:`array` IxJ   
%                                       Costs of the final assignment
%    varargout :obj:`int`:              
%                                       An array for each varargin with the 
%                                       assignmentLocIdx applied of same
%                                       size.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

NRow = size(costMatrix, 1);
NCol = size(costMatrix, 2) - NRow;
assignment = this.lapjv(costMatrix);

assignmentRowIdx = 1:NRow;
assignmentColIdx = assignment;
assignmentLocIdx = sub2ind(size(costMatrix), assignmentRowIdx, ...
    assignmentColIdx);

validAssignmentsLocIdx = ismember(assignment, 1:NCol);

assignmentCosts = nan(1, NCol);
assignmentCostsTemp = costMatrix(assignmentLocIdx); % We get them sorted in
% the order of the rows but we need them in the order of the columns.
assignmentCosts(assignmentColIdx(validAssignmentsLocIdx)) = ...
    assignmentCostsTemp(validAssignmentsLocIdx);

varargout = cell(1, size(varargin, 2));
try
    for iArgIn = 1:size(varargin, 2)
        assignmentCostsVarargout = nan(1, NCol);
        individualWeightCosts = varargin{iArgIn};
        assignmentCostsTemp = individualWeightCosts(assignmentLocIdx);
        assignmentCostsVarargout(assignmentColIdx(validAssignmentsLocIdx)) ...
            = assignmentCostsTemp(validAssignmentsLocIdx);
        varargout{iArgIn} = assignmentCostsVarargout;
    end
catch ME
    error(ME.message)
end
end