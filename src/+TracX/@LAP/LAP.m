%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef LAP
    % LAP class to create cost matricies and solve linear assignment problem.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
    end
    
    methods
        
        % Constructor
        function obj = LAP()
            % LAP Constructs a :class:`TemporaryTrackingData` object to create cost matricies and
            % solve linear assignment problem.
            %
            % Returns
            % -------
            %    obj: :obj:`object`         Returns a :class:`TemporaryTrackingData` instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
        end
        
        [ rowsol, cost, v, u, costMat ] = lapjv(costMat,resolution)
        % LAPJV Jonker-Volgenant Algorithm for Linear Assignment
        %  Problem.
        
        [ assignmentRowIdx, assignmentColIdx, assignmentLocIdx, ...
            assignmentCosts, varargout ] = solveLAP(this, costMatrix, ...
            varargin )
        % SOLVELAP Computes linear assignment and returns the
        %  assignments for the row and columns of the costMatrix (i.e.
        %  oldFrame and newFrame) and the linear assignement location
        %  indicies of the costMatrix directly.
        
        [ totalCost, positionCost, areaCost, rotationCost, ...
            frameSkippingCost ] = computeAssignmentCostMatrix(this, ...
            oldFrame, newFrame, maxCellCenterDisplacement, ...
            maxMajorAxisRotation, individualFunctionPenalty, ...
            maxCellSizeDecrease, averageCellSizeGrowth, maxCellSizeIncrease, ...
            usedFunctionsForCostMatrix, meanCellDiameter, ...
            maxTrackFrameSkipping, displacementWeightingMode, data3D)
        % COMPUTEASSIGNMENTCOSTMATRIX computes the individual
        %  assignement costs for spatial (position), size (area) and
        %  rotation (orientation) differences as well as frame skipping
        %  and returns the matrices for each cost individually (positionCost:
        %  frameSkippingCost) as well as the sum of all costs (totalCost).
        
        [ weightedTotalCosts, weightedDisplacementCost, weightedAreaCost ] = ...
            computeNeighbourhoodMotionCostMatrix(this, oldFrame, newFrame, ...
            displacementOfAssignedTracks, distToNeighbourhood, cellCloseNeighbours, ...
            individualFunctionPenalty, maxCellSizeDecrease, averageCellSizeGrowth, ...
            maxCellSizeIncrease)
        % COMPUTENEIGHBOURHOODMOTIONCOSTMATRIX Computes the individual
        %  assignement costs for spatial (position), size (area) differences
        %  and returns the matrices for each weighted cost component individually
        %  (weightedPositionCost, weightedAreaCost, weightedTotalCost). For
        %  computing the individual spatial differences, the individual
        %  neighbourhood is considered and the indivudial spatial differeces are
        %  compenstated by the minimal difference of its neighbourhood. For the
        %  area weight an averageCellSizeGrowth is assumed and the maximal cell
        %  size decrease and increase can be specified individually.
        
        [ costMatrix ] = fingperprintCostMatrixRefinement(this, newFrame, ...
            oldFrame, costMatrix, fingerprintDistThreshold)
        % FINGERPRINTCOSTMATRIXREFINEMENT Refines the cost matrix by setting
        %  costs for confirmed/trusted assignments (below fingerprint distance
        %  threshold) to 0 and detection of invalid costMatrix rows (all costs
        %  above max individual function penalty.
        
        [ newTrackIndex, assignmentTotalCost, assignmentPositionCost, ...
            assignmentAreaCost, assignmentRotationCost, ...
            assignmentFrameSkippingCost] = computeNewFrameAssignment( this, ...
            oldFrame, newFrame, maxCellCenterDisplacement, ...
            maxMajorAxisRotation, maxTrackFrameSkipping, ...
            individualFunctionPenalty, maxCellSizeDecrease, ...
            averageCellSizeGrowth, maxCellSizeIncrease, ...
            usedFunctionsForCostMatrix, fingerprintDistThreshold, ...
            meanCellDiameter, data3D)
        % COMPUTENEWFRAMEASSIGNMENT Computes linear assignment and returns
        %  returns track indicies for the subsequent frame ('newFrame').
        
        [ vec ] = orientationDecompressor (~, tag)
        % ORIENTATIONDECOMPRESSOR Decompresses the linearized orientation vector to
        % its 3D format.
        
        result = computeOrientationDifference(~, a, b)
        % COMPUTEORIENTATIONDIFFERENCE Calculates the central angle between two
        % orientation vectors
        
        resultMatrix = computeOrientationDifferenceMatrix(~, A, B)
        % COMPUTEORIENTATIONDIFFERENCEMATRIX Computes the orientation difference
        % matrix.
        
        [ cost ] = mammalianLineageCost(~, oldFrameData, newFrameData, ...
            daughterpairs, parents, daughterpairtypes, parenttypes)
        % MAMMALIANLINEAGECOST Cost matrix function for mammalian lineage
        % reconstruction in 2 & 3D.
        
        [totalCost, positionCost, areaCost, rotationCost, ...
            frameSkippingCost] = computeFFPCostMatrix(this, ...
            oldFrame, newFrame, maxCellCenterDisplacement, ...
            maxMajorAxisRotation, individualFunctionPenalty, maxCellSizeDecrease, ...
            averageCellSizeGrowth, maxCellSizeIncrease, ...
            usedFunctionsForCostMatrix, meanCellDiameter, ...
            maxTrackFrameSkipping, displacementWeightingMode, is3DData)
        % COMPUTEFFPCOSTMATRIX computes the individual
        % assignement costs for an FFP based tracker based on spatial (position),
        % differences and returns the matrices for each cost individually (positionCost:
        % frameSkippingCost) as well as the sum of all costs (totalCost) to keep the
        % existing structure.
        
    end
end