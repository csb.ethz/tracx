%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [rowsol,cost,v,u,costMat] = lapjv(~, costMat, varargin )
% LAPJV  Wrapper around the Jonker-Volgenant Algorithm for Linear 
% Assignment Problem. Returns the optimal column indices, ROWSOL, 
% assigned to row in solution, and the minimum COST based on the 
% assignment problem represented by the COSTMAT, where the (i,j)th 
% element represents the cost to assign the jth job to the ith worker.
%
% Args:
%    ignoredArg (:obj:`object`):  :class:`LAP` instance.
%    costMat (:obj:`array`, IxJ): [IxJ, Float] Cost matrix, where the (i,j)th element
%                                 represents the cost to assign the  jth
%                                 job to the ith worker.
%    varargin (:obj:`int`, 1x1):  'resolution' (Optional) input can be used
%                                 to define data resolution to accelerate 
%                                 speed.
%
% Returns
% -------
%    rowsol: :obj:`array` 
%                                 The optimal column indices
%    cost: :obj:`array`
%                                 Minimum COST based on the assignment
%                                 problem
%    v: :obj:`array`                
%                                 Dual variables, column reduction numbers.
%    u:  :obj:`array`                
%                                 Dual variables, row reduction numbers.
%    costMat: :obj:`array`          
%                                 The reduced cost matrix.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if isempty(varargin)
    maxcost = min(1e16,max(max(costMat)));
    resolution = eps(maxcost);
else
    resolution = varargin{1};
end

[rowsol,cost,v,u,costMat] = TracX.ExternalDependencies.lapjv(costMat, ...
    resolution);

end