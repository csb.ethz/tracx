%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [totalCost, positionCost, areaCost, rotationCost, ...
    frameSkippingCost] = computeFFPCostMatrix(~, ...
    oldFrame, newFrame, individualFunctionPenalty, radius, dCRFM)
% COMPUTEFFPCOSTMATRIX computes the individual
% assignement costs for an FFP based tracker based on spatial (position),
% differences and returns the matrices for each cost individually (positionCost:
% frameSkippingCost) as well as the sum of all costs (totalCost) to keep the 
% existing structure.
%
% Args:
%    ignoredArg (:obj:`object`):                    :class:`LAP` instance.
%    oldFrame (:obj:`object`):                      oldFrame :class:`TemporaryTrackingData` instance.
%    newFrame (:obj:`object`):                      newFrame :class:`TemporaryTrackingData` instance.
%    individualFunctionPenalty (:obj:`int` 1x1):    Individual function penalty
%    radius (:obj:`int` 1x1):                       Radius for
%                                                   neighbourhood
%    dCRFM (:obj:`array` NxM):                      Cell region fingerprint
%                                                   distances.
%
% Returns
% -------
%    totalCost: :obj:`float` IxJ
%                                                Costmatrix with the sum
%                                                of cost matricies
%    positionCost: :obj:`float` IxJ
%                                                Costmatrix for dCRF
%                                                
%    areaCost: :obj:`float` IxJ
%                                                Costmatrix placeholder
%    rotationCost: :obj:`float` IxJ
%                                                Costmatrix placeholder
%    frameSkippingCost: :obj:`float` IxJ
%                                                Costmatrix placeholder
%
% :Authors:
%    Andreas P. Cuny - Initial implementation
%

% Determine dimension of cost matrix
ro = length(oldFrame.cell_area); % dim of rows
cn = length(newFrame.cell_area); % dim of cols
individualFunctionPenalty = 1.0196;

% Construct of the different costs
% dCRFM
positionCost = [dCRFM .*1 repmat(individualFunctionPenalty, ro, cn)];
positionCost = positionCost .* 1;
% Set to 0 but return matrices to not break logic
areaCost = [repmat(individualFunctionPenalty, ro, cn) repmat(individualFunctionPenalty, ro, cn)];
areaCost = areaCost .* 0;
rotationCost = areaCost;
frameSkippingCost = areaCost;

% Sum of individual costs
totalCost = positionCost + areaCost + rotationCost + frameSkippingCost;

% LAPJV is faster with integers that float. 
totalCost = totalCost; %  round(totalCost);

end
