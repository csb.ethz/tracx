%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [trackIdxToChange] = moveToVeryOld(this, selectedTrackArray, ...
    neighbourhoodSearchRadius)
% MOVETOVERYOLD Moves temporary data stored in OldFrame to
% VeryOld.
%
% Args:
%    this (:obj:`object`):                   :class:`LineageData` instance
%    selectedTrackArray (:obj:`array`, kx1): Array of selected track indicies to
%                                            move.
%    neighbourhoodSearchRadius (:obj:`int`): Radius in pixel within an other
%                                            point defined by the coordinate
%                                            arrays x and y is considered to
%                                            be a neighbour of (x_i,y_i).
%                                            :class:`~+TracX.@TrackerParameter.TrackerParameter.neighbourhoodSearchRadius` 
%
% Returns
% -------
%    trackIdxToChange :obj:`array` NxM                               
%                                           Array of track idndicies to change.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Remove Track(s) that are in VeryOld allready
selectedTrackArray(ismember(selectedTrackArray, this.VeryOld.track_index)) = [];
trackIdxToChange = nan(numel(selectedTrackArray), 1);
initialTracksInVeryOld = this.VeryOld.track_index;

% Move selected tracks from NewFrame to VeryOld
% object
structNameArray = fieldnames(this.NewFrame);
iIteration = 1;
while iIteration <= numel(structNameArray)
    for i = 1:numel(selectedTrackArray)
        if size(this.NewFrame.(structNameArray{iIteration}),2) == 1
            if ~strcmp(structNameArray{iIteration}, 'track_index')
                this.VeryOld.(structNameArray{iIteration}) = [this.VeryOld.(structNameArray{iIteration});...
                    this.NewFrame.(structNameArray{iIteration})(this.NewFrame.track_index == selectedTrackArray(i))];
            else
                this.VeryOld.(structNameArray{iIteration}) = [...
                    this.VeryOld.(structNameArray{iIteration}); this.numberOfTracks];
                trackIdxToChange(i) = this.numberOfTracks;
                this.numberOfTracks = this.numberOfTracks + 1;
            end
        else
            %> @todo check why NewFrame is sometimes empty for
            %the neighbourhood where there clearly should be
            %one.
            if ~isempty(this.NewFrame.(structNameArray{iIteration}))
                this.VeryOld.(structNameArray{iIteration}) = [this.VeryOld.(structNameArray{iIteration});...
                    this.NewFrame.(structNameArray{iIteration})(this.NewFrame.track_index == selectedTrackArray(i), :)];
            end
        end
    end
    iIteration = iIteration+1;
end

structNameArray = fieldnames(this.OldFrame);
iIteration=1;
while iIteration <= numel(structNameArray)
    for i = 1:numel(selectedTrackArray)
        if size(this.OldFrame.(structNameArray{iIteration}),2) == 1
            if ~strcmp(structNameArray{iIteration}, 'track_index')
                this.VeryOld.(structNameArray{iIteration}) = [this.VeryOld.(structNameArray{iIteration});...
                    this.OldFrame.(structNameArray{iIteration})(this.OldFrame.track_index == selectedTrackArray(i))];
            else
                this.VeryOld.(structNameArray{iIteration}) = [...
                    this.VeryOld.(structNameArray{iIteration}); this.OldFrame.track_index(this.OldFrame.track_index == selectedTrackArray(i))];
            end
        else
            %> @todo: Recompute neighbourhood for cell frome
            %> OldFrame being in NewFrame. Does potentially
            %> overlap with assignment from NewFrame but does
            %> not have to be correct
            coordinateArray = [ [this.OldFrame.cell_center_x(this.OldFrame.track_index == selectedTrackArray(i)); ...
                this.NewFrame.cell_center_x], ...
                [this.OldFrame.cell_center_y(this.OldFrame.track_index == selectedTrackArray(i));...
                this.NewFrame.cell_center_y]];
            neighbourMatrix = this.getCloseCellNeighbourIdx(...
                coordinateArray(:,1), coordinateArray(:,2), neighbourhoodSearchRadius);
            
%             this.VeryOld.(structNameArray{iIteration}) = [this.VeryOld.(structNameArray{iIteration});...
%                 neighbourMatrix(1, 2:end)];
        end
    end
    iIteration = iIteration+1;
end
% Increase age by 2 for newly added tracks in VeryOld
this.VeryOld.track_age(~ismember(this.VeryOld.track_index,initialTracksInVeryOld)) = ...
    this.VeryOld.track_age(~ismember(this.VeryOld.track_index,initialTracksInVeryOld)) + 2;
% Remove selected tracks from NEWFRAME Object
%> @todo: Remove object actually from new frame and also move it
%> there to the veryold frame
iIteration = 1;
removeInNewFrameIdx = ismember(this.NewFrame.track_index, selectedTrackArray);
while iIteration <= numel(structNameArray)
    if size(this.NewFrame.(structNameArray{iIteration}),2) == 1
        this.NewFrame.(structNameArray{iIteration})(removeInNewFrameIdx) = [];
    else
        this.NewFrame.(structNameArray{iIteration})(removeInNewFrameIdx, :) = [];
        this.NewFrame.(structNameArray{iIteration})(:, removeInNewFrameIdx) = [];
    end
    
    iIteration = iIteration+1;
end
end