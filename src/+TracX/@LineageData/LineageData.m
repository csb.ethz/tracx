%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef LineageData < handle
    % LINEAGEDATA Creates an object to store data generated during
    % lineage reconstruction such as i.e. segmentation and quantification of
    % bud neck marker Myo1 in S. cerevisiae.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties (SetAccess=private)
        
        uuid % Primary key (unique identifier for each object).
        
    end
    
    properties
        
        
        obj_track_index % Track index of obj
        
        obj_track_age % Track age of each obj
       
        obj_track_fingerprint_real = {}  % Track fingerprint real
                
        track_fingerprint_real_distance % Track fingerprint read distance
        
        obj_dif_x % old to newFrame displacement in x.
        obj_dif_y % old to newFrame displacement in y.
        obj_dif_z % old to newFrame displacement in z.
        obj_filtered_dif_x % Filtered old to newFrame displacement in x.
        obj_filtered_dif_y % Filtered old to newFrame displacement in x.
        obj_filtered_dif_z % Filtered old to newFrame displacement in x.
       
        obj_frame  % Image frame number of segmented object.
        
        obj_channel_id % Image channel id of segmented object.
        
        obj_index % Unique segmentation object label for each frame.
       
        obj_parent_seg_mask_id % Cell segmentation mask id overlaping with object.
        
        obj_parent_uuid % UUID of parent cell.
        
        obj_parent_track_index % Trackindex of parent cell.
        
        obj_center_x % X component of segmented object center.
        
        obj_center_y % Y component of segmented object center.
        
        obj_center_z % Z component of segmented object center.
        
        obj_major_axis % Major axis of segmented object.
        
        obj_minor_axis % Minor axis of segmented object.
        
        obj_area % Area of segmented object.
        
        obj_orientation % Orientation of segmented object.
        
        obj_perimeter % Perimeter of segmented object.
        
    
        obj_fluo_total % Total denoised signal of segmented object.
    
        obj_fluo_mean % Mean denoised signal of segmented object.
        
        obj_parent_fluo_total % Total denoised signal of parent cell of segmented object (i.e. background).
        
        obj_parent_fluo_mean % Mean denoised signal of parent cell of segmented object (i.e. background).
        
        
        obj_pixelIdxList % Pixel indicies list of segmented object {obj1:obj_n, chl_1:chl_n}.
       
        obj_fluoImgDenoised  % Denoised image of obj_channel_id {obj1:obj_n, chl_1:chl_n}.
        
        obj_close_neighbour % Neighbourhood of each object
        
        
        OldFrame = TracX.TemporaryTrackingData(); % OldFrame to keep obj frame properties for tracking
        
        NewFrame = TracX.TemporaryTrackingData(); % NewFrame to keep obj frame properties for tracking
        
        VeryOld = TracX.TemporaryTrackingData(); % VeryOld to keep obj frame properties for tracking
        
        numberOfTracks % Max number of tracks
        
    end
    
    methods
        
        function this = setObjParent(this, daughters, mothers, motherCandidates, frameOccurance)
            % SETOBJPARENT Sets the parent (cell) of the current segmented
            % object (budneck marker signal).
            %
            % Args:
            %    this (:obj:`object`):                   :class:`LineageData` instance
            %    daughters (:obj:`array`):               Array of Track indices
            %                                            for the daughters.
            %    mothers (:obj:`array`):                 Array of Track indices
            %                                            for the mothers.   
            %    motherCandidates (:obj:`array`):        Array of Track indices
            %                                            for the mother candidates.
            %    frameOccurance (:obj:`array`):          Array of frame
            %                                            numbers for mother 
            %                                            daughter pairs.
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                 :class:`LineageData` instance.

            this.obj_parent_track_index = nan(numel(this.uuid), 1);
            
            for k = 1:numel(daughters)
                disp(k)
                currDaughter = daughters(k);
                currMother = mothers(k);
                if currMother ~=0
                    idx = ismember(motherCandidates{k}, currMother);
                    assignedFrames = frameOccurance{k}(idx);
                    maxTrustedFrameIdx = find(diff(assignedFrames) > 2, 1, 'first');
                    if isempty(maxTrustedFrameIdx)
                        maxTrustedFrameIdx = numel(assignedFrames);
                    end
                    trustedFrames = assignedFrames(1:maxTrustedFrameIdx);
                    this = this.evaluateObjSigAroundTrustedFrames(trustedFrames, currDaughter, currMother);
                end
            end
            
            % motherCanditates and frameOccurance is rather imprecise and
            % other later signals may be included. Use a rather strict
            % approach to trust only subsequent image frames with at laeast
            % a two frame gap.
            
            %
            %             frameIdx = ismember(this.obj_frame, trustedFrames);
            %             matchingMothersIdx = ismember(this.obj_parent_seg_mask_id, currMother);
            %             matchingDaughtersIdx = ismember(this.obj_parent_seg_mask_id, currDaughter);
            %             cmbIdx = or(and(frameIdx, matchingMothersIdx), and(frameIdx, matchingDaughtersIdx));
            %             this.obj_parent_seg_mask_id(cmbIdx) % could be either id of daughter or mother;
            %             figure;
            %             plot(this.obj_center_x(cmbIdx), this.obj_center_y(cmbIdx), 'x')
            %             figure; plot(this.obj_fluo_mean(cmbIdx))
            %             x = this.obj_center_x(cmbIdx);
            %             y = this.obj_center_y(cmbIdx);
        end
        
        function this = evaluateObjSigAroundTrustedFrames(this, trustedFrames, currDaughter, currMother)
            % EVALUATEOBJSIGAROUNDTRUSTEDFRAMES Evaluates the signal of the
            % detected object around the trusted frames of mother daughter
            % candidate pairs.
            %
            % Args:
            %    this (:obj:`object`):                   :class:`LineageData` instance
            %    trustedFrames (:obj:`array`, kx1):      Array of trusted
            %                                            frames.
            %    currDaughter (:obj:`int`):              Track index of
            %                                            current daughter.
            %    currMother (:obj:`int`):                Track index of
            %                                            current mother.                             
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                 :class:`LineageData` instance.
            
            % Note we could extend this fuction in case of no match, such
            % that it uses last known mask to quantify the the intensity in
            % case of an absent signal.
            trustedFrames = unique(trustedFrames);
            objFrameIdx = ismember(this.obj_frame, trustedFrames);
            matchingMothersIdx = ismember(this.obj_parent_seg_mask_id, currMother);
            matchingDaughtersIdx = ismember(this.obj_parent_seg_mask_id, currDaughter);
            cmbIdx = or(and(objFrameIdx, matchingMothersIdx), and(objFrameIdx, matchingDaughtersIdx));
            xObj = this.obj_center_x(cmbIdx);
            yObj = this.obj_center_y(cmbIdx);
            idObj = this.uuid(cmbIdx);
            ds = squareform(pdist([xObj, yObj]));
            if isempty(ds)
                dis = 1;
            else
                dis = [0, diff(ds(1,:))];
            end
            xObj = xObj(dis < 8); %> @todo solve hardcoded threshold such that one can adjust for different magnification cell cellsize.
            yObj = yObj(dis < 8);
            idObj = idObj(dis < 8);
            % Evaluate proceeding frames
            evalLower = 1;
            k = 1;
            while evalLower
                if min(trustedFrames) - k >= 1
                    xEval = this.obj_center_x(ismember(this.obj_frame, ...
                        min(trustedFrames) - k));
                    yEval = this.obj_center_y(ismember(this.obj_frame, ...
                        min(trustedFrames) - k));
                    idEval = this.uuid(ismember(this.obj_frame, ...
                        min(trustedFrames) - k));
                    dist = pdist2([xObj(1),yObj(1)], [xEval, yEval]);
                    if ~isempty(dist) && min(dist) < 5 % Fix the size threshold
                        % determine if in close neighbourhood. (determine
                        % dynamically as max. or do it for whole experiment to
                        % get a 'mean' value -> bad for outlier. what to do
                        % with single events?
                        % determine min dist add it id, coords to trusted frame
                        minDist = min(dist);
                        minDistIdx = ismember(dist, min(dist));
                        
                        trustedFrames = [min(trustedFrames) - 1, trustedFrames];
                        idObj = [idEval(min(dist) == dist), idObj];
                        xObj = [xEval(minDistIdx) ; xObj];
                        yObj = [yEval(minDistIdx) ; yObj];
                    else
                        % Evaluate around potential segmentation gaps
                        k = k + 1;
                        if k > 2
                            evalLower = 0;
                        end
                    end
                else
                    evalLower = 0;
                end
            end
            
            % Evaluate follwing frames
            evalUpper = 1;
            k = 1;
            while evalUpper
                if max(trustedFrames) + k <= max(this.obj_frame)
                    xEval = this.obj_center_x(ismember(this.obj_frame, ...
                        max(trustedFrames) + k));
                    yEval = this.obj_center_y(ismember(this.obj_frame, ...
                        max(trustedFrames) + k));
                    idEval = this.uuid(ismember(this.obj_frame, ...
                        max(trustedFrames) + k));
                    dist = pdist2([xObj(end), yObj(end)], [xEval, yEval]);
                    if ~isempty(dist) && min(dist) < 5 % Fix the size threshold
                        % determine if in close neighbourhood. (determine
                        % dynamically as max. or do it for whole experiment to
                        % get a 'mean' value -> bad for outlier. what to do
                        % with single events?
                        % determine min dist add it id, coords to trusted frame
                        minDist = min(dist);
                        minDistIdx = ismember(dist, min(dist));
                        
                        trustedFrames(end + 1) = max(trustedFrames) + 1;
                        idObj(end + 1) = idEval(min(dist) == dist);
                        xObj(end + 1) = xEval(minDistIdx);
                        yObj(end + 1) = yEval(minDistIdx);
                    else
                        % Evaluate around potential segmentation gaps
                        k = k + 1;
                        if k > 2
                            evalUpper = 0;
                        end
                    end
                else
                    evalUpper = 0;
                end
            end
            % Evaluate missing frames
            %> @todo Fix array joining!
            contFrames = min(trustedFrames):max(trustedFrames);
            missingFrameIdx = ~ismember(contFrames, trustedFrames);
            missingFrames = contFrames(missingFrameIdx);
            if missingFrameIdx ~= 0
                idObjTmp = nan(1, numel(contFrames));
                idObjTmp(~missingFrameIdx) = idObj;
                xObjTmp = nan(1, numel(contFrames));
                xObjTmp(~missingFrameIdx) = xObj;
                yObjTmp = nan(1, numel(contFrames));
                yObjTmp(~missingFrameIdx) = yObj;
                
                for missingFrame = missingFrames
                    xEval = this.obj_center_x(ismember(this.obj_frame, ...
                        missingFrame));
                    yEval = this.obj_center_y(ismember(this.obj_frame, ...
                        missingFrame));
                    idEval = this.uuid(ismember(this.obj_frame, ...
                        missingFrame));
                    dist = pdist2([xObjTmp(trustedFrames == missingFrame-1), yObjTmp(trustedFrames == missingFrame-1)], [xEval, yEval]);
                    if ~isempty(dist) && min(dist) < 5 % Fix the size threshold
                        % determine if in close neighbourhood. (determine
                        % dynamically as max. or do it for whole experiment to
                        % get a 'mean' value -> bad for outlier. what to do
                        % with single events?
                        % determine min dist add it id, coords to trusted frame
                        minDistIdx = ismember(dist, min(dist));
                        
                        idObjTmp(contFrames == missingFrame) = idEval(minDistIdx);
                        xObjTmp(contFrames == missingFrame) = xEval(minDistIdx);
                        yObjTmp(contFrames == missingFrame) = yEval(minDistIdx);
                    end
                end
                xObj = xObjTmp';
                yObj = yObjTmp';
                idObj = idObjTmp;
            end
            
            this.obj_parent_track_index(ismember(this.uuid, idObj)) = currMother;
            
        end
        
        function ret = getConditionalFieldArray(this, fieldName, ...
                fieldNameCond, Condition )
            % GETCONDITIONALFIELDARRAY Returns an array with
            % all the data for a given fieldName (SegmentationData property)
            % matching a given condition for the fieldNameCon
            % (SegmentationData property).
            %
            % Args:
            %    this (:obj:`object`):        :class:`LineageData` instance
            %    fieldName (:obj:`str`):      String with fieldname where to
            %                                 get values from.
            %    fieldNameCond (:obj:`str`):  String with fieldname to match
            %                                 the condition
            %    Condition (:obj:`array`): :  Array with condition to match the
            %                                 match with the fieldNameCond.
            %
            % Returns
            % -------
            %    ret :obj:`array` 
            %                                 Array of selected data.
            try
                ret = this.(fieldName)(ismember(...
                    this.(fieldNameCond), Condition));
            catch ME
                error([ME.message])
            end
        end
        
        function ret = getFieldArrayForFrame(this, fieldName, ...
                frameNumber )
            % GETFIELDARRAYFORFRAME Returns an array with
            % all the data for a given fieldName (LineageData property)
            % for a given frameNumber (image frame).
            %
            % Args:
            %    this (:obj:`object`):        :class:`LineageData` instance
            %    fieldName (:obj:`str`):      String with fieldname where to
            %                                 get values from.
            %    frameNumber (:obj:`int`):    Frame number for which the fieldName
            %                                 data should be returned.
            %
            % Returns
            % -------
            %    ret :obj:`array` 
            %                                 Array of selected data.
            try
                ret = this.(fieldName)(this.obj_frame == frameNumber);
            catch ME
                error([ME.message])
            end
        end
        
        function this = initializeOldFrame(this)
            % INITIALIZEOLDFRAME Initailizes the OldFrame property for
            % Lineage marker tracker with data from the LineageData object
            % for the first image frame (frameNumber i.e. 1).
            %
            % Args:
            %    this (:obj:`object`):        :class:`LineageData` instance
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                 :class:`LineageData` instance.
            
            ofl = length(this.getFieldArrayForFrame('obj_frame', 1 ));
            
            this.OldFrame.addData((1:ofl).', (1:ofl).', ones(ofl,1),...
                this.getFieldArrayForFrame('uuid', 1)', ...
                this.getFieldArrayForFrame('obj_center_x', 1), ...
                this.getFieldArrayForFrame('obj_center_y', 1), ...
                this.getFieldArrayForFrame('obj_center_y', 1), ...
                zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), ...
                zeros(ofl,1), zeros(ofl,1), ...
                this.getFieldArrayForFrame('obj_orientation', 1), ...
                this.getFieldArrayForFrame('obj_area', 1), ...
                this.obj_close_neighbour(1).Indicies, ...
                zeros(ofl,1), ...
                this.getFieldArrayForFrame(...
                'obj_track_fingerprint_real', 1)', ....
                zeros(ofl,1), zeros(ofl,1), zeros(ofl,1));
            
        end
        
        function this = initializeNewFrame(this, frameNumber)
            % INITIALIZENEWFRAME Initailizes the NewFrame property of the
            % Tracker with data from the Tracker.Data object for the current
            % image frame (frameNumber).
            %
            % Args:
            %    this (:obj:`object`):        :class:`LineageData` instance
            %    frameNumber (:obj:`int`):    Frame number of subsequent image
            %                                 frame.
            %
            % Returns
            % -------
            %    this :obj:`object`
            %                                 Initializes the :class:`LineageData.NewFrame`
            %                                 object and returns :class:`LineageData`.
            
            % NewFrame needs no trackindex or age therefore it will be
            % initialized empty
            ofl = length(this.getFieldArrayForFrame('obj_frame', frameNumber ));
            
            if ~isempty(ofl)
                this.NewFrame.addData([], [], [], ...
                    this.getFieldArrayForFrame('uuid', frameNumber)', ...
                    this.getFieldArrayForFrame('obj_center_x', frameNumber), ...
                    this.getFieldArrayForFrame('obj_center_y',frameNumber), ...
                    this.getFieldArrayForFrame('obj_center_z',frameNumber), ...
                    zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), zeros(ofl,1), ...
                    zeros(ofl,1), zeros(ofl,1), ...
                    this.getFieldArrayForFrame('obj_orientation', frameNumber), ...
                    this.getFieldArrayForFrame('obj_area', frameNumber), ...
                    this.obj_close_neighbour(frameNumber).Indicies, ...
                    zeros(ofl,1), ...
                    this.getFieldArrayForFrame('obj_track_fingerprint_real', ...
                    frameNumber)', ...
                    zeros(ofl,1), zeros(ofl,1), zeros(ofl,1));
                
            else
                this.NewFrame.addData([], [], [], ...
                    nan, nan, nan,  nan, nan, nan, nan,  nan, nan, nan, ...
                    nan, nan, nan, zeros(ofl,1), zeros(ofl,1), ...
                    zeros(ofl,1), zeros(ofl,1), zeros(ofl,1));
            end
            
        end
        
        function this = InitializeVeryOld(this, assignmentRowIdx, ...
                maxTrackFrameSkipping)
            % INITIALIZEVERYOLD Initailizes the VeryOld property of the
            % Tracker with data from OldFrame.
            %
            % Args:
            %    this (:obj:`object`):                :class:`LineageData` instance
            %    assignmentRowIdx (:obj:`int`):       Row index of track
            %                                         assignment.
            %    maxTrackFrameSkipping (:obj:`int`):  Max tolerated
            %                                         frames a track is
            %                                         allowed to be missing.
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                         :class:`LineageData` instance.
            
            if isempty(this.OldFrame.cell_close_neighbour)
                cell_close_neighbour = [];
            else
                cell_close_neighbour = this.OldFrame.cell_close_neighbour(...
                    assignmentRowIdx, :);
            end
            % Initialisation of VeryOld from OldFrame
            this.VeryOld.addData(this.OldFrame.track_index(assignmentRowIdx), ...
                this.OldFrame.track_index_qc(assignmentRowIdx), ...
                this.OldFrame.track_age(assignmentRowIdx), ...
                this.OldFrame.cell_index(assignmentRowIdx),...
                this.OldFrame.cell_center_x(assignmentRowIdx), ...
                this.OldFrame.cell_center_y(assignmentRowIdx), ...
                this.OldFrame.cell_center_z(assignmentRowIdx), ...
                this.OldFrame.cell_dif_x(assignmentRowIdx), ...
                this.OldFrame.cell_dif_y(assignmentRowIdx), ...
                this.OldFrame.cell_dif_z(assignmentRowIdx), ...
                this.OldFrame.cell_filtered_dif_x(assignmentRowIdx), ...
                this.OldFrame.cell_filtered_dif_y(assignmentRowIdx), ...
                this.OldFrame.cell_filtered_dif_z(assignmentRowIdx), ...
                this.OldFrame.cell_orientation(assignmentRowIdx), ...
                this.OldFrame.cell_area(assignmentRowIdx), cell_close_neighbour, ...
                this.OldFrame.cell_del(assignmentRowIdx), ...
                this.OldFrame.track_fingerprint_real(assignmentRowIdx), ...
                this.OldFrame.track_fingerprint_real_distance(assignmentRowIdx), ...
                this.OldFrame.track_assignment_fraction(assignmentRowIdx), ...
                this.OldFrame.track_fingerprint_age(assignmentRowIdx));
            
            
            fieldNames = fieldnames(this.VeryOld);
            %             iFieldName = 1;
            %             while iFieldName <= numel(fieldNames)
            %                 kAssignment = numel(assignmentRowIdx);
            %                 while kAssignment > 0
            %                     if size(this.VeryOld.(fieldNames{iFieldName}), 2) == 1
            %                         this.VeryOld.(fieldNames{iFieldName})(...
            %                             assignmentRowIdx(kAssignment)) = [];
            %                     else
            %                         if ~isempty(this.VeryOld.(fieldNames{iFieldName}))
            %                             this.VeryOld.(fieldNames{iFieldName})(...
            %                                 assignmentRowIdx(kAssignment), :) = [];
            %                         end
            %                     end
            %                     kAssignment = kAssignment - 1;
            %                 end
            %                 iFieldName = iFieldName + 1;
            %             end
            
            % Remove too old old tracks (track_age above
            % maxTrackFrameSkipping threshold)
            if ~isempty(this.VeryOld.track_age)
                tooOldTrackArray = (find(this.VeryOld.track_age > ...
                    maxTrackFrameSkipping));
                iFieldName = 1;
                if ~isempty(tooOldTrackArray)
                    while iFieldName <= numel(fieldNames)
                        kAssignment = numel(tooOldTrackArray);
                        while kAssignment > 0
                            if size(this.VeryOld.(fieldNames{iFieldName}), 2) == 1
                                this.VeryOld.(fieldNames{iFieldName})(...
                                    tooOldTrackArray(kAssignment)) = [];
                            else
                                if ~isempty(this.VeryOld.(fieldNames{iFieldName}))
                                    this.VeryOld.(fieldNames{iFieldName})(...
                                        tooOldTrackArray(kAssignment), :) = [];
                                end
                            end
                            kAssignment = kAssignment - 1;
                        end
                        iFieldName = iFieldName + 1;
                    end
                end
                
                if isempty(this.VeryOld.cell_close_neighbour)
                    this.VeryOld.cell_close_neighbour = [];
                end
            end
            
            
        end
        
        function clearMarkerTracking(this)
            % CLEANMARKGERTRACKING Deletes all previous assignments made
            % during marker trakcing (for cell cycle determination of
            % asymmetrically dividing cells).
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`) 
            
            dataProps = properties(this);
            propertiesToClear = {'obj_track_index', 'obj_track_age', ...
                'obj_dif_x', 'obj_dif_y', 'obj_filtered_dif_x', ...
                'obj_filtered_dif_y'};
            for k = 1:numel(propertiesToClear)
                this.(dataProps{strcmp(dataProps, ...
                    propertiesToClear{k})}) = nan(numel(this.obj_frame), 1);
            end
        end
        
        function saveTrackingAssignmentsToTrackerData(this, frameNumber, ...
                dataToSave)
            % SAVETRACKINGASSIGNMENTSTOTRACKERDATA Update the 
            % temporary data (oldFrame) by joining newFrame and veryOld for
            % the next frame track assignment iteration.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %    frameNumber (:obj:`int`):       Frame number for which the
            %                                    data should be saved.
            %    dataToSave (:obj:`array`):      New tracking assignment 
            %                                    data to be saved.
            %
            % Returns
            % -------
            %    void (:obj:`-`) 
            
            names = fieldnames(dataToSave);
            for name = names'
                this.setFieldArrayForFrame(char(name), frameNumber, ...
                    dataToSave.(char(name)));
            end
        end
        
        function updateTemporaryTrackingDataForNextFrameEvaluation(this, ...
                neighbourhoodSearchRadius)
            % UPDATETEMPORARYTRACKINGDATAFORNEXTFRAMEEVALUATION Update the 
            % temporary data (oldFrame) by joining newFrame and veryOld for
            % the next frame track assignment iteration.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %    neighbourRadius (:obj:`int`):   Distance in pixel to consider
            %                                    other cells to be a neighbouring
            %                                    cell.
            %
            % Returns
            % -------
            %    void (:obj:`-`) 
            
            dataProps = properties(this.OldFrame);
            for k = 1:numel(dataProps)
                if strcmp(dataProps{k}, 'cell_close_neighbour') ~= 1
                    this.OldFrame.(dataProps{k}) = [this.NewFrame.(dataProps{k}); ...
                        this.VeryOld.(dataProps{k})];
                else
                    this.OldFrame.(dataProps{k}) = TracX.TrackerData. ...
                        getCloseCellNeighbourIdx(...
                        this.OldFrame.cell_center_x, ...
                        this.OldFrame.cell_center_x, ...
                        neighbourhoodSearchRadius);
                end
            end
        end
        
        function this = initializeCellCloseNeighbours(this, neighbourRadius, ...
                nUniqueFrames)
            % INITIALIZECELLCLOSENEIGHBOURS Initializes the cell
            % neighbourhood index matrix for all cells within neighbourRadius
            % related fields of the :class:`LineageData` object.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %    neighbourRadius (:obj:`int`):   Distance in pixel to consider
            %                                    other cells to be a neighbouring
            %                                    cell.
            %    nUniqueFrames (:obj:`int`):     Max number of frames to
            %                                    initialize neighbourhood.
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                    :class:`LineageData` instance.
            
            for iFrame = 1:nUniqueFrames
                x = this.getFieldArrayForFrame('obj_center_x', iFrame);
                y = this.getFieldArrayForFrame('obj_center_y', iFrame);
                this.obj_close_neighbour(iFrame).Indicies = ...
                    TracX.TrackerData.getCloseCellNeighbourIdx(x, y, neighbourRadius);
            end
            
        end
        
        function nRows = getNumberOfRowsFromFrame(this, frameNumber)
            % GETNUMBEROFROWSFORFRAME Returns the number of rows in
            % the SegmentationData object for the selected frameNumber.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %    frameNumber (:obj:`int`):       Frame number for which the
            %                                    data should be retrieved.
            %
            % Returns
            % -------
            %    nRows (:obj:`int`)
            %                                    Number of rows in :class:`LineageData` for
            %                                    frameNumber.
            
            nRows = numel(this.obj_frame(...
                this.obj_frame == frameNumber));
        end
        
        function [oldFrameLocIdx, emptyDataLocIdx] = getTrackIndexStorageIndicies(this)
            % GETTRACKINDEXSTORAGEINDICIES Returns indicies of track_index
            % where to store them.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %
            % Returns
            % -------
            %    oldFrameIdx (:obj:`array`)
            %                                   OldFrame indicies for track_index.
            %    emptyDataIdx (:obj:`array`)
            %                                   Indicies of empty data.
            
            range = transpose(1:1:numel(this.OldFrame.track_index));
            matchIdx = arrayfun(@(x)find([this.OldFrame.track_index; ...
                this.VeryOld.track_index]==x,1),this.NewFrame.track_index,...
                'UniformOutput', 0);
            matchArray = arrayfun(@(x)range(cell2mat(x)),matchIdx, 'UniformOutput', 0);
            oldFrameLocIdx = cell2mat(matchArray);
            emptyDataLocIdx = cellfun('isempty',matchArray);
        end
        
        function copyResultsToTrackerData(this, dataToSave, fieldName, ...
                emptyDataLocIdx, oldFrameLocIdx, frameNumber)
            % COPYRESULTSTOTRACKERDATA Copies results of the frame
            % by frame tracking to the data store TracX.TrackerData.
            %
            % Args:
            %    this (:obj:`object`):           :class:`LineageData` instance
            %    dataToSave (:obj:`str`):        Empty array (cell, nan) of the correct
            %                                    size to hold the data to save.
            %    fieldName (:obj:`str`):         String with fieldname where to
            %                                    add new values.
            %    emptyDataLocIdx (:obj:`array`): Location indicies of empty data.
            %    oldFrameLocIdx (:obj:`array`):  OldFrame location indicies for track_index.
            %    frameNumber (:obj:`int`):       Frame number for which the fieldName
            %                                    data should be added.
            %
            % Returns
            % -------
            %    void (:obj:`-`) 
            
            dataToSave(~emptyDataLocIdx) = this.OldFrame.(fieldName)...
                (oldFrameLocIdx);
            if iscell(dataToSave)
                dataToSave(emptyDataLocIdx) = {nan};
            else
                dataToSave(emptyDataLocIdx) = nan;
            end
            this.setFieldArrayForFrame(fieldName, frameNumber, ...
                dataToSave);
        end
        
        function setFieldArrayForFrame(this, fieldName, frameNumber,...
                fieldDataArray )
            % SETFIELDARRAYFORFRAME Sets data for a particular fieldname
            % in the SegmentationData object of TrackerData.
            %
            % Args:
            %    this (:obj:`object`):          :class:`LineageData` instance
            %    fieldName (:obj:`str`):        String with fieldname where to
            %                                   add new values.
            %    frameNumber (:obj:`int`):      Frame number for which the fieldName
            %                                   data should be added.
            %    fieldDataArray (:obj:`array`): Data to add to fieldName.
            %
            % Returns
            % -------
            %    void (:obj:`-`) 
            
            this.(fieldName)(...
                this.obj_frame == frameNumber) = fieldDataArray;
        end
                
        function this = setUUID(this)
            % SETUUID Sets a unique id for each object in LineageData
            % over the whole project to facilitate the selection of data.
            % Args:
            %    this (:obj:`object`):                :class:`LineageData` instance
            %
            % Returns
            % -------
            %    this :obj:`object` 
            %                                         :class:`LineageData` instance.
            
            if isempty(this.uuid)
                this.uuid = 1;
            else
                this.uuid(end + 1) = max(this.uuid) + 1;
            end
        end
        
        % MOVETOVERYOLD Moves temporary data stored in OldFrame to
        % VeryOld.
        [ trackIdxToChange ] = moveToVeryOld(this, selectedTrackArray, ...
            neighbourhoodSearchRadius)
        
        % GETCLOSECELLNEIGHBOURIDX Returns a square logical matrix of size
        % length(x) where as 1 corresponds to a neighbour within the
        % neighbourhoodSearchRadius.
        [ closeNeighboursIdx ]  = getCloseCellNeighbourIdx(~, ...
            xCoordinateArray, yCoordinateArray, neighbourhoodSearchRadius)
        
    end
    
end
