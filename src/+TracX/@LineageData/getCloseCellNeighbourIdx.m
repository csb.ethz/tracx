%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  closeNeighboursIdx  = getCloseCellNeighbourIdx(~, ...
    xCoordinateArray, yCoordinateArray, neighbourhoodSearchRadius)
% GETCLOSECELLNEIGHBOURIDX Returns a square logical matrix of size
% length(x) where as 1 corresponds to a neighbour within the
% neighbourhoodSearchRadius.
%
% Args:
%    ignoredArg (:obj:`object`):             :class:`LineageData` instance
%    xCoordinateArray (:obj:`array`, kx1):   Array with x coordinates
%    yCoordinateArray (:obj:`array`, kx1):   Array with y coordinates
%    neighbourhoodSearchRadius (:obj:`int`): Radius in pixel within an other
%                                            point defined by the coordinate
%                                            arrays x and y is considered to
%                                            be a neighbour of (x_i,y_i).
%                                            :class:`~+TracX.@TrackerParameter.TrackerParameter.neighbourhoodSearchRadius` 
%
% Returns
% -------
%    closeNeighboursIdx :obj:`array` NxM                               
%                                           Square logical matrix selecting the
%                                           neighbours within
%                                           neighbourhoodSearchRadius.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if ~isempty(xCoordinateArray)
    coordinateArray = [xCoordinateArray, yCoordinateArray];
    cellDist = pdist(coordinateArray) <= neighbourhoodSearchRadius;
    closeNeighboursIdx = squareform(cellDist);
else
    closeNeighboursIdx = [];
end

end