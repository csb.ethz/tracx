%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function destackTimepoints(this, inputFilePath, fileRegEx, nTimepoints, ...
    nPlanes, direction)
% DESTACKTIMEPOINTS Destacks a 3D (multipage tiff) stack into single files
% per time point.
%
% Args:
%    this (:obj:`obj`):           :class:`IO` instance.
%    inputFilePath (:obj:`str`):  Path to files.
%    fileRegEx (:obj:`str`):      File name identifier regular expression
%    nTimepoints (:obj:`int`):    Number of time points in stack
%    nPlanes (:obj:`int`):        Number of planes in stack
%    direction (:obj:`int`):      Direction to destack either planes (0) or
%                                 timepoints (1).
%
% Returns
% -------
%    void (:obj:`-`)
%                                 Writes destacked files to disk directly.  
% :Authors:
%    Thomas Kuendig - initial implementation


files = dir(fullfile(inputFilePath,fileRegEx));
filenames = {files.name};

for currentfilename = filenames
    inputFilePathcurrent = char(fullfile(inputFilePath,currentfilename));
    fileInfo = imfinfo(inputFilePathcurrent);
    [filepath, filename, fileext] = fileparts(inputFilePathcurrent);
    
    originalFileFolder = 'UndestackedOriginalFiles';
    if isfolder(fullfile(filepath, originalFileFolder))
        warning("Files had already been destacked.")
    end
    
    if nTimepoints * nPlanes ~= length(fileInfo)
        error("Number of pages (%d) in TIF file do not match number of timepoints and planes", length(fileInfo))
    end
    
    C = cell(nTimepoints,1);
    C(:) = {zeros(fileInfo(1).Height, fileInfo(1).Width, nPlanes, 'uint16')};
    
    timepoint = 1;
    plane = 1;
    this.utils.printToConsole("Starting import")
    for i = 1:length(fileInfo)
        slice = this.readImageToGrayScale(inputFilePathcurrent,i);
        C{timepoint}(:,:,plane) = slice;
        if direction
            timepoint = timepoint + 1;
            if timepoint / nTimepoints > 1
                timepoint = timepoint - nTimepoints;
                plane = plane + 1;
            end
        else
            plane = plane + 1;
            if plane / nPlanes > 1
                plane = plane - nPlanes;
                timepoint = timepoint + 1;
            end
        end
    end
    this.utils.printToConsole("Import complete")
    
    this.utils.printToConsole("Starting export")
    
    if ~isfolder(fullfile(filepath, originalFileFolder))
        mkdir(fullfile(filepath, originalFileFolder));
    end
    registerFile = fopen(fullfile(filepath,originalFileFolder,'DestackedFileRegister.txt'),'a');
    originalFile = fopen(fullfile(filepath,originalFileFolder,'OriginalFileRegister.txt'),'a');
    fprintf(originalFile,'%s\n',[filename,fileext]);
    fclose(originalFile);
    for i = 1:length(C)
        currfilename = [filename, sprintf('_time%0.3d',i), fileext];
        outputFilePath = fullfile(filepath, currfilename);
        imwrite(C{i}(:,:,1), outputFilePath)
        fprintf(registerFile,'%s\n',currfilename);
        for j = 2:size(C{i},3)
            imwrite(C{i}(:,:,j), outputFilePath, 'writemode', 'append')
        end
    end
    fclose(registerFile);
    this.utils.printToConsole("Export complete")
    movefile(inputFilePathcurrent, [fullfile(filepath,originalFileFolder, filename), fileext])
    this.utils.printToConsole(["Original file " + filename + " moved to /" + originalFileFolder])
end
end