%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeTrackerDataToDisk(this, joinedTrackerDataTable, resultFilePath, ...
    resultFileName, varargin)
% WRITETRACKERDATATODISK Writes tracker results to disk as
% text, csv or mat file format delimited by your choice. Default
% is as text file and tab delimited.
%
% Args:
%    this (:obj:`object`):                  :class:`IO` instance..
%    joinedTrackerDataTable (:obj:`table`): Table with joined :class:`TrackerData` 
%    resultFilePath (:obj:`str`):           Path to where tracker results
%                                           should be saved.
%    resultFileName (:obj:`str`):           Name of result file
%    varargin (:obj:`str varchar`):
%
%        * **fileType** (:obj:`str`):       Save tracker results with specific file types ('txt', 'csv', 'mat').
%        * **dataDelimiter** (:obj:`str`):  Data delimiter (',', 'comma', ' ', 'space', '\t', 'tab', ';', 'semi', '|', 'bar').
%
% Returns
% -------
%    void (:obj:`-`)
%                                            Writes data file to disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
defaultFileType = 'txt';
defaultDelimiter = '\t';
validFileTypes = {'txt', 'csv', 'mat'};
validDelimiter = {',', 'comma', ' ', 'space', '\t', ...
    'tab', ';', 'semi', '|', 'bar'};

addRequired(p,'resultFilePath', @isdir);
addRequired(p,'resultFileName', @ischar);
addOptional(p,'fileType',defaultFileType, ...
    @(x) any(validatestring(x,validFileTypes)));
addOptional(p,'dataDelimiter',defaultDelimiter, ...
    @(x) any(validatestring(x,validDelimiter)));
parse(p, resultFilePath, resultFileName, varargin{:});

if isempty(joinedTrackerDataTable)
    disp('Nothing to save. Please run joinTrackerDataAsTable() first')
    return
    
elseif strcmp(p.Results.fileType, 'txt')
    
    this.writeToTXT(joinedTrackerDataTable, p.Results. ...
        resultFilePath, p.Results.resultFileName, ...
        p.Results.dataDelimiter)
    
elseif strcmp(p.Results.fileType, 'csv')
    
    this.writeToCSV(joinedTrackerDataTable, p.Results. ...
        resultFilePath, p.Results.resultFileName, ...
        p.Results.dataDelimiter)
    
elseif strcmp(p.Results.fileType, 'mat')
    
    this.writeToMAT(joinedTrackerDataTable, p.Results. ...
        resultFilePath, p.Results.resultFileName)
    
end

end