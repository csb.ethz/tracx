%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef IO < handle
    % IO class to deal with TracX input and  output while reading
    % or writing data to disk.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
        
        rawImageFileNameArray = {}; % Raw image file name array
        
        convertedImageFileNameArray = {}; % Converted image file name array
        
        utils = []; % :class:`+TracX.@Utils` instance implementing utility methods
        
    end
    
    methods
        
        function obj = IO(utils)
            % IO Constructor of an TracX.IO class to deal with TracX input and
            % output while reading or writing data to disk.
            %
            % Args:
            %    utils (:obj:`obj`):         UTILS instance.
            %
            % Returns
            % -------
            %    obj (:obj:`obj`):
            %                                Returns a IO object instance.
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj.utils = utils;
        end
        
        imageFileNameParser( this, imageFilePath, imageFileType, varargin)
        % IMAGEFILENMAEPARSER Parses any image file name to convert them to the
        % filename format which is used by Cellx as a standard:
        % :obj: `ChannelIdentifier_WellPositionWellPlane_Timepoint.FileType`
        % Using regular experssions part in an image name can be identified
        % and converted to a default or custom value.
        
        convertImageFileNames( this, imageFilePath)
        % CONVERTIMAGEFILENAMES Converts a previously parsed list of raw image file
        % names to a new format. Therby it copies the raw images to a 'RawImages
        % subfolder and keeps the newly renamed files in the imageFilePath.
        
        convertXLSToCSV( this, fullFileName, dataDelimiter)
        % CONVERTXLSTOCSV Convert a excel spreadsheet to csv table
        % from a file.
        
        [ image ] = readImageToGrayScale( pathToFile, varargin )
        % READIMAGETOGRAYSCALE loads an image and converts it to
        % gray scale if it has 3 channels (RGB). Gray conversion is
        % performed as follows: img = (R+G+B)/3
        
        [ image ] = readImageToRGB( imageFilePath, varargin )
        % READIMAGETORGB loads an image as RGB matrix.
        
        [ mask ] = readSegmentationMask(this, pathToFile )
        % READSEGMENTATIONMASK Loads an segmentation mask file
        % stored in a .mat or .tif file. Each segment should consist of a
        % positive integer value. A segment can span any number of pixel,
        % at least one at max all.
        
        [ data, dataMatrix, fieldNameArray] = readSegmentationData(this, ...
            pathName, fileNameArray, varargin)
        % READSEGMENTATIONDATA Reads segmentation data. Currently
        % it supports the CellX format where segmentation data is split
        % into a file per image frame.
        
        [ configuration ] = readFromXML(this, configuration, file)
        % READFROMXML Reads a TracX configuration xml file and stores the
        % parameters in the ParameterConfiguration object.
        
        [ ret ] = readCellXParameters(this, file)
        % READCELLXPARAMETERS Reads a CellX parameter xml file and stores
        % and returns them as struct.
        
        writeBudNeckCellSegmentationMaskImage(~, maskDilated, ...
            foundEdgesFilled, trackIndexArray, cellCenterXArray, ...
            cellCenterYArray, segmentationResultDir, frameNumber, ...
            imageCropCoordinateArray)
        % WRITEBUDNECKCELLSEGMENTATIONMASKIMAGE Detect bud neck
        % marker from fluorescence images. A ROF denoising is first
        % applied and then an edge detection to get an array of pixel
        % that each bud covers.
                
        writeTrackerDataToDisk(~, joinedDataTable, resultFilePath, ...
            resultFileName, varargin)
        % WRITETRACKERDATATODISK Writes tracker results to disk as
        % text, csv or mat file format delimited by your choice. Default
        % is as text file and tab delimited.
        
        writeToCSV(~, data, resultFilePath, resultFileName, ...
            dataDelimiter)
        % WRITETOCSV Writes tracker results to disk in csv format.
        
        writeToTXT(~, data, resultFilePath, resultFileName, ...
            dataDelimiter)
        % WRITETOTEXT Writes tracker results to disk in txt format.
        
        writeToXLS(~, data, resultFilePath, resultFileName, ...
            varargin)
        % WRITETOXLS Writes tracker results to disk as excel spreadsheet.
        
        writeToMAT(~, data, resultFilePath, resultFileName, varargin)
        % WRITETOMAT Writes tracker results to disk in mat format.
        
        writeToXML(this, configuration, file)
        % WRITETOXML Writes a TracX configuration xml file with all the
        % parameters stored in the ParameterConfiguration and
        % ProjectConfiguration object.
        
        writeToPNG(~, image, fullPath )
        % WRITETOPNG Writes an image matrix as png file to disk.
      
        writeToTIF(~, image, fullPath )
        % WRITETOTIF Writes an image matrix as tif file to disk.
        
        % EXTRACTCELLBOUNDARYPOINTSFROM2DMASKS extracts cell boundary
        % points from .mat files containing segmentation results
        
        [ sortedFilenames, checkMatrix, nTimePoints, nPlanes ] = ...
            filenameSorterForCellXinput(~, fileNameArray)
        % FILENAMESORTERFORCELLXINPUT Sorts CellX output file names and returns
        % them as well as the number of time points and planes.
        
        convertChallengeFilesToParseable(this, path, setName)
        % CONVERTCHALLENGEFILESTOPARSEABLE Converts files from the tracker
        % challenge into parseable format.
                
        [ tag ] = orientationCompressor (~, vec)
        % ORIENTATIONCOMPRESSOR Compresses the orientation vector into a single
        % number.
        
        maskInterface3D(this, frameNumber, fileseg, fileimg, resultPath, ...
            pxPerZIntervall, fluotags, fluoimgs)
        % MASKINTERFACE3D Interface to import 3D segmentation data from mask files
        % into TracX internal data format.
        
        destackTimepoints(this, inputFilePath, fileRegEx, nTimepoints, ...
            nPlanes, direction)
        % DESTACKTIMEPOINTS Destacks a 3D (multipage tiff) stack into single 
        % files per time point.
        
        [ remnant ] = stackZPlanes(this, inputFilePath, fileRegEx, ...
            planeRegEx, convert)  
        % STACKZPLANES Stacks single file z planes into one multipage tiff.

    end 
end