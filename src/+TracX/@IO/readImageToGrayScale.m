%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ image ] = readImageToGrayScale(~, pathToFile, varargin )
% READIMAGETOGRAYSCALE loads an image and converts it to gray scale if it 
% has 3 channels (RGB). Gray conversion is performed as follows:
% img = (R+G+B)/3
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    pathToFile (:obj:`str`):     Path to image file.
%    varargin:                    [x, y, w-1, h-1] :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray` 
%                                 used to crop theloaded image to this
%                                 size.
%
% Returns
% -------
%    image (:obj:`array`, NxM):   Image matrix, optionally cropped to
%                                 the size given by :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`
%
% :Authors:
%   Sotiris Dimopoulos - initial implementation in CellX
%   Andreas P. Cuny - Adaptation for TracX

imageCropCoordinateArray = [];
pageNumber = 1;
if( size(varargin,2) > 2 )
    error 'Variable argument list too long';
elseif ( size(varargin,2)==1 && ~isscalar(varargin{1}))
    imageCropCoordinateArray = varargin{1};
elseif (size(varargin,2)==1 && isscalar(varargin{1}))
    pageNumber = varargin{1};
elseif ( size(varargin,2)==2 )
    imageCropCoordinateArray = varargin{1};
    pageNumber = varargin{2};
end

% reduce to imageCropCoordinateArray region if speficied
if( ~isempty(imageCropCoordinateArray) )
    image = double(imread( pathToFile, pageNumber, 'PixelRegion', ...
        {[imageCropCoordinateArray(2), 1, imageCropCoordinateArray(2) + ...
        imageCropCoordinateArray(4)], [imageCropCoordinateArray(1), 1, ...
        imageCropCoordinateArray(1) + imageCropCoordinateArray(3)]})); 
else
    image = double(imread( pathToFile , pageNumber));
end
dimensionCount = length(size(image));

% check if image is truecolor(RGB) image
if( dimensionCount==3 && size(image,3)==3 )
    fprintf('Converting ''%s'' to grayscale\n', pathToFile);
    image = ( ...
        image(:,:,1) + ...
        image(:,:,2) + ...
        image(:,:,3) ...
        )/3;
elseif( dimensionCount~=2 )
    error('Image import error (unknown image format)\n');
end
end
