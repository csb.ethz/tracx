%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function image = readImageToRGB(~, imageFilePath, varargin )
% READIMAGETORGB loads an image as RGB matrix
%
% Args:
%    ignoredArg (:obj:`obj`):    :class:`IO` instance.
%    imageFilePath (:obj:`str`): Path to image file.
%    varargin: [x, y, w-1, h-1]  :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray` 
%                                used to crop the loaded image to this size.
%
% Returns
% -------
%    image (:obj:`array`, NxM):  
%                                Image matrix, optionally cropped to the size specified by 
%                                :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`.
% :Authors:
% 	Andreas P. Cuny - initial implementation

imageCropCoordinateArray = [];
if( size(varargin,2) > 1 )
    error 'Variable argument list too long';
elseif ( size(varargin,2)==1 )
    imageCropCoordinateArray = varargin{1};
end

[image, map] = imread( imageFilePath );
dimensionCount = length(size(map));

% check if image is truecolor(RGB) image
if( dimensionCount==3 && size(map,3)==3 )
elseif( dimensionCount~=2 )
    error('Image import error (unknown image format)\n');
end

% reduce to imageCropCoordinateArray region if specified
if( ~isempty(imageCropCoordinateArray) )
    image = TracX.Utils.imcropC(image, imageCropCoordinateArray);
end
image = ind2rgb(image, map);

end

