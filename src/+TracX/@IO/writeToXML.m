%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeToXML(~, configuration, file)
% WRITETOXML Writes a TracX configuration xml file with all the
% parameters stored in the ParameterConfiguration and
% ProjectConfiguration object.
%
% Args:
%    ignoredArg (:obj:`obj`):           :class:`IO` instance. instance
%    configuration (:obj:`obj`):        :class:`TrackerConfiguration` object.
%    file (:obj:`str`):                 Path to TracX configuration xml
%                                       file and its name.
%
% Returns
% -------
%    void (:obj:`-`)
%                                     Writes to disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

try
    doc = com.mathworks.xml.XMLUtils.createDocument('TrackerConfiguration');
    root = doc.getDocumentElement;
    root.setAttribute('timestamp',datestr(now));
    tagName = {'TrackerParam', 'TrackerProject'};
    arrayElemTagName = 'ArrayElement';
    nameAttrName = 'name';
    valueAttrName = 'value';
    charAttrName = 'string';
    configProps = properties(configuration);
    configProps = configProps(1:2);
    
    for iConfProp = 1:numel(configProps)
        props = properties(configuration.(configProps{iConfProp}));
        n = numel(props);
        for i=1:n
            id = props{i};
            element = doc.createElement(tagName{iConfProp});
            element.setAttribute(nameAttrName, id);
            fn = numel(configuration.(configProps{iConfProp}).(id));
            if(fn==1 || ischar(configuration.(configProps{iConfProp}).(id)) )
                if( isnumeric(configuration.(configProps{iConfProp}).(id)) )
                    if(configuration.(configProps{iConfProp}).(id) ==round(configuration.(configProps{iConfProp}).(id)))
                        value = num2str(configuration.(configProps{iConfProp}).(id), '%d');
                    else
                        value = num2str(configuration.(configProps{iConfProp}).(id), '%f');
                    end
                    element.setAttribute(valueAttrName, value);
                elseif(islogical(configuration.(configProps{iConfProp}).(id)))
                    if( configuration.(configProps{iConfProp}).(id) )
                        element.setAttribute(valueAttrName, '1');
                    else
                        element.setAttribute(valueAttrName, '0');
                    end
                else
                    %> @todo dirty fix for 1x1 cells
                    if ~iscell(configuration.(configProps{iConfProp}).(id))
                        element.setAttribute(charAttrName, configuration.(configProps{iConfProp}).(id));
                    else
                        if isempty(configuration.(configProps{iConfProp}).(id){1})
                            element.setAttribute(charAttrName, [])
                        else
                            if ~iscell(configuration.(configProps{iConfProp}).(id){1})
                                element.setAttribute(charAttrName, configuration.(configProps{iConfProp}).(id){1});
                            end
                        end
                    end
                end
            else
                for j=1:fn
                    childElement = doc.createElement(arrayElemTagName);
                    x = configuration.(configProps{iConfProp}).(id)(j);
                    if( numel(x)~=1 )
                        error('Cannot handle property ''%s'' in XML export', id);
                    end
                    if( isnumeric(x) )
                        if( x==round(x) )
                            value = num2str(x, '%d');
                        else
                            value = num2str(x, '%f');
                        end
                        childElement.setAttribute(valueAttrName, value);
                    else
                        childElement.setAttribute(charAttrName, x);
                    end
                    element.appendChild(childElement);
                end
            end
            root.appendChild(element);
        end
    end
    
    [~, ~, fileExtension] = fileparts(file);
    
    if ~isempty(fileExtension)
        xmlwrite(file, doc);
    else
        xmlwrite([file, '.xml'], doc);
    end

catch ME
    disp(ME.identifier)
end
end