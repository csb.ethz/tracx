%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [tag] = orientationCompressor (~, vector3D) 
% ORIENTATIONCOMPRESSOR Compresses the orientation vector into a single
% number. Note: Vector should be already of unit length!
%
% Args:
%    ignoredArg (:obj:`obj`): :class:`IO` instance.
%    vector3D (:obj:`array`): 3D vector
%
% Returns
% -------
%    tag: :obj:`int`
%                             Orientation incompressed format.
%
% :Authors:
%    Thomas Kuendig - initial implementation

vector3D = round(vector3D,4) + 1;
tag = vector3D(:,1)*1e14 + vector3D(:,2)*1e9 + + vector3D(:,3)*1e4;

end