%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function convertImageFileNames(this, imageFilePath)
% CONVERTIMAGEFILENAMES Converts a previously parsed list of raw image file
% names to a new format. Therby it copies the raw images to a 'RawImages
% subfolder and keeps the newly renamed files in the imageFilePath.
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    imageFilePath (:obj:`str`):  Path to the images to be paresed                                      
%
% Returns
% -------
%    void (:obj:`-`)
%                               Directly acts on images on the filesystem.
%
% Note: 
%                               Run Tracker.io.imageFileNameParser() first.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

% Remove file names which will not be renamed (i.e. are
% identical)
rmIdx = strcmp(this.rawImageFileNameArray, ...
    this.convertedImageFileNameArray);
this.rawImageFileNameArray = this.rawImageFileNameArray(~rmIdx);
this.convertedImageFileNameArray = this.convertedImageFileNameArray(~rmIdx);

% Duplicate images and rename them
nI = numel(this.rawImageFileNameArray);
pBar = this.utils.progressBar(nI, ...
            'Title', 'FILE: Converting file names' );
for k = 1:nI
    if exist(fullfile(imageFilePath, this.rawImageFileNameArray{k}), 'file')
        
        copyfile(fullfile(imageFilePath, this.rawImageFileNameArray{k}), ...
            fullfile(imageFilePath, ...
            this.convertedImageFileNameArray{k}), 'f')
    end
    pBar.step(1, [], []);
end
pBar.release();


% Create subfolder for raw images
if ~exist(fullfile(imageFilePath, 'RawImages'), 'dir')
    mkdir(fullfile(imageFilePath, 'RawImages'))
end

% Move original raw images to subfolder
nI = numel(this.rawImageFileNameArray);
pBar = this.utils.progressBar(nI, ...
            'Title', 'FILE: Moving raw data' );
for k = 1:nI
    if exist(fullfile(imageFilePath, this.rawImageFileNameArray{k}), 'file')
        
        movefile(fullfile(imageFilePath, this.rawImageFileNameArray{k}), ...
            fullfile(imageFilePath, 'RawImages', ...
            this.rawImageFileNameArray{k}), 'f');  
    end
    pBar.step(1, [], []);
end
pBar.release();

this.utils.printToConsole('FILE: Done with file name conversion.')

end