%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function imageFileNameParser(this, imageFilePath, imageFileType, varargin)
% IMAGEFILENMAEPARSER Parses any image file name to convert them to the 
% filename format which is used by Cellx as a standard:
% :obj: `ChannelIdentifier_WellPositionWellPlane_Timepoint.FileType` 
% Using regular experssions part in an image name can be identified and converted to a default or custom value.
%  
% Args:
%    ignoredArg (:obj:`obj`):      :class:`IO` instance.
%    imageFilePath (:obj:`str`):   Path to the images to be paresed
%    imageFileType (:obj:`str`):   Image file type. Supported are ['tif', 'TIF', 'TIFF', 'tiff', 'png', ...
%                                  'PNG', 'JPG', 'jpg']
%    varargin (:obj:`str varchar`):
%
%        * **channelRE** (:obj:`str`):        Regular expression to identify the image channel.
%        * **positionRE** (:obj:`str`):       Regular expression to identify the well position.
%        * **planeRE** (:obj:`str`):          Regular expression to identify the image plane.
%        * **timeRE** (:obj:`bool`):          Regular expression to identify the image frame number.
%        * **customChannel** (:obj:`str`):    Custom name for the channel.
%        * **customPosition** (:obj:`str`):   Custom position name.
%        * **customZIndex** (:obj:`str`):     Custom image plane index.
%        * **channelNameDict** (:obj:`bool`): Channel name dictionary.                                   
%
% Returns
% -------
%    void (:obj:`-`)
%                                IO object with the result filled into
%                                `rawImageFileNameArray` and
%                                `convertedImageFileNameArray`. 
%
% Example:
%
% >>> Tracker.io.imageFileNameParser( 'Path\to\folder', 'JPG', 'timeRE', '(?<=time)\d*', 'positionRE', '(?<=Position\()\d*')
%
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
defaultChannel = 'BF';
defaultPosition = '0101'; % well A 1 (0101)
defaultPlane = '00'; % z index 0 (00)
validImageFileType = {'tif', 'TIF', 'TIFF', 'tiff', 'png', ...
    'PNG', 'JPG', 'jpg'};

addRequired(p,'imageFilePath',@isfolder);
addRequired(p,'imageFileType', @(x) any(validatestring(x,validImageFileType)));
addParameter(p,'channelRE', [], @ischar);
addParameter(p,'positionRE', [], @ischar);
addParameter(p,'planeRE', [], @ischar);
addParameter(p,'timeRE', [],  @ischar);
addParameter(p,'customChannel', [], @ischar);
addParameter(p,'customPosition', [], @ischar );
addParameter(p,'customZIndex', [], @ischar );
addParameter(p,'channelNameDict', [], @iscell );
parse(p, imageFilePath, imageFileType, varargin{:});
p.Results;

% Reset names list before parsing
this.rawImageFileNameArray = {};
this.convertedImageFileNameArray = {};

imageFiles = dir(fullfile(p.Results.imageFilePath, sprintf('*%s', ...
    p.Results.imageFileType)));
this.rawImageFileNameArray = {imageFiles.name}';
if ~isempty(this.rawImageFileNameArray)
    for i = 1:numel(this.rawImageFileNameArray)
        iFile = this.rawImageFileNameArray{i};
        
        % Determine channel idenitifier
        if ~isempty(p.Results.channelRE)
            channelIdentifier = regexp(iFile, p.Results.channelRE, 'match');
            if ~isempty(channelIdentifier)
                channelIdentifier = channelIdentifier{:};
                
                if ~isnan(str2double(channelIdentifier))
                    
                    % if user provides custom name for channel
                    % numbers, use name value pair to map i.e.
                    % 1 to GFP or any string which is given as
                    % variable input.
                    
                    if  ~isempty(p.Results.channelNameDict)
                        channelNameDict = cellfun(@(x) num2str(x), ...
                            p.Results.channelNameDict, ...
                            'UniformOutput', false);
                        idx = find(strcmp(channelNameDict, ...
                            channelIdentifier));
                        if ~isempty(idx) && idx+1 <= ...
                                size(channelNameDict, 2)
                            channelIdentifier = channelNameDict{idx+1};
                        end
                    else
                        channelIdentifier = sprintf('Ch%0.2d', ...
                            str2double(channelIdentifier));
                    end
                else
                    % keep string as identifier
                end
            end
        else
            if ~isempty(p.Results.customChannel)
                channelIdentifier = p.Results.customChannel;
            else
                channelIdentifier = defaultChannel;
            end
        end
        
        % Determine position idenitifier
        if ~isempty(p.Results.positionRE)
            positionIdentifier = regexp(iFile, p.Results.positionRE, 'match');
            if ~isempty(positionIdentifier)
                positionIdentifier = positionIdentifier{:};
                
                if ~isnan(str2double(positionIdentifier))
                    positionIdentifier = sprintf('01%0.2d', ...
                        str2double(positionIdentifier));
                else
                    [letter, number] = strsplit(positionIdentifier, ...
                        {'\d'}, 'CollapseDelimiters',true, ...
                        'DelimiterType','RegularExpression');
                    letter2num = @(c)1+lower(c)-'a';
                    
                    positionIdentifier = sprintf('%0.2d%0.2d', ...
                        letter2num(letter{1}), number);
                    
                end
                
            end
            
        else
            if ~isempty(p.Results.customPosition)
                positionIdentifier = p.Results.customPosition;
            else
                positionIdentifier = defaultPosition;
            end
        end
        
        % Determine z index position idenitifier
        if ~isempty(p.Results.planeRE)
            planeIdentifier = regexp(iFile, p.Results.planeRE, 'match');
            if ~isempty(planeIdentifier)
                planeIdentifier = planeIdentifier{:};
            end
        else
            if ~isempty(p.Results.customZIndex)
                planeIdentifier = p.Results.customZIndex;
            else
                planeIdentifier = defaultPlane;
            end
        end
        
        % Determine time idenitifier
        if ~isempty(p.Results.timeRE)
            timeIdentifier = regexp(iFile, p.Results.timeRE, 'match');
            if ~isempty(timeIdentifier)
                timeIdentifier = timeIdentifier{:};
            end
        else
            timeIdentifier = sprintf('%05d', i);
        end
        
        if ~any([isempty(channelIdentifier), ...
                isempty(positionIdentifier), ...
                isempty(planeIdentifier), ...
                isempty(timeIdentifier), ...
                isempty(imageFileType)])
            if all(positionIdentifier == '.') && all(planeIdentifier == '.')
                newFileName = sprintf('%s_time%s.%s', ...
                channelIdentifier,  timeIdentifier, imageFileType);
            else
                newFileName = sprintf('%s_position%s_time%s.%s', ...
                channelIdentifier, [positionIdentifier, ...
                planeIdentifier], timeIdentifier, imageFileType);
            end
            
            this.convertedImageFileNameArray{i, 1} = newFileName;
        else
            this.convertedImageFileNameArray{i, 1} = iFile;
        end
    end
    if numel(this.rawImageFileNameArray) >= 2
        this.utils.printToConsole(sprintf('PARSING PREVIEW: %s -> %s', this.rawImageFileNameArray{1}, this.convertedImageFileNameArray{1}))
        this.utils.printToConsole(sprintf('PARSING PREVIEW: %s -> %s', this.rawImageFileNameArray{2}, this.convertedImageFileNameArray{2}))
    end
else
    this.utils.printToConsole(sprintf('PARSING INFO: No images found to parse'))
end
end