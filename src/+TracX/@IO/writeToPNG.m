%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = writeToPNG(~, image, fullPath )
% WRITETOPNG Writes an image matrix as png file to disk.
%
% Args:
%    ignoredArg (:obj:`obj`):    :class:`IO` instance.
%    image (:obj:`array`, NxM):  Image matrix
%    fullPath (:obj:`str`):      Path to where the image will be
%                                saved.
%
% Returns
% -------
%    void (:obj:`-`)
%                                Writes image file to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

[base, fileName, fileExtension] = fileparts(fullPath);

[x, map] = rgb2ind(image, 256);

if ~isempty(fileExtension) && strcmp(fileExtension, '.png')
    imwrite(x, map, fullPath);
else
    imwrite(x, map, fullfile(base,fileName,'.png'));
end

end
