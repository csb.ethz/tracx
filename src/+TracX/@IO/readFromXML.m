%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function configuration = readFromXML(~, configuration, file)
% READFROMXML Reads a TracX configuration xml file and stores the
% parameters in the ParameterConfiguration object.
%
% Args:
%    ignoredArg (:obj:`obj`):      :class:`IO` instance.
%    configuration (:obj:`obj`):   :class:`TrackerConfiguration` object.
%    file (:obj:`str`):            Path to parameter xml file
%
% Returns
% -------
%    configuration (:obj:`obj`):   Configuration. Acts on :class:`TrackerConfiguration`
%                                  object directly and returns it parametrized.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

try
    paramConfMethods = methods(configuration.ParameterConfiguration);
    projectConfMethods = methods(configuration.ProjectConfiguration);
    tagName = {'TrackerParam', 'TrackerProject'};
    arrayElemTagName = 'ArrayElement';
    nameAttrName = 'name';
    valueAttrName = 'value';
    charAttrName = 'string';
    doc = xmlread(file);
    root = doc.getDocumentElement();
    for tn = 1:numel(tagName)
        paramNodes = root.getElementsByTagName(tagName{tn});
        n = paramNodes.getLength;
        for i=1:n
            node = paramNodes.item(i-1);
            id = node.getAttribute(nameAttrName);
            hasValue=0;
            value=[];
            if(node.hasChildNodes)
                arrayElements = node.getElementsByTagName(arrayElemTagName);
                cn = arrayElements.getLength;
                value = cell(1,cn);
                for j=1:cn
                    cnode = arrayElements.item(j-1);
                    if(cnode.hasAttribute(valueAttrName))
                        value{j} = str2double(cnode.getAttribute(valueAttrName));
                        hasValue=1;
                    elseif(cnode.hasAttribute(charAttrName))
                        value{j} = char(cnode.getAttribute(charAttrName));
                        hasValue=1;
                    end
                end
            else
                if(node.hasAttribute(valueAttrName))
                    value = str2double(node.getAttribute(valueAttrName));
                    hasValue=1;
                elseif(node.hasAttribute(charAttrName))
                    value = char(node.getAttribute(charAttrName));
                    hasValue=1;
                end
            end
            if(hasValue)
                if strcmp(id, 'minor') || strcmp(id, 'version') || strcmp(id, 'patch')
                else
                    if( strcmp(tagName{tn}, 'TrackerParam') )
                        methodEvalRes = regexp(paramConfMethods, ...
                            char(id), 'match','ignorecase');
                        methodIdx = find(~cellfun(@isempty,methodEvalRes));
                        if numel(methodIdx) > 1
                            nameLength = nan(size(methodIdx));
                            for l = 1:numel(methodIdx)
                                nameLength(l) = size(paramConfMethods{methodIdx(l)},2);
                            end
                            nameLengthDiff = nameLength - size(char(id),2);
                            methodIdx = methodIdx(nameLengthDiff == min(nameLengthDiff));
                        end
                        configuration.ParameterConfiguration.(paramConfMethods{methodIdx})(value);
                    elseif( strcmp(tagName{tn}, 'TrackerProject') )
                        methodEvalRes = regexp(projectConfMethods, ...
                            char(id), 'match','ignorecase');
                        methodIdx = find(~cellfun(@isempty,methodEvalRes));
                        configuration.ProjectConfiguration.(projectConfMethods{~cellfun(@isempty,methodEvalRes)})(value);
                    end
                end
            end
        end
    end
catch ME
    disp(ME.identifier)
end
end