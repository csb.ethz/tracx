%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = convertXLSToCSV( this, fullFileName, dataDelimiter)
% CONVERTXLSTOCSV Convert a excel spreadsheet to csv table from a
% file.
%
% Args:
%    this (:obj:`obj`):          :class:`IO` instance.
%    fullFileName (:obj:`str`):  Full path and name of file as string.
%    dataDelimiter (:obj:`str`): Name or symbol of delimiter
%
% Returns
% -------
%    void (:obj:`-`)
%                                Writes text file to disk
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

[data, header] = xlsread(fullFileName);
dataTable = array2table(data);
dataTable.Properties.VariableNames = header;

[~, ~, extension] = fileparts(fullFileName);
if strcmpi(extension, '.xls')
    csvFileName = strrep(fullFileName, '.xls', '.csv');
elseif strcmpi(extension, '.xlsx')
    csvFileName = strrep(fullFileName, '.xlsx', '.csv');
end

[folder, baseFileName, extension] = fileparts(csvFileName);
if strcmpi(extension, '.csv')
    this.writeToCSV(dataTable, folder, [baseFileName, extension], ...
        dataDelimiter)
end
end
