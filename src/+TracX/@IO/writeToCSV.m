%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeToCSV(~, data, resultFilePath, resultFileName, ...
    dataDelimiter)
%  WRITETOCSV Writes tracker results to disk in csv format.
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    data (:obj:`table`):         Table of :class:`TrackerData` instance..
%    resultFilePath (:obj:`str`): Path to where tracker results
%                                 should be saved.
%    resultFileName (:obj:`str`): Name of result file
%    dataDelimiter (:obj:`str`):  Name or symbol of delimiter
%
% Returns
% -------
%    void (:obj:`-`)
%                               Writes text file to disk
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

[~, ~, fileExtension] = fileparts(resultFileName);

if ~isempty(fileExtension) && strcmp(fileExtension, '.csv')
     writetable(data, fullfile(resultFilePath, sprintf(...
        '%s', resultFileName)), 'Delimiter', dataDelimiter);
else
    writetable(data, fullfile(resultFilePath, sprintf(...
        '%s.csv', resultFileName)), 'Delimiter', dataDelimiter);
end

end