%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function mask  = readSegmentationMask(this, pathToFile )
% READSEGMENTATIONMASK Loads an segmentation mask file stored in a
% `.mat` or `.tif` file. Each segment should consist of a positive integer
% value. A segment can span any number of pixel, at least one at max all.
%
% Args:
%    ignoredArg (:obj:`obj`):  :class:`IO` instance.
%    pathToFile (:obj:`str`):  Path to mask file.
%
% Returns
% -------
%    mask (:obj:`array`, NxM)
%                              Segmentation mask matrix.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

[~,~,extension] = fileparts(pathToFile);
if strcmp(extension,'.mat')
    mask = load('-mat', pathToFile); 
    name = fieldnames(mask);
    if isstruct(mask)
        mask = mask.(name{1});
    end
elseif strcmp(extension,'.tif')
    for i = 1:length(imfinfo(pathToFile))
        mask(:,:,i) = this.readImageToGrayScale(pathToFile,i);
    end
else
    error('Currently only .mat and .tif files are supported.')
end

end