%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeToXLS(~, data, resultFilePath, resultFileName, ...
    varargin)
% WRITETOXLS Writes tracker results to disk as excel spreadsheet.
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    data (:obj:`table`):         Table of :class:`TrackerData` instance..
%    resultFilePath (:obj:`str`): Path to where tracker results
%                                 should be saved.
%    resultFileName (:obj:`str`): Name of result file
%    varargin (:obj:`str varchar`):
%
%        * **sheetName** (:obj:`str`):     Name of the  excel sheet.
%        * **rangeStartCol** (:obj:`str`): Range start column. Has to be capital characters i.e. A, B, C, AA.
%        * **rangeStartRow** (:obj:`int`): Range start row. Has to be  integers i.e. 1,2,3.
%        * **writeVariableNames** (:obj:`bool`): Flag, if table variable names (header) should be written. Defaults to true.
%
% Returns
% -------
%    void (:obj:`-`)
%                                Writes excel file to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
defaultSheetName = 1;
defaultRangeStartCol = 'A';
defaultRangeStartRow = 1;
defaultWriteVariableNames = true;

validSheetName = @(x) isnumeric(x) || x>1 || ischar(x) ;
validWriteVariableNames = @(x) islogical(x);
validRangeStartCol = @(x) ischar(x); 
validRangeStartRoe = @(x) isnumeric(x) || x>1 ;

addRequired(p, 'data', @istable);
addRequired(p, 'resultFilePath', @ischar);
addRequired(p, 'resultFileName', @ischar);
addOptional(p, 'sheetName', defaultSheetName, validSheetName);
addOptional(p, 'rangeStartCol', defaultRangeStartCol, validRangeStartCol);
addOptional(p, 'rangeStartRow', defaultRangeStartRow, validRangeStartRoe);
addOptional(p, 'writeVariableNames', defaultWriteVariableNames, ...
    validWriteVariableNames);

parse(p, data, resultFilePath, resultFileName, varargin{:});

[~, ~, fileExtension] = fileparts(resultFileName);

if ~isempty(fileExtension) && strcmp(fileExtension, '.xls')
      writetable(data,fullfile(resultFilePath, sprintf('%s', resultFileName)), ...
     'Sheet', p.Results.sheetName, 'Range', [p.Results.rangeStartCol ...
     num2str(p.Results.rangeStartRow)], 'WriteVariableNames', ...
     p.Results.writeVariableNames) 
else
     writetable(data,fullfile(resultFilePath, sprintf('%s.xls', resultFileName)), ...
     'Sheet', p.Results.sheetName, 'Range', [p.Results.rangeStartCol ...
     num2str(p.Results.rangeStartRow)], 'WriteVariableNames', ...
     p.Results.writeVariableNames) 
end

end