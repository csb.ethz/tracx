%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [sortedFilenames, checkMatrix, nTimePoints, nPlanes] = ...
    filenameSorterForCellXinput(~, fileNameArray)
% FILENAMESORTERFORCELLXINPUT Sorts CellX output file names and returns
% them as well as the number of time points and planes.
%
% Args:
%    this (:obj:`obj`):                   :class:`IO` instance.
%    fileNameArray (:obj:`str`, 1xK):     Array with filenames to be sorted.
%    dataDelimiter (:obj:`str`):          Name or symbol of delimiter
%
% Returns
% -------
%    sortedFilenames: (:obj:`array`, 1xK)
%                                          Array with sorted file names.
%    checkMatrix: (:obj:`array`, 3xK)
%                                          Matrix sorted by frame with
%                                          timepoints, plane and well
%                                          position.
%    nTimePoints: (:obj:`int`, 1xK)
%                                          Number of time points.
%    nPlanes: (:obj:`int`, 1xK)
%                                          Number of z planes.
%
% :Authors:
% 	Thomas Kuendig - initial implementation

timepoints = regexpi(fileNameArray,'(?<=_time)...','match');
timepoints = str2double([timepoints{:}]);
nTimePoints = length(unique(timepoints));


wellpositions = regexpi(fileNameArray,'(?<=_position)....','match');
wellpositions = str2double([wellpositions{:}]);
nWells = length(unique(wellpositions));

wellpositionsX = floor(wellpositions/100)-1;
nWellRow = length(unique(wellpositionsX));

wellpositionsY = mod(wellpositions,100);
nWellCol = length(unique(wellpositionsY));

wellIDs = wellpositionsX*nWellCol + wellpositionsY;

planes = regexpi(fileNameArray,'(?<=_position....)...','match');
planes = str2double([planes{:}]);
nPlanes = length(unique(planes));


frameNumbers =  (nPlanes*nTimePoints)*(wellIDs-1) + ...
    (timepoints-1)*nPlanes + planes;
[ ~ , frameOrder] = sort(frameNumbers);


sortedFilenames = fileNameArray(frameOrder);
checkMatrix(1,:) = timepoints(frameOrder);
checkMatrix(2,:) = planes(frameOrder);
checkMatrix(3,:) = wellpositions(frameOrder);
end