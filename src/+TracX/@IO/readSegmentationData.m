%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ data, dataMatrix, fieldNameArray] = readSegmentationData(this, ...
    pathName, fileNameArray, varargin)
% READSEGMENTATIONDATA Reads segmentation data. Currently it supports
% the CellX format where segmentation data is split into a file per image
% frame.
%
% Args:
%    this (:obj:`obj`):            :class:`IO` instance.
%    pathName (:obj:`str`):        The path to where the data is
%                                  stored.
%    fileNameArray (:obj:`str`):   Array of filename(s) of the data to
%                                  import.
%    varargin (:obj:`str varchar`):
%
%        * **maxNrOfFrames** (:obj:`str`):  Max number of files to import (if not full experimental data of a time series should be imported.
%        * **dataDelimiter** (:obj:`str`):  Data delimiter (',', 'comma', ' ', 'space', '\t', 'tab', ';', 'semi', '|', 'bar').
%
% Returns
% -------
%    data (:obj:`struct`): 
%                                   Returns segmentation data as stuct, with 
%                                   fieldnames corresponding to column
%                                   headers of raw data.
% 
%    dataMatrix (:obj:`array`):   
%                                   Data as matrix in a cell for each frame in 
%                                   fileNameArray
%    fieldNameArray (:obj:`array`): 
%                                   Array of cell arrays with the column header
%                                   names of the imported data for each frame.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
defaultMaxNrOfFrames = [];
defaultDataDelimiter = '\t';
validMaxNrOfFrames = @(x) isnumeric(x) || x>1 ;
validDataDelimiter = {',', 'comma', ' ', 'space', '\t', ...
    'tab', ';', 'semi', '|', 'bar', '\n'};
addRequired(p, 'pathName', @ischar);
addRequired(p, 'fileNameArray', @iscell);
addOptional(p, 'maxNrOfFrames', defaultMaxNrOfFrames, validMaxNrOfFrames);
addOptional(p, 'dataDelimiter', defaultDataDelimiter, ...
    @(x) any(validatestring(x,validDataDelimiter)));

parse(p, pathName, fileNameArray, varargin{:});

if ~isempty(p.Results.maxNrOfFrames)
    NFrames = p.Results.maxNrOfFrames;
else
    NFrames = numel(p.Results.fileNameArray);
    if NFrames == 0
        this.utils.printToConsole('No FileNames found. Returning')
        return
    end
end

data = struct();
fieldNameArray = cell(size(NFrames));
dataMatrix = cell(size(NFrames));
for fn = 1:NFrames
    % Import data only if frame is not present in the object yet
    % if ~ismember(fn, unique(this.SegmentationData.cell_frame))
    tmpImportedData = importdata(fullfile(p.Results.pathName, ...
        p.Results.fileNameArray{fn}), p.Results.dataDelimiter, 1);
    NTempFieldNames = numel(tmpImportedData.colheaders);
    
    % Convert table heads to valid format. Strings separated by a dot is
    % not a valid struct field name. Use underscore instead.
    currentFrameFieldNames = cell(1,NTempFieldNames);
    for n = 1:NTempFieldNames
        currentFrameFieldNames{n} = strrep(strrep(...
            tmpImportedData.colheaders(n), '.', '_'),'"','');
        if fn == 1
            
            data.(char(currentFrameFieldNames{n})) = tmpImportedData.data(:,n);
        else
            data.(char(currentFrameFieldNames{n})) = [data. ...
                (char(currentFrameFieldNames{n})); tmpImportedData.data(:,n)];
        end
    end
    fieldNameArray{fn} = currentFrameFieldNames;
    dataMatrix{fn} = tmpImportedData.data;
end
end