%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function ret = readCellXParameters(~, file)
% READCELLXPARAMETERS Reads a CellX parameter xml file and stores 
% and returns them as struct. To be used for automatic project
% configuration such as i.e. image crop.
%
% Args:
%    ignoredArg (:obj:`obj`): :class:`IO` instance.
%    file (:obj:`str`):       Path to CellX parameter xml file
%
% Returns
% -------
%    ret (:obj:`struct`):     CellX parameter as struct. 
%
% :Authors:
%    Andreas P. Cuny - initial implementation
        
ret =  struct();
tagName = 'CellXParam';
arrayElemTagName = 'ArrayElement';
nameAttrName = 'name';
valueAttrName = 'value';
charAttrName = 'string';
doc = xmlread(file);
root = doc.getDocumentElement();
paramNodes = root.getElementsByTagName(tagName);
n = paramNodes.getLength;
for i=1:n
    node = paramNodes.item(i-1);
    id = node.getAttribute(nameAttrName);
    hasValue=0;
    value=[];
    if(node.hasChildNodes)
        arrayElements = node.getElementsByTagName(arrayElemTagName);
        cn = arrayElements.getLength;
        value = zeros(1,cn);
        for j=1:cn
            cnode = arrayElements.item(j-1);
            if(cnode.hasAttribute(valueAttrName))
                value(j) = str2double(cnode.getAttribute(valueAttrName));
                hasValue=1;
            end
        end
    else
        if(node.hasAttribute(valueAttrName))
            value = str2double(node.getAttribute(valueAttrName));
            hasValue=1;
        elseif(node.hasAttribute(charAttrName))
            value = char(node.getAttribute(charAttrName));
            hasValue=1;
        end
    end
    if(hasValue)
        ret.(char(id)) = value;
    end
end
end