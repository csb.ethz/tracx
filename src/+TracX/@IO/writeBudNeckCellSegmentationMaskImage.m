%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeBudNeckCellSegmentationMaskImage(~, maskDilated, ...
    foundEdgesFilled, trackIndexArray, cellCenterXArray, cellCenterYArray, ...
    segmentationResultDir, frameNumber, imageCropCoordinateArray)
% WRITEBUDNECKCELLSEGMENTATIONMASKIMAGE Detect bud neck marker from 
% fluorescence images. A ROF denoising is first applied and then an edge 
% detection to get an array of pixel that each bud covers.
%
% Args:
%    ignoredArg (:obj:`obj`):                 :class:`IO` instance.
%    maskDilated (:obj:`array`, NxM):         Segmentation mask. Dilated such that cells touch each others.
%    foundEdgesFilled (:obj:`array`, NxM):    Budneck segmentation mask.
%    trackIndexArray (:obj:`array`, 1xK):     Track index array
%    cellCenterXArray (:obj:`array`, 1xK):    Cell center x array
%    cellCenterYArray (:obj:`array`, 1xK):    Cell center y array
%    segmentationResultDir (:obj:`str`):      Path to segmentation results.
%    frameNumber (:obj:`int`):                Image frame number
%    imageCropCoordinateArray (:obj:`array`): Image crop coordinates
%
% Returns
% -------
%    void (:obj:`-`)
%                                             Writes the output image directly
%                                             to disk.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation
 

% Construct RGB channels
g = double(zeros(size(maskDilated,1), size(maskDilated,2)));
b = double(maskDilated);
r = double(zeros(size(maskDilated,1), size(maskDilated,2)));

g(logical(maskDilated)) = maskDilated(logical(maskDilated));
r(logical(foundEdgesFilled)) = maskDilated(logical(foundEdgesFilled));
rangeGreenIncr = (2^16 - (2^16*0.2)) / max(trackIndexArray);
uniqueTrackArray = unique(g);
uniTracksL = uniqueTrackArray(2:end);
uniqueTrackArray(uniqueTrackArray>0) = rangeGreenIncr;
uniqueTrackArray = cumsum(uniqueTrackArray) + (2^16*0.2);

if exist('changem', 'file') == 2
    g = changem(g, uniqueTrackArray(1:end-1), uniTracksL);
else
   newLabel = uniqueTrackArray(1:end-1);
   [lia, locb] = ismember(g, uniTracksL);
   g(lia) = newLabel(locb(lia));
end

% Construct empty image of correct size and add channel data
img = uint16(zeros(size(maskDilated, 1), size(maskDilated, 2), 3));
img(:,:,1) = r.* ((2^16 - 1) / 1);
img(:,:,2) = g;
img(:,:,3) = b;

% Label the image with track indicies
if ~isempty(imageCropCoordinateArray)
    img = TracX.Utils.imcropC(img, imageCropCoordinateArray);
    % Label the image with track indicies
    imgLabeled = insertText(img, [cellCenterXArray - ...
        imageCropCoordinateArray(1), cellCenterYArray - ...
        imageCropCoordinateArray(2)], ...
        trackIndexArray, 'AnchorPoint', 'Center', 'BoxOpacity', 0);
else
    imgLabeled = insertText(img, [cellCenterXArray, cellCenterYArray], ...
    trackIndexArray, 'AnchorPoint', 'Center', 'BoxOpacity', 0);
end

% Save the image
imwrite(imgLabeled, fullfile(segmentationResultDir, ...
    filesep, sprintf('BudNeckMaskOverlayControl_frame%0.4d.png', ...
    frameNumber)))

end
