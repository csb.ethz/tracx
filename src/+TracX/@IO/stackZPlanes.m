%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ uRemnants ] = stackZPlanes(~, inputFilePath, fileRegEx, ...
    planeRegEx, convert)
% STACKZPLANES Stacks single file z planes into one multipage tiff.
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    inputFilePath (:obj:`str`):  File path to images     
%    fileRegEx (:obj:`str`):      File identifier regular expression
%    planeRegEx (:obj:`str`):     File z plane identifier regular expression
%    convert (:obj:`bool`):       Bool if stack should be converted.
%
% Returns
% -------
%    uRemnants: :obj:`str` 
%                                 Unique remnants.
%
% :Authors:
%    Thomas Kuendig - initial implementation


files = dir(fullfile(inputFilePath,fileRegEx));
filenames = {files.name};

planes = double(string(regexp(filenames, '(?<=position)\d*', 'match')));
remnant = regexp(filenames, '(?<=position)\d*', 'split');
remnant = string(cellfun(@(x) strjoin(x, ''),remnant,'UniformOutput',false));
[uRemnants, ~,idx] = unique(remnant);

if convert 
    for i = unique(idx)'
        curr = idx == i;
        currFilename = uRemnants(i);
        
        currFilesToStack = filenames(curr);
        
        currPlanes = planes(curr);
        [~,currPlaneOrder] = sort(currPlanes);
        
        currFilesToStack = currFilesToStack(currPlaneOrder);
        
        originalFileFolder = 'UnstackedOriginalFiles';
        if ~isfolder(fullfile(inputFilePath, originalFileFolder))
            mkdir(fullfile(inputFilePath, originalFileFolder));
        end
        registerFile = fopen(fullfile(inputFilePath,originalFileFolder,'StackedFileRegister.txt'),'a');
        originalFile = fopen(fullfile(inputFilePath,originalFileFolder,'OriginalFileRegister.txt'),'a');
        fprintf(registerFile,'%s\n',currFilename);
        fclose(registerFile);
        
        for j = 1:length(currFilesToStack)
            
            if j == 1
                imwrite(double(imread(fullfile(inputFilePath,currFilesToStack{j}))),...
                    fullfile(inputFilePath,currFilename));
            else
                imwrite(double(imread(fullfile(inputFilePath,currFilesToStack{j}))),...
                    fullfile(inputFilePath,currFilename), 'writemode', 'append');
            end
            fprintf(originalFile,'%s\n',currFilesToStack{j});
            movefile(fullfile(inputFilePath,currFilesToStack{j}),...
                fullfile(inputFilePath,originalFileFolder,currFilesToStack{j}))
        end
                fclose(originalFile);

    end 
end
end