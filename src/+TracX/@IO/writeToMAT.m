%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function writeToMAT(~, data, resultFilePath, resultFileName, varargin)
%  WRITETOMAT Writes tracker results to disk in mat format. 
%
% Args:
%    ignoredArg (:obj:`obj`):     :class:`IO` instance.
%    data (:obj:`table`):         Table of :class:`TrackerData`.
%    resultFilePath (:obj:`str`): Path to where tracker results
%                                 should be saved.
%    resultFileName (:obj:`str`): Name of result file
%
% Returns
% -------
%    void (:obj:`-`)
%                                 Writes text file to disk
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

[~, ~, fileExtension] = fileparts(resultFileName);
if isempty(varargin)
    dataName = 'data';
else
    dataName = varargin{1};
end
S = struct;
S.(dataName) = data;

if ~isempty(fileExtension) && strcmp(fileExtension, '.mat')
    save(fullfile(resultFilePath, sprintf(...
        '%s', resultFileName)), dataName, '-struct', 'S');
else
    save(fullfile(resultFilePath, sprintf(...
        '%s.mat', resultFileName)), dataName, '-struct', 'S');
end

end