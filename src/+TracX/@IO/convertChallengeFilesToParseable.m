%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function convertChallengeFilesToParseable(~, path, setName)
% CONVERTCHALLENGEFILESTOPARSEABLE Converts files from the tracker challenge 
% into parseable format.
%
% Args:
%    ignoredArg (:obj:`obj`): :class:`IO` instance.
%    path (:obj:`str`):       Path to the images    
%    setName (:obj:`str`):    Name of the dataset.
%
% Returns
% -------
%    void (:obj:`-`)
%                             Converted set saved to disk directly.
%
% :Authors:
%    Thomas Kuendig - initial implementation

files = dir(fullfile(path, '*.tif*'));
originalFilespath = fullfile(path, "OriginalFiles");
mkdir(originalFilespath)
for f = 1:numel(files)
    numberOfZplanes = length(imfinfo(fullfile(path,files(f).name)));
    for p = 1:numberOfZplanes
        current_file = imread(fullfile(path,files(f).name),p);
        imwrite(current_file, fullfile(path,[setName + "_z" + ...
            sprintf("%0.3d",p) + "_" + files(f).name]))
    end
    movefile(fullfile(path,files(f).name), ...
        fullfile(originalFilespath,files(f).name));
end