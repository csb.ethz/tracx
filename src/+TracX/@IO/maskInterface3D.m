%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function maskInterface3D(this, frameNumber, segmentationFile, ...
    imageFile, resultPath, pxPerZInterval, fluoTags, fluoImagePaths)
% MASKINTERFACE3D Interface to import 3D segmentation data from mask files
% into TracX internal data format.
%
% Args:
%    ignoredArg (:obj:`obj`):         :class:`IO` instance.
%    frameNumber (:obj:`int`):        Image frame number   
%    segmentationFile (:obj:`str`):   Name of segmentation file  
%    imageFile (:obj:`str`):          Name of image file  
%    resultPath (:obj:`str`):         Path to where output should be saved. Defaults
%                                     to experiment folder. 
%    pxPerZInterval (:obj:`int`):     Conversion factor pixel to z interval
%    fluoTags (:obj:`str`):           Fluorescent channel identifiers (optional).
%    fluoImagePaths (:obj:`str`):     Path to fluorescent images
%
% Returns
% -------
%    void (:obj:`-`)
%                                      Data written directly to disk.
%
% :Authors:
%    Thomas Kuendig - initial implementation

segHeader = { ...
    'cell.frame', ...
    'cell.index', ...
    'cell.center.x', ...
    'cell.center.y', ...
    'cell.center.z', ...
    'cell.orientation', ...
    'cell.area', ...
    'cell.extent', ...
    'cell.solidity', ...
    'cell.surfacearea', ...
    'cell.equivdiameter' };

segFormat = '%d\t%d\t%d\t%d\t%d\t%.16g\t%d\t%d\t%d\t%d\t%d';

if ~isempty(fluoTags)
    fluoFormat = [repmat('\t%d\t%d\t%d\t%d\t%d\t%d',1,length(fluoTags))];
    
    fluoheadingformat = {'.background.mean', '.background.std',  '.cell.total',	'.cell.q75', '.cell.median', '.cell.q25'};
    for ft = fluoTags
        newHeadings = compose('%s%s', string(ft), string(fluoheadingformat));
        segHeader = cat(2,segHeader,newHeadings); 
    end
end

segFormat = [segFormat];

resultFile = fopen(fullfile(resultPath,["cells_" + frameNumber + ".txt"]),'w');

cells = struct();

%Print Header
for i = 1:numel(segHeader)
    if i < numel(segHeader)
        fprintf(resultFile, '%s\t',  [segHeader{i}]);
    else
        fprintf(resultFile, '%s\n', [segHeader{i}]);
    end
end

infos = imfinfo(segmentationFile);
sizeOfImg = [infos(1).Height infos(1).Width length(infos)];

Mask = zeros(sizeOfImg, 'uint16');
for fl = 1:length(fluoTags)
    FluoImages{i} = zeros(sizeOfImg, 'uint16');
end

for plane = 1:sizeOfImg(3)
    Mask(:,:,plane) = imread(segmentationFile,plane);
    for fl = 1:length(fluoTags)
        FluoImages{fl}(:,:,plane) = imread(fluoImagePaths{fl},plane);
    end
end

% % Mask = Mask > 0;

% if the mask is binary, try to find connected compnents and overwrite
% the original matrix file.
if length(unique(Mask)) <= 2
    V2cc = bwconncomp(Mask,6);
    Mask = labelmatrix(V2cc);
    % if more than one component are found, the file is overwritten with
    % the labelled mask
    if length(unique(Mask)) > 2
        this.writeToTIF(Mask, segmentationFile)
        this.utils.printToConsole(sprintf('labeled and overwritten binary Mask %03d', frameNumber));
    end
end

% find connected components (only conneced by the sides of a cube)
%V2cc = bwconncomp(Mask,6);
%extract region properties
V = regionprops3(Mask,'Image','Centroid','Volume',...
    'Extent','Solidity','SurfaceArea','EquivDiameter', 'BoundingBox');
uniqueLabels = double(unique(Mask));
%this can be used when connected compoenents are already labeled in mask
    for fl = 1:length(fluoTags)
                FluoValues{fl} = regionprops3(Mask,FluoImages{fl},'VoxelValues');
                FluoBackgroundMean(fl) = mean(FluoImages{fl}(:));
                FluoBackgroundStd(fl) = std(double(FluoImages{fl}(:)));
    end
%V = regionprops3(Seg,Image,'all');

if isempty(V)
    %no cells found in mask
    fprintf(resultFile, segFormat, zeros(1,19));
else
    actualCellCounter = 1;
    
    for cell = uniqueLabels(2:end)'
        
%         if (V(cell,:).Volume<5)
%             %ignore artefacts
%             continue
%         end
        
        % orientation select eigenvector with highest eigenvalue
        VEig = regionprops3(repelem(V.Image{cell},1,round(pxPerZInterval)),...
            'EigenVectors','EigenValues');
        
        [~, maxEigenvalue] = max(VEig.EigenValues{:});
        maxEigenvector = VEig.EigenVectors{1}(maxEigenvalue,:);
%         [~, maxEigenvalue] = max(V(cell,:).EigenValues{:});
%         maxEigenvector = V(cell,:).EigenVectors{1}(maxEigenvalue,:);
        
        % normalizing eigenvector to unitlength and compress to
        % orientationtag
        maxEigenvector = maxEigenvector/norm(maxEigenvector);
        orientationTag = this.orientationCompressor(maxEigenvector);
        
        % write region properties to result file
        data = [frameNumber, cell, V(cell,:).Centroid(1), V(cell,:).Centroid(2), ...
            V(cell,:).Centroid(3)*(pxPerZInterval), orientationTag, V(cell,:).Volume, ...
            V(cell,:).Extent V(cell,:).Solidity V(cell,:).SurfaceArea V(cell,:).EquivDiameter];
        fprintf(resultFile, segFormat, data);
        
        if ~isempty(fluoTags)
            fluoData = double([]);
            for fl = 1:length(fluoTags)
                cellFluo = table2array(FluoValues{fl}(cell,:));
                cellFluo = double(cellFluo{:});
                fluoData = [fluoData FluoBackgroundMean(fl) FluoBackgroundStd(fl) sum(cellFluo), quantile(cellFluo,0.25), median(cellFluo),  quantile(cellFluo,0.75)];
            end
            fprintf(resultFile, fluoFormat, fluoData);
        end
        
        fprintf(resultFile, '\n');
        % Create Alpha Shapes
        %extract only surface pixels of cell for more efficient alphashape
        binaryMask = V.Image{cell};
%         for eps = 1:size(binaryMask,3)
%             if eps == 1 || eps==size(binaryMask,3)
%                 continue
%             end
%         binaryMask(:,:,eps) = binaryMask(:,:,eps) - imerode(binaryMask(:,:,eps), true(3));
%         end
        se = strel('cube',3);
        binaryMask = binaryMask & ~ imerode(binaryMask,se);
        [x, y, z] = ind2sub(size(binaryMask),find(binaryMask));
        try
        voxelPos = [y x z] + repmat(V.BoundingBox(cell,1:3),length(x),1);
        catch
            continue
        end
        
        %scale z axis according to custom parameter
        voxelPos(:,3) = voxelPos(:,3)*(pxPerZInterval);
        
        %create alphashape 
        ashape = alphaShape(voxelPos);
        ashape.Alpha = V(cell,:).EquivDiameter*1.5/4;
        
        cells.alphashape{cell}=ashape;
        
        ashape_patch = plot(ashape, 'Visible', 'off');
        ashape_patch_reduced = reducepatch(ashape_patch,0.1);
        cells.alphashape_red{cell} = ashape_patch_reduced;
        actualCellCounter = actualCellCounter + 1;

    end
end

fclose(resultFile);

%export alphashape
this.writeToMAT(cells, resultPath, ["cells_" + frameNumber + ".mat"]);
this.writeToMAT(Mask, resultPath, ["mask_" + frameNumber + ".mat"], 'segmentationMask');
this.utils.printToConsole(["Mask file " + sprintf('%03d',frameNumber) + " has been extracted." ]);

end