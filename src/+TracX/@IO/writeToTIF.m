%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = writeToTIF(~, image, fullPath )
% WRITETOTIF Writes an image matrix as tif file to disk.
%
% Args:
%    ignoredArg (:obj:`obj`):    :class:`IO` instance.
%    image (:obj:`array`, NxM):  Image matrix
%    fullPath (:obj:`str`):      Path to where the image will be
%                                saved.
%
% Returns
% -------
%    void (:obj:`-`)
%                                Writes image file to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

[base, fileName, fileExtension] = fileparts(fullPath);


if ~isempty(fileExtension) && strcmp(fileExtension, '.tif')
    imwrite(squeeze(image(:,:,1,:)), fullPath);
else
    imwrite(squeeze(image(:,:,1,:)), fullfile(base,fileName,'.tif'));
end

for i = 2:size(image,3)
    if ~isempty(fileExtension) && strcmp(fileExtension, '.tif')
        imwrite(squeeze(image(:,:,i,:)), fullPath, 'writemode', 'append');
    else
        imwrite(squeeze(image(:,:,i,:)), fullfile(base,fileName,'.tif'), ...
            'writemode', 'append');
    end
end

end
