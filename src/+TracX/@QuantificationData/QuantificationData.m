%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef QuantificationData < handle & dynamicprops
    % QUANTIFICATIONDATA This class contains all the quantification
    % data of the configured Tracker project for a single experiment (i.e.
    % one well position of a multi well experiment.) The QuantificationData
    % is mapped to the SegmentationData via a primary key stored in the
    % uuid property.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties (SetAccess=private)
        uuid % Primary key
    end
    
    properties
        % Fluorescence quantification data
        
        
        fluo_id
        % Id of fluorescence channel [int].   
        fluo_name
        % Fluorescence channel name [string]    
        fluo_background_mean
        % Mean background fluorescence [a.u.]     
        fluo_background_std
        % Standard deviation of background fluorescence [a.u.]        
        fluo_cell_total
        % Total cell fluorescence [a.u.]     
        fluo_cell_q75
        % Quantile 75 of cell fluorescence [a.u.]      
        fluo_cell_q50
        % Quantile 50 of cell fluorescence [a.u.]      
        fluo_cell_q25
        % Quantile 25 of cell fluorescence [a.u.]      
        fluo_mem_total
        % Total cell membrane fluorescence [a.u.]     
        fluo_mem_q75
        % Quantile 75 cell membrane fluorescence [a.u.]
        fluo_mem_q50
        % Quantile 50 of cell fluorescence [a.u.]       
        fluo_mem_q25
        % Quantile 25 of cell fluorescence [a.u.]        
        fluo_nuc_total
        % Total cell nucleus fluorescence [a.u.]        
        fluo_nuc_q75
        % Quantile 75 of cell nucleus fluorescence [a.u.]       
        fluo_nuc_q50
        % Quantile 50 of cell nucleus fluorescence [a.u.]        
        fluo_nuc_q25
        % Quantile 25 of cell nucleus fluorescence [a.u.]       
        fluo_bright_total
        % Brightest pixel of whole cell fluorescence [a.u.]       
        fluo_bright_q75
        % Quantile 75 brightest pixel fluorescence [a.u.]      
        fluo_bright_q50
        % Quantile 50 brightest pixel fluorescence [a.u.]       
        fluo_bright_q25
        % Quantile 25 brightest pixel fluorescence [a.u.]
        fluo_bright_euler
        % Euler number of brightest pixel fluorescence [a.u.]
    end
    
    methods
        
        % Constructor
        function obj = QuantificationData()
            % QUANTIFICATIONDATA constructs an empty object of
            % type :class:`QuantificationData`. This object acts as datastorage
            % for fluorescence quantification data for a TracX Project.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                                   Returns a :class:`QuantificationData`
            %                                   instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            
            % Primary key
            obj.uuid = [];
            % Fluorescence quantification data
            obj.fluo_id = [];
            obj.fluo_name = [];
            obj.fluo_background_mean = [];
            obj.fluo_background_std = [];
            obj.fluo_cell_total = [];
            obj.fluo_cell_q75 = [];
            obj.fluo_cell_q50 = [];
            obj.fluo_cell_q25 = [];
            obj.fluo_mem_total = [];
            obj.fluo_mem_q75 = [];
            obj.fluo_mem_q50 = [];
            obj.fluo_mem_q25 = [];
            obj.fluo_nuc_total = [];
            obj.fluo_nuc_q75 = [];
            obj.fluo_nuc_q50 = [];
            obj.fluo_nuc_q25 = [];
            obj.fluo_bright_total = [];
            obj.fluo_bright_q75 = [];
            obj.fluo_bright_q50 = [];
            obj.fluo_bright_q25 = [];
            obj.fluo_bright_euler = [];
        end
        
        function setUUID(this, value)
            % SETUUID Sets a unique id for each segmented cell per
            % image frame such that the QuantificationData is mapped to the
            % SegmentationData object via a primary key stored in the
            % uuid property.
            %
            % Args:
            %    this (:obj:`object`):          :class:`QuantificationData` instance.
            %    value (:obj:`int`):            Value to be set for uuid
            this.uuid = value;
            % Returns
            % -------
            %    void (:obj:`-`)
        end
        
        function array = getFieldNameForFluoChannel(this, fieldName, fluoID)
            fieldNames = fieldnames(this);
            % GETFIELDNAMEFORFLUOCHANNEL Returns the data array for the
            % specified fieldname and fluorescence channel. The fluo channel
            % can be given by its name or id (fluo_id)
            %
            % Args:
            %    this (:obj:`object`):           :class:`QuantificationData` instance.
            %    fieldName (:obj:`str`):         Field name of
            %                                    :class:`QuantificationData`.
            %    fluoID (:obj:`str|int`):        Fluorescence channel
            %                                    identifier either as
            %                                    numerical id or its name
            %                                    (i.e. 'GFP').
            % Returns
            % -------
            %     array: :obj:`array`
            %                                    Array of data for a given
            %                                    fluorescence channel and
            %                                    fieldName.
            ids = unique(this.fluo_id);
            names = unique(this.fluo_name);
            if isempty(names) || isempty(ids)
                disp('No quantification data found in QuantificationData.')
                array = [];
                return
            end
            if ischar(fluoID)
                if any(ismember(names, fluoID)) == 1
                    fluoID = find(ismember(this.fluo_name, fluoID) == 1, ...
                        1, 'first');
                else
                    error(['The given fluo id does not exist in' ...
                        ' QuantificationData. Valid fluo ids are: %s.'], ...
                        strjoin(names, ', '))
                end
            elseif isnumeric(fluoID)
                if any(ismember(ids, fluoID)) == 1
                    
                else
                    idsC = num2cell(ids);
                    idsC = cellfun(@(x) num2str(x), idsC, 'UniformOutput', ...
                        false);
                    error(['The given fluo id does not exist in' ...
                        ' QuantificationData. Valid fluo ids are: %s.'], ...
                        strjoin(idsC, ', '))
                end
            end
            if any(ismember(fieldNames, fieldName)) == 1
                array = this.(fieldName)(this.fluo_id == fluoID);
            else
                error(['The given field name does not exist in' ...
                    ' QuantificationData. Valid field names are: %s.'], ...
                    strjoin(fieldNames, ', '))
            end
        end
    end
    
end