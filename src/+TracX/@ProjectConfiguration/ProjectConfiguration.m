%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef ProjectConfiguration < handle
    % PROJECTCONFIGURATION This class creates an object containing
    % all information required for a tracking an experiment such as
    % path to image files, segmentation results and where to save
    % tracking results for the Tracker.
    % It tries to identify the number and type of image channels as
    % well as well positions from the image filenames directly. The
    % convention used is the following:
    % ChannelID_WellPosition_ImageFrameNumber.FileType. Where as the
    % ChannelID defines the different image modalities such as BrightField,
    % Fluorescence channels such as e.g. GFP, YFP, PC (phase contrast) etc.
    % WellPosition is 000000 if only one position was imaged or if a multi well
    % experiment was performet it is based on well plate derived format in
    % decimal system e.g. 030200 (e.g. C2; C = '03' and 2 = '02' and no z
    % level = '00'). ImageFrameNumber is the number describing the image frame
    % in a long term experiment. FileType is the file type used. Default is
    % tiff. (YouScope naming convenction).
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties (SetAccess=private, GetAccess=public)
        
        projectName % Name of ccurrent project (i.e. experiment name).
        cellDivisionType = -1 % Cell division type
        imageDir % Image directory to be used in the current project.
        segmentationResultDir % Directory containing the segmentation results for the current project. Images in imageDir have to be segmented with e.g. CellX prior to tracking.
        imageFileArray = {}; % Array of all image file names
        imageFingerprintFileArray = {}; % Array of (ideally) out-of-focus Brigthfield image file names to be used to compute unique fingerprints. In fact any channel can be used.
        fluoFileArray = {}; % Array of all fluorescent image file names
        fluoLineageFileArray = {}; % Array of all fluorescent image file names needed to reconstruct the cell lineage
        segmentationResultFileArray = {}; % Array of all segmentation result text file names
        segmentationResultMatFileArray = {}; % Array of all segmentation result mat file names
        segmentationResultMaskFileArray = {}; % Array of all segmentation result mask file names
        segmentationControlFileArray = {}; % Array of all segmentation control image file names
        imageCropCoordinateArray = []; % Array with crop coordinates used during segmentation. Needed to map segmentation cell centers to correct position within the raw images (for CellX segmentation data).
        imageCropCoordinatePriorReversionArray = []; % Array with crop coordinates used during segmentation as given previous to reversion mapping to raw data size.
        imagePositionArray = {}; % All experiment positions (based on well plate derived format in decimal system e.g. 0302 (C = '03' and 2 = '02') C2.
        imagePrefixArray = {}; % All image prefixes of the experiment.
        imageFrameNumberArray = {}; % All image frame numbers of the experiment identifying each existing timepoint.
        imageWidth = []; % Image width
        imageHeight = []; % Image height
        imageDepth = []; % Image number of Z planes
        imageBitDepth = []; % Image bit depth
        imageMaxSampleValue = []; % Image max sample value
        imageMinSampleValue = []; % Image min sample value
        fluoImageIdentifier = {}; % Identifiers for fluorescence images (e.g. gfp, yfp).
        trackerResultsDir = []; % The directory where the tracking results should be saved.
        trackerResultMaskFileArray = {}; % Array of all tracking result mask file names (relabeled segmentation masks [i.e cell_index -> track_index]).
        trackerStartFrame = []; % Starting frame number of where the tracker should start.
        trackerEndFrame = []; % End frame number where the tracker should terminate.
        nFramesToImport = []; % Number of frames to import. If for e.g. not all files should be tracked.
        trackerPrefix = 'tracked' % Prefix for tracking outputs.
        maskPrefix = 'mask' % Prefix for segmentation mask files.
        controlPrefix = 'control' % Prefix for segmentation control files.
        fontSize = 14 % Default fontsize for plotting.
        imageFileFormat = 'tif' % Default imageFileFormat for image and mask import / export.
        
    end
    
    methods
        
        % Constructor
        function obj = ProjectConfiguration()
            % PROJECTCONFIGURATION Constructs an empty ProjectConfiguration
            % object.
            %
            % Returns:
            %    this (:obj:`object`): :class:`ProjectConfiguration` instance
        end
        
        function this = createProjectConfiguration(this, projectName, imageDir, ...
                segmentationResultDir, fileIdentifierFingerprintImages, ...
                fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...
                imageCropCoordinateArray, varargin)
            
            % CREATEPROJECTCONFIGURATION creates a ProjectConfiguration
            % object for a given TracX project
            %
            % Args:
            %    this (:obj:`object`):                               :class:`ProjectConfiguration` instance.
            %    projectName (:obj:`str`):                           Name of project/experiment.
            %    imageDir (:obj:`str`):                              Image directory to
            %                                                        be used in the
            %                                                        current project.
            %    segmentationResultDir (:obj:`str`):                 Directory containing
            %                                                        the segmentation
            %                                                        results for the
            %                                                        current project.
            %    fileIdentifierFingerprintImages (:obj:`str`):       File name identifier
            %                                                        for raw Brightfield
            %                                                        images.
            %    fileIdentifierWellPositionFingerprint (:obj:`str`): File name
            %                                                        identifier
            %                                                        specifing the well
            %                                                        position (of a multi
            %                                                        well experiment).
            %    fileIdentifierCellLineage (:obj:`str`):             File name identifier
            %                                                        of the fluorescent
            %                                                        channel images to
            %                                                        be used to reconstruct
            %                                                        the cell lineage
            %                                                        (i.e. bud neck marker)
            %    imageCropCoordinateArray (:obj:`array`, 1x4):       Array with crop coordinates
            %                                                        used during segmentation.
            %    varargin (:obj:`str varchar`):
            %
            %        * **nFramesToImport** (:obj:`int`):             Number of frames to import.
            %        * **setCellDivisionType** (:obj:`str`):         Type of cell division.
            %
            %
            % .. note::
            %
            %    The images used for fingerprinting (image filenames)
            %    can be specified explicitly (full path) and do not need to
            %    reside within the segmentation results dir.
            %
            % Returns
            % -------
            %    this (:obj:`object`)
            %                                            :class:`ProjectConfiguration`
            %                                            instance  with all the
            %                                            information needed
            %                                            for tracking.
            
            try
                this.projectName = projectName;
                this.imageDir = imageDir;
                this.segmentationResultDir = segmentationResultDir;
                this.setTrackerResultsDirectory(segmentationResultDir)
                this.setImageFileArray()
                this.setSegmentationFiles()
                this.setSegmentationControlFileArray()
                this.setMatFileNames()
                this.setSegmentationResultMaskFileArray()
                this.setTrackerResultMaskFileArray()
                this.setImageIdentifiers()
                
                
                if numel(this.imagePositionArray) == 1
                    fileIdentifierWellPositionFingerprint = this.imagePositionArray{1};
                end
                
                this.setFingerprintImageNames(fileIdentifierFingerprintImages, ...
                    fileIdentifierWellPositionFingerprint)
                this.setFluoLineageFileArray(fileIdentifierCellLineage, ...
                    fileIdentifierWellPositionFingerprint)
                this.setImageCropCoordinateArray(imageCropCoordinateArray)
                uniqueFrames = unique(this.imageFrameNumberArray);
                this.setTrackerStartFrame(min(uniqueFrames))
                this.setImageProperties();
                if isempty(varargin{1})
                    this.setNFramesToImport(max(uniqueFrames))
                    this.setTrackerEndFrame(max(uniqueFrames))
                else
                    this.setNFramesToImport(varargin{1})
                    this.setTrackerEndFrame(varargin{1})
                end
                if ~isempty(varargin{2})
                    this.setCellDivisionType(varargin{2});
                end
            catch ME
                disp(ME)
            end
        end
        
        function setProjectName(this, projectName)
            % SETPROJECTNAME Sets the directory to all the images of the
            % TracX project.
            %
            % Args:
            %    this (:obj:`object`):          :class:`ProjectConfiguration` instance.
            %    projectName (:obj:`str`): Name of project/experiment.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.projectName = projectName;
        end
        
        function setCellDivisionType(this, cellDivisionType)
            % SETCELLDIVISIONTYPE Sets the cell division type for the
            % given TracX project. Symmetrical division is denoted as 0 where
            % as asymmetrical division with 1.
            %
            % Args:
            %    this (:obj:`object`):          :class:`ProjectConfiguration` instance.
            %    cellDivisionType (:obj:`str`): Cell division type.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            validDivisionType = {'sym', 'symmetrical', 'symmetric', 'Symmetrical', ...
                'asym', 'asymmetrical', 'asymmetric', 'Asymmetrical', '-1', '0', '1'};
            
            p = inputParser;
            p.addRequired('this', @isobject);
            p.addRequired('cellDivisionType', ...
                @(x) any(validatestring(string(x), validDivisionType)))
            parse(p, this, cellDivisionType)
            
            if any(strcmp(string(p.Results.cellDivisionType), ...
                    {'sym', 'symmetrical', 'symmetric', 'Symmetrical', '0'}))
                this.cellDivisionType = 0;
            elseif any(strcmp(string(p.Results.cellDivisionType), ...
                    {'asym', 'asymmetrical', 'asymmetric', 'Asymmetrical', '1'}))
                this.cellDivisionType = 1;
            else
                this.cellDivisionType = -1; % default, not set.
            end
        end
        
        function setImageDir(this, path)
            % SETIMAGEDIR Sets the directory to all the images of the
            % TracX project.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ProjectConfiguration` instance.
            %    path (:obj:`str`):         Path to project images.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageDir = path;
        end
        
        function setSegmentationResultDir(this, path)
            % SETSEGMENTATIONRESULTDIR Sets the directory to all the
            % segmentation results of the TracX project.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ProjectConfiguration` instance.
            %    path (:obj:`str`):         Path to project images.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.segmentationResultDir = path;
        end
        
        function setImagePrefixArray(this, prefixArray)
            % SETIMAGEPREFIXARRAY Sets all the image prefixes.
            % According to the file name convention:
            % ChannelID_WellPosition_ImageFrameNumber.FileType
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    prefixArray (:obj:`str`):   Image prefix array.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imagePrefixArray = prefixArray;
        end
        
        function setImageFrameNumberArray(this, frameArray)
            % SETIMAGEFRAMENUMBERARRAY Sets all the image frame numbers.
            % According to the file name convention:
            % ChannelID_WellPosition_ImageFrameNumber.FileType
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    frameArray (:obj:`int`):    Image frame number array.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageFrameNumberArray = cell2mat(frameArray);
        end
        
        function setImageFileArray(this, varargin)
            % SETIMAGEFILEARRAY Sets all the image names
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    varargin (:obj:`array`):    Optional array with image
            %                                filenames.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if isempty(varargin)
                if ~isempty(dir(fullfile(this.imageDir, lower(['*.', this.imageFileFormat]))))
                    imageFilesTmp = dir(fullfile(this.imageDir, lower(['*.', this.imageFileFormat])));
                else
                    imageFilesTmp = dir(fullfile(this.imageDir, upper(['*.', this.imageFileFormat])));
                end
                % Filter out potential hidden files
                imageFilesTmp = imageFilesTmp(arrayfun(@(x) ~strcmp(x.name(1),'.'), ...
                    imageFilesTmp));
                this.imageFileArray = {imageFilesTmp.name};
            else
                this.imageFileArray = varargin{1}; %vertcat(varargin{:});
            end
        end
        
        function setImageFingerprintFileArray(this, fileNames)
            % SETIMAGEFINGERPRINTFILEARRAY Sets all the image names to
            % be used for fingerprint computation.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    fileNames (:obj:`str`):     File names.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageFingerprintFileArray = fileNames;
        end
        
        function setSegmentationResultFileArray(this, fileNames)
            % SETSEGMENTATIONRESULTFILEARRAY Sets all the text file names
            % of the segmentation results.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    fileNames (:obj:`str`):     File names.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.segmentationResultFileArray = fileNames;
        end
        
        function setSegmentationResultMatFileArray(this, fileNames)
            % SETSEGMENTATIONRESULTMATFILEARRAY Sets all the mat file names
            % of the segmentation results.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    fileNames (:obj:`str`):     File names.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.segmentationResultMatFileArray = fileNames;
        end
        
        function setMatFileNames(this)
            % SETMATFILEMANES Sets all the mat file names.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            matFileNamesTmp = dir(fullfile(this.segmentationResultDir, ...
                'cells*.mat'));
            this.segmentationResultMatFileArray = ...
                TracX.ExternalDependencies.natsortfiles({...
                matFileNamesTmp.name});
        end
        
        function setSegmentationResultMaskFileArray(this, varargin)
            % SETSEGMENTATIONRESULTMASKFILEARRAY Sets all the
            % segmentation mask file names.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    varargin (:obj:`array`, ):  Array with mask filenames.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if isempty(varargin)
                MaskFileNamesTmp = dir(fullfile(this.segmentationResultDir, ...
                    'mask*.mat'));
                this.segmentationResultMaskFileArray = ...
                    TracX.ExternalDependencies.natsortfiles({...
                    MaskFileNamesTmp.name});
            else
                this.segmentationResultMaskFileArray = varargin{1};
            end
        end
        
        function setTrackerResultMaskFileArray(this, varargin)
            % SETTRACKERRESULTMASKFILEARRAY Sets all the tracker mask
            % file names.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    varargin (:obj:`array`, ):  Array with tracker mask filenames.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if isempty(varargin)
                this.trackerResultMaskFileArray = cellfun(@(x) ['track_' x], ...
                    this.segmentationResultMaskFileArray, 'UniformOutput', ...
                    false);
            else
                this.trackerResultMaskFileArray = varargin{1};
            end
        end
        
        function setSegmentationFiles(this)
            % SETSEGMENTATIONFILES Sets all the segmentation file names
            % in .txt format (based on CellX segmentation software output)
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            segFiles = dir(fullfile(this.segmentationResultDir, ...
                'cells*.txt'));
            this.segmentationResultFileArray = ...
                TracX.ExternalDependencies.natsortfiles({segFiles.name});
        end
        
        function setSegmentationControlFileArray(this, varargin)
            % SETSEGMENTATIONCONTROLFILEARRAY Sets all the segmentation
            % control files.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    varargin (:obj:`array`, ):  Array with control filenames.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if isempty(varargin)
                segControlFiles = dir(fullfile(this.segmentationResultDir, ...
                    'control*.png'));
                this.segmentationControlFileArray = {segControlFiles.name};
            else
                this.segmentationControlFileArray = varargin{1};
            end
        end
        
        function setImageCropCoordinateArray(this, crop)
            % SETIMAGECROPCOORDINATEARRAY Sets the segmentation image
            % crop coordinates in case not the entire image frame was
            % segmented.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    crop (:obj:`array`, 1x4):   Segmentation crop array
            %                                [x, y, width, height].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if iscell(crop) == 1
                this.imageCropCoordinateArray = cell2mat(crop);
            else
                this.imageCropCoordinateArray = crop;
            end
        end
        
        function setImageCropCoordinatePriorReversionArray(this, crop)
            % SETIMAGECROPCOORDINATEPRIORREVERSIONARRAY Sets the
            % segmentation image crop coordinates in case not the entire image
            % frame was segmented and the reversion was applied to map them back
            % to the raw data size.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration` instance.
            %    crop (:obj:`array`, 1x4):   Segmentation crop array
            %                                [x, y, width, height].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if iscell(crop) == 1
                this.imageCropCoordinatePriorReversionArray = cell2mat(crop);
            else
                this.imageCropCoordinatePriorReversionArray = crop;
            end
        end
        
        function setImageIdentifiers(this)
            % SETIMAGEIDENTIFIERS Sets all the image identifiers. The
            % convention is ChannelID_WellPosition_ImageFrameNumber.FileType.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration` instance
            
            % @todo: Enable regex as arguments /filters; as different for
            % every movie
            imagePrefixes = regexpi(this.imageFileArray, ...
                '^[a-z]+(+?-?5?)?','match');
            imagePositions = regexpi(this.imageFileArray, ...
                'position+\d+','match');
            imageFrameNumbers = regexpi(this.imageFileArray, ...
                '[[0-9]*]*(?=[[0-9]*]*.tif$)','match');
            imageFrameNumbers = cellfun(@(x) str2double(cell2mat(x)), ...
                imageFrameNumbers);
            this.imagePrefixArray = unique(cat(2,imagePrefixes{:}));
            this.imagePositionArray = unique(cat(2,imagePositions{:}));
            this.imageFrameNumberArray = unique(imageFrameNumbers);
        end
        
        function setTrackerResultsDirectory(this, path)
            % SETTRACKERRESULTSDIRECTORY Sets the tracker results
            % directory.
            %
            % Args:
            %    this (:obj:`object`):  :class:`ProjectConfiguration` instance.
            %    path (:obj:`str`):     Path to tracker results dir.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if( path(end-1:end)~=filesep )
                path = [path filesep];
            end
            this.trackerResultsDir = path;
            this.ensureResultsDirExists();
        end
        
        function setFingerprintImageNames(this, fileIdentFingerprintImages, ...
                fileIdentWellPosFingerPrint)
            % SETFINGERPRINTIMAGENAMES Sets the image file names used
            % for the fingerprint computation.
            %
            % Args:
            %    this (:obj:`object`):                     :class:`ProjectConfiguration`
            %                                              instance.
            %    fileIdentFingerprintImages (:obj:`str`):  File name identifier
            %                                              of images to be used
            %                                              for fingerprint computation
            %    fileIdentWellPosFingerPrint (:obj:`str`): File name identifier
            %                                              of images to be used
            %                                              for fingerprint computation
            %                                              specifing the position
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            channelIdx = strfind(this.imageFileArray, fileIdentFingerprintImages);
            channelSubsetAllPos = this.imageFileArray(not(...
                cellfun('isempty', channelIdx)));
            positionIdx = strfind( channelSubsetAllPos, ...
                fileIdentWellPosFingerPrint);
            
            if ~isempty(fileIdentWellPosFingerPrint)
                positionIdx = strfind( channelSubsetAllPos, ...
                    fileIdentWellPosFingerPrint);
                this.imageFingerprintFileArray = channelSubsetAllPos(not(...
                    cellfun('isempty', positionIdx)));
            else
                this.imageFingerprintFileArray = channelSubsetAllPos;
            end
            this.imageFingerprintFileArray =  ...
                TracX.ExternalDependencies.natsort(this.imageFingerprintFileArray);
        end
        
        function setFluoLineageFileArray(this, fileIdentifierCellLineage, ...
                fileIdentWellPos)
            % SETFLUOLINEAGEFILEARRAY Sets the fluorescence image files
            % used for mother-daughter relationship detection.
            %
            % Args:
            %    this (:obj:`object`):                   :class:`ProjectConfiguration`
            %                                            instance.
            %    fileIdentifierCellLineage (:obj:`str`): File name prefix/
            %                                            identifier cell lineage.
            %    fileIdentWellPos (:obj:`str`):          File name prefix/
            %                                            identifier position.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            if ischar(fileIdentifierCellLineage)
                channelIdx = strfind( this.imageFileArray, fileIdentifierCellLineage);
                channelSubsetAllPos = this.imageFileArray(not(...
                    cellfun('isempty', channelIdx)));
                this.addFluoImageTag(this.fluoLineageFileArray, ...
                    fileIdentifierCellLineage)
                
                if ~isempty(fileIdentWellPos)
                    this.fluoLineageFileArray{1, 1} = channelSubsetAllPos( ...
                        contains(channelSubsetAllPos, fileIdentWellPos));
                else
                    this.fluoLineageFileArray{1, 1} = this.imageFileArray(not(...
                        cellfun('isempty', channelSubsetAllPos)));
                end
                
            elseif iscell(fileIdentifierCellLineage)
                for k = 1:numel(fileIdentifierCellLineage)
                    channelIdx = strfind( this.imageFileArray, fileIdentifierCellLineage{k});
                    channelSubsetAllPos = this.imageFileArray(not(...
                        cellfun('isempty', channelIdx)));
                    
                    if ~isempty(fileIdentWellPos)
                        this.fluoLineageFileArray{k, 1} = channelSubsetAllPos( ...
                            contains(channelSubsetAllPos, fileIdentWellPos));
                        this.setFluoImageIdentifier(fileIdentifierCellLineage{k})
                    else
                        this.fluoLineageFileArray{k, 1} = channelSubsetAllPos(not(...
                            cellfun('isempty', channelSubsetAllPos)));
                        this.setFluoImageIdentifier(fileIdentifierCellLineage{k})
                    end
                end
            end
        end
        
        function setTrackerStartFrame(this, f)
            % SETTRACKERSTARTFRAME Sets the end frame of the current
            % tracking project.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    f  (:obj:`int`):          First image frame number of
            %                              track.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.trackerStartFrame = f;
        end
        
        function setTrackerEndFrame(this, f)
            % SETTRACKERENDFRAME Sets the end frame of the current tracking
            % project.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    f  (:obj:`int`):          Last image frame number of
            %                              track.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.trackerEndFrame = f;
        end
        
        function setImageWidth(this, val)
            % SETIMAGEWIDTH Sets the image width.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        Image width (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageWidth = val;
        end
        
        function setImageHeight(this, val)
            % SETIMAGEHEIGHT Sets the image height.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        Image height (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageHeight = val;
        end
        
        function setImageDepth(this, val)
            % SETIMAGEDEPHT Sets the image depth (number of z planes).
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        Depth (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageDepth = val;
        end
        
        function setImageBitDepth(this, val)
            % SETIMAGEBITDEPTH Sets the image bit depth.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        BitDepth (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageBitDepth = val;
        end
        
        function setImageMaxSampleValue(this, val)
            % SETIMAGEMAXSAMPLEVALUE Sets the image max sample value.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        MaxSampleValue (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageMaxSampleValue = val;
        end
        
        function setImageMinSampleValue(this, val)
            % SETIMAGEMINSAMPLEVALUE Sets the image min sample value.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    val  (:obj:`int`):        MinSampleValue (imfinfo)
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageMinSampleValue = val;
        end
        
        function setImageProperties(this)
            % SETIMAGEPROPERTIES Sets the image properties such as bit
            % depth, image height and width and min/max sample value.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            imageInfo = imfinfo(fullfile(this.imageDir, ...
                this.imageFingerprintFileArray{1}));
            this.setImageDepth(length(imageInfo));
            if length(imageInfo)>1
                imageInfo = imageInfo(1);
            end
            this.setImageWidth(imageInfo.Width);
            this.setImageHeight(imageInfo.Height);
            this.setImageBitDepth(imageInfo.BitDepth);
            this.setImageMaxSampleValue(imageInfo.MaxSampleValue);
            this.setImageMinSampleValue(imageInfo.MinSampleValue);
        end
        
        function setNFramesToImport(this, nFrames)
            % SETNFRAMESTOIMPORT Sets max nr of image frames to import.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    nFrames (:obj:`int`):     Max image frame number to import.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.nFramesToImport = nFrames;
        end
        
        function setFontSize(this, fontSize)
            % SETFONTSIZE Sets label font size
            %
            % Args:
            %    this (:obj:`object`):        :class:`ProjectConfiguration`
            %                                 instance.
            %    fontSize (:obj:`str`):       Label font size.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.fontSize = fontSize;
        end
        
        function setImageFileFormat(this, imageFileFormat)
            % SETIMAGEFILEFORMAT Sets the image file format.
            %
            % Args:
            %    this (:obj:`object`):        :class:`ProjectConfiguration`
            %                                 instance.
            %    imageFileFormat (:obj:`str`):       Image file format. Defaults to 'tif'
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imageFileFormat = imageFileFormat;
        end
        
        function setTrackerPrefix(this, trackerPrefix)
            % SETTRACKERPREFIX Sets tracker output filename prefix
            %
            % Args:
            %    this (:obj:`object`):        :class:`ProjectConfiguration`
            %                                 instance.
            %    trackerPrefix (:obj:`str`):  Tracker output file name
            %                                 prefix.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.trackerPrefix = trackerPrefix;
        end
        
        function setMaskPrefix(this, maskPrefix)
            % SETMASKPREFIX Sets mask output filename prefix
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration`
            %                                instance.
            %    maskPrefix (:obj:`str`):    Mask file name prefix.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.maskPrefix = maskPrefix;
        end
        
        function setControlPrefix(this, controlPrefix)
            % SETCONTROLPREFIX Sets control output filename prefix
            %
            % Args:
            %    this (:obj:`object`):       :class:`ProjectConfiguration`
            %                                instance.
            %    controlPrefix (:obj:`str`): Control file name prefix.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.controlPrefix = controlPrefix;
        end
        
        function setFluoImageIdentifier(this, tag)
            % SETFLUOIMAGEIDENTIFIER Sets the fluorescence channel tags /
            % identifiers to the array.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    tag (:obj:`str`):         Fluorescence image file
            %                              name channel identifier.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.fluoImageIdentifier{end+1, 1} = tag;
        end
        
        function setFluoFileArray(this, array)
            % SETFLUOFILEARRAY Sets the fluorescence image array.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    array (:obj:`str`):       Array of fluorescence
            %                              image file names.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.fluoFileArray = array;
        end
        
        function setImagePositionArray(this, pos)
            % SETIMAGEPOSITIONARRAY Sets the image position array of
            % multi well experiments.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    pos (:obj:`str`):         Array with well positions names.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.imagePositionArray = pos;
        end
        
        function fmt = getControlImageFormat(this)
            % GETCONTROLIMAGEFORMAT Returns the image file format of
            % the segmentation control images.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %
            % Returns
            % -------
            %    fmt (:obj:`str`)
            %                              Retunrs the image format.
            
            [~ , ~, ext] = fileparts(this.controlImageFile);
            if( length(ext)>2 )
                fmt = ext(2:end);
            else
                fmt = 'jpg';
            end
        end
        
        function addFluoImage(this, fluoImgFile)
            % ADDFLUOIMAGE Adds the fluo image identifier.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    fluoImgFile (:obj:`str`): Fluorescence image file name.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            [~, name] = fileparts(fluoImgFile);
            tag = regexp(name, '^(.+?)_', 'tokens');
            if(numel(tag)~=1)
                error(['Cannot parse fluo tag, please use', ...
                    'addFluoImageTag(file, tag) method']);
            else
                this.addFluoImageTag(fluoImgFile, char(tag{1}));
            end
        end
        
        function addFluoImageTag(this, fluoImgFile, tag)
            % ADDFLUOIMAGETAG Adds the fluorescence channel tags /
            % identifiers to the array.
            %
            % Args:
            %    this (:obj:`object`):     :class:`ProjectConfiguration`
            %                              instance.
            %    fluoImgFile (:obj:`str`): Fluorescence image file name.
            %    tag (:obj:`str`):         Fluorescence image file
            %                              name channel identifier.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.fluoImageIdentifier{end+1} = tag;
            this.fluoFileArray{end+1} = fluoImgFile;
        end
        
        function nfluoImageIdentifier = getNumberOfFluoImages(this)
            % GETNUMBEROFFLUOIMAGES Returns the number of fluorescence
            % channels.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration` instance
            %
            % Returns
            % -------
            %    nfluoImageIdentifier (:obj:`int`)
            %                          Number of fluorescence
            %                          channels in thisexperiment.
            nfluoImageIdentifier = numel(this.fluoImageIdentifier);
        end
        
        
        function fluoID = getFluoID(this, fluoIdx)
            % GETFLUOID Returns the fluorescence channel identifier
            % at the given index.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration` instance
            %    idx (:obj:`int`):     Index of fluorescence channel
            %                          in this experiment.
            %
            % Returns
            % -------
            %    fluoID (:obj:`str`)
            %                          Name of fluorescence channel
            %                          at fluoIdx position.
            fluoID = this.fluoImageIdentifier{fluoIdx};
        end
        
        function iFluoImage = getFluoImage(this, idx)
            % GETFLUOIMAGE Returns the folorescence image at the given
            % index.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration`
            %                          instance.
            %    idx (:obj:`int`):     Index of a fuorescence image
            %                          in the fluoFileArray array.
            %
            % Returns
            % -------
            %    iFluoImage (:obj:`str`)
            %                           Returns the fluorescence image name
            %                           at idx.
            iFluoImage = this.fluoFileArray{idx};
        end
        
        function ensureResultsDirExists(this)
            % ENSURERESULTSDIREXISTS Tests if project result dir exists.
            % If not it creates the directory.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration`
            %                          instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            if( ~exist(this.trackerResultsDir, 'dir') )
                mkdir(this.trackerResultsDir);
            end
        end
        
        function checkProjectConfiguration(this)
            % CHECKPROJECTCONFIGURATION Checks if all values in the
            % ProjectConfiguration object are defined.
            %
            % Args:
            %    this (:obj:`object`): :class:`ProjectConfiguration`
            %                          instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            TracX.Utils.printToConsole('CHECK: New TracX project configuration...')
            
            %  check for matlab > 2009, assignment of ~
            if verLessThan('matlab', '7.9')
                error(['TracX requires MATLAB 7.9 or higher. You can use the' ...
                    'deployed version and the MATLAB Compiler Runtime (MCR) if' ...
                    'your MATALB version is too old']);
            end
            
            %  check if defined
            if( isempty(this.projectName) )
                error('Undefined value for projectName.');
            end
            
            if( isempty(this.imageFingerprintFileArray) )
                ME = MException('ProjectConfiguration:fileNotFound', ...
                    'Undefined value for imageFingerprintFileArray. Terminating here.');
                error(getReport(ME))
                return
            end
            
            if( isempty(this.segmentationResultDir) )
                error('Undefined value for segmentationResultDir.');
            end
            
            if( isempty(this.segmentationResultFileArray) )
                error('Undefined value for segmentationResultFileArray.');
            end
            
            if( isempty(this.nFramesToImport) )
                error('Undefined value for nFramesToImport.');
            end
            
            TracX.Utils.printToConsole('CHECK: Project configuration check passed!')
            
        end
        
    end
end