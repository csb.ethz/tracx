%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TemporaryLineageAssignment < handle & matlab.mixin.Copyable
    % TEMPORARYLINEAGEASSIGNMENT Stores temporary mother daughter
    % lineage assignments until final decision is made which cells to assign.
    %
    % :Authors:
            %    Andreas P. Cuny - initial implementation
    
    properties

        daughterCell = []; % DaughterCell property stores daughter cells' track indicies.

        daughterCellStartFrame % Start frame of daughter cell segmentation (can be error prone due to missing segmentation)
        
        motherCellCandidates = {}; % MotherCellCandidates stores track indicies that are considered to be potenial mother cells.
        
        motherCellCandidatesStartFrame = {}; % MotherCellCandidatesStart frame stores frame number where the mother cell was segmented for the first time.

        motherCell % MotherCell property stores the detected mother cells (track indicies).

        motherSigInNucCandidates = {}; % MotherSigInNucCandidates (G1 phase) 1 if in nuc 0 otherwise for candidates.
        
        frameOfMDPair % Frame on which a mother daughter pair was found
        
        motherAssignmentProb = [] % Assignment probability
        
        motherAssignmentScore = [] % Assignment score
        
        hasBudNeckSignal % Binary bud neck signal quantification
        
        lineProfile % Line profile between daughter cell and mother candidate cell centroid
        
        
        markerID = {}; % ID of cell division marker segmentation.
        
        markerChannelID = {}; % Image channel of cell division marker segmentation.
        
        markerTrack % Track index of marker corresponding to MD pair.
        
        markerTrackStartFrame % Track index of marker corresponding to MD pair start frame
        
        markerTrackEndFrame % Track index of marker corresponding to MD pair end frame (can be error prone due to low SNR / not detectable signal)
        
        isManualCorr % isManualCorr indicates if assignment has been manually corrected
        maxFrameEvalAfterBirth = 20; % Number of frames for how long mother-dauther pairs should be evaluated if this assignment is correct after daughter cell birth.
        
        reconstructionIsFinished % Flag if reconstruction has been finished sucessfully
    end
    
    methods
        
        % Constructor
        function obj = TemporaryLineageAssignment()
            % TEMPORARYLINEAGETEMPASSIGNMENT Object constructor.
            %
            % Returns
            % -------
            %    obj :obj:`object`
            %                        :class:`TemporaryLineageAssignment` instance
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj.reconstructionIsFinished = false;
        end
        
        
        function setMotherDaughterPair(this, daughterArray, ...
                motherArray, frameNumber, budNeckSig, lineProfileRe, ...
                daughterFrameArray, motherFrameArray)
            % SETMOTHERDAUGHTERPAIR Sets a mother - daughter pair
            % in the TemporaryLineageAssignment object.
            %
            % Args:
            %    this (:obj:`object`):              :class:`TemporaryLineageAssignment` instance
            %    daughterArray (:obj:`array` kx1):  With daughter track_index to
            %                                       add to the object for evaluation.
            %    motherArray (:obj:`array` kx1):    With mother track_index to
            %                                       add to the object for evaluation
            %    frameNumber(:obj:`int`):           Image frame number
            %    budNeckSig(:obj:`bool`):           Flag if budneck signal was detected
            %                                       1, 0 otherwise.
            %    lineProfileRe(:obj:`array` 1x100): Resampled line profile
            %                                       between mother daugher centroid
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            for i = 1:numel(daughterArray)
                iDaughter = daughterArray(i);
                iDaughterF = daughterFrameArray(i);
                iMother = motherArray(i);
                iMotherF = motherFrameArray(i);
                if isempty(this.daughterCell(ismember(this.daughterCell, ...
                        iDaughter)))
                    this.daughterCell = [this.daughterCell, iDaughter];
                    this.daughterCellStartFrame = [...
                        this.daughterCellStartFrame, iDaughterF];
                end
                if isempty(this.motherCellCandidates)
                    this.motherCellCandidates = {iMother};
                    this.motherCellCandidatesStartFrame = {iMotherF};
                    this.frameOfMDPair = {frameNumber};
                    this.hasBudNeckSignal = {budNeckSig};
                    this.lineProfile = {lineProfileRe};
                    this.isManualCorr = [this.isManualCorr, 0];
                else
                    if find(ismember(this.daughterCell, iDaughter)) > ...
                            numel(this.motherCellCandidates)
                        %currVal = {};
                        %currVal{end+1} = Mother;
                        currVal = iMother;
                        frameArray = frameNumber;
                        this.motherCellCandidates{ismember(...
                            this.daughterCell, iDaughter)} = currVal;
                        this.motherCellCandidatesStartFrame{ismember(...
                            this.daughterCell, iDaughter)} = iMotherF;
                        this.frameOfMDPair{ismember(...
                            this.daughterCell, iDaughter)} = frameArray;
                        this.hasBudNeckSignal{ismember(...
                            this.daughterCell, iDaughter)} = budNeckSig;
                        this.lineProfile{ismember(...
                            this.daughterCell, iDaughter)} = lineProfileRe;
                        this.isManualCorr(ismember(...
                            this.daughterCell, iDaughter)) = 0;
                    else
                        currVal = this.motherCellCandidates{ismember(...
                            this.daughterCell, iDaughter)};
                        currValF = this.motherCellCandidatesStartFrame{ismember(...
                            this.daughterCell, iDaughter)};
                        frameArray = this.frameOfMDPair{ismember(...
                            this.daughterCell, iDaughter)};
                        budNeckSigArray = this.hasBudNeckSignal{ismember(...
                            this.daughterCell, iDaughter)};
                        lineProfileArray = this.lineProfile{ismember(...
                            this.daughterCell, iDaughter)};
                        if iscell(currVal)
                            currVal{end+1} = iMother;
                            currValF{end+1} = iMotherF;
                            frameArray{end+1} = frameNumber;
                            budNeckSigArray{end+1} = budNeckSig;
                            lineProfileArray{end+1} = lineProfileRe;
                        else
                            currVal = [currVal, iMother];
                            currValF = [currValF, iMotherF];
                            frameArray = [frameArray, frameNumber];
                            budNeckSigArray = [budNeckSigArray, budNeckSig];
                            lineProfileArray = [lineProfileArray, lineProfileRe];
                            
                        end
                        this.motherCellCandidates{ismember(...
                            this.daughterCell, iDaughter)} = currVal;
                        this.motherCellCandidatesStartFrame{ismember(...
                            this.daughterCell, iDaughter)} = currValF;
                        this.frameOfMDPair{ismember(...
                            this.daughterCell, iDaughter)} = frameArray;
                        this.hasBudNeckSignal{ismember(...
                            this.daughterCell, iDaughter)} = budNeckSigArray;
                        this.lineProfile{ismember(...
                            this.daughterCell, iDaughter)} = lineProfileArray;
                        this.isManualCorr(ismember(...
                            this.daughterCell, iDaughter)) = 0;
                    end
                end
                %             end
                % ret = nan;
                %[ret, this] = this.setFinalAssignment();
            end
        end
                
        function deleteRow(this, daughterArray)
            % DELETEROW Deletes the row of assigned mother - daughter
            % pairs.
            %
            % Args:
            %    this (:obj:`object`):              :class:`TemporaryLineageAssignment` instance
            %    daughterArray (:obj:`array` kx1):  With daughter track_index to
            %                                       remove from object after
            %                                       sucessfull assignment.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.motherCellCandidates(ismember(this.daughterCell, ...
                daughterArray)) = [];
            this.daughterCell(ismember(this.daughterCell, ...
                daughterArray)) = [];
        end

        function determineMotherCell(this)
            % DETERMINEMOTHERCELL Determines the mother cells from the
            % mother cell canditates based on most frequence occurence.
            %
            % Args:
            %    this (:obj:`object`):              :class:`TemporaryLineageAssignment` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.motherCell = zeros(1, numel(this.motherCellCandidates));
            this.motherAssignmentProb = zeros(1, numel(this.motherCellCandidates));
            this.motherAssignmentScore = zeros(1, numel(this.motherCellCandidates));
            for k = 1:numel(this.motherCellCandidates)
                
                % Consider only MD pairs which were detected less than
                % maxFrameEvalAfterBirth frames after the daughter start
                selIdx = this.frameOfMDPair{k} < this.daughterCellStartFrame(k) + ...
                    this.maxFrameEvalAfterBirth;
                
                [occ, cand] = hist(this.motherCellCandidates{k}(selIdx), ...
                    unique(this.motherCellCandidates{k}(selIdx)));
                p = occ./numel(this.motherCellCandidates{k}(selIdx));
                if numel(cand) >=3
                    candFin = cand((p >= max(p)));
                    pFin = p(p >= max(p));
                    occFin = occ(p >= max(p));
                    if numel(candFin) == 1
                        this.motherCell(k) = candFin;
                        this.motherAssignmentProb(k) = pFin;
                        this.motherAssignmentScore(k) = this.getWeightedScore(pFin, occFin); %todo set probability to 1 if occ> 2-3* threshold
                    else
                        % can not determine which of two potential
                        % assignment is correct as both have same
                        % probability. We assign here nan and one has
                        % to check manually which one of the suggested
                        % ones is the correct one.
                        this.motherCell(k) = nan;
                        this.motherAssignmentProb(k) = pFin(1);
                        this.motherAssignmentScore(k) = this.getWeightedScore(pFin(1), occFin(1));
                    end
                else
                    if ~isempty(cand( p > 0.5))
                        
                        candFin = cand( p > 0.5);
                        pFin = p(p > 0.5);
                        occFin = occ(p > 0.5);
                        if numel(candFin) == 1
                            this.motherCell(k) = candFin;
                            this.motherAssignmentProb(k) = pFin;
                            this.motherAssignmentScore(k) = this.getWeightedScore(pFin, occFin);
                        else
                            % can not determine which of two potential
                            % assignment is correct as both have same
                            % probability. We assigne here nan and one has
                            % to check manually which one of the suggested
                            % ones is the correct one.
                            this.motherCell(k) = nan;
                            this.motherAssignmentProb(k) = pFin;
                            this.motherAssignmentScore(k) = this.getWeightedScore(pFin, occFin);
                        end
                        
                    else
                        % can not determine which of two potential
                        % assignment is correct as both have same
                        % probability. We assigne here nan and one has
                        % to check manually which one of the suggested
                        % ones is the correct one.
                        this.motherCell(k) = nan;
                        this.motherAssignmentProb(k) = 0;
                        this.motherAssignmentScore(k) = 0;
                        
                    end
                end
            end
        end
        
        function score = getWeightedScore(this, p, occ)
            % GETWEIGHTEDSCORE Computes a weighted score for mother
            % daughter candidate pairs based on a probablity and MD pair
            % occurance.
            %
            % Args:
            %    this (:obj:`object`):         :class:`TemporaryLineageAssignment` instance
            %    p (:obj:`array` kx1):         Probability of MD (mother daughter pair)
            %                                  assignments.
            %    occ (:obj:`array`, kx1):      MD pair detection occurance.
            %
            % Returns
            % -------
            %    score (:obj:`float`)
            %                                  Weighted score for MD
            %                                  assignments.
            weight = TracX.ExternalDependencies.sigmf(occ,[3/(3/3) ...
                3/2]);
            score = p * weight;
            
            if numel(occ) >= 3 * 2 && weight == 1
                score = 1;
            end
            
        end
        
        function setMarker(this, markerID, channelID, Daughter)
            % SETMARKER Sets the cell division event marker
            % segmentation index as well as the image channel the marker
            % was recorded with.
            %
            % Args:
            %    this (:obj:`object`):              :class:`TemporaryLineageAssignment` instance
            %    markerID (:obj:`int`):             Cell division segmentation label index.
            %    channelID (:obj:`int`):            Cell division image channel index.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            idx = find(this.daughterCell == Daughter);
            if size(this.markerID, 2) ~= idx && idx > size(this.markerID, 2)
                this.markerID{idx} = [];
                this.markerChannelID{idx} = [];
            end
            this.markerID{idx} = ...
                [this.markerID{idx}, markerID];
            this.markerChannelID{idx} = ...
                [this.markerChannelID{idx}, channelID];
        end
        
        function setSigInNuc(this, sigInNuc, daughter)
            % SETSIGINNUC Sets a flag if a signal in the nucleus of a
            % mother cell has been detected (for cell cycle determination).
            %
            % Args:
            %    this (:obj:`object`):         :class:`TemporaryLineageAssignment` instance
            %    sigInNuc (:obj:`int`):        Flag if signal in nucleus has been detected.
            %    daughter (:obj:`int`):        Daughter track index.
            %
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            idx = find(this.daughterCell == daughter);
            if size(this.motherSigInNucCandidates, 2) ~= idx && ...
                    idx > size(this.motherSigInNucCandidates, 2)
                this.motherSigInNucCandidates{idx} = [];
            end
            this.motherSigInNucCandidates{idx} = ...
                [this.motherSigInNucCandidates{idx}, sigInNuc];
        end
        
        function setMarkerTracks(this, lineageData)
            % SETMARKERTRACKS Assigns the marker track id of the bud neck
            % marker to a mother daughter (MD) canditate pair.
            %
            % Args:
            %    this (:obj:`object`):         :class:`TemporaryLineageAssignment` instance
            %    lineageData (:obj:`object`):  Marker lineage data.
            %
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            n = numel(this.frameOfMDPair);
            uuidAllAss = cell(1, n);
            this.markerTrack = nan(1, n);
            this.markerTrackStartFrame = nan(1, n);
            this.markerTrackEndFrame = nan(1, n);
            for p = 1:n
                frames = unique(this.frameOfMDPair{p});
                % Consider only markers which were detected less than
                % maxFrameEvalAfterBirth frames after the daughter start
                selIdx = frames < this.daughterCellStartFrame(p) + ...
                    this.maxFrameEvalAfterBirth;
                frames(~selIdx) = [];
                if ~isempty(frames)
                    for f = 1:numel(frames)
                        occIdx = this.frameOfMDPair{p} == frames(f);
                        markerId = this.markerID{p};
                        objIndex = lineageData.obj_index(lineageData.obj_frame == frames(f));
                        uuid = lineageData.uuid(lineageData.obj_frame == frames(f));
                        uuidAllAss{p} = [uuidAllAss{p}, uuid(ismember(objIndex, markerId(occIdx)))];
                    end
                    track = lineageData.obj_track_index(ismember(...
                        lineageData.uuid, uuidAllAss{p}));
                    uniqueTrack = unique(track);
                    Ncount = histc(track, uniqueTrack);
                    mT = uniqueTrack(find(Ncount == max(Ncount), 1, 'first'));
                    if isempty(mT) || numel(mT) > 1
                        this.markerTrack(p) = nan;
                        this.markerTrackStartFrame(p) = nan;
                        this.markerTrackEndFrame(p) = nan;
                    else
                        this.markerTrack(p) = uniqueTrack(find(Ncount == max(Ncount), 1, 'first'));
                        this.markerTrackStartFrame(p) = unique(min(lineageData.getConditionalFieldArray(...
                            'obj_frame', 'obj_track_index', this.markerTrack(p))));
                        this.markerTrackEndFrame(p) = unique(max(lineageData.getConditionalFieldArray(...
                            'obj_frame', 'obj_track_index', this.markerTrack(p))));
                    end
                else
                    this.markerTrack(p) = nan;
                    this.markerTrackStartFrame(p) = nan;
                    this.markerTrackEndFrame(p) = nan;
                end
            end
            
        end
        
        function deleteEntry(this, delIdx)
            % DELETEENTRY Deletes a mother daughter (MD) pair entry form 
            % :class:`TemporaryLineageAssignment`
            %
            % Args:
            %    this (:obj:`object`):    :class:`TemporaryLineageAssignment` instance
            %    delIdx (:obj:`int`):     Index for deletion.
            %
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            names = fieldnames(this);
            for name = names'
                if ~any(strcmp(name, {'maxFrameEvalAfterBirth', ...
                        'daughterCellStartFrame', 'reconstructionIsFinished', ...
                        'motherSigInNucCandidates'}))
                    try
                        this.(char(name))(delIdx) = [];
                    catch Exceptrion
                        disp(Exceptrion)
                    end
                end
            end
        end
        
        function replaceOrAddNew(this, data2Modify)
            % REPLACEORADDNEW Replaces or adds new data to :class:`TemporaryLineageAssignment`
            %
            % Args:
            %    this (:obj:`object`):        :class:`TemporaryLineageAssignment` instance
            %    data2Modify (:obj:`array`):  Array with data to be modified. 
            %                                 The collumns correspond to daughter, 
            %                                 new mother, old mother, division frame.
            %
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            for k = 1:size(data2Modify, 1)
                daughter = data2Modify(k, 1);
                newMother = data2Modify(k, 2);
                oldMother = data2Modify(k, 3);
                divisionFrame = data2Modify(k, 4);
                % Check if MD pair allready exists if so update mother
                idx = and(this.daughterCell == daughter, ...
                    this.motherCell == oldMother);
                
                if any(idx) == 1
                    % We have an existing pair which we now need to update
                    if sum(idx) == 1
                        this.motherCell(idx) = newMother;
                        this.isManualCorr(idx) = 1;
                        this.motherAssignmentProb(idx) = 1;
                        this.motherAssignmentScore(idx) = 1;
                        this.markerTrackStartFrame(idx) = divisionFrame;
                        this.markerTrackEndFrame(idx) = divisionFrame;
                    end
                else
                    % We need to add a new entry
                    names = fieldnames(this);
                    for name = names'
                        if ~any(strcmp(name, {'maxFrameEvalAfterBirth', ...
                                'daughterCellStartFrame', 'reconstructionIsFinished'}))
                            if strcmp(char(name), 'daughterCell')
                                this.(char(name))(end+1) = daughter;
                            elseif strcmp(char(name), 'motherCell')
                                this.(char(name))(end+1) = newMother;
                            elseif strcmp(char(name), 'isManualCorr')
                                this.(char(name))(end+1) = 1;
                            elseif strcmp(char(name), 'motherAssignmentProb')
                                this.(char(name))(end+1) = 1;
                            elseif strcmp(char(name), 'motherAssignmentScore')
                                this.(char(name))(end+1) = 1;
                            elseif strcmp(char(name), 'markerTrackStartFrame')
                                this.(char(name))(end+1) = divisionFrame;
                            elseif strcmp(char(name), 'markerTrackEndFrame')
                                this.(char(name))(end+1) = divisionFrame;
                            else
                                if iscell(this.(char(name))) == 1
                                    this.(char(name)){end+1} = -1;
                                else
                                    this.(char(name))(end+1) = -1;
                                end
                            end
                        end
                    end
                end
            end
        end 
    end
end