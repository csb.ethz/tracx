classdef TracXSegEdit < handle
    % TRACXSEGEDIT Implements a basic segmentation mask editor based on
    % existing CellX masks. If you have labeled segmentation mask convert
    % them first to CellX format with 'prepareDataFromSegmentationMask'.
    % Original masks will be moved to a temp location and are available to
    % reset modified segmentation masks at any time.
    % Note modified segmentation masks might differ by some pixels due to
    % mask to interactive polygon conversion.
    % 
    % Built on CROIEditor by Jonas Reber, Mai 6th 2011, published on
    % fileexchange.
    % TracX implementation written by Andreas Cuny
    
    events
        doneSegEdit % thrown when "apply" button is hit, listen to this event
        % to define subsequent actions
    end
    
    properties
        
        tracker  % Handle to TracX instance
        image    % Current image frame
        modMaskFrameArray % Frame numbers of modified masks
        
        figh = 600; % initial figure height
    end
    
    properties(Access=public)
        % UI 
        guifig   % mainwindow
        imax    % holds working area
        roiax   % holds roid preview image
        imag    % image to work on
        roifig  % roi image
        tl      % userinfo bar
        waLH    % Working area label handle
        paLH    % preview area label handle
        
        minFrame             % Min frame of the project
        maxFrame             % Max frame of the project
        currentFrame         % Current visible frame of the project
        slider               % Frame slider handle
        maskPreview = false; % Bool to switch between binary and labeled mask preview
        
        figw                 % initial window height, this is calculated on load
        hwar = 2.1;          % aspect ratio
        
        % Class stuff
        currentUnsavedState = 0  % Bool to indecate if there are unsaved changes to the mask  
        currentOriginalMask      % The original segmentation mask of the current frame
        currentOriginalSeg       % The original segmentation objects (polygons) of the current frame
        currentWorkingSeg        % The working segmentation objects (polygons) of the current frame
        currentWorkingMask       % The working mask of the current frame
        interactiveSegmentations % Array with polygon objects
        segLabels                % Segmentation labels
        selectedSegmentation     % Selected Segmentation polygon object
        selectedSegmentationIdx  % Selected Segmentation polygon object index

    end
    
    methods
        function this = TracXSegEdit(tracker)
            % TRACXSEGEDIT Constructor
            
            % Add refrence to Tracker
            this.tracker = tracker;
            this.currentFrame = this.tracker.configuration.ProjectConfiguration.trackerStartFrame;
            this.minFrame = this.tracker.configuration.ProjectConfiguration.trackerStartFrame;
            this.maxFrame = this.tracker.configuration.ProjectConfiguration.trackerEndFrame;

            % Create the Figure
            this.figw = this.figh*this.hwar;
            this.createWindow;
            this.slider.Min = this.minFrame;
            this.slider.Max = this.maxFrame;
            this.slider.Value = this.minFrame;
            this.slider.SliderStep = [1, 1] / (this.maxFrame - this.minFrame);
            % Load the current Frame
            this.loadFrame()
            
        end
        
        function delete(this)
            % TRACXSEGEDIT destructor
            delete(this.guifig);
        end
        
        function cMap = getColorMap(this)
            cMap = parula(size(this.currentWorkingSeg, 1));
        end
        
        function set.image(this, theImage)
            % set method for image. uses grayscale images for region selection
            if size(theImage,3) == 3
                this.image = im2double(rgb2gray(theImage));
            elseif size(theImage,3) == 1
                this.image = im2double(theImage);
            else
                error('Unknown Image size?');
            end
            this.resetImages;
            this.resizeWindow;
        end
        
        function set.figh(this, height)
            this.figh = height;
            this.figw = this.figh*this.hwar;
            this.resizeWindow;
        end
                
    end
    
    methods(Access=private)
        
        function drawInteractiveSegmentations(this)
            
            newMask = zeros(size(this.image));
            this.interactiveSegmentations = cell(size(this.currentWorkingSeg));
            for k = 1:size(this.currentWorkingSeg, 1)
                h = drawpolygon('FaceAlpha',0, 'Position', this.currentWorkingSeg{k}, ...
                    'Label', num2str(this.segLabels(k)), 'Tag', ...
                    sprintf('imsel_%.f', numel(this.segLabels(k))), ...
                    'parent', this.imax, 'LabelVisible', 'hover', ...
                    'FaceSelectable', true); % 'LabelTextColor', 'w',
                % reduce(h) % To reduce number of vertices from R2019b on
                addlistener(h, 'MovingROI', @this.updateSelSeg);
                addlistener(h, 'ROIClicked', @this.getSelSeg);
                addlistener(h, 'DeletingROI', @this.onDeleteROI);
                this.interactiveSegmentations{k} = h;
                bw = this.interactiveSegmentations{k}.createMask(this.image);
                newMask = newMask| bw;
            end
            if this.maskPreview == 0
                set(this.roifig, 'CData', this.image .* logical(newMask));
            else
                set(this.roifig, 'CData', label2rgb(this.onTempSave))
            end
            this.colorInteractiveSeg
        end
        
        function resetImages(this)
            % load images
            this.imag = imshow(this.image,'parent',this.imax);
            this.roifig = imshow(this.image,'parent',this.roiax);  
        end
        
        function getSelSeg(this, h, e)
            this.selectedSegmentation = str2double(h.Label);
            this.selectedSegmentationIdx = ismember(cellfun(@(x) x.Label, ...
                this.interactiveSegmentations, 'UniformOutput', false), h.Label);
        end
        
        function updateSelSeg(this, h,e)
            set(this.tl,'String','Warning: Unsaved changes!', 'Visible', 'on', ...
                'BackgroundColor',[255 182 193]./256);
            set(this.guifig, 'Color',[255 182 193]./256)
            set(this.waLH, 'BackgroundColor',[255 182 193]./256);
            set(this.paLH, 'BackgroundColor',[255 182 193]./256);
            this.currentUnsavedState = 1;
            
            this.drawMask
        end
        
        function drawMask(this)
            if this.maskPreview == 0
                this.currentWorkingMask = zeros(size(this.image));
                for i=1:numel(this.interactiveSegmentations)
                    bw = this.interactiveSegmentations{i}.createMask(this.image);
                    this.currentWorkingMask = this.currentWorkingMask | bw;
                end
                set(this.roifig, 'CData', this.image .* this.currentWorkingMask);
            else
                set(this.roifig, 'CData', label2rgb(this.onTempSave))
            end
            this.colorInteractiveSeg
        end
        
        function newlyCreatedSeg(this)
            label = this.getNewLabel;
            set(this.interactiveSegmentations{end},'Tag',sprintf('imsel_%.f', label));
            set(this.interactiveSegmentations{end},'Label',sprintf('%d', label));
            set(this.interactiveSegmentations{end}, 'LabelVisible', 'hover');
            set(this.interactiveSegmentations{end}, 'FaceSelectable', true);
            addlistener(this.interactiveSegmentations{end}, 'MovingROI', @this.updateSelSeg);
            addlistener(this.interactiveSegmentations{end}, 'ROIClicked', @this.getSelSeg);
            addlistener(this.interactiveSegmentations{end}, 'DeletingROI', @this.onDeleteROI);
            
            this.updateSelSeg;
        end
        
        function colorInteractiveSeg(this)
            cMap = parula(length(this.interactiveSegmentations));
            for k = 1:numel(this.interactiveSegmentations)
                this.interactiveSegmentations{k}.Color = cMap(k, :);
            end
        end
        
        function onAddNewSeg(this, h,e)
            newSeg = drawassisted(this.imag);
            this.interactiveSegmentations{end+1} = newSeg;
            this.newlyCreatedSeg; % add tag, and callback to new shape
        end
        
        function tmpMask = onTempSave(this, h, e)
            tmpMask = zeros(size(this.image));
            for i=1:numel(this.interactiveSegmentations)
                bw = this.interactiveSegmentations{i}.createMask(this.image);
                tmpMask(bw) = str2double(this.interactiveSegmentations{i}.Label);
            end
        end
        
        function onSaveCurrent(this, h, e)
            
            segmentationMask = this.onTempSave;
            maskFile = strsplit(this.tracker.configuration.ProjectConfiguration. ...
                segmentationResultMaskFileArray{this.currentFrame}, '_');
            ending = maskFile{2};
            maskFile{2} = 'time';
            name = ['tmp', strjoin(maskFile, '_'), ending];
            
            save(fullfile(this.tracker.configuration.ProjectConfiguration. ...
                segmentationResultDir, name), 'segmentationMask')
            
            % Remove warning
            set(this.guifig, 'Color', [1, 1, 1])
            set(this.waLH, 'BackgroundColor', [1, 1, 1]);
            set(this.paLH, 'BackgroundColor', [1, 1, 1]);
            set(this.tl,'String','Changes saved!', 'Visible', 'on', ...
                'BackgroundColor', [0, 1, 0]);
            this.currentUnsavedState = 0; 
        end
        
        function onResetSeg(this, h, e)
            % Delete interactive objects
            h = findobj(this.imax, 'Type', 'images.roi.Polygon');
            delete(h)
            h = findobj(this.imax, 'Type', 'images.roi.AssistedFreehand');
            delete(h)
            % Get original segmentations for the frame
            this.currentWorkingSeg = this.currentOriginalSeg;
            this.getInputLabels(this.currentOriginalMask')
            
            % Initialize new ROI
            this.drawInteractiveSegmentations()
            
            % Remove warning
            set(this.guifig, 'Color', [1, 1, 1])
            set(this.waLH, 'BackgroundColor', [1, 1, 1]);
            set(this.paLH, 'BackgroundColor', [1, 1, 1]);
            set(this.tl,'String','', 'Visible', 'off', ...
                'BackgroundColor', [1, 1, 1]);
            this.currentUnsavedState = 0; 
        end
        
        function onDeleteROI(this,h,e)
            
            if ~isempty(this.selectedSegmentationIdx)
                % Delete interactive objects
                h = findobj(this.imax, 'Type', 'images.roi.Polygon');
                if ~isempty(h)
                    idx = ismember(cell2mat(cellfun(@(x) str2double(x), ...
                        {h.Label}, 'UniformOutput', false)), ...
                        this.segLabels(this.selectedSegmentationIdx));
                    if any(idx)
                        delete(h(idx))
                    end
                end
                
                h = findobj(this.imax, 'Type', 'images.roi.AssistedFreehand');
                if ~isempty(h)
                    idx = ismember(cell2mat(cellfun(@(x) str2double(x), ...
                        {h.Label}, 'UniformOutput', false)), ...
                        this.segLabels(this.selectedSegmentationIdx));
                    if any(idx)
                        
                        delete(h(idx))
                    end
                end
                % delete currently selected shape
                this.interactiveSegmentations(this.selectedSegmentationIdx) = [];
                this.segLabels(this.selectedSegmentationIdx) = [];
                this.selectedSegmentation = [];
                this.selectedSegmentationIdx  = [];
                this.updateSelSeg;
            end
        end
        
        function newLabel = getNewLabel(this)
            if isempty(this.segLabels)
                newLabel = 1;
            else
                newLabel = max(this.segLabels) + 1;
            end
            this.segLabels(end+1) = newLabel;
        end
        
        function loadFrame(this, w, h)
            % Load the image
            img = this.tracker.io.readImageToGrayScale(fullfile(...
                this.tracker.configuration.ProjectConfiguration.imageDir, ...
                this.tracker.configuration.ProjectConfiguration.imageFingerprintFileArray{this.currentFrame}), ...
                this.tracker.configuration.ProjectConfiguration.imageCropCoordinatePriorReversionArray);
            img = this.tracker.imageProcessing.normalizeImage(img);
            this.image = img;
            % Load the mask
            this.getSegmentation()
            this.getInputLabels(this.currentWorkingMask')
            
            % Load interactive segmentations
            this.drawInteractiveSegmentations()
            
            ret = this.getCenterCoord();
            this.imax.XLim = [ret(1), ret(3)];
            this.imax.YLim = [ret(2), ret(4)];
            
            this.waLH.String = sprintf('Working area frame %d', this.currentFrame);
        end
        
        function segArray = getSegmentationBoundary(~, tmpMask)
            p = regionprops(tmpMask, 'PixelList');
            idx = arrayfun(@(s) all(structfun(@isempty,s)),p);
            p = p(~idx); % Remove empty entries
            segArray = {p.PixelList};
            k = cellfun(@(x) boundary(x(:, 1), x(:, 2)), {p.PixelList}, ...
                'UniformOutput' , false);
            segArray = cellfun(@(x, y) [x(y, 1), x(y, 2)], ...
                segArray, k, 'UniformOutput' , false)';
        end
        
        function getInputLabels(this, tmpMask)
            tmpLabels = nan(size(this.currentWorkingSeg));
            for l = 1:size(this.currentWorkingSeg, 1)
                idx = sub2ind(size(tmpMask), ...
                    this.currentWorkingSeg{l,1}(:,1), ...
                    this.currentWorkingSeg{l,1}(:,2));
                if isempty(unique(tmpMask(idx)))
                    tmpLabels(l) = 0; % 
                elseif numel(unique(tmpMask(idx))) > 1
                    tmpLabels(l) = 2^16-1; % more than one parent label - use new one
                else
                    tmpLabels(l) = unique(tmpMask(idx));
                end
            end
            this.segLabels = tmpLabels;
        end
        
        function getSegmentation(this)
            
            if exist(fullfile(this.tracker.configuration. ...
                    ProjectConfiguration.segmentationResultDir, ...
                    'originalSeg'), 'dir')
                % Segmentation has been manually edited and initial
                % original data is found under 'originalSeg'
                try
                    this.currentOriginalMask = this.tracker.io.readSegmentationMask(fullfile(...
                        this.tracker.configuration.ProjectConfiguration.segmentationResultDir, ...
                        'originalSeg', this.tracker.configuration.ProjectConfiguration. ...
                        segmentationResultMaskFileArray{this.currentFrame}));
                    this.currentOriginalSeg = this.getSegmentationBoundary(...
                        this.currentOriginalMask);
                catch
                    this.currentOriginalMask = this.tracker.io.readSegmentationMask(fullfile(...
                        this.tracker.configuration.ProjectConfiguration.segmentationResultDir, ...
                        this.tracker.configuration.ProjectConfiguration.segmentationResultMaskFileArray{this.currentFrame}));
                    this.currentOriginalSeg = this.getSegmentationBoundary(...
                        this.currentOriginalMask);
                end
            else
                % Original segmentation not modified; load raw
                this.currentOriginalMask = this.tracker.io.readSegmentationMask(fullfile(...
                    this.tracker.configuration.ProjectConfiguration.segmentationResultDir, ...
                    this.tracker.configuration.ProjectConfiguration.segmentationResultMaskFileArray{this.currentFrame}));
                this.currentOriginalSeg = this.getSegmentationBoundary(...
                    this.currentOriginalMask);
            end
            
            % Check if temporary (autosaved) manual segmentation edits
            % exist. If so load them
            maskFile = strsplit(this.tracker.configuration.ProjectConfiguration. ...
                segmentationResultMaskFileArray{this.currentFrame}, '_');
            ending = maskFile{2};
            maskFile{2} = 'time';
            name = ['tmp', strjoin(maskFile, '_'), ending];
%             name = ['tmp', this.tracker.configuration.ProjectConfiguration. ...
%                 segmentationResultMaskFileArray{this.currentFrame}];
            if exist(fullfile(this.tracker.configuration.ProjectConfiguration. ...
                    segmentationResultDir, name), 'file')
                this.currentWorkingMask = this.tracker.io.readSegmentationMask(...
                    fullfile(this.tracker.configuration.ProjectConfiguration. ...
                    segmentationResultDir, name));
                this.currentWorkingSeg  = this.getSegmentationBoundary(...
                    this.currentWorkingMask);
            else
                this.currentWorkingMask = this.currentOriginalMask;
                this.currentWorkingSeg  = this.getSegmentationBoundary(...
                    this.currentOriginalMask); 
            end 
        end
                
        function moveOriginalToTemp(this)
            
            if ~exist(fullfile(this.tracker.configuration. ...
                    ProjectConfiguration.segmentationResultDir, ...
                    'originalSeg'), 'dir')
                mkdir(fullfile(this.tracker.configuration. ...
                    ProjectConfiguration.segmentationResultDir, ...
                    'originalSeg'))
            end
            
            maskArray = dir(fullfile(this.tracker.configuration. ...
                ProjectConfiguration.segmentationResultDir, 'tmpmask*'));
            maskArrayS = TracX.ExternalDependencies.natsortfiles({maskArray.name});
            this.modMaskFrameArray = cellfun(@(x) str2double(regexp(x,'\d*','Match')), ...
                maskArrayS, 'UniformOutput' , false);
            
            % Copy original files to backup location
            fileTypeArray = {'cells_%d.txt', 'cells_%d.mat', 'mask_%d.mat', ...
                'control_%d.png', 'seeding_%d.png'};
            nLeadingZeros = this.tracker.utils.getLeadingZeros(...
                this.tracker.configuration.ProjectConfiguration. ...
                segmentationResultFileArray{1}) + 1;
            sel = "%0" + string(nLeadingZeros) + 'd';
            for f = 1:numel(this.modMaskFrameArray)
                for fileType = fileTypeArray
                    src = fullfile(this.tracker.configuration. ...
                        ProjectConfiguration.segmentationResultDir, ...
                        sprintf(strrep(fileType{1}, '%d', sel), this.modMaskFrameArray{f}));
                    dst = fullfile(this.tracker.configuration. ...
                        ProjectConfiguration.segmentationResultDir, ...
                        'originalSeg', sprintf(strrep(fileType{1}, '%d', sel), ...
                        this.modMaskFrameArray{f}));
                    if exist(src, 'file')
                        movefile(src, dst);
                    end
                end
            end            
        end
        
        function resizeCroppedMasks(this)
            
            % Resize masks to original image size if needed.
            if ~isempty(this.tracker.configuration.ProjectConfiguration. ...
                    imageCropCoordinatePriorReversionArray)
                
                % Manually modified masks are of size of crop coordinates.
                % We need to resize the masks to image size priror to
                % requantification.
                maskArray = dir(fullfile(this.tracker.configuration. ...
                ProjectConfiguration.segmentationResultDir, 'tmpmask*'));
                maskArrayS = TracX.ExternalDependencies.natsortfiles({maskArray.name});
                
                for k = 1:numel(maskArrayS)
                    
                    segmentationMask = zeros(this.tracker.configuration. ...
                        ProjectConfiguration.imageWidth, ...
                        this.tracker.configuration.ProjectConfiguration. ...
                        imageHeight);
                    
                    % Load mask.
                    mask = this.tracker.io.readSegmentationMask(...
                    fullfile(this.tracker.configuration.ProjectConfiguration. ...
                    segmentationResultDir, maskArrayS{k}));
                    
                    % Add mask
                    cropCoords = this.tracker.configuration. ...
                        ProjectConfiguration. ...
                        imageCropCoordinatePriorReversionArray;
                    
                    segmentationMask(cropCoords(2) : cropCoords(2) + cropCoords(4), ...
                        cropCoords(1) : cropCoords(1) + cropCoords(3)) = mask;
                    
                    % Save mask
                    maskFile = strsplit(this.tracker.configuration.ProjectConfiguration. ...
                        segmentationResultMaskFileArray{this.currentFrame}, '_');
                    ending = maskFile{2};
                    maskFile{2} = 'time';
                    name = ['tmp2', strjoin(maskFile, '_'), ending];
                    
                    save(fullfile(this.tracker.configuration.ProjectConfiguration. ...
                        segmentationResultDir, name), 'segmentationMask')
                    
                end
                
            end
            
        end
        
        function onApply(this, w, h)
            
            % Move original data to temp location
            this.tracker.utils.printToConsole(['REQUANTIFICATION: Moving ', ...
                ' original files to backup location'])
            this.moveOriginalToTemp()
            % Resize masks to original image size if needed
            this.resizeCroppedMasks()
            
            % Run CellXMaskInterface on changed frames.
            this.tracker.utils.printToConsole(['REQUANTIFICATION: Starting re segmentation and', ...
                ' quantification from merged masks'])
            
            % Requantify final_track_mask_* from temp dir (merged bud with mother until real division)
            trackerSeg = TracX.Tracker();
            imagePath = this.tracker.configuration.ProjectConfiguration.imageDir;
            segmentationPath = fullfile(this.tracker.configuration.ProjectConfiguration. ...
                segmentationResultDir);
            
            if ~isempty(this.tracker.configuration.ProjectConfiguration. ...
                    imageCropCoordinatePriorReversionArray)
                segmentationFileNameRegex = 'tmp2mask_*';
            else
                segmentationFileNameRegex = 'tmpmask_*';
            end
            bfIdent = strsplit(this.tracker.configuration.ProjectConfiguration. ...
                imageFingerprintFileArray{1}, '_');
            imageFileNameRegex = sprintf('%s*', bfIdent{1});
            fluoTags = this.tracker.configuration.ProjectConfiguration.fluoImageIdentifier;
            trackerSeg.prepareDataFromSegmentationMask(imagePath, ...
                segmentationPath, segmentationFileNameRegex, ...
                imageFileNameRegex, 'fluoTags', fluoTags)
            this.tracker.utils.printToConsole(['REQUANTIFICATION: Done with re segmentation and' ...
                ' quantification from merged masks'])
            
            % Quit
            this.closefig()
        end
        
        function createWindow(this, w, h)
            
            this.guifig=figure('MenuBar','none','Resize','on','Toolbar','none','Name','TracX :: Segmentation Editor', ...
                'NumberTitle','off','Color','white', 'units','pixels','position',[0 0 this.figw this.figh],...
                'CloseRequestFcn',@this.closefig, 'visible','off');
            
            warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
            warning('off', 'MATLAB:class:PropUsingAtSyntax');
            javaFrame = get(this.guifig, 'JavaFrame');
            javaFrame.setFigureIcon(javax.swing.ImageIcon(fullfile(which('TrackerLogo.png'))));
            
            
            % buttons
            btnH = [];
            btnH(end+1) = uicontrol('Parent',this.guifig,'String','Add new segmentation',...
                'units','normalized',...
                'Position',[0.01 0.8 0.15 0.15], ...
                'Callback',@(h,e)this.onAddNewSeg(h,e));
            btnH(end+1) = uicontrol('Parent',this.guifig,'String','Reset original',...
                'units','normalized',...
                'Position',[0.01 0.65 0.15 0.15],...
                'Callback',@(h,e)this.onResetSeg(h,e));
            btnH(end+1) = uicontrol('Parent',this.guifig,'String','Delete selected',...
                'units','normalized',...
                'Position',[0.01 0.5 0.15 0.15],...
                'Callback',@(h,e)this.onDeleteROI(h,e));
            btnH(end+1) = uicontrol('Parent',this.guifig,'String','Save changes on current frame',...
                'units','normalized',...
                'Position',[0.01 0.2 0.15 0.15],...
                'Callback',@(h,e)this.onSaveCurrent(h,e));
            btnH(end+1) = uicontrol('Parent',this.guifig,'String','Apply & Close',...
                'units','normalized',...
                'Position',[0.01 0.05 0.15 0.15],...
                'Callback',@(h,e)this.onApply(h,e));
            p = uipanel('Parent',this.guifig, 'Title','','FontSize',12,...
                'BackgroundColor','white',...
                'Position',[0.18 0.01 0.812 0.05], ...
                'BorderType', 'none');
            
            btnH(end+1) = uicontrol('Parent',p,'String','|<<',...
                'units','normalized', 'Position',[0.0 0.0 0.05 1], ...
                'Callback', @(h,e) this.toStBtn(h, e));
            btnH(end+1) = uicontrol('Parent',p,'String','-10',...
                'units','normalized', 'Position',[0.05 0.0 0.05 1], ...
                'Callback', @(h,e) this.m10Btn(h, e));
            this.slider = uicontrol('Parent',p,'Style','slider',...
                'units','normalized', 'Position',[0.1 0.0 0.8 1], ...
                'Callback', @this.onSlider); 
            btnH(end+1) = uicontrol('Parent',p,'String','+10',...
                'units','normalized', 'Position',[0.9 0.0 0.05 1], ...
                'Callback', @(h,e) this.p10Btn(h, e));
            btnH(end+1) = uicontrol('Parent',p,'String','>>|',...
                'units','normalized', 'Position',[0.95 0.0 0.05 1], ...
                'Callback', @(h,e) this.toEBtn(h, e));
            
            % axes
            this.imax = axes('parent',this.guifig,'units','normalized','position',[0.18 0.06 0.4 0.87]);
            this.roiax = axes('parent',this.guifig,'units','normalized','position',[0.59 0.06 0.4 0.87]);
            linkaxes([this.imax this.roiax]);
            
            % create toolbar
            this.createToolbar(this.guifig);
            
            % axis titles
            this.waLH = uicontrol('tag','txtimax','style','text','string','Working Area frame 1', ...
                'units','normalized', 'FontSize', 12, ...
                'position',[0.18 0.95 0.4 0.05], ...
                'BackgroundColor','w');
            this.paLH = uicontrol('tag','txtroiax','style','text','string','Segmentation Preview', ...
                'units','normalized', 'FontSize', 12, ...
                'position',[0.59 0.95 0.4 0.05], ...
                'BackgroundColor','w');
            
            % file load info
            this.tl = uicontrol('tag','txtfileinfo','style','text','string', ...
                '','units','normalized',...
                'position', [0.01 0.4 0.15 0.04], ...
                'BackgroundColor','g','visible','off');
        end
        
        function toStBtn(this, h, e)
            if this.currentUnsavedState == 1
                this.confirmDlg;
            end
            if this.currentUnsavedState == 0
                this.currentFrame = this.minFrame;
                this.slider.Value = this.minFrame;
                this.loadFrame()
            end
        end
        
        function m10Btn(this, h, e)
            if this.currentUnsavedState == 1
                this.confirmDlg;
            end
            if this.currentUnsavedState == 0
                if this.currentFrame > (1 + 9)
                    this.currentFrame = this.currentFrame - 10;
                else
                    this.currentFrame = 1;
                end
                this.slider.Value = this.currentFrame;
                this.loadFrame()
            end
        end
        
        function p10Btn(this, h, e)
            if this.currentUnsavedState == 1
                this.confirmDlg;
            end
            if this.currentUnsavedState == 0
                if this.currentFrame <= (this.maxFrame - 10)
                    this.currentFrame = this.currentFrame + 10;
                else
                    this.currentFrame = this.maxFrame;
                end
                this.slider.Value = this.currentFrame;
                this.loadFrame()
            end
        end
        
        function toEBtn(this, h, e)
            if this.currentUnsavedState == 1
                this.confirmDlg;
            end
            if this.currentUnsavedState == 0
                this.currentFrame = this.maxFrame;
                this.slider.Value = this.maxFrame;
                this.loadFrame()
            end
        end
        
        function onSlider(this, h, e)
            if this.currentUnsavedState == 1
                this.confirmDlg()
            end
            if this.currentUnsavedState == 1
                this.slider.Value = this.currentFrame;
            else
                value = this.slider.Value;
                this.currentFrame = round(value);
                this.loadFrame()
            end
        end
                
        function onSwitchPreview(this, h, e)
            this.maskPreview = ~this.maskPreview;
            this.drawMask
        end
        
        function onHelp(this, h, e)
            web('https://tracx.readthedocs.io/en/latest/gui.html#segmentation-editor');
        end
        
        function ret = getCenterCoord(this)
            p = regionprops(bwconvhull(logical(this.currentOriginalMask)), ...
                'BoundingBox');
            maxLen = max(p.BoundingBox(3:4));
            centerX = (p.BoundingBox(1) + (p.BoundingBox(3)/2)) - maxLen/2;
            centerY = p.BoundingBox(2) + (p.BoundingBox(4)/2) - maxLen/2;
            ret = [centerX, centerY, centerX + maxLen, centerY + maxLen];
        end
        
        function closefig(this, ~, ~)
            delete(this);
        end
        
        function resizeWindow(this)
            [h,w]=size(this.image);
            f = w/h;
            this.figw = this.figh*this.hwar*f;
            
            set(this.guifig,'position',[100 100 this.figw this.figh]);
            %movegui(this.guifig,'center');
            set(this.guifig,'visible','on');
            
        end
        
        function cdata = readIcon(~, pathToIcon)
            [cdata, map, alpha] = imread(pathToIcon,'Background','none');
        end
        
        function tb=createToolbar(this, fig)
            tb = uitoolbar('parent',fig);
            
            hpt=[];
            hpt(end+1) = uitoggletool(tb,'CData', this.readIcon('zoomin16.png'),...
                'TooltipString','Zoom In',...
                'ClickedCallback',...
                'putdowntext(''zoomin'',gcbo)',...
                'Separator','on');
            hpt(end+1) = uitoggletool(tb,'CData',this.readIcon('zoomout16.png'),...
                'TooltipString','Zoom Out',...
                'ClickedCallback',...
                'putdowntext(''zoomout'',gcbo)');
            hpt(end+1) = uitoggletool(tb,'CData',this.readIcon('drag16.png'),...
                'TooltipString','Pan',...
                'ClickedCallback',...
                'putdowntext(''pan'',gcbo)');
            hpt(end+1) = uitoggletool(tb,'CData',this.readIcon('toggle_mask16.png'),...
                'TooltipString','Switch preview',...
                'ClickedCallback', @this.onSwitchPreview);
            hpt(end+1) = uitoggletool(tb,'CData',this.readIcon('question-mark-6x16.png'),...
                'TooltipString','Help',...
                'ClickedCallback', @this.onHelp);
        end
        
        function confirmDlg(this, w, h)
           
            f = figure('Units','Normalized',...
                'Position',[.4 .6 .2 .1],...
                'NumberTitle','off',...
                'Color', 'white', ...
                'MenuBar','none','Resize','on','Toolbar','none', ...
                'Resize','off', ...
                'Name', 'TracX :: Confirm save');
            javaFrame = get(f,'JavaFrame');
            javaFrame.setFigureIcon(javax.swing.ImageIcon(fullfile(fileparts(pwd), 'doc', 'TrackerLogo.png')));
            btnH = [];
            btnH(end+1) = uicontrol('Style','PushButton',...
                'Units','Normalized',...
                'Position',[.1 .1 .25 .3],...
                'String', 'Discard changes', ...
                'CallBack', @(h, e)this.onDiscard(h, e));
            
            btnH(end+1) = uicontrol('Style','PushButton',...
                'Units','Normalized',...
                'Position',[.4 .1 .25 .3],...
                'String', 'Save changes');
            
            btnH(end+1) = uicontrol('Style','PushButton',...
                'Units','Normalized',...
                'Position',[.7 .1 .25 .3],...
                'String', 'Cancel',...
                'CallBack', @(h, e)this.onCancel(h, e));
            
            btnH(end+1) = uicontrol('Style','text',...
                'Units','Normalized',...
                'Position',[.1 .6 .8 .3],...
                'BackgroundColor', 'white', ...
                'String', 'Latest changes applied to the current segmentation mask will be lost if not saved.');
            
            uiwait(f)
            if ishandle(f)
                delete(f)
            end
        end
        
        function onCancel(this, h, e)
            get(gco, 'String');
            delete(gcf)
        end
        
        function onAccept(this, h, e)
            this.onSaveCurrent()
            get(gco, 'String');
            delete(gcf)
        end
        
        function onDiscard(this, h, e)
            this.currentUnsavedState = 0;
            set(this.guifig, 'Color', [1, 1, 1])
            set(this.waLH, 'BackgroundColor', [1, 1, 1]);
            set(this.paLH, 'BackgroundColor', [1, 1, 1]);
            set(this.tl,'String','', 'Visible', 'off', ...
                'BackgroundColor', [1, 1, 1]);
            get(gco, 'String');
            delete(gcf)
        end
    end
end
