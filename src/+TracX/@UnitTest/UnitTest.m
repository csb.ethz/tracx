%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef UnitTest
    
    properties (Access = private)
        
        Tracker % Tracker object used for unit tests

    end
    
    methods
        
        % Constructor
        function obj = UnitTest(tracker)
            % UNITTEST constructs an empty object of
            % type UnitTest. This class implements the fingerprint and
            % related functionalities.
            %
            % Args:
            %    tracker (:obj:`object`):          :class:`+TracX.@Tracker` instance.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                                      Returns a :class:`+TracX.@UnitTest` instance
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj.Tracker = tracker;
        end

        
        
        % completeUnitTesting
        %> @brief completeTesting iterates through the unittesting
        %> functions and executes all of them
        %>
        % Input:
        %>
        % Output:
        %> @retval ErrorsFound:              [Integer] Number of errors found
        %> @retval TestedFunctions:         [cell] Array of tested methods
        %> @retval TestResults:         [cell] Array of test results 
        function unitTestStruct = completeUnitTesting(this)
            unitTestStruct = struct();
            allMethods = methods(this);
            allMethods = allMethods(3:end); 
            unitTestStruct.TestedFunctions = cell(size(allMethods));
            unitTestStruct.TestResults = cell(size(allMethods));
            unitTestStruct.ErrorsFound = 0;
            for i = 1:length(allMethods)
                current_method = allMethods{i};
                unitTestStruct.TestedFunctions{i} = current_method;
                try 
                   result = this.(current_method);
                    unitTestStruct.TestResults{i} = result;
                    unitTestStruct.ErrorsFound = unitTestStruct.ErrorsFound + result;
                catch ME
                    unitTestStruct.unitTestStructTestResults{i} = ME;
                    unitTestStruct.ErrorsFound = unitTestStruct.ErrorsFound + 1;
                end
            end
        end
        
        
        function result = lineageTreeTest(this)
            trackIndexArray = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15];
            trackParentArray = [0 1 1 2 2 3 3 4 4 5 5 6 6 7 7];
            rootTrack = 1;
            rootTrackRelatedCellArray = [2:15];
            lineageTree = this.Tracker.lineage.getRootTrackTreeObject(trackIndexArray, ...
                trackParentArray, rootTrack, rootTrackRelatedCellArray );
            %lineageTree.plot;
            load('lineageTreeTest.mat');
            result = ~isequal(lineageTreeDefault, lineageTree);
        end
        
        
        function result = mammalianLineageCostTest(this)
             load('mammalianLineageCostTest.mat');
             costRes = this.Tracker.lap.mammalianLineageCost( ...
                 oldFrameData, newFrameData, daughterpairs, ...
                 parents, daughterpairtypes, parenttypes);
             result = ~isequal(cost, costRes);
        end
        
        
    end
end

