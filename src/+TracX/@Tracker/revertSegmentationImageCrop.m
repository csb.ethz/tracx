%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = revertSegmentationImageCrop( this )
% REVERTSEGMENTATIONIMAGECROP Reverts a potentially applied image
% crop during segmentation to map the segmentation objects' centroid
% coordintates to the raw image.
%
% Args:
%    this (:obj:`obj`):                     :class:`Tracker` object
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                           Acts on :class:`Tracker` object
%                                           directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if ~isempty(this.configuration.ProjectConfiguration. ...
        imageCropCoordinateArray)
    this.data.mapCrop2RawCoordinates(this.configuration.ProjectConfiguration. ...
        imageCropCoordinateArray)
    this.configuration.ProjectConfiguration. ...
        setImageCropCoordinatePriorReversionArray(this.configuration. ...
        ProjectConfiguration.imageCropCoordinateArray)
    
    this.configuration.ProjectConfiguration.setImageCropCoordinateArray([])
    this.utils.printToConsole('INFO: Coordinate reversion: DONE')
else
    this.utils.printToConsole(['INFO: Coordinate reversion: No crop coordinates', ...
        ' found. Skipping this step'])
end

end
