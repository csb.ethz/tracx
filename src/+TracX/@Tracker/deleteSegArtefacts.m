%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function deleteSegArtefacts(this, varargin)
% DELSEGARTEFACTS Deletes potential segmentation artefacts (segmented
% objects which are no cells based the autofluorescence.
% 
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    varargin (:obj:`str varchar`):
%
%        * **channel** (:obj:`int`):     Channel number :class:`~+TracX.@QuantificationData.QuantificationData.fluo_id`  of the fluorescent channel  to use for segmentation artefact detection. Defaults to 1.
%        * **stdFactor** (:obj:`float`): Factor standard deviation. Defaults to 2.
%        * **neighbourhoodSearchRadius** (:obj:`int`): Neighbourhood radius. Defaults to :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`Tracker` object
%                                               directly and deletes selected
%                                               data below threshold.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
defaultChannel = 1;
defaultStdFactor = 2;
defaultNeighbourhoodSearchRadius = this.configuration.ParameterConfiguration. ...
    neighbourhoodSearchRadius;
ids = unique(this.data.QuantificationData.fluo_id);
names = unique(this.data.QuantificationData.fluo_name);
validChannel = @(x) any(ismember(x, ids)) || any(cell2mat(strfind(names, x)));
validStdFactor = @(x) isnumeric(x) && x > 0; 
validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
addRequired(p, 'this', @isobject);
addOptional(p, 'channel', defaultChannel, validChannel);
addOptional(p, 'stdFactor', defaultStdFactor, validStdFactor);
addOptional(p, 'neighbourhoodSearchRadius', defaultNeighbourhoodSearchRadius, ...
    validNeighbourhoodSearchRadius);
parse(p, this, varargin{:});

this.data.delSegArtefacts(p.Results.neighbourhoodSearchRadius, ...
    p.Results.channel, p.Results.stdFactor)

end
