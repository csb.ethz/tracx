%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = testMarkerTrackingParameters(this, testRange, varargin)
% TESTTRACKINGPARAMETERS Tests the marker tracking parameters as defined in 
% defined in :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration`
% and displays the results for the given range.
% 
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    testRange (:obj:`array`, 1x2):            Image frame test range to
%                                              test Tracking [fromFrame, toFrame].
%    varargin (:obj:`str varchar`):
%
%        * **divisionMarkerMaxTrackFrameSkipping** (:obj:`int`): Max marker frame skiping. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerMaxTrackFrameSkipping`
%        * **divisionMarkerMaxObjCenterDisplacement** (:obj:`int`): Max marker displacement. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerMaxObjCenterDisplacement`
%        * **divisionMarkerMaxObjSizeIncrease** (:obj:`float`): Max marker size increase. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerMaxObjSizeIncrease`
%        * **divisionMarkerMaxObjSizeDecrease** (:obj:`float`): Max marker size decrease. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerMaxObjSizeDecrease`
%        * **divisionMarkerFingerprintHalfWindowSideLength** (:obj:`int`): Max marker fingerprint half window side length. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerFingerprintHalfWindowSideLength`
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Displays the marker tracking
%                                              results in an interactive
%                                              movie player.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
defaultMaxTrackFrameSkipping = 2;
defaultMaxAllowedMarkerCenterDisplacement = mean(this.lineage.lineageData.obj_major_axis);
defaultMaxMarkerSizeIncrease = 4;
defaultMaxMarkerSizeDecrease = 0.6;
defaultFingerprintHalfWindowSideLength = 10;
defaultQc = false;
validParam = @(x) assert(isnumeric(x) && isscalar(x) && (x > 0));
p.addRequired('testRange', @validateInput)
addOptional(p,'divisionMarkerMaxTrackFrameSkipping', defaultMaxTrackFrameSkipping, validParam);
addOptional(p,'divisionMarkerMaxObjCenterDisplacement', defaultMaxAllowedMarkerCenterDisplacement, validParam);
addOptional(p,'divisionMarkerMaxObjSizeIncrease',defaultMaxMarkerSizeIncrease, validParam);
addOptional(p,'divisionMarkerMaxObjSizeDecrease',defaultMaxMarkerSizeDecrease, validParam);
addOptional(p,'divisionMarkerFingerprintHalfWindowSideLength',defaultFingerprintHalfWindowSideLength, ...
    validParam);
addOptional(p,'qc',defaultQc, @isbool);
p.parse(testRange, varargin{:});

this.utils.printToConsole(sprintf(...
    'TEST: Tracking parameters frames: %d - %d', testRange(1), testRange(2)))

stF = this.configuration.ProjectConfiguration.trackerStartFrame;
eF = this.configuration.ProjectConfiguration.trackerEndFrame;
this.configuration.ProjectConfiguration.setTrackerStartFrame(testRange(1))
this.configuration.ProjectConfiguration.setTrackerEndFrame(testRange(2))

msg = evalc('builtin(''disp'',this.configuration.ParameterConfiguration'')');
this.utils.printToConsole(sprintf('TEST: %s', msg))

trackingMask = fullfile(this.configuration.ProjectConfiguration.segmentationResultDir,...
    this.configuration.ProjectConfiguration. ...
    trackerResultMaskFileArray{1});
if (exist(trackingMask, 'file') == 2) == 0
     pBar = this.utils.progressBar(this.configuration.ProjectConfiguration. ...
        trackerEndFrame, 'Title', 'Writing tracking images:');
    for frameNumber = this.configuration.ProjectConfiguration. ...
            trackerStartFrame : this.configuration.ProjectConfiguration. ...
        trackerEndFrame
        
        this.saveTrackerMaskFrame(frameNumber)  
        pBar.step([], [], []);
    end
    pBar.release();
    this.utils.printToConsole('SAVE: Done writing tracking masks.')
end

% Segment the frames first
this.lineage.lineageData = TracX.LineageData();
for channel = this.configuration.ProjectConfiguration. ...
        fluoImageIdentifier
    
    selChl = strfind(this.configuration.ProjectConfiguration. ...
        fluoImageIdentifier, channel);
    chlIdx = find(~cellfun('isempty', selChl), 1);
    for f = testRange(1):testRange(2)-1
        [foundEdgesFilledMaskedLabeled, segMaskDilatedRaw, ...
            fluoImgDenoised, this.lineage] = this.lineage.segmentBudNeck(f, chlIdx, []);
        
    end
end

% Run the tracker with the given parameters
this.lineage.runMarkerTracking(varargin{:})

% Display results
img = this.imageProcessing.generateMarkerControlImageFrame(1);
stack = zeros(size(img,1), size(img,2), 3, (testRange(2) - ...
    testRange(1))+1, 'uint8');
for f = testRange(1):testRange(2)-1
   img = this.imageProcessing.generateMarkerControlImageFrame(f);
   stack(:,:,:, f-(testRange(1)-1)) = img;
end

% Create a figure
hFig = figure('Name','TracX :: Movie Player', 'NumberTitle', 'off', 'Color', ...
    'white');
warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
warning('off', 'MATLAB:class:PropUsingAtSyntax');
javaFrame=get(hFig, 'javaframe');
javaFrame.setFigureIcon(javax.swing.ImageIcon(fullfile(which('TrackerLogo.png'))));

hImg = imshow(stack(:,:,:,1));
axis tight manual
ax = gca;
ax.NextPlot = 'replaceChildren';
% Create the PlayBar object 
playBar = TracX.ExternalDependencies.PlayBar(hFig, numel(testRange(1):testRange(2)));
% Create a listener to listen for the PlayBar's UpdateEvent event
addlistener(playBar, 'UpdateEvent', @onUpdate);

this.utils.printToConsole('TEST: Done')

% Reset previous parameter & state
this.configuration.ProjectConfiguration.setTrackerStartFrame(stF)
this.configuration.ProjectConfiguration.setTrackerEndFrame(eF)
this.lineage.lineageData = TracX.LineageData();

function onUpdate( hSource, hEventData )
j = hSource.getIndex;
frame = stack(:,:,:, j);
hImg.CData = frame;
drawnow();
end

end

function ret = validateInput(x)
   ret = false;
   if x(1) < x(2) && x(2)-x(1) >= 1
       ret = true;
   else
       error(['Input [trackStartFrame, trackEndFrame] does not satisfy ' ...
           '(trackStartFrame < trackEndFrame) and (trackEndFrame - ', ...
           'trackStartFrame >= 1)']);
   end
end