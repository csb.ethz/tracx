%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = testImageBorderDataRemoval(this, ...
    frameNumber, borderOffset)
% TESTIMAGEBORDERDATAREMOVAL Tests the data removal for cells touching the
% image border within borderOffset distance.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
%    borderOffset (:obj:`int`):                Offset from image border in
%                                              pixel to exclude from tracking.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Displays the detected
%                                              segmentation artifacts in an 
%                                              image.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
validFrame = @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame;
validOffset = @(x) x > 1 & x < min(this.configuration. ...
    ProjectConfiguration.imageHeight, this.configuration. ...
    ProjectConfiguration.imageWidth);
p.addRequired('frameNumber', validFrame)
p.addRequired('borderOffset', validOffset)
p.parse(frameNumber, borderOffset);

this.utils.printToConsole(sprintf('TEST: Data removal on frame %d with offset %d', ...
    frameNumber, borderOffset))

imgSize = [this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth];
idx = this.data.getUUIDOutsideROI(imgSize, borderOffset);


if ~isempty(idx)
    uuidArray = this.data.getFieldArray('uuid');
    fr = this.data.getFieldArray('cell_frame');
    ci = this.data.getFieldArray('cell_index');
    this.imageVisualization.plotAnnotatedControlImageFrame(frameNumber, 'fieldName', ...
        'cell_index', 'highlightFieldNameVal', ci(and(ismember(uuidArray, ...
        idx), fr == frameNumber)))
else
    this.utils.printToConsole('TEST: Nothing found to remove')
end

this.utils.printToConsole('TEST: Done')

end
