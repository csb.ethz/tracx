%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function runLineageReconstruction(this, varargin )
% RUNLINEAGERECONSTRUCTION Starts lineage reconstruction based on tracking
% results for symmetrically and asymmetrically dividing cells. Wrapper for
% lineage instance.
%
% Args:
%    this (:obj:`object`):                          :class:`Tracker` instance   
%    varargin (:obj:`str varchar`):
%
%        * **symmetricalDivision** (:obj:`str`):    Flag if lineage reconstruction should be run for symmetrical division. Defaults to false.
%        * **writeControlImages** (:obj:`bool`):    Flag if control images should be written. Deftaults to false.
%        * **nuclearMarkerChannel** (:obj:`int`):   Channel that depicts a nuclear marker (such as i.e. Whi5)
%
% Returns
% -------
%    void :obj:`-`                                
%                                                   Acts on:class:`Tracker` directly
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched = true;
defaultNuclearMarkerChannel = nan;
p.addParameter('symmetricalDivision', false, @(x)validateattributes(x, ...
    {'logical'}, {'scalar'}))
p.addParameter('writeControlImages', false, @(x)validateattributes(x, ...
    {'logical'}, {'scalar'}))
p.addOptional('nuclearMarkerChannel', defaultNuclearMarkerChannel, ...
    @(x) isnumeric(x) || isnan(x))
p.parse(varargin{:});

% Make sure the tracking mask were exported
trackingMask = fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, this.configuration.ProjectConfiguration. ...
    trackerResultMaskFileArray{1});
if (exist(trackingMask, 'file') == 2) == 0
     pBar = this.utils.progressBar(this.configuration.ProjectConfiguration. ...
        trackerEndFrame, 'Title', 'Writing tracking images:');
    for frameNumber = this.configuration.ProjectConfiguration. ...
            trackerStartFrame : this.configuration.ProjectConfiguration. ...
        trackerEndFrame
        
        this.saveTrackerMaskFrame(frameNumber)  
        pBar.step([], [], []);
    end
    pBar.release();
    this.utils.printToConsole('SAVE: Writing tracking masks. Done!')
end

if p.Results.symmetricalDivision == 1 % Lineage reconstruction for symmetrically
    % dividing cells
    
    this.lineage.symmetricalLineageReconstruction();
    % Set finished flag
    this.data.TmpLinAss.reconstructionIsFinished = true;
    treeArray = this.imageVisualization.plotLineageTree('divisionType', ...
        'symmetrical', 'plotToFigure', true);
    
    if p.Results.writeControlImages
        
        this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
            ' Save control images']);
        
        pBar = this.utils.progressBar(numel(treeArray), ...
            'Title', 'SAVE: lineage trees' );
        for i = 1:numel(treeArray)
            if isa(treeArray{i},'matlab.ui.Figure')
                saveas(treeArray{i}, fullfile(this.configuration.ProjectConfiguration. ...
                    segmentationResultDir, sprintf('lineage_tree_track_%d.png', ...
                    i)));
            end
            pBar.step(1, [], []);
        end
        pBar.release();
        
        nI = this.configuration.ProjectConfiguration. ...
                trackerEndFrame - this.configuration.ProjectConfiguration. ...
                trackerStartFrame + 1;
        pBar = this.utils.progressBar(nI, ...
            'Title', 'SAVE: Cell pole age control' );
        for frameNumber = this.configuration.ProjectConfiguration. ...
                trackerStartFrame:this.configuration.ProjectConfiguration. ...
                trackerEndFrame
            
            labeledImg = this.imageProcessing. ...
                generatePoleAgeControlImageFrame(this.configuration. ...
                ProjectConfiguration.fontSize, frameNumber);
            this.io.writeToPNG(labeledImg, fullfile(this.configuration. ...
                ProjectConfiguration.segmentationResultDir, ...
                sprintf('lineage_pole_age_%d.png', frameNumber)));
            pBar.step(1, [], []);
        end
        pBar.release();
    end  
    this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                ' Done']);
    
else % Lineage reconstruction for asymmetrically
    % dividing cells
    
    this.lineage.runLineageReconstruction(...
        'symmetricalDivision',  p.Results.symmetricalDivision, ...
        'writeControlImages', p.Results.writeControlImages, ...
        'nuclearMarkerChannel', p.Results.nuclearMarkerChannel, ...
        varargin{:})
    
    if p.Results.writeControlImages == true && this.data.TmpLinAss.reconstructionIsFinished == true
        this.saveTrackerMarkerControlImages()
        
        this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
            ' Save lineage tree figures']);
        
        treeArray = this.imageVisualization.plotLineageTree(...
            'plotToFigure', true);
        pBar = this.utils.progressBar(numel(treeArray), ...
            'Title', 'SAVE: lineage trees' );
        for i = 1:numel(treeArray)
            if isa(treeArray{i},'matlab.ui.Figure')
                saveas(treeArray{i}, fullfile(this.configuration.ProjectConfiguration. ...
                    segmentationResultDir, sprintf('lineage_tree_track_%d.png', ...
                    i)));
            end
            pBar.step(1, [], []);
        end
        pBar.release();
        this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                ' Done!']);
    end
    
end
end