%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ roi ] = testROIDataRemoval(this, frameNumber)
% TESTROIDATAREMOVAL Tests the data removal for cells outside a ROI.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame number.
% 
% Returns
% -------
%  roi: (:obj:`array`)          
%                                              Returns the region of
%                                              interest polygon vertices.
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
validFrame = @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame;
validOffset = @(x) x > 1 & x < min(this.configuration. ...
    ProjectConfiguration.imageHeight, this.configuration. ...
    ProjectConfiguration.imageWidth);
p.addRequired('frameNumber', validFrame)
p.parse(frameNumber);

this.utils.printToConsole(sprintf('TEST: Data removal on frame %d.', ...
    frameNumber))

imgSize = [this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth];
% idx = this.data.getUUIDOutsideROI(imgSize, borderOffset);

% Check coordinates if they are in roi for currentFrame
this.imageVisualization.plotAnnotatedControlImageFrame(frameNumber, 'fieldName', ...
        'cell_index')
% Raw ROI
pH = drawpolygon;
roi = pH.Position;

try
    insideIdx = pH.inROI(this.data.getFieldArrayForFrame('cell_center_x', frameNumber), ...
        this.data.getFieldArrayForFrame('cell_center_y', frameNumber));
    
    if ~isempty(insideIdx)
        close(gcf)
        ci = this.data.getFieldArrayForFrame('cell_index', frameNumber);
        this.imageVisualization.plotAnnotatedControlImageFrame(...
            frameNumber, 'fieldName', 'cell_index', 'highlightFieldNameVal', ...
            ci(~insideIdx));
        
        this.utils.printToConsole('TEST: Remove:')

        d = ci(~insideIdx)';
        fac = ceil(size(d, 2)/10);
        dd = nan(10*fac, 1);
        dd(1:numel(d)) = d;
        ddd = reshape(dd', 10, [])';
        for k = 1:size(ddd, 1)
            this.utils.printToConsole(sprintf('TEST: %s', regexprep(num2str(ddd(k, ...
                ~isnan(ddd(k,:)))),'\s+',', ')))
        end
         
    else
        this.utils.printToConsole('TEST: Nothing found to remove')
    end
    
    this.utils.printToConsole('TEST: Done')
catch e
    close(gcf)
    this.utils.printToConsole('ERROR: No polygon found. Please try again.')
end

end
