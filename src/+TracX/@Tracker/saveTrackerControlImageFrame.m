%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = saveTrackerControlImageFrame( this, frameNumber, varargin )
% SAVETRACKERCONTROLIMAGES Saves a tracking control images with 
% each tracked cell labeled with its track index for a given image frame.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
%    varargin (:obj:`str varchar`):
%
%        * **fieldName** (:obj:`str`):              Property to be displayed on the image. Defaults to 'track_index' can be any propery of :class:`~+TracX.@SegmentationData.SegmentationData`.
%        * **highlightFieldNameVal** (:obj:`int`):  Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **fontSize** (:obj:`str`):               Font size of property to be displayed.
%        * **removeContainedTracks** (:obj:`bool`): If contained tracks within tracks should be removed. Defaults to true.
%        * **isMarker** (:obj:`bool`):              If markers should be displayed. Defaults to false.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves images to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

controlImg = this.imageProcessing.generateAnnotatedControlImageFrame(frameNumber);

if ndims(controlImg) == 4
    this.io.writeToTIF(controlImg, fullfile(this.configuration.ProjectConfiguration. ...
        trackerResultsDir, sprintf('%s_%s_frame%0.3d.tif', ...
        this.configuration.ProjectConfiguration. ...
        trackerPrefix, this.configuration. ...
        ProjectConfiguration.controlPrefix, frameNumber)))
elseif ndims(controlImg) == 3
    this.io.writeToPNG(controlImg, fullfile(this.configuration.ProjectConfiguration. ...
        trackerResultsDir, sprintf('%s_%s_frame%0.3d.png', ...
        this.configuration.ProjectConfiguration. ...
        trackerPrefix, this.configuration. ...
        ProjectConfiguration.controlPrefix, frameNumber)))
else
    error('Image dimensions are not valid.');
end
end
