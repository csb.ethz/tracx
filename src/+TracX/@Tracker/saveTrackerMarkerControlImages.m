%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = saveTrackerMarkerControlImages(this)
% SAVEMARKERCONTROLIMAGES Saves marker tracking control images to disk.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves images to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if isempty(this.data.TmpLinAss)
    this.utils.printToConsole('WARNING: Please run lineage reconstruction first!')  
    return
end
warning('off','images:label2rgb:zerocolorSameAsRegionColor') 
n = this.configuration.ProjectConfiguration.trackerEndFrame-1;
obj = this.utils.progressBar(n, ...
    'Title', 'SAVE: Marker control images' ...
    );
for frameNumber = this.configuration.ProjectConfiguration.trackerStartFrame:n
    
    labeledMask = this.imageProcessing.generateMarkerControlImageFrame(...
        frameNumber);
    
    this.io.writeToPNG(labeledMask, fullfile(this.configuration. ...
        ProjectConfiguration.trackerResultsDir, ...
        sprintf('%s_%s_%s_frame%0.3d.png', ...
        this.configuration.ProjectConfiguration. ...
        trackerPrefix, 'marker', this.configuration. ...
        ProjectConfiguration.controlPrefix, frameNumber)))

  obj.step([], [], []);
end
obj.release();
