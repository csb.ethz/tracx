%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = runReQuantification(this, fluoTags, varargin)
% RUNREQUANTIFICATION Runs the re segmentation and quantification of merged
% tracking masks for buds and mothers prior division. 
% It first runs the CellXMaskInterface to resegment and quanfity the
% merged labels. Then in loads the data and updates it for the mothers
% and deletes the bud entries prior division (now merged with mothers) to
% make sure there is no data duplication.
%
% Args:
%    this (:obj:`object`):                                    :class:`Tracker` instance. 
%    fluoTags (:obj:`array`):                                 Array with fluoTags names. 
%                                                             Fluorescent
%                                                             images file
%                                                             identifier.
%    varargin (:obj:`str varchar`):
%
%        * **fileIdentifierSegmentationImages** (:obj:`str`): File identifier for segmentation images.
%
% Returns
% -------
%    void :obj:`-`                                
%                                                             Runs re
%                                                             quantification and
%                                                             writes results to
%                                                             disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

bfIdent = strsplit(this.configuration.ProjectConfiguration. ...
    imageFingerprintFileArray{1}, '_');
defaultFileIdentifierSegmentationImages = sprintf('%s*', bfIdent{1});
p = inputParser;
p.addRequired('fluoTags', ...
    @(x) validateattributes(x, {'cell'}, {}));
p.addOptional('fileIdentifierSegmentationImages', ...
    defaultFileIdentifierSegmentationImages, @ischar);
parse(p, fluoTags, varargin{:})

idx = strfind(p.Results.fileIdentifierSegmentationImages, '*');
if isempty(idx)
   fileIdentifierSegmentationImages = sprintf('%s*', ...
        p.Results.fileIdentifierSegmentationImages);
elseif idx ~= numel(p.Results.fileIdentifierSegmentationImages)
    fileIdentifierSegmentationImages = sprintf('%s*', ...
        p.Results.fileIdentifierSegmentationImages);
else
    fileIdentifierSegmentationImages = p.Results. ...
        fileIdentifierSegmentationImages;
end
            
this.utils.printToConsole(['REQUANTIFICATION: Starting merging segmentation', ...
    ' masks'])
% Merge masks
this.imageProcessing.generateMergedMask()
% Export the masks to tif and tmp folder
fprintf('\n')
this.utils.printToConsole(['REQUANTIFICATION: ', ...
                        ' Done merging segmentation masks']);
this.utils.printToConsole(['REQUANTIFICATION: ', ...
                        ' Start copying merged segmentation masks']);
this.imageProcessing.generateMergedMaskForReSegmentation(this.lineage.tracks2Merge)
fprintf('\n')
this.utils.printToConsole(['REQUANTIFICATION: ', ...
                        ' Done copying merged segmentation masks']);

this.utils.printToConsole(['REQUANTIFICATION: Starting re segmentation and', ...
    ' quantification from merged masks'])

% Requantify final_track_mask_* from temp dir (merged bud with mother until real division)
trackerSeg = TracX.Tracker();
imagePath = this.configuration.ProjectConfiguration.imageDir;
segmentationPath = fullfile(this.configuration.ProjectConfiguration. ...
segmentationResultDir, 'tmp');
segmentationFileNameRegex = 'final_track_mask_*';
imageFileNameRegex = fileIdentifierSegmentationImages;
fluoTags = p.Results.fluoTags;
trackerSeg.prepareDataFromSegmentationMask(imagePath, ...
    segmentationPath, segmentationFileNameRegex, ...
    imageFileNameRegex, 'fluoTags', fluoTags, 'customRegEx', '(?<=)(\d*)')
this.utils.printToConsole(['REQUANTIFICATION: Done with re segmentation and' ...
    ' quantification from merged masks'])

% Import re-segemented & quantified data
this.utils.printToConsole('REQUANTIFICATION: Start importing new data')
trackerTemp = TracX.Tracker();
trackerTemp.configuration.ProjectConfiguration.setSegmentationResultDir(...
    fullfile(this.configuration.ProjectConfiguration.segmentationResultDir, ...
    'tmp'))
trackerTemp.configuration.ProjectConfiguration.setNFramesToImport(...
    trackerSeg.configuration.ProjectConfiguration.nFramesToImport)
trackerTemp.configuration.ParameterConfiguration.setNeighbourhoodSearchRadius(...
    this.configuration.ParameterConfiguration.neighbourhoodSearchRadius)
trackerTemp.configuration.ProjectConfiguration.setSegmentationFiles()
trackerTemp.configuration.ProjectConfiguration.setTrackerEndFrame(...
    trackerSeg.configuration.ProjectConfiguration.nFramesToImport)
trackerTemp.importData()
clear trackerSeg

% Insert re-segemented & quantified data into TrackerData
data.SegmentationData = trackerTemp.data.SegmentationData;
data.QuantificationData = trackerTemp.data.QuantificationData;
this.data.insertMergedData(data, this.lineage.tracks2Merge)
clear trackerSeg

% Delete daughters entry up to division in TrackerData
this.data.deleteData( this.lineage.tracks2Merge(:,5), ...
    this.configuration.ParameterConfiguration.neighbourhoodSearchRadius)
this.utils.printToConsole('REQUANTIFICATION: Done updating merged data')
this.utils.printToConsole('REQUANTIFICATION: successfully finished!')

end