%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function prepareDataFromSegmentationMask(this, imagePath, ...
    segmentationPath, segmentationFileNameRegex, ...
    imageFileNameRegex, varargin)
% PREPAREDATAFROMSEGMENTATIONMASK Allows tracking with
% segmentation results from any software given a un- or labeled
% segmentation mask per image frame. The mask is then converted
% into the CellX format which is used internaly for structuring the
% data during tracking.
%
% Args:
%    this (:obj:`obj`):                      :class:`Tracker` instance.
%    imagePath (:obj:`str`):                 Path to the experiment raw images.
%                                            :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageDir`
%    segmentationPath (:obj:`str`):          Path to segmentation results. 
%                                            :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
%    segmentationFileNameRegex (:obj:`str`): Regex for the mask
%                                            filename identifier
%                                            i.e. Mask_*.
%    imageFileNameRegex (:obj:`str`):        Regex for the image
%                                            filename identifier
%                                            i.e. BF_*.
%    varargin (:obj:`str varchar`):
%
%        * **pxPerPlane** (:obj:`int`):      Pixel per image plane. Defaults to 0.
%        * **fluoTags** (:obj:`int`):        Identifier for potential fluorescent images to quantify i.e. {'XFP'}
%        * **flatFieldFileNames** (:obj:`int`): Names of potential flat  fields.
%        * **customRegEx** (:obj:`str`):     Custom regex to identify the segmetnation masks.
%
% Returns
% -------
%     void: (:obj:`-`)          
%                                            Writes results to disk
%                                            directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultFluoTags = {};
defaultFlatFieldFileNames = {};
defaultRegEx = '(?<=_time)(\d*)';
p = inputParser;
p.addRequired('imagePath', @ischar);
p.addRequired('segmentationPath', @ischar);
p.addRequired('segmentationFileNameRegex', @ischar);
p.addRequired('imageFileNameRegex', @ischar);
p.addParameter('pxPerPlane', 0);
p.addParameter('fluoTags', defaultFluoTags,  ...
    @(x) validateattributes(x, {'cell'}, {}));
p.addParameter('flatFieldFileNames', defaultFlatFieldFileNames, ...
    @(x)validateattributes(x, 'cell'));
p.addParameter('customRegEx', defaultRegEx, @ischar);
parse(p, imagePath, segmentationPath, segmentationFileNameRegex, ...
    imageFileNameRegex, varargin{:})
this.utils.printToConsole('INFO: Started CellXMask extractor with following parameters:')
disp(p.Results)
% Start cellx mask segmenter
fluoFileArray = {};
segmentationFileNamesTmp = dir(fullfile(segmentationPath, ...
    segmentationFileNameRegex));
segmentationResultFiles = this.utils.natSortNames(...
    {segmentationFileNamesTmp.name});
this.configuration.ProjectConfiguration.setNFramesToImport(...
    numel(segmentationResultFiles))
imageFileNamesTmp = dir(fullfile(imagePath, imageFileNameRegex));
imageFileArray = this.utils.natSortNames({imageFileNamesTmp.name});
for f = 1:numel(p.Results.fluoTags)
    fluoTmp = dir(fullfile(imagePath, sprintf('%s*', ...
        p.Results.fluoTags{f})));
    fluoFileArray{f} = this.utils.natSortNames({fluoTmp.name});
end

% Check if file input is correct
if any([isempty(imageFileArray), isempty(segmentationResultFiles)])
    chkR = [isempty(imageFileArray), isempty(segmentationResultFiles)];
    if chkR(1) == 1
        this.utils.printToConsole('ERROR: Could not identify the image files.')
        this.utils.printToConsole(sprintf('INFO: Please check the image file filter %s', ...
            imageFileNameRegex))
    elseif chkR(2) == 1
        this.utils.printToConsole('ERROR:  Could not identify the mask files.')
        this.utils.printToConsole(sprintf('INFO: Please check the mask file filter %s', ...
            segmentationFileNameRegex))
    elseif chkR(1) == 1 && chkR(2) == 1
        this.utils.printToConsole('ERROR: Could not identify the image and mask files.')
        this.utils.printToConsole(sprintf('INFO: Please check the image and mask file filter %s %s', ...
            imageFileNameRegex, segmentationFileNameRegex))
    end
    return
end

if ~isempty(fluoFileArray)
    if any(cellfun(@isempty, fluoFileArray))
        chkR = cellfun(@isempty, fluoFileArray);
        strP = join(p.Results.fluoTags(find(chkR)));
        this.utils.printToConsole('ERROR: Could not identify the additional channel.')
        this.utils.printToConsole(sprintf('INFO: Please check the image and mask file filter %s ', ...
            strP{1}))
        return
    end
end

% Automatically create a CellX config if does not exist yet and
% read that file. Can be modified, if needed to influence the
% CellXMaskInterface outcome.
configFileName = fullfile(segmentationPath, 'CellXMaskInterfaceCfg.xml');
if ~exist(configFileName, 'file') && p.Results.pxPerPlane == 0
    c = TracX.ExternalDependencies.CellXMaskInterface.CellXMaskConfiguration();
    c.setMaximumCellLength(11)
    c.setMembraneWidth(1)
    c.setMembraneLocation(2)
    c.setSeedRadiusLimit([4, 5])
    c.membraneIntensityProfile = [1,8,10,8, 1];
    c.setFluoAlignPixelMove(0) % assumed that segmentation mask
    % and fluo channels are well aligned. Can be changed in the
    % config file if needed
    c.toXML(configFileName)
end
% Automatically create segmentation result directory to have
% a consistent project structure
resultFolder = fullfile(segmentationPath);
if ~exist(resultFolder, 'dir')
    mkdir(resultFolder)
end

frameNumbers = regexpi(segmentationResultFiles, p.Results.customRegEx, 'match'); 
frameNumbers = str2double([frameNumbers{:}]);
if isempty(frameNumbers)
    this.utils.printToConsole('ERROR: No valid mask file frame number detected.')
    this.utils.printToConsole(sprintf('INFO: Please check the mask file filter %s and mask file names to contain _timeTTTT', ...
        segmentationFileNameRegex))
    return
else
    if frameNumbers(1) == 0
        frameNumbers = frameNumbers + 1;
    end
end
% Assumption is that all arrays are of equal size and sorted. I.e. if we
% have frame number 5 we find the oofImg and fluoImg as the 5th element.
parfor k = 1:numel(segmentationResultFiles)
    segmImage = fullfile(segmentationPath, ...
        segmentationResultFiles{k});
    oofImage = fullfile(imagePath, imageFileArray{frameNumbers(k)});
    fluoImages = cell(size((p.Results.fluoTags)));
    if p.Results.pxPerPlane == 0
        for f = 1:numel(p.Results.fluoTags)
            fluoImages{f} = double(imread(fullfile(imagePath, ...
                fluoFileArray{f}{frameNumbers(k)})));
        end
        segmMask = importdata(segmImage);
        cellxMaskInterface = TracX.ExternalDependencies.CellXMaskInterface. ...
            CellXMaskInterface(frameNumbers(k), configFileName,...
            segmMask, p.Results.fluoTags, p.Results.flatFieldFileNames,...
            fluoImages, resultFolder, oofImage);
        cellxMaskInterface.run()
    else
        for f = 1:numel(p.Results.fluoTags)
            fluoImages{f} = fullfile(imagePath, ...
                fluoFileArray{f}{k});
        end
        u = TracX.Utils();
        io = TracX.IO(u);
        io.maskInterface3D(k, segmImage, oofImage, resultFolder, ...
            p.Results.pxPerPlane, p.Results.fluoTags, fluoImages)
    end
end
end