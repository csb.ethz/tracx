%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function deleteByROI(this, roi, varargin)
% DELETEBYROI Deletes segmentation objects outside a region of interest
% (ROI). 
% 
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    roi (:obj:`array`):                       Region of interest defined 
%                                              by polygon vertices to keep
%                                              data for tracking.
%    varargin (:obj:`str varchar`):
%
%        * **neighbourhoodSearchRadius** (:obj:`int`): Neighbourhood radius. Defaults to :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`Tracker` object
%                                               directly and deletes selected
%                                               data outside ROI.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
defaultNeighbourhoodSearchRadius = this.configuration.ParameterConfiguration. ...
    neighbourhoodSearchRadius;

validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
validROI = @(x) isnumeric(x); 
addRequired(p, 'this', @isobject);
addRequired(p, 'roi', validROI);
addOptional(p, 'neighbourhoodSearchRadius', defaultNeighbourhoodSearchRadius, ...
    validNeighbourhoodSearchRadius);
parse(p, this, roi, varargin{:});

% Delete 
this.data.delByROI(p.Results.roi, p.Results.neighbourhoodSearchRadius)

end