%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = testSegmentationArtefactRemoval(this, frame, channel, ...
    stdFactor, varargin)
% TESTSEGMENTATIONARTEFACTREMOVAL Test of potential segmentation artefacts 
% removal using the cell autofluorescence. Outcome is visualized as image
% and detected cell indices printed to the console.
% Assumption is that false positive segmentations have no autofluorecence
% or comparable to the cell surounding.
% Useage Tracker.data.getPotSegArtefacts(fluoChannelId, stdFactor)
% fluoChannelId: Id number of the fluo channel as found in 
% unique(:class:`~+TracX.@QuantificationData.QuantificationData.fluo_id`) or its corresponding name
% unique(:class:`~+TracX.@QuantificationData.QuantificationData.fluo_name`)
% stdFactor: factor by which the std of the fluo background is multiplied
% to define a threshold below a cell intensity would be classified as
% artefact
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frame (:obj:`int`):                       Image frame to test.
%    channel (:obj:`int`):                     Channel number
%                                              :class:`~+TracX.@QuantificationData.QuantificationData.fluo_id` 
%                                              of the fluorescent channel 
%                                              to use for segmentation artefact 
%                                              detection. Defaults to 1.
%    stdFactor (:obj:`float`):                 Factor standard deviation.
%                                              Defaults to 2.
%    varargin (:obj:`str varchar`):
%
%        * **figure** (:obj:`object`):         Figure handle. Defaults  [];
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Displays the detected
%                                              segmentation artifacts in an 
%                                              image.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
defaultChannel = 1;
defaultStdFactor = 2;
ids = unique(this.data.QuantificationData.fluo_id);
names = unique(this.data.QuantificationData.fluo_name);
validChannel = @(x) any(ismember(x, ids)) || any(cell2mat(strfind(names, x)));
validStdFactor = @(x) isnumeric(x) && x > 0;
defaultFigureHandle = axes;
validFigure = @(x) isgraphics(x, 'figure') || isgraphics(x, 'axes');
addRequired(p, 'this', @isobject);
addRequired(p, 'frame', @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame);
addOptional(p, 'channel', defaultChannel, validChannel);
addOptional(p, 'stdFactor', defaultStdFactor, validStdFactor);
addOptional(p, 'figure', defaultFigureHandle, validFigure);
p.KeepUnmatched=true;
parse(p, this, frame, channel, stdFactor, varargin{:});

this.utils.printToConsole(sprintf(...
    'TEST: Segmentation artefact removal on frame: %d', frame))

[~, fr, ci, ~] = this.data.getPotSegArtefacts(channel, stdFactor);

values = ci(fr == frame)';
this.utils.printToConsole(sprintf('TEST: Detected cell indices: %s', ...
    regexprep(num2str(values),'\s+',', ')))

this.imageVisualization.plotAnnotatedControlImageFrame(frame, 'fieldName', ...
    'cell_index', 'highlightFieldNameVal', ci(fr == frame), 'figure', p.Results.figure)

this.utils.printToConsole('TEST: Done')
end
