%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = testFeatureDataRemoval(this, frameNumber, feature, threshold)
% TESTFEATUREDATAREMOVAL Tests the data removal for cells above a feature 
% (:class:`~+TracX.@SegmentationData.SegmentationData`) value threshold.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
%    feature (:obj:`str`):                     Any segmentation feature to
%                                              use to threshold the data before
%                                              tracking. :class:`~+TracX.@SegmentationData.SegmentationData`
%    threshold (:obj:`float`):                 Threshold. Feature values
%                                              below the threshold are selected.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Displays the detected
%                                              segmentation artifacts in an 
%                                              image.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
validFrame = @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame;
validFieldNames = [fieldnames(this.data.SegmentationData); fieldnames(this.data.QuantificationData)];
validFeature =  @(x) any(validatestring(x, validFieldNames));
validThreshold = @(x) isnumeric(x);
addRequired(p, 'this', @isobject);
addRequired(p, 'frameNumber', validFrame);
addRequired(p, 'feature', validFeature);
addRequired(p, 'threshold', validThreshold);
parse(p, this, frameNumber, feature, threshold);

featureVal = this.data.getFieldArrayForFrame(p.Results.feature, p.Results.frameNumber);
ci = this.data.getFieldArrayForFrame('cell_index', p.Results.frameNumber);

this.utils.printToConsole(sprintf('TEST: Data removal on frame %d.', ...
    frameNumber))

if ~isempty(ci(featureVal<p.Results.threshold))
    
    this.imageVisualization.plotAnnotatedControlImageFrame(...
        p.Results.frameNumber, 'fieldName', 'cell_index', ...
        'highlightFieldNameVal', ci(featureVal<p.Results.threshold));
    
    this.utils.printToConsole('TEST: Remove:')
    
    d = ci(featureVal<p.Results.threshold)';
    fac = ceil(size(d, 2)/10);
    dd = nan(10*fac, 1);
    dd(1:numel(d)) = d;
    ddd = reshape(dd', 10, [])';
    for k = 1:size(ddd, 1)
        this.utils.printToConsole(sprintf('TEST: %s', regexprep(num2str(ddd(k, ...
            ~isnan(ddd(k,:)))),'\s+',', ')))
    end
    
else
    this.utils.printToConsole('TEST: Nothing found to remove')
end

this.utils.printToConsole('TEST: Done')

end