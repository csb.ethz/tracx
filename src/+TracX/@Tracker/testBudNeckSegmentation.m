%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = testBudNeckSegmentation(this, frameNumber, varargin)
% TESTBUDNECKSEGMENTATION Tests the parameter for budneck segmentation and
% displays a control image to optimize the parameters if needed.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
%    varargin (:obj:`str varchar`):
%
%        * **divisionMarkerDenoising** (:obj:`int`): ROF denoising parameter. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerDenoising`
%        * **divisionMarkerEdgeSensitivityThresh** (:obj:`int`): Edge sensitivity threshold for segmenting the marker outlines. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerEdgeSensitivityThresh`
%        * **divisionMarkerConvexAreaLowerThresh** (:obj:`int`): Lower area limit for detected markers. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerConvexAreaLowerThresh`
%        * **divisionMarkerConvexAreaUpperThresh** (:obj:`int`): Upper area limit for detected markers. :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerConvexAreaUpperThresh`
%        * **contrastLimits** (:obj:`array`, 1x2): Image contras limits 0-1. 
% 
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Displays the detected
%                                              segmentation artifacts in an 
%                                              image.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

this.utils.printToConsole(sprintf('TEST: Bud neck segmentation on frame: %d', frameNumber))

defaultDivisionMarkerDenoising = this.configuration.ParameterConfiguration. ...
    divisionMarkerDenoising;
defaultDivisionMarkerEdgeSensitivityThresh = this.configuration.ParameterConfiguration. ...
    divisionMarkerEdgeSensitivityThresh;
defaultDivisionMarkerConvexAreaLowerThresh = this.configuration.ParameterConfiguration. ...
    divisionMarkerConvexAreaLowerThresh;
defaultDivisionMarkerConvexAreaUpperThresh = this.configuration.ParameterConfiguration. ...
    divisionMarkerConvexAreaUpperThresh;
defaultContrastLimits = [0, 1];
validContrastLimits = {'>=', 0, '<=', 1};

p = inputParser;
p.addRequired('this', @isobject)
p.addRequired('frame', @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}))
p.addParameter('divisionMarkerDenoising', defaultDivisionMarkerDenoising, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}))
p.addParameter('divisionMarkerEdgeSensitivityThresh', defaultDivisionMarkerEdgeSensitivityThresh, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}))
p.addParameter('divisionMarkerConvexAreaLowerThresh', defaultDivisionMarkerConvexAreaLowerThresh, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}))
p.addParameter('divisionMarkerConvexAreaUpperThresh', defaultDivisionMarkerConvexAreaUpperThresh, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}))
p.addParameter('contrastLimits', defaultContrastLimits, @(x)validateattributes(x, ...
    {'numeric'}, validContrastLimits))
p.parse(this, frameNumber, varargin{:});


dilationMorphStructElement = strel('square', 8);

% Load segmentation mask for current frame
segMask = this.io.readSegmentationMask(fullfile(...
    this.configuration.ProjectConfiguration.segmentationResultDir,...
    this.configuration.ProjectConfiguration. ...
    trackerResultMaskFileArray{p.Results.frame}));

% Dilate segmentation mask, that cells touch each others.
% Idea is to achive an overlap with bud neck marker segmentation
segMaskDilated = imdilate(segMask, dilationMorphStructElement);


[ foundEdgesFilledMaskedLabeled, foundEdgesFilled, fluoImgDenoised ] = ...
    this.lineage.detectBudNeckMarkerPixelArray( this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.fluoLineageFileArray{1}{p.Results.frame}, ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray, segMaskDilated, p.Results.divisionMarkerDenoising, ...
    p.Results.divisionMarkerEdgeSensitivityThresh, p.Results.divisionMarkerConvexAreaLowerThresh, ...
    p.Results.divisionMarkerConvexAreaUpperThresh);

fluoImgDenoisedN = this.imageProcessing.normalizeImage(fluoImgDenoised);
foundEdgesFilledMaskedLabeledM = foundEdgesFilledMaskedLabeled;
foundEdgesFilledMaskedLabeledM(foundEdgesFilledMaskedLabeled>=1) = 1;

figure('Renderer', 'painters', 'Position', [100 100 800 900], 'DefaultAxesPosition', [0.1, 0.1, 0.8, 0.8]); 
sh1 = subplot(2,2,1);
imagesc(foundEdgesFilled)
axis image;
title('budneck raw mask control')
sh2 = subplot(2,2,2);
imshow(imadjust(fluoImgDenoisedN, p.Results.contrastLimits))
title('Denoising control')
sh3 = subplot(2,2,3);
imagesc(foundEdgesFilledMaskedLabeled)
axis image;
title('detected budnecks control')
sh4 = subplot(2,2,4);
imshowpair(imadjust(fluoImgDenoisedN, p.Results.contrastLimits), ...
    foundEdgesFilledMaskedLabeledM)
title('Denoised image and detected budnecks overlay')
set(gcf,'color','w');
linkaxes([sh1, sh2, sh3, sh4],'xy');

this.utils.printToConsole('TEST: Done')

end
