%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = deleteSegObjAtBorder(this, borderOffset)
% DELETESEGOBJATBORDER Delete segmentation objects at the image border
% within the border offset.
% 
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    borderOffset (:obj:`int`):                Offset from image border in
%                                              pixel to exclude from tracking.
%
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`Tracker` object
%                                               directly and deletes selected
%                                               data from the image border 
%                                               up to the border offset.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
validFrame = @(x) isnumeric(x) && x>=this.configuration. ...
    ProjectConfiguration.trackerStartFrame && x<= this.configuration. ...
    ProjectConfiguration.trackerEndFrame;
validOffset = @(x) x > 1 & x < min(this.configuration. ...
    ProjectConfiguration.imageHeight, this.configuration. ...
    ProjectConfiguration.imageWidth);
p.addRequired('borderOffset', validOffset)
p.parse(borderOffset);

this.utils.printToConsole(sprintf('DELETE: Data removal with border offset %d', ...
    borderOffset))

imgSize = [this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth];
idx = this.data.getUUIDOutsideROI(imgSize, borderOffset);


if ~isempty(idx)
    
    this.utils.printToConsole('DELETE: cell indices:')
    
    d = idx';
    fac = ceil(size(d, 2)/10);
    dd = nan(10*fac, 1);
    dd(1:numel(d)) = d;
    ddd = reshape(dd', 10, [])';
    for k = 1:size(ddd, 1)
        this.utils.printToConsole(sprintf('DELETE: %s', regexprep(num2str(ddd(k, ...
            ~isnan(ddd(k,:)))),'\s+',', ')))
    end
    
    this.data.deleteData(idx, this.configuration.ParameterConfiguration. ...
    neighbourhoodSearchRadius)

    
else
    this.utils.printToConsole('DELETE: Nothing found to delete')
end

this.utils.printToConsole('DELETE: Done')

end
