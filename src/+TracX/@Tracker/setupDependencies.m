%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function [ status ] = setupDependencies(this)
% SETUPDEPENDENCIES Checks the presence of the external dependencies. If
% some are missing they are automatically downloaded. Required for manual
% installation (zip download) of tracx in the absence of git.
%
% Args:
%    this (:obj:`obj`):           :class:`Tracker` object
%
% Returns
% -------
%    status: (:obj:`int`)
%                                  Status, 1 successful 0 otherwise.
%
% :Authors:
%       - Andreas P. Cuny - Initial implementation

requiredSubmodules = {...
    'https://gitlab.com/csb.ethz/cellxmaskinterface/-/archive/CellXMaskInterfaceTracX/cellxmaskinterface-CellXMaskInterfaceTracX.zip', ...
    'https://gitlab.com/csb.ethz/MatlabProgressBar/-/archive/MatlabProgressBarTracX/MatlabProgressBar-MatlabProgressBarTracX.zip', ...
    'https://gitlab.com/csb.ethz/matlab-tree/-/archive/tracx-tree/matlab-tree-tracx-tree.zip', ...
    'https://github.com/grocid/rofdenoise/archive/refs/heads/master.zip'};

status = 0;

if ~isempty(fileparts(which('TracX.Tracker')))
    this.utils.printToConsole('CHECK: Full installation of external dependencies.')
    trackerDir = fileparts(which('TracX.Tracker'));
    trackerRoot = fileparts(trackerDir);
    if ~isfile(fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+CellXMaskInterface', filesep, 'CellXMaskInterface.m'))
        % download and unzip
        dest = this.utils.downloadFile(requiredSubmodules{1}, ...
            fullfile(trackerRoot, 'tmp.zip'));
        unzip(dest, fullfile(fileparts(dest), 'tmp'))
        dirList = dir(fullfile(fileparts(dest), 'tmp'));
        movefile(fullfile(dirList(3).folder, dirList(3).name, '*'), ...
            fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+CellXMaskInterface'));
        rmdir(fullfile(trackerRoot, 'tmp'), 's');
        
    end
    
    if ~isfile(fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+matlabProgressBar', filesep, 'ProgressBar.m'))
        % download and unzip
        dest = this.utils.downloadFile(requiredSubmodules{2}, ...
            fullfile(trackerRoot, 'tmp.zip'));
        unzip(dest, fullfile(fileparts(dest), 'tmp'))
        dirList = dir(fullfile(fileparts(dest), 'tmp'));
        movefile(fullfile(dirList(3).folder, dirList(3).name, '*'), ...
            fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+matlabProgressBar'));
        rmdir(fullfile(trackerRoot, 'tmp'), 's');
    end
    
    if ~isfile(fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+tree', filesep, '@tree', filesep, 'tree.m'))
        % download and unzip
        dest = this.utils.downloadFile(requiredSubmodules{3}, ...
            fullfile(trackerRoot, 'tmp.zip'));
        unzip(dest, fullfile(fileparts(dest), 'tmp'))
        dirList = dir(fullfile(fileparts(dest), 'tmp'));
        movefile(fullfile(dirList(3).folder, dirList(3).name, '*'), ...
            fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+tree'));
        rmdir(fullfile(trackerRoot, 'tmp'), 's');
    end
    
    if ~isfile(fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+rofdenoise', filesep, 'ROFdenoise.m'))
        % download and unzip
        dest = this.utils.downloadFile(requiredSubmodules{4}, ...
            fullfile(trackerRoot, 'tmp.zip'));
        unzip(dest, fullfile(fileparts(dest), 'tmp'))
        dirList = dir(fullfile(fileparts(dest), 'tmp'));
        movefile(fullfile(dirList(3).folder, dirList(3).name, '*'), ...
            fullfile(trackerRoot, '+ExternalDependencies', filesep, ...
            '+rofdenoise'));
        rmdir(fullfile(trackerRoot, 'tmp'), 's');
    end
    status = 1;
    this.utils.printToConsole('DONE: All dependencies successfully installed.')
else
    this.utils.printToConsole('ERROR: Tracker is not on the MATLAB PATH!')
    this.utils.printToConsole("INFO: Please run addpath(genpath('tracx\src'))inside the downloaded tracx folder.")
    status = 0;
end
end
