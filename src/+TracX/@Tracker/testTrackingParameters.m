%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function [ ] = testTrackingParameters(this, testRange, varargin)
% TESTTRACKINGPARAMETERS Tests the current set of tracker parameters as
% defined in :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration`.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    testRange (:obj:`array`, 1x2):            Image frame test range to
%                                              test Tracking [fromFrame, toFrame].
%    varargin (:obj:`str varchar`):
%
%        * **mode** (:obj:`array` 1x2):        Allows to test the dCRF tracker instead of the full tracker as well as with or without motion correction i.e [1,0].
%
% Returns
% -------
%     void: (:obj:`-`)
%                                              Displays the tracking
%                                              results in an interactive
%                                              movie player.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
validTestRange = @(x) x(1) < x(2) & x(2)-x(1) >= 1;
p.addRequired('testRange', @validateInput)
p.addParameter('mode', [], @(x) numel(x) == 2);
p.parse(testRange, varargin{:});

this.utils.printToConsole(sprintf(...
    'TEST: Tracking parameters frames: %d - %d', testRange(1), testRange(2)))

stF = this.configuration.ProjectConfiguration.trackerStartFrame;
eF = this.configuration.ProjectConfiguration.trackerEndFrame;
this.configuration.ProjectConfiguration.setTrackerStartFrame(testRange(1))
this.configuration.ProjectConfiguration.setTrackerEndFrame(testRange(2))
this.data.clearTrackerData(this.configuration.ParameterConfiguration. ...
    neighbourhoodSearchRadius);
this.data.VeryOld = TracX.TemporaryTrackingData();
this.data.VeryOldEval = TracX.NonAssignedTracks();

msg = evalc('builtin(''disp'',this.configuration.ParameterConfiguration'')');
this.utils.printToConsole(sprintf('TEST: %s', msg))
% Run the tracker with the given parameters
if ~isempty(varargin)
    this.runTracker(varargin{1});
else
    this.runTracker();
end

% Display results
stack = zeros(this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth, 3, testRange(2) - ...
    testRange(1), 'uint8');
for f = testRange(1):testRange(2)
    img = this.imageProcessing.generateAnnotatedControlImageFrame(f, 'track_index');
    stack(:,:,:, f-(testRange(1)-1)) = double(img);
end

% Create a figure
hFig = figure('Name','TracX :: Movie Player', 'NumberTitle', 'off', 'Color', ...
    'white');
warning('off','MATLAB:ui:javaframe:PropertyToBeRemoved');
warning('off', 'MATLAB:class:PropUsingAtSyntax');
javaFrame=get(hFig, 'javaframe');
javaFrame.setFigureIcon(javax.swing.ImageIcon(fullfile(which('TrackerLogo.png'))));

hImg = imshow(stack(:,:,:,1));
axis tight manual
ax = gca;
ax.NextPlot = 'replaceChildren';
% Create the PlayBar object 
playBar = TracX.ExternalDependencies.PlayBar(hFig, numel(testRange(1):testRange(2)));
% Create a listener to listen for the PlayBar's UpdateEvent event
addlistener(playBar, 'UpdateEvent', @onUpdate);

this.utils.printToConsole('TEST: Done')

% Reset previous parameter & state
this.configuration.ProjectConfiguration.setTrackerStartFrame(stF)
this.configuration.ProjectConfiguration.setTrackerEndFrame(eF)

function onUpdate( hSource, hEventData )
j = hSource.getIndex;
frame = stack(:,:,:, j);
hImg.CData = frame;
drawnow();
end

end

function ret = validateInput(x)
ret = false;
if x(1) < x(2) && x(2)-x(1) >= 1
    ret = true;
else
    error(['Input [trackStartFrame, trackEndFrame] does not satisfy ' ...
        '(trackStartFrame < trackEndFrame) and (trackEndFrame - ', ...
        'trackStartFrame >= 1)']);
end
end
