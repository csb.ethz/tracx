%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function generateScriptFromTracker(this, varargin)
% GENERATESCRIPTFROMTRACKER Generates a Matlab (.m) script file based on
% the tracker project configuration made with the GUI.
%
% Args:
%    this (:obj:`obj`):                     :class:`Tracker` object
%    varargin (:obj:`str varchar`):
%
%        * **fileName** (:obj:`str`):       File name. Defaults to TracX_project_ProjectName.m. 
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                           Writes text file to disk
%
% :Authors:
%    Thomas Kuendig - initial implementation

if ~isempty(varargin)
    [path, file, ext] = fileparts(char(varargin{1}));
    file = [file , ext];
else
    scriptName = ['TracX_project_',strrep(this.configuration.ProjectConfiguration.projectName,'-',''),'.m'];
    [file,path] = uiputfile('*.m','File Selection',scriptName);
end

if  all([path file])
    % checks if name contains - and replaces it
    if contains(file,'-')
        file = strrep(file,'-','_');
        this.utils.printToConsole("'-' is invalid in MATLAB script names has been replaced with '_'");
    end
    
    fpIdentifier = strsplit(this.configuration.ProjectConfiguration.imageFingerprintFileArray{1}, '_');
    lineageIdentifier = strsplit(this.configuration.ProjectConfiguration.fluoLineageFileArray{1}{1}, '_');
    cropPriorReversion = this.configuration.ProjectConfiguration.imageCropCoordinatePriorReversionArray;
    if isempty(cropPriorReversion)
        if isempty(this.configuration.ProjectConfiguration.imageCropCoordinateArray)
            cropCoords = [];
        else
            cropCoords = this.configuration.ProjectConfiguration.imageCropCoordinateArray;
        end
    else
       cropCoords = cropPriorReversion; 
    end
    
    if this.configuration.ProjectConfiguration.cellDivisionType == 1
        cellDivisionType = 'asymmetrical';
    elseif this.configuration.ProjectConfiguration.cellDivisionType == 0
        cellDivisionType = 'symmetrical';
    else
        cellDivisionType = ''; % -1
    end
    
    %open file
    scriptFile = fopen(fullfile(path,file),'w');
    
    
    %% configure
    fprintf(scriptFile, "%% Configure a new tracking project\n");
    fprintf(scriptFile, "projectName = '%s';\n",this.configuration.ProjectConfiguration.projectName);
    fprintf(scriptFile, "fileIdentifierFingerprintImages = '%s';\n", fpIdentifier{1});
    fprintf(scriptFile, "fileIdentifierWellPositionFingerprint = '%s';\n", this.configuration. ...
        ProjectConfiguration.imagePositionArray{1});
    fprintf(scriptFile, "fileIdentifierCellLineage = '%s';\n", lineageIdentifier{1});
    if isvector(cropCoords)
       fprintf(scriptFile, "imageCropCoordinateArray = [%d, %d, %d, %d];\n", cropCoords);
    else
       fprintf(scriptFile, "imageCropCoordinateArray = [];\n"); 
    end
    fprintf(scriptFile, "movieLength = %d;\n",this.configuration.ProjectConfiguration.nFramesToImport);
    fprintf(scriptFile, "cellsFilePath = '%s';\n",this.configuration.ProjectConfiguration.segmentationResultDir);
    fprintf(scriptFile, "imagesFilePath = '%s';\n",this.configuration.ProjectConfiguration.imageDir);
    fprintf(scriptFile, "cellDivisionType = '%s';\n",cellDivisionType);
    fprintf(scriptFile, "\n");
    
    %% create
    fprintf(scriptFile, "%%%% Create Tracker\n");
    fprintf(scriptFile, "Tracker = TracX.Tracker();\n");
    fprintf(scriptFile, "Tracker.createNewTrackingProject(projectName, imagesFilePath, ...\n");
    fprintf(scriptFile, "        cellsFilePath, fileIdentifierFingerprintImages, ...\n");
    
    fprintf(scriptFile, "        fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...\n");
    
    fprintf(scriptFile, "        imageCropCoordinateArray, movieLength, cellDivisionType);\n");
    fprintf(scriptFile, "Tracker.revertSegmentationImageCrop()\n");
    fprintf(scriptFile, "\n");
    
    %% set start and end
    fprintf(scriptFile, "%% Project configuration\n");
    fprintf(scriptFile,"Tracker.configuration.ProjectConfiguration.setTrackerStartFrame(%d);\n", ...
        this.configuration.ProjectConfiguration.trackerStartFrame);
    fprintf(scriptFile,"Tracker.configuration.ProjectConfiguration.setTrackerEndFrame(%d);\n", ...
        this.configuration.ProjectConfiguration.trackerEndFrame);
    
    %% extract parameters
    %Parameter sets
    defaultConfig = TracX.ParameterConfiguration();
    
    %select editableprops
    tempMethods = methods(this.configuration.ParameterConfiguration);
    setFunctions = contains(tempMethods,'set');
    editableProps = strrep(tempMethods(setFunctions),'set','');
    %make lowercase first letter
    for i = 1:length(editableProps)
        editableProps{i}(1) = lower(editableProps{i}(1));
    end

    fprintf(scriptFile, "%% Parameter configuration\n");
    for i = 1:length(editableProps)
        if defaultConfig.(editableProps{i}) ~= this.configuration.ParameterConfiguration.(editableProps{i})
            setfunction = editableProps{i};
            setfunction(1) = upper(setfunction(1));
            setfunction = sprintf('set%s',setfunction);
            fprintf(scriptFile, "Tracker.configuration.ParameterConfiguration.%s(%.4f);\n", ...
                setfunction,this.configuration.ParameterConfiguration.(editableProps{i}) );
        end
    end
    fprintf(scriptFile, "\n");
    
    fprintf(scriptFile, "%%%% Run Tracker\n");
    fprintf(scriptFile, "Tracker.runTracker();\n");
    
    fclose(scriptFile); 
    this.utils.printToConsole(sprintf("Generated and saved script as %s", file));
    this.utils.printToConsole(sprintf("%s",fullfile(path,file)));
end
end