%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = exportFinalTrackingMasks(this)
% EXPORTFINALTRACKINGMASKS Exports tracked masks for cells that divide
% asymmetrically after lineage reconstruction to merge the individual buds
% with their respective mother mask prior cell division.
%
% Args:
%    this (:obj:`obj`):                     :class:`Tracker` object
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                           Writes mask files to disk
%
% :Authors:
%    Andreas P. Cuny - initial implementation

mergeTracks = this.lineage.getTracks2Merge();

% Export tracking masks
% this.imageProcessing.mergeBudPriorDivision()

% Convert mat masks to labeled tiff masks
this.imageProcessing.generateMergedMaskForReSegmentation(mergeTracks)

% Complement missing frames where no merge has been applied 
% @todo check where needed & if MaskSegmenter can deal with empty masks!

end