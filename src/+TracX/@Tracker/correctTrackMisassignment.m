%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = correctTrackMisassignment(this, frameArray, oldTrackIndexArray, ...
    newTrackIndexArray)
% CORRECTTRACKMISASSIGNMENT Allows to manually correct a cell to track
% assignment on specific frames. A given track index will be replaced with 
% a new track index. The arrays need to have the same size. 
%
% Args:
%    this (:obj:`obj`):                      :class:`Tracker` instance.
%    frameArray (:obj:`array`):              Array of image frames.
%    oldTrackIndexArray (:obj:`array`):      Array of old track indices to
%                                            be replaced.
%    newTrackIndexArray (:obj:`array`):      Array of new track indices to
%                                            relpace the old ones with.
%
% Returns
% -------
%     void: (:obj:`-`)          
%                                            Acts on :class:`Tracker` instance
%                                            directly
%
% :Authors:
%    Andreas P. Cuny - initial implementation

this.utils.printToConsole('INFO: Start manual track assignment correction.')

% Check input size
if ~isequal(size(frameArray), size(oldTrackIndexArray), size(newTrackIndexArray))
    this.utils.printToConsole('ERROR: Arrays must have same size')
    return
end

% Relabel track indices frame by frame.
for k = 1:numel(unique(frameArray))
    
    tmpOldTrack = oldTrackIndexArray(frameArray == frameArray(k));
    tmpNewTrack = newTrackIndexArray(frameArray == frameArray(k));
    uuid = this.data.getFieldArrayForTrackIndexForFrame('uuid', ...
        frameArray(k), tmpOldTrack);
    frac = this.data.getFieldArrayForTrackIndexForFrame(...
        'track_assignment_fraction', frameArray(k), tmpOldTrack);

    % Relabel track
    this.data.setFieldnameValuesForTracksOnFrame('track_index', ...
                frameArray(k), tmpOldTrack, tmpNewTrack)
    % Relabel assignment fraction to remove entries from pending edits        
    this.data.setFieldnameValuesForTracksOnFrame('track_assignment_fraction', ...
                frameArray(k), tmpOldTrack, zeros(size(tmpOldTrack)))
            
    % Save original data to temp file for possible revert / reproducibility
    data = [uuid, frameArray(k), tmpOldTrack, tmpNewTrack, frac];
    T = array2table(data,...
    'VariableNames',{'uuid','cell_frame','track_index', 'track_index_mod', ...
    'track_assignment_fraction'});
    path = fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, '.manualTrackEdits.csv');
    if ~exist(path, 'file')
        % write data
        writetable(T, path)
    else
        % load data
       TSaved = readtable(path);
        % Check if we have an enrty if so update otherwise append
        if any(TSaved.uuid == T.uuid)
            TSaved.track_index_mod(TSaved.uuid == T.uuid) = T.track_index_mod;
        else
            TSaved = [TSaved; T];
        end
        writetable(TSaved, path)
    end

end

this.utils.printToConsole('INFO: Done with manual track assignment correction.')

end