%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = saveTrackerProjectControlImages( this, varargin )
% SAVETRACKERPROJECTCONTROLIMAGES Saves control images for the
% current experiment for all tracked image frames.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    varargin (:obj:`str varchar`):
%
%        * **isParallel** (:obj:`bool`): Flag if should be run in parallel. Defaults to false.
%        * **maxWorkers** (:obj:`int`): Number of workers to us when running in parallel. Defaults to 4. Limited by CPU.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves images to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultIsParallel = false;
defaultMaxWorkers = this.utils.getMaxNumCompThreads;
validMaxWorkers = {'>=', 1, '<=', this.utils.getMaxNumCompThreads};

p = inputParser;
p.addRequired('this', @isobject);
p.addOptional('isParallel', defaultIsParallel,  @(x)validateattributes(x, ...
    {'logical'}, {'scalar'}));
p.addOptional('maxWorkers', defaultMaxWorkers, @(x)validateattributes(x, ...
    {'double'}, validMaxWorkers, 'Rankings'));

if ~isempty(find(strcmp(varargin,'maxWorkers')))
	loc = find(strcmp(varargin,'maxWorkers'));
	if varargin{loc+1} > defaultMaxWorkers
		varargin{loc+1} = defaultMaxWorkers;
	end
end
	
parse(p, this, varargin{:})

this.utils.printToConsole('SAVE: Start writing control images.')

if p.Results.isParallel == true
    
    % Start parpool with max cores if speficied
    % if non default use maxCores
    if isempty(gcp('nocreate'))
        pObj  = parpool('local', p.Results.maxWorkers);
    else
        pObj = gcp('nocreate');
    end
    
    pBar = this.utils.progressBar(this.configuration.ProjectConfiguration. ...
        trackerEndFrame-this.configuration.ProjectConfiguration. ...
        trackerStartFrame+1, ...
        'IsParallel', true, ...
        'WorkerDirectory', pwd, ...
        'Title', 'writing control images:' ...
        );
    
    pBar.setup([], [], []);
    parfor frameNumber = this.configuration.ProjectConfiguration. ...
            trackerStartFrame : this.configuration.ProjectConfiguration. ...
            trackerEndFrame
        
        this.saveTrackerMaskFrame(frameNumber)
        this.saveTrackerControlImageFrame(frameNumber)
        this.saveTrackerControlMaskImageFrame(frameNumber)
        TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
    end
    pBar.release();
    this.utils.printToConsole('SAVE: Done writing control images.')
    % delete(pObj);
else
    pBar = this.utils.progressBar(this.configuration.ProjectConfiguration. ...
        trackerEndFrame-this.configuration.ProjectConfiguration. ...
        trackerStartFrame+1, 'Title', 'writing control images:');
    for frameNumber = this.configuration.ProjectConfiguration. ...
            trackerStartFrame : this.configuration.ProjectConfiguration. ...
        trackerEndFrame
        
        this.saveTrackerMaskFrame(frameNumber)
        this.saveTrackerControlImageFrame(frameNumber)
        this.saveTrackerControlMaskImageFrame(frameNumber)       
        pBar.step([], [], []);
    end
    pBar.release();
    this.utils.printToConsole('SAVE: Done writing control images.')
end
end