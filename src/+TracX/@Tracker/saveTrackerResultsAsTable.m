%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function saveTrackerResultsAsTable(this, varargin)
% SAVETRACKERRESULTSASTABLE Save the tracking results along with the
% segmentation data in one table to a file.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    varargin (:obj:`str varchar`):
%
%        * **resultFilePath** (:obj:`str`): Path to where to save the results. Defaults to :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
%        * **resultFileName** (:obj:`str`): Results file name. Defaults to ProjectName_TrackingResults_Timestamp
%        * **fileType** (:obj:`str`): File type. Defaults to txt. Valid options are 'txt', 'csv', 'mat'.
%        * **dataDelimiter** (:obj:`str`): Data delimiter. Defaults to tab. Valid options are ',', 'comma', ' ', 'space', '\t',  'tab', ';', 'semi', '|', 'bar'.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves data as table to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultResultFilePath = this.configuration.ProjectConfiguration. ...
    segmentationResultDir;
defaultResultFileName = sprintf('%s_TrackingResults_%s', ...
    datetime('now','Format','yMdHHmmss'), ...
    this.configuration.ProjectConfiguration.projectName);
defaultFileType = 'txt';
defaultDelimiter = '\t';
validFileTypes = {'txt', 'csv', 'mat'};
validDelimiter = {',', 'comma', ' ', 'space', '\t', ...
    'tab', ';', 'semi', '|', 'bar'};
p = inputParser;
addOptional(p,'resultFilePath', defaultResultFilePath, @isfolder);
addOptional(p,'resultFileName', defaultResultFileName, @ischar);
addOptional(p,'fileType',defaultFileType, ...
    @(x) any(validatestring(x,validFileTypes)));
addOptional(p,'dataDelimiter',defaultDelimiter, ...
    @(x) any(validatestring(x,validDelimiter)));
parse(p, varargin{:});

this.data.joinTrackerDataAsTable()

this.exportTrackingResults(this.data.joinedTrackerDataTable, ...
    p.Results.resultFilePath, p.Results.resultFileName, varargin{:})

end
