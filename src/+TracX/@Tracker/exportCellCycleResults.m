%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function exportCellCycleResults(this, resultFilePath, resultFileName, ...
    varargin)
% EXPORTCELLCYCLERESULTS Exports the cell cycle results to disk as
% text, csv or mat file format delimited by your choice. Default
% is as text file and tab delimited.
%
% Args:
%    this (:obj:`obj`):                     :class:`Tracker` object
%    resultFilePath (:obj:`str`):           Path to where to save the results. Defaults to :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
%    resultFileName (:obj:`str`):           Results file name. Defaults to ProjectName_CellCycleResults_Timestamp
%    varargin (:obj:`str varchar`):
%
%        * **fileType** (:obj:`str`):       File type. Defaults to txt. Valid options are 'txt', 'csv', 'mat'.
%        * **dataDelimiter** (:obj:`str`):  Data delimiter. Defaults to tab. Valid options are ',', 'comma', ' ', 'space', '\t',  'tab', ';', 'semi', '|', 'bar'.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                           Writes text file to disk
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
defaultFileType = 'txt';
defaultDelimiter = '\t';
validFileTypes = {'txt', 'csv', 'mat'};
validDelimiter = {',', 'comma', ' ', 'space', '\t', ...
    'tab', ';', 'semi', '|', 'bar'};

addRequired(p,'resultFilePath',@isfolder);
addRequired(p,'resultFileName',@ischar);
addOptional(p,'fileType',defaultFileType, ...
    @(x) any(validatestring(x,validFileTypes)));
addOptional(p,'dataDelimiter',defaultDelimiter, ...
    @(x) any(validatestring(x,validDelimiter)));
parse(p, resultFilePath, resultFileName, varargin{:});

this.utils.printToConsole('SAVE: CellCycle results ...')

this.io.writeTrackerDataToDisk(this.lineage.cellCyclePhaseTable, ...
    resultFilePath, resultFileName, 'fileType', p.Results.fileType, ...
    'dataDelimiter', p.Results.dataDelimiter)

this.utils.printToConsole(sprintf('SAVE: %s.%s', fullfile(resultFilePath, resultFileName), ...
    p.Results.fileType))
this.utils.printToConsole('SAVE: CellCycle results done!')

end
