%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = saveTrackerControlMaskImageFrame( this, frameNumber, varargin)
% SAVETRACKERCONTROLMASKIMAGEFRAME Saves a tracker control
% image for a particular frame where the labeled tracker mask are 
% overlayed onto the raw image. Optionaly the label color can be set 
% using a cmap.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
%    varargin (:obj:`str varchar`):
%
%        * **useTrackIndices** (:obj:`bool`): If track indices should be used. Defaults to true. Othewise cell indices are used.
%        * **customCMap** (:obj:`array` kx3): Use custom colormat to color the cell segmentation masks. Otherwise auto generates one.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves masks to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

labeledTrackerMask = this.imageProcessing.generateTrackerControlMaskImageFrame( ...
    frameNumber, varargin{:} );

% Write to disk
if ndims(labeledTrackerMask) == 4
    this.io.writeToTIF(labeledTrackerMask, fullfile(this.configuration. ...
        ProjectConfiguration.trackerResultsDir, sprintf('%s_%s_%s_frame%0.3d.tif', ...
        this.configuration.ProjectConfiguration.trackerPrefix, ...
        this.configuration.ProjectConfiguration.controlPrefix, ...
        this.configuration.ProjectConfiguration.maskPrefix, frameNumber)))
elseif ndims(labeledTrackerMask) == 3
    this.io.writeToPNG(labeledTrackerMask, fullfile(this.configuration. ...
        ProjectConfiguration.trackerResultsDir, sprintf('%s_%s_%s_frame%0.3d.png', ...
        this.configuration.ProjectConfiguration.trackerPrefix, ...
        this.configuration.ProjectConfiguration.controlPrefix, ...
        this.configuration.ProjectConfiguration.maskPrefix, frameNumber)))
else
    this.utils.printToConsole(sprintf(['ERROR: Image dimensions are not valid.', ...
    ' Could not write %s_%s_%s_frame%0.3d'], this.configuration.ProjectConfiguration.trackerPrefix, ...
        this.configuration.ProjectConfiguration.controlPrefix, ...
        this.configuration.ProjectConfiguration.maskPrefix, frameNumber));
end
end