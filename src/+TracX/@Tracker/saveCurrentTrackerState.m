%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = saveCurrentTrackerState( this, varargin )
% SAVECURRENTTRACKERSTATE Saves the current state of the tracker to
% disk.
%
% Args:
%    this (:obj:`obj`):                     :class:`Tracker` object
%    varargin (:obj:`str varchar`):
%
%        * **resultFilePath** (:obj:`str`): Path to where to save the tracker stat. Defaults to :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
%        * **resultFileName** (:obj:`str`): Results file name. Defaults to Timestamp_TracX_State_Project_ProjectName
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                           Saves the tracker object stat
%                                           to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultDirectory = [];
defaultProjectName = []; 
p = inputParser;
addRequired(p, 'this');
p.addParameter('resultFileName', defaultProjectName, @ischar);
p.addParameter('resultFilePath', defaultDirectory, @isfolder);
parse(p, this, varargin{:})

if isempty(p.Results.resultFilePath) && isempty(this.configuration.ProjectConfiguration. ...
    segmentationResultDir)
    this.utils.printToConsole(['ERROR: Tracker please create or load a ', ...
        'tracking project first or indicate a valid directory and project name'])
    return
elseif ~isempty(p.Results.resultFilePath)
    dir = p.Results.resultFilePath;
    name = p.Results.resultFileName;
else
    dir = this.configuration.ProjectConfiguration. ...
    segmentationResultDir;
    name = this.configuration.ProjectConfiguration.projectName;
end

nameToSave = sprintf('%s_TracX_State_Project_%s.mat', datetime('now','Format','yMdHHmmss'), ...
    name);

this.utils.printToConsole('SAVE: Tracker state')

Tracker = this;
save(fullfile(dir, nameToSave), 'Tracker', '-v7.3')

this.utils.printToConsole(sprintf('SAVE: %s', fullfile(dir, ...
    nameToSave)))
this.utils.printToConsole('SAVE: Tracker state done!')


end
