%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
classdef Tracker < handle
    % TRACKER Tracker main TracX object
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
        
        configuration % :class:`+TracX.@TrackerConfiguration` instance with the tracking parameters and project metadata.
        data % :class:`+TracX.@TrackData` instance which stores all segmentation, quantification and lineage data.
        io % :class:`+TracX.@IO` instance implementing read/write methods.
        utils % :class:`+TracX.@Utils` instance implementing utility methods.
        imageProcessing % :class:`+TracX.@ImageProcessing` instance implementing image processing methods.
        imageVisualization % :class:`+TracX.@ImageVisualization` instance implementing image visualization methods.
        fingerprint % :class:`+TracX.@Fingerprint` instance implementing cell fingerprinting methods.
        lap % :class:`+TracX.@LAP` instance implementing linear assignment methods.
        motion % :class:`+TracX.@Motion` instance implementing cell motion methods.
        lineage % :class:`+TracX.@Lineage` instance implementing lineage reconstruction methods
        evaluation % :class:`+TracX.@TrackerEvaluation` instance implementing tracker performance evaluation methods
        
    end
    
    properties (GetAccess=public, SetAccess=private)
        isFullCosts = 1; % Flag if full costs should be used during tracking (1) or if a minimalized tracker based on dCRF should be run. Defaults to true.
        isCorrectMotion = 1; % Flag, if motion should be corrected for (1). Defaults to true.
        isAllInstalled = 0; % Flag, if all TracX dependencies and toolboxes are installed. Defaults to false.
        isReady = 0; % Flag, if TracX is ready to be used. Defaults to false.
        isFFPEval = 0; %Flag, to evaluate FFP for manuscript. Defaults to false
        
        version; % Current TracX version
        
        % numberOfTracks %  Highest used track index number
        % nonSegmentedTracks % Non segmented tracks
        
    end
    
    
    methods
        
        % Constructor
        function obj = Tracker()
            % TRACKER Tracker Constructs an empty Tracker object
            % object to save segmentation data as well as tracking results.
            % for a TracX project.
            %
            % Returns
            % -------
            %     obj: (:obj:`obj`)
            %                                   :class:`Tracker`  instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj = obj.initializeTrackerCore();
        end
        
        function createNewTrackingProject(this, projectName, imageDir, ...
                segmentationResultDir, fileIdentifierFingerprintImages, ...
                fileIdentifierWellPositionFingerprint, ...
                fileIdentifierCellLineage, imageCropCoordinateArray, ...
                nrOfFramesToImport, cellDivisionType)
            % CREATENEWTRACKINGPROJECT Creates a new TracX project.
            % Sets file paths, and load segmentation data.
            %
            % Args:
            %    this (:obj:`obj`):                          :class:`Tracker` instance
            %    projectName (:obj:`str`):                   Name of the tracking
            %                                                project.
            %    imageDir (:obj:`str`):                      Image directory to
            %                                                be used in the
            %                                                current project.
            %    segmentationResultDir (:obj:`str`)::        Directory containing
            %                                                the segmentation
            %                                                results for the
            %                                                current project
            %    fileIdentifierFingerprintImages (:obj:`str`): File name identifier
            %                                                for raw Brightfield
            %                                                images.
            %    fileIdentifierWellPositionFingerprint (:obj:`str`): File name
            %                                                identifier
            %                                                specifing the well
            %                                                position (of a multi
            %                                                well experiment).
            %    fileIdentifierCellLineage (:obj:`str`)::    File name identifier
            %                                                of the fluorescent
            %                                                channel images to
            %                                                be used to reconstruct
            %                                                the cell lineage
            %                                                (i.e. bud neck marker)
            %    imageCropCoordinateArray (:obj:`array`, 1x4): Array with crop coordinates
            %                                                used during
            %                                                segmentation.
            %    nrOfFramesToImport (:obj:`int`):            Number of frames
            %                                                to track.
            %    cellDivisionType (:obj:`str`):              Cell division type
            %
            % Returns
            % -------
            %     this: (:obj:`obj`)
            %                                              :class:`Tracker` instance
            %                                              with new project
            %                                              initialized.
            
            % Check input arguments and parse them first.
            validDivisionType = {'sym', 'symmetrical', 'Symmetrical', ...
                'asym', 'asymmetrical', 'Asymmetrical', '1', '0', '-1'};
            
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('this', @isobject);
            p.addRequired('projectName', @ischar)
            p.addRequired('imageDir', @isfolder)
            p.addRequired('segmentationResultDir', @isfolder)
            p.addRequired('fileIdentifierFingerprintImages', ...
                @(x)validateattributes(x, {'char'}, {'nonempty'}))
            p.addRequired('fileIdentifierWellPositionFingerprint', ...
                @(x) ischar(x) || isempty(x) )
            p.addRequired('fileIdentifierCellLineage', ...
                @(x) ischar(x) || isempty(x) || iscell(x))
            p.addRequired('imageCropCoordinateArray', ...
                @(x) all(size(x) == [1,4]) || isempty(x))
            p.addRequired('nrOfFramesToImport', ...
                @(x) isnumeric(x) && x > 0)
            p.addRequired('cellDivisionType', ...
                @(x) any(validatestring(string(x), validDivisionType)))
            
            p.parse(this, projectName, imageDir, ...
                segmentationResultDir, fileIdentifierFingerprintImages, ...
                fileIdentifierWellPositionFingerprint, ...
                fileIdentifierCellLineage, imageCropCoordinateArray, ...
                nrOfFramesToImport, cellDivisionType);
            
            if ~this.isAllInstalled
                this.isAllInstalled = this.utils.checkToolboxStatus();
                if ~this.isAllInstalled
                    this.utils.printToConsole('ERROR: TracX not ready! Aborting now.')
                    return
                else
                end
            end
            
            try
                this.utils.printToConsole('NEW: Creating new TracX project.')
                
                this.configuration.ProjectConfiguration. ...
                    createProjectConfiguration(p.Results.projectName, ...
                    p.Results.imageDir,  p.Results.segmentationResultDir, ...
                    p.Results.fileIdentifierFingerprintImages, ...
                    p.Results.fileIdentifierWellPositionFingerprint, ...
                    p.Results.fileIdentifierCellLineage, ...
                    p.Results.imageCropCoordinateArray, ...
                    p.Results.nrOfFramesToImport, ...
                    p.Results.cellDivisionType);
                
                % Check configuration first.
                this.configuration.ProjectConfiguration. ...
                    checkProjectConfiguration()
                
                % Clear potential previous data first.
                this.data.clearAllData();
                
                this.importData()
                
                if range(this.data.SegmentationData.cell_center_z) > 0
                    this.utils.printToConsole("3D data detected")
                    this.configuration.ParameterConfiguration.setData3D(1);
                    
                    load(fullfile(this.configuration. ...
                        ProjectConfiguration.segmentationResultDir,["cells_1.mat"]))
                    tempPixel2ZRatio = min(diff(unique(...
                        data.alphashape{find(~cellfun(@isempty, ...
                        data.alphashape),1)}.Points(:,3))));
                    
                    this.utils.printToConsole(["Automatically detected voxel dimensions: 1x1x" ...
                        + tempPixel2ZRatio + " pixels"])
                    this.configuration.ParameterConfiguration.setPixelsPerZPlaneInterval(tempPixel2ZRatio);
                    if this.configuration.ParameterConfiguration. ...
                            fingerprintMaxConsideredFrequencies > ...
                            this.configuration.ProjectConfiguration.imageDepth
                        this.utils.printToConsole("Warning: ")
                        this.utils.printToConsole("Tracker will be using 2D fingerprint of Z planes closest to cell centers")
                        this.configuration.ParameterConfiguration.setActive3DFingerprint(0);
                    else
                        this.utils.printToConsole("Tracker will be using 3D fingerprint");
                        this.configuration.ParameterConfiguration.setActive3DFingerprint(1);
                    end
                else
                    this.utils.printToConsole("INFO: 2D data detected")
                end
                
                % Set status
                this.isReady = 1;
                
            catch ME
                this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                for i =1:numel(ME.stack)
                    this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                    this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                    this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                end
                return
            end
        end
        
        function saveTrackingProject(this, varargin)
            % SAVETRACKINGPROJECT Save a TracX project. Includes the
            % meta data and parameters used during tracking.
            %
            % Args:
            %    this (:obj:`obj`):               :class:`Tracker` instance
            %    varargin (:obj:`str varchar`):
            %
            %        * **filePath** (:obj:`str`): File path for saving the TracX project.
            %        * **fileName** (:obj:`str`): Filename for the Tracking project.
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                     Saves project to disk
            %                                     directly.
            
            defaultResultFilePath = this.configuration.ProjectConfiguration. ...
                segmentationResultDir;
            defaultResultFileName = sprintf('%s_TrackerConfiguration_%s', ...
                datetime('now','Format','yMdHHmmss'), ...
                this.configuration.ProjectConfiguration.projectName);
            
            p = inputParser;
            addOptional(p,'resultFilePath', defaultResultFilePath, @isfolder);
            addOptional(p,'resultFileName', defaultResultFileName, @ischar);
            
            parse(p, varargin{:});
            
            try
                this.utils.printToConsole('SAVE: TracX Project configuration ...')
                % Make sure that the file ending is correct
                [~, name, ext] = fileparts(p.Results.resultFileName);
                if isempty(ext) == 1
                    ext = '.xml';
                elseif strcmp(ext, '.xml') == 0
                    ext = '.xml';
                end
                this.configuration.exportTrackerConfiguration(fullfile(...
                    p.Results.resultFilePath, [name, ext]))
                this.utils.printToConsole(sprintf('SAVE: %s', fullfile(...
                    p.Results.resultFilePath, [name, ext])))
                this.utils.printToConsole('SAVE: TracX Project configuration done!')
            catch ME
                this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                for i =1:numel(ME.stack)
                    this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                    this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                    this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                end
                return
            end
        end
        
        function loadTrackingProject(this, filePath, fileName)
            % LOADTRACKINGPROJECT load a TracX project. Load from xml
            % file the meta data and parameters used during tracking.
            %
            % Args:
            %    this (:obj:`obj`):               :class:`Tracker` instance
            %    filePath (:obj:`str`):           File path for loading the TracX project
            %    fileName (:obj:`str`):           Filename of Tracking project to load.
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                     Loads the project.
            
            if ~this.isAllInstalled
                this.isAllInstalled = this.utils.checkToolboxStatus();
                if ~this.isAllInstalled
                    this.utils.printToConsole('ERROR: TracX not ready! Aborting now.')
                    return
                else
                end
            end
            
            try
                this.utils.printToConsole('LOAD: TracX Project')
                [~, name, ext] = fileparts(fileName);
                if isempty(ext) == 1
                    ext = '.xml';
                elseif strcmp(ext, '.xml') == 0
                    ext = '.xml';
                end
                this.configuration.importTrackerConfiguration(fullfile(filePath, ...
                    [name, ext]));
                % Check configuration first.
                this.configuration.ProjectConfiguration. ...
                    checkProjectConfiguration()
                
                % Clear potential previous data first.
                this.data.clearAllData();
                
                % Import data directly afterwards
                this.importData()
                
                % Set status
                this.isReady = 1;
                
            catch ME
                this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                for i =1:numel(ME.stack)
                    this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                    this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                    this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                end
                return
            end
        end
        
        % PREPAREDATAFROMSEGMENTATIONMASK Allows tracking with
        % segmentation results from any software given a un- or labeled
        % segmentation mask per image frame. The mask is then converted
        % into the CellX format which is used internaly for structuring the
        % data during tracking.
        prepareDataFromSegmentationMask(this, imagePath, ...
            segmentationPath, segmentationFileNameRegex, ...
            imageFileNameRegex, varargin)
        
        function importData(this)
            % IMPORTDATA Imports segmentation and eventual
            % quantification data using the settigs set in the
            % configuration.ProjectConfiguration object.
            %
            % Args:
            %    this (:obj:`obj`):               :class:`Tracker` instance
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                     Imports data into the
            %                                     :class:`Tracker` instance
            
            try
                this.utils.printToConsole('LOAD: Start loading data.')
                this.data.importData(...
                    this.configuration.ProjectConfiguration. ...
                    segmentationResultDir, this.configuration. ...
                    ProjectConfiguration.segmentationResultFileArray, ...
                    this.configuration.ProjectConfiguration. ...
                    nFramesToImport);
                
                if isempty(this.data.SegmentationData.cell_center_z)
                    this.data.SegmentationData.cell_center_z =  ...
                        zeros(length(this.data.SegmentationData.cell_center_x),1);
                end
                
                % Set the average cell diameter. We use here the median on
                % purpose to automatically define this parameter
                this.configuration.ParameterConfiguration.setMeanCellDiameter(...
                    median(this.data.getFieldArray('cell_majoraxis')));
                
                % Set the neighbourhood search radius as half the 2 times
                % the average average cell diamter rounded to the next 10
                % using the end frame of the project.
                %   radius = ceil(round(median(this.data.SegmentationData. ...
                %    cell_majoraxis)*2)/2/10)*10
                x = this.data.getFieldArrayForFrame('cell_center_x', ...
                    this.configuration.ProjectConfiguration.trackerEndFrame);
                y = this.data.getFieldArrayForFrame('cell_center_y', ...
                    this.configuration.ProjectConfiguration.trackerEndFrame);
                m = squareform(pdist([x, y]));
                m(m == 0) = nan;
                minVal = min(m);
                [~, edges] = histcounts(minVal, 32);
                edges(1) = [];
                selVal = minVal(minVal <= edges(sum(edges < round(...
                    median(minVal)))+1));
                this.configuration.ParameterConfiguration. ...
                    setNeighbourhoodSearchRadius(ceil(round(...
                    mean(selVal)*1.6)/10)*10);
                
                % Initialize the cell close neighbours
                % @todo Note if this param is changed later it wont be
                % considered and re evaluated
                this.data.initializeCellCloseNeighbours(...
                    this.configuration.ParameterConfiguration. ...
                    neighbourhoodSearchRadius);
                this.utils.printToConsole('LOAD: Done loading data.')
                
                % Set the maxExpectedMovement parameter as 2.5 times the
                % average cell diameter
                this.configuration.ParameterConfiguration. ...
                    setMaxExpectedMovement(2.5 * ...
                    this.configuration.ParameterConfiguration.meanCellDiameter)
                
            catch ME
                this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                for i =1:numel(ME.stack)
                    this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                    this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                    this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                end
                return
            end
            
        end
        
        function runTracker(this, varargin)
            % RUNTRACKER Runs the tracker.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance
            %    varargin (:obj:`str varchar`):
            %
            %        * **flag** (:obj:`array`, 1x2): Flag if full cost (tracker) should be used (1) or not (0) as well if motion should be corrected (1) or not (0). Defaults to [1,1].
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                        :class:`Tracker` instance
            if ~this.isAllInstalled
                this.isAllInstalled = this.utils.checkToolboxStatus();
                if ~this.isAllInstalled
                    this.utils.printToConsole('ERROR: TracX not ready! Aborting now.')
                    return
                else
                end
            end
            
            if ~this.isReady
                this.utils.printToConsole('ERROR: Please check that a new or existing TracX project is properly initialized/loaded. ')
                this.utils.printToConsole('ERROR: TracX not ready! Aborting now.')
                return
            end
            
            if ~isempty(varargin) && numel(varargin{1}) == 2
                this.isFullCosts = varargin{1}(1);
                this.isCorrectMotion = varargin{1}(2);
            end

            if this.isFFPEval
                evalFFP = struct();
                evalFFP.dCRFM = [];
            end
            
            % Reset tracking data from previous runs
            this.data.clearTrackerData(this.configuration.ParameterConfiguration. ...
                neighbourhoodSearchRadius);
            this.data.VeryOld = TracX.TemporaryTrackingData();
            this.data.VeryOldEval = TracX.NonAssignedTracks();
            
            % Check first if all parameters are set properly
            this.revertSegmentationImageCrop()
            
            startF = this.configuration.ProjectConfiguration.trackerStartFrame;
            endF = this.configuration.ProjectConfiguration.trackerEndFrame;
            
            this.configuration.ParameterConfiguration. ...
                checkParameterConfiguration()
            this.fingerprint.debugLevel = this.configuration. ...
                ParameterConfiguration.debugLevel;
            
            uniqueFrames = unique(this.data.SegmentationData.cell_frame( ...
                this.data.SegmentationData.cell_area > 0));
            uniqueFramesInRange = uniqueFrames((uniqueFrames >= startF & uniqueFrames <= endF));
            
            NuniqueFinR = length(uniqueFramesInRange);
            firstFrame = uniqueFramesInRange(1);
            lastFrame = uniqueFramesInRange(end);
            
            this.configuration.ProjectConfiguration.setTrackerStartFrame(firstFrame);
            this.configuration.ProjectConfiguration.setTrackerEndFrame(lastFrame);
            %  @todo: Run per each position in config file in loop or par
            %  for loop as calculations are independent from each other
            %  ImportData and initializeneighbours as well as write out
            %  tracking results has to be in that loop
            disp(this.configuration.ParameterConfiguration)
            this.utils.printToConsole('TRACKING: Started ...')
            this.utils.printToConsole(["Selected non-empty frames " ...
                + firstFrame + " - " + lastFrame + " for tracking:"])
            
            timeTotalTracking = tic;
            
            % Initialization of tracking data structs for first frame
            if this.configuration.ParameterConfiguration.data3D
                [ret_fq] = this.fingerprint.computeFingerprintForFrame3D(...
                    firstFrame, firstFrame, ...
                    this.configuration.ParameterConfiguration. ...
                    fingerprintHalfWindowSideLength, ...
                    this.configuration.ParameterConfiguration.fingerprintResizeFactor, ...
                    this.configuration.ParameterConfiguration. ...
                    fingerprintMaxConsideredFrequencies  , ...
                    this.configuration.ProjectConfiguration.imageDir, ...
                    this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
                    this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
                    this.data.SegmentationData.cell_frame, ...
                    this.data.SegmentationData.cell_center_x, ...
                    this.data.SegmentationData.cell_center_y, ...
                    this.data.SegmentationData.cell_center_z, ...
                    this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval,...
                    this.configuration.ParameterConfiguration.active3DFingerprint);
            else
                [ret_fq] = this.fingerprint.computeFingerprintForFrame(...
                    firstFrame, firstFrame, ...
                    this.configuration.ParameterConfiguration. ...
                    fingerprintHalfWindowSideLength, ...
                    this.configuration.ParameterConfiguration.fingerprintResizeFactor, ...
                    this.configuration.ParameterConfiguration. ...
                    fingerprintMaxConsideredFrequencies  , ...
                    this.configuration.ProjectConfiguration.imageDir, ...
                    this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
                    this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
                    this.data.SegmentationData.cell_frame, ...
                    this.data.SegmentationData.cell_center_x, ...
                    this.data.SegmentationData.cell_center_y);
            end
            % Save Fingerprints
            this.data.setFieldArrayForFrame('track_fingerprint_real', ...
                firstFrame, ...
                ret_fq);
            % Initialize OldFrame
            this.data.initializeOldFrame(firstFrame);
            %
            this.data.VeryOldEval.setMaxTrackFrameSkipping(...
                this.configuration.ParameterConfiguration. ...
                maxTrackFrameSkipping);
            
            ofl = length(this.data.getFieldArrayForFrame('cell_index', firstFrame));
            
            maxTrackIndex = max(this.data.getFieldArray('track_index'));
            
            if isnan(maxTrackIndex)
                maxTrackIndex = 0;
            end
            
            this.data.numberOfTracks = [maxTrackIndex+ofl];
            % Set start and length of tracking
            
            frameNumberIdx = 1;
            if frameNumberIdx == 0
                frameNumberIdx = 1;
            end
            
            pBarT = this.utils.progressBar(NuniqueFinR, ...
                'Title', 'TRACKING: Progress' ...
                );
            %% here, the tracking starts
            while frameNumberIdx <= NuniqueFinR
                frameNumber = uniqueFramesInRange(frameNumberIdx);
                
                if this.configuration.ParameterConfiguration.debugLevel > 1
                    tic
                end
                % Compute fingerprints (real freq) for current
                % image frame
                if this.configuration.ParameterConfiguration.data3D
                    
                    [ret_fq] = this.fingerprint.computeFingerprintForFrame3D(frameNumber, ...
                        frameNumber,  this.configuration.ParameterConfiguration.fingerprintHalfWindowSideLength, ...
                        this.configuration.ParameterConfiguration.fingerprintResizeFactor, ...
                        this.configuration.ParameterConfiguration.fingerprintMaxConsideredFrequencies  , ...
                        this.configuration.ProjectConfiguration.imageDir, ...
                        this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
                        this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
                        this.data.SegmentationData.cell_frame, ...
                        this.data.SegmentationData.cell_center_x, ...
                        this.data.SegmentationData.cell_center_y, ...
                        this.data.SegmentationData.cell_center_z, ...
                        this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval,...
                        this.configuration.ParameterConfiguration.active3DFingerprint);
                else
                    [ret_fq] = this.fingerprint.computeFingerprintForFrame(...
                        frameNumber, frameNumber, ...
                        this.configuration.ParameterConfiguration. ...
                        fingerprintHalfWindowSideLength, ...
                        this.configuration.ParameterConfiguration.fingerprintResizeFactor, ...
                        this.configuration.ParameterConfiguration. ...
                        fingerprintMaxConsideredFrequencies  , ...
                        this.configuration.ProjectConfiguration.imageDir, ...
                        this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
                        this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
                        this.data.SegmentationData.cell_frame, ...
                        this.data.SegmentationData.cell_center_x, ...
                        this.data.SegmentationData.cell_center_y);
                end
                
                % Save fingerprints for current image frame
                this.data.setFieldArrayForFrame('track_fingerprint_real', ...
                    frameNumber, ret_fq);
                
                % Initialize NewFrame
                this.data.initializeNewFrame(frameNumber);
                                
                % A costMatrix based on NewFrame and OldFrame and cost
                % penalty functions is created.
                if this.isFullCosts
                    [totalCost, positionCost, areaCost, rotationCost, ...
                        frameSkippingCost] = this.lap.computeAssignmentCostMatrix(...
                        this.data.OldFrame, this.data.NewFrame, ...
                        this.configuration.ParameterConfiguration.maxCellCenterDisplacement, ...
                        this.configuration.ParameterConfiguration.maxMajorAxisRotation, ...
                        this.configuration.ParameterConfiguration.individualFunctionPenalty, ...
                        this.configuration.ParameterConfiguration.maxCellSizeDecrease, ...
                        this.configuration.ParameterConfiguration.averageCellSizeGrowth, ...
                        this.configuration.ParameterConfiguration.maxCellSizeIncrease,...
                        this.configuration.ParameterConfiguration.usedFunctionsForCostMatrix, ...
                        this.configuration.ParameterConfiguration.meanCellDiameter, ...
                        this.configuration.ParameterConfiguration.maxTrackFrameSkipping, ...
                        0, ...
                        this.configuration.ParameterConfiguration.data3D);
                else

                    % Get the cell region fingerprints for frameNumber
                    crfO = this.data.OldFrame.track_fingerprint_real;
                    crfN = this.data.NewFrame.track_fingerprint_real;
                    % Get dCRF matrix
                    [dCRFM] = this.fingerprint.computeFingerprintDistance(...
                        crfO, crfN);
                    evalFFP(frameNumber).dCRFM = dCRFM;
                    
                    % FFP based tracker; We only use CRF distance for tracking.
                    [totalCost, positionCost, areaCost, rotationCost, ...
                        frameSkippingCost] = this.lap.computeFFPCostMatrix(...
                        this.data.OldFrame, this.data.NewFrame, ...
                        this.configuration.ParameterConfiguration.individualFunctionPenalty, ...
                        this.configuration.ParameterConfiguration.neighbourhoodSearchRadius, ...
                        dCRFM);
                end
                
                % Compute linear assignment
                [ assignmentRowIdx, assignment, ~, ...
                    totalCostAssigned, positionCostAssigned, areaCostAssigned, ...
                    rotationCostAssigned, frameSkippingCostAssigned] = ...
                    this.lap.solveLAP( totalCost, positionCost, ...
                    areaCost, rotationCost, frameSkippingCost );
                
                if this.configuration.ParameterConfiguration.debugLevel > 1
                    this.utils.printToConsole(sprintf(['Elapsed time is %4.4f', ...
                        ' seconds for solving the linear assignment on frame %d.'], ...
                        toc, frameNumber))
                end
                
                tic
                lengthNewFrame = length(this.data.NewFrame.cell_area);
                assignedRow = ismember(assignment, 1:lengthNewFrame);
                assignmentIdx = find(assignedRow == 1);
                
                % Keep old tracks which could not be assigned in this frame
                % (not segmented or cost was too high) to be able to
                % potentially assign them in the next frame
                this.data.InitializeVeryOld( assignmentRowIdx, ...
                    this.configuration.ParameterConfiguration. ...
                    maxTrackFrameSkipping);
                
                % Displacement stored (but not used atm as we use the more
                % precise one of the filtered vector field)
                assignmentValue = assignment(assignmentIdx);
                difX = zeros(lengthNewFrame,1);
                difY = zeros(lengthNewFrame,1);
                difZ = zeros(lengthNewFrame,1);
                difX(assignmentValue) = (this.data.NewFrame.cell_center_x(...
                    assignmentValue) - this.data.OldFrame.cell_center_x(...
                    assignmentIdx))./this.data.OldFrame.track_age(assignmentIdx);
                difY(assignmentValue) = (this.data.NewFrame.cell_center_y(...
                    assignmentValue) - this.data.OldFrame.cell_center_y(...
                    assignmentIdx))./this.data.OldFrame.track_age(assignmentIdx);
                difZ(assignmentValue) = (this.data.NewFrame.cell_center_z(...
                    assignmentValue) - this.data.OldFrame.cell_center_z(...
                    assignmentIdx))./this.data.OldFrame.track_age(assignmentIdx);
                
                % Included Aaron's filtered vector calculation (unchanged)
                if this.configuration.ParameterConfiguration.radiusVectorFilterX ~= 0
                    filteredDifX = zeros(lengthNewFrame,1);
                    filteredDifY = zeros(lengthNewFrame,1);
                    filteredDifZ = zeros(lengthNewFrame,1);
                    [filteredDifX(assignmentValue), filteredDifY(...
                        assignmentValue), filteredDifZ(...
                        assignmentValue)] = this.motion.filterVectorField(...
                        this.data.NewFrame.cell_center_x(assignmentValue), ...
                        this.data.NewFrame.cell_center_y(assignmentValue), ...
                        this.data.NewFrame.cell_center_z(assignmentValue), ...
                        difX(assignmentValue),...
                        difY(assignmentValue),...
                        difZ(assignmentValue),...
                        this.configuration.ParameterConfiguration.radiusVectorFilterX, ...
                        this.configuration.ParameterConfiguration.radiusVectorFilterY, ...
                        this.configuration.ParameterConfiguration.radiusVectorFilterZ);
                else
                    filteredDifX = difX;
                    filteredDifY = difY;
                    filteredDifZ = difZ;
                end
                
                this.data.VeryOld.track_age = this.data.VeryOld.track_age + 1;
                
                newTrackIndex = zeros(lengthNewFrame,1);
                newTrackIndex(assignmentValue) = this.data.OldFrame.track_index(assignmentIdx);
                where = find(newTrackIndex == 0);
                add = length(where);
                newTrackIndex(where) = this.data.numberOfTracks + 1 : this.data.numberOfTracks + add;
                this.data.numberOfTracks = this.data.numberOfTracks + add + 1;
                
                % Put the assignment of the  new tracks into the NewFrame
                % object
                this.data.NewFrame.track_index = newTrackIndex;
                this.data.NewFrame.track_index_qc = zeros(numel(newTrackIndex),1);
                this.data.NewFrame.track_age = ones(numel(newTrackIndex),1);
                this.data.NewFrame.cell_dif_x = difX;
                this.data.NewFrame.cell_dif_y = difY;
                this.data.NewFrame.cell_dif_z = difZ;
                this.data.NewFrame.cell_filtered_dif_x = filteredDifX;
                this.data.NewFrame.cell_filtered_dif_y = filteredDifY;
                this.data.NewFrame.cell_filtered_dif_z = filteredDifZ;
                
                % Recompute neighbourhood for VeryOld cells in the NewFrame
                if ~isempty(this.data.VeryOld.cell_center_x)
                    coords = [ [this.data.NewFrame.cell_center_x; ...
                        this.data.VeryOld.cell_center_x, this.data.VeryOld.cell_center_z], [this.data.NewFrame.cell_center_y;...
                        this.data.VeryOld.cell_center_y, this.data.VeryOld.cell_center_z]];
                    neighbourMatrix = TracX.Functions.getCloseCellNeighbourIdx(coords(:,1), coords(:,2), ...
                        this.configuration.ParameterConfiguration.neighbourhoodSearchRadius, coords(:,3));
                    NNewFrame = size(this.data.NewFrame.cell_center_x,1);
                    this.data.VeryOld.cell_close_neighbour = neighbourMatrix(NNewFrame+1:end,1:NNewFrame);
                end
                
                [realFingerprintTrackDistance] = ...
                    this.fingerprint.computeAssignmentFingerprintDistanceArray(this.data.OldFrame, ...
                    this.data.NewFrame);
                                
                % Here we add new distances in wrong shape to old frame! We
                % need them to be in the shape and order of the old frame.
                this.data.OldFrame.track_fingerprint_real_distance = realFingerprintTrackDistance;
                
                [oldFrameLocIdx, emptyDataLocIdx] = getTrackIndexStorageIndicies(this);
                
                % Save 'track_fingerprint_real_distance'
                dataToSave = nan(this.data.getNumberOfRowsFromFrame(frameNumber),1);
                this.copyResultsToTrackerData(dataToSave, 'track_fingerprint_real_distance', ...
                    emptyDataLocIdx, oldFrameLocIdx, frameNumber)
                
                % Get the cell region fingerprints for frameNumber
                crfO = this.data.OldFrame.track_fingerprint_real;
                crfN = this.data.NewFrame.track_fingerprint_real;
                
                xO = this.data.OldFrame.cell_center_x;
                xOF = this.data.OldFrame.cell_filtered_dif_x;
                xN = this.data.NewFrame.cell_center_x;
                yO = this.data.OldFrame.cell_center_y;
                yOF = this.data.OldFrame.cell_filtered_dif_y;
                yN = this.data.NewFrame.cell_center_y;
                trO = this.data.OldFrame.track_index;
                trN = this.data.NewFrame.track_index;
                
                % dCRF for assigned tracks on frame frameNumber
                dCRFAsgmt = realFingerprintTrackDistance;
                                
                [joinedNeighbours, joinedNeighboursWMotion, ...
                    oldNeighbours, newNeighbours] = ...
                    this.data.getConsecutiveCloseCellNeighbourIdx(...
                    xO, xOF, yO, yOF, trO, xN, yN, trN, this.configuration. ...
                    ParameterConfiguration.neighbourhoodSearchRadius);
                
                [FFPa, FFP, NNFPa, NNFP, nNhbra, nNhbr] = this.fingerprint. ...
                    computeFingerprintAsgmtFrac(crfO, crfN, dCRFAsgmt, ...
                    joinedNeighboursWMotion);
                if frameNumber - 1 >= min(this.data.getFieldArray('cell_frame'))
                    this.data.setFieldArrayForFrame(...
                        'track_assignment_fraction', frameNumber - 1, FFPa)
                end
                
                if this.isFFPEval
                    %% Temporary test of FFPa
                    [FFPaJnhbrhd, ~, ~, ~, ~, ~] = this.fingerprint. ...
                        computeFingerprintAsgmtFrac(crfO, crfN, dCRFAsgmt, ...
                        joinedNeighbours);
                    [FFPaJnhbrhdMot, ~, ~, ~, ~, ~] = this.fingerprint. ...
                        computeFingerprintAsgmtFrac(crfO, crfN, dCRFAsgmt, ...
                        joinedNeighboursWMotion);
                    [FFPaOnhbrhd, ~, ~, ~, ~, ~] = this.fingerprint. ...
                        computeFingerprintAsgmtFrac(crfO, crfN, dCRFAsgmt, ...
                        oldNeighbours);
                    [FFPaNnhbrhd, ~, ~, ~, ~, ~] = this.fingerprint. ...
                        computeFingerprintAsgmtFrac(crfO, crfN, dCRFAsgmt, ...
                        newNeighbours);
                    
                    evalFFP.frame = [evalFFP.frame; repmat(frameNumber, numel(FFPaJnhbrhd), 1)];
                    evalFFP.FFPaJnhbrhd = [evalFFP.FFPaJnhbrhd; FFPaJnhbrhd];
                    evalFFP.FFPaJnhbrhdMot = [evalFFP.FFPaJnhbrhdMot; FFPaJnhbrhdMot];
                    evalFFP.FFPaOnhbrhd = [evalFFP.FFPaOnhbrhd; FFPaOnhbrhd];
                    evalFFP.FFPaNnhbrhd = [evalFFP.FFPaNnhbrhd; FFPaNnhbrhd];
                end
                
                
                % Apply fingerprint rejection
                tracksForReconsideration = this.data.getTrackAssignmentReconsiderationArray(...
                    this.data.OldFrame.track_index, ...
                    this.data.OldFrame.track_fingerprint_real_distance, ...
                    this.data.OldFrame.track_assignment_fraction, ...
                    this.configuration.ParameterConfiguration.fingerprintDistThreshold);
                if ~isempty(tracksForReconsideration)
                    if this.configuration.ParameterConfiguration.debugLevel > 1
                        this.utils.printToConsole(sprintf('Tracks moved to veryOld: %s',...
                            sprintf(repmat('%d ', 1, length(tracksForReconsideration)), ...
                            tracksForReconsideration)))
                    end
                    
                    [this, trackIdxToChange] = this.moveToVeryOld(tracksForReconsideration);
                    [~, locIdx] = ismember(tracksForReconsideration, newTrackIndex);
                    newTrackIndex(locIdx(locIdx > 0)) = trackIdxToChange;
                end
                
                if this.isFFPEval
                    evalFFP.reconsideredTracksThresh = [evalFFP. ...
                        reconsideredTracksThresh; tracksForReconsideration];
                    evalFFP.reconsideredTracksThreshFrame = [evalFFP. ...
                        reconsideredTracksThreshFrame; repmat(frameNumber, ...
                        numel(tracksForReconsideration), 1)];
                    evalFFP.reconsideredTracksFFPa = [evalFFP. ...
                        reconsideredTracksFFPa; trO(FFPaJnhbrhdMot>0)];
                    evalFFP.reconsideredTracksFFPaFrame = [evalFFP. ...
                        reconsideredTracksFFPaFrame; repmat(frameNumber, ...
                        numel(trO(FFPaJnhbrhdMot>0)), 1)];
                end
                
                %---------------------------------------------------------%
                % Save tracking assignments contained in OldFrame into the
                % SegmentationData object for the current frame
                %---------------------------------------------------------%
                if this.isFullCosts
                    this.data.saveTrackingAssignmentsToTrackerData(frameNumber, ...
                        struct('track_index', newTrackIndex, 'cell_dif_x', difX, ...
                        'cell_dif_y', difY, 'cell_dif_z', difZ, 'cell_filtered_dif_x', filteredDifX, ...
                        'cell_filtered_dif_y', filteredDifY, 'cell_filtered_dif_z', filteredDifZ,...
                        'track_total_assignment_cost', totalCostAssigned', ...
                        'track_position_assignment_cost', positionCostAssigned', ...
                        'track_area_assignment_cost', areaCostAssigned', ...
                        'track_rotation_assignment_cost', rotationCostAssigned', ...
                        'track_frame_skipping_cost', frameSkippingCostAssigned'));
                else
                    this.data.saveTrackingAssignmentsToTrackerData(frameNumber, ...
                        struct('track_index', newTrackIndex, 'cell_dif_x', difX, ...
                        'cell_dif_y', difY, 'cell_dif_z', difZ, 'cell_filtered_dif_x', filteredDifX, ...
                        'cell_filtered_dif_y', filteredDifY, 'cell_filtered_dif_z', filteredDifZ));
                end
               
                if this.isCorrectMotion
                    % Such that one can track experiments not starting with frame nr 1
                    if frameNumber > firstFrame + 1 % uniqueFrames(2)
                        this.data.initializeOldFrameEval(frameNumber - 2);
                        this.data.initializeNewFrameEval(frameNumber - 1);
                        [ ~ ] = this.improveInitialTracking(frameNumber -2 );                      
                    end
                end
                
                this.data.updateTemporaryTrackingDataForNextFrameEvaluation(...
                    this.configuration.ParameterConfiguration. ...
                    neighbourhoodSearchRadius); 
                                
                frameNumberIdx = frameNumberIdx +1;
                if this.configuration.ParameterConfiguration.debugLevel > 1
                    this.utils.printToConsole(sprintf( ...
                        'Elapsed time is %4.4f seconds for tracking frame transition %d-%d.', ...
                        toc, frameNumber-1, frameNumber))
                end
                pBarT.step([], [], []);
            end
            pBarT.release();
            
            % Evaluate tracking of last frame
            this.data.initializeOldFrameEval(frameNumber - 1);
            this.data.initializeNewFrameEval(frameNumber);
            [ ~ ] = this.improveInitialTracking(frameNumber - 1);
            
            % Get and set each track start and end frame
            [startFrameArray, endFrameArray] = this.data.getStartEndFrameOfTrack(...
                this.data.SegmentationData.cell_frame, ...
                this.data.SegmentationData.track_index);
            
            this.data.setFieldArray('track_start_frame', startFrameArray);
            this.data.setFieldArray('track_end_frame', endFrameArray);
            
            % Fill track holes
            % @todo needs proper validation -> future release
%             this.data.fillTrackHoles(this.configuration. ...
%                 ProjectConfiguration.imageFrameNumberArray, ...
%                 this.configuration.ParameterConfiguration. ...
%                 fingerprintHalfWindowSideLength, ...
%                 this.configuration.ParameterConfiguration. ...
%                 fingerprintMaxConsideredFrequencies, ...
%                 this.configuration.ProjectConfiguration.imageDir, ...
%                 this.configuration.ProjectConfiguration. ...
%                 imageFingerprintFileArray, ...
%                 this.configuration.ProjectConfiguration. ...
%                 imageCropCoordinateArray, ...
%                 this.configuration.ParameterConfiguration. ...
%                 neighbourhoodSearchRadius)
            
            % Finalize the track indicies
            this.data.setFinalTrackIndicies()
            
            % Recompute fingerprint distances
            for fn = this.configuration.ProjectConfiguration. ...
                    trackerStartFrame:this.configuration.ProjectConfiguration. ...
                    trackerEndFrame-1
                % Oldframe is fused with veryold! we would need to modify
                % veryoldEval to carry also the fingerprint and its distances
                % and remove those from the results before saving!
                % Needs to be recomputed after filling the track holes! where
                % we need to compute the fingerprint first for newly inserted
                % cells (is that not already done??
                oldFrame.track_index = this.data.getFieldArrayForFrame('track_index', fn);
                oldFrame.track_fingerprint_real = this.data.getFieldArrayForFrame('track_fingerprint_real', fn);
                newFrame.track_fingerprint_real = this.data.getFieldArrayForFrame('track_fingerprint_real', fn + 1);
                newFrame.track_index = this.data.getFieldArrayForFrame('track_index', fn + 1);
                
                [realFingerprintTrackDistance] = ...
                    this.fingerprint.computeAssignmentFingerprintDistanceArray(oldFrame, ...
                    newFrame);
                
                this.data.setFieldArrayForFrame('track_fingerprint_real_distance', fn, ...
                    realFingerprintTrackDistance);
            end
            
            if this.isFFPEval
                save(sprintf('%s_dCRFM.mat', this.configuration. ...
                    ProjectConfiguration.projectName), 'evalFFP')
            end

            this.utils.printToConsole(sprintf(['TRACKING: Done with tracking!' ...
                ' Elapsed time was %4.4f seconds'], toc(timeTotalTracking)))
        end
        
        function this = initializeTrackerCore(this)
            % INITIALIZETRACKERCORE Initailizes Tracker core.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                        :class:`Tracker` instance
            
            
            this.configuration = TracX.TrackerConfiguration;
            this.data = TracX.TrackerData;
            this.data.VeryOldEval.setMaxTrackFrameSkipping(this. ...
                configuration.ParameterConfiguration. ...
                maxTrackFrameSkipping)
            this.lineage = TracX.Lineage(this.data, this.configuration);
            this.utils = TracX.Utils();
            this.io = TracX.IO(this.utils);
            this.imageProcessing = TracX.ImageProcessing(this.io, ...
                this. configuration, this.data, this.utils, this.lineage);
            this.imageVisualization = TracX.ImageVisualization(this.data, ...
                this.configuration, this.imageProcessing, this.lineage);
            this.fingerprint = TracX.Fingerprint(this.utils, this.io);
            this.lap = TracX.LAP();
            this.motion = TracX.Motion();
            this.evaluation = TracX.TrackerEvaluation(this.configuration, ...
                this.data, this.io, this.utils, this.fingerprint, ...
                this.imageVisualization);
            this.version = sprintf('%d.%d.%d', ...
                this.configuration.ParameterConfiguration.major, ...
                this.configuration.ParameterConfiguration.minor, ...
                this.configuration.ParameterConfiguration.patch);
            
            % Check if toolboxes are installed
            this.utils.printToConsole('INITIALIZE: Welcome to TracX!')
            this.isAllInstalled = this.utils.checkToolboxStatus();
            this.isAllInstalled = this.setupDependencies();
            if this.isAllInstalled == 1
                this.utils.printToConsole(sprintf('INFO: TracX version %s is ready.', ...
                    this.version))
            else
                this.utils.printToConsole(sprintf('INFO: TracX version %s aborting...', ...
                      this.version))
            end
            
        end
        
        function newTrackIndex = improveInitialTracking(this, frameNumber )
            % IMPROVEINITIALTRACKING Improves the initial tracking by
            % correcting for motion and by using the CRF as proofreading.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance
            %    frameNumber (:obj:`int`):           Image frame number.
            %
            % Returns
            % -------
            %     newTrackIndex: (:obj:`array`)
            %                                        Array with new,
            %                                        potentially corrected
            %                                        tracking indices.
            
            newTrackIndex = [];
            MAXEVALUATION = 2;
            oldFrame = this.data.OldFrameEval;
            newFrame = this.data.NewFrameEval;          
            
            evaluation = 1;
            % Get non assigned cells from previous frames
            veryOld = this.data.VeryOldEval.getCellStruct;
            oldFrame = this.data.VeryOldEval.merge(oldFrame, veryOld);
            oldFrame.recalculateMotionCorrNeighbourhood(...
                this.configuration.ParameterConfiguration.neighbourhoodSearchRadius)
            % Here start verification loop
            while evaluation <= MAXEVALUATION
                if evaluation == 1
                    % Determine the data subset
                    [ subsetOldFrame, subsetNewFrame ] = this.data. ...
                        getCommonTrackDataSubset( ...
                        oldFrame, newFrame);
                else
                    % Determine the data subset
                    [ subsetOldFrame, subsetNewFrame ] = this.data. ...
                        getCommonTrackDataSubset( ...
                        oldFrame, newFrame, newTrackIndex2);
                end
                
                if ~isempty(subsetNewFrame) && ~isempty(subsetOldFrame)
                    % Calculate motions for correctly assigned cells
                    displacementOfAssignedTracks =  subsetNewFrame(:, 2:4) - ...
                        subsetOldFrame(:, 2:4);
                    
                    distToNeighbourhood = pdist2([oldFrame.cell_center_x, ...
                        oldFrame.cell_center_y oldFrame.cell_center_z], [subsetOldFrame(:, 2), ...
                        subsetOldFrame(:, 3), subsetOldFrame(:, 4)], 'euclidean');
                    
                    cellCloseNeighbour = distToNeighbourhood <= this. ...
                        configuration.ParameterConfiguration. ...
                        neighbourhoodSearchRadius;
                    
                    % Compute motion costs
                    [ assignmentCost, ~, ~] = this.lap. ...
                        computeNeighbourhoodMotionCostMatrix( ...
                        oldFrame, newFrame, displacementOfAssignedTracks, ...
                        this.configuration.ParameterConfiguration. ...
                        maxExpectedMovement, cellCloseNeighbour, ...
                        this.configuration.ParameterConfiguration. ...
                        individualFunctionPenalty, ...
                        this.configuration.ParameterConfiguration. ...
                        maxCellSizeDecrease, ...
                        this.configuration.ParameterConfiguration. ...
                        averageCellSizeGrowth, ...
                        this.configuration.ParameterConfiguration. ...
                        maxCellSizeIncrease);
                    
                    % Apply knowledge from fingerprint distance check
                    assignmentCost = this.lap.fingperprintCostMatrixRefinement( ...
                        newFrame, oldFrame, assignmentCost, this. ...
                        configuration.ParameterConfiguration. ...
                        fingerprintDistThreshold);
                    
                    % Solve assignment
                    [ ~, assignmentColIdx, ~, ~] = this.lap. ...
                        solveLAP(assignmentCost);
                    
                    validAssignmentsLocIdx = ismember(assignmentColIdx, ...
                        1:numel(newFrame.cell_area));
                    newTrackIndex = zeros(numel(newFrame.cell_area), 1);
                    newTrackIndex(assignmentColIdx(validAssignmentsLocIdx)) = ...
                        oldFrame.track_index(validAssignmentsLocIdx);
                    
                    % Use newFrame tack indicies directly for non assigned tracks
                    nonAssignedNewFrameTracks = newFrame.track_index(...
                        newTrackIndex == 0);
                    newTrackIndex(newTrackIndex == 0) = nonAssignedNewFrameTracks;
                                        
                    [Lia, Ci] = ismember(oldFrame.track_index, newTrackIndex');
                    Ci = Ci(Ci > 0);
                    
                    neighbourMatrix = this.data.getCloseCellNeighbourIdx( ...
                        oldFrame.cell_center_x(Lia), oldFrame.cell_center_y(Lia),...
                        this.configuration.ParameterConfiguration. ...
                        neighbourhoodSearchRadius, oldFrame.cell_center_z(Lia));
                    
                    [ invalidVectorsIdx, ~] = this.motion.getInvalidVectors( ...
                        oldFrame.cell_center_x(Lia), oldFrame.cell_center_y(Lia), ...
                        oldFrame.cell_center_z(Lia), ...
                        newFrame.cell_center_x(Ci), newFrame.cell_center_y(Ci), ...
                        newFrame.cell_center_z(Ci), ...
                        neighbourMatrix, this.configuration. ...
                        ParameterConfiguration.meanCellDiameter , 1 );
                    %  @todo nStd standard devation should that be a
                    %  user definable parameter? If so how would the user be able
                    %  to check / control potential changes
                    
                    [Lia2, ~] = this.utils.getCommonLogical( Lia, ...
                        ~invalidVectorsIdx );
                    
                    % Filter the vector field
                    difX = newFrame.cell_center_x(Ci(~invalidVectorsIdx)) - ...
                        oldFrame.cell_center_x(Lia2);
                    difY = newFrame.cell_center_y(Ci(~invalidVectorsIdx)) - ...
                        oldFrame.cell_center_y(Lia2);
                    difZ = newFrame.cell_center_z(Ci(~invalidVectorsIdx)) - ...
                        oldFrame.cell_center_z(Lia2);
                    [filteredDifX, filteredDifY, filteredDifZ] = this.motion. ...
                        filterVectorField(...
                        oldFrame.cell_center_x(Lia2), ...
                        oldFrame.cell_center_y(Lia2), ...
                        oldFrame.cell_center_z(Lia2), ...
                        difX,...
                        difY,...
                        difZ,...
                        65, ...
                        65, ...
                        65);
                    
                    % Recalculated neighbourhood, as the size of the matrix changes depending
                    % on how many cells are joined form the veryOld struct.
                    neighbourMatrix = this.data.getCloseCellNeighbourIdx( ...
                        oldFrame.cell_center_x, oldFrame.cell_center_y, ...
                        this.configuration.ParameterConfiguration. ...
                        neighbourhoodSearchRadius, oldFrame.cell_center_z);
                    
                    % Interpolate the filtered vector field by averaging
                    [filteredDifXAll, filteredDifYAll, filteredDifZAll] = this.motion. ...
                        interpolateVectorComponents( oldFrame.cell_center_x,  ...
                        oldFrame.cell_center_y, filteredDifX, filteredDifY, filteredDifZ, ...
                        Lia2, neighbourMatrix);
                    
                    oldFrame.cell_filtered_dif_x = filteredDifXAll;
                    oldFrame.cell_filtered_dif_y = filteredDifYAll;
                    oldFrame.cell_filtered_dif_z = filteredDifZAll;
                    
                    % 2nd tracking step (our tracker)
                    [ newTrackIndex2, ~, ~, ~, ~, ~ ] = this.lap. ...
                        computeNewFrameAssignment( ...
                        oldFrame, newFrame, this.configuration. ...
                        ParameterConfiguration.maxCellCenterDisplacement, ...
                        this.configuration.ParameterConfiguration. ...
                        maxMajorAxisRotation, this.configuration. ...
                        ParameterConfiguration.maxTrackFrameSkipping, ...
                        this.configuration.ParameterConfiguration. ...
                        individualFunctionPenalty, this.configuration. ...
                        ParameterConfiguration.maxCellSizeDecrease, ...
                        this.configuration.ParameterConfiguration. ...
                        averageCellSizeGrowth, this.configuration. ...
                        ParameterConfiguration.maxCellSizeIncrease, ...
                        this.configuration.ParameterConfiguration. ...
                        usedFunctionsForCostMatrix, this.configuration. ...
                        ParameterConfiguration.fingerprintDistThreshold, ...
                        this.configuration.ParameterConfiguration. ...
                        meanCellDiameter, this.configuration.ParameterConfiguration. ...
                        data3D);
                else
                    break;
                end
                
                evaluation = evaluation + 1;
            end
            
            
            if exist('newTrackIndex2','var')
                                
                this.data.setFieldArrayForFrame('track_index', frameNumber + 1, ...
                    newTrackIndex2);
                                
                % Handle veryOld cases
                nonSegmentedTracksIdx = ~ismember(oldFrame.track_index,...
                    newTrackIndex2);
                if ~isempty(veryOld)
                    veryOldTrackIndex = veryOld.track_index;
                    veryOldTrackIndex(~ismember(veryOldTrackIndex, ...
                        oldFrame.track_index(nonSegmentedTracksIdx)));
                    this.data.VeryOldEval.removeCell(veryOldTrackIndex(...
                        ~ismember(veryOldTrackIndex, oldFrame.track_index(...
                        nonSegmentedTracksIdx))));
                end
                
                % Add non assigned tracks to veryOld
                % @todo add neighbourhood support
                this.data.VeryOldEval.addCell([oldFrame.track_index(...
                    nonSegmentedTracksIdx), ...
                    oldFrame.cell_center_x(nonSegmentedTracksIdx), ...
                    oldFrame.cell_center_y(nonSegmentedTracksIdx), ...
                    oldFrame.cell_center_z(nonSegmentedTracksIdx), ...
                    oldFrame.cell_filtered_dif_x(nonSegmentedTracksIdx), ...
                    oldFrame.cell_filtered_dif_y(nonSegmentedTracksIdx), ...
                    oldFrame.cell_filtered_dif_z(nonSegmentedTracksIdx), ...
                    oldFrame.cell_area(nonSegmentedTracksIdx), ...
                    oldFrame.cell_orientation(nonSegmentedTracksIdx), ...
                    oldFrame.track_age(nonSegmentedTracksIdx), ...
                    oldFrame.track_fingerprint_real_distance(nonSegmentedTracksIdx), ...
                    oldFrame.track_assignment_fraction(nonSegmentedTracksIdx)])
                this.data.VeryOldEval.increaseTrackAge();
            end
        end
        
        function [this, trackIdxToChange] = moveToVeryOld(this, selectedTrackArray)
            % MOVETOVERYOLD Moves temporary data stored in OldFrame to
            % VeryOld.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance
            %    selectedTrackArray (:obj:`int`):    Array with track
            %                                        indices to be moved to veryOld.
            %
            % Returns
            % -------
            %     this: :obj:`obj`
            %                                        :class:`Tracker` instance
            %     trackIdxToChange: :obj:`array`
            %                                        Array with tracking
            %                                        indices to move.
            
            % Remove Track(s) that are in VeryOld allready
            selectedTrackArray(ismember(selectedTrackArray, this.data.VeryOld.track_index)) = [];
            trackIdxToChange = nan(numel(selectedTrackArray), 1);
            initialTracksInVeryOld = this.data.VeryOld.track_index;
            
            % Move selected tracks from NewFrame to VeryOld
            % object
            structNameArray = fieldnames(this.data.NewFrame);
            iIteration = 1;
            while iIteration <= numel(structNameArray)
                for i = 1:numel(selectedTrackArray)
                    if size(this.data.NewFrame.(structNameArray{iIteration}),2) == 1
                        if ~strcmp(structNameArray{iIteration}, 'track_index')
                            this.data.VeryOld.(structNameArray{iIteration}) = [this.data.VeryOld.(structNameArray{iIteration});...
                                this.data.NewFrame.(structNameArray{iIteration})(this.data.NewFrame.track_index == selectedTrackArray(i))];
                        else
                            this.data.VeryOld.(structNameArray{iIteration}) = [...
                                this.data.VeryOld.(structNameArray{iIteration}); this.data.numberOfTracks];
                            trackIdxToChange(i) = this.data.numberOfTracks;
                            this.data.numberOfTracks = this.data.numberOfTracks + 1;
                        end
                    else
                        % @todo check why NewFrame is sometimes empty for
                        % the neighbourhood where there clearly should be
                        % one.
                        if ~isempty(this.data.NewFrame.(structNameArray{iIteration}))
                            this.data.VeryOld.(structNameArray{iIteration}) = [this.data.VeryOld.(structNameArray{iIteration});...
                                this.data.NewFrame.(structNameArray{iIteration})(this.data.NewFrame.track_index == selectedTrackArray(i), :)];
                        end
                    end
                end
                iIteration = iIteration+1;
            end
            
            structNameArray = fieldnames(this.data.OldFrame);
            iIteration=1;
            while iIteration <= numel(structNameArray)
                for i = 1:numel(selectedTrackArray)
                    if size(this.data.OldFrame.(structNameArray{iIteration}),2) == 1
                        if ~strcmp(structNameArray{iIteration}, 'track_index')
                            this.data.VeryOld.(structNameArray{iIteration}) = [this.data.VeryOld.(structNameArray{iIteration});...
                                this.data.OldFrame.(structNameArray{iIteration})(this.data.OldFrame.track_index == selectedTrackArray(i))];
                        else
                            this.data.VeryOld.(structNameArray{iIteration}) = [...
                                this.data.VeryOld.(structNameArray{iIteration}); selectedTrackArray(i)];
                        end
                    else
                        %  @todo: Recompute neighbourhood for cell frome
                        %  OldFrame being in NewFrame. Does potentially
                        %  overlap with assignment from NewFrame but does
                        %  not have to be correct
                        coordinateArray = [ [this.data.OldFrame.cell_center_x(this.data.OldFrame.track_index == selectedTrackArray(i)); ...
                            this.data.NewFrame.cell_center_x], ...
                            [this.data.OldFrame.cell_center_y(this.data.OldFrame.track_index == selectedTrackArray(i));...
                            this.data.NewFrame.cell_center_y], ...
                            [this.data.OldFrame.cell_center_z(this.data.OldFrame.track_index == selectedTrackArray(i));...
                            this.data.NewFrame.cell_center_z]];
                        neighbourMatrix = this.data.getCloseCellNeighbourIdx(...
                            coordinateArray(:,1), coordinateArray(:,2), ...
                            this.configuration.ParameterConfiguration.neighbourhoodSearchRadius, coordinateArray(:,3));
                        
                        this.data.VeryOld.(structNameArray{iIteration}) = [this.data.VeryOld.(structNameArray{iIteration});...
                            neighbourMatrix(1, 2:end)];
                    end
                end
                iIteration = iIteration+1;
            end
            % Increase age by 2 for newly added tracks in VeryOld
            this.data.VeryOld.track_age(~ismember(this.data.VeryOld.track_index,initialTracksInVeryOld)) = ...
                this.data.VeryOld.track_age(~ismember(this.data.VeryOld.track_index,initialTracksInVeryOld)) + 2;
            % Remove selected tracks from NEWFRAME Object
            %  @todo: Remove object actually from new frame and also move it
            %  there to the veryold frame
            iIteration = 1;
            removeInNewFrameIdx = ismember(this.data.NewFrame.track_index, selectedTrackArray);
            while iIteration <= numel(structNameArray)
                if size(this.data.NewFrame.(structNameArray{iIteration}),2) == 1
                    this.data.NewFrame.(structNameArray{iIteration})(removeInNewFrameIdx) = [];
                else
                    if ~(islogical(this.data.NewFrame.(structNameArray{iIteration})) && ...
                            isempty(this.data.NewFrame.(structNameArray{iIteration})))
                        this.data.NewFrame.(structNameArray{iIteration})(removeInNewFrameIdx, :) = [];
                        this.data.NewFrame.(structNameArray{iIteration})(:, removeInNewFrameIdx) = [];
                    end
                end
                
                iIteration = iIteration+1;
            end
        end
        
        function [oldFrameLocIdx, emptyDataLocIdx] = getTrackIndexStorageIndicies(this)
            % GETTRACKINDEXSTORAGEINDICIES Returns indicies of track_index
            % where to store them.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance
            %
            % Returns
            % -------
            %     oldFrameLocIdx: :obj:`array`
            %                                        Array with location
            %                                        indices in the
            %                                        oldFrame.
            %     emptyDataLocIdx: :obj:`array`
            %                                        Array with location
            %                                        indices for empty data.
            
            range = transpose(1:1:numel(this.data.OldFrame.track_index));
            matchIdx = arrayfun(@(x)find([this.data.OldFrame.track_index; ...
                this.data.VeryOld.track_index]==x,1),this.data.NewFrame.track_index,...
                'UniformOutput', 0);
            matchArray = arrayfun(@(x)range(cell2mat(x)),matchIdx, 'UniformOutput', 0);
            oldFrameLocIdx = cell2mat(matchArray);
            emptyDataLocIdx = cellfun('isempty',matchArray);
        end
        
        function copyResultsToTrackerData(this, dataToSave, fieldName, ...
                emptyDataLocIdx, oldFrameLocIdx, frameNumber)
            % COPYRESULTSTOTRACKERDATA Copies results of the frame
            % by frame tracking to the data store Tracker.TrackerData.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance.
            %    dataToSave (:obj:`array`):          Array with data to be
            %                                        saved to :class:`~+TracX.@TrackerData.TrackerData`
            %    fieldName (:obj:`str`):             Fieldname name
            %    emptyDataLocIdx (:obj:`array`):     Location indicies of empty data.
            %    oldFrameLocIdx (:obj:`array`):      OldFrame location indicies for track_index.
            %    frameNumber (:obj:`int`):           Frame number
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                        :class:`Tracker` instance
            
            dataToSave(~emptyDataLocIdx) = this.data.OldFrame.(fieldName)...
                (oldFrameLocIdx);
            if iscell(dataToSave)
                dataToSave(emptyDataLocIdx) = {nan};
            else
                dataToSave(emptyDataLocIdx) = nan;
            end
            this.data.setFieldArrayForFrame(fieldName, frameNumber, ...
                dataToSave);
        end
        
        function importAlphaShapes(this)
            % IMPORTALPHASHAPES Imports the alpha shapes form the
            % project directory.
            %
            % Args:
            %    this (:obj:`obj`):                  :class:`Tracker` instance.
            %
            % Returns
            % -------
            %     void: (:obj:`-`)
            %                                        :class:`Tracker` instance
            
            this.data.importAlphaShapes(this.configuration. ...
                ProjectConfiguration.segmentationResultDir);
            
        end
        
        function set.isFullCosts(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max allowed segmentation
            % object centroid displacement [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`Tracker` instance.
            %    value (:obj:`float`):       Sets value for max allowed
            %                                segmentation object centroid
            %                                displacement [pixel].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.isFullCosts = value;
        end
             
        function set.isCorrectMotion(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max allowed segmentation
            % object centroid displacement [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`Tracker` instance.
            %    value (:obj:`float`):       Sets value for max allowed
            %                                segmentation object centroid
            %                                displacement [pixel].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.isCorrectMotion = value;
        end
        
        function set.isAllInstalled(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max allowed segmentation
            % object centroid displacement [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`Tracker` instance.
            %    value (:obj:`float`):       Sets value for max allowed
            %                                segmentation object centroid
            %                                displacement [pixel].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.isAllInstalled = value;
        end
        
        function set.isReady(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max allowed segmentation
            % object centroid displacement [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`Tracker` instance.
            %    value (:obj:`float`):       Sets value for max allowed
            %                                segmentation object centroid
            %                                displacement [pixel].
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.isReady = value;
        end
        
        
        exportTrackingResults(this, joinedDataTable, resultFilePath, ...
            resultFileName, varargin)
        % EXPORTTRACKINGRESULTS Exports results to disk as
        % text, csv or mat file format delimited by your choice. Default
        % is as text file and tab delimited.
        
        runLineageReconstruction(this, varargin)
        % RUNLINEAGERECONSTRUCTION Starts lineage reconstruction based on tracking
        % results for symmetrically and asymmetrically dividing cells. Wrapper for
        % lineage instance.
        
        revertSegmentationImageCrop( this )
        % REVERTSEGMENTATIONIMAGECROP Reverts a potentially applied
        % image crop during segmentation to map the segmentation objects'
        % centroid coordintates to the raw image.
        
        saveCurrentTrackerState( this, varargin )
        % SAVECURRENTTRACKERSTATE Saves the current state of the
        % tracker to disk.
        
        saveTrackerMaskFrame( this, frameNumber )
        % SAVETRACKERMASKFRAME Saves a labeled mask file. It is a
        % matrixwhere the elements correspond to a segmented cell are
        % labeled with their track indices.
        
        saveTrackerControlImageFrame( this, frameNumber, varargin )
        % SAVETRACKERCONTROLIMAGES Saves a tracking control images with
        % each tracked cell labeled with its track index for a given image frame.
        
        saveTrackerControlMaskImageFrame( this, frameNumber, varargin)
        % SAVETRACKERCONTROLMASKIMAGEFRAME Saves a tracker control
        % image for a particular frame where the labeled tracker mask are
        % overlayed onto the raw image. Optionaly the label color can be set
        % using a cmap.
        
        saveTrackerProjectControlImages( this, varargin )
        % SAVETRACKERPROJECTCONTROLIMAGES Saves control images for
        % the current experiment for all tracked image frames.
        
        saveTrackerMarkerControlImages(this)
        % SAVEMARKERCONTROLIMAGES Saves marker tracking control images to disk.
        
        saveTrackerCellCycleResultsAsTable(this, varargin)
        % SAVETRACKERCELLCYCLERESULTSASTABLE Saves the cell cycle results to
        % disk as text, csv or mat file format delimited by your choice.
        
        [ status ] = setupDependencies(this)
        % SETUPDEPENDENCIES Checks the presence of the external dependencies.
        % If some are missing they are automatically downloaded.
        
        [ ] = testSegmentationArtefactRemoval(this, frame, channel, ...
            stdFactor, varargin)
        % TESTSEGMENTATIONARTEFACTREMOVAL Test of potential segmentation
        % artefacts removal using the cell autofluorescence. Outcome is
        % visualized as image and detected cell indices printed to the console.
        
        [ ] = testBudNeckSegmentation(this, frame, varargin)
        % TESTBUDNECKSEGMENTATION Tests the parameter for budneck segmentation
        % and displays a control image to optimize the parameters if needed.
        
        [ ] = testTrackingParameters(this, testRange, varargin)
        % TESTTRACKINGPARAMETERS Tests the current set of tracker
        % parameters.
        
        [ ] = testMarkerTrackingParameters(this, testRange, varargin)
        % TESTMARKERTRACKINGPARAMETERS Tests the marker tracking parameters
        % and displays the results for the given range.
        
        [ ] = testImageBorderDataRemoval(this, frameNumber, borderOffset)
        % TESTIMAGEBORDERDATAREMOVAL Tests the data removal for cells
        % touching the image border within borderOffset distance.
        
        [ ] = testFeatureDataRemoval(this, frameNumber, feature, threshold)
        % TESTFEATUREDATAREMOVAL Tests the data removal for cells above a
        % feature threshold.
        
        [ roi ] = testROIDataRemoval(this, frameNumber)
        % TESTROIDATAREMOVAL Tests the data removal for cells outside a ROI.
        
        correctMDMisassignment(this, trackIndexDaughter, ...
            newMotherTrackIndex, oldMotherTrackIndex, divisionFrameNumber)
        % CORRECTMDMISASSIGNMENT Allows to manually correct a mother
        % daughter assignment for a given lineage.
        
        correctTrackMisassignment(this, frameArray, oldTrackIndexArray, ...
            newTrackIndexArray)
        % CORRECTTRACKMISASSIGNMENT Allows to manually correct a cell to track
        % assignment on specific frames.
        
        exportCellCycleResults(this, resultFilePath, resultFileName, ...
            varargin)
        % EXPORTCELLCYCLERESULTS Exports the cell cycle results to disk as
        % text, csv or mat file format delimited by your choice.
        
        saveTrackerResultsAsTable(this, varargin)
        % SAVETRACKERRESULTSASTABLE Save the tracking results along with the
        % segmentation data in one table to a file.
        
        deleteSegObjAtBorder(this, frameNumber, borderOffset)
        % DELETESEGOBJATBORDER Delete segmentation objects at the image border
        % within the border offset.
        
        deleteSegArtefacts(this, varargin)
        % DELSEGARTEFACTS Deletes potential segmentation artefacts (segmented
        % objects which are no cells based the autofluorescence.
        
        deleteByROI(this, roi, varargin)
        % DELETEBYROI Deletes segmentation objects outside a region of interest
        % (ROI).
        
        deleteByFeature(this, feature, threshold, varargin)
        % DELETEBYFEATURE Deletes segmentation objects exceeding a threshold
        % for a feature.
        
        runReQuantification(this, fluoTags, varargin)
        % RUNREQUANTIFICATION Runs the re segmentation and quantification
        % of merged tracking masks for buds and mothers prior division.
        
        generateScriptFromTracker(this,varargin)
        % GENERATESCRIPTFROMTRACKER Generates a Matlab (.m) script file based on
        % the tracker project configuration made with the GUI.
                
    end
    
end