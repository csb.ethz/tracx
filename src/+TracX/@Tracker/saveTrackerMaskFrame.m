%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = saveTrackerMaskFrame( this, frameNumber )
% SAVETRACKERMASKFRAME Saves a labeled mask file. It is a matrix
% where the elements correspond to a segmented cell are labeled with their
% track indices.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    frameNumber (:obj:`int`):                 Image frame to test.
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                              Saves masks to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

trackIdxOnCurrentFrame = this.data.getFieldArrayForFrame('track_index', ...
    frameNumber);
cellIdxOnCurrentFrame = this.data.getFieldArrayForFrame('cell_index', ...
    frameNumber);

mask = this.io.readSegmentationMask(fullfile(this.configuration. ...
    ProjectConfiguration.segmentationResultDir, ...
    this.configuration.ProjectConfiguration. ...
    segmentationResultMaskFileArray{frameNumber}));

maskIdx = unique(mask);
cellIdxR = cellIdxOnCurrentFrame(~isnan(cellIdxOnCurrentFrame));
mask(ismember(mask, maskIdx(~ismember(maskIdx, cellIdxR)))) = 0;

% Relabel the segmentation indicies with the track indices
mask = this.imageProcessing.relabelMaskIndices(mask, trackIdxOnCurrentFrame, ...
    cellIdxOnCurrentFrame);

cropCoords = this.configuration.ProjectConfiguration. ...
        imageCropCoordinatePriorReversionArray;
    
if ~isempty(cropCoords)
    % Correct the segmentation mask from a cropped image region back to its
    % original position in the raw image
    maskRawSize = zeros(this.configuration.ProjectConfiguration.imageHeight, ...
        this.configuration.ProjectConfiguration.imageWidth);
    maskRawSize(cropCoords(2):cropCoords(2)+cropCoords(4), ...
        cropCoords(1):cropCoords(1)+cropCoords(3)) = mask;
    
    % Save mask
    this.io.writeToMAT(maskRawSize, this.configuration.ProjectConfiguration. ...
        segmentationResultDir, ...
        this.configuration.ProjectConfiguration.trackerResultMaskFileArray{...
        frameNumber})
else
    % Save mask
    this.io.writeToMAT(mask, this.configuration.ProjectConfiguration. ...
        segmentationResultDir, ...
        this.configuration.ProjectConfiguration.trackerResultMaskFileArray{...
        frameNumber})
end

end
