%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function deleteByFeature(this, feature, threshold, varargin)
% DELETEBYFEATURE Deletes segmentation objects exceeding a threshold for a
% feature.
%
% Args:
%    this (:obj:`obj`):                        :class:`Tracker` object
%    feature (:obj:`str`):                     Any segmentation feature to
%                                              use to threshold the data before
%                                              tracking. :class:`~+TracX.@SegmentationData.SegmentationData`
%    threshold (:obj:`float`):                 Threshold. Feature values
%                                              below the threshold are selected.
%    varargin (:obj:`str varchar`):
%
%        * **neighbourhoodSearchRadius** (:obj:`int`): Neighbourhood radius. Defaults to :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius`
% 
% Returns
% -------
%     void: (:obj:`-`)          
%                                               Acts on :class:`Tracker` object
%                                               directly and deletes selected
%                                               data below threshold.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
validFieldNames = [fieldnames(this.data.SegmentationData); ...
    fieldnames(this.data.QuantificationData)];
validFeature =  @(x) any(validatestring(x, validFieldNames)); 
validThreshold = @(x) isnumeric(x); 
defaultNeighbourhoodSearchRadius = this.configuration.ParameterConfiguration. ...
    neighbourhoodSearchRadius;
validNeighbourhoodSearchRadius = @(x) isnumeric(x) && x > 0; 
 
addRequired(p, 'this', @isobject);
addRequired(p, 'feature', validFeature);
addRequired(p, 'threshold', validThreshold);
addOptional(p, 'neighbourhoodSearchRadius', defaultNeighbourhoodSearchRadius, ...
    validNeighbourhoodSearchRadius);
parse(p, this, feature, threshold, varargin{:});

% Delete 
this.data.delByFeature(p.Results.feature, p.Results.threshold, ...
    p.Results.neighbourhoodSearchRadius)

end