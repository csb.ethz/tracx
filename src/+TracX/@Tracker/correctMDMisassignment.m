%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [] = correctMDMisassignment(this, trackIndexDaughter, ...
    newMotherTrackIndex, oldMotherTrackIndex, divisionFrameNumber)
% CORRECTMDMISASSIGNMENT Allows to manually correct a mother daughter
% assignment for a given lineage. A given daughter track index
% will be replaced with a new mother track index. The arrays need
% to have the same size. Lineage roots or on the first frame should have
% the value 0 instead of a track index.
%
% Args:
%    this (:obj:`object`):                       :class:`Tracker` instance.   
%    trackIndexDaughter (:obj:`array`):          Array of daughter track indices.
%    newMotherTrackIndex (:obj:`array`):         Array of mother track indices.
%    oldMotherTrackIndex (:obj:`array`):         Array of old mother track indices.
%    divisionFrameNumber (:obj:`array`):         Frame number  of division frame.
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Andreas P. Cuny - initial implementation


if ~isequal(size(trackIndexDaughter), size(newMotherTrackIndex), ...
        size(oldMotherTrackIndex), size(divisionFrameNumber))
    this.utils.printToConsole(sprintf(['ERROR: Arrays need to have the same' ...
        ' size. Daughter array has size %d,%d \n New mother array has size' ...
        ' %d,%d Old mother array has size %d,%d division frame array has' ...
        'size %d,%d'], ...
        size(trackIndexDaughter), size(newMotherTrackIndex), ...
        size(oldMotherTrackIndex), size(divisionFrameNumber)))
    return
end

this.utils.printToConsole('INFO: Start manual mother daughter assignment correction.') 

% Reset relevant fields to make sure no leftover info is kept
emptyData = zeros(numel(this.data.getFieldArray('cell_frame')), 1);

this.data.setFieldArray('track_parent', emptyData)
this.data.setFieldArray('track_parent_prob', emptyData)
this.data.setFieldArray('track_parent_score', emptyData)
this.data.setFieldArray('track_lineage_tree', emptyData)
this.data.setFieldArray('track_generation', emptyData)
this.data.setFieldArray('track_has_bud', emptyData)
this.data.setFieldArray('track_age', emptyData)

if size(trackIndexDaughter, 1) > size(trackIndexDaughter, 2)
    data2Replace = horzcat(trackIndexDaughter, newMotherTrackIndex, ...
        oldMotherTrackIndex, divisionFrameNumber);
else
    data2Replace = horzcat(trackIndexDaughter', newMotherTrackIndex', ...
        oldMotherTrackIndex', divisionFrameNumber');
end
this.data.TmpLinAss.replaceOrAddNew(data2Replace)

% Update 'track_has_bud' and 'track_parent', 'track_parent_prob',
% 'track_parent_score'
this.lineage.setParentAndHasBud()

% Update 'track_lineage_tree' and 'track_generation'
this.lineage.setTrackGeneration()

% Update 'track_age'
this.lineage.setTrackAge()

this.utils.printToConsole('INFO: Done with manual mother daughter assignment correction!') 
end