%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef ParameterConfiguration < handle & matlab.mixin.CustomDisplay
    % PARAMETERCONFIGURATION This class creates an object containing all
    % the parameters for the Tracker.
    % The class can create and read a Tracer parameter XML file to
    % automize the tracking process.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties(Constant)
        
        major = 1;   % Current latest version major of TracX.
        minor = 0;   % Current latest version minor of TracX.
        patch = 0;   % Current latest version patch of TracX.
    end
    
    properties (GetAccess=public, SetAccess=private)
        
        %> Parameter defining the debug level [0,1,2]
        %> (Tracker core)
        %>
        %> Parameter defining the debug level. Level 0 shows no output.
        %> level 1 shows general information (info mode) level 2 shows
        %> additional debug info (debug mode).
        %> advanced param
        debugLevel = 1; % Parameter defining the debug level [0,1,2]
        
        %> The number of pixels that one Z-stack interval is equal to
        %> (Tracker core)
        %>
        %> Number of pixels per Z-stack interval / step.
        %> optional param
        pixelsPerZPlaneInterval = 1; % The number of pixels that one Z-stack interval is equals to.
        
        %> Max displacement allowed [px]
        %> (Tracker core)
        %>
        %> Max displacement of segmented objects in pixel (depends on pixel
        %> size and magnification) allowed in any direction for subsequent
        %> image frames.
        %> general param
        maxCellCenterDisplacement = 15; % Max displacement allowed [px].
        
        %> Max expected (linear) movement [px]
        %> (Tracker core)
        %>
        %> Max expected (linear) movement of one or multiple segmented
        %> objects as i.e colony jump in pixel (depends on pixel
        %> size and magnification). By default it is 2.5
        %> times the average cell diameter but depends on the actual
        %> largest movement.
        maxExpectedMovement = 75; % Max expected (linear) movement [px]
        
        %> Fractional cell size growth
        %> (Tracker core)
        %>
        %> Average fractional growth of an identical segmented object between
        %> two subsequent image frames (i.e. cell area or nuclear area)
        %> general param
        averageCellSizeGrowth = 0.03; % Fractional cell size growth.
        
        %> Max allowed fractional cell size decrease
        %> (Tracker core)
        %>
        %> Penalty for maximum fractional segmented object size decrease for
        %> subsequent image frames;
        %> general param
        maxCellSizeDecrease = 0.75; % Max allowed fractional cell size decrease.
        
        %> Max allowed fractional cell size increase
        %> (Tracker core)
        %>
        %> Penalty for maximum fractional segmented object size increase for
        %> subsequent image frames;
        %> general param
        maxCellSizeIncrease = 2.5; % Max allowed fractional cell size increase.
        
        %> Major axis rotation  [degrees]
        %> (Tracker core)
        %>
        %> Max allowed major axis rotation (for round objects such as
        %> S.cerevisiae cells the penalty is not telling and should be set
        %> to 0. The paramater might be important for S.pombe (rod shaped)
        %> where the majoraxis is clearly defined);
        %> general param
        maxMajorAxisRotation = 0; % Major axis rotation  [degrees].
        
        %> Individual function penalty
        %> (Tracker core)
        %>
        %> Individual penalty maximum for each cost function (e.g. for
        %> position, area, rotation, frame skipping);
        %> general param
        individualFunctionPenalty = 500; % Individual function penalty.
        
        %> Max number of frames where a segmented object is allowed to be
        %> missing from a track (miss segmentation, out of focus, etc)
        %> [image frames]
        %> (Tracker core)
        %>
        %> The number of frames where a segmented object is not present;
        %> general param
        maxTrackFrameSkipping = 2; % Max number of frames where a segmented object is allowed to be missing from a track (miss segmentation, out of focus, etc).
        
        %> Logical array to select which of the four cost functions
        %> should be used to build the final cost matrix. [boolean]
        %> (Tracker core)
        %>
        %> Array specifying which cost functions to consider to build
        %> final cost matrix;
        %> general param
        usedFunctionsForCostMatrix = [1,1,0,0]; % Logical array to select which of the four cost functions should be used to build the final cost matrix. [bool]
        
        %> Mean cell diameter of all segmented objects in
        %> the experiment [px].
        %> (Tracker core)
        %>
        %> Mean cell diameter of all segmented objects of all image frames
        %> of the whole experiment;
        %> general param
        meanCellDiameter = 30; % Mean cell diameter of all segmented objects in the experiment [px].
        
        %> Mean cell diameter scaling factor.
        %> (Tracker core)
        %>
        %> Mean cell diameter scaling factor to scale the max allowed linear
        %> motion of cells in respect to the mean cell diameter.
        %> general param
        meanCellDiameterScalingFactor = 2.5; % Mean cell diameter scaling factor.
        
        %> Half window side length of a square around the centroid
        %> coordinates of a segmented object to be used to for fingerprint
        %> computation [px].
        %> (Tracker core)
        %>
        %> general param
        fingerprintHalfWindowSideLength = 25; % Half window side length of a square around the centroid coordinates of a segmented object to be used to for fingerprint computation [px].
        
        %> Resize factor for fingerprint calculation
        %> (Tracker core)
        %>
        %> Resize factor applied to image matrix before fingerprint
        %> calculation;
        %> general param
        fingerprintResizeFactor = 32; % Resize factor for fingerprint calculation.
        
        %> Number of DCT frequencies to include for fingerprint computation
        %> (Tracker core)
        %>
        %> Number of DCT frequencies to include for fingerprint computation;
        %> advanced param
        fingerprintMaxConsideredFrequencies = 8; %  Number of DCT frequencies to include for fingerprint computation.
        
        %> Fingerprint distance threshhold [a.u.]
        %> (Tracker core)
        %>
        %> Fingerprint distance threshold below which two fingerprints are
        %> considered to be identical. A fingerprint is computed by a
        %> window around the center of a segmented object on an image
        %> frame. Assignments between two objects with a fingerprint
        %> distance above this threshold are deleted;
        %> general param
        fingerprintDistThreshold = 0; % Fingerprint distance threshhold [a.u.].
        
        %> Radius around a centroid cooridnates of a segmented object used
        %> to detect the neighbouring cells [px]
        %> (Tracker core, Lineage reconstruction)
        %>
        %> All centroid cooridnates of segmented objects within the
        %> neighbourhoodSearchRadius are counted as neigbours of a selected
        %> segmented object;
        %> general param
        neighbourhoodSearchRadius = 40; % Radius around a centroid cooridnates of a segmented object used to detect the neighbouring cells [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in X [px].
        %> (Tracker core)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in X. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> general param
        radiusVectorFilterX = 100; % Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in X [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in Y [px].
        %> (Tracker core)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in Y. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> general param
        radiusVectorFilterY = 100; % Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in Y [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in Z [px].
        %> (Tracker core)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in Z. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> general param
        radiusVectorFilterZ = 100; % Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in Z [px].
        
        %> Edge sensitivity threshold for division marker segmentation
        %> [0-1]
        %> (Lineage reconstruction)
        %>
        %> Edge sensitivity threshold for division marker segmentation
        %> optional param
        divisionMarkerEdgeSensitivityThresh = 0.3; % Edge sensitivity threshold for division marker segmentation [0-1]
        
        %> ROF denoising parameter theta for division marker.
        %> (Lineage reconstruction)
        %>
        %> ROF de-noising parameter theta used for de-noising fluorescent
        %> images.
        %> optional param
        divisionMarkerDenoising = 25; % ROF denoising parameter theta for division marker.
        
        %> Convex area upper threshold for division marker segmentation.
        %> (Lineage reconstruction)
        %>
        %> Convex area upper threshold used for division marker segmentation
        %> selection to exclude segmentation artefacts. Only segmented
        %> marker object smaller than this treshold are used for the mother
        %> daughter assignment.
        %> optional param
        divisionMarkerConvexAreaUpperThresh = 100; % Convex area upper threshold for division marker segmentation.
        
        %> Convex area lower threshold for division marker segmentation.
        %> (Lineage reconstruction)
        %>
        %> Convex area lower threshold used for division marker segmentation
        %> selection to exclude segmentation artefacts. Only segmented
        %> marker object larger than this treshold are used for the mother
        %> daughter assignment.
        %> optional param
        divisionMarkerConvexAreaLowerThresh = 5; % Convex area lower threshold for division marker segmentation.
        
        %> Max displacement allowed [px]
        %> (Lineage reconstruction)
        %>
        %> Max displacement of segmented objects in pixel (depends on pixel
        %> size and magnification) allowed in any direction for subsequent
        %> image frames.
        %> optional param
        divisionMarkerMaxObjCenterDisplacement = 15; % Max displacement allowed [px].
        
        %> Max expected (linear) movement [px]
        %> (Lineage reconstruction)
        %>
        %> Max expected (linear) movement of one or multiple segmented
        %> objects as i.e colony jump in pixel (depends on pixel
        %> size and magnification). By default it is 2.5
        %> times the average cell diameter but depends on the actual
        %> largest movement.
        %> optional param
        divisionMarkerMaxExpectedMovement = 75; % Max expected (linear) movement [px].
        
        %> Fractional marker size growth
        %> (Lineage reconstruction)
        %>
        %> Average fractionalgrowth of an identical segmented object between
        %> two subsequent image frames (i.e. cell area or nuclear area)
        %> optional param
        divisionMarkerAverageObjSizeGrowth = 0.03; % Fractional marker size growth.
        
        %> Max allowed fractional marker size decrease
        %> (Lineage reconstruction)
        %>
        %> Penalty for maximum fractional segmented object size decrease for
        %> subsequent image frames;
        %> optional param
        divisionMarkerMaxObjSizeDecrease = 0.75; % Max allowed fractional marker size decrease.
        
        %> Max allowed fractional marker size increase
        %> (Lineage reconstruction)
        %>
        %> Penalty for maximum fractional segmented object size increase for
        %> subsequent image frames;
        %> optional param
        divisionMarkerMaxObjSizeIncrease = 2.5; % Max allowed fractional marker size increase.
        
        %> Major axis rotation  [degrees]
        %> (Lineage reconstruction)
        %>
        %> Max allowed major axis rotation (for round objects such as
        %> S.cerevisiae cells the penalty is not telling and should be set
        %> to 0. The paramater might be important for S.pombe (rod shaped)
        %> where the majoraxis is clearly defined);
        %> optional param
        divisionMarkerMaxMajorAxisRotation = 0; % Major axis rotation  [degrees].
        
        %> Individual function penalty
        %> (Lineage reconstruction)
        %>
        %> Individual penalty maximum for each cost function (e.g. for
        %> position, area, rotation, frame skipping);
        %> optional param
        divisionMarkerIndividualFunctionPenalty = 500; % Individual function penalty.
        
        %> Max number of frames where a segmented object is allowed to be
        %> missing from a track (miss segmentation, out of focus, etc)
        %> [image frames]
        %> (Lineage reconstruction)
        %>
        %> The number of frames where a segmented object is not present;
        %> optional param
        divisionMarkerMaxTrackFrameSkipping = 2; % Max number of frames where a segmented object is allowed to be missing from a track (miss segmentation, out of focus, etc).
        
        %> Logical array to select which of the four cost functions
        %> should be used to build the final cost matrix. [boolean]
        %> (Lineage reconstruction)
        %>
        %> Array specifying which cost functions to consider to build
        %> final cost matrix;
        %> optional param
        divisionMarkerUsedFunctionsForCostMatrix = [1,1,0,1]; % Logical array to select which of the four cost functions should be used to build the final cost matrix. [boolean].
        
        %> Mean cell diameter of all segmented objects in
        %> the experiment [px].
        %> (Lineage reconstruction)
        %>
        %> Mean cell diameter of all segmented objects of all image frames
        %> of the whole experiment;
        %> optional param
        divisionMarkerMeanObjDiameter = 30; % Mean marker diameter of all segmented objects in the experiment [px].
        
        %> Mean cell diameter scaling factor.
        %> (Lineage reconstruction)
        %>
        %> Mean cell diameter scaling factor to scale the max allowed linear
        %> motion of cells in respect to the mean cell diameter.
        %> optional param
        divisionMarkerMeanObjDiameterScalingFactor = 2.5; % Mean marker diameter scaling factor.
        
        %> Half window side length of a square around the centroid
        %> coordinates of a segmented object to be used to for fingerprint
        %> computation [px]
        %> (Lineage reconstruction)
        %>
        %> optional param
        divisionMarkerFingerprintHalfWindowSideLength = 25; % Half window side length of a square around the centroid coordinates of a segmented object to be used to for fingerprint computation [px].
        
        %> Resize factor for fingerprint calculation
        %> (Lineage reconstruction)
        %>
        %> Resize factor applied to image matrix before fingerprint
        %> calculation;
        %> optional param
        divisionMarkerFingerprintResizeFactor = 32; % Resize factor for fingerprint calculation.
        
        %> Number of DCT frequencies to include for fingerprint computation
        %> (Lineage reconstruction)
        %>
        %> Number of DCT frequencies to include for fingerprint computation;
        %> optional param
        divisionMarkerFingerprintMaxConsideredFrequencies = 8; % Number of DCT frequencies to include for fingerprint computation.
        
        %> Radius around a centroid cooridnates of a segmented object used
        %> to detect the neighbouring cells [px]
        %> (Lineage reconstruction)
        %>
        %> All centroid cooridnates of segmented objects within the
        %> neighbourhoodSearchRadius are counted as neigbours of a selected
        %> segmented object;
        %> optional param
        divisionMarkerNeighbourhoodSearchRadius = 40; % Radius around a centroid cooridnates of a segmented object used to detect the neighbouring cells [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in X [px].
        %> (Lineage reconstruction)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in X. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> optional param
        divisionMarkerRadiusVectorFilterX = 100; % Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in X [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in Y [px].
        %> (Lineage reconstruction)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in Y. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> optional param
        divisionMarkerRadiusVectorFilterY = 100; % Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in Y [px].
        
        %> Radius equals the first standard deviation of a 2D gaussion
        %> distribution around a cell center in Z [px].
        %> (Lineage reconstruction)
        %>
        %> The radius is used calculate the neighbourhood weighted cell
        %> center displacement in Z. The larger the radius, the more more
        %> neighbours are used for weighting the vector component;
        %> optional param
        divisionMarkerRadiusVectorFilterZ = 100; %  Radius equals the first standard deviation of a 2D gaussion distribution around a cell center in Z [px].
        
        %> Line profile length.
        %> (Lineage reconstruction)
        %>
        %> Maximum length the line profile between two cell centers are
        %> resampled to. Used to be able to compare the profiles.
        %> optional param
        divisionMarkerLineProfileLength = 100; % Line profile length.
        
        %> Line profile peak lower bound.
        %> (Lineage reconstruction)
        %>
        %> Lower bound where a peak on line profile between two cell
        %> centers is still accepted. Used as threshold.
        %> optional param
        divisionMarkerLineProfilePeakLowerBound = 10; % Line profile peak lower bound.
        
        %> Line profile peak upper bound.
        %> (Lineage reconstruction)
        %>
        %> Upper bound where a peak on line profile between two cell
        %> centers is still accepted. Used as threshold.
        %> optional param
        divisionMarkerLineProfilePeakUpperBound = 90; % Line profile peak upper bound.
        
        %> is data 2D (0) or 3D (1)?
        %> optional param
        data3D = 0; % Flag if data is 3D (1) or 2D (0) [bool].
        
        %> is 3D fingerprint used or closest z stack method
        %> optional param
        active3DFingerprint = 0; % Flag, if is 3D fingerprint used or closest z stack method.
        
        %> Thumbnail side length around the center of a segmented object
        %> (i.e a cell) [px]
        %> (Tracker output generation)
        %>
        %> Used for example to cut out all cells contained in a single track
        %> from all image frames for visualization;
        %> optional param
        cellThumbSideLength = 100; % Thumbnail side length around the center of a segmented object (i.e a cell) [px].
        
        %> Trasparency (alpha) level for segmentation mask overlay on raw
        %> images [0-1]
        %> (Tracker output generation)
        %>
        %> Transparency (alpha) level for segmentation mask overlay on raw
        %> images for visualization;
        %> optional param
        maskOverlayAlpha = 0.4; % Trasparency (alpha) level for segmentation mask overlay on raw images [0-1]
        
    end
    
    methods
        
        % Constructor
        %       function obj = ParameterConfiguration
        %       end
        
        function checkParameterConfiguration(this)
            % CHECKPARAMETERCONFIGURATION Checks if all values in the
            % ParameterConfiguration object are defined.
            %
            % Args:
            %    this (:obj:`object`):    :class:`ParameterConfiguration` instance
            %
            % Returns
            % -------
            %    void :obj:`-`                                                     
            
            TracX.Utils.printToConsole('CHECK: Parameter configuration...')
            %  check for matlab>2009, assignment of ~
            if verLessThan('matlab', '7.9')
                error(['TracX requires MATLAB 7.9 or higher. You can use the' ...
                    'deployed version and the MATLAB Compiler Runtime (MCR) if' ...
                    'your MATALB version is too old']);
            end
            
            %  check defined
            if( isempty(this.maxCellCenterDisplacement) )
                error('Undefined value for displacement penalty');
            end
            
            if(isempty(this.maxExpectedMovement) )
                error('Undefined value for maximmum expected movement');
            end
            
            if( isempty(this.maxMajorAxisRotation) )
                error('Undefined value for major axis rotation penalty');
            end
            
            if( isempty(this.averageCellSizeGrowth) )
                error('Undefined value for cell size growth penalty');
            end
            
            if( isempty(this.maxTrackFrameSkipping) )
                error('Undefined value for maxTrackFrameSkipping');
            end
            
            if( isempty(this.fingerprintHalfWindowSideLength) )
                error('Undefined value for fingerprint window side length');
            end
            
            if( isempty(this.neighbourhoodSearchRadius) )
                error('Undefined value for neighbourhood window side length');
            end
            
            if( isempty(this.radiusVectorFilterX) )
                error('Undefined value for radius vector filter in x');
            end
            
            if( isempty(this.radiusVectorFilterY) )
                error('Undefined value for radius vector filter in y');
            end
            
            if( isempty(this.cellThumbSideLength) )
                error('Undefined value for cell thumb side length');
            end
            
            if( isempty(this.maskOverlayAlpha) )
                error('Undefined value for mask overlay alpha');
            end
            
            if( isempty(this.divisionMarkerEdgeSensitivityThresh) )
                error('Undefined value for division marker edge sensitivity threshold');
            end
            
            if( isempty(this.divisionMarkerConvexAreaUpperThresh) )
                error('Undefined value for division marker convex area upper thrshold');
            end
            
            if( isempty(this.divisionMarkerConvexAreaLowerThresh) )
                error('Undefined value for division marker convex area lower thrshold');
            end
            
            if( isempty(this.meanCellDiameterScalingFactor) )
                error('Undefined value for mean cell diameter scaling factor');
            end
            
            if( isempty(this.divisionMarkerDenoising) )
                error('Undefined value for division marker ROF denoising parameter');
            end
            
            if( isempty(this.divisionMarkerMaxObjCenterDisplacement) )
                error('Undefined value for division marker max object center displacement');
            end
            
            if( isempty(this.divisionMarkerMaxExpectedMovement) )
                error('Undefined value for division marker max expected movement');
            end
            
            if( isempty(this.divisionMarkerAverageObjSizeGrowth) )
                error('Undefined value for division marker average object size growth');
            end
            
            if( isempty(this.divisionMarkerMaxObjSizeDecrease) )
                error('Undefined value for division marker max  object size decrease');
            end
            
            if( isempty(this.divisionMarkerMaxObjSizeIncrease) )
                error('Undefined value for division marker max  object size increase');
            end
            
            if( isempty(this.divisionMarkerMaxTrackFrameSkipping) )
                error('Undefined value for division marker track frame skipping');
            end
            
            if( isempty(this.divisionMarkerMaxMajorAxisRotation) )
                error('Undefined value for division marker max major axis rotation');
            end
            
            if( isempty(this.divisionMarkerIndividualFunctionPenalty) )
                error('Undefined value for division marker individual function penalty');
            end
            
            if( isempty(this.divisionMarkerUsedFunctionsForCostMatrix) )
                error('Undefined value for division marker used functions for cost matrix');
            end
            
            if( isempty(this.divisionMarkerMeanObjDiameter) )
                error('Undefined value for division marker mean object diameter');
            end
            
            if( isempty(this.divisionMarkerMeanObjDiameterScalingFactor) )
                error('Undefined value for division marker mean object diameter scaling factor');
            end
            
            if( isempty(this.divisionMarkerFingerprintHalfWindowSideLength) )
                error('Undefined value for division marker fingerprint half window side length');
            end
            
            if( isempty(this.divisionMarkerFingerprintResizeFactor) )
                error('Undefined value for division marker fingerprint resize factor');
            end
            
            if( isempty(this.divisionMarkerFingerprintMaxConsideredFrequencies) )
                error('Undefined value for division marker max considered frequencies');
            end
            
            if( isempty(this.divisionMarkerNeighbourhoodSearchRadius) )
                error('Undefined value for division marker neighbourhood search radius');
            end
            
            if( isempty(this.divisionMarkerRadiusVectorFilterX) )
                error('Undefined value for division marker radius vector filter x');
            end
            
            if( isempty(this.divisionMarkerRadiusVectorFilterY) )
                error('Undefined value for division marker radius vector filter y');
            end
            
            if( isempty(this.divisionMarkerRadiusVectorFilterZ) )
                error('Undefined value for division marker radius vector filter z');
            end
            
            if( isempty(this.divisionMarkerLineProfileLength) )
                error('Undefined value for line profile length parameter');
            end
            
            if( isempty(this.divisionMarkerLineProfilePeakLowerBound) )
                error('Undefined value for line profile peak lower bound');
            end
            
            if( isempty(this.divisionMarkerLineProfilePeakUpperBound) )
                error('Undefined value for line profile peak upper bound');
            end
            
            if( isempty(this.data3D) )
                error('Undefined value for data dimensionality (2D or 3D)');
            end
            
            if( isempty(this.active3DFingerprint) )
                error('Undefined value for active 3D fingerprinting method');
            end
            
            if( isempty(this.pixelsPerZPlaneInterval) )
                error('Undefined value for pixels per Z-Plane interval');
            end
            
            % check ranges
            %> @todo Add proper check for all parameters if the are in the
            %>       correct range.
            if( this.maxTrackFrameSkipping < 0 )
                error('Maxjump (%d) cannot be less than 0', this.maxTrackFrameSkipping);
            end
            
            TracX.Utils.printToConsole(['CHECK: Parameter configuration ' ...
                'check passed!'])
        end
        
        function setMaxCellCenterDisplacement(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max allowed segmentation
            % object centroid displacement [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for max allowed 
            %                                segmentation object centroid 
            %                                displacement [pixel].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxCellCenterDisplacement = value;
        end
        
        function setMaxExpectedMovement(this, value)
            % SETMAXEXPECTEDMOVEMENT Sets value for max expected movement of a /
            % multiple segmentation object(s) [pixel].
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for max expected movement
            %                                of a / multiple segmentation 
            %                                object(s) [pixel].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxExpectedMovement = value;
        end
        
        function setMaxMajorAxisRotation(this, value)
            % SETMAXMAJORAXISROTATION Sets value for majoraxis rotation 
            % penalty [degrees].
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for majoraxis rotation 
            %                                penalty [degrees].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxMajorAxisRotation = value;
        end
        
        function setAverageCellSizeGrowth(this, value)
            % SETAVERAGECELLSIZEGROWTH Sets value for max allowed fractional
            % cell size growth (of the segmented object).
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for max allowed 
            %                                fractional cell size growth 
            %                                (of the segmented object).
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.averageCellSizeGrowth = value;
        end
        
        function setMaxCellSizeDecrease(this, value)
            % SETMAXCELLSIZEDECREASE Sets value for max allowed fractional 
            % cell size decrease due to segmenation variation (e.g. miss 
            % segmentation, focus drift of the segmented object).
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for max allowed 
            %                                fractional cell size decrease 
            %                                due to segmenation variation.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxCellSizeDecrease = value;
        end
        
        function setMaxCellSizeIncrease(this, value)
            % SETMAXCELLSIZEINCREASE Sets value for max allowed fractional 
            % cell size increase due to segmenation variation (e.g. miss 
            % segmentation, focus drift of the segmented object).
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`):       Sets value for max allowed 
            %                                fractional cell size increase 
            %                                due to segmenation variation.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxCellSizeIncrease = value;
        end
        
        %> @param this:  ParameterConfiguration object
        %> @param value: Sets value for all individaul function penalty
        function setIndividualFunctionPenalty(this, value)
            % SETINDIVIDUALFUNCTIONPENALTY Sets value for all individaul 
            % function penalty
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`int`):         Sets value for all individaul 
            %                                function penalty
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.individualFunctionPenalty = value;
        end
        
        function setMaxTrackFrameSkipping(this, value)
            % SETMAXTRACKFRAMESKIPPING Sets value for max allowed frame skip.
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`int`):         Sets value for max allowed frame skip.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maxTrackFrameSkipping = value;
        end
        
        function setUsedFunctionsForCostMatrix(this, value)
            % SETUSEDFUNCTIONSFORCOSTMATRIX Sets value for the selection of 
            % which of the four cost functions should be used to build the 
            % final cost matrix. 1x4 array [boolean]
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`array`, 1x4):  Sets value for the selection of 
            %                                which of the four cost functions
            %                                should be used.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            assert(all(size(value) == [1, 4]), ...
                'Array has to be of size (1,4). Values allowed are 0 and 1 only.')
            if iscell(value)
                this.usedFunctionsForCostMatrix = cell2mat(value);
            else
                this.usedFunctionsForCostMatrix = value;
            end
        end
        
        function setMeanCellDiameter(this, value)
            % SETMEANCELLDIAMETER Sets value for the mean segmentation object 
            % (cell) diameter of the experiment in pixel  [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`float`):      Value for mean segmentation object 
            %                               (cell) diameter of the experiment 
            %                               in pixel  [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.meanCellDiameter = value;
        end
        
        function setMeanCellDiameterScalingFactor(this, value)
            % SETFINGERPRINTHALFWINDOWSIDELENGTH Sets the mean cell diameter 
            % scaling factor.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`float`):      Value for mean cell diameter 
            %                               scaling factor.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.meanCellDiameterScalingFactor = value;
        end
        
        function setFingerprintHalfWindowSideLength(this, value)
            % SETFINGERPRINTHALFWINDOWSIDELENGTH Sets value for the half window side
            % length of a square around the centroid coordinates of a 
            % segmented cell to be used to for fingerprint computation [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the half window side
            %                               length of the cell center.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.fingerprintHalfWindowSideLength = value;
        end
        
        function setFingerprintResizeFactor(this, value)
            % SETFINGERPRINTRESIZEFACTOR Sets value for the
            % image resizing for fingerprint calculation Default is 32.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the image resizing for 
            %                               fingerprint calculation Default is 32.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.fingerprintResizeFactor = value;
        end
        
        function setFingerprintMaxConsideredFrequencies(this, value)
            % SETFINGERPRINTMAXCONSIDEREDFREQUENCIES Sets value for the
            % maximal DCT frequency to be used to calculate the fingerprint.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the maximal DCT frequency
            %                               to be used to calculate the fingerprint.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.fingerprintMaxConsideredFrequencies = value;
        end
        
        function setFingerprintDistThreshold(this, value)
            % SETFINGERPRINTDISTTHRESHOLD Sets value for the 
            % fingerprint distance threshold.
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`float`):      Value for the fingerprint 
            %                               distance threshold.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.fingerprintDistThreshold = value;
        end
        
        function setNeighbourhoodSearchRadius(this, value)
            % SETNEIGHBOURHOODSEARCHRADIUS Sets value for the neigbourhood half
            % window side length [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the neigbourhood half
            %                               window side length [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.neighbourhoodSearchRadius = value;
        end
        
        function setRadiusVectorFilterX(this, value)
            % SETRADIUSVECTORFILTERX Sets value for the radius of the vector
            % filter in x [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the radius of the vector
            %                               filter in x [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.radiusVectorFilterX = value;
        end
        
        function setRadiusVectorFilterY(this, value)
            % SETRADIUSVECTORFILTERY Sets value for the radius of the vector
            % filter in z [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the radius of the vector
            %                               filter in y [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.radiusVectorFilterY = value;
        end
        
        function setRadiusVectorFilterZ(this, value)
            % SETRADIUSVECTORFILTERZ Sets value for the radius of the vector
            % filter in z [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the radius of the vector
            %                               filter in z [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.radiusVectorFilterZ = value;
        end
        
        function setCellThumbSideLength(this, value)
            % SETCELLTHUMBSIDELENGTH Sets value for the side length of the image crop
            % around the cell center for single cell trace generation [px].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`int`):        Value for the side length of the
            %                               image crop around the cell center 
            %                               for single cell trace generation [px].
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.cellThumbSideLength = value;
        end
        
        function setMaskOverlayAlpha(this, value)
            % SETMASKOVERLAYALPHA Sets value for the transparency (alpha) level of
            % the segmentation mask overlay onto the raw images [0-1].
            %
            % Args:
            %    this (:obj:`object`):      :class:`ParameterConfiguration`
            %                               instance.
            %    value (:obj:`float`, 0-1): Value for the transparency (alpha) level of
            %                               the segmentation mask overlay onto the
            %                               raw images.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.maskOverlayAlpha = value;
        end
        
        function setDivisionMarkerEdgeSensitivityThresh(this, value)
            % SETDIVISIONMARKEREDGESENSITIVITYTHRESH Sets value for the edge 
            % sensitivity threshold for division marker segmentation [0-1]
            %
            % Args:
            %    this (:obj:`object`):       :class:`ParameterConfiguration`
            %                                instance.
            %    value (:obj:`float`, 0-1):  Value for the edge sensitivity threshold
            %                                for division marker segmentation
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerEdgeSensitivityThresh = value;
        end
        
        function setDivisionMarkerDenoising(this, value)
            % SETDIVISIONMARKERCONVEXAREAUPPERTHRESH Sets value for the ROF 
            % denoising parameter theta for division marker.
            %
            % Args:
            %    this (:obj:`object`):   :class:`ParameterConfiguration`
            %                            instance.
            %    value (:obj:`float`):   Value for the ROF denoising
            %                            parameter theta for division marker.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerDenoising = value;
        end
        
        function setDivisionMarkerConvexAreaUpperThresh(this, value)
            % SETDIVISIONMARKERCONVEXAREAUPPERTHRESH Sets value for the
            % convex area upper thrshold for  division marker segmentation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the convex area upper threshold
            %                          for  division marker segmentation
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerConvexAreaUpperThresh = value;
        end
        

        function setDivisionMarkerConvexAreaLowerThresh(this, value)
            % SETDIVISIONMARKERCONVEXAREALOWERTHRESH Sets value for the
            % convex area lower thrshold for  division marker segmentation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the convex area lower threshold
            %                          for  division marker segmentation
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerConvexAreaLowerThresh = value;
        end
        
        function setDivisionMarkerLineProfileLength(this, value)
            % SETDIVISIONMARKERLINEPROFILEPEAKUPPERBOUND Sets value for the
            % line profile length for line profile comparison for different 
            % cell pairs.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the line profile length.
            %                          
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerLineProfileLength = value;
        end
        
        function setDivisionMarkerLineProfilePeakUpperBound(this, value)
            % SETDIVISIONMARKERLINEPROFILEPEAKUPPERBOUND Sets value for the
            % line profile peak upper bound for thresholding the detecatble
            % peak region.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the line profile
            %                          peak upper bound
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerLineProfilePeakUpperBound = value;
        end
        
        function setDivisionMarkerLineProfilePeakLowerBound(this, value)
            % SETDIVISIONMARKERLINEPROFILEPEAKLOWERBOUND Sets value for the
            % line profile peak lower bound for thresholding the detecatble 
            % peak region.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the line profile
            %                          peak lower bound
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerLineProfilePeakLowerBound = value;
        end
        
        function setDivisionMarkerMaxObjCenterDisplacement(this, value)
            % SETDIVISIONMARKERMAXOBJCENTERDISPLACEMENT Sets value for the
            % divisison marker max object center displacement.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          max object center displacement.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerMaxObjCenterDisplacement = value;
        end
        
        function setDivisionMarkerMaxExpectedMovement(this, value)
            % SETDIVISIONMARKERMAXEXPECTEDMOVEMENT Sets value for the
            % divisison marker max object moviement.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          max object moviement.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerMaxExpectedMovement = value;
        end
        
        function setDivisionMarkerAverageObjSizeGrowth(this, value)
            % SETDIVISIONMARKERAVERAGEOBJSIZEGROWTH Sets value for the
            % divisison marker max object size growth.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          average object size growth.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerAverageObjSizeGrowth = value;
        end
        
        function setDivisionMarkerMaxObjSizeDecrease(this, value)
            % SETDIVISIONMARKERMAXOBJSIZEDECREASE Sets value for the
            % divisison marker max object size decrease.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          max object size decrease.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerMaxObjSizeDecrease = value;
        end
        
        function setDivisionMarkerMaxObjSizeIncrease(this, value)
            % SETDIVISIONMARKERMAXOBJSIZEINCREASE Sets value for the
            % divisison marker max object size increase.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          max object size increase.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerMaxObjSizeIncrease = value;
        end
        
        function setDivisionMarkerMaxTrackFrameSkipping(this, value)
            % SETDIVISIONMARKERMAXTRACKFRAMESKIPPING Sets value for the 
            % divisison marker max track frame skipping.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker 
            %                          max track frame skipping.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerMaxTrackFrameSkipping = value;
        end
        
        function setDivisionMarkerMaxMajorAxisRotation(this, value)
            % SETDIVISIONMARKERMAXMAJORAXISROTATION Sets value for the 
            % divisison marker max major axis rotation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker 
            %                          max major axis rotation.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerMaxMajorAxisRotation = value;
        end
        
        function setDivisionMarkerIndividualFunctionPenalty(this, value)
            % SETDIVISIONMARKERINDIVIDUALFUNCTIONPENALTY Sets value
            % for the divisison marker individual function penalty.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker 
            %                          individual function penalty.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerIndividualFunctionPenalty = value;
        end
        
        function setDivisionMarkerUsedFunctionsForCostMatrix(this, value)
            % SETDIVISIONMARKERUSEDFUNCTIONSFORCOSTMATRIX Sets value
            % for the divisison marker used functions for cost matrix.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker 
            %                          used functions for cost matrix.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            assert(all(size(value) == [1, 4]), ...
                'Array has to be of size (1,4). Values allowed are 0 and 1 only.')
            if iscell(value)
                this.divisionMarkerUsedFunctionsForCostMatrix = cell2mat(value);
            else
                this.divisionMarkerUsedFunctionsForCostMatrix = value;
            end
        end
        
        function setDivisionMarkerMeanObjDiameter(this, value)
            % SETDIVISIONMARKERMEANOBJDIAMETER Sets value
            % for the divisison marker mean object diameter.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker mean
            %                          object diameter
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerMeanObjDiameter = value;
        end
        
        function setDivisionMarkerMeanObjDiameterScalingFactor(this, value)
            % SETDIVISIONMARKERMEANOBJDIAMETERSCALINGFACTOR Sets value
            % for the divisison marker mean object scaling factor.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`):   Value for the divisison marker mean 
            %                          object scaling factor
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerMeanObjDiameterScalingFactor = value;
        end
        
        function setDivisionMarkerFingerprintHalfWindowSideLength(this, value)
            % SETDIVISIONMARKERFINGERPRINTHALFWINDOWSIDELENGTH Sets value
            % for the divisison half window side length.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker fingerprint
            %                          half window side length.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerFingerprintHalfWindowSideLength = value;
        end
        
        function setDivisionMarkerFingerprintResizeFactor(this, value)
            % SETDIVISIONMARKERFINGERPRINTRESIZEFACTOR Sets value
            % for the divisison marker fingerprint resize factor.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker fingerprint
            %                          resize factor.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerFingerprintResizeFactor = value;
        end
        
        function setDivisionMarkerFingerprintMaxConsideredFrequencies(this, value)
            % SETDIVISIONMARKERFINGERPRINTMAXCONSIDEREDFREQUENCIES Sets value 
            % for the divisison marker fingerprint max considered frequencies.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker fingerprint
            %                          max considered frequencies.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerFingerprintMaxConsideredFrequencies = value;
        end
        
        function setDivisionMarkerNeighbourhoodSearchRadius(this, value)
            % SETDIVISIONMARKERNEIGHBOURHOODSEARCHRADIUS Sets value for the 
            % divisison marker neighbourhood search radius.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker
            %                          neighbourhood search radius.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.divisionMarkerNeighbourhoodSearchRadius = value;
        end
        
        function setDivisionMarkerRadiusVectorFilterX(this, value)
            % SETDIVISIONMARKERRADIUSVECTORFILTERX Sets value for the
            % divisison marker radius vector filter x.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker radius
            %                          vector filter x.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerRadiusVectorFilterX = value;
        end
        
        function setDivisionMarkerRadiusVectorFilterY(this, value)
            % SETDIVISIONMARKERRADIUSVECTORFILTERY Sets value for the
            % divisison marker radius vector filter y.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker radius
            %                          vector filter y.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerRadiusVectorFilterY = value;
        end
        
        function setDivisionMarkerRadiusVectorFilterZ(this, value)
            % SETDIVISIONMARKERRADIUSVECTORFILTERZ Sets value for the
            % divisison marker radius vector filter z.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the divisison marker radius
            %                          vector filter z.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.divisionMarkerRadiusVectorFilterZ = value;
        end
        
        function setDebugLevel(this, value)
            % SETDEBUGLEVEL Sets value for the debug level.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`int`):   Value for the debug level.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.debugLevel = value;
        end
        
        function setData3D(this, value)
            % SETDATA3D Sets value if data is three dimensional.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`bool`):  Value if data is three dimensional.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.data3D = value;
        end
        
        function setActive3DFingerprint(this, value)
            % SETACTIVE3DFINGERPRINT Sets value if 3D fingerprint should
            % be activated.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`bool`):  Value if 3D fingerprint should be activated.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.active3DFingerprint = value;
        end
        
        function setPixelsPerZPlaneInterval(this, value)
            % SETPIXELSPERZPLANEINTERVAL Sets value for pixels per Z plane
            % interval / step.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %    value (:obj:`float`): Value for pixels per Z plane interval / step.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            this.pixelsPerZPlaneInterval = value;
        end
        
    end
    
    methods (Access = protected)
        function header = getHeader(obj)
            % GETHEADER Returns header the object properties string
            % represntation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            if ~isscalar(obj)
                header = getHeader@matlab.mixin.CustomDisplay(obj);
            else
                className = matlab.mixin.CustomDisplay.getClassNameForHeader(obj);
                newHeader = [className, ': '];
                header = sprintf('%s\n',newHeader);
            end
        end
        
        function propgrp = getPropertyGroups(obj)
            % GETPROPERTYGROUPS Returns groups the object properties string
            % represntation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            if ~isscalar(obj)
                propList = {'debugLevel','data3D'};
                propgrp = matlab.mixin.util.PropertyGroup(propList);
            else
                gTitle1 = 'General parameter:';
                gTitle2 = 'Tracking core parameter:';
                gTitle3 = 'Lineage reconstruction parameter:';
                propList1 = {'debugLevel','data3D', 'cellThumbSideLength', ...
                    'maskOverlayAlpha'};
                propList2 = {'maxCellCenterDisplacement','maxExpectedMovement', ...
                    'averageCellSizeGrowth', 'maxCellSizeDecrease', 'maxCellSizeIncrease', ...
                    'maxTrackFrameSkipping',...
                    'maxMajorAxisRotation', 'individualFunctionPenalty', ...
                    'usedFunctionsForCostMatrix', ...
                    'meanCellDiameter', 'meanCellDiameterScalingFactor', ...
                    'fingerprintDistThreshold', 'fingerprintHalfWindowSideLength', ...
                    'fingerprintResizeFactor', 'fingerprintMaxConsideredFrequencies', ...
                    'neighbourhoodSearchRadius', 'radiusVectorFilterX', ...
                    'radiusVectorFilterY', 'radiusVectorFilterZ', };
                propList3 = {'divisionMarkerEdgeSensitivityThresh','divisionMarkerDenoising', ...
                    'divisionMarkerConvexAreaUpperThresh', 'divisionMarkerConvexAreaLowerThresh', ...
                    'divisionMarkerLineProfileLength', 'divisionMarkerLineProfilePeakLowerBound', 'divisionMarkerLineProfilePeakUpperBound', ...
                    'divisionMarkerMaxObjCenterDisplacement','divisionMarkerMaxExpectedMovement', ...
                    'divisionMarkerAverageObjSizeGrowth', 'divisionMarkerMaxObjSizeDecrease', ...
                    'divisionMarkerMaxObjSizeIncrease', 'divisionMarkerMaxTrackFrameSkipping', ...
                    'divisionMarkerMaxMajorAxisRotation', 'divisionMarkerIndividualFunctionPenalty', ...
                    'divisionMarkerUsedFunctionsForCostMatrix', 'divisionMarkerMeanObjDiameter', ...
                    'divisionMarkerMeanObjDiameterScalingFactor', ...
                    'divisionMarkerFingerprintHalfWindowSideLength', ...
                    'divisionMarkerFingerprintResizeFactor', ...
                    'divisionMarkerFingerprintMaxConsideredFrequencies', ...
                    'divisionMarkerNeighbourhoodSearchRadius', ...
                    'divisionMarkerRadiusVectorFilterX', ...
                    'divisionMarkerRadiusVectorFilterY', ...
                    'divisionMarkerRadiusVectorFilterZ'};
                propgrp(1) = matlab.mixin.util.PropertyGroup(propList1,gTitle1);
                propgrp(2) = matlab.mixin.util.PropertyGroup(propList2,gTitle2);
                propgrp(3) = matlab.mixin.util.PropertyGroup(propList3,gTitle3);
            end
        end
        
        function footer = getFooter(obj)
            % GETFOOTER Returns the footer of the object string
            % represntation.
            %
            % Args:
            %    this (:obj:`object`): :class:`ParameterConfiguration`
            %                          instance.
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            if isscalar(obj)
                footer = sprintf('\n TracX version %g.%g.%g | %s', ...
                    obj.major, obj.minor, obj.patch, computer('arch'));
            else
                footer = '';
            end
        end
    end
end