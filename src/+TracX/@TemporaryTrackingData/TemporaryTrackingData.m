%>  @todo Refractor CurrentTrackingData TemporaryTrackingData
%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TemporaryTrackingData < handle
    % TEMPORARYTRACKINGDATA Creates an object of minimal required
    % information needed for frame to frame tracking.
    %
    % :Authors:
            %    Andreas P. Cuny - initial implementation
    
    properties
        
        track_index
        %  Track index of segmented cell.
        track_index_qc
        %  Track index qc validated of segmented cell.
        track_age
        %  Track age segmented cell.
        cell_index
        %  Segmentation cell index of segmented cell.
        cell_center_x
        %  X component of segmented cell.
        cell_center_y
        %  Y component of segmented cell.
        cell_center_z
        %  Z component of segmented cell.
        cell_dif_x
        %  X component frame to frame difference of segmented cell.
        cell_dif_y
        %  Y component frame to frame difference of segmented cell.
        cell_dif_z
        %  Z component frame to frame difference of segmented cell.
        cell_filtered_dif_x
        %  Filtered x component frame to frame difference of segmented cell.
        cell_filtered_dif_y
        %  Filtered y component frame to frame difference of segmented cell.
        cell_filtered_dif_z
        %  Filtered z component frame to frame difference of segmented cell.
        cell_orientation
        %  Orientation of segmented cell.
        cell_area
        %  Area of segmented cell.
        cell_close_neighbour
        %  Array of closest cell neighbours of segmented cell.
        cell_del
        %  Deletion of segmented cell.
        track_fingerprint_real
        %  Track real frequency based fingerprint of segmented cell.
        track_fingerprint_real_distance
        %  Track fingerprint distance of segmented cell based on track_fingerprint_real.
        track_assignment_fraction
        %  Fraction of neighbouring cell with lower track_fingerprint_real_distance
        track_fingerprint_age
        %  Track fingerprint age
    end
    
    methods
        
        % Constructor
        function this = TemporaryTrackingData()
            % TEMPORARYTRACKINGDATA constructs an object of type
            % TracX.TemporaryTrackingData. This object acts as a temporary data
            % storage for relevant data needed during the frame to frame
            % tracking process for a TracX Project.         
            %
            % Returns
            % -------
            %    this (:obj:`object`)
            %                                       :class:`TemporaryTrackingData`
            %                                       instance.
            % :Authors:
            %    Andreas P. Cuny - initial implementation
        end
        
        function this = addData(this, track_index, track_index_qc, ...
                track_age, cell_index, cell_center_x, ...
                cell_center_y, cell_center_z, cell_dif_x, cell_dif_y, cell_dif_z, cell_filtered_dif_x, ...
                cell_filtered_dif_y, cell_filtered_dif_z, cell_orientation, cell_area, ...
                cell_close_neighbour, cell_del, ...
                track_fingerprint_real, track_fingerprint_real_distance, ...
                track_assignment_fraction, track_fingerprint_age)
            % ADDDATA Adds data to TracX.TemporaryTrackingData object.
            %
            % Args:
            %    this (:obj:`object`):                     :class:`TemporaryTrackingData`
            %                                              instance.
            %    track_index (:obj:`int`, kx1):            Unique track index arrau
            %    track_index_qc (:obj:`int`, kx1):         Unique qc verified track index
            %                                              array
            %    track_age (:obj:`int`, kx1):              Track age arrau
            %    cell_index (:obj:`int`, kx1):             Unique segmentation cell index
            %                                              for each frame array
            %    cell_center_x (:obj:`double`, kx1):       X coordinate array
            %    cell_center_y (:obj:`double`, kx1):       Y coordinate array
            %    cell_center_z (:obj:`double`, kx1):       Z coordinate array
            %    cell_dif_x (:obj:`double`, kx1):          Frame to frame difference
            %                                              of X coordinates array
            %    cell_dif_y (:obj:`double`, kx1):          Frame to frame difference of
            %                                              Y coordinates array
            %    cell_dif_z (:obj:`double`, kx1):          Frame to frame difference of
            %                                              Z coordinates array
            %    cell_filtered_dif_x (:obj:`double`, kx1): Filtered frame to frame
            %                                              differences of X coordinates
            %                                              array
            %    cell_filtered_dif_y (:obj:`double`, kx1): Filtered frame to frame
            %                                              differences of Y coordinates
            %                                              array
            %    cell_filtered_dif_z (:obj:`double`, kx1): Filtered frame to frame
            %                                              differences of Z coordinates
            %                                              array
            %    cell_orientation (:obj:`double`, kx1):    Orienation
            %    cell_area (:obj:`double`, kx1):           Area
            %    cell_close_neighbour (:obj:`bool`, kxk):  Matrix of closest cell
            %                                              neighbours
            %    cell_del (:obj:`int`, kx1):               Cell deletion
            %    track_fingerprint_real (:obj:`double`, kx1):   Track fingerprint real
            %    track_fingerprint_real_distance (:obj:`double`, kx1):   Track fingerprint
            %                                                            real distance
            %    track_assignment_fraction (:obj:`double`, kx1): Fraction of neighbouring
            %                                                    cell with lower
            %                                                    track_fingerprint_real_distance
            %    track_fingerprint_age (:obj:`int`, kx1):  Track fingerprint age
            %
            % Returns
            % -------
            %    this (:obj:`object`)
            %                                               Returns TracX.TemporaryTrackingData
            
            assert(nargin == 22, 'Number of arguments does not match!')
            % Add data
            this.track_index = track_index;
            this.track_index_qc = track_index_qc;
            this.track_age = track_age;
            this.cell_index = cell_index;
            this.cell_center_x = cell_center_x;
            this.cell_center_y = cell_center_y;
            this.cell_center_z = cell_center_z;
            this.cell_dif_x = cell_dif_x;
            this.cell_dif_y = cell_dif_y;
            this.cell_dif_z = cell_dif_z;
            this.cell_filtered_dif_x = cell_filtered_dif_x;
            this.cell_filtered_dif_y = cell_filtered_dif_y;
            this.cell_filtered_dif_z = cell_filtered_dif_z;
            this.cell_orientation = cell_orientation;
            this.cell_area = cell_area;
            this.cell_close_neighbour = cell_close_neighbour;
            this.cell_del = cell_del;
            this.track_fingerprint_real = track_fingerprint_real;
            this.track_fingerprint_real_distance = ...
                track_fingerprint_real_distance;
            this.track_assignment_fraction = track_assignment_fraction;
            this.track_fingerprint_age = track_fingerprint_age;
            
        end
        
        function recalculateMotionCorrNeighbourhood(this, ...
                neighbourhoodSearchRadius)
            % RECALCULATEMOTIONCORRNEIGHBOURHOOD Recalculates the motion.
            % corrected segmentation neighbourhood.
            %
            % Args:
            %    this (:obj:`object`):                      :class:`TemporaryTrackingData`
            %                                               instance. 
            %    neighbourhoodSearchRadius (:obj:`double`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.neighbourhoodSearchRadius` 
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            xo = this.cell_center_x;
            yo = this.cell_center_y;
            xof = this.cell_filtered_dif_x;
            xof(isnan(xof)) = 0;
            yof = this.cell_filtered_dif_y;
            yof(isnan(yof)) = 0;
            xn = this.cell_center_x;
            yn = this.cell_center_y;
            
            distM = pdist2([xo+xof, yo+yof], [xn, yn],  'euclidean');
            distLocM = distM < neighbourhoodSearchRadius;
            this.cell_close_neighbour = distLocM;
        end
    end
end