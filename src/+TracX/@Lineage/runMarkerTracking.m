%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = runMarkerTracking(this, varargin)
% RUNMARKERTRACKING Runs the tracker algorithm for cell cycle marker
% channels. To track i.e a bud neck marker.
%
% Args:
%    this (:obj:`object`):                     :class:`Lineage` instance  
%    varargin (:obj:`str varchar`):
%
%        * **divisionMarkerMaxTrackFrameSkipping** (:obj:`int`):           Max frame skipping for segmented division marker.
%        * **divisionMarkerMaxObjCenterDisplacement** (:obj:`float`):      Max displacement for segmented division marker. 
%        * **divisionMarkerMaxObjSizeIncrease** (:obj:`float`):            Max object size increase for segmented division marker.
%        * **divisionMarkerFingerprintHalfWindowSideLength** (:obj:`int`): Fingerprint half window side length.
%        * **qc** (:obj:`str`):                                            Flag, if quality control images should be written. Defaults to false.
%
%
% Returns
% -------
%    void :obj:`-`                                
%                                             
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultMaxTrackFrameSkipping = 2;
defaultMaxAllowedMarkerCenterDisplacement = mean(this.lineageData.obj_major_axis);
defaultMaxMarkerSizeIncrease = 4;
defaultMaxMarkerSizeDecrease = 0.6;
defaultFingerprintHalfWindowSideLength = 10;
defaultQc = false;
validParam = @(x) assert(isnumeric(x) && isscalar(x) && (x > 0));
p = inputParser;
p.KeepUnmatched = true;
addOptional(p,'divisionMarkerMaxTrackFrameSkipping', defaultMaxTrackFrameSkipping, validParam);
addOptional(p,'divisionMarkerMaxObjCenterDisplacement', defaultMaxAllowedMarkerCenterDisplacement, validParam);
addOptional(p,'divisionMarkerMaxObjSizeIncrease',defaultMaxMarkerSizeIncrease, validParam);
addOptional(p,'divisionMarkerMaxObjSizeDecrease',defaultMaxMarkerSizeDecrease, validParam);
addOptional(p,'divisionMarkerFingerprintHalfWindowSideLength',defaultFingerprintHalfWindowSideLength, ...
    validParam);
addOptional(p,'qc',defaultQc, @isbool);
parse(p, varargin{:});

qc = p.Results.qc;

if any(strcmp(varargin, 'divisionMarkerMaxTrackFrameSkipping'))
    this.configuration.ParameterConfiguration.setDivisionMarkerMaxTrackFrameSkipping(p.Results.divisionMarkerMaxTrackFrameSkipping);
elseif any(strcmp(varargin, 'divisionMarkerMaxObjCenterDisplacement'))
    this.configuration.ParameterConfiguration.setDivisionMarkerMaxObjCenterDisplacement(p.Results.divisionMarkerMaxObjCenterDisplacement);
elseif any(strcmp(varargin, 'divisionMarkerMaxObjSizeIncrease'))
    this.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeIncrease(p.Results.divisionMarkerMaxObjSizeIncrease);
elseif any(strcmp(varargin, 'divisionMarkerMaxObjSizeDecrease'))
    this.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeDecrease(p.Results.divisionMarkerMaxObjSizeDecrease);
elseif any(strcmp(varargin, 'divisionMarkerFingerprintHalfWindowSideLength'))
    this.configuration.ParameterConfiguration.setDivisionMarkerFingerprintHalfWindowSideLength(p.Results.divisionMarkerFingerprintHalfWindowSideLength);
else
end

ofl = length(this.lineageData.getFieldArrayForFrame('obj_index', ...
    this.configuration.ProjectConfiguration. ...
    trackerStartFrame ));
this.lineageData.numberOfTracks = ofl;

% Set start and length of tracking
uniqueFrames = unique(this.data.SegmentationData.cell_frame);
NuniqueFrames = this.configuration.ProjectConfiguration. ...
    trackerEndFrame - 1;
frameNumberIdx = this.configuration.ProjectConfiguration. ...
    trackerStartFrame;
if frameNumberIdx == 0
    frameNumberIdx = 1;
end

this.lineageData.clearMarkerTracking()
% Initialize obj neighbourhood
this.lineageData = this.lineageData.initializeCellCloseNeighbours(this.configuration. ...
    ParameterConfiguration.divisionMarkerNeighbourhoodSearchRadius, NuniqueFrames);

% Initialization of tracking data structs for first frame
[ret_fq] = this.fingerprint.computeFingerprintForFrame(1, 1, ...
    this.configuration.ParameterConfiguration.divisionMarkerFingerprintHalfWindowSideLength, ...
    this.configuration.ParameterConfiguration.divisionMarkerFingerprintResizeFactor, ...
    this.configuration.ParameterConfiguration.divisionMarkerFingerprintMaxConsideredFrequencies  , ...
    this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
    this.lineageData.obj_frame, ...
    this.lineageData.obj_center_x, ...
    this.lineageData.obj_center_y);
% Save Fingerprints
this.lineageData.setFieldArrayForFrame('obj_track_fingerprint_real', 1, ...
    ret_fq);
% Initialize OldFrame
this.lineageData.initializeOldFrame();

%% here, the tracking starts
while frameNumberIdx <= NuniqueFrames
    frameNumber = uniqueFrames(frameNumberIdx);
    
    if this.configuration.ParameterConfiguration.debugLevel == 1
        tic
    end
    % Compute fingerprints (real freq) for current
    % image frame
    [ret_fq] = this.fingerprint.computeFingerprintForFrame(frameNumber, ...
        frameNumber, this.configuration.ParameterConfiguration.divisionMarkerFingerprintHalfWindowSideLength, ...
        this.configuration.ParameterConfiguration.divisionMarkerFingerprintResizeFactor, ...
        this.configuration.ParameterConfiguration.divisionMarkerFingerprintMaxConsideredFrequencies  , ...
        this.configuration.ProjectConfiguration.imageDir, ...
        this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
        this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
        this.lineageData.obj_frame, ...
    this.lineageData.obj_center_x, ...
    this.lineageData.obj_center_y);
    % Save fingerprints for current image frame
    this.lineageData.setFieldArrayForFrame('obj_track_fingerprint_real', ...
        frameNumber, ret_fq);    
    
    % Initialize NewFrame
    this.lineageData.initializeNewFrame(frameNumber);
    
    % A costMatrix based on NewFrame and OldFrame and cost
    % penalty functions is created.
    if qc == true
        fprintf('Frame begin: %d \n', frameNumber)
        fprintf('OldFrame: # cell_index %d \n', numel(this.lineageData.OldFrame.cell_index))
        fprintf('NewFrame: # cell_index %d \n',  numel(this.lineageData.NewFrame.cell_index))
    end
    [totalCost, positionCost, areaCost, rotationCost, ...
        frameSkippingCost] = this.lap.computeAssignmentCostMatrix(...
        this.lineageData.OldFrame, this.lineageData.NewFrame, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxObjCenterDisplacement, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxMajorAxisRotation, ...
        this.configuration.ParameterConfiguration.divisionMarkerIndividualFunctionPenalty, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxObjSizeDecrease, ...
        this.configuration.ParameterConfiguration.divisionMarkerAverageObjSizeGrowth, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxObjSizeIncrease, ...
        this.configuration.ParameterConfiguration.divisionMarkerUsedFunctionsForCostMatrix, ...
        mean(this.lineageData.obj_major_axis)+1, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxTrackFrameSkipping, 0, ...
        this.configuration.ParameterConfiguration.data3D);
    
    % Compute linear assignment
    if isempty(totalCost)
        totalCost = [];
%         this.lineageData.updateTemporaryTrackingDataForNextFrameEvaluation(...
%         this.configuration.ParameterConfiguration. ...
%         neighbourhoodSearchRadius);
%         frameNumberIdx = frameNumberIdx +1;
%         continue
    end
    assignment = TracX.ExternalDependencies.lapjv(totalCost);
    [ assignmentRowIdx, assignmentColIdx, ~, ...
        totalCostAssigned, positionCostAssigned, areaCostAssigned, ...
        rotationCostAssigned, frameSkippingCostAssigned] = ...
        this.lap.solveLAP( totalCost, positionCost, ...
        areaCost, rotationCost, frameSkippingCost );
    assert(isequal(assignment, assignmentColIdx))
    
    if this.configuration.ParameterConfiguration.debugLevel > 1
        this.utils.printToConsole(sprintf(['Elapsed time is %4.4f', ...
            ' seconds for solving the linear assignment on frame %d.'], ...
            toc, frameNumber))
    end
    
    tic
    lengthNewFrame = length(this.lineageData.NewFrame.cell_area);
    assignedRow = ismember(assignmentColIdx, 1:lengthNewFrame);
    assignmentIdx = find(assignedRow == 1);
    
    % Keep old tracks which could not be assigned in this frame
    % (not segmented or cost was too high) to be able to
    % potentially assign them in the next frame
    this.lineageData.InitializeVeryOld( ~assignedRow, ...
        this.configuration.ParameterConfiguration.divisionMarkerMaxTrackFrameSkipping);
%     sprintf('frame %d', frameNumber)
%     this.lineageData.VeryOld.track_index
    
    % Displacement stored (but not used atm as we use the more
    % precise one of the filtered vector field)
    assignmentValue = assignmentColIdx(assignmentIdx);
    difX = zeros(lengthNewFrame,1);
    difY = zeros(lengthNewFrame,1);
	difZ = zeros(lengthNewFrame,1);
    difX(assignmentValue) = (this.lineageData.NewFrame.cell_center_x(...
        assignmentValue) - this.lineageData.OldFrame.cell_center_x(...
        assignmentIdx))./this.lineageData.OldFrame.track_age(assignmentIdx);
    difY(assignmentValue) = (this.lineageData.NewFrame.cell_center_y(...
        assignmentValue) - this.lineageData.OldFrame.cell_center_y(...
        assignmentIdx))./this.lineageData.OldFrame.track_age(assignmentIdx);
	difZ(assignmentValue) = (this.lineageData.NewFrame.cell_center_z(...
        assignmentValue) - this.lineageData.OldFrame.cell_center_z(...
        assignmentIdx))./this.lineageData.OldFrame.track_age(assignmentIdx);
    
    % Included Aaron's filtered vector calculation (unchanged)
    if this.configuration.ParameterConfiguration.divisionMarkerRadiusVectorFilterX ~= 0
        filteredDifX = zeros(lengthNewFrame,1);
        filteredDifY = zeros(lengthNewFrame,1);
        filteredDifZ = zeros(lengthNewFrame,1);
        [filteredDifX(assignmentValue), filteredDifY(...
            assignmentValue), filteredDifZ(...
            assignmentValue)] = this.motion.filterVectorField(...
            this.lineageData.NewFrame.cell_center_x(assignmentValue), ...
            this.lineageData.NewFrame.cell_center_y(assignmentValue), ...
			this.lineageData.NewFrame.cell_center_z(assignmentValue), ...
            difX(assignmentValue),...
            difY(assignmentValue),...
			difZ(assignmentValue),...
            this.configuration.ParameterConfiguration.divisionMarkerRadiusVectorFilterX, ...
            this.configuration.ParameterConfiguration.divisionMarkerRadiusVectorFilterY,...
			this.configuration.ParameterConfiguration.divisionMarkerRadiusVectorFilterZ);
    else
        filteredDifX = difX;
        filteredDifY = difY;
		filteredDifZ = difZ;
    end
      
    this.lineageData.VeryOld.track_age = this.lineageData.VeryOld.track_age + 1;

    
    newTrackIndex = zeros(lengthNewFrame,1);
    newTrackIndex(assignmentValue) = this.lineageData.OldFrame.track_index(assignmentIdx);
    where = find(newTrackIndex == 0);
    add = length(where);
    %this.lineageData.numberOfTracks = numel([this.lineageData.OldFrame.track_index; this.lineageData.VeryOld.track_index]);
    this.lineageData.numberOfTracks + 1 : this.lineageData.numberOfTracks + add;
    newTrackIndex(where) = this.lineageData.numberOfTracks + 1 : this.lineageData.numberOfTracks + add;
    if ~isempty(where)
        this.lineageData.numberOfTracks = this.lineageData.numberOfTracks + add;
    end
    
    % Put the assignment of the  new tracks into the NewFrame
    % object
    this.lineageData.NewFrame.track_index = newTrackIndex;
    this.lineageData.NewFrame.track_index_qc = zeros(numel(newTrackIndex),1);
    this.lineageData.NewFrame.track_age = ones(numel(newTrackIndex),1);
    this.lineageData.NewFrame.cell_dif_x = difX;
    this.lineageData.NewFrame.cell_dif_y = difY;
    this.lineageData.NewFrame.cell_filtered_dif_x = filteredDifX;
    this.lineageData.NewFrame.cell_filtered_dif_y = filteredDifY;
    
    % Recompute neighbourhood for VeryOld cells in the NewFrame
    % @todo never called!
    if isfield(this, 'VeryOld') && ishandle(this.VeryOld)
        if ~isempty(this.lineageData.VeryOld.cell_center_x)
            coords = [ [this.lineageData.NewFrame.cell_center_x; ...
                this.lineageData.VeryOld.cell_center_x], [this.lineageData.NewFrame.cell_center_y;...
                this.lineageData.VeryOld.cell_center_y]];
            neighbourMatrix = TracX.Functions.getCloseCellNeighbourIdx(coords(:,1), coords(:,2), ...
                this.configuration.ParameterConfiguration.divisionMarkerNeighbourhoodSearchRadius);
            NNewFrame = size(this.lineageData.NewFrame.cell_center_x,1);
            this.lineageData.VeryOld.cell_close_neighbour = neighbourMatrix(NNewFrame+1:end,1:NNewFrame);
        end
    end
    
    if ~isempty(this.lineageData.NewFrame.track_fingerprint_real) && ...
            ~isempty(this.lineageData.OldFrame.track_fingerprint_real)
        [realFingerprintTrackDistance] = ...
            this.fingerprint.computeAssignmentFingerprintDistanceArray(this.lineageData.OldFrame, ...
            this.lineageData.NewFrame);
        %         realFingerprintTrackDistance = nan(size(this.lineageData.OldFrame.track_fingerprint_real));
    else
        realFingerprintTrackDistance = nan(size(this.lineageData.OldFrame.track_fingerprint_real));
    end
    
    % Here we add new distances
    % need them to be in the shape and order of the old frame.
    this.lineageData.OldFrame.track_fingerprint_real_distance = realFingerprintTrackDistance;
    
    [oldFrameLocIdx, emptyDataLocIdx] = this.lineageData.getTrackIndexStorageIndicies();
    
    % Save 'track_fingerprint_real_distance'
    dataToSave = nan(this.lineageData.getNumberOfRowsFromFrame(frameNumber),1);
    this.lineageData.copyResultsToTrackerData(dataToSave, 'track_fingerprint_real_distance', ...
        emptyDataLocIdx, oldFrameLocIdx, frameNumber)
    
   
    %---------------------------------------------------------%
    % Save tracking assignments contained in OldFrame into the
    % SegmentationData object for the current frame
    %---------------------------------------------------------%
    ds2Save = [newTrackIndex, ones(numel(newTrackIndex), 1), ...
        difX, difY, filteredDifX, filteredDifY];
    if ~isempty(ds2Save)
        this.lineageData.saveTrackingAssignmentsToTrackerData(frameNumber, ...
            struct('obj_track_index', newTrackIndex, 'obj_track_age',...
                ones(numel(newTrackIndex),1), 'obj_dif_x', difX, ...
                    'obj_dif_y', difY, 'obj_dif_z', difZ, 'obj_filtered_dif_x', filteredDifX, ...
                    'obj_filtered_dif_y', filteredDifY, 'obj_filtered_dif_z', filteredDifZ))
        %         totalCostAssigned', positionCostAssigned', ...
        %         areaCostAssigned', rotationCostAssigned', ...
        %         frameSkippingCostAssigned'
    end
      
    this.lineageData.updateTemporaryTrackingDataForNextFrameEvaluation(...
        this.configuration.ParameterConfiguration. ...
        divisionMarkerNeighbourhoodSearchRadius);
    
    frameNumberIdx = frameNumberIdx +1;
    
    if qc == true
        fprintf('Frame end %d \n', frameNumber)
        fprintf('End oldFrame # track_index %d \n', numel(this.lineageData.OldFrame.track_index))
    end
    
end

end
