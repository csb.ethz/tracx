%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function validateTmpLinAss(this)
% VALIDATETMPLINASS Validates temporary mother daughter (MD) assignments based on
% how often an overlapping assignment was found and with which budneck
% marker track. Assignments where a marker track is used for different MD
% pairs will be modified by keeping the one pair with the most detections.
% The others will get the second longest assignment or if there is none
% these will be deleted.
%
% Args:
%    this (:obj:`object`):                        :class:`Tracker` instance   
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Andreas P. Cuny - initial implementation

%@ todo compute a final assignment score to get an idea how trustworty a
%lineage assignment is. What should we include in  score?
% Presence / Absence of whi5 marker? myo1 marker?
% Length of frames which determines the assignment? - depends on image
% frequency
% intersection with merged mask?

% First move MD pairs which could not be determined automatically. Mothers
% with Nan to another temporary object
this.unresolvedTempLinAss = copy(this.data.TmpLinAss);
this.unresolvedTempLinAss.deleteEntry(~isnan(this.data.TmpLinAss.motherCell))
this.data.TmpLinAss.deleteEntry(isnan(this.data.TmpLinAss.motherCell))

markerTrackArray = this.data.TmpLinAss.markerTrack;
occ = grouptransform(markerTrackArray(:),markerTrackArray(:),@numel);
mTV = unique(markerTrackArray(occ > 1 & ~isnan(markerTrackArray(:))));

for i = mTV
    
    markerTrackArray = this.data.TmpLinAss.markerTrack;
    trackidx = markerTrackArray == i;
    m = this.data.TmpLinAss.motherCell(trackidx);
    d = this.data.TmpLinAss.daughterCell(trackidx);
    fp = this.data.TmpLinAss.frameOfMDPair(trackidx);
    mid = this.data.TmpLinAss.markerID(trackidx);
    n  = numel(m);
    score = zeros(n,1);
    indices = find(trackidx);
    
    markerOcc = this.lineageData.getConditionalFieldArray( ...
        'obj_frame', 'obj_track_index', i);
    
    %     score = zeros(numel(m), 2);
    for j = 1:numel(m)
        % Consider only MD pairs which were detected less than
        % maxFrameEvalAfterBirth frames after the daughter start
        daughteridx = this.data.TmpLinAss.daughterCell == d(j);
        valFrames = this.data.TmpLinAss.frameOfMDPair{daughteridx} < (...
            this.data.TmpLinAss.daughterCellStartFrame(daughteridx) + ...
            this.data.TmpLinAss.maxFrameEvalAfterBirth);
        score(j) = sum(ismember(markerOcc, fp{j}(valFrames)));
    end
    
    keepIdx   = score == max(score);
    deleteIdx = false(n,1);
    
    if sum(keepIdx) == 1
        % We have one winning assignment to keep
        %         score(keepIdx, :) = [];
        for k = 1:n
            if ~keepIdx(k)
                remainingFrames = fp{k};
                rmIdx = ismember(remainingFrames, markerOcc);
                remainingFrames = remainingFrames(~rmIdx);
                markerID = mid{k}(~rmIdx);
                uuids = this.lineageData.getConditionalFieldArray( ...
                    'obj_index', 'obj_frame', remainingFrames);
                trid = this.lineageData.getConditionalFieldArray( ...
                    'obj_track_index', 'obj_frame', remainingFrames);
                newTrack = trid(ismember(uuids, markerID));
                uniqueTrack = unique(newTrack);
                Ncount = histc(newTrack, uniqueTrack);
                mT = uniqueTrack(find(Ncount == max(Ncount), 1, 'first'));
                if isempty(mT) || numel(mT) > 1
                    % @todo Currently we call the delete function but we might
                    % want to keep this temporary mother daughter assignments
                    % for manual validation
                    deleteIdx(k) = true;                   
                else
                    this.data.TmpLinAss.markerTrack(indices(k)) = mT;
                    this.data.TmpLinAss.markerTrackStartFrame(indices(k)) = ...
                        unique(min(this.lineageData.getConditionalFieldArray(...
                        'obj_frame', 'obj_track_index', mT)));
                    this.data.TmpLinAss.markerTrackEndFrame(indices(k)) = ...
                        unique(max(this.lineageData.getConditionalFieldArray(...
                        'obj_frame', 'obj_track_index', mT)));
                end
            end
        end
    else
        % We have multiple assignment of same length, we need to check them
        % all for alternatives and/or delete them all
        deleteIdx(:) = true;
    end

    % implement all identified deletions
    for k = 1:n
        if deleteIdx(k)
            this.data.TmpLinAss.deleteEntry(indices(k));
        end
    end
        
end

% Resolve circular MD assignments
this.resolveCircAssignment();

end
