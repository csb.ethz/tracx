%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function setSignalInNucleus(this, channel, qc, varargin)
% SETSIGNALINNUCLEUS Quantifies if a signal is in the nucleus when
% it is larger than the mean + 0.5 * mad.
%
% Args:
%    this (:obj:`object`):                   :class:`Lineage` instance 
%    channel (:obj:`int`):                   Number of the fluorescent channel
%                                            to read.
%    qc (:obj:`bool`):                       Flag if quality control images 
%                                            should be generated. 
%    varargin (:obj:`str varchar`):
%
%        * **minSignalLength** (:obj:`int`): Minimal length of signal [frames]. Defaults to 2.
%
% Returns
% -------
%    sigInNuc :obj:`array`              
%                                            Binary array. 1 if signal is 
%                                            in nucleus, 0 otherwise.                        
%
% :Authors:
%    Andreas P. Cuny - initial implementation


% sig = this.data.QuantificationData.getFieldNameForFluoChannel( ...
%     'fluo_nuc_q75', channel); %, 2
% sigInNuc = zeros(numel(sig), 1);
% sigInNuc(sig > (mean(sig) + 0.5 * mad(sig))) = 1;

% Parsing the inputs
defaultMinSigLength = 2;
p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('channel', @isnumeric);
p.addRequired('qc', @islogical);
p.addParameter('minSignalLength', defaultMinSigLength,  @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}));
parse(p, this,  channel, qc, varargin{:})

trackArray = unique(this.data.getFieldArray('track_index'))';
sigArray = this.data.QuantificationData.getFieldNameForFluoChannel( ...
        'fluo_nuc_q75', channel);
signq50 = this.data.QuantificationData.getFieldNameForFluoChannel('fluo_nuc_q50', channel);
sigcell = this.data.QuantificationData.getFieldNameForFluoChannel('fluo_cell_q50', channel);
sigArray = signq50./sigcell;

if isempty(this.data.getFieldArray('track_cell_cycle_phase'))
    this.data.initializeLineageDataFields();
end
    
pBar = this.utils.progressBar(numel(trackArray), ...
    'Title', 'Determine G1 phase' ...
    );

for track = trackArray
       
    frameArray = this.data.getFieldArrayForTrackIndex('cell_frame', track);
    sig = sigArray(this.data.SegmentationData.track_index == track);
    
    if size(frameArray, 1) > 4
        [~, validSigL, fh] = this.getBkgCorrSig(frameArray, sig, track, ...
            p.Results.minSignalLength, p.Results.qc);
        
        % Save control figure if qc == true
        if p.Results.qc == true
            print(fh, fullfile(this.configuration.ProjectConfiguration. ...
                segmentationResultDir, sprintf('G1Sig_%s_track_%d.png', this. ...
                configuration.ProjectConfiguration.controlPrefix, track)), '-dpng')
        end
        
        % Set the cell cycle phases
        this.data.setFieldnameValueForTrack('track_cell_cycle_phase', track, ...
            double(validSigL));
    end
    pBar.step([], [], []);
end
pBar.release();
end