%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function setParentAndHasBud(this)
% SETPARENTANDHASBUD Sets the track parent, the probability of the parent
% being correct and the parent score as well as during which frames the cell
% has a bud.
%
% Args:
%    this (:obj:`object`):                             :class:`Lineage` instance   
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Andreas P. Cuny - initial implementation

for ii = 1:size(this.data.TmpLinAss.daughterCell, 2)
    
    try
        
        % Set track parent
        this.data.SegmentationData.track_parent(...
            this.data.SegmentationData.track_index ...
            == this.data.TmpLinAss.daughterCell(ii)) = this.data.TmpLinAss.motherCell(ii);
        
        % Set track parent prob
        this.data.SegmentationData.track_parent_prob(...
            this.data.SegmentationData.track_index ...
            == this.data.TmpLinAss.daughterCell(ii)) = this.data.TmpLinAss.motherAssignmentProb(ii);
        
        % Set track parent score
        this.data.SegmentationData.track_parent_score(...
            this.data.SegmentationData.track_index ...
            == this.data.TmpLinAss.daughterCell(ii)) = this.data.TmpLinAss.motherAssignmentScore(ii);
        
        this.data.TmpLinAss.daughterCellStartFrame(ii) = ...
            unique(this.data.getFieldArrayForTrackIndex(...
            'track_start_frame', this.data.TmpLinAss. ...
            daughterCell(ii)));
        
        % Add track_has_bud information. (i.e is g2/m
        % phase). Note that the real start might be
        % overlapping with bud seperated from mother but
        % allready started as new track.
        dSegOcc = this.data.getFieldArrayForTrackIndex(...
            'cell_frame', this.data.TmpLinAss.daughterCell(ii));
        dSegOccDiv = dSegOcc(1:find(dSegOcc == this.data. ...
            TmpLinAss.markerTrackEndFrame(ii),1,'first'));
        this.data.SegmentationData.track_has_bud(and(ismember( ...
            this.data.SegmentationData.cell_frame, dSegOccDiv), ...
            this.data.SegmentationData.track_index == ...
            this.data.TmpLinAss.motherCell(ii))) = 1;
        % Nice extension: Method fusing daughter and mother
        % masks prior to cell division and run trough
        % cellxmask interface to correct for volume
        % estimation and better signal quantification.
        % -Done
        
    catch ME
        this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
        for i =1:numel(ME.stack)
            this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
            this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
            this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
        end
        continue
        
    end
end

end
