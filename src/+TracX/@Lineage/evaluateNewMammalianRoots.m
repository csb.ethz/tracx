%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function evaluateNewMammalianRoots(this, varargin)
% EVALUATENEWMAMMALIANROOTS Determination of best lineage for given root cells 
% for mammalian lineage reconstruction.
%
% Args:
%    this (:obj:`object`):                               :class:`Lineage` instance 
%    varargin (:obj:`str varchar`):
%
%        * **roots** (:obj:`array`):                     Array of lineage roots.
%  
% Returns
% -------
%    void (:obj:`-`)
%                                                        Prints reslut to
%                                                        console.
%
% :Authors:
%    Thomas Kuendig - initial implementation

if isempty(varargin)
    roots = unique(this.data.SegmentationData.track_index(...
        this.data.getFieldArray('track_parent')==0 & ...
        this.data.getFieldArray('cell_frame')>=this.configuration.ProjectConfiguration.trackerStartFrame &...
        this.data.getFieldArray('cell_frame')<=this.configuration.ProjectConfiguration.trackerEndFrame));
else
    roots = varargin{1};
end

fieldNameArray = {'track_index', 'track_index_corrected', 'cell_center_x', 'cell_center_y', 'cell_center_z','cell_area', ...
    'cell_filtered_dif_x', 'cell_filtered_dif_y', 'cell_filtered_dif_z', 'track_parent', 'track_lineage_tree' };

uniqueTrees = unique(this.data.SegmentationData.track_lineage_tree(...
    this.data.getFieldArray('cell_frame')>=this.configuration.ProjectConfiguration.trackerStartFrame &...
    this.data.getFieldArray('cell_frame')<=this.configuration.ProjectConfiguration.trackerEndFrame));

uniqueTrees = uniqueTrees(~isnan(uniqueTrees));

fprintf('***EVALUATION OF UNEXPECTED ROOTS***                      | TreeScores:\n');
fprintf('Root  First Frame  Likely tree:   Rating: ConfidenceScore: | %s Others\n',strjoin(compose('%6d',uniqueTrees),' '));


for i = 1:length(roots)
    iRoot = roots(i);
    firstFrame = min(this.data.getFieldArrayForTrackIndex('cell_frame',iRoot));
        
    if firstFrame <= 1 this.configuration.ProjectConfiguration.trackerStartFrame ||...
            firstFrame >= this.configuration.ProjectConfiguration.trackerEndFrame;
        continue;
    end
    
    oldFrameData = this.data.getTemporaryDataStruct(fieldNameArray, firstFrame-1);
    newFrameData = this.data.getTemporaryDataStruct(fieldNameArray, firstFrame);
    
    coords = [...
        [oldFrameData.cell_center_x + oldFrameData.cell_filtered_dif_x; newFrameData.cell_center_x + newFrameData.cell_filtered_dif_x],...
        [oldFrameData.cell_center_y + oldFrameData.cell_filtered_dif_y; newFrameData.cell_center_y + newFrameData.cell_filtered_dif_y],...
        [oldFrameData.cell_center_z + oldFrameData.cell_filtered_dif_z; newFrameData.cell_center_z + newFrameData.cell_filtered_dif_z],...
        ];
    trees = ...
        [oldFrameData.track_lineage_tree; newFrameData.track_lineage_tree];
    areas = ...
        [oldFrameData.cell_area; newFrameData.cell_area];
    
    rootTrackIdx = [oldFrameData.track_index; newFrameData.track_index] == iRoot;

    rootCoords = coords(rootTrackIdx,:);
    rootArea = areas(rootTrackIdx);
    coords = coords(~rootTrackIdx,:);
    trees = trees(~rootTrackIdx);
    
    distance2Root = coords - repmat(rootCoords, size(coords,1), 1);
    distance2Root = distance2Root.^2;
    distance2Root = sum(distance2Root,2);
    distance2Root = sqrt(distance2Root);
    
    [distance2RootSorted, sortIdx] = sort(distance2Root);
    distance2RootSorted = distance2RootSorted / (2*nthroot(3*rootArea/(4*pi),3));
    distance2RootSorted = distance2RootSorted .^2 .* (1:size(distance2RootSorted,1))';
    treesSorted = trees(sortIdx);

    %numConsideredCells = ceil(length(treesSorted)/5);
    %distance2RootSorted = distance2RootSorted(1:numConsideredCells);
    %treesSorted = treesSorted(1:numConsideredCells);
    
    [~, treesSortedIdx] = ismember(treesSorted, uniqueTrees);
    
    treeScores = zeros(1, length(uniqueTrees));
    nanScore = 0;
    for j = 1:length(treesSortedIdx)
        if treesSortedIdx(j)
            treeScores(treesSortedIdx(j)) = ...
                treeScores(treesSortedIdx(j)) + 1/distance2RootSorted(j);
        else
            nanScore = nanScore + 1/distance2RootSorted(j);
        end
    end
    [bestScore, maxIdx] = max(treeScores);
    bestTree = uniqueTrees(maxIdx);
    confidence = bestScore^2*bestScore/(sum(treeScores)-bestScore+nanScore);
%     if confidence > 27
%         rating = 'excellent';
%     elseif confidence > 9
%         rating = 'high';
%     elseif confidence > 3
%         rating = 'moderate';
%     else
%         rating = 'low';
%     end
    rating = repmat('*',1,max(floor(log(confidence)/log(3)),0));
    if floor(log(confidence)/log(3)) < 1
        bestTree = sprintf('%7d?',bestTree);
    else
        bestTree = sprintf('%8d',bestTree);
    end
        
    fprintf('%4d  %11d     %s %10s       %9.4f  | %s %.4f\n',...
        iRoot,firstFrame,bestTree,rating,confidence,strjoin(compose('%6.3f',treeScores),' '),nanScore);

    
end
end