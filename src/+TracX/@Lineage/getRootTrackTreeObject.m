%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function lineageTree = getRootTrackTreeObject(~, trackIndexArray, ...
    trackParentArray, rootTrack, rootTrackRelatedCellArray )
% GETROOTTRACKTREEOBJECT Constructs a tree lineage tree object for
% a specified rootTrack (SegmentationData.track_index typically of the
% first image frame) and all its relatives.
%
% Args:
%    ignoredArg (:obj:`object`):                  :class:`Lineage` instance.
%    trackIndexArray (:obj:`array`, kx1):         Array with all track indicies
%                                                 :class:`~+TracX.@SegmentationData.SegmentationData.track_index` 
%    trackParentArray (:obj:`array`, kx1):        Array with all track parents
%                                                 :class:`~+TracX.@SegmentationData.SegmentationData.track_parent` 
%    rootTrack (:obj:`int`, 1x1):                 The lineage tree root
%                                                 track
%    rootTrackRelatedCellArray (:obj:`int`, jx1): Related track indices with parent 
%                                                 parent == rootTrack
% Returns
% -------
%    lineageTree :obj:`object`
%                                                 Lineage tree object for 
%                                                 rootTrack and all its 
%                                                 relatives. Can be visualized 
%                                                 as cmd text output using 
%                                                 lineageTree.tostring or
%                                                 graphically using 
%                                                 lineageTree.plotBinaryTree(
%                                                 [], 'Parent', figureHandle
%                                                 'TextRotation', 360).
% 
% :Authors:
%    Andreas P. Cuny - initial implementation

queueTrackArrayForTree = rootTrackRelatedCellArray;
checkedTrackArray = [];

% Constructor of a @tree object using the rootTrack. Here we construct one
% lineageTree per rootTrack.
lineageTree = TracX.ExternalDependencies.tree.tree(rootTrack);

while ~isempty(queueTrackArrayForTree)
    
    if unique(trackParentArray(trackIndexArray == queueTrackArrayForTree(1))) ...
            == rootTrack
        % attach to root
        [ lineageTree ] = lineageTree.addnode(1, queueTrackArrayForTree(1)); 
        checkedTrackArray = [checkedTrackArray, queueTrackArrayForTree(1)];
        queueTrackArrayForTree = setdiff(queueTrackArrayForTree, ...
        checkedTrackArray);
    else
        trackParent = unique(trackParentArray(trackIndexArray ==  ...
            queueTrackArrayForTree(1)));
        if ~isempty(find(cell2mat(lineageTree.Node) == trackParent, 1))
            treeIdx = find(cell2mat(lineageTree.Node) == trackParent);
            % attach to respective node
            [ lineageTree  ] = lineageTree.addnode( treeIdx, ...
                queueTrackArrayForTree(1)); 
            checkedTrackArray = [checkedTrackArray, queueTrackArrayForTree(1)];
            queueTrackArrayForTree = setdiff(queueTrackArrayForTree, ...
                checkedTrackArray);
        else
            queueTrackArrayForTree = [queueTrackArrayForTree(2:end), ...
                queueTrackArrayForTree(1)];
        end
    end
end
end