%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = setTrackAge(this)
% SETTRACKAGE Sets the cell age for each track after lineage 
% reconstruction.
%
% Args:
%    this (:obj:`object`):             :class:`Lineage` instance 
%
% Returns
% -------
%    void :obj:`-`                                      
%
% :Authors:
%    Andreas P. Cuny - initial implementation

trackArray = this.data.getFieldArray('track_index');
parentArray = this.data.getFieldArray('track_parent');

uTrack = unique(trackArray)';

try
    
    for track = uTrack
        
        age = this.data.getFieldArrayForTrackIndex('track_age', track);
        frame = this.data.getFieldArrayForTrackIndex('cell_frame', track);
        
        children = this.getRootTrackRelatedDaughterArray(trackArray, ...
            parentArray, track);
        
        if ~isempty(children)
            cnt = 1;
            for child = children
                
                divisionFrame = this.data.TmpLinAss.markerTrackEndFrame( ...
                    this.data.TmpLinAss.daughterCell == child);
                age(frame(frame == divisionFrame):end) = cnt;
                cnt = cnt + 1;
            end
        end
        
        % Store the age array
        this.data.setFieldnameValueForTrack('track_age', track, age)
    end
    
catch ME
    this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
    for i =1:numel(ME.stack)
        this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
        this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
        this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
    end
    return
end

end
