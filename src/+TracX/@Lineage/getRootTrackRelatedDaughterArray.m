%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function rootTrackRelatedCellArray = getRootTrackRelatedDaughterArray(~, ...
    trackIndexArray, trackParentArray, rootTrack )
% GETROOTTRACKRELATEDDAUGHTERARRAY Finds all SegmentationData.track_index  
% directly and indirectly related to a certain track_index of interest 
% (e.g. root of a lineage).
%
% Args:
%    ignoredArg (:obj:`object`):               :class:`Lineage` instance.
%    trackIndexArray (:obj:`array`, kx1):      Array with all track indicies
%                                              :class:`~+TracX.@SegmentationData.SegmentationData.track_index` 
%    trackParentArray (:obj:`array`, kx1):     Array with all track parents
%                                              :class:`~+TracX.@SegmentationData.SegmentationData.track_parent` 
%    rootTrack (:obj:`int`, 1x1):              The lineage tree root
%                                              track
% Returns
% -------
%    rootTrackRelatedCellArray :obj:`array`
%                                              Array containing track_index 
%                                              numbers of to rootTrack related 
%                                              cells. (For all those track 
%                                              indicies the initial 
%                                              cell_parent track_index (rootTrack) 
%                                              can be added to 
%                                              :class:`~+TracX.@SegmentationData.SegmentationData.track_lineage`
% 
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get directly related daughter cells of rootTrack
queueM = unique(trackIndexArray(trackParentArray == rootTrack))';
% Find all indirectly related daughter cells of the daughter cells of the
% rootTrack
checkedCellArray = 1;
rootTrackRelatedCellArrayTmp = [];
while ~isempty(queueM)
    currentCell = queueM(1);
    checkedCellArray = [checkedCellArray, currentCell];
    queueD = unique((trackIndexArray(trackParentArray == currentCell)))';
    queueM = [queueM(1:end) queueD];
    rootTrackRelatedCellArrayTmp = [rootTrackRelatedCellArrayTmp queueM];
    queueM = setdiff(queueM, checkedCellArray);
end
% All related cells (defined by the track_index) of rootTrack.
rootTrackRelatedCellArray = unique(rootTrackRelatedCellArrayTmp);

end