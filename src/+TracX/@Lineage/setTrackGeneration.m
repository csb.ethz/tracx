%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = setTrackGeneration(this)
% SETTRACKGENERATION Sets the cell generation for each track after lineage 
% reconstruction.
%
% Args:
%    this (:obj:`object`):             :class:`Lineage` instance 
%
% Returns
% -------
%    void :obj:`-`                                      
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% @todo
% if varargin is empty do
% else use indicies form given array (not necessarily starting from frame
% 1.

lineageRootArray = this.data.getFieldArrayForFrame('track_index', ...
    this.configuration.ProjectConfiguration.trackerStartFrame)';
% For the first frame filter out cells allready belonging to a tree
lineageRootArrayIdx = ones(size(lineageRootArray));
for k = 1:numel(lineageRootArray)
    parent = unique(this.data.getFieldArrayForTrackIndex('track_parent', ...
        lineageRootArray(k)));
    if parent ~= 0
        lineageRootArrayIdx(k) = 0;
    end
end
lineageRootArray(~lineageRootArrayIdx) = [];

trackIndicesArray = this.data.getFieldArray('track_index');
trackParentsArray =  this.data.getFieldArray('track_parent');

for lineageRoot = lineageRootArray
rootRelated = this.getRootTrackRelatedDaughterArray(trackIndicesArray, ...
    trackParentsArray, lineageRoot);
t = this.getRootTrackTreeObject(trackIndicesArray, trackParentsArray, ...
    lineageRoot, rootRelated);
dt = t.depthtree;
tracksInTreeArray = cell2mat(t.Node);
generationIdx = cell2mat(dt.Node);

for i = 1:numel(tracksInTreeArray)
    track_index = tracksInTreeArray(i);
    
    % Set track lineage using the root index as identifier
    this.data.setFieldnameValueForTrack('track_lineage_tree', ...
        track_index, lineageRoot)
    
    % Set track generation
    this.data.setFieldnameValueForTrack('track_generation', ...
        track_index, generationIdx(i))
end

end
