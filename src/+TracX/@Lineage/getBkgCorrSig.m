%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [bkgCorrSig, validSigL, fh] = getBkgCorrSig(~, x, y, track, ...
    minSigLength, qc)
% GETBKGCURRSIG Returns a background corrected nuclear signal for G1
% determination.
%
% Args:
%    ignoredArg (:obj:`object`):               :class:`Lineage` instance  
%    x (:obj:`array`):                         Array of frame numbers of
%                                              signal.
%    y (:obj:`array`):                         Array with raw signal 
%    track (:obj:`int`):                       Current track index. 
%                                              track indices.
%    minSigLength (:obj:`int`):                Minimal length of signal to be 
%                                              considered.
%    qc (:obj:`bool`):                         Flag, if quality control
%                                              images should be written.
%
% Returns
% -------
%    bkgCorrSig :obj:`array`                                
%                                             Array with background corrected  
%                                             signal. 
%    validSigL :obj:`array`                                
%                                             Logical array for valid signal. 
%    fh :obj:`handle`                                
%                                             Figure handle of quality
%                                             control image
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% @todo check homogentiy of signal (no holes)

warning('off', 'MATLAB:legend:IgnoringExtraEntries')
% Pad frame and signals array to estimate start and end better
%ysort = sort(y);
nPad = round(numel(y) *0.10);
if nPad < 5
    nPad = 5;
end
% if size(ysort, 1) >= 15
%     ypad = mean(ysort(1:15));
% else
%     ypad = mean(ysort);
% end
thresh = (max(y)-min(y)) *0.08 + min(y);
ypad = mean(y(y < thresh));
x = [repmat(min(x)-1-(nPad-1):min(x)-1,1,1)'; x; repmat(x(end)+1:x(end)+nPad,1,1)'];
y = [repmat(ypad,nPad,1); y; repmat(ypad,nPad,1)];
% Get background sampling indices
pts = find(y < thresh); %median(y)
% Remove outliers
vpts = y(y < thresh);
mpts = mean(y(y < thresh));
spts = std(y(y < thresh));
pts = pts(vpts < mpts + 3 * spts);

if numel(pts) > 3
    % Add index for first element if not present
    if isempty(pts(ismember(pts, 1)))
        pts = [1; pts];
    end
    % Subset indices to make estimation more robust
    pts = pts(1:1:end);
    % Add index for last element if not present
    if isempty(pts(ismember(pts, numel(y))))
        pts(end) =  numel(y);
        pts(end-1) =  numel(y)-1;
        pts(end-2) =  numel(y)-2;
    end
    % disp([mean(y), median(y), std(y), mad(y)])
    yfit = interp1(pts,y(pts),x, 'pchip');
else
    yfit = zeros(numel(y), 1);
end
yBkgCorr = medfilt1(y - yfit, 4); % (yfit+mad(yfit));
bkg = yBkgCorr(pts);

bkgCutOff = nanmean(bkg) + 2.5 * nanstd(bkg); % mad(y) * 1/2;
nonBkg = yBkgCorr > bkgCutOff;
% if std(y) < 5
%     nonBkg(:) = 0;
% end
dNonBkg = [true; diff(nonBkg) ~= 0; true];
nNonBkg = diff(find(dNonBkg));
sigLOcc = repelem(nNonBkg, nNonBkg);
if size(sigLOcc,2) > size(sigLOcc, 1)
    sigLOcc = sigLOcc';
end
validSigL = and(nonBkg == 1, sigLOcc >= minSigLength);
% bkgCutOff2 = mean(bkg(bkg < mean(bkg))) + 3 * std(bkg(bkg < mean(bkg)));
% validSigLRef = bkg >= bkgCutOff2;

if qc == true
    fh = figure('Renderer', 'painters', 'Position', [10 10 1000 550], ...
        'Visible', 'off');
    
    plot(x,y, 'k')
    hold on
    plot(x(pts),y(pts), 'xc')
    hold on
    plot(x, yfit, 'm')
    hold on
    plot(x, yBkgCorr, 'b')
    hold on
    plot(x(validSigL), yBkgCorr(validSigL), 'gx')
    hold on
    %     plot(x(validSigLRef), bkg(validSigLRef), 'rx')
    %     hold on
    %yline(mad(y),'--b');
    % yline(bkgCutOff,'--b');
    xlims = get(gca,'XLim');
    plot(xlims, [bkgCutOff bkgCutOff], '--b')
    hold on
    %yline(nanmean(bkg(bkg < nanmean(bkg))),'--r');
    plot(xlims, [nanmean(bkg(bkg < nanmean(bkg))) nanmean(bkg(bkg < nanmean(bkg)))], '--r')
    %     hold on
    %     yline(bkgCutOff2,'-m');
    if size(x(validSigL), 1) == 0
        leg = legend('raw sig', 'bkg sample', 'bkg fit', 'bkg corr sig', ...
            'mean(bkg) + 2.5 * std(bkg)', 'mean(bkg)', 'Location', 'northoutside', ...
            'Orientation', 'horizontal');
    else
        leg = legend('raw sig', 'bkg sample', 'bkg fit', 'bkg corr sig', ...
            'valid signal', 'mean(bkg) + 2.5 * std(bkg)', 'mean(bkg)', ...
            'Location', 'northoutside', ...
            'Orientation', 'horizontal');
    end
    title(leg,sprintf('Track: %d', track))
    set(leg,'Box','off')
    xlabel('frame number')
    ylabel('Signal (a.u.)')
    grid on
    box off
else
    fh = nan;
end

% Remove padding for output
bkgCorrSig = yBkgCorr(nPad+1:end-nPad);
validSigL = validSigL(nPad+1:end-nPad);

end