%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = resolveCircAssignment(this)
% RESOLVECIRCASSIGNMENT Aims to resove circular mother-daugher assignments
% where each track assignes each other as parent. The incorect assignment
% will be deleted.
%
% Args:
%    this (:obj:`object`):                       :class:`Lineage` instance.   
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get the MD pairs which assign each others as parents
daugherArray = this.data.TmpLinAss.daughterCell;
motherArray = this.data.TmpLinAss.motherCell;
d = nan(1, 2);
for k = 1:numel(daugherArray)
    daughter = daugherArray(k);
    ret = unique(this.data.getFieldArrayForTrackIndex('track_parent', ...
        daughter));
    ret2 = unique(this.data.getFieldArrayForTrackIndex('track_parent', ...
        ret));
    
    if daughter == ret2
        idx = ismember(d, [daughter, ret; ret, daughter], 'rows');
        if any(idx == 1) == 0
            d = [d; [daughter, ret]];
        end
    end
end
d = d(all(~isnan(d),2),:);

% Resolve the MD pairs which assign each others as parents
for k = 1:size(d, 1)
    stat = nan(4, 2);
    idx = ismember([daugherArray; motherArray]', [d(k,:); d(k,[2,1])], 'rows');
    startFrame = this.data.TmpLinAss.daughterCellStartFrame(idx);
    stat(1, :) = startFrame == max(startFrame);
    motherEval = this.data.TmpLinAss.motherCell(idx);
    motherScore = this.data.TmpLinAss.motherAssignmentScore(idx);
    stat(2, :) = motherScore == max(motherScore);
    framePair = this.data.TmpLinAss.frameOfMDPair(idx);
    
    area = nan(size(startFrame));
    frame = nan(size(startFrame));
    for j = 1:size(startFrame, 2)
        area(j) = this.data.getFieldArrayForSubsetOnFrame('cell_area', ...
            startFrame(j), motherEval(j));
        frame(j) = min(framePair{j});
    end
    stat(3, :) = area == max(area);
    stat(4, :) = frame == min(frame);
    
    % Determine which MD assignment has the highest score which is the one to
    % keep
    keepIdx = sum(stat) == max(sum(stat));
    
    if sum(keepIdx) == 1
        delIdx = ismember([daugherArray; motherArray]', [d(k,~keepIdx), ...
            d(k, keepIdx)], 'rows');
        % Delete the wrong assignment
        this.data.TmpLinAss.deleteEntry(delIdx);
    else
        % Could not determine what to delete
        % @todo move them to the manual verification
        % @todo add addEntry and getEntry method to TemporaryLineageAssignment
        % this.unresolvedTempLinAss.addEntry()
        % this.data.TmpLinAss.getEntry()
    end
end
end