%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function stable = setParentMamDivCells(this, startFrame, endFrame, imageFrameNumberArray, ...
    joinTracks, printOutput)
% SETPARENTMAMDIVCELLS Determines the relationship
% between daughter and mother cell of symmetricaly dividing cells.
%
% Args:
%    this (:obj:`object`):                    :class:`Lineage` instance  
%    startFrame (:obj:`int`):                 Number of the first frame of 
%                                             the project where to start the 
%                                             mother daughter assignment.
%    endFrame (:obj:`int`):                   Number of the last frame of the
%                                             project where to end the mother 
%                                             daughter assignment.
%    imageFrameNumberArray (:obj:`object`):   Array with all image frame 
%                                             numbers for current project. 
%                                             This function evaluates
%                                             this array in the range 
%                                             startFrame to endFrame.
%    joinTracks (:obj:`bool`):                Flag to join tracks. Defaults
%                                             to true.
%    printOutput (:obj:`bool`):               Flag, if output should be
%                                             printed to console.
%
% Returns
% -------
%    stable :obj:`array`                                
%                                             Indices of joined non empty 
%                                             splitted track indices and
%                                             joined track indices.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Get data for needed field names from TrackerData.SegmentationData for the
% first project image frame.
%, ...
%     'cell_majoraxis', 'cell_pole1_x' 'cell_pole1_y', 'cell_pole2_x', ...
%     'cell_pole2_y', 'cell_pole1_age', 'cell_pole2_age', 'track_parent' };

SIZETHRESHOLD = 1.3;
joinTracks = 1;

if ~isempty(this.configuration.ProjectConfiguration.fluoImageIdentifier)
    if printOutput
        this.data.utils.printToConsole(sprintf(...
            'Using fluorescent channel %s for lineage reconstruction.',...
            this.configuration.ProjectConfiguration.fluoImageIdentifier{1}));
    end
    if ~isprop(this.data.SegmentationData,'track_criticalchange')
        addprop(this.data.SegmentationData,'track_criticalchange');
    end
    this.data.SegmentationData.track_criticalchange = nan(size(this.data.getFieldArray('cell_area')));
    fieldNameArray = {'track_index', 'track_index_corrected', 'cell_center_x', 'cell_center_y', 'cell_center_z','cell_area', ...
    'cell_filtered_dif_x', 'cell_filtered_dif_y', 'cell_filtered_dif_z', 'track_parent' ,'track_criticalchange'};

    fluoActive = 1;
else
    if printOutput
        this.data.utils.printToConsole(sprintf(...
            'Not using fluorescent data for lineage reconstruction'));
    end
    fieldNameArray = {'track_index', 'track_index_corrected', 'cell_center_x', 'cell_center_y', 'cell_center_z','cell_area', ...
    'cell_filtered_dif_x', 'cell_filtered_dif_y', 'cell_filtered_dif_z', 'track_parent'};
    fluoActive = 0;
end

frameArray = imageFrameNumberArray(startFrame:endFrame);

% Total frame number
nFrames = length(frameArray);

this.data.SegmentationData.track_index_corrected = this.data.SegmentationData.track_index;


% list cell appearance and disappearances
firstFrames = cell(nFrames,1);
lastFrames = cell(nFrames+1,1);
uniqueTracks = unique(this.data.SegmentationData.track_index(...
    ismember(this.data.SegmentationData.cell_frame,frameArray)))';
uniqueTracks = uniqueTracks(~isnan(uniqueTracks));
for t = uniqueTracks
    first_frame = this.data.SegmentationData.cell_frame( ...
        find(this.data.SegmentationData.track_index == t, 1, 'first'));
    firstFrames{first_frame}(end+1,1) = t;
    last_frame = this.data.SegmentationData.cell_frame( ...
        find(this.data.SegmentationData.track_index == t, 1, 'last'));
    lastFrames{last_frame+1}(end+1,1) = t;
    
    %% analyise fluorescence chang in each track
    if fluoActive
        totfluo = this.data.QuantificationData.getFieldNameForFluoChannel('fluo_cell_total',1);
        backfluo = this.data.QuantificationData.getFieldNameForFluoChannel('fluo_background_mean',1);
        meanInt = totfluo( this.data.getFieldArray('track_index') == t) ...
            ./ this.data.getFieldArrayForTrackIndex('cell_area',t)- ...
            backfluo( this.data.getFieldArray('track_index') == t);
        meano = (meanInt)/mean((meanInt));
        this.data.setFieldnameValueForTrack('track_criticalchange', t, [meano>1+2*std(meano)]);
    end
end


oldFrameData = this.data.getTemporaryDataStruct(fieldNameArray, startFrame );

trackSplits = zeros(1,length(this.data.SegmentationData.cell_frame));

for f = 2:nFrames
    frameNumber = frameArray(f);
    
    newFrameData = this.data.getTemporaryDataStruct(fieldNameArray, frameNumber );
    
% %     Find track differences between frames
% %     parentCandidateArrayTemp = setdiff(oldFrameData.track_index, ...
% %         newFrameData.track_index);
% %     parentCandidateArrayTempIdx = ismember(oldFrameData.track_index, ...
% %         parentCandidateArrayTemp);
% %     parentCandidateArray2 = oldFrameData.track_index(parentCandidateArrayTempIdx);    
% %     daughterCandidateArrayTemp = setdiff(newFrameData.track_index, ...
% %         oldFrameData.track_index);
% %     daughterCandidateArrayTempIdx = ismember(newFrameData.track_index, ...
% %         daughterCandidateArrayTemp);
% %     daughterCandidateArray = newFrameData.track_index(daughterCandidateArrayTempIdx);

    
%   parents are tracks that ended in the previous frame
    parentCandidateArray = lastFrames{f};
    parentCandidateType = zeros(size(parentCandidateArray));
    
%   daughters are tracks that newly appear
    daughterCandidateArray = firstFrames{f};
    daughterCandidateType = zeros(size(daughterCandidateArray));
    
%   survivors are tracks that continue but have an above SIZETHRESHOLD jump
%   in cell_area
    survivingArrayTempIdx = repmat(oldFrameData.track_index,1,length(newFrameData.track_index)) == ...
        repmat(newFrameData.track_index,1,length(oldFrameData.track_index))';
    survivingArrayTempArea = repmat(oldFrameData.cell_area,1,length(newFrameData.track_index)) ./ ...
        repmat(newFrameData.cell_area,1,length(oldFrameData.track_index))';
    if fluoActive
        survivingArray = unique([newFrameData.track_index(any(survivingArrayTempArea > SIZETHRESHOLD & survivingArrayTempIdx,1)); ...
            oldFrameData.track_index(logical(oldFrameData.track_criticalchange)&ismember(oldFrameData.track_index, newFrameData.track_index))]);
    else
        survivingArray = unique([newFrameData.track_index(any(survivingArrayTempArea > SIZETHRESHOLD & survivingArrayTempIdx,1))]);
    end
    survivingArray = survivingArray(~ismember(survivingArray,lastFrames{f}));
    % survivors are appended to the parents and daughters
    % their divisiontype is 1 as opposed to 0 for division with a
    % disappearing parent
    daughterCandidateArray = [daughterCandidateArray; survivingArray];
    daughterCandidateType = [daughterCandidateType; ones(size(survivingArray))];
    parentCandidateArray = [parentCandidateArray; survivingArray];
    parentCandidateType = [parentCandidateType; ones(size(survivingArray))];
    
    %if daughters are present, the main body is executed
    if ~isempty(daughterCandidateArray) && ~isempty(parentCandidateArray)
        
        % Initialize Daughter Pairs: pairs between two survivor tracks are
        % not permitted. Type 0: parent disappears, Type 1: one of
        % the two daughter is asurvivor. If there is only one new track and
        % no available survivors, then only one daughtercandidate is
        % present, and thus the new track will keep the default root 0;
        daughterpairs = [];
        daughterpair_type = [];
        
        ignoreDaughterpair = ~considerDaughterPair(newFrameData,daughterCandidateArray,...
            this.configuration.ParameterConfiguration.neighbourhoodSearchRadius);
        
        for i = 1:length(daughterCandidateArray)
            for j = i:length(daughterCandidateArray)
                if (i == j) || all([daughterCandidateType(i) daughterCandidateType(j)]) || ...
                        ignoreDaughterpair(i,j)
                    continue
                else
                    daughterpairs(end+1,:) = [daughterCandidateArray(i), daughterCandidateArray(j)];
                    daughterpair_type(end+1) = daughterCandidateType(i)| daughterCandidateType(j);
                end                
            end
        end
         
        if ~isempty(daughterpairs)
            % Compute Cost Matrix for each parent/daughterpair combination. 
            % Rows: daughterpairs, Cols: Parents

            cost = this.lap.mammalianLineageCost(oldFrameData, newFrameData, daughterpairs, parentCandidateArray, daughterpair_type, parentCandidateType);

            assignment = this.lap.lapjv([cost' repmat(min(quantile(cost,0.33,[1 2]),max(cost(~isinf(cost))))+1,size(cost,2),size(cost,2))]);
            assignment = assignment(:,1:size(cost,2));
            
            %discard parents assigned to dummy pairs and the assigned
            %dummypairs.
            parentCandidateArray = parentCandidateArray(assignment <= size(daughterpairs,1));
            parentCandidateType = parentCandidateType(assignment <= size(daughterpairs,1));
            assignment = assignment(assignment <= size(daughterpairs,1));
            assignedPairs = daughterpairs(assignment,:);
            
            % if atleast one daughter has been assigned to more than one
            % parent, reverse assignment is initiated
            uniqueDaughters = unique(assignedPairs);
            if numel(uniqueDaughters) ~= numel(assignedPairs)
                
                %filter cost for only assigned
                numAssPair = size(assignedPairs,1);
%                 assignedCost = cost(assignment,1:length(assignment));
                parentDaughter = parentDaughterCost(...
                    uniqueDaughters,parentCandidateArray, ...
                    oldFrameData, newFrameData);
                %create matrix of unique daughters (rows) and their
                %occurecne in the assigned daughterpairs (colums) with the 
                %distance of the daughter to the assigned parent
                costDaughterInPair = inf(length(uniqueDaughters),numAssPair);
                for i = 1:length(uniqueDaughters)
                    for j = 1:numAssPair
                        if any(ismember(uniqueDaughters(i),assignedPairs(j,:)))
%                             if parentCandidateType(j) == 1
%                                 costDaughterInPair(i,j) =  assignedCost(j,j);
%                             else
                                costDaughterInPair(i,j) = parentDaughter(i,j);
%                             end
                        end
                    end
                end
                
                %place each daughter in the pair with the minimal cost, set
                %the rest to zero then make the placement a logical matrix
                [~, minDaughCostidx] = min(costDaughterInPair,[],2);
                minDaughCostidx = minDaughCostidx .* ~all(isinf(costDaughterInPair),2);
                for i =1:length(minDaughCostidx)
                    costDaughterInPair(i,setdiff(1:end,minDaughCostidx(i)))=nan;
                end
                costDaughterInPair = ~isnan(costDaughterInPair);
                
                %reassign daughters to the 
                assignedPairs(:,:) = 0;
                for i = 1:size(assignedPairs,1)
                    pair = uniqueDaughters(costDaughterInPair(:,i))';
                    if ~isempty(pair)
                        assignedPairs(i,1:length(pair))=pair;
                    end
                end
                parentCandidateArray = parentCandidateArray(sum(assignedPairs,2)>0);
                parentCandidateType = parentCandidateType(sum(assignedPairs,2)>0);
                assignedPairs = assignedPairs(sum(assignedPairs,2)>0,:);
                assignment = 1:size(assignedPairs,1);
            end

            % assign the track_parent to assigned daughters
            [newFrameData,splits] = this.data.assignMammalianTrackparent(...
                newFrameData, assignment, parentCandidateArray,...
                parentCandidateType, assignedPairs, f, printOutput);
            trackSplits = trackSplits + splits;
        end
    end
    
    % Update oldFrameData
    oldFrameData.track_index = newFrameData.track_index;
    oldFrameData.cell_center_x = newFrameData.cell_center_x;
    oldFrameData.cell_center_y = newFrameData.cell_center_y;
    oldFrameData.cell_center_z = newFrameData.cell_center_z;
    oldFrameData.cell_area = newFrameData.cell_area;
    oldFrameData.cell_filtered_dif_x = newFrameData.cell_filtered_dif_x;
    oldFrameData.cell_filtered_dif_y = newFrameData.cell_filtered_dif_y;
    oldFrameData.cell_filtered_dif_z = newFrameData.cell_filtered_dif_z;
    oldFrameData.track_parent = newFrameData.track_parent;
    if fluoActive
        oldFrameData.track_criticalchange = newFrameData.track_criticalchange;
    end
end
roots = unique(this.data.SegmentationData.track_index(this.data.SegmentationData.track_parent==0));
roots = roots(~isnan(roots));

%join tracks of a parent with its lone daughter.
trackJoinsIdx = find(trackSplits<0);
 if ~isempty(trackJoinsIdx) && ~printOutput
     this.data.utils.printToConsole(sprintf("Joining %d tracks:",length(trackJoinsIdx)));
     this.data.utils.printToConsole(sprintf("\t\tDaughter:\t%s",strjoin(compose("%4d",this.data.SegmentationData.track_index(trackJoinsIdx)),',')));
     this.data.utils.printToConsole(sprintf("\t\tParent:  \t%s",strjoin(compose("%4d",-trackSplits(trackJoinsIdx)),',')));
     for join  = trackJoinsIdx
         currenttrack = this.data.SegmentationData.track_index(join);
         currentparent = -trackSplits(join);
         this.data.SegmentationData.track_index(...
             this.data.SegmentationData.track_index == currenttrack) =  currentparent;
    end
%     
%     
    this.data.utils.printToConsole(sprintf("\t\tat frame:\t%s",strjoin(compose("%4d",this.data.SegmentationData.cell_frame(trackJoinsIdx)),',')));
end

%split tracks where survivors were assigned as parents (put here for easier
%parallelization
trackSplitsIdx = find(trackSplits>0);
if ~isempty(trackSplitsIdx) && ~printOutput && joinTracks
    this.data.utils.printToConsole(sprintf("Splitting %d tracks:",length(trackSplitsIdx)));
    this.data.utils.printToConsole(sprintf("\t\tTracks:\t\t%s",strjoin(compose("%4d",this.data.SegmentationData.track_index(trackSplitsIdx)),',')));
    
    nextFreeTrack = max(this.data.SegmentationData.track_index)+1;
    firstNextTrack = nextFreeTrack;
    for split  = trackSplitsIdx
        currenttrack = this.data.SegmentationData.track_index(split);
        currentframe = this.data.SegmentationData.cell_frame(split);
        this.data.SegmentationData.track_index(...
            this.data.SegmentationData.track_index == currenttrack & ...
            this.data.SegmentationData.cell_frame >= currentframe) = nextFreeTrack;
        nextFreeTrack = nextFreeTrack + 1;
    end
    
    this.data.utils.printToConsole(sprintf("\t\tnew ID:\t\t%s",strjoin(compose("%4d",firstNextTrack:(nextFreeTrack-1)),',')));
    this.data.utils.printToConsole(sprintf("\t\tat frame:\t%s",strjoin(compose("%4d",this.data.SegmentationData.cell_frame(trackSplitsIdx)),',')));
end

stable = isempty(trackSplitsIdx) && isempty(trackJoinsIdx);

if printOutput
    this.data.utils.printToConsole(sprintf("Roots (N=%d): %s",length(roots),strjoin(compose("%3d",roots),',')));
    this.data.utils.printToConsole("Lineage reconstruction complete.");
end
end

%%daughterDistance computes the distance between all daughters to
%%determine whether they should be considered or not
function considerMat = considerDaughterPair(newFrame, daughterCandidates, searchRadius)
    numDaughers = length(daughterCandidates);
    [~,Idx] = ismember(daughterCandidates,newFrame.track_index);
    
    considerMat = sqrt( ( repmat(newFrame.cell_center_x(Idx)',numDaughers,1) - repmat(newFrame.cell_center_x(Idx),1,numDaughers) ) .^2 + ...
        ( repmat(newFrame.cell_center_y(Idx)',numDaughers,1) - repmat(newFrame.cell_center_y(Idx),1,numDaughers )) .^2 + ...
        ( repmat(newFrame.cell_center_z(Idx)',numDaughers,1) - repmat(newFrame.cell_center_z(Idx),1,numDaughers )) .^2 );
    considerMat = considerMat < searchRadius;
end

%%computes the distance between daughters and parents
function dist = parentDaughterCost(...
    uniqueDaughters,parentCandidateArray,oldFrame,newFrame)
numDaughers = length(uniqueDaughters);
[~,dIdx] = ismember(uniqueDaughters,newFrame.track_index);
numParents = length(parentCandidateArray);
[~,pIdx] = ismember(parentCandidateArray,oldFrame.track_index);

dist = sqrt( ( repmat(oldFrame.cell_center_x(pIdx)',numDaughers,1) - repmat(newFrame.cell_center_x(dIdx),1,numParents) ) .^2 + ...
        ( repmat(oldFrame.cell_center_y(pIdx)',numDaughers,1) - repmat(newFrame.cell_center_y(dIdx),1,numParents )) .^2 + ...
        ( repmat(oldFrame.cell_center_z(pIdx)',numDaughers,1) - repmat(newFrame.cell_center_z(dIdx),1,numParents )) .^2 );
end
                