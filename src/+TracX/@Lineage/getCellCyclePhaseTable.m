%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ t ] = getCellCyclePhaseTable(this)
% GETCELLCYCLEPHASETABLE Returns the full cell cycle phase table.
%
% Args:
%    this (:obj:`object`):                     :class:`Lineage` instance  
%
% Returns
% -------
%    t :obj:`table`                                
%                                             Full cell cycle table with
%                                             columns 'track', 'parent', 
%                                             'daughter', 'tree' ...
%                                             'generation', 'age', 'pole1_age',
%                                             'pole2_age', 'g1_st', 'g1_e', ...
%                                             'g1_dur', 'g2_st', 'g2_e', 'g2_dur',
%                                             'div', 'seg_st_track', 
%                                             'seg_st_daughter'.
%                                             Abbreviations st, start, e,
%                                             end, dur, duration, div,
%                                             division, seg_st,
%                                             segmentation start.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

dd = [];
tracks = unique(this.data.getFieldArray('track_index'))';
trackIndexArray = this.data.getFieldArray('track_index');
trackParentArray = this.data.getFieldArray('track_parent');

if this.configuration.ProjectConfiguration.cellDivisionType == 0
    % Get lineage information for symetrically dividing cells
    for track = tracks
        
        lin = unique(this.data.getFieldArrayForTrackIndex(...
            'track_lineage_tree', track));
        lin(isnan(lin(1:end-1))) = [];
        
        lineageMembersArray = this.getRootTrackRelatedDaughterArray(...
            trackIndexArray, trackParentArray, track);
        lineageMembersArray = [track, lineageMembersArray];
        
        for stIdx = 1:numel(lineageMembersArray)
            lMember = lineageMembersArray(stIdx);
            seg_st_track = unique(this.data.getFieldArrayForTrackIndex(...
                'track_start_frame', lMember));
            daughterArray = unique(this.data.getConditionalFieldArray(...
                'track_index', 'track_parent', lMember));
            generationArray = unique(this.data.getFieldArrayForTrackIndex(...
                'track_generation', lMember));
            generationArray(isnan(generationArray)) = [];
            if isempty(generationArray)
                generation = nan;
            else
                generation = generationArray;
            end
            % Get information for each daughter of 'cell'
            if ~isempty(daughterArray)
                for d = 1:numel(daughterArray)
                    daughter = daughterArray(d);
                    parent = unique(this.data.getFieldArrayForTrackIndex(...
                        'track_parent', lMember));
                    pl1_age = unique(this.data.getFieldArrayForTrackIndex(...
                        'cell_pole1_age', lMember));
                    pl1_age(isnan(pl1_age(1:end-1))) = [];
                    pl2_age = unique(this.data.getFieldArrayForTrackIndex(...
                        'cell_pole2_age', lMember));
                    pl2_age(isnan(pl2_age(1:end-1))) = [];
                    seg_st_daughter = unique(this.data.getFieldArrayForTrackIndex(...
                        'track_start_frame', daughter));
                    div = seg_st_daughter - 1;
                    % Age and cell cycle phases will be set to nan
                    row = [lMember, parent, daughter, lin, generation, nan, ...
                        pl1_age, pl2_age, nan, nan, nan, nan, nan, nan, ...
                        div, seg_st_track, seg_st_daughter];
                    dd = [dd; row];
                end
            else
                % Cells did not divide yet so there is no daughter cells
                % nor division information to be set
                daughter = nan;
                parent = unique(this.data.getFieldArrayForTrackIndex(...
                    'track_parent', lMember));
                pl1_age = unique(this.data.getFieldArrayForTrackIndex(...
                    'cell_pole1_age', lMember));
                pl1_age(isnan(pl1_age(1:end-1))) = [];
                pl2_age = unique(this.data.getFieldArrayForTrackIndex(...
                    'cell_pole2_age', lMember));
                pl2_age(isnan(pl2_age(1:end-1))) = [];
                seg_st_daughter = nan;
                div = seg_st_daughter - 1;
                % Age and cell cycle phases will be set to nan
                row = [lMember, parent, daughter, lin, generation, nan, ...
                    pl1_age, pl2_age, nan, nan, nan, nan, nan, nan, ...
                    div, seg_st_track, seg_st_daughter];
                dd = [dd; row];
            end
        end
    end
elseif this.configuration.ProjectConfiguration.cellDivisionType == 1
    % Get lineage information for asymetrically dividing cells
    if all(isnan(this.data.getFieldArray('track_cell_cycle_phase')))
        for track = tracks
            
            frameArray = this.data.getFieldArrayForTrackIndex('cell_frame', track);
            phaseArray = this.data.getFieldArrayForTrackIndex('track_cell_cycle_phase', track);
            generationArray = this.data.getFieldArrayForTrackIndex('track_generation', track);
            ageArray = this.data.getFieldArrayForTrackIndex('track_age', track);
            parentArray = this.data.getFieldArrayForTrackIndex('track_parent', track);
            seg_st_track = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', track));
            lin = unique(this.data.getFieldArrayForTrackIndex('track_lineage_tree', track));
            lin(isnan(lin(1:end-1))) = [];
            generationArray(isnan(generationArray)) = [];
            if isempty(generationArray)
                gen = nan;
            else
                gen = unique(generationArray);
            end
            parent = unique(parentArray);
            
            childStarts =  this.data.TmpLinAss.markerTrackStartFrame(this.data.TmpLinAss.motherCell == track);
            childEnds =  this.data.TmpLinAss.markerTrackEndFrame(this.data.TmpLinAss.motherCell == track);
            childArray =  this.data.TmpLinAss.daughterCell(this.data.TmpLinAss.motherCell == track);
            
            for stIdx = 1:numel(childStarts)
                
                p2S = childStarts(stIdx);
                p2E = childEnds(stIdx);
                dp2 = (p2E - p2S) + 1;
                daughter = childArray(stIdx);
                seg_st_daughter = unique(this.data.getFieldArrayForTrackIndex(...
                    'track_start_frame', daughter));
                % Pole age has no interpretation therefore we set it to nan
                row = [track, parent, daughter, lin, gen, stIdx, nan, nan, ...
                    nan, nan, nan, p2S, p2E, dp2, p2E, seg_st_track, ...
                    seg_st_daughter];
                dd = [dd; row];
            end
            
        end
    else
        for track = tracks
            frameArray = this.data.getFieldArrayForTrackIndex('cell_frame', track);
            phaseArray = this.data.getFieldArrayForTrackIndex('track_cell_cycle_phase', track);
            generationArray = this.data.getFieldArrayForTrackIndex('track_generation', track);
            ageArray = this.data.getFieldArrayForTrackIndex('track_age', track);
            parentArray = this.data.getFieldArrayForTrackIndex('track_parent', track);
            seg_st_track = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', track));
            lin = unique(this.data.getFieldArrayForTrackIndex('track_lineage_tree', track));
            lin(isnan(lin(1:end-1))) = [];
            
            
            childStarts =  this.data.TmpLinAss.markerTrackStartFrame(this.data.TmpLinAss.motherCell == track);
            childEnds =  this.data.TmpLinAss.markerTrackEndFrame(this.data.TmpLinAss.motherCell == track);
            childArray =  this.data.TmpLinAss.daughterCell(this.data.TmpLinAss.motherCell == track);
            
            uGen = unique(generationArray);
            uAge = unique(ageArray);
            parent = unique(parentArray);
            if all(isnan(parent)) == 1
                parent = nan;
            end
            
            % @todo loop over daughters and get phase by checking which frames
            % the daughters are present until division
            p1 = phaseArray';
            p2 = phaseArray';
            
            p1(phaseArray > 1) = 0;
            p1StartIdx = strfind([0 p1 0],[0 1]);
            p1EndIdx  = strfind([0 p1 0],[1 0]) - 1;
            
            p2(phaseArray < 2) = 0;
            p2(phaseArray >= 2) = 2;
            p2StartIdx = strfind([0 p2 0],[0 2]);
            p2EndIdx  = strfind([0 p2 0],[2 0]) - 1;
            
            for a = 1:numel(p1StartIdx)
                
                childFrameArray = cell(size(childArray));
                childFrameMinDist = zeros(size(childArray));
                for cIdx = 1:numel(childArray)
                    if a <= numel(p2StartIdx)
                        p2Frames = frameArray(p2StartIdx(a):p2EndIdx(a));
                    else
                        p2Frames = 0;
                    end
                    childFrameArray{cIdx} = this.data.getFieldArrayForTrackIndex('cell_frame', childArray(cIdx));
                    childFrameMinDist(cIdx) = abs(min(childFrameArray{cIdx}) - min(p2Frames));
                end
                
                if ~isempty(p1StartIdx) && size(p1StartIdx, 2) >= a
                    gen = generationArray(p1StartIdx(a));
                elseif ~isempty(p2StartIdx) && size(p2StartIdx, 2) >= a
                    gen = generationArray(p2StartIdx(a));
                else
                    gen = nan;
                end
                
                if ~isempty(p1StartIdx) && ~isempty(p1EndIdx) && size(p1StartIdx, 2) >= a
                    p1S = frameArray(p1StartIdx(a));
                    p1E = frameArray(p1EndIdx(a));
                    dp1 = (p1E - p1S) + 1;
                else
                    dp1 = nan;
                    p1S = nan;
                    p1E = nan;
                end
                if ~isempty(p2StartIdx) && ~isempty(p2EndIdx) && size(p2StartIdx, 2) >= a
                    p2S = frameArray(p2StartIdx(a));
                    p2E = frameArray(p2EndIdx(a));
                    dp2 = (p2E - p2S) + 1;
                else
                    dp2 = nan;
                    p2S = nan;
                    p2E = nan;
                end
                
                dIdx = find(childFrameMinDist == min(childFrameMinDist));
                if ~isempty(dIdx) && numel(dIdx) == 1 && dIdx < dp2
                    daughter = childArray(dIdx);
                    seg_st_daughter = unique(this.data. ...
                        getFieldArrayForTrackIndex('track_start_frame', daughter));
                else
                    daughter = nan;
                    seg_st_daughter = nan;
                end
                % Pole age has no interpretation therefore we set it to nan
                row = [track, parent, daughter, lin, gen, a, nan, nan, ...
                    p1S, p1E, dp1, p2S, p2E, dp2, p2E, seg_st_track, ...
                    seg_st_daughter];
                dd = [dd; row];
            end
        end
    end
else
    t = [];
    return % no valid division type set, return
end

t = array2table(dd, 'VariableNames',{'track', 'parent', 'daughter', 'tree' ...
    'generation', 'age', 'pole1_age', 'pole2_age', 'g1_st', 'g1_e', ...
    'g1_dur', 'g2_st', 'g2_e', 'g2_dur', 'div', 'seg_st_track', ...
    'seg_st_daughter'});

end
