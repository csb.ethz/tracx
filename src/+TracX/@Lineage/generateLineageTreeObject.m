%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ lineageTreeArray ] = generateLineageTreeObject(this, varargin )
% GENERATELINEAGETREEObject Generates a lineage tree object
% for one or multiple root cells (i.e. cells on the first image frame).
%
% Args:
%    this (:obj:`object`):                               :class:`Lineage` instance 
%    varargin (:obj:`str varchar`):
%
%        * **rootIndicies** (:obj:`array`):              Array of lineage 
%                                                        root track indices.
%  
% Returns
% -------
%    lineageTreeArray (:obj:`array`)
%                                                        Array containing 
%                                                        a lineage tree
%                                                        object for each
%                                                        rood index. 
%
% .. note:: 
%
%    The returned tree object can be visualized in a figure or as text in
%    the console with i.e lineageTreeArray{1}.tostring.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if ~isempty(this.data.getFieldArray('track_index'))
    
    validRootIndices = {'>',0,'<',max(this.data.getFieldArray('track_index'))};
    defaultRootIndices = [];
    
    p = inputParser;
    p.addRequired('this', @isobject);
    p.addOptional('rootIndicies', defaultRootIndices, @(x)validateattributes(x, ...
        {'double'}, validRootIndices, 'Rankings'));
    parse(p, this, varargin{:})
    
    if isempty(p.Results.rootIndicies)
        startFrameTrackIndexArray = this.data.getFieldArrayForFrame(...
            'track_index', this.configuration. ...
            ProjectConfiguration.trackerStartFrame)';
    else
        startFrameTrackIndexArray = p.Results.rootIndicies;
    end
    
    trackIndexArray = this.data.getFieldArray('track_index');
    trackParentArray = this.data.getFieldArray('track_parent');
    
    lineageTreeArray = cell(numel(startFrameTrackIndexArray), 1);
    for iRoot = startFrameTrackIndexArray
        
        % Get all related cells
        toRootTrackRelatedCellArray = this.getRootTrackRelatedDaughterArray(...
            trackIndexArray, trackParentArray, iRoot );
        % Construct lineage tree object
        lineageTreeArray{iRoot, 1} = this.getRootTrackTreeObject( trackIndexArray, ...
            trackParentArray, iRoot, toRootTrackRelatedCellArray );
    end
else
    this.utils.printToConsole('Please runTracker first! No lineage plotted.')
end
end
