%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [durationTreeObj, phaseStartIdxNew, phaseEndIdxNew] = ...
    getTreeNodeDurations(this, lineageTree, firstFrame, lastFrame, imageFreq)
% GETTREENODEDURATIONS Returns a tree object containing the cell cycle
% duration until cell division.
%
% Args:
%    this (:obj:`object`):                        :class:`Lineage` instance. 
%    lineageTree (:obj:`obj`):                    Lineage tree
%    firstFrame (:obj:`int`):                     First frame of experiment 
%    lastFrame (:obj:`int`):                      Last frmae of experiment
%    imageFreq (:obj:`float`):                    Image frequency [h].
%
% Returns
% -------
%    durationTreeObj :obj:`obj` 
%                                                 Duration tree object.
%    phaseStartIdxNew :obj:`array` 
%                                                 Array with cell cycle
%                                                 phase start indices.
%    phaseEndIdxNew :obj:`array` 
%                                                 Array with cell cycle
%                                                 phase end indices.
%
% :Authors:
%    Andreas P. Cuny - initial implementation


cellFrameArray = this.data.getFieldArray('cell_frame');
trackParentArray = this.data.getFieldArray('track_parent');
plottingFrameRange = firstFrame:lastFrame;
parentNodes = lineageTree.Parent;
uniqueParentNodes = unique(parentNodes);
nodes = cell2mat(lineageTree.Node);
division = zeros(numel(nodes), 1);
for j = uniqueParentNodes'
    if j == 0
%         [ ~, phaseEndIdx ] = getCellCyclePhaseIndicies(ismember(...
%             plottingFrameRange, cellFrameArray(trackParentArray == 1)));
%         division(parentNodes==j) = phaseEndIdx(1);
    else
        [ phaseStartIdx, phaseEndIdx ] = getCellCyclePhaseIndicies(ismember(...
            plottingFrameRange, cellFrameArray(trackParentArray == nodes(j))));
        phaseStartIdxNew = [];
        phaseEndIdxNew = [];
        k = 1;
        maxIdx = numel(phaseStartIdx);
        while k < maxIdx
            if phaseStartIdx(k+1)-phaseEndIdx(k) <=2
                phaseStartIdxNew = [phaseStartIdxNew, phaseStartIdx(k)];
                phaseEndIdxNew = [phaseEndIdxNew, phaseEndIdx(k+1)];
                k = k + 2;
            else
                phaseStartIdxNew = [phaseStartIdxNew, phaseStartIdx(k)];
                phaseEndIdxNew = [phaseEndIdxNew, phaseEndIdx(k)];
                k = k + 1;
            end
            if k == maxIdx
                phaseStartIdxNew = [phaseStartIdxNew, phaseStartIdx(k)];
                phaseEndIdxNew = [phaseEndIdxNew, phaseEndIdx(k)];
            end
        end
        if numel(phaseEndIdx) == 1
            phaseEndIdxNew = phaseEndIdx;
        end
        division(parentNodes==j) = phaseEndIdxNew;
    end
end

% Create duration tree object for each node
durationTreeObj = TracX.ExternalDependencies.tree.tree(lineageTree, ...
    'clear');
trackNodes = cell2mat(lineageTree.Node);
divisionTimes = (division+1).*(imageFreq/60);
for i = 1:numel(trackNodes)
    durationTreeObj = durationTreeObj.set(i, divisionTimes(i));
end

end
