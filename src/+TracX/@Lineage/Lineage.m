%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef Lineage < handle
    % LINEAGE Implements functionalities to reconstruct the cell lineage
    % for asymmetrically and symetrically dividing cell types.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
         
        data; % :class:`+TracX.@TrackerData` instance stores all segmentation, quantification and lineage data.
        configuration; % :class:`+TracX.@TrackerConfiguration` TrackerConfiguration instance with the tracking parameters and project metadata.
        lineageData; % :class:`+TracX.@LineageData` instance implementing temporary lineage data storage.
        unresolvedTempLinAss % :class:`+TracX.@TemporaryLineageAssignment` instance implementing temporary assignments methods. 
        utils % :class:`+TracX.@Utils` instance implementing utility methods.
        io % :class:`+TracX.@IO` instance implementing read/write methods.
        
        lineageTable % Lineage summary table.

        lap % :class:`+TracX.@LAP` instance implementing tracking methods. 
        motion % :class:`+TracX.@Motion` instance implementing motion methods.
        fingerprint  % :class:`+TracX.@Fingerprint` instance implementing fingerprinting methods.
        
        cellCyclePhaseTable % Cell cycle phases
        tracks2Merge % Matrix with info about mother and buds to merge prior division
    
    end
    
    methods
        
        % Constructor
        function obj = Lineage(data, config)
            % LINEAGE Constructs a Lineage object to reconstruct the
            % generalogy for asymmetric and symmetric dividing cells.
            %
            % Returns
            % -------
            %    obj (:obj:`-`)
            %                           Returns a :class:`Lineage`  object 
            %                           instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj.data = data;
            obj.configuration = config;
            obj.lineageData = TracX.LineageData();
            obj.utils = TracX.Utils();
            obj.io = TracX.IO(obj.utils);
            obj.lap = TracX.LAP();
            obj.motion = TracX.Motion();
            obj.fingerprint = TracX.Fingerprint(obj.utils, obj.io);
        end
        
        function runLineageReconstruction(this, varargin )
        % RUNLINEAGERECONSTRUCTION Starts lineage reconstruction based on 
        % tracking results for symmetrically and asymmetrically dividing cells.
        %
        % Args:
        %    this (:obj:`object`):              :class:`Lineage` instance
        %    varargin (:obj:`str varchar`):
        %
        %        * **symmetricalDivision** (:obj:`bool`):    Flag, defining which lineage reconstruction approach to use. Defaults to is asymmetrical divison (0).
        %        * **writeControlImages** (:obj:`bool`):     Flag, to save lineage control images segmentation are written out. Defaults to false.
        %        * **nuclearMarkerChannel** (:obj:`int`):    Fluorescent channel number depicting a nuclear marker.
        %        * **maxFrameEvalAfterBirth** (:obj:`int`):  Max number of frames after brith to consider a MD pair.
        %
        % Returns
        % -------
        %    void (:obj:`-`)
            
            % Parse arguments. Default is asymmetrical division (SymmetricalDivision:
            % 0)
            p = inputParser;
            p.KeepUnmatched = true;
            defaultNuclearMarkerChannel = nan;
            defaultMaxFrameEvalAfterBirth = 20;
            p.addParameter('symmetricalDivision', false, @(x)validateattributes(x, ...
                {'logical'}, {'scalar'}))
            p.addParameter('writeControlImages', false, @(x)validateattributes(x, ...
                {'logical'}, {'scalar'}))
            p.addOptional('nuclearMarkerChannel', defaultNuclearMarkerChannel, ...
                @(x) isnumeric(x) || isnan(x))
            p.addOptional('maxFrameEvalAfterBirth', defaultMaxFrameEvalAfterBirth, ...
                @(x) isnumeric(x) && ~isnan(x) && x > 1)           
            p.parse(varargin{:});
            
                        
            if p.Results.symmetricalDivision == 1 % Lineage reconstruction for symmetrically
                % dividing cells
                
                this.symmetricalLineageReconstruction();
                % Set finished flag
                this.data.TmpLinAss.reconstructionIsFinished = true;
                
                
            else % Lineage reconstruction for asymmetrically dividing cells
                this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                    ' Selected assymmetrical cell division']);
                
                this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                    ' Start cell lineage reconstruction']);
                
                lineProfilesRaw = {};
                lineProfilesRe = {};
                
                % Initialized lineage data fields
                this.data.initializeLineageDataFields();
                
                this.lineageData = TracX.LineageData();
                
                % for frame, do an mother-daugther assignment loop
                this.data.TmpLinAss.maxFrameEvalAfterBirth = ...
                    p.Results.maxFrameEvalAfterBirth;
                 
                % Get segmetation bounding box
                offset = this.configuration.ParameterConfiguration.meanCellDiameter;
                xc = this.data.getFieldArrayForFrame('cell_center_x', this.configuration. ...
                    ProjectConfiguration.trackerEndFrame);
                yc = this.data.getFieldArrayForFrame('cell_center_y', this.configuration. ...
                    ProjectConfiguration.trackerEndFrame);
                
                if ~isempty(this.configuration.ProjectConfiguration. ...
                        imageCropCoordinateArray)
                    imageCropCoordinateArray = this.configuration.ProjectConfiguration. ...
                        imageCropCoordinateArray;
                else
%                     [bx, by, bw, bh] = this.utils.getSegAreaBoundingBox(xc, yc, offset);
%                     cropArray = [bx, by, bw, bh];
%                     imageCropCoordinateArray = cropArray;
                      imageCropCoordinateArray = [];
                end
                
                % Set G1 signal
                if ~isnan(p.Results.nuclearMarkerChannel)
                    this.setSignalInNucleus(p.Results.nuclearMarkerChannel, ...
                        p.Results.writeControlImages);
                else
                    this.utils.printToConsole(...
                        'WARNING: nuclearMarkerChannel is not defined. No G1 phase will be determined.')
                end
                
                pBarO = this.utils.progressBar(numel(this.configuration. ...
                    ProjectConfiguration.fluoImageIdentifier), ...
                    'UpdateRate', 5, ...
                    'Title', 'segmenting and quantifying cell division markers', ...
                    'KeepTimer', true ...
                    );
                
                pBarO.setup([], [], []);
                % CHANNEL_ID = 1;
                for channel = this.configuration.ProjectConfiguration. ...
                        fluoImageIdentifier'
                    try
                        selChl = strfind(this.configuration.ProjectConfiguration. ...
                            fluoImageIdentifier, channel);
                        chlIdx = find(~cellfun('isempty', selChl), 1);
                        
                        pBarI = this.utils.progressBar(this.configuration. ...
                            ProjectConfiguration.trackerEndFrame - 1, ...
                            'UpdateRate', 5, ...
                            'Title', sprintf(...
                            'segmenting and quantifying cell division marker %s', ...
                            channel{1}), 'KeepTimer', true);
                        pBarI.setup([], [], []);
                        for frameNumber = this.configuration.ProjectConfiguration. ...
                                trackerStartFrame:this.configuration.ProjectConfiguration. ...
                                trackerEndFrame - 1
                            % NOTE: This part could be parallelized and
                            % TemporaryLineageAssignments merged after
                            % denoising all images
                            try
                                
                                %CHANNEL_ID = 1;
                                [foundEdgesFilledMaskedLabeled, segMaskDilated, ...
                                    fluoImgDenoised, this, segMaskRaw] = this.segmentBudNeck(...
                                    frameNumber, chlIdx, imageCropCoordinateArray);
                                
                                trackIndexArray = this.data.getFieldArrayForFrame('track_index', ...
                                    frameNumber);
                                x = this.data.getFieldArrayForFrame('cell_center_x', frameNumber);
                                y = this.data.getFieldArrayForFrame('cell_center_y', frameNumber);
                                
%                                 if isempty(this.configuration.ProjectConfiguration. ...
%                                         imageCropCoordinateArray)
%                                     x = x - cropArray(1);
%                                     y = y - cropArray(2);
%                                 end
                                
                                if p.Results.writeControlImages
                                    this.io.writeBudNeckCellSegmentationMaskImage( segMaskDilated, ...
                                        foundEdgesFilledMaskedLabeled, trackIndexArray, ...
                                        x, y, this.configuration.ProjectConfiguration. ...
                                        segmentationResultDir, frameNumber, ...
                                        imageCropCoordinateArray)
                                end
                                %----%
                                % 1. Find segmented budnecks that are present in COI and one of its
                                % neighbour. If true might be related.
                                % 2. Draw line profile a. has to be above thres, b. has to have a segmented
                                % budneck marker to be related
                                % 3. what to do with cases that have more than one budneck at the
                                % intersection?
                                % 4. Assign relationship only for a. first b. few folloing frames after
                                % track start of new buds?
                                trackStartFrame = this.data.getFieldArrayForFrame('track_start_frame', ...
                                    frameNumber);
                                trackArray = trackIndexArray(trackStartFrame <= frameNumber); %trackIndexArray(and(trackStartFrame <= frameNumber, trackStartFrame > 1));
                                % @todo import start frame number as it might not start on frame 1!
                                %trackArray = trackIndexArray(and(trackStartFrame <= frameNumber, ...
                                %    trackStartFrame >= (frameNumber - 5)));
                                % trackArray = trackIndexArray(ismember(trackStartFrame,frameNumber));
                                
                                maskLabels = unique(foundEdgesFilledMaskedLabeled);
                                maskLabels(1) = [];
                                
                                for k = 1:numel(maskLabels)
                                    
                                    try
                                        %> @todo save the k (mask label into temp
                                        %assignment) then later do the connection and
                                        %interpolate using signal to check when it goes
                                        %below background level also interpolate mask for
                                        %missing frames
                                        iLabel = maskLabels(k);
                                        budTrackIntersections = unique(segMaskDilated(foundEdgesFilledMaskedLabeled == iLabel));
                                        % If not empty and there are zeros, remove zeros.
                                        % Assign track_indices whereas larger area track will be mother
                                        budTrackIntersections = budTrackIntersections(budTrackIntersections~=0);
                                        
                                        if size(budTrackIntersections, 1) == 2
                                            [ validPair, lRe, lRaw ] = this.getLineProfileVerification( ...
                                                fluoImgDenoised, x(ismember(trackIndexArray, budTrackIntersections)), ...
                                                y(ismember(trackIndexArray, budTrackIntersections)), 1, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfileLength, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakLowerBound,...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakUpperBound);
                                            lineProfilesRaw{end+1} = lRaw;
                                            if size(lRe, 1) < size(lRe, 2)
                                                lineProfilesRe{end+1} = lRe';
                                            else
                                                lineProfilesRe{end+1} = lRe;
                                            end
                                            % Do assignment
                                            if validPair == 1
                                                %> @todo: Evaluate all Tracks from which had a
                                                %> track start in the past 4 frames and put them
                                                %> into a temp array after 4frames save the one with
                                                %> the highest occurance as mother.
                                                if ~isempty(trackArray)
                                                    daughter = this.data.getDaugterTrack(...
                                                        budTrackIntersections, frameNumber);
                                                    %                     daughter = budTrackIntersections(ismember(...
                                                    %                         budTrackIntersections,trackArray));
                                                    % smaller in size and born at later time! except for
                                                    % first frame! only size!
                                                    if ~isempty(daughter)
                                                        daughterF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', daughter));
                                                        mother = budTrackIntersections(...
                                                            budTrackIntersections~=daughter);
                                                        if ~isempty(mother)
                                                            motherF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', mother));
                                                            %[Assignment, this.data.TmpLinAss] = ...
                                                                this.data.TmpLinAss. ...
                                                                setMotherDaughterPair(...
                                                                daughter, mother, frameNumber, ...
                                                                validPair, lRe, daughterF, motherF);
                                                            % this.data.TmpLinAss = 
                                                            this.data.TmpLinAss.setMarker(...
                                                                iLabel, chlIdx, daughter);
%                                                             this.data.TmpLinAss.setSigInNuc(...
%                                                                 sig, daughter);
                                                        end
                                                    end
                                                end
                                            end
                                        elseif size(budTrackIntersections, 1) > 2
                                            budMask = foundEdgesFilledMaskedLabeled;
                                            budMask(budMask ~= iLabel) = 0;
                                            maxEval = 15; % @todo make it to be half of the mean cell diameter
                                            budMaskEroded = budMask;
                                            while maxEval ~= 0
                                                budMaskEroded = imerode(budMaskEroded, strel('disk', 1));
                                                budTrackIntersections = unique(segMaskDilated(budMaskEroded == iLabel));
                                                budTrackIntersections = budTrackIntersections(budTrackIntersections~=0);
                                                if size(budTrackIntersections, 1) > 2
                                                    maxEval = maxEval - 1;
                                                else
                                                    maxEval = 0;
                                                end
                                            end
                                            % If condition is met. Veryfy with line profiles. If
                                            % positive do assignments. If negative go to next one.
                                            [ validPair, lRe, lRaw ] = this.getLineProfileVerification( ...
                                                fluoImgDenoised, x(ismember(trackIndexArray, budTrackIntersections)), ...
                                                y(ismember(trackIndexArray, budTrackIntersections)), 1, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfileLength, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakLowerBound,...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakUpperBound);
                                            lineProfilesRaw{end+1} = lRaw;
                                            if size(lRe, 1) < size(lRe, 2)
                                                lineProfilesRe{end+1} = lRe';
                                            else
                                                lineProfilesRe{end+1} = lRe;
                                            end
                                            
                                            if validPair == 1
                                                if ~isempty(trackArray)
                                                    daughter = this.data.getDaugterTrack(...
                                                        budTrackIntersections, frameNumber);
                                                    %                         daughter = budTrackIntersections(ismember(...
                                                    %                             budTrackIntersections,trackArray));
                                                    if ~isempty(daughter)
                                                        daughterF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', daughter));
                                                        mother = budTrackIntersections(...
                                                            budTrackIntersections~=daughter);
                                                        if ~isempty(mother)
                                                            motherF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', mother));
                                                            % [Assignment, this.data.TmpLinAss] = ...
                                                                this.data.TmpLinAss. ...
                                                                setMotherDaughterPair(...
                                                                daughter, mother, frameNumber, ...
                                                                validPair, lRe, daughterF, motherF);
                                                            % this.data.TmpLinAss = 
                                                            this.data.TmpLinAss.setMarker(...
                                                                iLabel, chlIdx, daughter);
                                                        end
                                                    end
                                                end
                                            end
                                            
                                        elseif size(budTrackIntersections, 1) < 2
                                            budMask = foundEdgesFilledMaskedLabeled;
                                            budMask(budMask ~= iLabel) = 0;
                                            maxEval = 15; % @todo make it to be half of the mean cell diameter
                                            budMaskDilated = budMask;
                                            while maxEval ~= 0
                                                budMaskDilated = imdilate(budMaskDilated, strel('disk', 1));
                                                budTrackIntersections = unique(segMaskDilated(budMaskDilated == iLabel));
                                                budTrackIntersections = budTrackIntersections(budTrackIntersections~=0);
                                                if size(budTrackIntersections, 1) < 2
                                                    maxEval = maxEval - 1;
                                                else
                                                    maxEval = 0;
                                                end
                                            end
                                            % If condition is met. Veryfy with line profiles. If
                                            % positive do assignments. If negative go to next one.
                                            [ validPair, lRe, lRaw ] = this.getLineProfileVerification( ...
                                                fluoImgDenoised, x(ismember(trackIndexArray, budTrackIntersections)), ...
                                                y(ismember(trackIndexArray, budTrackIntersections)), 1, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfileLength, ...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakLowerBound,...
                                                this.configuration.ParameterConfiguration.divisionMarkerLineProfilePeakUpperBound);
                                            lineProfilesRaw{end+1} = lRaw;
                                            if size(lRe, 1) < size(lRe, 2)
                                                lineProfilesRe{end+1} = lRe';
                                            else
                                                lineProfilesRe{end+1} = lRe;
                                            end
                                            if validPair == 1
                                                if ~isempty(trackArray)
                                                    daughter = this.data.getDaugterTrack(...
                                                        budTrackIntersections, frameNumber);
                                                    %                         daughter = budTrackIntersections(ismember(...
                                                    %                             budTrackIntersections,trackArray));
                                                    if ~isempty(daughter)
                                                        daughterF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', daughter));
                                                        mother = budTrackIntersections(...
                                                            budTrackIntersections~=daughter);
                                                        if ~isempty(mother)
                                                            if numel(mother) > 1
                                                                for iMother = mother'
                                                                    motherF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', iMother));
                                                                    % [Assignment, this.data.TmpLinAss] = ...
                                                                    this.data.TmpLinAss. ...
                                                                        setMotherDaughterPair(...
                                                                        daughter, iMother, frameNumber,...
                                                                        validPair, lRe, daughterF, motherF);
                                                                    % this.data.TmpLinAss = ...
                                                                    this.data.TmpLinAss. ...
                                                                        setMarker(iLabel, chlIdx, ...
                                                                        daughter);
                                                                end
                                                            else
                                                                motherF = unique(this.data.getFieldArrayForTrackIndex('track_start_frame', mother));
                                                                % [Assignment, this.data.TmpLinAss] = ...
                                                                this.data.TmpLinAss. ...
                                                                    setMotherDaughterPair(...
                                                                    daughter, mother, frameNumber,...
                                                                    validPair, lRe, daughterF, motherF);
                                                                % this.data.TmpLinAss = ...
                                                                this.data.TmpLinAss. ...
                                                                    setMarker(iLabel, chlIdx, ...
                                                                    daughter);
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                            
                                        end
                                        
                                    catch ME
                                        this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                                        for i =1:numel(ME.stack)
                                            this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                                            this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                                            this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                                        end
                                        continue
                                    end
                                    
                                    %                         this.utils.printTimeLeft('Segmenting and quantifying cell division marker', ...
                                    %                             frameNumber, this.configuration.ProjectConfiguration. ...
                                    %                             trackerEndFrame - 1)
                                end
                                
                            catch ME
                                this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                                for i =1:numel(ME.stack)
                                    this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                                    this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                                    this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                                end
                                continue
                            end
                            pBarI.step(1, [], []);
                        end
                        pBarI.release();
                        
                        % this.data.TmpLinAss = 
                        this.data.TmpLinAss.determineMotherCell();
                        
                        % Run marker tracking
                        this.runMarkerTracking(varargin{:})
                                               
                        % Match MD pairs with tracks to determine start and end
                        % of this phase
                        %this.data.TmpLinAss = 
                        this.data.TmpLinAss.setMarkerTracks(this.lineageData);
                        
                        % Validate temporary MD lineage assignments
                        % @TODO fix this validation
                        this.validateTmpLinAss();
                        
                        % Set cell parent and has bud
                        this.setParentAndHasBud();
                        
                        % Set budneck signal
                        mothers = this.data.TmpLinAss.motherCell;
                        markerTrack = this.data.TmpLinAss.markerTrack;
                        for k = 1:numel(mothers)
                            frames = this.lineageData.getConditionalFieldArray(...
                                'obj_frame', 'obj_track_index', markerTrack(k));
                            sig = this.lineageData.getConditionalFieldArray(...
                                'obj_fluo_total', 'obj_track_index', markerTrack(k));
                            this.data.setFieldnameValueForTrackSubset('track_budneck_total', ...
                                mothers(k), frames, sig)
                        end
                        
                        this.data.TmpLinAss.reconstructionIsFinished = true;
                        
                    catch ME
                        this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
                        for i =1:numel(ME.stack)
                            this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
                            this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
                            this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
                        end
                        continue
                    end
                    pBarO.step(1, [], []);
                end
                pBarO.release();
                
                if this.data.TmpLinAss.reconstructionIsFinished
                    
                     this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                        ' Determine track generation']);
                    % Set track generation and lineage id
                    this.setTrackGeneration();
                    
%                     % Set G1 signal
%                     if ~isnan(p.Results.nuclearMarkerChannel)
%                     this.setSignalInNucleus(p.Results.nuclearMarkerChannel, ...
%                         p.Results.writeControlImages);
%                     else
%                         this.utils.printToConsole(...
%                             'WARNING: nuclearMarkerChannel is not defined. No G1 phase will be determined.')
%                     end
                    
                    % Set the G2 phase
                    this.setBirthToDivPhase();
                    
                    this.setTrackAge()
                    
                    % Get cell cycle phases
                    this.cellCyclePhaseTable = this.getCellCyclePhaseTable();
                    
                    this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                        ' Cell lineage reconstruction done!']);
                else
                    this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                        ' Finished with ERRORS']);
                end
                
            end
        end
        
        function symmetricalLineageReconstruction(this)
            % SYMMETRICALLINEAGERECONSTRUCTION Runs the symmetrical lineage
            % reconstruction (rod shaped cells).
            %
            % Args:
            %    this (:obj:`object`):              :class:`Lineage` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                ' Selected symmetrical cell division']);
            tic
            % Initialized data fields
            this.data.initializeLineageDataFields();
            
            % Set cell poles first
            this.data.setCellPoleCoordinates();
            % Run reconstruction
            this.data.setParentSymDivCells(...
                this.configuration.ProjectConfiguration.trackerStartFrame, ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                unique(this.data.SegmentationData.cell_frame), ...
                this.configuration.ProjectConfiguration.segmentationResultDir, ...
                this.configuration.ProjectConfiguration.trackerResultMaskFileArray);

            % Set the track generation and lineage information
            this.setTrackGeneration();
            
            % Get cell cycle phases
            this.cellCyclePhaseTable = this.getCellCyclePhaseTable();
            
            this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                        ' Cell lineage reconstruction done!']);
        end
        
        
        function mammalianLineageReconstruction(this)
            % MAMMALIANLINEAGERECONSTRUCTION Runs the symmetrical lineage
            % reconstruction for mammalian cells with convex shapes.
            %
            % Args:
            %    this (:obj:`object`):              :class:`Lineage` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.utils.printToConsole(['LINEAGE RECONSTRUCTION: ', ...
                ' Selected mammalian cell division']);
            maxIterations = 10;
            
            stable = 0;
            counter = 0;
            while ~stable && counter < maxIterations
            % Initialized data fields
            this.data.initializeLineageDataFields();
            % Run reconstruction
            stable = this.setParentMamDivCells(...
                this.configuration.ProjectConfiguration.trackerStartFrame, ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                unique(this.data.SegmentationData.cell_frame), 0, 0);
            counter = counter + 1;
            end
            
            %% join tracks
            this.data.initializeLineageDataFields();
                        this.setParentMamDivCells(...
                this.configuration.ProjectConfiguration.trackerStartFrame, ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                this.configuration.ProjectConfiguration.trackerStartFrame: ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                1, 0);
            
            this.data.initializeLineageDataFields();
            this.utils.printToConsole('Moving old track indices to track_index_qc')
            this.data.SegmentationData.track_index_qc = ...
                this.data.SegmentationData.track_index;
            this.utils.printToConsole('Relabeling track indices continously')
            this.data.SegmentationData.track_index(:)=0;
            oldOrder = unique(this.data.SegmentationData.track_index_qc,'stable');
            newOrder = [1:length(oldOrder)]';
            for i = 1:length(oldOrder)
            this.data.SegmentationData.track_index(...
                this.data.SegmentationData.track_index_qc == oldOrder(i)) = ...
                newOrder(i);
            end
            
            %run last reconstruction without splitting and joining
            % and print output
            this.setParentMamDivCells(...
                this.configuration.ProjectConfiguration.trackerStartFrame, ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                this.configuration.ProjectConfiguration.trackerStartFrame: ...
                this.configuration.ProjectConfiguration.trackerEndFrame, ...
                0, 1);
            
            % update start and end frame 
            [startFrameArray, endFrameArray] = this.data.getStartEndFrameOfTrack(...
                this.data.SegmentationData.cell_frame, ...
                this.data.SegmentationData.track_index);
            this.data.setFieldArray('track_start_frame', startFrameArray);
            this.data.setFieldArray('track_end_frame', endFrameArray);
            
            % Set the track generation and lineage inforamtion
            this.setTrackGeneration();
            this.evaluateNewMammalianRoots();
            
            % Get cell cycle phases
            this.cellCyclePhaseTable = this.getCellCyclePhaseTable();
            
            % Set finished flag
            this.data.TmpLinAss.reconstructionIsFinished = true;
        end
        
        
        stable = setParentMamDivCells( this, startFrame, endFrame, ...
            imageFrameNumberArray, joinTracks,printOutput)

        splitSurvivingParentTracks(this);
        
        function asymmetricalLineageReconstruction(this)
            % ASYMMETRICALLINEAGERECONSTRUCTION Runs the asymmetrical lineage
            % reconstruction i.e for budding yeast.
            %
            % Args:
            %    this (:obj:`object`):              :class:`Lineage` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
        end
                
        function [foundEdgesFilledMaskedLabeled, segMaskDilatedRaw, ...
                fluoImgDenoised, this, segMaskRaw] = segmentBudNeck(this, frameNumber, ...
                channelIdx, cropArray)
            
            dilationMorphStructElement = strel('square', 8); %strel('disk',3,4);
            %> @todo solve start frame missing at last frame issiue
            %             for frameNumber =this.configuration.ProjectConfiguration. ...
            %                     trackerStartFrame:1%this.configuration.ProjectConfiguration. ...
            %                 trackerEndFrame - 1
            
            %------------------------------------------------------------------
            %> @todo Add loop to loop over fluoChannel / fluoImageIdentifier ->
            %> subset to the ones to be used for LR. Check how to merge results
            %> from different channels! Could be done in parallel i.e
            %> parfor
            %------------------------------------------------------------------
            %                 for iChannel = 1:numel(this.configuration.ProjectConfiguration.fluoLineageFileArray)
            %                     identifier = strsplit(Tracker.TrackerConfiguration.ProjectConfiguration.fluoLineageFileArray{1, iChannel}, '_');
            %                     identifier = identifier{1};
            %                     indexFluoNames = strfind(Tracker.Data.QuantificationData.fluo_name,identifier);
            %                     indexFluoNames = not(cellfun('isempty',indexFluoNames));
            %                     fluoId = unique(Tracker.Data.QuantificationData.fluo_id(indexFluoNames));
            %                     % Do this calculation once and the use lookup to set
            %                     % chanel id
            %                 end
            % Default should be one channel, optional other
            % channels should be selectable. Probably one should be
            % able to set different paraps per channel?
            
            % Load segmentation mask for current frame
            segMaskRaw = this.io.readSegmentationMask(fullfile(...
                this.configuration.ProjectConfiguration.segmentationResultDir,...
                this.configuration.ProjectConfiguration. ...
                trackerResultMaskFileArray{frameNumber}));
            if ~isempty(this.configuration.ProjectConfiguration. ...
                    imageCropCoordinateArray)
                imageCropCoordinateArray = this.configuration.ProjectConfiguration. ...
                    imageCropCoordinateArray;
            else
                imageCropCoordinateArray = cropArray;
                if ~isempty(imageCropCoordinateArray)
                    segMask = TracX.Utils.imcropC(segMaskRaw, imageCropCoordinateArray);
                else
                    segMask = segMaskRaw;
                end
            end
            
            % Dilate segmentation mask, that cells touch each others.
            % Idea is to achive an overlap with bud neck marker segmentation
            segMaskDilated = imdilate(segMask, dilationMorphStructElement);
            segMaskDilatedRaw = zeros(size(segMaskRaw));
            if ~isempty(imageCropCoordinateArray)
                
                xFrom = imageCropCoordinateArray(1);
                xTo = imageCropCoordinateArray(1)+imageCropCoordinateArray(3);
                yFrom = imageCropCoordinateArray(2);
                yTo = imageCropCoordinateArray(2)+imageCropCoordinateArray(4);
                segMaskDilatedRaw(yFrom:yTo, xFrom:xTo) = segMaskDilated;
            else
                segMaskDilatedRaw = segMaskDilated;
            end
            % Detect bud neck pixels
            %             [foundEdgesFilledMaskedLabeled, ~, fluoImgDenoised] = ...
            %                 TracX.lineageReconstruction.detectBudNeckMarkerPixelArray(...
            %                 this.configuration.ProjectConfiguration.imageDir, ...
            %                 this.configuration.ProjectConfiguration.fluoLineageFileArray{frameNumber}, ...
            %                 this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
            %                 segMaskDilated);
            
            % this.configuration.ProjectConfiguration.fluoLineageFileArray{channelIdx,1}{frameNumber}
            [ foundEdgesFilledMaskedLabeled, ~, fluoImgDenoised ] = ...
                this.detectBudNeckMarkerPixelArray(...
                this.configuration.ProjectConfiguration.imageDir, ...
                this.configuration.ProjectConfiguration.fluoLineageFileArray{channelIdx}{frameNumber}, ...
                imageCropCoordinateArray, ...
                segMaskDilated, this.configuration.ParameterConfiguration. ...
                divisionMarkerDenoising, this.configuration. ...
                ParameterConfiguration.divisionMarkerEdgeSensitivityThresh, ...
                this.configuration.ParameterConfiguration. ...
                divisionMarkerConvexAreaLowerThresh, ...
                this.configuration.ParameterConfiguration. ...
                divisionMarkerConvexAreaUpperThresh);
            
            p = regionprops(foundEdgesFilledMaskedLabeled, {'Centroid', ...
                'Area', 'Orientation', 'MajorAxisLength', 'MinorAxisLength', ...
                'PixelIdxList', 'Perimeter'});
            
            if ~isempty(p)
                p([p.Area] == 0) = [];
            end
            pixelIdxList = cell(numel(p), 1);
            for k = 1:numel(p)
                this.lineageData = this.lineageData.setUUID();
                this.lineageData.obj_frame = [this.lineageData.obj_frame; frameNumber];
                this.lineageData.obj_channel_id = [this.lineageData.obj_channel_id; channelIdx];
                this.lineageData.obj_center_x = [this.lineageData.obj_center_x; round(p(k).Centroid(1))];
                this.lineageData.obj_center_y = [this.lineageData.obj_center_y; round(p(k).Centroid(2))];
                this.lineageData.obj_center_z = [this.lineageData.obj_center_z; 0];
                this.lineageData.obj_index = [this.lineageData.obj_index; foundEdgesFilledMaskedLabeled(round(p(k).Centroid(2)), round(p(k).Centroid(1)))];
                this.lineageData.obj_area = [this.lineageData.obj_area; p(k).Area];
                %this.lineageData.obj_eccentricity      = p(k).Eccentricity;
                this.lineageData.obj_orientation = [this.lineageData.obj_orientation; p(k).Orientation];
                %this.lineageData.obj_diameter     = p(k).EquivDiameter;
                this.lineageData.obj_parent_seg_mask_id = [this.lineageData.obj_parent_seg_mask_id; segMaskDilatedRaw(round(p(k).Centroid(2)), round(p(k).Centroid(1)))];
                
                this.lineageData.obj_major_axis = [this.lineageData.obj_major_axis; p(k).MajorAxisLength];
                this.lineageData.obj_minor_axis = [this.lineageData.obj_minor_axis; p(k).MinorAxisLength];
                this.lineageData.obj_perimeter = [this.lineageData.obj_perimeter; p(k).Perimeter];
                pixelIdxList{k, 1} = p(k).PixelIdxList;
                
                % Note we extract signal on denoised image!
                this.lineageData.obj_fluo_total = [this.lineageData.obj_fluo_total; sum(fluoImgDenoised(pixelIdxList{k}))];
                this.lineageData.obj_fluo_mean = [this.lineageData.obj_fluo_mean ; mean(fluoImgDenoised(pixelIdxList{k}))];
                
            end
            %> @todo save correct property!
            this.lineageData.obj_fluoImgDenoised{channelIdx, frameNumber} = foundEdgesFilledMaskedLabeled;
            this.lineageData.obj_pixelIdxList{channelIdx, frameNumber} = pixelIdxList;
            
            %                 % Display status bar
            %                 TracX.Utils.printTimeLeft('Segmenting and quantifying cell division marker', ...
            %                     frameNumber, this.configuration.ProjectConfiguration. ...
            %                     trackerEndFrame - 1)
            
        end
        
        function this = setLineageTable(this)
            % SETLINEAGETABLE Sets the lineage summary table.
            %
            % Args:
            %    this (:obj:`object`):              :class:`Lineage` instance
            %
            % Returns
            % -------
            %    void (:obj:`-`)
            
            this.lineageTable = getLineageSummary(this);
        end
        
        [ foundEdgesFilledMaskedLabeled, foundEdgesFilled, fluoImgDenoised ] = ...
            detectBudNeckMarkerPixelArray( this, imageDir, fluoLineageFileArray, ...
            imageCropCoordinateArray, segMaskDilated, divisionMarkerDenoising, ...
            divisionMarkerEdgeSensitivityThresh, divisionMarkerConvexAreaLowerThresh, ...
            divisionMarkerConvexAreaUpperThresh)
        % DETECTBUDNECKMARKERPIXELARRAY Detect bud neck marker from
        % fluorescence images. A ROF denoising is first applied and then
        % an edge detection to get an array of pixel that each bud covers.
        
        [ validPair, lineProfileResampled, lineProfileRaw ] = ...
            getLineProfileVerification(~, img, x, y, resizeFactor, ...
            divisionMarkerLineProfileLength, divisionMarkerLineProfilePeakLowerBound, ...
            divisionMarkerLineProfilePeakUpperBound  )
        % GETLINEPROFILEVERIFICATION Computes a line profile between
        % two centroids and returns true if a peak in the line profile
        % above a given threshold is found. Additionaly the retrived line
        % profile is returned in its raw and resampled form.
        
        lineageTree = getRootTrackTreeObject(~, trackIndexArray, ...
            trackParentArray, rootTrack, rootTrackRelatedCellArray )
        % GETROOTTRACKTREEOBJECT Constructs a tree lineage tree
        % object for a specified rootTrack (SegmentationData.track_index
        % typically of the first image frame) and all its relatives.
        
        rootTrackRelatedCellArray = getRootTrackRelatedDaughterArray(~, ...
            trackIndexArray, trackParentArray, rootTrack )
        % GETROOTTRACKRELATEDDAUGHTERARRAY Finds all
        % SegmentationData.track_index directly and indirectly related to
        % a certain track_index of interest (e.g. root of a lineage).
        
        [ lineageTreeObjectArray ] = generateLineageTreeObject(this, ...
            varargin )
        % GENERATELINEAGETREEOBJECT Generates a lineage tree
        % object for one or multiple root cells (i.e. cells on the first
        % image frame).
        
        [durationTreeObj, phaseStartIdxNew, phaseEndIdxNew] = ...
            getTreeNodeDurations(this, lineageTree, firstFrame, lastFrame, ...
            imageFreq)
        % GETTREENODEDURATIONS Returns a tree object containing the cell cycle
        % duration until cell division.
        
        [summaryTable] = getLineageSummary(this)
        % GETLINEAGESUMMARY Returns a condenst version of the cell lineage as a
        % table.
        
        setSignalInNucleus(this, channel,  qc, varargin)
        % SETSIGNALINNUCLEUS Quantifies if a signal is in the nucleus when
        % it is larger that the mean + 0.5 * mad.
        
        runMarkerTracking(this, varargin)
        % RUNMARKERTRACKING Runs the tracker algorithm for cell cycle marker
        % channels. To track i.e a bud neck marker.
        
        setTrackGeneration(this)
        % SETTRACKGENERATION Sets the cell generation for each track after lineage
        % reconstruction.
        
        setBirthToDivPhase(this)
        % SETBIRTHTODIVPHASE Sets the phase between birth and cell division, also
        % known as G2 or S/G2/M phase in budding yeast.
        
        setParentAndHasBud(this)
        % SETPARENTANDHASBUD Sets the track parent, the probability of the parent
        % being correct and the parent score as well as during which frames the cell
        % has a bud.
        
        [bkgCorrSig, validSigL, fh] = getBkgCorrSig(this, x, y, track, ...
            minSigLength, qc)
        % GETBKGCURRSIG Returns a background corrected nuclear signal for G1
        % determination.
        
        validateTmpLinAss(this)
        % VALIDATETMPLINASS Validates temporary mother daughter (MD) assignments based on
        % how often an overlapping assignment was found and with which budneck
        % marker track. Assignments where a marker track is used for different MD
        % pairs will be modified by keeping the one pair with the most detections.
        % The others will get the second longest assignment or if there is none
        % these will be deleted.
        
        [ t ] = getCellCyclePhaseTable(this)
        % GETCELLCYCLEPHASETABLE Returns the full cell cycle phase table.
        
        setTrackAge(this)
        % SETTRACKAGE Sets the cell age for each track after lineage
        % reconstruction.
        
        [ tracks2Merge ] = getTracks2Merge(this)
        % GETTRACKS2MERGE Returns a matrix with the mothers and buds to be 
        % merged prior division.
        
        resolveCircAssignment(this)
        % RESOLVECIRCASSIGNMENT Aims to resove circular mother-daugher
        % assignments where each track assignes each other as parent.
        
        colormap = recursiveTrackParentColoring(this, root, color, colormap, ...
            track_index, track_parent)
        % RECURSIVETRACKPARENTCOLORING Returns a colormap for recursive track
        % parent coloring.
                       
        [divisionFrameArray] = getDivisionFrames(this, toRootTrackRelatedCellArray)
        % GETDIVISIONFRAMES Returns an array of the division frames for an array of
        % track indicies such as i.e. lineage root related tracks.
        
        [B, N, varargout] = getNewickFormat(this, varargin)
        % GETNEWICKFORMAT Converts the LineageTreeObject/Summary to a phytree object
        % or to the Newick format.

    end   
end