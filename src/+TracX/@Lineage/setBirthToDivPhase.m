%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function setBirthToDivPhase(this)
% SETBIRTHTODIVPHASE Sets the phase between birth and cell division, also
% known as G2 or S/G2/M phase in budding yeast.
%
% Args:
%    this (:obj:`object`):                       :class:`Lineage` instance   
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Andreas P. Cuny - initial implementation

evaluatedMarkers = nan(size(this.data.TmpLinAss.motherCell, 2));

for m = 1:size(this.data.TmpLinAss.motherCell, 2)
    dSegOcc = this.data.getFieldArrayForTrackIndex(...
        'cell_frame', this.data.TmpLinAss.motherCell(m));
    
    try
        if ~isempty(dSegOcc)
            currentMarker = this.data.TmpLinAss.markerTrack(m);
            difStart = abs(this.data.TmpLinAss.markerTrackStartFrame(m) - dSegOcc);
            closestStart = dSegOcc(difStart == min(difStart));
            closestStart = closestStart(1);
            difEnd = abs(this.data.TmpLinAss.markerTrackEndFrame(m) - dSegOcc);
            closestEnd = dSegOcc(difEnd == min(difEnd));
            closestEnd = closestEnd(1);
            dSegOccDiv = dSegOcc(find(dSegOcc == closestStart, 1, 'first'): ...
                find(dSegOcc == closestEnd, 1, 'first'));
            
            phase = this.data.getFieldArrayForTrackIndex('track_cell_cycle_phase', ...
                this.data.TmpLinAss.motherCell(m));
            idx = ismember(this.data.getFieldArrayForTrackIndex('cell_frame', ...
                this.data.TmpLinAss.motherCell(m)), dSegOccDiv);
            
            if ~ismember(evaluatedMarkers, currentMarker)
                % 0 no assigned phase, 1 phase one, 2 phase two, 3 overlapping phase 1 and 2
                phase(idx) = phase(idx) + 2;
                % Save phase info
                this.data.SegmentationData.track_cell_cycle_phase(...
                    this.data.SegmentationData.track_index == ...
                    this.data.TmpLinAss.motherCell(m)) = phase;
            end
            
            evaluatedMarkers(m) = this.data.TmpLinAss.markerTrack(m);
        end
        
    catch ME
        this.utils.printToConsole((sprintf('ERROR: %s in:', ME.message)))
        for i =1:numel(ME.stack)
            this.utils.printToConsole(sprintf('file: %s', ME.stack(i).file))
            this.utils.printToConsole(sprintf('name: %s', ME.stack(i).name))
            this.utils.printToConsole(sprintf('line: %d', ME.stack(i).line))
        end
        return
    end
    
end
end