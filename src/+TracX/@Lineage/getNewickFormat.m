%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function [B, nwk, phyStruct, figH] = getNewickFormat(this, varargin)
% GETNEWICKFORMAT Converts the LineageTreeObject/Summary to a phytree object 
% or to the Newick format.
%
% Args:
%    this (:obj:`obj`):                        :class:`Lineage` object
%
%    varargin (:obj:`str varchar`):
%
%        * **rootIndicies** (:obj:`int`):      The lineage root; Any 'track_index' number.
%        * **plotToFigure** (:obj:`bool`):     Flag if lineage tree should be plotted to a figure. Defaults to false.
%
% Returns
% -------
% B: (:obj:`array`, nx2)
%                                               Numeric array of size [NUMBRANCHES X 2] in which every row represents a branch of the tree. It contains two pointers to the branch or leaf nodes, which are its children.
%
% nwk: (:obj:`str`):
%                                               NEWICK tree string format.
% phyStruct: (:obj:`stuct`):
%                                               Phytree struct.
% figH: (:obj:`object`):
%                                               Phytree figure handle.
%
% :Authors:
% 	Andreas P. Cuny - initial implementation
% 	Eline Y. Bijman - initial implementation

defaultRootIndices = [];
defaultPlotToFigure = false;
validRootIndices = {'>', 0, '<', max(this.data.getFieldArray('track_index'))};

p = inputParser;
p.addRequired('this', @isobject);
p.addOptional('rootIndicies', defaultRootIndices, @(x)validateattributes(x, ...
    {'double'}, validRootIndices, 'Rankings'));
p.addParameter('plotToFigure', defaultPlotToFigure, @(x)validateattributes(x, ...
    {'logical'}, {'scalar'}))
parse(p, this, varargin{:})

startFrameTrackIndexArray = this.data.getFieldArrayForFrame(...
    'track_index', this.configuration. ...
    ProjectConfiguration.trackerStartFrame);

if ~isempty(p.Results.rootIndicies)
    rootsToPlot = p.Results.rootIndicies;
else
    rootsToPlot = startFrameTrackIndexArray;
end

lineageTreeArray = this.generateLineageTreeObject('rootIndicies', ...
    rootsToPlot);
trackIndexArray = this.data.getFieldArray('track_index');
trackParentArray = this.data.getFieldArray('track_parent');

%% Iterate over tracks
phyStruct = cell(max(rootsToPlot), 1);
B = cell(max(rootsToPlot), 1);
nwk = cell(max(rootsToPlot), 1);
figH = cell(max(rootsToPlot), 1);        
for iRoot = rootsToPlot
    
    % Get all related cells
    toRootTrackRelatedCellArray = this.getRootTrackRelatedDaughterArray(...
        trackIndexArray, trackParentArray, startFrameTrackIndexArray(...
        ismember(startFrameTrackIndexArray, iRoot)));

    trackNodes = cell2mat(lineageTreeArray{iRoot}.Node);
    if numel(trackNodes) == 1
        continue
    else
        
        % Get Labels
        cT = lineageTreeArray{iRoot,1};  
        tOrder = cT.depthfirstiterator;
        parent = cT.Parent;
        node = cT.Node;
        n = sum(tOrder); % Not complete correct calculation..
        labels = cell(sum(parent)-1, 2);
        
        currNode = node;
        currParent = parent;
        
        for k = n:-1:1
            if ~isempty(currNode)
                cNode = currNode(end);
                cParent = currParent(end);
                
                if cParent > 0
                    labels{(n-k)+1, 2} = sprintf('%d.%d', node{cParent}, ...
                        sum(cParent == currParent));
                    labels{(n-k)+1, 1} = num2str(cNode{1});
                end
                
                currNode = currNode(1:end-1);
                currParent = currParent(1:end-1);
            end
        end
        
        labels = labels(~cellfun('isempty',labels(:,1)),:);  % remove empty cells (due to high n)
        
        % Create array/table with the information needed for obtaining newick
        % tree structure
        % Contains: cell number/name, mother cell, generation, (opt: timediv)
        
        trTable = zeros(length(labels(:))+1, 3);
        trTable(:,1) = [iRoot; str2double(labels(:))];
        cellNames = str2double(labels(:));
        
        % Add mother cell
        for nCell=1:numel(cellNames)
            
            if ismember(cellNames(nCell),  toRootTrackRelatedCellArray) 
                didx = find(ismember(str2double(labels), cellNames(nCell))); %double check
                trTable(nCell+1, 2) = str2double(labels(didx, 2)) - 0.1;
            else
                trTable(nCell+1,2) = cellNames(nCell) -0.1;
            end
        end
        
        % Add generation number
        nGen = 1;
        motherCell = iRoot;
        cset = cellNames;
        
        while ~isempty(cset) && nGen < 1000
            currMothers = motherCell;
            motherCell = [];
            for nMother=1:numel(currMothers)
                midx = ismembertol(trTable(:,2), currMothers(nMother), 0.0001);
                daughterCells = trTable(midx, 1);
                if ~isempty(daughterCells)
                    trTable(midx, 3) = nGen;
                    didx = ismembertol(cset, daughterCells, 0.0001);
                    cset(didx) = [];
                    motherCell = [motherCell daughterCells.'];
                end
            end
            nGen = nGen + 1;
        end
        
        if ~isempty(cset)
            this.utils.printToConsole('WARNING: Not all cells could be assigned to a generation number')
        end
        
        % Get newick string
        maxGen = max(trTable(:,3));
        nwkStr = mat2str(iRoot);
        
        for ngen=1:maxGen
            ncellsGen = sum(trTable(:,3) == ngen-1);
            ncellNames = trTable((trTable(:,3) == ngen-1),1); %mother cells
            for ncells=1:ncellsGen %iterate over mother cells
                idxM = ismembertol(trTable(:,2), ncellNames(ncells), 0.000001);
                daughtercells = (trTable(idxM, 1));
                if ~isempty(daughtercells)
                    strdaughter = "(" + daughtercells(1) + ":1," + daughtercells(2) + ":1)" ;
                    if ngen == 1
                        nwkStr = insertBefore(nwkStr,  "" +  ncellNames(ncells)+ "", strdaughter);
                    else
                        a = ['(?<=\D+)', strrep(num2str(ncellNames(ncells)), '.', '\.'), '(?=\D)'];
                        matchstr = regexp(nwkStr,a);
                        nwkStr = insertBefore(nwkStr, matchstr, strdaughter);
                    end
                end
            end
        end
        
        % Get phytree object
        tObj = phytreeread(nwkStr);
        tStruct = get(tObj);
        
        if p.Results.plotToFigure
            h = plot(phytree(tStruct.Pointers, tStruct.Distances, ...
                tStruct.NodeNames)); 
        else
            h = [];
        end
        
        % Output
        phyStruct{iRoot, 1} = tStruct;
        B{iRoot, 1} = tStruct.Pointers;
        nwk{iRoot, 1} = nwkStr;
        figH{iRoot, 1} = h;
    end
end
