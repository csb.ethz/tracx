%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function splitSurvivingParentTracks(this)
% SPLITSURVIVINGPARENTTRACKS Splits surviving parent tracks during
% mammalian lineage reconstruction.
%
% Args:
%    this (:obj:`object`):                        :class:`Tracker` instance   
%
% Returns
% -------
%    void :obj:`-`                                
%
% :Authors:
%    Thomas Kuendig - initial implementation

    uniqueFrames = unique(this.data.getFieldArray('cell_frame'));
    
    for frame = uniqueFrames'
        
        parentsNow = this.data.getFieldArrayForFrame('track_parent',frame);
        tracksFromNowOn = this.data.SegmentationData.track_index(...
            this.data.getFieldArray('cell_frame') >= frame);
        
        overlap = ismember(parentsNow,tracksFromNowOn);
        
        if any(overlap)
            
            parentsToSplit = parentsNow(overlap);
            
            for p = parentsToSplit'
                newTrack = this.data.SegmentationData.track_index == p & ...
                    this.data.getFieldArray('cell_frame') >= frame;
                
                %Create new track after split
                this.data.SegmentationData.track_parent(newTrack) = p;
                this.data.SegmentationData.track_index(newTrack) = ...
                    max( this.data.SegmentationData.track_index)+1;
                
                daughtersIdx = this.data.SegmentationData.track_parent == p &...
                    this.data.getFieldArray('cell_frame') >= frame;
                daughters = unique(this.data.SegmentationData.track_index(daughtersIdx));
                for d = daughters'
                    this.utils.printToConsole(sprintf('%d of %d at %d',d,p,frame));
                    if ~( min(this.data.getFieldArrayForTrackIndex('cell_frame',d))<=frame)
                        this.data.SegmentationData.track_parent(...
                            this.data.getFieldArray('track_index')==d) = p;
                    end
                end
            end
        end
    end
end