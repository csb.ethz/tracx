%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [summaryTable] = getLineageSummary(this)
% GETLINEAGESUMMARY Returns a condensed version of the cell lineage as a
% table. Each row is a division event with the columns mother_start_frame, 
% trackindex_mother, daughter_start_frame, trackindex_daughter,
% division_frame.
%
% Args:
%    this (:obj:`object`):                    :class:`Lineage` instance  
%
% Returns
% -------
%    summaryTable :obj:`table`                                
%                                             Lineage summary as table.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

startFrameTrackIndexArray = this.data.getFieldArrayForFrame(...
    'track_index', this.configuration. ...
    ProjectConfiguration.trackerStartFrame);

trackIndexArray = this.data.getFieldArray('track_index');
trackParentArray = this.data.getFieldArray('track_parent');

nRows = nan(numel(startFrameTrackIndexArray), 1);
for k = 1:numel(startFrameTrackIndexArray)
    nRows(k) = numel(this.getRootTrackRelatedDaughterArray(...
        trackIndexArray, trackParentArray, startFrameTrackIndexArray(k)));
end
nRowsTotal = sum(nRows);

% first loop over all daughters and count how many we have to initialize
% correctly. And then use an index counter to where to insert.
mother_start_frame = nan(nRowsTotal, 1);
track_index_mother = nan(nRowsTotal, 1); 
daughter_start_frame = nan(nRowsTotal, 1);
track_index_daughter = nan(nRowsTotal, 1);
bud_neck_start_frame = nan(nRowsTotal, 1);
division_frame = nan(nRowsTotal, 1);

cnt = 1;
for m = 1:numel(startFrameTrackIndexArray)
    
    % Get all related cells
    toRootTrackRelatedCellArray = this.getRootTrackRelatedDaughterArray(...
        trackIndexArray, trackParentArray, startFrameTrackIndexArray(m) );
    
    if ~isempty(toRootTrackRelatedCellArray)       
        for d = toRootTrackRelatedCellArray
%             mother_start_frame(cnt) = unique(this.data.getFieldArrayForTrackIndex(...
%                 'track_start_frame', startFrameTrackIndexArray(m)));
%             track_index_mother(cnt) = startFrameTrackIndexArray(m);
            mother_start_frame(cnt) = unique(this.data.getFieldArrayForTrackIndex(...
                'track_start_frame', unique(this.data.getFieldArrayForTrackIndex('track_parent', d))));
            track_index_mother(cnt) = unique(this.data.getFieldArrayForTrackIndex('track_parent', d));

            daughter_start_frame(cnt) =  unique(this.data.getFieldArrayForTrackIndex(...
                'track_start_frame', d));
            track_index_daughter(cnt) = d;
            bud_neck_start_frame(cnt) = min(this.data.TmpLinAss.frameOfMDPair{this.data. ...
                TmpLinAss.daughterCell == d});
            division_frame(cnt) = max(this.data.TmpLinAss.frameOfMDPair{this.data. ...
                TmpLinAss.daughterCell == d});
            cnt = cnt + 1;
        end
    end
end

summaryTable = array2table([mother_start_frame, ...
    track_index_mother, daughter_start_frame, track_index_daughter, ...
    bud_neck_start_frame, division_frame], 'VariableNames',{'mother_start_frame', ...
    'trackindex_mother', 'daughter_start_frame', 'trackindex_daughter', ...
    'bud_neck_start_frame', 'division_frame'});

this.lineageTable = summaryTable;
% 
% array2table([fillmissing(mother_start_frame,'previous'), ...
%     fillmissing(track_index_mother,'previous'), ...
%     daughter_start_frame, track_index_daughter, division_frame], ...
%     'VariableNames',{'mother_start_frame', 'trackindex_mother', ...
%     'daughter_start_frame', 'trackindex_daughter', 'division_frame'});

this.io.writeTrackerDataToDisk(summaryTable, this.configuration. ...
    ProjectConfiguration.segmentationResultDir, sprintf(...
    '%s_TrackingResultsLineageSummary', ...
    this.configuration.ProjectConfiguration.projectName), ...
    'fileType', 'txt', 'dataDelimiter', '\t')
end
