%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ validPair, lineProfileResampled, lineProfileRaw ] = ...
    getLineProfileVerification(~, img, x, y, resizeFactor, divisionMarkerLineProfileLength, ...
    divisionMarkerLineProfilePeakLowerBound, divisionMarkerLineProfilePeakUpperBound  )
% GETLINEPROFILEVERIFICATION Computes a line profile between two 
% centroids and returns true if a peak in the line profile above a 
% given threshold is is found. Additionaly the retrived line profile is 
% returned in its raw and resampled form.
%
% Args:
%    ignoredArg (:obj:`object`):               :class:`Lineage` instance.
%    img (:obj:`array`, NxM):                  Image matrix.
%    x (:obj:`array`, kx1):                    Spatial coordinates of start 
%                                              and endpoint for line profile 
%                                              in x.
%    y (:obj:`array`, kx1):                    Spatial coordinates of start 
%                                              and endpoint for line profile 
%                                              in y.
%    resizeFactor (:obj:`int`, 1x1):           Number used to resize the 
%                                              image matrix to increase 
%                                              processing speed. Attention: 
%                                              To high downscaleing can 
%                                              result in false positive line
%                                              profiles since only a few 
%                                              pixel remain.
%    divisionMarkerLineProfileLength (:obj:`int`):          Max length to resample the 
%                                                           profiles to.
%    divisionMarkerLineProfilePeakLowerBound (:obj:`int`):  Lower bound to accept a peak
%                                                           in the Line profile.
%    divisionMarkerLineProfilePeakUpperBound (:obj:`int`):  Upper bound to accept a peak
%                                                           in the Line profile.
%
% Returns
% -------
%    validPair :obj:`bool`
%                                             Binarization of the result. 
%                                             1 if at least one peak was 
%                                             detected. 0 if no peak was 
%                                             detected.
%    lineProfileResampled :obj:`array`        Array of resampled line profile.
%    lineProfileRaw :obj:`array`              Array of raw line profile 
%                                             with applied resizing.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

warning('off', 'signal:findpeaks:largeMinPeakHeight')

% Resize image for better performance
if resizeFactor ~= 1
    img = imresize(img, 1/resizeFactor);
end

intProfile = improfile(img, x/resizeFactor, y/resizeFactor);
if ~isempty(intProfile)
    
    lineProfileRe = resample(intProfile, divisionMarkerLineProfileLength, length(intProfile));
    [peakAmp, peakLoc] = findpeaks(lineProfileRe);
    
    % Calculated the peak amplitude threshold dynamically due to shifts of
    % the cellular background fluorescence over the experiment duration.
    peakAmpThres = mean(peakAmp(peakLoc<divisionMarkerLineProfilePeakLowerBound | ...
        peakLoc> divisionMarkerLineProfilePeakUpperBound));
    
    peakInLineProfile = findpeaks(lineProfileRe(...
        divisionMarkerLineProfilePeakLowerBound:divisionMarkerLineProfilePeakUpperBound), ...
        'MINPEAKHEIGHT', peakAmpThres);
    
    % Binarization of peak detetction such that several pixel need to be
    % above threshold to count as a 'peak signal'
    if ~isempty(peakInLineProfile)
        validPair = 1;
    else
        validPair = 0;
    end
    
    lineProfileResampled = lineProfileRe;
    lineProfileRaw = intProfile;
else
    lineProfileResampled = intProfile;
    lineProfileRaw = intProfile;
    validPair = 0;
end
end