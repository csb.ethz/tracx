%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ foundEdgesFilledMaskedLabeled, foundEdgesFilled, fluoImgDenoised ] = ...
    detectBudNeckMarkerPixelArray( this, imageDir, fluoLineageFileName, ...
    imageCropCoordinateArray, segMaskDilated, divisionMarkerDenoising, ...
    divisionMarkerEdgeSensitivityThresh, divisionMarkerConvexAreaLowerThresh, ...
    divisionMarkerConvexAreaUpperThresh)
% DETECTBUDNECKMARKERPIXELARRAY Detect bud neck marker from
% fluorescence images. A ROF denoising is first applied and then an
% edge detection to get an array of pixel covered by each bud.
%
% Args:
%    this (:obj:`object`):                               :class:`Lineage` instance 
%    imageDir (:obj:`str`):                              Path to image directory        
%    fluoLineageFileName (:obj:`str`):                   Fluorescence image
%                                                        file names for the 
%                                                        given channel and frame.  
%    imageCropCoordinateArray (:obj:`array`, 1x4):       Array with crop coordinates 
%                                                        used during segmentation.  
%                                                        Needed to map segmentation
%                                                        cell cennters to correct 
%                                                        position within the raw
%                                                        images.  
%    segMaskDilated (:obj:`array`, NxM):                 Dilated cell
%                                                        segmentation mask
%                                                        where masks touch each 
%                                                        others.
%    divisionMarkerDenoising (:obj:`float`):             :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerDenoising` 
%    divisionMarkerEdgeSensitivityThresh (:obj:`float`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerEdgeSensitivityThresh` 
%    divisionMarkerConvexAreaLowerThresh (:obj:`float`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerConvexAreaLowerThresh` 
%    divisionMarkerConvexAreaUpperThresh (:obj:`float`): :class:`~+TracX.@ParameterConfiguration.ParameterConfiguration.divisionMarkerConvexAreaUpperThresh`  
%
% Returns
% -------
%    budPixelIndiciesArray :obj:`array`                               
%                                                       Array of all pixels 
%                                                       belonging to a bud.
%    foundEdgesFilled :obj:`array` NxM                              
%                                                       Image after edge detection
%                                                       with filled holes
%    fluoImgDenoised :obj:`array` NxM                              
%                                                       Denoised image
%
% :Authors:
%    Andreas P. Cuny - initial implementation

fluoImg = this.io.readImageToGrayScale(fullfile(imageDir, ...
    fluoLineageFileName), imageCropCoordinateArray); % im2uint8

fluoImgDenoised = TracX.ExternalDependencies.rofdenoise.ROFdenoise(fluoImg, ...
    divisionMarkerDenoising); % denoisingParamteter

% Remove hot pixels and speckle noise
fluoImgDenoised = medfilt2(fluoImgDenoised, [3 3]);

% Compute the difference of gaussians
gFilt1 = fspecial('gaussian',21,1);
gFilt2 = fspecial('gaussian',21,2);
dog = gFilt1 - gFilt2;
fluoImgDenoisedG = conv2(fluoImgDenoised, dog, 'same');

% % Apply a canny edge filter
% foundEdges = edge(fluoImgDenoised, 'Canny', ...
%     divisionMarkerEdgeSensitivityThresh); 
foundEdges = edge(fluoImgDenoisedG, 'Canny', ...
    divisionMarkerEdgeSensitivityThresh);

% Remove border pixel artefacts
margin = 10;
foundEdges(1:margin,:)=0;
foundEdges(end-margin:end,:)=0;
foundEdges(:,1:margin)=0;
foundEdges(:,end-margin:end)=0;

% dilate by 1 pixel
% SE1 = strel('square',1);
% res = imdilate(foundEdges,SE1);
% dilate by 3 pixel
% res = imerode(foundEdges, ones(5,5));

% Close open circles
foundEdges = imdilate(foundEdges, strel('disk', 1));
% Fill holes inside found edge contours
foundEdgesFilled = imfill( foundEdges, 'holes' );
% Remove dilation step from before
foundEdgesFilled = imerode(foundEdgesFilled, strel('disk', 1));
% foundEdgesFilled = imdilate(foundEdgesFilled, ones(2,2));

% % Compute eucledian distances
% euclidDist = bwdist(~foundEdgesFilled);
% euclidDist = -euclidDist;
% euclidDist(~foundEdgesFilled) = -Inf;
% 
% % Compute simple watershed
% waterShedRegions = watershed(euclidDist);
% 
% % Compute necessary regionprops
% budPixelIndiciesArray = regionprops(waterShedRegions, 'PixelIdxList');

foundEdgesFilledMasked = foundEdgesFilled;
% foundEdgesFilledMasked(segMaskDilated < 1) = 0; % Does not segment
% budnecks at colony borders
% Get perimenter of colony and cell segmentations to define the region of
% interest
segMaskDilatedPerim = bwperim(segMaskDilated);
segMaskDilatedPerimFilledD = imdilate(segMaskDilatedPerim, strel('disk', 4));
foundEdgesFilledMasked(and(segMaskDilatedPerimFilledD < 1, segMaskDilated < 1)) = 0;
%figure; imshow(foundEdgesFilledMasked)
% Get rid of small snake like structures
foundEdgesFilledMasked = imdilate(imerode(foundEdgesFilledMasked, ...
    strel('disk', 1)), strel('disk', 1));
%figure; imshow(foundEdgesFilledMasked)
foundEdgesFilledMaskedLabeled = bwlabel(foundEdgesFilledMasked);
% Remove to large objects from mask (i.e. autofluorescent dead, cells)
p = regionprops(foundEdgesFilledMasked, 'ConvexArea');
labelsToRemove = find([p.ConvexArea] > divisionMarkerConvexAreaUpperThresh | ...
    [p.ConvexArea] <= divisionMarkerConvexAreaLowerThresh);

% Final bud mask; can be used to erode and dilate on a per bud basis
foundEdgesFilledMaskedLabeled(ismember(foundEdgesFilledMaskedLabeled, ...
    labelsToRemove)) = 0;
foundEdgesFilledMaskedLabeled = imfill( foundEdgesFilledMaskedLabeled, ...
    'holes' );

if ~isempty(imageCropCoordinateArray)
    xFrom = imageCropCoordinateArray(1);
    xTo = imageCropCoordinateArray(1)+imageCropCoordinateArray(3);
    yFrom = imageCropCoordinateArray(2);
    yTo = imageCropCoordinateArray(2)+imageCropCoordinateArray(4);
    imOut1 = zeros(this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth);
    imOut2 = imOut1;
    imOut3 = imOut1;
    imOut1(yFrom:yTo, xFrom:xTo) = foundEdgesFilledMaskedLabeled;
    imOut2(yFrom:yTo, xFrom:xTo) = foundEdgesFilled;
    imOut3(yFrom:yTo, xFrom:xTo) = fluoImgDenoised;
    foundEdgesFilledMaskedLabeled = imOut1;
    foundEdgesFilled = imOut2;
    fluoImgDenoised = imOut3;
end

end
