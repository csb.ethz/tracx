%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [tracks2Merge] = getTracks2Merge(this)
% GETTRACKS2MERGE Returns a matrix with the mothers and buds to be merged
% prior division.
%
% Args:
%    this (:obj:`object`):                        :class:`Lineage` instance. 
%
% Returns
% -------
%    tracks2Merge :obj:`array` 
%                                                 Array with the tracks to
%                                                 merge. Colums are
%                                                 frame_number, mother track_index
%                                                 daughter track_index, mother uuid,
%                                                 daughter uuid
%
% :Authors:
%    Andreas P. Cuny - initial implementation


allFrames = this.configuration.ProjectConfiguration.trackerStartFrame: ...
    this.configuration.ProjectConfiguration.trackerEndFrame;
% 
% frames2Process = allFrames(sum(frameMatrix, 1) > 0);

frames = this.data.getFieldArray('cell_frame');
isBud = this.data.getFieldArray('track_has_bud');
framesWithBud = unique(frames(isBud == 1))';
frames2Process = allFrames(ismember(allFrames, framesWithBud));

tracks2Merge = zeros(1,5);
for f = 1:numel(frames2Process)
    
    fn = frames2Process(f);
    
    tracks = this.data.getFieldArrayForFrame('track_index', fn);
    parent = this.data.getFieldArrayForFrame('track_parent', fn);
    isBudFrame = this.data.getFieldArrayForFrame('track_has_bud', fn);
    uuid = this.data.getFieldArrayForFrame('uuid', fn);
    
    daughterArray = tracks(parent > 0)';
    for d = daughterArray
        startFrame = this.data.TmpLinAss.daughterCellStartFrame(this.data. ...
            TmpLinAss.daughterCell == d);
        endFrame = this.data.TmpLinAss.markerTrackEndFrame(this.data. ...
            TmpLinAss.daughterCell == d);
        motherTrack = parent(tracks == d);
        if isBudFrame(tracks == motherTrack) == 1
            if fn >= startFrame && fn <= endFrame
                daughterTrack = d;
                tracks2Merge = [[tracks2Merge(:, 1); fn], ...
                    [tracks2Merge(:, 2); motherTrack], ...
                    [tracks2Merge(:, 3); daughterTrack], ...
                    [tracks2Merge(:, 4); uuid(tracks == motherTrack)], ...
                    [tracks2Merge(:, 5); uuid(tracks == daughterTrack)]];
            end
        end
    end
end
tracks2Merge(1, :) = [];

end