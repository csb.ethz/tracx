%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function colormap = recursiveTrackParentColoring(this, root, color, ...
    colormap, trackIndexArray, trackParentArray)
% RECURSIVETRACKPARENTCOLORING Returns a colormap for recursive track
% parent coloring.
%
% Args:
%    this (:obj:`object`):                        :class:`Lineage` instance. 
%    root (:obj:`int`):                           Root track index.
%    color (:obj:`array`):                        Color.
%    colormap (:obj:`array`):                     Colormap 
%    trackIndexArray (:obj:`array`):              Track index array. 
%    trackParentArray (:obj:`array`):             Track parent array. 
%
% Returns
% -------
%    colormap :obj:`array` kx3
%                                                      Colormap
%
% :Authors:
%    Thomas Kuendig - initial implementation

% % finalmap = recursiveTrackPatentColoring(0, [0.5 0.5 0.5],...
% %     zeros(length(Tracker.data.SegmentationData.track_index),3), ...
% %     Tracker.data.SegmentationData.track_index, ...
% %     Tracker.data.SegmentationData.track_parent )

if root == 0
    %for the select the root origin 0 and start them with random colors
    children = trackParentArray==root;
    children_tracks = unique(trackIndexArray(children));
        for t = children_tracks'
        colormap = this.recursiveTrackParentColoring(t, rand(1,3), ...
            colormap, trackIndexArray, trackParentArray);
        end
else
    %for a root track: set a goal color at the right distance
    currenttrack = trackIndexArray==root;
    children = trackParentArray==root;
    nextcolor = randomizer(color, sum(currenttrack));
    
    %interpolate the steps between the start color and the goal color
    inbetween = zeros(sum(currenttrack),3);
    for i = 1:3
        inbetween(:,i) = linspace(color(i),nextcolor(i),sum(currenttrack))';
    end
    
    % assign the color gradient to the object that belong to the track
    colormap(currenttrack,:) = inbetween;
    children_tracks = unique(trackIndexArray(children));
    
    for t = children_tracks'
        
        % for each child find the last cell belonging to the parent before
        % the beginning of the child
        k = find(trackIndexArray==t,1);
        while ~currenttrack(k)
            k = k-1;
        end
        
        % start with child as root and the color of the last parent cell
        % before the division
        colormap = this.recursiveTrackParentColoring(t, colormap(k,:), ...
            colormap, trackIndexArray, trackParentArray);
    end
end
end

function color = randomizer(color, number)

    % generate a random color at a distance to the input color  
    % proportional to the length of the track
    rand = randn(1,3);
    rand = rand./norm(rand)/30*number;
    color = color + rand;
    
    % floor and ceil impossible colors
    color(color > 1) = 1;
    color(color < 0) = 0;
end