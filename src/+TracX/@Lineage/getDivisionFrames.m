%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [divisionFrameArray] = getDivisionFrames(this, toRootTrackRelatedCellArray)
% GETDIVISIONFRAMES Returns an array of the division frames for an array 
% of track indicies such as i.e. lineage root related tracks. It is
% assumed that the first element is allways the rote of the lineage tree.
%
% Args:
%    this (:obj:`object`):                     :class:`Lineage` instance  
%    toRootTrackRelatedCellArray (:obj:`int`): Array with to root related
%                                              track indices.
%
% Returns
% -------
%    divisionFrameArray :obj:`array`                                
%                                             Indices of joined non empty 
%                                             splitted track indices and
%                                             joined track indices.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

expEndTime = this.configuration.ProjectConfiguration.trackerEndFrame;

divisionFrameArray = nan(size(toRootTrackRelatedCellArray));
for k = 1:numel(toRootTrackRelatedCellArray)
    
    tr = toRootTrackRelatedCellArray(k);
    
    if this.configuration.ProjectConfiguration.cellDivisionType == 1 % asymmetrical case
        
        parent = unique(this.data.getFieldArrayForTrackIndex(...
            'track_parent', tr));
        
        div = unique(this.cellCyclePhaseTable.div(...
            and(this.cellCyclePhaseTable.daughter == ...
            tr, this.cellCyclePhaseTable.track == parent)));
        
    else % symmetrical case
        
        if k == 1 % We assume that the first element is allways the root 
            % of the tree.
            div = unique(this.cellCyclePhaseTable.div(...
                this.cellCyclePhaseTable.track == ...
                tr));
        else
            
            if all(isnan(this.cellCyclePhaseTable.div(...
                    this.cellCyclePhaseTable.track == tr)))
                
                div = expEndTime - ((unique(this.cellCyclePhaseTable. ...
                    seg_st_track(this.cellCyclePhaseTable.track == tr)) -1));
            else
                div = unique(((this.cellCyclePhaseTable.div(...
                    this.cellCyclePhaseTable.track == tr) - ...
                    this.cellCyclePhaseTable.seg_st_track(...
                    this.cellCyclePhaseTable.track == tr)) + 1));
                
            end
        end
    end
    
    if all(isnan(div))
        % That would indicate an erronous lineage tree. Error in generating
        % the cellCyclePhaseTable.
        if unique(this.data.getFieldArrayForTrackIndex('track_start_frame', ...
                tr)) > this.configuration.ProjectConfiguration.trackerStartFrame
            divisionFrameArray(k) = inf; % Cell present but not yet divided.
        else
            divisionFrameArray(k) = 0; 
        end
    else
        divisionFrameArray(k) = div;
    end
    
end
end
