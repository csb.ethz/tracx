%> @brief PRINTTIMELEFT printTimeLeft(taskName, actStep, totStep) prints the
%> remaining time in a computation when actStep steps out of totSteps have
%> been made. taskName is a String describing what will be computed
%> A "tic" must have been called at the beginnig of the computation. This
%> code must be called at the end of the step act_step (and not at the
%> beginning).
%> To reduce the computaton overhead, the code will only be active if
%> floor(percentage) has changed in the last step (this can easy be
%> removed by deleting the first 'if' condition).
%> Inital implementation taken from Nicolas Le Roux.
%> https://ch.mathworks.com/matlabcentral/fileexchange/8076-ascii-progress-bar?s_tid=prof_contriblnk
% /*************************************************************
%>  @copyright Copyright (c) 2016-2018 Andreas P. Cuny, andreas.cuny@bsse.ethz.ch
%>  Computational Systems Biology group, ETH Zurich
%>  All rights reserved. MIT
%>
% Contributors:
%>  @contributors
%>  @contributor{Nicolas Le Roux [lerouxni@iro.umontreal.ca] - initial implementation July, 20th, 2005}
%>  @contributor{Andreas P. Cuny - Restructured code and modified design of waitbar}
% *************************************************************/
function printTimeLeft(taskName, actStep, totStep)

MAX_TASK_NAME_LENGTH = 30;
SECONDS_TO_DAY_CONVERSION = 60 * 60 * 24;
% Percentage completed
oldPercComplete = floor(100*(actStep-1)/totStep);
percComplete = floor(100*actStep/totStep);

if oldPercComplete ~= percComplete
    % Fix display width for printing text to MAX_TASK_NAME_LENGTH. If longer
    % just print char from 1:MAX_TASK_NAME_LENGTH
    if length(taskName) > MAX_TASK_NAME_LENGTH
        taskName = taskName(1:MAX_TASK_NAME_LENGTH);
    else
        tmpTaskName = repmat(' ', 1, MAX_TASK_NAME_LENGTH+1);
        tmpTaskName(MAX_TASK_NAME_LENGTH+1) = '|';
        tmpTaskName(1:length(taskName)) = taskName;
        taskName = tmpTaskName;
    end
    
    % Time spent so far
    timeSpent = toc;
    
    % Estimated time per step
    estTimePerStep = timeSpent/actStep;
    
    % Estimated remaining time. tot_step - act_step steps are still to make
    estRemTime = (totStep - actStep)*estTimePerStep;
    strSteps = [' ' num2str(floor(actStep/totStep*100),'%03d') '%% | ' ];
    
    % Correctly print the remaining time
    if (floor(estRemTime/60) >= 1)
        strTime = ...
            [' : ' num2str(floor(estRemTime/60)) 'm' ...
            num2str(floor(rem(estRemTime,60))) 's'];
    else
        strTime = ...
            [' | ' num2str(datestr(floor(rem(estRemTime,60))/...
            SECONDS_TO_DAY_CONVERSION, 'HH:MM:SS')) '  '];
    end
    
    % Create the final string strFinal in the form:
    % TaskName | act_step/tot_step % | [###.....] | (01:10:36)
    percentage = floor(percComplete);
    strO = repmat('#',1,percentage);
    strDotsBeg = repmat('.',1,100 - percentage);
    strDotsEnd = repmat('.',1,1-percentage);
    strPb = strcat(' [', strO, strDotsBeg, strDotsEnd, ']');
    strFinal = strcat(taskName, strSteps, strPb, strTime);
    
    % Print it
    if oldPercComplete == 0
        clearEOL = 1;
    else
        clearEOL = repmat('\b',1,length(strFinal));
    end
    fprintf(1, ['\n' clearEOL strFinal]);
end

if actStep == totStep
    wipeLine = repmat(' ',1,length(strFinal)-2);
    fprintf(1, ['\n' wipeLine '\n'] );
end


end