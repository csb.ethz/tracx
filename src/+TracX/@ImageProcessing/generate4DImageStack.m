%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ imgStack ] = generate4DImageStack(this, imageFilePath, ...
    segmentationResultDir, imageFileNameArray, trackerResultMaskArray, ...
    coiFrameArray, cellCenterXArray, cellCenterYArray, halfWindowLength, ...
    imageFrameArray, trackIndexArray, trackOI, trackContainedInTrack )
% GENERATE4DIMAGESTACK Generates 2D images with single precision in
% either greyscale or RGB into 4D stack 4th dimension here is time. To
% save memory, images are converted with im2uint8 from >8bit to 8bit
% images.
%   
% Args:
%    this (:obj:`object`):                      :class:`ImageProcessing` instance
%    imageFilePath (:obj:`str`):                Path to image location 
%    segmentationResultDir (:obj:`str`):        Path to segmentation result
%                                               files.
%    imageFileNameArray (:obj:`str` 1xN):       Cell array with image file names
%    trackerResultMaskArray (:obj:`str` 1xN):   Cell array with tracker mask
%                                               file names.
%    coiFrameArray (:obj:`int` 1xN):            Array with frame number
%                                               for cell of interest.
%    cellCenterXArray (:obj:`int` 1xN):         Array with x coordinates 
%                                               for the cell of interest.
%    cellCenterYArray (:obj:`int` 1xN):         Array with y coordinates 
%                                               for the cell of interest.
%    halfWindowLength (:obj:`int`):             Half window length of a 
%                                               square around cell center 
%                                               to cut out from the total 
%                                               image matrix.
%    imageFrameArray (:obj:`int` 1xN):          Array of image frames
%                                               where a tracked cell is 
%                                               present.
%    trackIndexArray (:obj:`int` 1xN):          Array with track indices
%    trackOI:                                   selected Track (current 
%                                               track of interest)
%    trackContainedInTrack (:obj:`int` 1xN):    Array with track indices 
%                                               contained in other tracks
%                                               (due to  discarded miss 
%                                               segmented objects)
%
% Returns
% -------
%    ImgStack: :obj:`array` WxHx3xN
%                                                4D Stack containing the 
%                                                image of cell of interest
%                                                windowed around its cell 
%                                                center.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Preallocated 4D matrix with the size of the images and number of images
% (row x col x chl x #frames)
imgStack = zeros(2 * halfWindowLength, 2 * halfWindowLength, 3,...
    numel(coiFrameArray));

cropCoordinateArray = [cellCenterXArray - halfWindowLength, cellCenterYArray ...
    - halfWindowLength, repmat(2 * halfWindowLength - 1,...
    numel(cellCenterXArray), 1), repmat(2 * halfWindowLength - 1, ...
    numel(cellCenterXArray), 1)];
% cropCoordinateArrayCopy = cropCoordinateArray;

for k = 1:numel(coiFrameArray)
    trackIndexArrayOnCurrentFrame = trackIndexArray(imageFrameArray == coiFrameArray(k));
    trackContainedInTrackSel = trackContainedInTrack(imageFrameArray == coiFrameArray(k));
    r_s = 1;
    r_e = halfWindowLength*2;
    c_s = 1;
    c_e = halfWindowLength*2;
    % Read image
    img = this.io.readImageToGrayScale(fullfile(imageFilePath,...
        imageFileNameArray{coiFrameArray(k)}));
    img = this.normalizeImage(img);
    mask = this.io.readSegmentationMask(fullfile(...
        segmentationResultDir, ...
        trackerResultMaskArray{coiFrameArray(k)}));
    
    % Solve cases for cells at the image border that they still get cut out
    % with the same window size and stay centered
%     while any(cropCoordinateArrayCopy(k, 1:2) <= 0) || ...
%             cropCoordinateArrayCopy(k, 1) + 2 * halfWindowLength > ...
%             size(img,2) || cropCoordinateArrayCopy(k, 2) + 2 * ...
%             halfWindowLength > size(img,1)
%         if cropCoordinateArrayCopy(k, 1) < 0
%             r_s = abs(cropCoordinateArrayCopy(k, 1)) + 2;
%             cropCoordinateArrayCopy(k, 3) = cropCoordinateArrayCopy(k, 3) - ...
%                 (abs(cropCoordinateArrayCopy(k,1)) + 1);
%             cropCoordinateArrayCopy(k, 1) = cropCoordinateArrayCopy(k,1) + ...
%                 abs(cropCoordinateArrayCopy(k, 1)) + 1;
%         elseif cropCoordinateArrayCopy(k, 2) < 0
%             c_s = abs(cropCoordinateArrayCopy(k,2)) + 2;
%             cropCoordinateArrayCopy(k,4) = cropCoordinateArrayCopy(k,4) - ...
%                 (abs(cropCoordinateArrayCopy(k, 2)) + 1);
%             cropCoordinateArrayCopy(k, 2) = cropCoordinateArrayCopy(k,2) + ...
%                 abs(cropCoordinateArrayCopy(k, 2)) + 1;
%         elseif cropCoordinateArrayCopy(k, 1) +2*halfWindowLength > size(img,2)
%             r_e = (2*halfWindowLength - abs(cropCoordinateArrayCopy(k, 1) + ...
%                 2 * halfWindowLength - size(img, 2) -1));
%             cropCoordinateArrayCopy(k, 3) = cropCoordinateArrayCopy(k, 3) -  ...
%                 abs(cropCoordinateArrayCopy(k, 1)+ 2 * halfWindowLength - size(img,2) -1);
%             cropCoordinateArrayCopy(k, 1) = cropCoordinateArrayCopy(k, 3) -  ...
%                 abs(cropCoordinateArrayCopy(k, 1)+2 * halfWindowLength - size(img,2) -1);
%         elseif cropCoordinateArrayCopy(k, 2) +2 * halfWindowLength > size(img,1)
%             c_e = (2 * halfWindowLength - abs(cropCoordinateArrayCopy(k, 2) ...
%                 + 2 * halfWindowLength - size(img,1) -1));
%             cropCoordinateArrayCopy(k, 4) = cropCoordinateArrayCopy(k, 4) -  ...
%                 abs(cropCoordinateArrayCopy(k, 2) + 2 * ...
%                 halfWindowLength - size(img, 1) -1);
%             cropCoordinateArrayCopy(k,2) = cropCoordinateArrayCopy(k,4) -  ...
%                 abs(cropCoordinateArrayCopy(k,2) + 2 * halfWindowLength - ...
%                 size(img, 1) -1);
%         elseif cropCoordinateArrayCopy(k, 2) == 0
%             c_e = 2*halfWindowLength - 1;
%             cropCoordinateArrayCopy(k, 2) = 1;
%         elseif cropCoordinateArrayCopy(k, 1) == 0
%             r_e = 2*halfWindowLength - 1;
%             cropCoordinateArrayCopy(k, 1) = 1;
%         else
%             % No interaction use coorinates as is
%         end
%     end
    
    % Crop mask and image to final size
    mask =  TracX.Utils.padImageCrop(mask, cropCoordinateArray(k,:)); 
    img =  TracX.Utils.padImageCrop(img, cropCoordinateArray(k,:)); 
    
    % trackOI_Idx = ismember(tracksOnCurrentFrame, trackOI);
    containedTrackIdx = ismember(trackContainedInTrackSel, trackOI);
    containedTrack = trackIndexArrayOnCurrentFrame(containedTrackIdx);
    maskIdx = unique(mask);
    maskIdx(maskIdx == 0) = [];
    if isempty(containedTrack)
        if ~isempty(maskIdx(ismember(maskIdx, trackOI)))
			newLabelArray = zeros(numel(maskIdx(~ismember(maskIdx, ...
                trackOI))),1);
			oldLabelArray = maskIdx(~ismember(maskIdx, trackOI));
			if exist('changem', 'file') == 2				
				mask_1 = changem(mask, newLabelArray, oldLabelArray);
			else
			   [lia, locb] = ismember(mask, oldLabelArray);
			   mask(lia) = newLabelArray(locb(lia));
			   mask_1 = mask;
			end
            imgOut = this.overlayMaskOnImage( img, mask_1, 'maskOverlayAlpha',...
                0.5, 'customColorMap', [0,1,0]);
        else
            mask_1 = zeros(size(mask, 1),size(mask, 2));
            mask_1(round(size(mask_1, 1) / 2), round(size(mask_1, 2) / 2)) = 1;
            se = strel('disk',4);
            mask_1 = imdilate(mask_1,se);
            imgOut = this.overlayMaskOnImage( img, mask_1, 'maskOverlayAlpha',...
                0.5, 'customColorMap', [0,1,0]);
        end
    else % cell index does not exist. place dot in image center
        % in the green channel and if we have a track contained in it
        
        % For virtual, non segmentent cells mark propagated cell
        % center with a disk
        mask_1 = zeros(size(mask, 1),size(mask, 2));
        mask_1(round(size(mask_1, 1) / 2), round(size(mask_1, 2) / 2)) = 1;
        se = strel('disk',4);
        mask_1 = imdilate(mask_1,se);
        imgOut = this.overlayMaskOnImage( img, mask_1, 'maskOverlayAlpha', ...
            0.5, 'customColorMap', [0,1,0]);
        
        if ~isempty(containedTrack)
            % Add miss segmented cell mask potentially belonging to the
            % current track of interest
			newLabelArray = zeros(numel(trackIndexArrayOnCurrentFrame(...
                ~ismember(trackIndexArrayOnCurrentFrame, containedTrack))),1);
			oldLabelArray = trackIndexArrayOnCurrentFrame(~ismember(trackIndexArrayOnCurrentFrame, ...
                containedTrack));
			if exist('changem', 'file') == 2				
				mask_2 = changem(mask, newLabelArray, oldLabelArray);
			else
			   [lia, locb] = ismember(mask, oldLabelArray);
			   mask(lia) = newLabelArray(locb(lia));
			   mask_2 = mask;
			end			
				
            mask_2 = bwperim(mask_2);
            imgOut = this.overlayMaskOnImage( imgOut, mask_2, ...
                'maskOverlayAlpha', 1, 'customColorMap', [1,0,0]);
        end
        
    end
    
    imgStack(c_s:c_e, r_s:r_e, :, k) = imgOut;
    
    % Add frame number label to image
    imgStack(:,:,:,k) = insertText(imgStack(:, :, :, k),[2 * halfWindowLength, ...
        2 * halfWindowLength], ...
        coiFrameArray(k),'AnchorPoint', 'RightBottom', 'FontSize', 12);
    
    %> @todo: insert top left track index and top right track
    %> contained in if there is one.
end
end
