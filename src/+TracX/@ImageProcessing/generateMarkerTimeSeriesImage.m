%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  generateMarkerTimeSeriesImage(this, varargin)
% GENERATEMARKERTIMESERIESIMAGE Generates and saves an image  
% series for each track and image frame of the experimet. Each track is 
% labeled with its segmentation mask and centered.
%
% Args:
%    this (:obj:`object`):                             :class:`ImageProcessing` instance   
%    img (:obj:`uint8` NxNx1 uint8, 16, 32, 64):       Image matrix         
%    imageFrequency (:obj:`float`):                    Image frequency in hours
%    frame(:obj:`int`):                                Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **fontSize** (:obj:`float`):          Fontsize of labeles. Defaults to 12.
%        * **halfWindowLength** (:obj:`int`):    Half window side length around cell center to be used for each image frame. Defaults to 50.
%        * **cycleMarker** (:obj:`array`):       Cell array with flourescence channel identifiers depicting cell cycle markers i.e  {'mKO', 'mKate'}.
%        * **isPlotMarkerSignal** (:obj:`bool`): Flag to plot cell cycle marker 'raw' signal as additional overlay; Defaults to false.
%
% Returns
% -------
%    void (:obj:`-`)                               
%                                                 Resulting images are written
%                                                 to disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% warning('off', 'images:initSize:adjustingMag')
% warning('off', 'MATLAB:images:montage:displayRangeForRGB')
% warning('off', 'images:label2rgb:zerocolorSameAsRegionColor')

% Parsing the inputs
defaultFontSize = 12;
defaultHalfWindowLength = 50;
defaultCycleMarker = {'mKO', 'mKate'};
defaultIsPlotMarkerSignal = false;
p = inputParser;
p.addRequired('this', @isobject);
p.addParameter('fontSize', defaultFontSize, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}));
p.addParameter('halfWindowLength', defaultHalfWindowLength, @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}));
p.addParameter('cycleMarker', defaultCycleMarker, @(x)validateattributes(x, ...
    {'cell'}, {'row'}));
p.addParameter('isPlotMarkerSignal', defaultIsPlotMarkerSignal, @islogical);
parse(p, this, varargin{:})
fontSize = p.Results.fontSize;
halfWindowLength = p.Results.halfWindowLength;

imageFilePath = this.configuration.ProjectConfiguration.imageDir;
segmentationResultDir = this.configuration.ProjectConfiguration.segmentationResultDir;
imageFileNameArray = this.configuration.ProjectConfiguration.imageFingerprintFileArray;
trackerResultMaskArray = this.configuration.ProjectConfiguration.trackerResultMaskFileArray;

trackArray = unique(unique(this.data.TmpLinAss.motherCell));
NTrack = numel(trackArray);
pBar = this.utils.progressBar(numel(trackArray), ...
    'Title', 'IMAGE GENERATION: Track time series control' ...
    );
% pBar = this.utils.progressBar(NTrack, ...
%     'IsParallel', true, ...
%     'WorkerDirectory', pwd, ...
%     'Title', 'IMAGEPROCESSING: Generating marker track time series control' ...
%     );

tic
%pBar.setup([], [], []);
for i = 1:NTrack

    iTrack = trackArray(i);
    if ~exist(sprintf('%s/marker_timeseries_track_%d.png', ...
            segmentationResultDir , iTrack), 'file')
                
        % Load all image frames in reduced bit depth into a stack
        imgStackC = this.generate4DImageStackMarker(...
            imageFilePath, segmentationResultDir, imageFileNameArray, ...
            trackerResultMaskArray, p.Results.cycleMarker, halfWindowLength, ...
            iTrack, fontSize, p.Results.isPlotMarkerSignal);
        
        
        imgStackCfin = uint8(zeros(size(imgStackC,1), size(imgStackC,2), ...
            size(imgStackC,3), size(imgStackC, 4)));
        imgStackCfin(:, :, :, 1: size(imgStackC, 4)) = imgStackC;
        
        out = montage(imgStackCfin, 'Size', [ceil(size(imgStackCfin,4)/12) ,...
            12], 'ThumbnailSize', [200, 200]);
        
        this.io.writeToPNG(out.CData, sprintf('%s/marker_timeseries_track_%d.png', ...
            segmentationResultDir , iTrack))
        
        %this.utils.printTimeLeft('Track timeseris generation', i, NTrack)
        pBar.step([], [], []);
        %TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
    else
        %this.utils.printTimeLeft('Track timeseris generation', i, NTrack)
        pBar.step([], [], []);
        %TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
    end
    
end
pBar.release();
%this.utils.printToConsole(sprintf('Track timeseries image generation took %0.5g seconds.', toc))

end