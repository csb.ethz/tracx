%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledImg ] = generateFingerprintControlImageFrame( this, fontSize, ...
    rectHalfSideLength, frameNumber)
% GENERATEFINGERPRINTCONTROLIMAGEFRAME Generates a control
% image where all cell tracks found on an image frame are labeled
% with their track indicies overlayed onto a (Brightfield) image.
% Additionally the fingerprint windows are displayed to estimate the size
% of the cell neighbourhood.
%
% Args:
%    this (:obj:`obj`):                        :class:`ImageProcessing` object
%    fontSize (:obj:`int`):                    Font size of track indicies labels.
%    rectHalfSideLength (:obj:`int`):          :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.fingerprintWindowHalfSideLength`
%    frameNumber (:obj:`int`):                 Image frame number.
% 
% Returns
% -------
%  labeledImg: (:obj:`array`, NxM)          
%                                            Image with the fingerprint window for all tracks present labeled.
% :Authors:
% 	Andreas P. Cuny - initial implementation

fontName = 'Arial';
fontEmphasis = 0;

x = this.data.getFieldArrayForFrame('cell_center_x', frameNumber);
y = this.data.getFieldArrayForFrame('cell_center_y', frameNumber);
indexLabel = this.data.getFieldArrayForFrame('track_index', frameNumber);

baseImg = this.io.readImageToGrayScale(fullfile(...
    this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.imageFingerprintFileArray{frameNumber}), ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray);


% Normalize
baseImg = uint8(255 * this.normalizeImage(baseImg));
% maxVal = this.configuration.ProjectConfiguration.imageMaxSampleValue;
% baseImg = baseImg(:,:) * maxVal;

if size(baseImg, 3) ~= 3
    r = baseImg;
    g = baseImg;
    b = baseImg;
else
    r = baseImg(:, :, 1);
    g = baseImg(:, :, 2);
    b = baseImg(:, :, 3);
end

rgbImg = cat(3, r, g, b);

if ~iscell(indexLabel)
    indexLabelArray = cell(numel(indexLabel),1);
    for iElement = 1:numel(indexLabel)
        iLabel = indexLabel(iElement);
        indexLabelArray{iElement} = num2str(iLabel);
    end
else
    if ~ischar(indexLabel{1})
        indexLabelArray = cellfun(@(x) num2str(x), indexLabel, ...
            'UniformOutput', false);
    else
        indexLabelArray = indexLabel;
    end
end

% Convert Matlab array to java BufferedImage for speed reasons
font = java.awt.Font(fontName, fontEmphasis, fontSize);
image = this.utils.rgb2java(rgbImg);

for iElement = 1:numel(indexLabelArray)
    graphics = image.getGraphics();
    graphics.setFont(font);
    graphics.setColor(java.awt.Color.green)
    graphics.drawString(indexLabelArray{iElement}, round(x(iElement) - ...
        fontSize/3), round(y(iElement)+fontSize/3));
    graphics.dispose();
end

% Convert Java BufferedImage back to Matlab array
nrows = image.getHeight;
ncols = image.getWidth;
channel = image.getRaster.getNumBands;

pixelsData = reshape(typecast(image.getData.getDataStorage, 'uint8'), ...
    channel, ncols, nrows);

rgb = cat(channel, ...
    transpose(reshape(pixelsData(1, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(2, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(3, :, :), ncols, nrows)));

rectPos = [x - repmat(rectHalfSideLength, numel(x), 1), ...
            y - repmat(rectHalfSideLength, numel(x), 1), 2 .* ...
            repmat(rectHalfSideLength, numel(x), 1), 2 .* ...
            repmat(rectHalfSideLength, numel(x), 1)];
        
labeledImg = insertShape(rgb,'rectangle',rectPos, 'LineWidth', 1);

end