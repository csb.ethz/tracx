%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [img] = addTimestampScalebar(this, img, imageFrequency, frame, varargin)
% ADDTIMESTAMPSCALEBAR Adds a timestamp (frame number or time in units time
% as well as a scale bar to an image.
%
% Args:
%    this (:obj:`object`):                             :class:`ImageProcessing` instance
%    img (:obj:`uint8` NxNx1 uint8, 16, 32, 64):       Image matrix         
%    imageFrequency (:obj:`float`):                    Image frequency in hours
%    frame(:obj:`int`):                                Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **scaleBarLength** (:obj:`float`): Length of scalebar in terms of unit.
%        * **pxToUnit** (:obj:`float`):       Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **unit** (:obj:`str`):             Unit of scalebar. Defaults to micron. Options are pixel, px, pixels.
%
% Returns
% -------
%    img :obj:`unit8` NxM                               
%                                                       Image matrix
%                                                       including timestamp
%                                                       and scalebar.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultScaleBarLength = 10; % in given units; typical microns.
defaultPxToUnit = 6.5/40; % pixelsize (width or height in microns) divided 
%                           by magnification of the objective.
defaultUnit = 'micron';

p = inputParser;
p.addRequired('imageFrequency', @(x) validateattributes(x,{'numeric'}, {'scalar'}));
p.addRequired('frame', @(x) validateattributes(x,{'numeric'}, {'scalar'}));
p.addParameter('scaleBarLength', defaultScaleBarLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('pxToUnit', defaultPxToUnit,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('unit', defaultUnit,  @(x)ischar(validatestring(x, {'micron', 'pixel', ...
    'px', 'pixels'})));

parse(p, imageFrequency, frame, varargin{:})

% Add time stamp
color = this.utils.getBWLabelColor(squeeze(double(img(7, 5, 1:3))./255)');
img = insertText(img, [7, 5], ...
    sprintf('%.2f h', round(frame * imageFrequency, 2)), ...
    'FontSize', 12, 'TextColor', color.*255, 'BoxOpacity', 0);
% Add scalebar
scaleBarVal = p.Results.scaleBarLength; % in microns
scaleBarWidth = floor( scaleBarVal / (p.Results.pxToUnit) );
scaleBarHeight = 3;
%
scaleBarXPos = 11;
scaleBarYPos = size(img,1)-28;
textCenterX = 7;
textCenterY = size(img,1)-25;
rectPosition = [scaleBarXPos, scaleBarYPos, scaleBarWidth, scaleBarHeight];
% hRect = rectangle('Position', rectPosition, 'FaceColor', imageLabelColor, ...
%     'EdgeColor', imageLabelColor);
img = insertShape(img, 'FilledRectangle', rectPosition, 'Color', color.*255);
if strcmp(p.Results.unit, 'micron')
    img = insertText(img, [textCenterX, textCenterY], sprintf(['%d ' char(181) 'm'], scaleBarVal), ...
        'FontSize', 12, 'TextColor', color.*255, 'BoxOpacity', 0);
else
    img = insertText(img, [textCenterX, textCenterY], sprintf(['%d px'], scaleBarVal), ...
        'FontSize', 12, 'TextColor', color.*255, 'BoxOpacity', 0);
end
end