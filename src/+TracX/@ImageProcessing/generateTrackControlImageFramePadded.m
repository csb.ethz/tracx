%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function labeledImg = generateTrackControlImageFramePadded( this, ...
    trackOfInterest, frameNumber, varargin)
% GENERATETRACKCONTROLIMAGEFRAMEPADDED Generates a control
% image where all cell tracks found on that frame are labeled
% with the tracked segmentation  mask as overlay to the raw
% (Brightfield) image. The final image is padded to keep the original image 
% size i.e. for cells at image boundary. In the corners of the generated control
% image frame information about tracking accuracy is encoded as
% mentioned in the note below.
%
% Args:
%    this (:obj:`object`):             :class:`ImageProcessing` instance 
%    trackOfInterest (:obj:`int` 1x1): Track number of a cell of interest
%    frameNumber (:obj:`int` 1x1):     Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **showImageIfTrackAbsent** (:obj:`bool`): Shows real image for image frames where a track is absent using its last position. Otherwise a black image is shown.
%
% Returns
% -------
%    labeledImg :obj:`array` NxM                               
%                                      Image where the track of
%                                      interest is labeled with
%                                      additional tracking accuracy
%                                      information at the image
%                                      corners.
%
% .. note:: 
%
%    - TopLeft = green   := track_index and track_index_qc are identical
%    - TopRight = red    := track was put to VeryOld
%    - BottomLeft = red  := Mask identity low (bad segmentation but still tracked.
%    - BottomRight = red := track_index and and mask identity ok but qc > thres
%
% :Authors:
%    Andreas P. Cuny - initial implementation

parser = inputParser;
parser.addParameter('showImageIfTrackAbsent', true, @(x) islogical(x));
parser.parse(varargin{:});

alpha = this.configuration.ParameterConfiguration.maskOverlayAlpha;
sideLength = this.configuration.ParameterConfiguration.cellThumbSideLength;
imageBitDepth = this.configuration.ProjectConfiguration.imageBitDepth;

% Load image and crop to same size as segmentation
img = this.io.readImageToGrayScale(fullfile(this.configuration. ...
    ProjectConfiguration.imageDir, this.configuration.ProjectConfiguration. ...
    imageFingerprintFileArray{frameNumber} ), ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray);

% Load segmentation mask
segmentationMask = this.io.readSegmentationMask(fullfile(...
    this.configuration.ProjectConfiguration.segmentationResultDir, ...
    this.configuration.ProjectConfiguration.trackerResultMaskFileArray{frameNumber}));

cellIndex = this.data.getFieldArrayForTrackIndexForFrame('track_index', ...
    frameNumber, trackOfInterest);

if ~isempty(cellIndex)
    maskCellIndex = unique(segmentationMask);
    maskCellIndex(maskCellIndex == 0) = [];
    maskTrackIndex = maskCellIndex;
    maskTrackIndex(maskTrackIndex ~= cellIndex) = 0;
    maskTrackIndex(maskTrackIndex == cellIndex) = trackOfInterest;
    
    if exist('changem', 'file') == 2
        segmentationMask = changem(segmentationMask, maskTrackIndex, maskCellIndex);
    else
        [lia, locb] = ismember(segmentationMask, maskCellIndex);
        segmentationMask(lia) = maskTrackIndex(locb(lia));
    end
    
    cellCenterX = this.data.getFieldArrayForTrackIndexForFrame('cell_center_x', ...
    frameNumber, trackOfInterest);
    cellCenterY = this.data.getFieldArrayForTrackIndexForFrame('cell_center_y', ...
    frameNumber, trackOfInterest);
    
    imageCropCoordinateArray = [(cellCenterX) - sideLength, (cellCenterY) - ...
        sideLength, 2 * sideLength,2 * sideLength];
    
    img = TracX.Utils.padImageCrop(img, imageCropCoordinateArray);
    segmentationMask = TracX.Utils.padImageCrop(...
        segmentationMask, imageCropCoordinateArray);
    segmentationMask2 = segmentationMask;
    
    % Create logical matrix for all selected cells on the mask
    segmentationMask2(segmentationMask2 > 0) = 1;
    
    if imageBitDepth == 8
        labeledImg = uint8(zeros(size(img,1), size(img,2), 3));
    elseif imageBitDepth == 16
        labeledImg = uint16(zeros(size(img,1), size(img,2), 3));
    elseif imageBitDepth == 32
        labeledImg = uint32(zeros(size(img,1), size(img,2), 3));
    elseif imageBitDepth == 64
        labeledImg = uint64(zeros(size(img,1), size(img,2), 3));
    else
        this.utils.printToConsole(sprintf(['The images in your project have an unsuported bit depth.' ...
            'Supported bit depths are: 8, 16, 32, 64, yours is: %d'], ...
            imageBitDepth))
    end
    r = img;
    g = img;
    b = img;
    
    % Overlay mask with image with defined alpha
    foreGround = zeros(size(segmentationMask));
    foreGround(:) = 2^imageBitDepth;
    overlay =  foreGround(logical(segmentationMask2)) .* alpha ...
        + g(logical(segmentationMask2)) .* (1 - alpha);
    g(logical(segmentationMask2)) = overlay;
    
%     % Mark image corners
%     switch this.get_data_wide_formated(this.Data.track_index_integrity( ...
% and(this.Data.track_index == trackOfInterest, this.Data.cell_frame == frameNumber)))
%         case 0
%             maskCorners = TrackerImgGen.get_CornerSquaresMask(img);
%             % maskCorners(maskCorners ~= 1) = 0;
%             g(logical(maskCorners)) = 2^16;
%         case 1
%             maskCorners = TrackerImgGen.get_CornerSquaresMask(img);
%             maskCorners(maskCorners ~= 2) = 0;
%             r(logical(maskCorners)) = 2^16;
%         case 2
%             maskCorners = TrackerImgGen.get_CornerSquaresMask(img);
%             maskCorners(maskCorners ~= 3) = 0;
%             r(logical(maskCorners)) = 2^16;
%         case 3
%             maskCorners = TrackerImgGen.get_CornerSquaresMask(img);
%             maskCorners(maskCorners ~= 4) = 0;
%             r(logical(maskCorners)) = 2^16;
%     end
    
    labeledImg(:,:,1) = r;
    labeledImg(:,:,2) = g;
    labeledImg(:,:,3) = b;
else
    
    if imageBitDepth == 8
        labeledImg = uint8(zeros(2 * sideLength + 1, 2 * ...
            sideLength + 1, 3));
    elseif imageBitDepth == 16
        labeledImg = uint16(zeros(2 * sideLength + 1, 2 * ...
            sideLength + 1, 3));
    elseif imageBitDepth == 32
        labeledImg = uint32(zeros(2 * sideLength + 1, 2 * ...
            sideLength + 1, 3));
    elseif imageBitDepth == 64
        labeledImg = uint64(zeros(2 * sideLength + 1, 2 * ...
            sideLength + 1, 3));
    else
        this.utils.printToConsole(sprintf(['The images in your project have an unsuported bit depth.' ...
            'Supported bit depths are: 8, 16, 32, 64, yours is: %d'], ...
            imageBitDepth))
    end
    
    if parser.Results.showImageIfTrackAbsent == 1
        lastFrameOfTrack = unique(this.data.getFieldArrayForTrackIndex(...
            'track_end_frame', trackOfInterest));
        cellCenterX = this.data.getFieldArrayForTrackIndexForFrame('cell_center_x', ...
            lastFrameOfTrack, trackOfInterest);
        cellCenterY = this.data.getFieldArrayForTrackIndexForFrame('cell_center_y', ...
            lastFrameOfTrack, trackOfInterest);
        
        imageCropCoordinateArray = [(cellCenterX) - sideLength, (cellCenterY) - ...
            sideLength, 2 * sideLength,2 * sideLength];
        labeledImg(:,:,1) = TracX.Utils.padImageCrop(img, imageCropCoordinateArray);
        labeledImg(:,:,2) = TracX.Utils.padImageCrop(img, imageCropCoordinateArray);
        labeledImg(:,:,3) = TracX.Utils.padImageCrop(img, imageCropCoordinateArray);
    else
        allChannels = zeros(2* sideLength + 1, 2 * sideLength + 1);
        labeledImg(:,:,1) = allChannels;
        labeledImg(:,:,2) = allChannels;
        labeledImg(:,:,3) = allChannels;
    end
end

end