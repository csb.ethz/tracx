%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [fh] = generateLineageFrame(this, rootTrack, imageFrequency, frame, ...
    varargin)
% GENERATELINEAGEFRAME Generates a figure for a specific image
% frame for the lineage movie generation. The figure consists of a labeled
% image, the lineage tree and a plotting area to plot quantified signals
% over time.
%
% Args:
%    this (:obj:`object`):                    :class:`ImageProcessing` instance   
%    rootTrack (:obj:`int`):                  Track index of the lineage root
%                                             to be visualized.
%    imageFrequency (:obj:`float`):           Image frequency in hours
%    frame(:obj:`int`):                       Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **figurePosition** (:obj:`float`): Figure position and size (x,y, width, height) array for the movie generation. Default is: [10 10 1350 600]
%        * **scaleBarLength** (:obj:`float`): Length of scalebar in terms of unit.
%        * **pxToUnit** (:obj:`float`):       Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **unit** (:obj:`str`):             Unit of scalebar. Defaults to micron. Opthons are pixel, px, pixels.
%
% Returns
% -------
%    fh :obj:`object`                                
%                                           Figure handle.                                        
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Parsing the inputs
defaultFigurePosition = [10 10 1350 600];
defaultScaleBarLength = 10; % in given units; typical microns.
defaultPxToUnit = 6.5/40; % pixelsize (width or height in microns) divided 
%                           by magnification of the objective.

p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('rootTrack',  @(x) validateattributes(x,{'numeric'}, {'scalar'}));
p.addRequired('imageFrequency', @(x) validateattributes(x,{'numeric'}, {'scalar'}));
p.addRequired('frame', @(x) validateattributes(x,{'numeric'}, {'scalar'}));
p.addParameter('figurePosition', defaultFigurePosition,  @(x)validateattributes(x, ...
    {'numeric'}, {'size', [1, 4]}));
p.addParameter('scaleBarLength', defaultScaleBarLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('pxToUnit', defaultPxToUnit,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));

parse(p, this, rootTrack, imageFrequency, frame, varargin{:})

% Turn off warnings
warning off

% Set axis to real black and override Matlab' default.
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'}, ...
    {'k','k','k'})

cellDivisionType = this.configuration.ProjectConfiguration.cellDivisionType;
if cellDivisionType == 0
    symDiv = true;
else
    symDiv = false;
end

expEndTime = this.configuration.ProjectConfiguration.trackerEndFrame * imageFrequency;
% Load data to be plotted and normalize if needed
%> @todo SIMPLYFY
% dataIdx = this.data.SegmentationData.track_index == rootTrack;
%> Get first the quantification data and then subset for current track!
%> fluoBright = this.data.QuantificationData.getFieldNameForFluoChannel('fluo_bright_total', 1)
%> fluoBrightTrack = fluoBright(dataIdx)
trackUUID = this.data.SegmentationData.uuid(this.data.SegmentationData.track_index == rootTrack);
presentFrames = this.data.SegmentationData.cell_frame(this.data.SegmentationData.track_index == rootTrack);
presentFrames = presentFrames(ismember(trackUUID, this.data.QuantificationData.uuid)).*imageFrequency;
fluoBrightTrack = this.data.QuantificationData.fluo_bright_total(ismember(this.data.QuantificationData.uuid, trackUUID));
fluoCellTotalTrack = this.data.QuantificationData.fluo_cell_total(ismember(this.data.QuantificationData.uuid, trackUUID));
fluoCellAreaTrack = this.data.SegmentationData.cell_area(this.data.SegmentationData.track_index == rootTrack);
fluoCellAreaTrack = fluoCellAreaTrack(ismember(trackUUID, this.data.QuantificationData.uuid)).*imageFrequency;
fluoID = this.data.QuantificationData.fluo_id(ismember(this.data.QuantificationData.uuid, trackUUID));

% maxSig = max(max(fluoBrightTrack(fluoID == 2)./fluoCellTotalTrack(fluoID == 2)));
% minSig = min(min(fluoBrightTrack(fluoID == 2)./fluoCellTotalTrack(fluoID == 2)));
% signal = fluoBrightTrack(fluoID == 2)./fluoCellTotalTrack(fluoID == 2);
% signal2 = fluoBrightTrack(fluoID == 2)./(fluoCellAreaTrack.*(p.Results.pxToUnit)^2); % pixelsize/magnification
% signalNorm = (signal - min(min(signal))) ./ (max(max(signal)) - min(min(signal)));

% Get track indices and track parents
trackIndexArray = this.data.getFieldArray('track_index');
trackParentArray = this.data.getFieldArray('track_parent');

% Get all root related tracks
toRootTrackRelatedCellArray = this.lineage. ...
    getRootTrackRelatedDaughterArray(trackIndexArray, trackParentArray, ...
    rootTrack );

% Construct lineage tree object for the given rootTrack
lineageTree = this.lineage.getRootTrackTreeObject( trackIndexArray, ...
    trackParentArray, rootTrack, toRootTrackRelatedCellArray );

% Add root to root track related cells
toRootTrackRelatedCellArray = [rootTrack, toRootTrackRelatedCellArray];

% Create a constant colormap and assign the colors to each root related
% tack
% colors = this.utils.getColorMap(max(toRootTrackRelatedCellArray));
% cMap = zeros(max(toRootTrackRelatedCellArray), 3);
% cMap(toRootTrackRelatedCellArray, :) = colors(toRootTrackRelatedCellArray, :);
% cMap = cMap .* this.configuration.ParameterConfiguration.maskOverlayAlpha;

% Get position and labels to add to the image
%> @todo move into mask function as option
trackIndex = this.data.getConditionalFieldArray('track_index', ...
    'track_index', toRootTrackRelatedCellArray);
centerX = this.data.getConditionalFieldArray('cell_center_x', ...
    'track_index', toRootTrackRelatedCellArray);
centerY = this.data.getConditionalFieldArray('cell_center_y', ...
    'track_index', toRootTrackRelatedCellArray);
framesRelatives = this.data.getConditionalFieldArray('cell_frame', ...
    'track_index', toRootTrackRelatedCellArray);

% xMax = max(presentFrames) + 0.15;

fh = figure('Renderer', 'painters', 'Position', p.Results.figurePosition, ...
    'visible', 'off');

% plotIdx = frame; %find(round(presentFrames/(imageFrequency)) == frame);
cMap = this.data.getFieldArrayColorMap('track_index');
cMapF = cMap(this.data.getFieldArray('cell_frame') == frame, :);
tracksOnFrame = this.data.getFieldArrayForFrame('track_index', frame);
[tracksOnFrameS, tracksOnFrameSIdx] = sort(tracksOnFrame);
colormap = zeros(max(tracksOnFrame), 3);
colormap(tracksOnFrameS, :) = cMapF(tracksOnFrameSIdx, :);
img = this.labelImageTrackMasks(toRootTrackRelatedCellArray, frame, ...
    'maskOverlayAlpha', 1, 'customColorMap', colormap);
% 'maskOverlayAlpha', 1
[bx, by, bw, bh] = this.utils.getSegAreaBoundingBox(centerX, centerY, ...
    (round(mean(this.data.getFieldArray('cell_majoraxis'))/10)*10)/2);
lS = max([bw, bh]);
nx = (bx + bh/2) - lS/2;
ny = by + bw/2 - lS/2;
% RGB = insertShape(img, 'rectangle',[nx, ny, lS, lS],'LineWidth',5);
%figure; imshow(RGB)
% Crop with segmetnation crop if present
% if ~isempty(this.configuration.ProjectConfiguration. ...
%         imageCropCoordinatePriorReversionArray)
%     img = this.utils.padImageCrop(img, this.configuration.ProjectConfiguration. ...
%         imageCropCoordinatePriorReversionArray);
% end

img = this.utils.padImageCrop(img, [nx, ny, lS, lS]);
%figure; imshow(img)

subplot(2,4,[1,2, 5, 6], 'Position', [0.0100 0.0300 0.4928 0.9350])
imshow(img)

%> @todo move to image creation function
%> Have flag to add labels onto mask ideally should be done in the get
%> image step
if ~isempty(framesRelatives == frame)
    hold on
    th = text(centerX(framesRelatives == frame)-nx, ...
        centerY(framesRelatives == frame)-ny, ...
        num2str(trackIndex(framesRelatives == frame)), 'Color', 'w', ...
        'FontWeight', 'bold', 'HorizontalAlignment', 'center');
    currentLabels = trackIndex(framesRelatives == frame);
    for m = 1:numel(currentLabels)
        th(m).Color = this.utils.getBWLabelColor( colormap(currentLabels(m), :));
    end
    
end
hold on
% Add time stamp
text(size(img, 2)*0.03, size(img, 1)*0.03, ...
    sprintf('%g h', round(frame * imageFrequency,2)), ...
    'FontSize', 12, 'Color', 'w')

% Add scalebar
scaleBarVal = p.Results.scaleBarLength; % in microns
scaleBarWidth = floor( scaleBarVal / (p.Results.pxToUnit) );
scaleBarHeight = 2;
imageLabelColor = [1,1,1];
%
scaleBarXPos = size(img,2)*0.03;
scaleBarYPos = size(img,1)*0.95;
textCenterX = scaleBarXPos;
textCenterY = size(img,1)*0.98; %(scaleBarYPos + scaleBarHeight + 12);
rectPosition = [scaleBarXPos, scaleBarYPos, scaleBarWidth, scaleBarHeight];
hRect = rectangle('Position', rectPosition, 'FaceColor', imageLabelColor, ...
    'EdgeColor', imageLabelColor);
text(textCenterX,textCenterY,sprintf(['%d ' char(181) 'm'], scaleBarVal), ...
    'FontSize', 12, 'Interpreter','tex', 'Color', imageLabelColor);

% isG1All = this.data.getFieldArray('track_cell_cycle_phase');
% isG1All = this.lineage.setSignalInNucleus(1, false);
% isG1 = isG1All(dataIdx);
%
% phaseStart = find(diff(isG1(:, 1)) > 0);
% phaseEnd = find(diff(isG1(:, 1)) < 0);
% if isempty(phaseEnd) && ~isempty(phaseStart)
%     phaseEnd = numel(isG1);
% end
% if phaseStart(1) > phaseEnd(1) % plot is shaded from start
%     phaseStart = [1; phaseStart];
% end
% if phaseStart(end) > phaseEnd(end) % plot is shaded until end
%     phaseEnd = [phaseEnd; length(presentFrames)];
% end

% Plot some signal above the tree. @Todo make it user selectable which
% signals to plot. I.e. signal, volume, area etc. colored by track.
% subplot(2,4,[3,4])
% for j = 1:min(length(phaseStart), length(phaseEnd))
%     patch([repmat( presentFrames(phaseStart(j)),1,2) repmat( ...
%         presentFrames(phaseEnd(j)),1,2)], ...
%         [get(gca,'YLim') fliplr(get(gca,'YLim'))], ...
%         [0 0 0 0], [0.85, 0.85, 0.85], 'EdgeColor', 'none');
% end
% hold on
%
% [~, plotToIdx] = min(abs(presentFrames-plotIdx * imageFrequency));
% plot(presentFrames(1:plotToIdx), signalNorm(1:plotToIdx), '-ok')
% hold on
% if frame*(imageFrequency) ~= presentFrames(end)
% rectangle('Position',[frame*(imageFrequency), 0.0001, xMax- ...
%     (frame*imageFrequency), xMax-0.02], ...
%     'FaceColor',[1 1 1], 'EdgeColor',[1 1 1], 'LineWidth',1)
% end
% ylabel('Whi5 norm. fluo. (a.u.)')
% set(gca, 'layer', 'top');
% xlim([0, xMax])
% ylim([0, 1])
% box off;

sh = subplot(2,4,[7,8], 'Position', [0.5422 0.1 0.3628 0.8212]); % [0.5422 0.1100 0.3628 0.3412]
%


% Determine the timepoint for the division
duration = TracX.ExternalDependencies.tree.tree(lineageTree, 'clear');
trackStarts = TracX.ExternalDependencies.tree.tree(lineageTree, 'clear');
labelsToGetDTime = cell2mat(lineageTree.Node);
[divisionFrameArray] = this.lineage.getDivisionFrames(labelsToGetDTime);
for la = 1:numel(labelsToGetDTime)
    duration = duration.set(la, divisionFrameArray(la).* imageFrequency);
    trStart = unique(this.data.getFieldArrayForTrackIndex(...
            'track_start_frame', labelsToGetDTime(la)));
    trackStarts = trackStarts.set(la, trStart.* imageFrequency);
end

% Plot the lineage tree
[vlh, hlh, tlh, hl2h] = lineageTree.plot(duration, 'SymDiv', symDiv, ...
    'MaxTime', expEndTime, 'Parent', sh, 'YLabel', {''},'TextRotation', 0, ...
    'startTree', trackStarts);
% Color the nodes and leafs of the tree
currTr = this.data.getFieldArray('track_index');
nodeLabels = cell2mat(lineageTree.Node);
for j = 1:numel(nodeLabels)
    aboveTreshold = lineageTree == nodeLabels(j);
    iterator = aboveTreshold.depthfirstiterator;
    for i = iterator
        if  aboveTreshold.get(i)
            if any(tracksOnFrameS == nodeLabels(j))
                
                set( vlh.get(i), 'Color' , colormap(nodeLabels(j), :) )
                set( hlh.get(i), 'Color' , colormap(nodeLabels(j), :) )
                set( hl2h.get(i), 'Color' , colormap(nodeLabels(j), :) )
                set( tlh.get(i), 'BackgroundColor' , colormap(nodeLabels(j), :) )
                set( tlh.get(i), 'FontWeight', 'bold')
                set( tlh.get(i), 'Color' , this.utils.getBWLabelColor(...
                    colormap(nodeLabels(j), :)))
                
            else
                
                set( vlh.get(i), 'Color' , cMap(find(nodeLabels(j)==currTr, 1, 'first'),:) )
                set( hlh.get(i), 'Color' , cMap(find(nodeLabels(j)==currTr, 1, 'first'),:) )
                set( hl2h.get(i), 'Color' , cMap(find(nodeLabels(j)==currTr, 1, 'first'),:) )
                set( tlh.get(i), 'BackgroundColor' , cMap(find(nodeLabels(j)==currTr, 1, 'first'),:) )
                set( tlh.get(i), 'FontWeight', 'bold')
                set( tlh.get(i), 'Color' , this.utils.getBWLabelColor(...
                    cMap(find(nodeLabels(j)==currTr, 1, 'first'),:)))
                
            end
        end
    end
end

if cellDivisionType == 0
    view([90 -90]);
else
    view([0 -90]);
end
hold on

% We use a white rectangle to hide the part of the tree which is not yet
% present on the current frame.
newYLim = sh.XLim; % due to rotation
newXLim = sh.YLim; % due to rotation
if cellDivisionType == 0
    if frame*imageFrequency < this.configuration.ProjectConfiguration. ...
            trackerEndFrame*imageFrequency
        % White rectangle to hide part of the tree not shown for the
        % current frame
        rectangle('Position',[newYLim(1)-0.09, frame*imageFrequency, ...
            sh.XLim(2)+0.2, (sh.YLim(2))-frame*imageFrequency + 0.01], ...
            'FaceColor',[1 1 1], 'EdgeColor',[1 1 1], 'LineWidth',1)
        % Line for the current time
        line([newYLim(1)-0.1, sh.XLim(2)], ...
            [frame*imageFrequency, frame*imageFrequency], ...
            'Color','black','LineStyle','--')
    end
else
    if frame*imageFrequency < this.configuration.ProjectConfiguration. ...
            trackerEndFrame*imageFrequency
        % White rectangle to hide part of the tree not shown for the
        % current frame
        rectangle('Position',[frame*imageFrequency, -0.02, ...
            newYLim(2)-frame*imageFrequency + 0.1,  newXLim(2)+0.12], ...
            'FaceColor',[1 1 1], 'EdgeColor',[1 1 1], 'LineWidth',1)
        % Line for the current time
        line([frame*imageFrequency, frame*imageFrequency], ...
            [newYLim(1), newXLim(2)+0.12], ...
            'Color','black','LineStyle','--')
    end
end

if cellDivisionType == 0
    
    ylim([0, newXLim(2)])
    xlim([newYLim(1)-0.15, newYLim(2) + 0.2])
    
    box off;
    grid off;
    set(gca,'XTickLabel',[]);
    
    set(gca,'XTick',[]);
    set(gcf,'color',[1,1,1].*1);
    ylabel('Time (h)')
    set(gca, 'YGrid', 'on')
    
else
    
    ylim([-0.02, newXLim(2)+0.15])
    xlim([newYLim(1), newYLim(2) + 0.2])
    
    box off;
    grid off;
    set(gca,'YTickLabel',[]);
    set(gca,'YTick',[]);
    set(gcf,'color','w');
    xlabel('Time (h)')
    set(gca, 'XGrid', 'on')
end

end
