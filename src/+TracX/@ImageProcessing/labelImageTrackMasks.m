%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledImg ] = labelImageTrackMasks( this, trackToLabelArray, ...
    frameNumber, varargin )
% LABELIMAGETRACKMASKS Generates a color image where all cell
% tracks found on that frame are labeled with the segmentation mask. The 
% mask are overlayed onto the raw (Brightfield) image. The transparency
% of the mask (maskOverlayAlpha) and the brightness of the image
% (imageTransparency) can be optionally controlled. Also a the labels can
% be colored with custom colors from a color map of the appropriate size.
%
% Args:
%    this (:obj:`object`):                  :class:`ImageProcessing`
%                                           instance
%    trackToLabelArray (:obj:`array` 1xK):  Array of track numbers which
%                                           should be labeled with their 
%                                           segmentation mask on an image.
%    frameNumber (:obj:`int` 1x1):          Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **maskOverlayAlpha** (:obj:`float` 0-1): Sets the transparency of the mask in the range[0,1].
%        * **imageBrightness** (:obj:`float` 0-1): Sets the brightness of the image in the range [0,1].
%        * **customColorMap** (:obj:`array`, Kx3): Applies a custom colormap of the size [K,3], with K = max(track_index) number
%
% Returns
% -------
%    labeledImg: :obj:`array` NxM
%                                           RGB image with all tracks of
%                                           interest are labeled with their 
%                                           masks..
% 
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultMaskOverlayAlpha = this.configuration.ParameterConfiguration. ...
    maskOverlayAlpha;
defaultImageBrightness = 1;
defaultCustomColorMap = [];
validMaskOverlayAlpha = {'>=', 0, '<=', 1};
validImageTransparency = {'>=', 0, '<=', 1};

p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('trackToLabelArray', @isnumeric)
p.addRequired('frameNumber', @isnumeric)
p.addParameter('maskOverlayAlpha', defaultMaskOverlayAlpha, @(x)validateattributes(x, ...
    {'double'}, validMaskOverlayAlpha, 'Rankings'));
p.addParameter('imageBrightness', defaultImageBrightness, @(x)validateattributes(x, ...
    {'double'}, validImageTransparency, 'Rankings'));
p.addParameter('customColorMap', defaultCustomColorMap, @isnumeric)
parse(p, this, trackToLabelArray, frameNumber, varargin{:})

% Load image and crop to same size as segmentation
img = this.io.readImageToGrayScale(fullfile(...
    this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.imageFingerprintFileArray{frameNumber}), ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray);
img = this.normalizeImage(img);

% Load tracking masks
mask = this.io.readSegmentationMask(fullfile(...
    this.configuration.ProjectConfiguration.segmentationResultDir, ...
    this.configuration.ProjectConfiguration.trackerResultMaskFileArray{frameNumber}));
labelsToKeep = unique(mask);
labelsToKeep(~ismember(labelsToKeep, trackToLabelArray)) = 0;
mask = this.relabelMaskIndices(mask, labelsToKeep, unique(mask));

labeledImg = this.overlayMaskOnImage(img, mask, 'maskOverlayAlpha', ...
    p.Results.maskOverlayAlpha, 'imageBrightness', p.Results.imageBrightness, ...
    'customColorMap',  p.Results.customColorMap );
end