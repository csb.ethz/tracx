%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledImg ] = generateAnnotatedControlImageFrame( this, ...
    frameNumber, varargin)
% GENERATEANNOTATEDCONTROLIMAGEFRAME Generates a control
% image where all data found on an image frame for a given property are
% overlayed onto a (Brightfield) image. The default property is
% 'track_index' but it can be any of 'SegmentationData'.
%
% Args:
%    this (:obj:`obj`):                        :class:`ImageProcessing` object
%    frameNumber (:obj:`int`):                 Image frame number.
%    varargin (:obj:`str varchar`):
%
%        * **fieldName** (:obj:`str`):              Property to be displayed on the image. Defaults to 'track_index' can be any propery of :obj:`+TracX.SegmentationData`.
%        * **highlightFieldNameVal** (:obj:`int`):  Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **fontSize** (:obj:`str`):               Font size of property to be displayed. Defaults to 14.
%		 * **fontName** (:obj:`str`):               Font name of property to be displayed. Defaults to Arial.
%		 * **fontEmphasis** (:obj:`int`):           Font emphasis of property to be displayed. Defaults to 0 (regular). Options 1 (bold), 2 (italic).
%        * **removeContainedTracks** (:obj:`bool`): If contained tracks within tracks should be removed. Defaults to true.
%        * **isMarker** (:obj:`bool`):              If markers should be displayed. Defaults to false.
% 
% Returns
% -------
%  labeledImg: (:obj:`array`, NxM)          
%                                            Image with all tracks present labeled.
% :Authors:
% 	Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;
defaultFontSize = 14;
defaultFontName = 'Arial';
defaultFieldName = 'track_index';
defaultEmphasis = 0; % 0 == regular, 1 == bold
defaultRemoveContainedTracks = true;
defaultIsMarker = false;
defaultHighlightFiledNameVal = [];
validFontSize = @(x) isnumeric(x) || x>4;
validFontName = @(x) ischar(x);
validFontEmphasis = @(x) isnumeric(x) && x>=0 && x<=2;
validFieldNames = [fieldnames(this.data.SegmentationData);fieldnames(this.lineage.lineageData)];
addRequired(p, 'this', @isobject);
addRequired(p, 'frameNumber', @(x) isnumeric(x) && x>=1);
addOptional(p, 'fieldName', defaultFieldName, ...
    @(x) any(validatestring(x, validFieldNames)));
addOptional(p, 'highlightFieldNameVal', defaultHighlightFiledNameVal, ...
    @(x) all(isnumeric(x)) && all(x>=1));
addOptional(p, 'fontSize', defaultFontSize, validFontSize);
addOptional(p, 'fontName', defaultFontName, validFontName);
addOptional(p, 'fontEmphasis', defaultEmphasis, validFontEmphasis);
addOptional(p, 'removeContainedTracks', defaultRemoveContainedTracks, @islogical);
addOptional(p, 'isMarker', defaultIsMarker, @islogical);

parse(p, this, frameNumber, varargin{:});

% maxVal = this.configuration.ProjectConfiguration.imageMaxSampleValue;

if p.Results.isMarker
    x = this.lineage.lineageData.getFieldArrayForFrame('obj_center_x', frameNumber);
    y = this.lineage.lineageData.getFieldArrayForFrame('obj_center_y', frameNumber);
    z = this.lineage.lineageData.getFieldArrayForFrame('obj_center_z', frameNumber);
    z = z / this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval;
    indexLabel = this.lineage.lineageData.getFieldArrayForFrame(p.Results.fieldName, ...
        frameNumber);
else
    
    x = this.data.getFieldArrayForFrame('cell_center_x', frameNumber);
    y = this.data.getFieldArrayForFrame('cell_center_y', frameNumber);
    z = this.data.getFieldArrayForFrame('cell_center_z', frameNumber);
    z = z / this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval;
    
    indexLabel = this.data.getFieldArrayForFrame(p.Results.fieldName, frameNumber);
    track_contained_in_track = this.data.getFieldArrayForFrame(...
        'track_contained_in_track', frameNumber);
    
    % Subset for tracks not contained in others only
    if p.Results.removeContainedTracks == true && ~isempty(track_contained_in_track)
        x = x(isnan(track_contained_in_track));
        y = y(isnan(track_contained_in_track));
        z = z(isnan(track_contained_in_track));
        indexLabel = indexLabel(isnan(track_contained_in_track));
    end
end

for i = 1:this.configuration.ProjectConfiguration.imageDepth
    baseImg(:,:,i) = this.io.readImageToGrayScale(fullfile(...
        this.configuration.ProjectConfiguration.imageDir, ...
        this.configuration.ProjectConfiguration. ...
        imageFingerprintFileArray{frameNumber}), ...
        this.configuration.ProjectConfiguration.imageCropCoordinateArray, i);
end

% Normalize
baseImg = uint8(255 * this.normalizeImage(baseImg));
if this.configuration.ParameterConfiguration.data3D
    if size(baseImg, 4) ~= 3
        r = baseImg;
        g = baseImg;
        b = baseImg;
    else
        r = baseImg(:, :, :, 1);
        g = baseImg(:, :, :, 2);
        b = baseImg(:, :, :, 3);
    end
    
    rgbImg = cat(4, r, g, b);
else
    
    if size(baseImg, 3) ~= 3
        r = baseImg;
        g = baseImg;
        b = baseImg;
    else
        r = baseImg(:, :, 1);
        g = baseImg(:, :, 2);
        b = baseImg(:, :, 3);
    end
    
    rgbImg = cat(3, r, g, b);
end

if ~iscell(indexLabel)
    indexLabelArray = cell(numel(indexLabel),1);
    for iElement = 1:numel(indexLabel)
        iLabel = indexLabel(iElement);
        iLabel = round(iLabel, 4);
        indexLabelArray{iElement} = num2str(iLabel);
    end
else
    if ~ischar(indexLabel{1})
        indexLabelArray = cellfun(@(x) num2str(x), indexLabel, ...
            'UniformOutput', false);
    else
        indexLabelArray = indexLabel;
    end
end

% Convert Matlab array to java BufferedImage for speed reasons
font = java.awt.Font(p.Results.fontName, p.Results.fontEmphasis, ...
    p.Results.fontSize);
image = this.utils.rgb2java(rgbImg);

if isempty(p.Results.highlightFieldNameVal)
    for iElement = 1:numel(indexLabelArray)
        graphics = image.getGraphics();
        graphics.setFont(font);
        graphics.setColor(java.awt.Color.green)
        graphics.drawString(indexLabelArray{iElement}, round(x(iElement) - ...
            p.Results.fontSize/3), ...
            round(y(iElement)+p.Results.fontSize/3));
        graphics.dispose();
    end
else
    selIdxP = find(ismember(indexLabel, p.Results.highlightFieldNameVal));
    selIdx = find(~ismember(indexLabel, p.Results.highlightFieldNameVal));

    for iElement = selIdxP'
        graphics = image.getGraphics();
        graphics.setFont(font);
        graphics.setColor(java.awt.Color.red)
        graphics.drawString(indexLabelArray{iElement}, round(x(iElement) - ...
            p.Results.fontSize/3), ...
            round(y(iElement)+p.Results.fontSize/2));
        graphics.dispose();
    end
    for iElement = selIdx'
        graphics = image.getGraphics();
        graphics.setFont(font);
        graphics.setColor(java.awt.Color.green)
        graphics.drawString(indexLabelArray{iElement}, round(x(iElement) - ...
            p.Results.fontSize/3), ...
            round(y(iElement) + p.Results.fontSize/2));
        graphics.dispose();
    end
end

% Convert Java BufferedImage back to Matlab array
nrows = image.getHeight;
ncols = image.getWidth;
channel = image.getRaster.getNumBands;

pixelsData = reshape(typecast(image.getData.getDataStorage, 'uint8'), ...
    channel, ncols, nrows);

labeledImg = cat(channel, ...
    transpose(reshape(pixelsData(1, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(2, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(3, :, :), ncols, nrows)));

labeledImg = squeeze(labeledImg);
end