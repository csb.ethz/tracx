%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = generateMergedMask(this)
% GENERATEMERGEDMASK Merges the bud mask with the mother mask
% prior cell division if exists (i.e. with CellX segmentation it does).
%
% Args:
%    this (:obj:`object`):                :class:`ImageProcessing` instance
%
% Returns
% -------
%    void: (:obj:`-`)
%                                         Corrected masks are saved
%                                         directly to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

SE = strel('disk', 6, 6);
se = strel('disk', 1);

if this.data.TmpLinAss.reconstructionIsFinished == 0
    this.utils.printToConsole(['ERROR: Run lineage recunstruction or',...
        'getLineageSummary first. Returning.'])
    return
end

if isempty(gcp('nocreate'))
    this.utils.printToConsole('INFO: Starting parpool')
    parpool();
    this.utils.printToConsole('INFO: Starting parpool done.')
end

if isempty(this.lineage.tracks2Merge)
    this.lineage.tracks2Merge = this.lineage.getTracks2Merge();
end

allFrames = this.configuration.ProjectConfiguration.trackerStartFrame: ...
    this.configuration.ProjectConfiguration.trackerEndFrame-1;

pBar = this.utils.progressBar(numel(allFrames), ...
    'IsParallel', true, ...
    'WorkerDirectory', pwd, ...
    'Title', 'MERGING: Buds prior division' ...
    );

tracks2Merge = this.lineage.tracks2Merge;
% Process the frames in parallel while fixing the labels sequentially
pBar.setup([], [], []);
parfor f = 1:numel(allFrames)
    
    fn = allFrames(f);
    % Read mask
    mask = this.io.readSegmentationMask(fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, this.configuration. ...
        ProjectConfiguration.trackerResultMaskFileArray{fn}));
    maskOut = mask;
    % Get tracks to be merged on the current frame
    tracks2MergeSub = tracks2Merge(tracks2Merge(:,1) == fn, :);
    
    for d = 1:size(tracks2MergeSub, 1)
        
        motherTrack = tracks2MergeSub(d, 2);
        daughterTrack = tracks2MergeSub(d, 3);
        
        maskTemp = mask;
        maskTemp(~ismember(mask, [motherTrack, daughterTrack])) = 0;
        maskOut(ismember(mask, [motherTrack, daughterTrack])) = 0;
        maskBin = imbinarize(maskTemp);
        maskDil = imdilate(maskBin, SE);
        maskDil = imclose(maskDil, se);
        maskErode = imerode(maskDil, SE);
        
        % Get indices
        [r, c] = find(maskErode == 1);
        ind = sub2ind(size(mask),r,c);
        
        % Add merged mass label back
        maskOut(ind) = motherTrack;
        
    end
    
    % Write updated mask to mat
    this.io.writeToMAT(maskOut, this.configuration.ProjectConfiguration. ...
        segmentationResultDir, ...
        strjoin({'final', this.configuration.ProjectConfiguration. ...
        trackerResultMaskFileArray{fn}}, '_'))

   TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
end
end