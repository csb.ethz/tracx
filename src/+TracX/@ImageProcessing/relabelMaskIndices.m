%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ maskRelabeled ] = relabelMaskIndices(~, mask, newLabelArray,...
    oldLabelArray )
% RELABELMASKINDICES Relabels indices on a 2D mask [NxM].
%
% Args:
%    ignoredArg (:obj:`object`):          :class:`ImageProcessing` instance
%    mask (:obj:`uint16` NxM):            Matrix with numerical labels
%    newLabelArray (:obj:`int` 1xN):      Array containing the new
%                                         numerical labels to replace
%                                         the old ones with.
%    oldLabelArray (:obj:`int` 1xN):      Array containing all the
%                                         numerical labels of mask.
% 
% Returns
% -------
%    maskRelabeled :obj:`double` NxM                               
%                                                       Matrix with labels 
%                                                       replaced.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if exist('changem', 'file') == 2
    maskRelabeled = changem(mask, newLabelArray, oldLabelArray);
else
   [lia, locb] = ismember(mask, oldLabelArray);
   mask(lia) = newLabelArray(locb(lia));
   maskRelabeled = mask;
end

end