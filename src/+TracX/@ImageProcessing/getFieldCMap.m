%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [cMap, cMapFull, cLim] = getFieldCMap(this, property)
% GETFIELDCMAP Returns a colormap based on a specific property. Where as each
% track_index is colored by a unique color. For track_parent each parent
% gets a unique_color that changes over time to longer the time distance to
% cell division. All other properties get a unique time invariant color.
%
% Args:
%    this (:obj:`object`):                             :class:`ImageProcessing` instance   
%    property (:obj:`str`):                            :class:`SegmentationData` property          
%
% Returns
% -------
%    cMap :obj:`float` Kx3                               
%                                                       Color map for track
%                                                       indices
%    cMapFull :obj:`float` Kx3                               
%                                                       Full color map
%    cLim :obj:`float` 1x2                               
%                                                       Min and max
%                                                       property values
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Check if property is non empty. Fallback to track_index. Run lineage
% reconstruction first.
if isempty(this.data.SegmentationData.(property))
    property = 'track_index';
end
% Check if property is non empty. Fallback to cell_index. Run tracker
% first.
if isempty(this.data.SegmentationData.(property))
    property = 'cell_index';
end

if strcmp(property, 'track_index')
    uniqueTracks = unique(this.data.SegmentationData.track_index);
    trackColors = hsv(length(uniqueTracks));
    c = trackColors;
    cMap = c(this.data.SegmentationData.track_index, :);
    cMapFull = c;
    cLim = [min(this.data.SegmentationData.track_index), ...
        max(this.data.SegmentationData.track_index)];
elseif strcmp(property, 'track_parent')
    cMap = this.lineage.recursiveTrackParentColoring(0, [0.5 0.5 0.5],...
        zeros(length(this.data.SegmentationData.track_index), 3), ...
        this.data.SegmentationData.track_index, ...
        this.data.SegmentationData.track_parent);
    cMapFull = cMap;
    cLim = [min(this.data.SegmentationData.track_parent), ...
        max(this.data.SegmentationData.track_parent)];
else
    c = jet(64);
    values = this.data.SegmentationData.(property);
    LuT = linspace(min(values), max(values), 64);
    cMap = interp1(LuT, c, values);
    cMapFull = c;
    cLim = [min(values), max(values)];
end
end