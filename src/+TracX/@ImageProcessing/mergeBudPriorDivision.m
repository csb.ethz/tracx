%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = mergeBudPriorDivision(this)
% MERGEBUDPRIORDIVISION Merges the bud mask with the mother mask
% prior cell division if exists (i.e. with CellX segmentation it does).
%
% Args:
%    this (:obj:`object`):                :class:`ImageProcessing` instance
%
% Returns
% -------
%    void: (:obj:`-`)
%                                         Corrected masks are saved
%                                         directly to disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

SE = strel('disk', 6, 6);
se = strel('disk', 1);

if this.data.TmpLinAss.reconstructionIsFinished == 0
   this.utils.printToConsole(['ERROR: Run lineage recunstruction or',...
       'getLineageSummary first. Returning.'])
   return
end

if isempty(gcp('nocreate'))
    this.utils.printToConsole('INFO: Starting parpool')
    parpool();
    this.utils.printToConsole('INFO: Starting parpool done.')
end

% % Matrix of row size of the lineageTable times number of tracked frames.
% frameMatrix = zeros( size(this.lineage.lineageTable, 1), ...
%     (this.configuration.ProjectConfiguration.trackerEndFrame + 1) - ...
%     this.configuration.ProjectConfiguration.trackerStartFrame);
% 
% % Set all frames between first bud apperance and division to 1
% for i = 1:size(this.lineage.lineageTable, 1)
%     frameMatrix(i, this.lineage.lineageTable.daughter_start_frame(i): ...
%         this.lineage.lineageTable.division_frame(i)) = 1;
% end
% 
allFrames = this.configuration.ProjectConfiguration.trackerStartFrame: ...
    this.configuration.ProjectConfiguration.trackerEndFrame;
% 
% frames2Process = allFrames(sum(frameMatrix, 1) > 0);

frames = this.data.getFieldArray('cell_frame');
isBud = this.data.getFieldArray('track_has_bud');
framesWithBud = unique(frames(isBud == 1))';
frames2Process = allFrames(ismember(allFrames, framesWithBud));

pBar = this.utils.progressBar(numel(frames2Process), ...
    'IsParallel', true, ...
    'WorkerDirectory', pwd, ...
    'Title', 'MERGING: Buds prior division' ...
    );

% Process the frames in parallel while fixing the labels sequentially
pBar.setup([], [], []);
parfor f = 1:numel(frames2Process)
%for f = 1:numel(frames2Process)
    
    fn = frames2Process(f);
    % Read mask
    mask = this.io.readSegmentationMask(fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, this.configuration. ...
        ProjectConfiguration.trackerResultMaskFileArray{fn}));
    maskOut = mask;
    
    tracks = this.data.getFieldArrayForFrame('track_index', fn);
    parent = this.data.getFieldArrayForFrame('track_parent', fn);
    isBudFrame = this.data.getFieldArrayForFrame('track_has_bud', fn);
    
    % Get the table indicies to merge
    % idx2Process = find(frameMatrix(:, fn))';
    daughterArray = tracks(parent > 0)';
    %     for tr = idx2Process
    for d = daughterArray
        
        motherTrack = parent(tracks == d);
        if isBudFrame(tracks == motherTrack) == 1
            daughterTrack = d;
            %         motherTrack = this.lineage.lineageTable.trackindex_mother(tr);
            %         daughterTrack = this.lineage.lineageTable.trackindex_daughter(tr);

            maskTemp = mask;
            maskTemp(~ismember(mask, [motherTrack, daughterTrack])) = 0;
            maskOut(ismember(mask, [motherTrack, daughterTrack])) = 0;
            maskBin = imbinarize(maskTemp);
            maskDil = imdilate(maskBin, SE);
            maskDil = imclose(maskDil, se);
            maskErode = imerode(maskDil, SE);

            % Get indices
            [r, c] = find(maskErode == 1);
            ind = sub2ind(size(mask),r,c);

            % Add merged mass label back
            maskOut(ind) = motherTrack;
        end
        
    end
    
    % Write updated mask to mat
    this.io.writeToMAT(maskOut, this.configuration.ProjectConfiguration. ...
        segmentationResultDir, ...
        strjoin({'final', this.configuration.ProjectConfiguration. ...
        trackerResultMaskFileArray{fn}}, '_'))
    
    % Write updated mask to tiff for resegmentation
    [~,n,~] = fileparts(this.configuration.ProjectConfiguration. ...
        trackerResultMaskFileArray{fn});
    this.io.writeToTIF(uint16(maskOut), fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, ...
        sprintf('final_%s.tif', n)))

    
    TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
end
end