%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledImg ] = generatePoleAgeControlImageFrame( this, fontSize, ...
    frameNumber)
% GENERATEPOLEAGECONTROLIMAGEFRAME Generates a control
% image with labeled cell poles for all cell tracks found on an image  
% frame with their pole age overlayed onto a (Brightfield) image. 
%
% Args:
%    this (:obj:`object`):           :class:`ImageProcessing` instance
%    fontSize (:obj:`float`):        Font size of track indicies labels.
%    frameNumber (:obj:`int`):       Image frame number.
%
% Returns
% -------
%    labeledImg :obj:`double` NxM                               
%                                    Raw image with all tracks present
%                                    labeled with their cell pole ages.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

fontName = 'Arial';
fontEmphasis = 0;

x = this.data.getFieldArrayForFrame('cell_center_x', frameNumber);
y = this.data.getFieldArrayForFrame('cell_center_y', frameNumber);
xPole1 = this.data.getFieldArrayForFrame('cell_pole1_x', frameNumber);
yPole1 = this.data.getFieldArrayForFrame('cell_pole1_y', frameNumber);
xPole2 = this.data.getFieldArrayForFrame('cell_pole2_x', frameNumber);
yPole2 = this.data.getFieldArrayForFrame('cell_pole2_y', frameNumber);
ageCellPole1 = this.data.getFieldArrayForFrame('cell_pole1_age', frameNumber);
ageCellPole2 = this.data.getFieldArrayForFrame('cell_pole2_age', frameNumber);
indexLabel = this.data.getFieldArrayForFrame('track_index', frameNumber);

baseImg = this.io.readImageToGrayScale(fullfile(...
    this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.imageFingerprintFileArray{frameNumber}), ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray);

% Normalize
baseImg = uint8(255 * this.normalizeImage(baseImg));
% maxVal = this.configuration.ProjectConfiguration.imageMaxSampleValue;
% baseImg = baseImg(:,:) * maxVal;

if size(baseImg, 3) ~= 3
    r = baseImg;
    g = baseImg;
    b = baseImg;
else
    r = baseImg(:, :, 1);
    g = baseImg(:, :, 2);
    b = baseImg(:, :, 3);
end

rgbImg = cat(3, r, g, b);

if ~iscell(indexLabel)
    indexLabelArray = cell(numel(indexLabel),1);
    pole1LabelArray = cell(numel(ageCellPole1),1);
    pole2LabelArray = cell(numel(ageCellPole2),1);
    for iElement = 1:numel(indexLabel)
        indexLabelArray{iElement} = num2str(indexLabel(iElement)); 
        pole1LabelArray{iElement} = num2str(ageCellPole1(iElement));
        pole2LabelArray{iElement} = num2str(ageCellPole2(iElement));
        
    end
else
    if ~ischar(indexLabel{1})
        indexLabelArray = cellfun(@(x) num2str(x), indexLabel, ...
            'UniformOutput', false);
    else
        indexLabelArray = indexLabel;
    end
end

% Convert Matlab array to java BufferedImage for speed reasons
fontS = java.awt.Font(fontName, fontEmphasis, 9);
font = java.awt.Font(fontName, fontEmphasis, fontSize);
image = this.utils.rgb2java(rgbImg);

for iElement = 1:numel(indexLabelArray)
    graphics = image.getGraphics();
    graphics.setFont(fontS);
    graphics.setColor(java.awt.Color.green)
    graphics.drawString(indexLabelArray{iElement}, round(x(iElement) - ...
        fontSize/3), round(y(iElement)+ fontSize/3));
    graphics.dispose();
    
    graphics = image.getGraphics();
    graphics.setFont(font);
    graphics.setColor(java.awt.Color.green)
    graphics.drawString(pole1LabelArray{iElement}, round(xPole1(iElement) - ...
        fontSize/2), round(yPole1(iElement)+ fontSize/2));
    graphics.dispose();
    
    graphics = image.getGraphics();
    graphics.setFont(font);
    graphics.setColor(java.awt.Color.red)
    graphics.drawString(pole2LabelArray{iElement}, round(xPole2(iElement) - ...
        fontSize/2), round(yPole2(iElement)+ fontSize/2));
    graphics.dispose();
        
end

% Convert Java BufferedImage back to Matlab array
nrows = image.getHeight;
ncols = image.getWidth;
channel = image.getRaster.getNumBands;

pixelsData = reshape(typecast(image.getData.getDataStorage, 'uint8'), ...
    channel, ncols, nrows);

labeledImg = cat(channel, ...
    transpose(reshape(pixelsData(1, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(2, :, :), ncols, nrows)), ...
    transpose(reshape(pixelsData(3, :, :), ncols, nrows)));

labeledImg = squeeze(labeledImg);
end
