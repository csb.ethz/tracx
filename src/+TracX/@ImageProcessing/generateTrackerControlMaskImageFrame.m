%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function [ labeledMask ] = generateTrackerControlMaskImageFrame( this, ...
    frameNumber, varargin )
% GENERATETRACKERCONTROLMASKIMAGEFRAME Generates a tracker control
% image where the labeled tracker mask are overlayed onto the raw image.
% Optionaly the label color can be set using a cmap.
%
% Args:
%    this (:obj:`object`):                    :class:`ImageProcessing` instance
%    frameNumber(:obj:`int`):                 Image frame number
%    varargin (:obj:`str varchar`):
%
%        * **useTrackIndices** (:obj:`bool`): If track indices should be used. Defaults to true. Othewise cell indices are used.
%        * **customCMap** (:obj:`array` kx3): Use custom colormat to color the cell segmentation masks. Otherwise auto generates one.
%
% Returns
% -------
%    labeledMask :obj:`array` NxM
%                                                       Image matrix with
%                                                       tracked cell
%                                                       segmentation masks.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

parser = inputParser;
parser.addParameter('useTrackIndices', true, @(x) islogical(x));
parser.addParameter('customCMap', '', @(x) size(x, 2) == 3 && size(x, 1) >= 1);

parser.parse(varargin{:});
useTrackIndices    = parser.Results.useTrackIndices;
customCMap    = parser.Results.customCMap;


if useTrackIndices
    track_index = this.data.getFieldArrayForFrame('track_index', frameNumber);
    track_contained_in_track = this.data.getFieldArrayForFrame(...
        'track_contained_in_track', frameNumber);
    
    track_index(~isnan(track_contained_in_track)) = 0;
    maxUniqueTrackIndicies = max(track_index);
    if isempty(customCMap)
        cmap = lines(maxUniqueTrackIndicies);
    else
        cmap = zeros(maxUniqueTrackIndicies, 3);
        cmap(track_index, :) = customCMap;
    end
else
    cell_index = this.data.getFieldArrayForFrame('cell_index', frameNumber);
    if isempty(customCMap)
        cmap = lines(max(cell_index));
    else
        cmap = ones(max(cell_index),3);
        cmap(cell_index,:) = customCMap;
    end
end

for i = 1:this.configuration.ProjectConfiguration.imageDepth
    img(:,:,i) = this.io.readImageToGrayScale(fullfile(...
        this.configuration.ProjectConfiguration.imageDir, ...
        this.configuration.ProjectConfiguration. ...
        imageFingerprintFileArray{frameNumber}), ...
        this.configuration.ProjectConfiguration.imageCropCoordinateArray, i);
end
img = this.normalizeImage(img);

% Load tracking masks
if useTrackIndices
    mask = this.io.readSegmentationMask(fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, this.configuration. ...
        ProjectConfiguration.trackerResultMaskFileArray{frameNumber}));
else
    mask = this.io.readSegmentationMask(fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, ...
        this.configuration.ProjectConfiguration. ...
        segmentationResultMaskFileArray{frameNumber}));
end


% Handle coordinate reversion case
coords = this.configuration.ProjectConfiguration. ...
    imageCropCoordinatePriorReversionArray;
if ~isempty(coords) && all(size(img) ~= size(mask))
    tempMask = zeros(size(img));
    tempMask(coords(2):(coords(2)+coords(4)), coords(1): ...
        (coords(1)+coords(3))) = mask;
    mask = tempMask;
end

for i = 1:this.configuration.ProjectConfiguration.imageDepth
    mask2(:,:,i,:) = label2rgb(mask(:,:,i), cmap, 'k');
end

% Create RGB from greyscale image
try
    rgb = img(:,:, :, [1 1 1]);
    
    for i = 1:this.configuration.ProjectConfiguration.imageDepth
        labeledMask(:,:,i,:) = imlincomb(1, im2uint8(rgb(:,:,i,:)), this.configuration. ...
            ParameterConfiguration.maskOverlayAlpha, mask2(:,:,i,:), 'uint8');
    end
    labeledMask = squeeze(labeledMask);
    
catch exception
    this.utils.printToConsole(sprintf(['ERROR: Image and segmentation mask', ...
        'dimensions are not compatible. Did you forget to set the "Crop"?', ...
        ' (image: %dx%d, mask: %dx%d)'], size(rgb, 1), size(rgb, 1), ...
        size(mask2, 1), size(mask2, 2)))
    this.utils.printToConsole(exception.message)
    labeledMask = [];
end
end
