%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ imageMatrixNorm ] = normalizeImage(~, imageMatrix )
% NORMALIZEIMAGE Normalizes image between 0 and 1.
%
% Input:
%    ignoredArg (:obj:`object`):             :class:`ImageProcessing` instance
%    imageMatrix (:obj:`array` NxM):         Image matrix to be normalized.
%
% Returns
% -------
%    imageMatrixNorm: :obj:`array` NxM
%                                             Normalized image matrix.                             
%   
% :Authors:
%    Sotiris Dimopoulos - initial implementation in CellX
% :Authors:
%    Andreas P. Cuny - adapation for TracX 

minimumBitValue = min(imageMatrix(:));
maximumBitValue = max(imageMatrix(:));
variableBitRange = maximumBitValue - minimumBitValue;
imageMatrixNorm = ( imageMatrix - minimumBitValue ) / variableBitRange;

end

