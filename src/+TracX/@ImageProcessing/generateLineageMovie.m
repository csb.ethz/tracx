%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = generateLineageMovie(this, rootTrack, imageFrequency, ...
    movieName, varargin)
% GENERATELINEAGEMOVIE Generates a video displaying the cell
% lineage, labeled image and quantified signals over time.
%
% Args:
%    this (:obj:`object`):                    :class:`ImageProcessing` instance   
%    rootTrack (:obj:`int`):                  Track index of the lineage root
%                                             to be visualized.
%    imageFrequency (:obj:`float`):           Image frequency in hours
%    movieName(:obj:`str`):                   Name of the movie
%    varargin (:obj:`str varchar`):
%
%        * **figurePosition** (:obj:`float`): Figure position and size (x,y, width, height) array for the movie generation. Default is: [10 10 1350 600]
%        * **videoQuality** (:obj:`int` 0-100): Quality (0:100) of exported movie; Defaults to 100.
%        * **videoFrameRate** (:obj:`int` ):  Frame per second in exported movie; Defaults to 5.
%        * **videoProfile** (:obj:`str` ):    Compression format that should be used for the exported move; Defautls to 'MPEG-4'; help VideoWriter in cmd for more help.
%        * **scaleBarLength** (:obj:`float`): Length of scalebar in terms of unit.
%        * **pxToUnit** (:obj:`float`):       Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **unit** (:obj:`str`):             Unit of scalebar. Defaults to micron. Opthons are pixel, px, pixels.
%
% Returns
% -------
%    void (:obj:`-`)                                
%                                             Writes movie to disk directly.                                     
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Parsing the inputs
defaultVideoQuality = 100;
defaultVideoFrameRate = 5;
if ispc == true
	defaultVideoProfile = 'MPEG-4';
else
	defaultVideoProfile = 'Motion JPEG AVI';
end
validVideoProfiles = {'Archival', 'Motion JPEG AVI', 'Motion JPEG 2000', ...
    'MPEG-4', 'Uncompressed AVI', 'Indexed AVI', 'Grayscale AVI'};
defaultScaleBarLength = 10; % in given units; typical microns.
defaultPxToUnit = 6.5/40; % pixelsize (width or height in microns) divided 
%                           by magnification of the objective.

p = inputParser;
p.addRequired('this', @isobject);
p.addParameter('videoQuality', defaultVideoQuality,  @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}));
p.addParameter('videoFrameRate', defaultVideoFrameRate,  @(x)validateattributes(x, ...
    {'numeric'}, {'scalar'}));
p.addParameter('videoProfile', defaultVideoProfile, @(x) any(validatestring(...
    x, validVideoProfiles)));
p.addParameter('scaleBarLength', defaultScaleBarLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('pxToUnit', defaultPxToUnit,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
parse(p, this, varargin{:})


cellDivisionType = this.configuration.ProjectConfiguration.cellDivisionType;
if cellDivisionType == 0
    presentFrames = this.configuration.ProjectConfiguration.trackerStartFrame:1: ...
    this.configuration.ProjectConfiguration.trackerEndFrame;
else
    
    trackUUID = this.data.SegmentationData.uuid(this.data.SegmentationData. ...
        track_index == rootTrack);
    presentFrames = this.data.SegmentationData.cell_frame(this.data. ...
        SegmentationData.track_index == rootTrack);
    presentFrames = presentFrames(ismember(trackUUID, this.data. ...
        QuantificationData.uuid)).*imageFrequency;
    
    if isempty(this.lineage.lineageTable)
        this.lineage.setLineageTable();
    end
end

v = VideoWriter(movieName, p.Results.videoProfile);
v.Quality = p.Results.videoQuality;
v.FrameRate = p.Results.videoFrameRate;
scaleBarLength = p.Results.scaleBarLength;
pxToUnit = p.Results.pxToUnit;
open(v);

frameArray = cell(numel(presentFrames), 1);

pBar = this.utils.progressBar(numel(presentFrames), ...
    'IsParallel', true, ...
    'WorkerDirectory', pwd, ...
    'Title', 'IMAGEPROCESSING: Generating lineage movie' ...
    );

% Process the frames in parallel
pBar.setup([], [], []);
parfor frame = 1:numel(presentFrames)
    fh = this.generateLineageFrame(rootTrack, imageFrequency, frame, ...
        'scaleBarLength', scaleBarLength, 'pxToUnit', pxToUnit);
    drawnow;
    frameArray{frame} = getframe(fh);
    close(fh);
    TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
end
pBar.release();

for f = 1:numel(presentFrames)
    writeVideo(v, frameArray{f});
end
end