%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  [ mask ] = generateMaskForMatrixCorners( img )
% GENERATEMASKFORMATRIXCORNERS Creates a mask of the same size
% of a 2D (image) matrix(NxM) with all corners are labeled by
% a square.
%
% Args:
%    img (:obj:`array`, NxM):    Image / mask matrix
%
%
% Returns
% -------
%    mask :obj:`array` NxM                               
%                                Matrix with edges labeled
%                                starting top left with 1.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

startRow = 1;
startCol = 1;
endRow = size(img, 1);
endCol = size(img, 2);
mask = zeros( endRow, endCol );
squareSideLength = sqrt((endRow * endCol / 25) / 4);

% Label each corner with unique number.
mask(startRow : squareSideLength, startCol : squareSideLength) = 1;
mask(startRow : squareSideLength, endCol - squareSideLength : endCol) = 2;
mask(endRow - squareSideLength : endRow, endCol - squareSideLength : endCol) = 3;
mask(endRow - squareSideLength : endRow, startCol : squareSideLength) = 4;

end