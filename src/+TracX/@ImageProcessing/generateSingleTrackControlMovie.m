%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function generateSingleTrackControlMovie(this, fromImageFrame, ...
    toImageFrame, trackOfInterest, movieFileName, varargin)
% GENERATESINGLETRACKCONTROLMOVIE Exports a movie for one
% selected cell in the indicated image frame range labeled with
% tracking accuray information encoded in the edges of the
% movie frames. The movies are intended to help the user to
% inspect the tracking but also segmentation accuracy for a
% selected cell of interst.
%
% Args:
%    this (:obj:`object`):         :class:`ImageProcessing` instance 
%    fromImageFrame (:obj:`int`):  Frame (image) number to start movie from.
%    toImageFrame (:obj:`int`):    Frame (image) number to end movie.
%    trackOfInterest (:obj:`int`): Track of interest for which a
%                                  control movie should be generated.
%    movieFileName (:obj:`str`):   Name for the resulting movie to save.
%    varargin (:obj:`str varchar`):
%
%        * **videoQuality** (:obj:`int` 0-100): Quality (0:100) of exported movie; Defaults to 100.
%        * **videoFrameRate** (:obj:`int` ):  Frame per second in exported movie; Defaults to 5.
%        * **videoProfile** (:obj:`str` ):    Compression format that should be used for the exported move; Defautls to 'MPEG-4'; help VideoWriter in cmd for more help.
%        * **scaleBarLength** (:obj:`float`): Length of scalebar in terms of unit. Defaults to 10.
%        * **pxToUnit** (:obj:`float`):       Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40. 
%        * **unit** (:obj:`str`):             Unit of scalebar. Defaults to micron. Options are pixel, px, pixels.
%        * **imageFrequency** (:obj:`int`):   Images aquired per hour.
%
% Returns
% -------
%    void (:obj:`-`)                               
%                                  Movie will be written directly to
%                                  disk.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultVideoQuality = 100;
defaultVideoFrameRate = 5;
if ispc == true
	defaultVideoProfile = 'MPEG-4';
else
	defaultVideoProfile = 'Motion JPEG AVI';
end
validVideoProfiles = {'Archival', 'Motion JPEG AVI', 'Motion JPEG 2000', ...
    'MPEG-4', 'Uncompressed AVI', 'Indexed AVI', 'Grayscale AVI'};
validMovieFrameRate = @(x) isnumeric(x) && x>=1;
validMovieQuality = @(x) isnumeric(x) && x>=1 && x<=100;
defaultScaleBarLength = 10; % in given units; typical microns.
defaultPxToUnit = 6.5/40; % pixelsize (width or height in microns) divided 
%                           by magnification of the objective.
defaultUnit = 'micron';
defaultImageFrequency = 1; % in hours

p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('fromImageFrame', @(x) isnumeric(x) && x>=1)
p.addRequired('toImageFrame', @(x) isnumeric(x) && x>=1)
p.addRequired('trackOfInterest', @(x) any(ismember(x, unique(this.data.getFieldArray('track_index')))))
p.addRequired('movieFileName', @(x) ischar(x))
p.addParameter('videoQuality', defaultVideoQuality, validMovieQuality);
p.addParameter('videoFrameRate', defaultVideoFrameRate, validMovieFrameRate);
p.addParameter('videoProfile', defaultVideoProfile, @(x) any(validatestring(...
    x, validVideoProfiles)));
p.addParameter('scaleBarLength', defaultScaleBarLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('pxToUnit', defaultPxToUnit,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('unit', defaultUnit,  @(x)ischar(validatestring(x, {'micron', 'pixel', ...
    'px', 'pixels'})));
p.addParameter('imageFrequency', defaultImageFrequency,  @isnumeric);
parse(p, this, fromImageFrame, toImageFrame, trackOfInterest, movieFileName, ...
    varargin{:})

this.utils.printToConsole(sprintf('SAVE: Start writing movie for track %d. %s',...
    trackOfInterest, fullfile(movieFileName)))
warning off
% Create VideoWriter handle
hVid = VideoWriter(movieFileName, p.Results.videoProfile);
set(hVid,'Quality', p.Results.videoQuality);
set(hVid,'FrameRate', p.Results.videoFrameRate);

pBar = this.utils.progressBar(toImageFrame-fromImageFrame+1, 'Title', ...
    'writing movie frames:');

% Open the object for writing
open(hVid);
for iFrame = fromImageFrame:toImageFrame
    % Label cells with cell index and track integrity
    % information
    im = this.generateTrackControlImageFramePadded(trackOfInterest, iFrame);
    
    im = im2uint8(im);
    im = this.addTimestampScalebar(im, p.Results.imageFrequency, iFrame, ...
        'scaleBarLength', p.Results.scaleBarLength, 'pxToUnit', ...
        p.Results.pxToUnit, 'unit', p.Results.unit);
    
    [img, map] = rgb2ind(im, 2^(this.configuration.ProjectConfiguration.imageBitDepth));
    frame = im2frame(uint8(img), map);
    
    % Write current video frame
    writeVideo(hVid, frame);
    pBar.step([], [], []);
end
close(hVid);
pBar.release();
this.utils.printToConsole('SAVE: Done writing movie.')
end