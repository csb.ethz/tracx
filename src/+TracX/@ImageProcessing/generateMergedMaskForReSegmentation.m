%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function generateMergedMaskForReSegmentation(this, tracks2Merge)
% GENERATEMERGEDMASKFORRESEGMENTATION Generates masks for resegmentation and
% quantification by deleteing all unaffected cells from the mask and
% copying them into the segmentationResultDir/tmp subfolder.
%
% Args:
%    this (:obj:`object`):                  :class:`ImageProcessing` instance   
%    tracks2Merge (:obj:`array` kx1):       Image matrix         
%
% Returns
% -------
%    void (:obj:`-`)                              
%                                             Writes new massks to disk
%                                             directly into a tmp folder. 
%                                             :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.segmentationResultDir`
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if isempty(gcp('nocreate'))
    this.utils.printToConsole('INFO: Starting parpool')
    parpool();
    this.utils.printToConsole('INFO: Starting parpool done.')
end

% Check if temp folder exists otherwise create
if ~exist(fullfile(this.configuration.ProjectConfiguration. ...
        segmentationResultDir, 'tmp'), 'file')
    mkdir(fullfile(this.configuration.ProjectConfiguration. ...
        segmentationResultDir, 'tmp'))
end


allFrames = this.configuration.ProjectConfiguration.trackerStartFrame: ...
    this.configuration.ProjectConfiguration.trackerEndFrame-1;

pBar = this.utils.progressBar(numel(allFrames), ...
    'IsParallel', true, ...
    'WorkerDirectory', pwd, ...
    'Title', 'COPY: Merged masks to tmp' ...
    );

% % Process the frames in parallel while fixing the labels sequentially
pBar.setup([], [], []);
parfor fn = allFrames
    
    idsToKeep = tracks2Merge(tracks2Merge(:, 1) == fn, 2); % mothers to keep
    
    % Get final masks
    mask = this.io.readSegmentationMask(fullfile(this.configuration.ProjectConfiguration. ...
        segmentationResultDir, strjoin({'final', this.configuration. ...
        ProjectConfiguration.trackerResultMaskFileArray{fn}}, '_')));
    oldLabelArray = unique(mask);
    newLabelArray = oldLabelArray;
    newLabelArray(~ismember(oldLabelArray, idsToKeep)) = 0;
    
    % Relabel masks
    maskRelabeled = this.relabelMaskIndices(mask, ...
        newLabelArray, oldLabelArray);
    
    % Save as tif in /temp folder for resegmentation
    [~,n,~] = fileparts(this.configuration.ProjectConfiguration. ...
        trackerResultMaskFileArray{fn});
    this.io.writeToTIF(uint16(maskRelabeled), fullfile(this.configuration. ...
        ProjectConfiguration.segmentationResultDir, 'tmp', ...
        sprintf('final_%s.tif', n)))
    
    TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
end
end