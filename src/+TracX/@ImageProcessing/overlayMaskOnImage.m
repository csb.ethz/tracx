%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledImg ] = overlayMaskOnImage(this, image, mask, varargin )
% OVERLAYMASKONIMAGE Overlays a mask file on an image where as the
% transparency of the overlay is set trough the optional maskOverlayAlpha 
% argument.
%
% Args:
%    this (:obj:`object`):           :class:`ImageProcessing` instance
%    image (:obj:`array` NxM):       Image matrix
%    mask (:obj:`array`)::           Mask to be overlayed with Img in
%                                    the specified channel with the in 
%                                    alpha specified transparency.
%    varargin (:obj:`str varchar`):
%
%        * **maskOverlayAlpha** (:obj:`float` 0-1): Sets the transparency of the mask in the range[0,1].
%        * **imageBrightness** (:obj:`float` 0-1):  Sets the brightness of the image in the range [0,1].
%        * **customColorMap** (:obj:`array`, Kx3):  Applies a custom colormap of the size [K,3], with K = max(track_index) number
%
% Returns
% -------
%    labeledImg :obj:`double` NxM                               
%                                     Image with mask overlayed onto image
%                                     with user defined transparency.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultMaskOverlayAlpha = this.configuration.ParameterConfiguration. ...
    maskOverlayAlpha;

defaultImageBrightness = 1;
defaultCustomColorMap = [];
validMaskOverlayAlpha = {'>=', 0, '<=', 1};
validImageTransparency = {'>=', 0, '<=', 1};

p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('image', @isnumeric)
p.addRequired('mask', @(x) isnumeric(x) || islogical(x))
p.addParameter('maskOverlayAlpha', defaultMaskOverlayAlpha, @(x)validateattributes(x, ...
    {'double'}, validMaskOverlayAlpha, 'Rankings'));
p.addParameter('imageBrightness', defaultImageBrightness, @(x)validateattributes(x, ...
    {'double'}, validImageTransparency, 'Rankings'));
p.addParameter('customColorMap', defaultCustomColorMap, @isnumeric)
parse(p, this, image, mask, varargin{:})

if all(islogical(mask))
    mask = double(mask);
end
idx = unique(mask);
idx(1) = [];
% Handle the case when the mask is empty. I.e object out of FoV.
if isempty(idx)
    idx = 1;
end
cmap = zeros(max(idx), 3);

% Note: Colormap need to have min size of max track_index plotted.
if ~isempty(p.Results.customColorMap)
    colors = this.utils.getColorMap(max(idx), 'customColorMap', ...
        p.Results.customColorMap);
else
    colors = this.utils.getColorMap(max(idx));
end
cmap(idx, :) = colors(idx, :);
mask2 = label2rgb(mask, cmap, 'k'); 

% Construct RGB channels
if size(image, 3) == 1
    % Create RGB from greyscale image
    rgb = image(:,:,[1 1 1]);
elseif size(image, 3) == 3
    rgb = image;
else
    ME = MException('overlayMaskOnIMage:wrongBitDepth', ...
        'Input has wrong size. Dimension has to be 1 for greyscale or 3 for RGB image', size(image, 3));
    throw(ME)
end

r = rgb(:,:, 1).* p.Results.imageBrightness;
g = rgb(:,:, 2).* p.Results.imageBrightness;
b = rgb(:,:, 3).* p.Results.imageBrightness;

rm = double(mask2(:,:, 1));
gm = double(mask2(:,:, 2));
bm = double(mask2(:,:, 3));

r(rm>0) = rm(rm>0)./255 * p.Results.maskOverlayAlpha;
g(gm>0) = gm(gm>0)./255 * p.Results.maskOverlayAlpha;
b(bm>0) = bm(bm>0)./255 * p.Results.maskOverlayAlpha;

labeledImg = rgb;
labeledImg(:,:, 1) = r;
labeledImg(:,:, 2) = g;
labeledImg(:,:, 3) = b;

% if( size(varargin,2)== 0 )
%     % Default varargin values
%     alpha = 0.3;
%     channel = 'G';
%     bitDepth = 16;
% elseif( size(varargin,2) == 1 )
%     alpha = varargin{1};
%     % Default values
%     channel = 'G';
%     bitDepth = 16;
% elseif( size(varargin,2) == 2 )
%     alpha = varargin{1};
%     channel = varargin{2};
%     % Default value
%     bitDepth = 16;
% elseif( size(varargin,2) == 3 )
%     alpha = varargin{1};
%     channel = varargin{2};
%     bitDepth = varargin{2};
% else(size(varargin,2) > 3 );
%     error 'Wrong # of arguments. Usage: Imag, Mask, Alpha [0,1], Channel [R,G,B], BitDepth [8, 16, 32, 64]';
% end
% 
% % Construct RGB channels
% if size(image, 3) == 1
%     r = image;
%     g = image;
%     b = image;
% elseif size(image, 3) == 3
%     r = image(:,:,1);
%     g = image(:,:,2);
%     b = image(:,:,3);
% else
%     ME = MException('overlayMaskOnIMage:wrongBitDepth', ...
%         'Input has wrong size. Dimension has to be 1 for greyscale or 3 for RGB image', size(image, 3));
%     throw(ME)
% end
% 
% % Overlay Mask with image with defined Alpha
% foreGround = zeros(size(mask));
% foreGround(:) = 1; % BitDepth;
% overlay = foreGround(logical(mask)) .* alpha + g(logical(mask)) .* (1-alpha);
% if channel == 'G'
%     g(logical(mask)) = (overlay);
%     % g = uint16(g);
% elseif channel == 'R'
%     r(logical(mask)) = (overlay);
%     % r = uint16(r);
% elseif channel == 'B'
%     b(logical(mask)) = (overlay);
%     % b = uint16(b);
% else
%     this.printToConsole('Error! Other channels than R,G,B are not implemented yet')
% end
% overlayedImg = (zeros(size(image,1), size(image,2), 3));
% overlayedImg(:,:,1) = r;
% overlayedImg(:,:,2) = g;
% overlayedImg(:,:,3) = b;

%> @param varargin: 
%> @param alpha:           [Float] Mask transparency with range [0,1]
%>                                 default is 0.3.
%> @param channel:        [String] RGB Channel used for overlay.
%>                                 default is 'G'.
%> @param bitDepth:          [Int] Bit depth of Img. [8, 16, 32, 64]
%>                                 default is 16.

end