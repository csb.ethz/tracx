%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ labeledMask ] = generateMarkerControlImageFrame(this, frameNumber)
% GENERATEMARKERCONTROLIMAGEFRAME Generates a marker tracking control image
% to visualize i.e budneck marker tracking.
%
% Args:
%    this (:obj:`object`):                             :class:`ImageProcessing` instance   
%    frameNumber (:obj:`int`):                                Image frame number
%
% Returns
% -------
%    labeledMask :obj:`array` NxM                               
%                                                       Image matrix
%                                                       including
%                                                       segmentation mask
%                                                       of cell cycle
%                                                       marker.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

mask = zeros(this.configuration.ProjectConfiguration.imageHeight, ...
    this.configuration.ProjectConfiguration.imageWidth);
labels = this.lineage.lineageData.obj_pixelIdxList{frameNumber};
tracks = this.lineage.lineageData.getFieldArrayForFrame('obj_track_index', ...
    frameNumber);
for l = 1:numel(labels)
    mask(labels{l}) = tracks(l);
end

if ~isempty(labels)
    img = this.generateAnnotatedControlImageFrame(frameNumber, ...
        'fieldName', 'obj_track_index', 'isMarker', true, 'fontSize', 9);
    
    mask2 = label2rgb(mask, this.utils.getColorMap(numel(unique(tracks)), ...
        'indices', unique(mask(mask>0))), 'k');
    
    labeledMask = imlincomb(1, im2uint8(img), this.configuration. ...
        ParameterConfiguration.maskOverlayAlpha, mask2, 'uint8');
    
% Get segmetation bounding box -  instead of full image size save only
% relevant segmentation area.
%     offset = this.configuration.ParameterConfiguration.meanCellDiameter;
%     xc = this.data.getFieldArrayForFrame('cell_center_x', this.configuration. ...
%         ProjectConfiguration.trackerEndFrame);
%     yc = this.data.getFieldArrayForFrame('cell_center_y', this.configuration. ...
%         ProjectConfiguration.trackerEndFrame);
%     [bx, by, bw, bh] = this.utils.getSegAreaBoundingBox(xc, yc, offset);
%     cropArray = [bx, by, bw, bh];
    if ~isempty(this.configuration.ProjectConfiguration. ...
            imageCropCoordinateArray)
        imageCropCoordinateArray = this.configuration.ProjectConfiguration. ...
            imageCropCoordinateArray;
    else
        % imageCropCoordinateArray = cropArray;
        imageCropCoordinateArray = [];
    end
    if ~isempty(imageCropCoordinateArray)
        labeledMask = TracX.Utils.imcropC(labeledMask, imageCropCoordinateArray);
    end
else
    labeledMask = mask(:,:, [1 1 1]);
end