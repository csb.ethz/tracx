%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef ImageProcessing
    % ImageProcessing Class for processing image matrices. It
    % implements methods to generate various control images.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
        
        configuration % :class:`+TracX.@TrackerConfiguration` instance with the tracking parameters and project metadata

        data % :class:`+TracX.@TrackerData` instance stores all segmentation, quantification and lineage data.

        io  % :class:`+TracX.@IO` instance implementing read/write methods

        utils % :class:`+TracX.@Utils` instance implementing utility methods

        lineage % :class:`+TracX.@Lineage` instance implementing lineage methods
        
    end
    
    methods
        
        % Constructor
        function obj = ImageProcessing(io, configuration, data, utils, ...
                lineage)
            % Constructor of an :class:`ImageProcessing` object
            %
            % Args:
            %    io (:obj:`object`):           :class:`+TracX.@IO` instance.
            %    configuration(:obj:`object`): :class:`+TracX.@TrackerConfiguration` instance.
            %    data(:obj:`object`):          :class:`+TracX.@TrackerData` instance.
            %    utils(:obj:`object`):         :class:`+TracX.@Utils` instance.
            %    lineage(:obj:`object`):       :class:`+TracX.@Lineage` instance.
            %
            % Returns
            % -------
            %    obj (:obj:`object`)                             
            %                             :class:`ImageProcessing` instance
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            obj.io = io;
            obj.configuration = configuration;
            obj.data = data;
            obj.utils = utils;
            obj.lineage = lineage;
        end
        
        % COMPUTECLAHE Computes the  Contrast Limited Adaptive
        % Histogram Equalization (CLAHE) to enhance the local contrast of
        % an image.
        [ claheImage ] = computeCLAHE( image, claheBlockSize, claheClipLimit )
        
        % NORMALIZEIMAGE Normalizes image between 0 and 1.
        [ imageMatrixNorm ] = normalizeImage(this, imageMatrix )
        
        % OVERLAYMASKONIMAGE Overlays a mask file on an image where as the
        % transparency of the overlay is set trough the alpha argument.
        [ overlayedImg ] = overlayMaskOnImage(this, image, mask, varargin )
                
        % RELABELMASKINDICES Relabels indices on a 2D mask [NxM].
        [ maskRelabeled ] = relabelMaskIndices(~, mask, newLabelArray,...
            oldLabelArray )
        
        % GENERATEMASKFORMATRIXCORNERS Creates a mask of the same
        % size of a 2D (image) matrix(NxM) with all corners are labeled by
        % a square.
        [ mask ] = generateMaskForMatrixCorners( img )
        
        % GENERATEMULTITRACKCONTROLMOVIE Exports a movie for all
        % detected and tracked cells in the indicated image frame range.
        % The movie helps the user to visially inspect the tracking
        % accuracy.
        generateMultiTrackControlMovie(this, fromImageFrame, toImageFrame, ...
            movieFileName, varargin)
        
        % GENERATESINGLETRACKCONTROLMOVIE Exports a movie for one
        % selected cell in the indicated image frame range labeled with
        % tracking accuray information encoded in the edges of the
        % movie frames. The movies are intended to help the user to
        % inspect the tracking but also segmentation accuracy for a
        % selected cell of interst.
        generateSingleTrackControlMovie(this, fromImageFrame, ...
            toImageFrame, trackOfInterest, movieFileName, varargin)
        
        % GENERATETIMESERIESIMAGEMATRIX Generates and saves an image  
        % series for each track and image frame of the experimet. Each track 
        % is labeled with its segmentation mask and centered.
        generateTimeSeriesImageMatrix(this, windowAroundCellCenter)
        
        % GENERATETRACKCONTROLIMAGEFRAME Displays a control
        % image where all cell tracks found on an image frame are labeled
        % with their track indicies overlayed onto a (Brightfield) image.
        [ labeledImg ] = generateTrackControlImageFrame( this, fontSize, ...
            frameNumber, varargin)
        
        % GENERATEANNOTATEDCONTROLIMAGEFRAME Generates a control
        % image where all data found on an image frame for a given property 
        % are overlayed onto a (Brightfield) image. The default property is
        % 'track_index' but it can be any of 'SegmentationData'.
        [ labeledImg ] = generateAnnotatedControlImageFrame( this, ...
            frameNumber, varargin)
        
        % GENERATETRACKCONTROLIMAGEFRAMEPADDED Generates a control
        % image where all cell tracks found on that frame are labeled
        % with the segmentation mask as overlay to the raw
        % (Brightfield) image. In the corners of the generated control
        % image frame information about tracking accuracy is encoded as
        % mentioned in the note below.
        [ labeledImg ] = generateTrackControlImageFramePadded( this, ...
            trackOfInterest, frameNumber)
        
        % GENERATEFINGERPRINTCONTROLIMAGEFRAME Generates a control
        % image where all cell tracks found on an image frame are labeled
        % with their track indicies overlayed onto a (Brightfield) image.
        % Additionally the fingerprint windows are displayed.
        [ labeledImg ] = generateFingerprintControlImageFrame( this, fontSize, ...
            rectHalfSideLength, frameNumber)
        
        % GENERATEPLOTAGECONTROLIMAGEFRAME Generates a control
        % image with labeled cell poles for all cell tracks found on an image
        % frame with their pole age overlayed onto a (Brightfield) image.
        [ labeledImg ] = generatePoleAgeControlImageFrame( this, fontSize, ...
            frameNumber)
        
        % GENERATETRACKERCONTROLMASKIMAGEFRAME Generates a tracker
        % control image where the labeled tracker mask are overlayed onto
        % the raw image. Optionaly the label color can be set using a cmap.
        [ labeledMask ] = generateTrackerControlMaskImageFrame( this, ...
            frameNumber, varargin )
        
        % GENERATE4DIMAGESTACK Loads 2D images with single precision in
        % either greyscale or RGB into 4D stack 4th dimension here is time.
        % To save memory, images are converted with im2uint8 from >8bit
        % to 8bit images.
        [ imgStack ] = generate4DImageStack(this, imageFilePath, ...
            segmentationResultDir, imageFileNameArray, ...
            trackerResultMaskArray, coiFrameArray, cellCenterXArray, ...
            cellCenterYArray, halfWindowLength, imageFrameArray, ...
            trackIndexArray, trackOI, trackContainedInTrack )
        
        % LABELIMAGETRACKMASKS Generates a color image where all cell
        % tracks found on that frame are labeled with the segmentation mask. The
        % mask are overlayed onto the raw (Brightfield) image. The transparency
        % of the mask (maskOverlayAlpha) and the brightness of the image
        % (imageTransparency) can be optionally controlled. Also a the labels can
        % be colored with custom colors from a color map of the appropriate size.
        [ rgbWithMask ] = labelImageTrackMasks( Tracker, labels, frameNumber, ...
            alpha, varargin )
        
        % GETTEXTONJAVAIMAGE Outputs image with string label at position for a 
        % Java BufferedImage.
        [ image ] = getTextOnJavaImage(this, image, string, position, ...
            fontColor, fontName, fontSize, fontEmphasis)
        
        % GENERATELINEAGEFRAME Generates a figure for a specific image
        % frame for the lineage movie generation. The figure consists of a labeled
        % image, the lineage tree and a plotting area to plot quantified signals
        % over time.
        [ fh ] = generateLineageFrame(this, rootTrack, imageFrequency, ...
            frame, varargin)
        
        % GENERATELINEAGEMOVIE Generates a video displaying the cell
        % lineage, labeled image and quantified signals over time.
        [ ] = generateLineageMovie(this, rootTrack, imageFrequency, ...
            movieName, varargin)
        
        % MERGEBUDPRIORDIVISION Merges the bud mask with the mother mask
        % prior cell division if exists (i.e. with CellX segmentation
        % it does).
        [ ] = mergeBudPriorDivision(this)
        
        % GENERATE4DIMAGESTACKMARKER Generates 2D images with single precision in
        % either greyscale or RGB into 4D stack 4th dimension here is time. To
        % save memory, images are converted with im2uint8 from >8bit to 8bit
        % images for cell cycle markers.
        [ imgStack ] = generate4DImageStackMarker(this, imageFilePath, ...
            segmentationResultDir, imageFileNameArray, trackerResultMaskArray, ...
            cycleMarker, halfWindowLength, trackOI, fontSize, isPlotMarkerSignal)
        
        % GENERATEMARKERTIMESERIESIMAGEMATRIX Generates and saves an image
        % series for each track and image frame of the experimet. Each track is
        % labeled with its segmentation mask and centered.
        generateMarkerTimeSeriesImage(this, varargin)
        
        % GENERATEMARKERCONTROLIMAGEFRAME Generates a marker tracking 
        % control image to visualize i.e budneck marker tracking
        [ labeledMask ] = generateMarkerControlImageFrame(this, frameNumber)

        % GENERATEMERGEDMASKFORRESEGMENTATION Generates masks for re
        % segmentation and quantification by deleteing all unaffected cells 
        % from the mask and copying them into the segmentationResultDir/tmp 
        % subfolder.
        generateMergedMaskForReSegmentation(this, tracks2Merge)
        
        % GENERATEMERGEDMASK Merges the bud mask with the mother mask
        % prior cell division if exists (i.e. with CellX segmentation 
        % it does).
        [ ] = generateMergedMask(this)
        
        % ADDTIMESTAMPSCALEBAR Adds a timestamp (frame number or time in units time
        % as well as a scale bar to an image.
        [ img ] = addTimestampScalebar(img, imageFrequency, frame, varargin)
        
        % GETPROPERTY Returns a colormap based on a specific property.
        [cMap, cMapFull, cLim] = getFieldCMap(this, property)
        
    end
    
end
