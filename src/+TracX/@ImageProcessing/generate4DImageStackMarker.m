%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ imgStack ] = generate4DImageStackMarker(this, imageFilePath, ...
    segmentationResultDir, imageFileNameArray,  trackerResultMaskArray, ...
    cycleMarker, halfWindowLength, trackOI, fontSize, isPlotMarkerSignal)
% GENERATE4DIMAGESTACKMARKER Generates 2D images with single precision in
% either greyscale or RGB into 4D stack 4th dimension here is time. To
% save memory, images are converted with im2uint8 from >8bit to 8bit
% images for cell cycle markers.
%   
% Args:
%    this (:obj:`object`):                      :class:`ImageProcessing` instance
%    imageFilePath (:obj:`str`):                Path to image location 
%    segmentationResultDir (:obj:`str`):        Path to segmentation result
%                                               files.
%    imageFileNameArray (:obj:`str` 1xN):       Cell array with image file names
%    trackerResultMaskArray (:obj:`str` 1xN):   Cell array with tracker mask
%                                               file names.
%    cycleMarker (:obj:`array` 1x2):            Cell array with marker 
%                                               file identifiers i.e
%                                               {'mKate', 'mKO'}
%    halfWindowLength (:obj:`int`):             Half window length of a 
%                                               square around cell center 
%                                               to cut out from the total 
%                                               image matrix.
%    trackOI:                                   selected Track (current 
%                                               track of interest)
%    fontSize (:obj:`int` 1x1):                 Fontsize for labels.
%    isPlotMarkerSignal (:obj:`bool`):          Flag if cell cycle marker
%                                               signal shold be plotted as 
%                                               additional layer. Default to 
%                                               false.
%
% Returns
% -------
%    ImgStack: :obj:`array` WxHx3xN
%                                                4D Stack containing the 
%                                                image of cell of interest
%                                                windowed around its cell 
%                                                center.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

maxVal = this.configuration.ProjectConfiguration.imageMaxSampleValue;

markerOI = this.data.TmpLinAss.markerTrack(this.data.TmpLinAss.motherCell ...
    == trackOI(1));

trackArray = this.data.getFieldArray('track_index');
parentArray = this.data.getFieldArray('track_parent');
children = unique(trackArray(parentArray == trackOI(1)));

phase = this.data.getFieldArrayForTrackIndex('track_cell_cycle_phase', trackOI(1));
motherFrames = this.data.getFieldArrayForTrackIndex('cell_frame', trackOI(1));
motherX = this.data.getFieldArrayForTrackIndex('cell_center_x', trackOI(1));
motherY = this.data.getFieldArrayForTrackIndex('cell_center_y', trackOI(1));
mL = min(motherFrames):this.configuration.ProjectConfiguration.trackerEndFrame;
allFrames =  unique([motherFrames; mL']);
dd = nan(numel(allFrames), 3);
dd(ismember(allFrames, motherFrames), 1) = phase;

allX = nan(numel(allFrames), 3);
allX(ismember(allFrames, motherFrames), 1) = motherX;
allX(:, 1) = interp1(motherFrames, motherX, allFrames, 'linear', 'extrap');

allY = nan(numel(allFrames), 3);
allY(ismember(allFrames, motherFrames), 1) = motherY;
allY(:, 1) = interp1(motherFrames, motherY, allFrames, 'linear', 'extrap');

for c = children'
    
    daughterFrames = this.data.getFieldArrayForTrackIndex('cell_frame', c);
    
    dd(ismember(allFrames, daughterFrames), 2) = repmat(c, 1, ...
        numel(daughterFrames));
    % dd(ismember(allFrames, daughterFrames), 3) = markerOI;
    
    daughterX = this.data.getFieldArrayForTrackIndex('cell_center_x', c);
    daughterY = this.data.getFieldArrayForTrackIndex('cell_center_y', c);
    % Interpolate missing values for crop and windowing coordinates
    allX(ismember(allFrames, daughterFrames), 2) = daughterX;
    res = interp1(daughterFrames, daughterX, allFrames, 'linear');
    idx = allFrames(ismember(min(daughterFrames), allFrames)): ...
        allFrames(ismember(max(daughterFrames), allFrames));
    allX(idx, 2) = res(idx);
    allY(ismember(allFrames, daughterFrames), 2) = daughterY;
    res = interp1(daughterFrames, daughterY, allFrames, 'linear');
    allY(idx, 2) = res(idx);
    
    % allX(~ismember(allFrames, daughterFrames), 2) = nan;
    % allY(~ismember(allFrames, daughterFrames), 2) = nan;
end
allX(:, 3) = round(nanmean(allX,2));
allY(:, 3) = round(nanmean(allY,2));

cropCoordinateArray = [allX(:,3) - halfWindowLength, allY(:,3) ...
    - halfWindowLength, repmat(2 * halfWindowLength,...
    numel(allX(:,3)), 1), repmat(2 * halfWindowLength, ...
    numel(allX(:,3)), 1)];
cropCoordinateArrayRaw = cropCoordinateArray;
cropCoordinateArray(:, [1,2]) = 1;

% Preallocated 4D matrix with the size of the images and number of images
% (row x col x chl x #frames)
imgStack = uint8(zeros(2 * halfWindowLength, 2 * halfWindowLength, 3,...
    numel(allFrames)));

for k = 1:numel(allFrames)
    %     cellCenterXArray = this.data.getFieldArrayForTrackIndexForFrame('cell_center_x', k, trackOI);
    %     cellCenterYArray = this.data.getFieldArrayForTrackIndexForFrame('cell_center_y', k, trackOI);
    objTrackArray = this.lineage.lineageData.getFieldArrayForFrame('obj_track_index', k);
    % objFrame = this.lineage.lineageData.getFieldArrayForFrame('obj_frame', k);
    objCenterX = this.lineage.lineageData.getFieldArrayForFrame('obj_center_x', k);
    objCenterY = this.lineage.lineageData.getFieldArrayForFrame('obj_center_y', k);
    objCenterX = objCenterX(ismember(objTrackArray, markerOI));
    objCenterY = objCenterY(ismember(objTrackArray, markerOI));
    
    r_s = 1;
    r_e = halfWindowLength*2;
    c_s = 1;
    c_e = halfWindowLength*2;
    
    % Read image and mask
    img = this.io.readImageToGrayScale(fullfile(imageFilePath,...
        imageFileNameArray{k}), cropCoordinateArrayRaw(k, :));
    img = this.normalizeImage(img);
    
    if isPlotMarkerSignal
        markerCoords = cropCoordinateArrayRaw(k, :);
        markerCoords([3,4]) = markerCoords([3,4]) - 1;
        fname = strsplit(imageFileNameArray{k}, '_');
        fname{1} = cycleMarker{1};
        imgNuc = this.io.readImageToGrayScale(fullfile(imageFilePath,...
            strjoin(fname, '_')), markerCoords);
        imgNuc = this.normalizeImage(imgNuc);
        fname{1} = cycleMarker{2};
        imgBud = this.io.readImageToGrayScale(fullfile(imageFilePath,...
            strjoin(fname, '_')), markerCoords);
        imgBud = this.normalizeImage(imgBud);
    end
       
    mask = this.io.readSegmentationMask(fullfile(segmentationResultDir, ...
        trackerResultMaskArray{k}));
    maskIdx = unique(mask);
    maskIdx(maskIdx == 0) = [];
    % Crop mask here!
    
    if exist('changem', 'file') == 2
        maskR = changem(mask, zeros(numel(maskIdx(~ismember(maskIdx, ...
            [trackOI, dd(k,2)]))),1), ...
            maskIdx(~ismember(maskIdx, [trackOI, dd(k,2)])));
    else
        [lia, locb] = ismember(mask, maskIdx(~ismember(maskIdx, [trackOI, dd(k,2)])));
        nF = zeros(numel(maskIdx(~ismember(maskIdx, ...
            [trackOI, dd(k,2)]))),1);
        mask(lia) = nF(locb(lia));
        maskR = mask;
    end
    
    if isPlotMarkerSignal
        pMask = TracX.Utils.padImageCrop(maskR, cropCoordinateArrayRaw(k, :));
        [N, edges] = histcounts(imgNuc);
        thresh = find(N>20, 1, 'last');
        cutoff = edges(thresh) * 0.95;
        imgNuc(imgNuc < cutoff) = 0;
        %Binarize signal
        imgNuc(imgNuc>0) = 1;
        imgNuc(pMask == 0) = 0;
        [N, edges] = histcounts(imgBud);
        thresh = find(N>20, 1, 'last');
        cutoff = edges(thresh) * 0.95;
        imgBud(imgBud < cutoff) = 0;
        %Binarize signal
        imgBud(imgBud>0) = 1;
        imgBud(pMask == 0) = 0;
    end
       
    % Use perimeter only
    maskR = bwperim(maskR, 8);
    
    % Mask overlay the image
    if numel(unique(maskR)) == 1
        imgOc = TracX.Utils.padImageCrop(img, cropCoordinateArray(k, :));
        imgOut = cat(3, imgOc, imgOc, imgOc);
    else
        imgOut = this.overlayMaskOnImage(TracX.Utils.padImageCrop(img, cropCoordinateArray(k, :)), ...
            TracX.Utils.padImageCrop(maskR, cropCoordinateArrayRaw(k, :)), 'maskOverlayAlpha',...
            1, 'customColorMap', [1, 0, 0]);
    end
    
    if isPlotMarkerSignal
        imgNuc = TracX.Utils.padImageCrop(imgNuc, cropCoordinateArray(k, :));
        imgBud = TracX.Utils.padImageCrop(imgBud, cropCoordinateArray(k, :));
        
        mask1 = label2rgb(imgNuc, [1,0,1], 'k');
        mask2 = label2rgb(imgBud, [0,1,1], 'k');
    end

    % Generate marker mask
    markerMask = zeros(size(mask));
    if k <= size(this.lineage.lineageData.obj_pixelIdxList, 2)
        if ~isempty(this.lineage.lineageData.obj_pixelIdxList{k}) && any(ismember(objTrackArray, markerOI)) == 1
            markerMask(vertcat(this.lineage.lineageData.obj_pixelIdxList{k}{ismember(objTrackArray, markerOI)})) = 1;
            markerMask = bwperim(markerMask, 8);
            imgOut = this.overlayMaskOnImage( imgOut, ...
                TracX.Utils.padImageCrop(markerMask, cropCoordinateArrayRaw(k, :)), 'maskOverlayAlpha',...
                1, 'customColorMap', [1, 1, 0]);
            indexLabelArray = strsplit(num2str([trackOI, dd(k,2), vertcat(objTrackArray(ismember(objTrackArray, markerOI)))']));
        else
            indexLabelArray = strsplit(num2str([trackOI, dd(k,2)]));
        end
    else
        indexLabelArray = strsplit(num2str([trackOI, dd(k,2)]));
    end
    
    % Label the image
    fontSizeCells = fontSize - 2;
    fontSizeArray = [repmat(fontSizeCells, 1, 2), ...
        repmat(fontSize, 1, numel(objTrackArray(ismember(objTrackArray, ...
        markerOI))))];
    cmapC = [1, 0, 0];
    cmapM = [0, 1, 1];
    cmap = [repmat(cmapC,  2, 1); ...
        repmat(cmapM, numel(objTrackArray(ismember(objTrackArray, ...
        markerOI))), 1)];
    
    cellCenterXArray = [allX(k,1:2), objCenterX' + 4];
    cellCenterYArray = [allY(k,1:2), objCenterY' - 9];
    
    % Convert Matlab array to java BufferedImage for speed reasons
    image = this.utils.rgb2java(im2uint8(imgOut));
    for iElement = 1:numel(indexLabelArray)
        if all(cmap(iElement, :) == [1, 0, 0])
            col = 'red';
        elseif all(cmap(iElement, :) == [0, 1, 1])
            col = 'cyan';
        else
            col = 'green';
        end
        if  all(~isnan([round(cellCenterXArray(iElement) ...
                - cropCoordinateArrayRaw(k, 1)), round(cellCenterYArray(iElement) - ...
                cropCoordinateArrayRaw(k, 2))]))
            image = this.getTextOnJavaImage(image, ...
                indexLabelArray{iElement}, [round(cellCenterXArray(iElement) ...
                - cropCoordinateArrayRaw(k, 1)), round(cellCenterYArray(iElement) - ...
                cropCoordinateArrayRaw(k, 2))], col, 'Arial', fontSizeArray(iElement), 0);
        end
    end
   % Convert Java BufferedImage back to Matlab array
   nrows = image.getHeight;
   ncols = image.getWidth;
   channel = image.getRaster.getNumBands;
   pixelsData = reshape(typecast(image.getData.getDataStorage, 'uint8'), ...
       channel, ncols, nrows);
   labeledImg = cat(channel, ...
       transpose(reshape(pixelsData(1, :, :), ncols, nrows)), ...
       transpose(reshape(pixelsData(2, :, :), ncols, nrows)), ...
       transpose(reshape(pixelsData(3, :, :), ncols, nrows)));
    
    if isPlotMarkerSignal
        labeledMask = imlincomb(1, im2uint8(labeledImg), 1, mask1, 'uint8');
        labeledMask = imlincomb(1, labeledMask, 1, mask2, 'uint8');
        imgStack(c_s:c_e, r_s:r_e, :, k) = labeledMask;
    else
        imgStack(c_s:c_e, r_s:r_e, :, k) = im2uint8(labeledImg);
    end
    
    % Add frame number label to image
    imgStack(:,:,:,k) = insertText(imgStack(:, :, :, k),[2 * halfWindowLength, ...
        2 * halfWindowLength], ...
        allFrames(k),'AnchorPoint', 'RightBottom', 'FontSize', 9);
    
    if dd(k, 1) == 0
        p1 = 0;
        p2 = 0;
    elseif dd(k, 1) == 1
        p1 = 1;
        p2 = 0;
        tmpImg = imgStack(:, :, :, k);
        tmpImg(1:10, 1:10, [1, 3]) = maxVal;
        imgStack(:,:,:,k) = tmpImg;
    elseif dd(k, 1) == 3
        p1 = 1;
        p2 = 1;
        tmpImg = imgStack(:, :, :, k);
        tmpImg(1:10, 1:10, [1, 3]) = maxVal;
        imgStack(:,:,:,k) = tmpImg;
        tmpImg = imgStack(:, :, :, k);
        tmpImg(1:10, end-10:end, [2, 3]) = maxVal;
        imgStack(:,:,:,k) = tmpImg;
    else
        p1 = 0;
        p2 = 1;
        tmpImg = imgStack(:, :, :, k);
        tmpImg(1:10, end-10:end, [2, 3]) = maxVal;
        imgStack(:,:,:,k) = tmpImg;
    end
    imgStack(:,:,:,k) = insertText(imgStack(:, :, :, k),[0, ...
        2 * halfWindowLength], sprintf('G1:%d S,G2/M:%d', p1, p2), ...
        'AnchorPoint', 'LeftBottom', 'FontSize', 9); % 'BoxColor', [0.7, 0.7, 0.7]
    
    % @todo: insert top left G1 phase and top right G2 phase == 1; 0
    % otherwise
end
end
