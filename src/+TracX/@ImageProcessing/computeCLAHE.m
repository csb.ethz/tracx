%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ claheImage ] = computeCLAHE( image, claheBlockSize, claheClipLimit )
% COMPUTECLAHE Computes the  Contrast Limited Adaptive Histogram Equalization
% (CLAHE) to enhance the local contrast of an image.
%
% Args:
%    image (:obj:`str`):                  Path to image file.
%    claheBlockSize (:obj:`int` [M, N]):  The size of the local region 
%                                         around a pixel  for which the histogram  
%                                         is equalized. This size should be larger 
%                                         than the size  of features to be preserved.
%    claheClipLimit (:obj:`float` [0-1]): Real scalar that specifies a
%                                         contrast enhancement limit. Higher numbers
%                                         result in more contrast.
%
% Returns
% -------
%    claheImage :obj:`ddouble` NxM                               
%                                         Image matrix with enhanced contrast.
%
% :Authors:
%    Sotiris Dimopoulos - initial implementation in CellX
% :Authors:
%    Andreas P. Cuny - adaptation for TracX

dim = size(image);

tiles = round(dim/claheBlockSize);
tiles = max(tiles, [2 2]);

claheImage = adapthisteq(  image, ...
    'NumTiles', tiles, ...
    'Range', 'original', ...
    'ClipLimit', claheClipLimit);
end