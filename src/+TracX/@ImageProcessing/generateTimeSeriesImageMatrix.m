%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  generateTimeSeriesImageMatrix(this, halfWindowLength)
% GENERATETIMESERIESIMAGEMATRIX Generates and saves an image  
% series for each track and image frame of the experimet. Each track is 
% labeled with its segmentation mask and centered.
%
% Args:
%    this (:obj:`object`):                :class:`ImageProcessing` instance 
%    halfWindowLength (:obj:`int`, 1x1):  Half window side length around
%                                         cell center to be used for each
%                                         image frame.
%
% Returns
% -------
%    void (:obj:`-`)                               
%                                         Resulting images are written to 
%                                         disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

warning('off', 'images:initSize:adjustingMag')
warning('off', 'MATLAB:images:montage:displayRangeForRGB')
warning('off', 'images:label2rgb:zerocolorSameAsRegionColor')
imageDir = this.configuration.ProjectConfiguration.imageDir;
imageFingerprintFileArray = this.configuration.ProjectConfiguration.imageFingerprintFileArray;
segmentationResultDir = this.configuration.ProjectConfiguration.segmentationResultDir;
trackerResultMaskFileArray = this.configuration.ProjectConfiguration.trackerResultMaskFileArray;
trackIndexArray = this.data.getFieldArray('track_index');
cellFrameArray = this.data.getFieldArray('cell_frame');
trackContainedInTrackArray = this.data.getFieldArray('track_contained_in_track');
trackArray = unique(trackIndexArray);
NTrack = numel(trackArray);
% pBar = this.utils.progressBar(numel(trackArray), ...
%     'Title', 'IMAGE GENERATION: Track time series control' ...
%     );
pBar = this.utils.progressBar(NTrack, ...
    'IsParallel', true, ...
    'WorkerDirectory', pwd, ...
    'Title', 'IMAGEPROCESSING: Generating track time series control' ...
    );

tic
pBar.setup([], [], []);
parfor i = 1:NTrack

    iTrack = trackArray(i);
    if ~exist(sprintf('%s/timeseries_track_%d.png', ...
            segmentationResultDir , trackArray(iTrack)), 'file')
        
        centerX = this.data.getFieldArrayForTrackIndex('cell_center_x', iTrack); 
        centerY = this.data.getFieldArrayForTrackIndex('cell_center_y', iTrack); 
        trackFrameArray =  this.data.getFieldArrayForTrackIndex('cell_frame', iTrack); 
        uniqueFrameArray = unique(cellFrameArray);
        framesNotInTrackIdx = ~ismember(uniqueFrameArray, trackFrameArray);
        missingFrames = zeros(halfWindowLength * 2, ...
            halfWindowLength * 2,...
            3, numel(uniqueFrameArray(framesNotInTrackIdx)));
        
        % Load all image frames in reduced bit depth into a stack
        imgStackC = this.generate4DImageStack(imageDir, ...
            segmentationResultDir,...
            imageFingerprintFileArray, trackerResultMaskFileArray,...
            trackFrameArray, centerX, centerY,  ...
            halfWindowLength, cellFrameArray, trackIndexArray, ...
            trackArray(iTrack), trackContainedInTrackArray);

        
        imgStackCfin = zeros(size(imgStackC,1), size(imgStackC,2), ...
            size(imgStackC,3), size(imgStackC,4) + size(missingFrames,4));
        imgStackCfin(:, :, :, uniqueFrameArray(~framesNotInTrackIdx)) = imgStackC;
        imgStackCfin(:, :, :, uniqueFrameArray(framesNotInTrackIdx)) = missingFrames;
        
        out = montage(imgStackCfin, 'DisplayRange', [0 0.7], 'Size', [1 ,...
            size(imgStackCfin,4)]);

        this.io.writeToPNG(out.CData, sprintf('%s/tracktimeseries_track_%d.png', ...
            segmentationResultDir , trackArray(iTrack)))
        
        %this.utils.printTimeLeft('Track timeseris generation', i, NTrack)
        %pBar.step([], [], []);
        TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
    else
        %this.utils.printTimeLeft('Track timeseris generation', i, NTrack)
        %pBar.step([], [], []);
        TracX.ExternalDependencies.matlabProgressBar.updateParallel([], pwd);
    end
    
end
pBar.release();
%this.utils.printToConsole(sprintf('Track timeseries image generation took %0.5g seconds.', toc))

end