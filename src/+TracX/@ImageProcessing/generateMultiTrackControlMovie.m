%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function generateMultiTrackControlMovie(this, fromImageFrame, toImageFrame, ...
    movieFileName, varargin)
% GENERATEMULTITRACKCONTROLMOVIE Exports a movie for all
% detected and tracked cells in the indicated image frame range.
% The movie helps the user to visually inspect the tracking
% accuracy.
%
% Args:
%    this (:obj:`object`):         :class:`ImageProcessing` instance
%    fromImageFrame (:obj:`int`):  Frame (image) number to start movie from.
%    toImageFrame (:obj:`int`):    Frame (image) number to end movie.
%    movieFileName (:obj:`str`):   Name for the resulting movie to save.
%    varargin (:obj:`str varchar`):
%
%        * **fieldname** (:obj:`str`):        Field name for which to create color map. Defaults to 'track_index'; Any :class:`SegmentationData` property
%        * **videoQuality** (:obj:`int` 0-100): Quality (0:100) of exported movie; Defaults to 100.
%        * **videoFrameRate** (:obj:`int` ):  Frame per second in exported movie; Defaults to 5.
%        * **videoProfile** (:obj:`str` ):    Compression format that should be used for the exported move; Defautls to 'MPEG-4'; help VideoWriter in cmd for more help.
%        * **scaleBarLength** (:obj:`float`): Length of scalebar in terms of unit. Defaults to 10.
%        * **pxToUnit** (:obj:`float`):       Pixel to unit conversion factor. I.e. for a 40X objective with 6.5 micron pixel size would result to 6.5/40.
%        * **unit** (:obj:`str`):             Unit of scalebar. Defaults to micron. Options are pixel, px, pixels.
%        * **imageFrequency** (:obj:`int`):   Images aquired per hour.
%        * **roi** (:obj:`array`, 1x4):       Region of interest to export defined as crop array [x, y, h, w]. Default is empty.
%        * **tracksOfInterest** (:obj:`array`,): Array of track indices to be labeled.
%
% Returns
% -------
%    void (:obj:`-`)
%                                       A movie with all cells in a track
%                                      labeled is written to disk directly.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultFieldName = 'track_index';
defaultVideoQuality = 100;
defaultVideoFrameRate = 5;
if ispc == true
	defaultVideoProfile = 'MPEG-4';
else
	defaultVideoProfile = 'Motion JPEG AVI';
end
validVideoProfiles = {'Archival', 'Motion JPEG AVI', 'Motion JPEG 2000', ...
    'MPEG-4', 'Uncompressed AVI', 'Indexed AVI', 'Grayscale AVI'};
validMovieFrameRate = @(x) isnumeric(x) && x>=1;
validMovieQuality = @(x) isnumeric(x) && x>=1 && x<=100;
validFieldNames = [fieldnames(this.data.SegmentationData);fieldnames(this.lineage.lineageData)];
defaultScaleBarLength = 10; % in given units; typical microns.
defaultPxToUnit = 6.5/40; % pixelsize (width or height in microns) divided
%                           by magnification of the objective.
defaultUnit = 'micron';
defaultImageFrequency = 1; % in hours
defaultROI = [];
defaultTracksOfInterest = [];

p = inputParser;
p.addRequired('this', @isobject);
p.addRequired('fromImageFrame', @(x) isnumeric(x) && x>=1)
p.addRequired('toImageFrame', @(x) isnumeric(x) && x>=1)
p.addRequired('movieFileName', @(x) ischar(x))
p.addParameter('fieldName', defaultFieldName, @(x) any(validatestring(x, validFieldNames)))
p.addParameter('videoQuality', defaultVideoQuality, validMovieQuality);
p.addParameter('videoFrameRate', defaultVideoFrameRate, validMovieFrameRate);
p.addParameter('videoProfile', defaultVideoProfile, @(x) any(validatestring(...
    x, validVideoProfiles)));
p.addParameter('scaleBarLength', defaultScaleBarLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('pxToUnit', defaultPxToUnit,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('unit', defaultUnit,  @(x)ischar(validatestring(x, {'micron', 'pixel', ...
    'px', 'pixels'})));
p.addParameter('imageFrequency', defaultImageFrequency,  @isnumeric);
p.addParameter('roi', defaultROI,  @(x) all(size(x) == [1,4]) || isempty(x));
p.addParameter('tracksOfInterest', defaultTracksOfInterest,  @(x) all(ismember(x, unique(this.data.getFieldArray('track_index')))));

parse(p, this, fromImageFrame, toImageFrame, movieFileName, varargin{:})

sideLength = this.configuration.ParameterConfiguration.cellThumbSideLength;

[mPath, mName, ~ ] = fileparts(p.Results.movieFileName);
if isempty(mPath)
    mPath = pwd;
end

this.utils.printToConsole(sprintf('SAVE: Start writing movie. %s', fullfile(mPath, mName)))
[cMap, ~, ~] = this.getFieldCMap(p.Results.fieldName);
cellFrameArray = this.data.getFieldArray('cell_frame');

% Create VideoWriter handle
hVid = VideoWriter(movieFileName, p.Results.videoProfile);
set(hVid,'Quality', p.Results.videoQuality);
set(hVid,'FrameRate', p.Results.videoFrameRate);

pBar = this.utils.progressBar(toImageFrame-fromImageFrame+1, 'Title', ...
    'writing movie frames:');
warning('off', 'images:label2rgb:zerocolorSameAsRegionColor')
% Open the object for writing
open(hVid);
for iFrame = fromImageFrame:toImageFrame
    % Label cells with cell index and track integrity
    %     % information
    %     im = this.labelImageTrackMasks(this.data.getFieldArrayForFrame(...
    %         'track_index', iFrame), iFrame, 'maskOverlayAlpha', 0.6);
    %
    
    currcmap = cMap(cellFrameArray == iFrame,:);
    if isempty(p.Results.tracksOfInterest)
        im = this.generateTrackerControlMaskImageFrame(iFrame, ...
            'customCMap', currcmap);
    else
        im = this.labelImageTrackMasks(p.Results.tracksOfInterest, iFrame, ...
            'maskOverlayAlpha', 1, 'customColorMap', currcmap);
        cellCenterX = this.data.getFieldArrayForTrackIndexForFrame('cell_center_x', ...
            iFrame, p.Results.tracksOfInterest);
        cellCenterY = this.data.getFieldArrayForTrackIndexForFrame('cell_center_y', ...
            iFrame, p.Results.tracksOfInterest);
        
        avgX = round(nanmean(cellCenterX));
        avgY = round(nanmean(cellCenterY));
        
        if all(isnan(avgX))
            stFrames = unique(this.data.SegmentationData.track_start_frame( ...
                ismember(this.data.SegmentationData.track_index,...
                p.Results.tracksOfInterest)));
            
            cellCenterXR = nan(numel(stFrames), 1);
            cellCenterYR = nan(numel(stFrames), 1);
            for k = 1:numel(stFrames)
                cellCenterXR(k) = this.data.getFieldArrayForTrackIndexForFrame(...
                    'cell_center_x', stFrames(k), p.Results.tracksOfInterest(k));
                cellCenterYR(k) = this.data.getFieldArrayForTrackIndexForFrame(...
                    'cell_center_y', stFrames(k), p.Results.tracksOfInterest(k));
            end
            
            avgX = round(nanmean(cellCenterXR));
            avgY = round(nanmean(cellCenterYR));
        end
        
        imageCropCoordinateArray = [avgX - sideLength, avgY - ...
            sideLength, 2 * sideLength,2 * sideLength];
        
        im = TracX.Utils.padImageCrop(im, imageCropCoordinateArray);
    end
    if ~isempty(p.Results.roi)
        im = imcrop(im, p.Results.roi);
    end
    
    im = this.addTimestampScalebar(im, p.Results.imageFrequency, iFrame, ...
        'scaleBarLength', p.Results.scaleBarLength, 'pxToUnit', ...
        p.Results.pxToUnit, 'unit', p.Results.unit);
    
    [img, map] = rgb2ind(im, 2^8);
    frame = im2frame(uint8(img), map);
    
    % Write current video frame
    writeVideo(hVid, frame);
    pBar.step([], [], []);
end
close(hVid);
pBar.release();
this.utils.printToConsole('SAVE: Done writing movie.')
warning('off', 'images:label2rgb:zerocolorSameAsRegionColor')
end