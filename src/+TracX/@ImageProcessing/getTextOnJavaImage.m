%>  ***********************************************************************
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function [ image ] = getTextOnJavaImage(~, image, string, position, ...
    fontColor, fontName, fontSize, fontEmphasis)
% GETTEXTONJAVAIMAGE Outputs image with string label at position for a 
% Java BufferedImage.
%
% Args:
%    ignoredArg (:obj:`object`):      :class:`ImageProcessing` instance
%    image (:obj:`array` NxM):        Image matrix where the text string
%                                     should be inserted. Note: Needs to be
%                                     a BufferedImage (java).
%    string (:obj:`str`):             Text that should be added to the image
%    position (:obj:`array` 2x1):     Position with x and y coordinates to
%                                     place the string onto the image
%    fontColor (:obj:`str`):          Font color. Defaults to green (green, red, blue)
%    fontName (:obj:`str`):           Font name. Defaults to Arial.
%    fontSize (:obj:`int`):           Font size. Defaults to 14.
%    fontEmphasis (:obj:`int`):       Font emphasis. Defaults to regular.
%
%
% Returns
% -------
%    image :obj:`array` NxM
%                                     Image 
%
% .. note::
%
%    This array (:obj:`image`) can be used in a loop if multiple text
%    strings need to be placed on an image.
%
% Example:
%
% >>> image = im2java2d(rand([100,100,3]));
% >>> % Label the image with the text only in the green channel
% >>> image = getTextOnJavaImage(image, 'Hello World!', [9,50], 'blue', 'Arial', 16, 1);
% >>> % Convert Java BufferedImage back to Matlab array
% >>> nrows = image.getHeight;
% >>> ncols = image.getWidth;
% >>> channel = image.getRaster.getNumBands;
% >>> pixelsData = reshape(typecast(image.getData.getDataStorage, 'uint8'), ...
% >>>              channel, ncols, nrows);
% >>> labeledImg = cat(channel, ...
% >>>     transpose(reshape(pixelsData(1, :, :), ncols, nrows)), ...
% >>>     transpose(reshape(pixelsData(2, :, :), ncols, nrows)), ...
% >>>     transpose(reshape(pixelsData(3, :, :), ncols, nrows)));
% >>> figure; imshow(labeledImg)
%
% :Authors:
%    Andreas P. Cuny - initial implementation

p = inputParser;
p.KeepUnmatched=true;

validString = @(x) ischar(x);
validFontColor = @(x) ischar(x) && 1;
validFontSize = @(x) isnumeric(x) && x>4;
validFontName = @(x) ischar(x);
validFontEmphasis = @(x) isnumeric(x) && x>=0 && x<=2;

addRequired(p, 'image', @(x) isjava(x));
addRequired(p, 'string', validString);
addRequired(p, 'position', @(x) all(isnumeric(x)));
addRequired(p, 'fontColor', validFontColor);
addRequired(p, 'fontName', validFontName);
addRequired(p, 'fontSize', validFontSize);
addRequired(p, 'fontEmphasis', validFontEmphasis);

parse(p, image, string, position, fontColor, fontName, ...
    fontSize, fontEmphasis)

switch p.Results.fontColor
    case 'red' 
        col = java.awt.Color.red;
    case 'blue'
        col = java.awt.Color.blue;
    case 'cyan'
        col = java.awt.Color.cyan;
    otherwise
        col = java.awt.Color.green;
end

font = java.awt.Font(p.Results.fontName, p.Results.fontEmphasis, ...
    p.Results.fontSize);

graphics = image.getGraphics();
graphics.setFont(font);
graphics.setColor(col)
graphics.drawString(string, round(p.Results.position(1) - ...
    p.Results.fontSize/3), round(p.Results.position(2) + p.Results.fontSize/3));
graphics.dispose();

end