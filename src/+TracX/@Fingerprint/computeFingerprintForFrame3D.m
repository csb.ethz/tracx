%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [retFq] = computeFingerprintForFrame3D(this, fromFrame, toFrame,  ...
    fingerprintHalfWindowSideLength, fingerprintResizeFactor, ...
    fingerprintMaxConsideredFrequencies, imageDir, imageFingerprintFileArray, ...
    imageCropCoordinateArray, cellFrameArray, cellCenterXArray, cellCenterYArray, ...
    cellCenterZArray, pixelPerZplane, active3DFingerprint)
% COMPUTEFINGERPRINTFORFRAME3D Computes a unique cell quality control feature
% based on an out-of-focus Brightfield 3D image using the DCT transform.
% Here the fingerprints are computed for each frame using DCT and
% the frequencies containing the structural
% information of the cell resulting in a unique string. It is possible
% to only calculate the fingerprint for a number of specified image frames.
%
% Args:
%    this (:obj:`object`):                              :class:`Fingerprint` instance 
%    fromFrame (:obj:`int`):                            Starting frame for
%                                                       calculation
%    toFrame (:obj:`int`):                              End frame for calculation
%    fingerprintHalfWindowSideLength  (:obj:`int`):     +TracX.ParameterConfiguration.fingerprintHalfWindowSideLength
%    fingerprintResizeFactor  (:obj:`int`):             +TracX.ParameterConfiguration.fingerprintResizeFactor
%    fingerprintMaxConsideredFrequencies (:obj:`int`):  +TracX.ParameterConfiguration.fingerprintMaxConsideredFrequencies
%    imageDir (:obj:`str`):                             +TracX.ProjectConfiguration.imageDir
%    imageFingerprintFileArray (:obj:`cell array` 1xK): +TracX.ProjectConfiguration.imageFingerprintFileArray
%    imageCropCoordinateArray (:obj:`double` 1x4):      +TracX.ProjectConfiguration.imageCropCoordinateArray
%    cellFrameArray (:obj:`int`):                       Array with all image
%                                                       frame numbers.
%    cellCenterXArray (:obj:`int`):                     Array with centroid x
%                                                       coordinates of segmented objects.
%    cellCenterYArray (:obj:`int`):                     Array with centroid
%                                                       y coordinates of segmented objects.
%    cellCenterZArray (:obj:`int`):                     Array with centroid
%                                                       z coordinates of segmented objects.
%    pixelPerZplane (:obj:`int`):                       Number of pixel per
%                                                       Zplane
%    active3DFingerprint (:obj:`bool`):                 Switch for active
%                                                       3D fingerprint.
%
% Returns
% -------
%    retFq: :obj:`double`                               Array with the real
%                                                       fingerprints for each
%                                                       segmented object.
%
% :Authors:
%    Tomas Kuendig - initial implementation

tic

cellCenterZArray = cellCenterZArray/pixelPerZplane;

if isequal(fromFrame,toFrame)
    startFrame = fromFrame;
    endFrame = fromFrame;
else
    startFrame = fromFrame;
    endFrame = toFrame;
end

retFq = cell(numel(cellFrameArray), 1);


for nFrame = startFrame:endFrame
    % Read image of current frame
    if isequal(fromFrame,toFrame)
        numberPlanes = length(imfinfo(fullfile(imageDir, ...
            imageFingerprintFileArray{fromFrame})));
        for i = 1:numberPlanes
            imgRaw(:,:,i) = this.io.readImageToGrayScale(fullfile(imageDir, ...
                imageFingerprintFileArray{fromFrame}), imageCropCoordinateArray,i);
        end
    else
        numberPlanes = length(imfinfo(fullfile(imageDir, ...
            imageFingerprintFileArray{nFrame})));
        for i = 1:numberPlanes
            imgRaw(:,:,i) = this.io.readImageToGrayScale(fullfile(imageDir, ...
                imageFingerprintFileArray{nFrame}), imageCropCoordinateArray,i);
        end
    end

        % Has to be wide formated!
    cellCenterX = cellCenterXArray(cellFrameArray == nFrame);
    cellCenterY = cellCenterYArray(cellFrameArray == nFrame);
    cellCenterZ = cellCenterZArray(cellFrameArray == nFrame);
    
%     h = figure;
%     imshow(imgRaw(:,:,round(size(imgRaw,3)/2))/100);
%     hold on
%     plot(cellCenterX,cellCenterY,'r*')
    
    
    fromCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'first');
    toCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'last');
    toCellIdx = toCellIdx - (fromCellIdx-1);
    fromCellIdx = fromCellIdx - (fromCellIdx-1);
    
    retIdx = find(cellFrameArray == nFrame, 1, 'first');
    
    %figure;
    %t = tiledlayout('flow');
    for mCellIdx = fromCellIdx:toCellIdx
        %nexttile;
        if active3DFingerprint
            % Crop image to specified region around cell of current track
            windowCropCoordinates = [cellCenterX(mCellIdx) - fingerprintHalfWindowSideLength, ...
                cellCenterY(mCellIdx) - fingerprintHalfWindowSideLength, ...
                cellCenterZ(mCellIdx) - 2*fingerprintHalfWindowSideLength/pixelPerZplane, 2 * ...
                fingerprintHalfWindowSideLength, 2 * fingerprintHalfWindowSideLength,...
                4 * fingerprintHalfWindowSideLength/pixelPerZplane];
            if numel(windowCropCoordinates) == 6
                img = this.utils.padImageCrop(imgRaw, round(windowCropCoordinates));
                                
                [ retFq(retIdx) ] = this.computeFingerprint3D(...
                    img, fingerprintResizeFactor, fingerprintMaxConsideredFrequencies );
            else
                retFq(retIdx)  = nan;
            end
            
        else
            ClosestZPlane = round(cellCenterZ(mCellIdx));
            LowerZPlane = floor(cellCenterZ(mCellIdx));
            UpperZPlane = ceil(cellCenterZ(mCellIdx));
            
            RatioOfLowerToUpper = cellCenterZ(mCellIdx)-LowerZPlane;
            theImage = (1-RatioOfLowerToUpper)*imgRaw(:,:,LowerZPlane) + RatioOfLowerToUpper*imgRaw(:,:,UpperZPlane);
            
            windowCropCoordinates = round([cellCenterX(mCellIdx) - fingerprintHalfWindowSideLength, ...
                cellCenterY(mCellIdx) - fingerprintHalfWindowSideLength, 2 * ...
                fingerprintHalfWindowSideLength, 2 * fingerprintHalfWindowSideLength]);
            if numel(windowCropCoordinates) == 4
                img = this.utils.padImageCrop(theImage, windowCropCoordinates);
                [ retFq(retIdx) ] = this.computeFingerprint(...
                    img, fingerprintResizeFactor, fingerprintMaxConsideredFrequencies );
            else
                retFq(retIdx)  = nan;
            end
            
        end
        retIdx = retIdx + 1;
    end
end

    %t.Title.String = ["Frame" +  nFrame];
    %t.TileSpacing = 'none';
    
if isequal(fromFrame,toFrame)
    startIdx = find(cellFrameArray == nFrame, 1, 'first');
    endIdx = find(cellFrameArray == nFrame, 1, 'last');
    retFq  = retFq(startIdx:endIdx);
    
    if this.debugLevel > 1
        this.utils.printToConsole(...
            sprintf(['All fingerprints computed in %4.4f seconds on'...
            ' frame %d.'], toc, fromFrame))
    end
else
    if this.debugLevel > 1
        this.utils.printToConsole(...
            printf(['All fingerprints computed in %4.4f seconds on'...
            ' frames %d - %d.'], toc, startFrame, endFrame))
    end
end
end