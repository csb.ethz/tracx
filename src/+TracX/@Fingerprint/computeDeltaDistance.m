%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [deltaDistance, deltaDistanceNeighbours, fingerprintDistance, ...
    fingerprintDistanceNeighboursSize, deltaDistanceLowestNeighbour, ...
    deltaDistance2ndLowest, deltaDistance2ndLowestNeighbour, ...
    assignedFPdistAllFrames, neighbourDeltaDistTotalMovie] = ...
    computeDeltaDistance(this, data, startFrame, endFrame)
% COMPUTEDELTADISTANCE Computes the delta distance for non or tracked data. The delta distance 
% is defined as the difference in fingerprint distance among two image 
% frames of the lowest to the second lowest value. In case the data was 
% tracked. it is the difference between the fingerprint distance of the 
% assignment and the lowest one from either all other cells, the neighbourhood
% of the assigned one or the lowest one thereof.
%
% Args:
%    this (:obj:`object`):                    :class:`Fingerprint` instance
%    data (:obj:`object`):                    :class:`TrackerData` instance 
%    startFrame (:obj:`int`):                 :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.trackerStartFrame`
%    endFrame (:obj:`int`):                   :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.trackerEndFrame`
%
% Returns
% -------
%    deltaDistance: :obj:`double`
%                                                     Array of the delta distance 
%                                                     for each assignment on
%                                                     all frames  concatenated. 
%    deltaDistanceNeighbours: :obj:`double`
%                                                     Array of the delta distance
%                                                     of the neighbourhood of each
%                                                     assignment on all frames concatenated.
%    fingerprintDistance: :obj:`double` 
%                                                     Array of the fingerprint 
%                                                     distance for each assignment
%                                                     on all frames concatenated.
%    fingerprintDistanceNeighboursSize: :obj:`double` 
%                                                     Array of the fingerprint 
%                                                     distance of theneighbourhood 
%                                                     of each assignment on 
%                                                     all frames concatenated.
%    deltaDistanceLowestNeighbour: :obj:`double` 
%                                                     Array of the lowest delta
%                                                     distance of the neighbourhood 
%                                                     of each assignment on  
%                                                     all frames
%                                                     concatenated.
%    deltaDistance2ndLowest: :obj:`double`            
%                                                     Array of the second
%                                                     lowest delta 
%                                                     distance for each 
%                                                     assignment on  all 
%                                                     frames concatenated.
%    deltaDistance2ndLowestNeighbour: :obj:`double`   
%                                                     Array of the second
%                                                     lowest delta 
%                                                     distance of the 
%                                                     neighbourhood for each 
%                                                     assignment on  all 
%                                                     frames concatenated.
%    assignedFPdistAllFrames: :obj:`cell`             
%                                                     Cell array of the fingerprint
%                                                     distance for each assignment
%                                                     on all frames.
%    neighbourDeltaDistTotalMovie: :obj:`cell`     
%                                                     Cell array of the delta
%                                                     distances of the neighbourhood 
%                                                     for each assignment on all frames.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

fpDistAllFrames = cell(1, endFrame) ;
assignedFPdistAllFrames = cell(1, endFrame);
neighbourDeltaDistTotalMovie = cell(1, endFrame);
currFrameFinalAssDistsTotalMovie = cell(1, endFrame);
lowest2FPdistsTotalMovie = cell(1, endFrame);
lowest2DeltaDistTotalMovie = cell(1, endFrame);
deltaDistanceAssignedFPLowestTotalMovie = cell(1, endFrame);
deltaDistance2ndLowestTotalMovie = cell(1, endFrame);
deltaDistanceLowestNeighbourhood = cell(1, endFrame);
deltaDistance2ndLowestNeighbourhood = cell(1, endFrame);
for fn = startFrame:(endFrame - 1)
    
    fpO = data.getFieldArrayForFrame('track_fingerprint_real', fn);
    fpN = data.getFieldArrayForFrame('track_fingerprint_real', fn+1);
    
    % Calculated all vs all fingerprint distances among
    % consecutive image frames.
    [fpDistAllCurrFrame] = this.computeFingerprintDistance(fpO, ...
        fpN);
    
    trackIDO = data.getFieldArrayForFrame('track_index', fn);
    trackIDN = data.getFieldArrayForFrame('track_index', fn+1);
    
    
    neighbourFPdist = cell(numel(trackIDO), 1);
    neighbourhoodFPdist = cell(numel(trackIDO), 1);
    assignedFPdist = nan(numel(trackIDO), 1);
    currFrameFinalAssDists = cell(numel(trackIDO), 1);
    neighbourDeltaDist = cell(numel(trackIDO), 1);
    lowestNeighbourDeltaDist = nan(numel(trackIDO), 1);
    secondLowestNeighbourDeltaDist = nan(numel(trackIDO), 1);
    for ti = 1:numel(trackIDO)
        nIdx = data.SegmentationData. ...
            cell_close_neighbour(fn+1).Indicies;
        matchIdx = ismember(trackIDN, trackIDO(ti))';
        if any(unique(matchIdx)) ~= 0
            neighbourFPdist{ti, 1} = fpDistAllCurrFrame(ti, ...
                nIdx(matchIdx,:));
            neighbourhoodFPdist = fpDistAllCurrFrame(ti, ...
                or(nIdx(matchIdx,:), matchIdx));
            assignedFPdist(ti, 1) = fpDistAllCurrFrame(ti,...
                matchIdx);
            if ~isempty(neighbourFPdist{ti, 1})
                neighbourhoodFPdistS = sort(neighbourhoodFPdist);
                lowestNeighbourDeltaDist(ti, 1) = ...
                    neighbourhoodFPdistS(1) - assignedFPdist(ti, 1);
                secondLowestNeighbourDeltaDist(ti, 1) = ...
                    neighbourhoodFPdistS(2) - assignedFPdist(ti, 1);
            else
                lowestNeighbourDeltaDist(ti, 1) = nan;
                secondLowestNeighbourDeltaDist(ti, 1) = nan;
            end
            currFrameFinalAssDists{ti, 1} = repmat( ...
                assignedFPdist(ti, 1), 1, numel(...
                fpDistAllCurrFrame(ti, nIdx(matchIdx,:))));
            neighbourDeltaDist{ti, 1} = neighbourFPdist{ti, 1}...
                - currFrameFinalAssDists{ti, 1};
        else
            neighbourFPdist{ti, 1} = nan;
            assignedFPdist(ti, 1) = nan;
            lowestNeighbourDeltaDist(ti, 1) = nan;
            secondLowestNeighbourDeltaDist(ti, 1) = nan;
            currFrameFinalAssDists{ti, 1} = assignedFPdist(ti, 1);
            neighbourDeltaDist{ti, 1} = neighbourFPdist{ti, 1} ...
                - currFrameFinalAssDists{ti, 1};
        end
    end
    fpDistAllFramesSorted =  sort(fpDistAllCurrFrame, 2);
    lowest2FPdists = fpDistAllFramesSorted(:, 1:2);
    
    fpDistAllFrames{fn} = fpDistAllCurrFrame;
    assignedFPdistAllFrames{fn} = assignedFPdist;
    neighbourDeltaDistTotalMovie{fn} = neighbourDeltaDist;
    deltaDistanceLowestNeighbourhood{fn} = lowestNeighbourDeltaDist;
    deltaDistance2ndLowestNeighbourhood{fn} = secondLowestNeighbourDeltaDist;
    currFrameFinalAssDistsTotalMovie{fn} = currFrameFinalAssDists;
    lowest2FPdistsTotalMovie{fn} = lowest2FPdists;
    deltaDistanceAssignedFPLowestTotalMovie{fn} = ...
        lowest2FPdists(:, 1) - assignedFPdist;
    deltaDistance2ndLowestTotalMovie{fn} = ...
        lowest2FPdists(:, 2) - assignedFPdist;
    lowest2DeltaDistTotalMovie{fn} = lowest2FPdists - ...
        repmat(assignedFPdist, 1, 2);
    
end
% Handle last frame
nTracksEndFrame = numel(data.getFieldArrayForFrame( 'cell_frame', endFrame));
assignedFPdistAllFrames{endFrame} = nan(nTracksEndFrame, 1);
deltaDistanceAssignedFPLowestTotalMovie{endFrame} = nan(nTracksEndFrame, 1);
deltaDistance2ndLowestTotalMovie{endFrame} = nan(nTracksEndFrame, 1);
currFrameFinalAssDistsTotalMovie{endFrame} = num2cell(nan(nTracksEndFrame,1)); %{nan(nTracksEndFrame, 1)}; % nest nan in cell array
neighbourDeltaDistTotalMovie{endFrame} = num2cell(nan(nTracksEndFrame,1)); % {nan(nTracksEndFrame, 1)}; % nest nan in cell array
deltaDistanceLowestNeighbourhood{endFrame} = nan(nTracksEndFrame, 1);
deltaDistance2ndLowestNeighbourhood{endFrame} = nan(nTracksEndFrame, 1);

deltaDistance = vertcat(deltaDistanceAssignedFPLowestTotalMovie{:});
deltaDistance2ndLowest = vertcat(deltaDistance2ndLowestTotalMovie{:});
fingerprintDistance = vertcat(assignedFPdistAllFrames{:});

currFrameFinalAssDistsTotalMovieArray = [];
neighbourDeltaDistTotalMovieArray = [];
for l = 1:size(currFrameFinalAssDistsTotalMovie, 2)
    if ~isempty(currFrameFinalAssDistsTotalMovie{l})
        currFrameFinalAssDistsTotalMovieArray = [...
            currFrameFinalAssDistsTotalMovieArray, ...
            [currFrameFinalAssDistsTotalMovie{l}{:}]];
        neighbourDeltaDistTotalMovieArray = [...
            neighbourDeltaDistTotalMovieArray, ...
            [neighbourDeltaDistTotalMovie{l}{:}]];
    end
end

fingerprintDistanceNeighboursSize = currFrameFinalAssDistsTotalMovieArray';
deltaDistanceNeighbours = neighbourDeltaDistTotalMovieArray';
deltaDistanceLowestNeighbour = vertcat(deltaDistanceLowestNeighbourhood{:});
deltaDistance2ndLowestNeighbour = vertcat(deltaDistance2ndLowestNeighbourhood{:});
end