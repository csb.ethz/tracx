%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ realTrackFingerprintDistance] = ...
    computeFingerprintDistance(~, realTrackFingerprintOldFrame, ...
    realTrackFingerprintNewFrame)
% COMPUTEFINGERPRINTDISTANCE Computes the fingerprint distance between two 
% typically subsequent image frames (oldFrame and newFrame)
% based on real fingerprints. For real fingerprints the
% least square distance is computed. The DC component (0,0) of the real
% fingerprints are removed.
%
% Args:
%    ignoredArg (:obj:`object`):                           :class:`Fingerprint` instance 
%    realTrackFingerprintOldFrame (:obj:`cell array`):     Real fingerprint array for 
%                                                          all cells in the oldFrame
%    realTrackFingerprintNewFrame (:obj:`cell array`):     Real fingerprint array for all cells
%                                                          in the subsequent image frame (newFrame).
%
% Returns
% -------
%    realTrackFingerprintDistance: :obj:`double`
%                                                  Fingerprint distances between
%                                                  all cells from the oldFrame 
%                                                  and all its assigned cells in
%                                                  the newFrame based on the
%                                                  real fingerprints.
%
% :Authors:
%    Andreas P. Cuny - initial implementation
% :Authors:
%    Joerg Stelling -  speed optimization

warning('off', 'stats:pdist2:DataConversion')

fpoM = cell2mat(realTrackFingerprintOldFrame)';
fpnM = cell2mat(realTrackFingerprintNewFrame)';

% Remove DC component first
fpoM(1, :) = [];
fpnM(1, :) = [];

nx = numel(realTrackFingerprintOldFrame);
ny = numel(realTrackFingerprintNewFrame);
realTrackFingerprintDistance = nan(nx,ny);
vy   = ones(1,ny);

for zx = 1:nx
    fpo = fpoM(:,zx);
    realTrackFingerprintDistance(zx,:) = sqrt(sum((fpo*vy - fpnM).^2)) ...
        / norm(fpo);
end

end