%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ pLowerFPArray ] = computeNeighbourProbFPBelowAss(~, ...
    assignedFPdistTotalMovie, ...
    neighbourDeltaDistTotalMovie, FPByFraction, startIdx, nData)
% COMPUTENEIGHBOURPROBFPBELOWASS Computes the cumultative
% probability for each assigned cell that a neighbouring cells has lower
% fingerprint distance (negative delta distance) and would a more similar
% cell according to the fingerprint measure alone.
%
% Args:
%    ignoredArg (:obj:`object`):                       :class:`Fingerprint` instance 
%    assignedFPdistTotalMovie (:obj:`cell array`):     Array of fingerprint 
%                                                      distances of the 
%                                                      assigned cells for 
%                                                      each tracked frame.
%    neighbourDeltaDistTotalMovie (:obj:`cell array`): Array of delta 
%                                                      distances from the 
%                                                      neighbours of the 
%                                                      assigned cells for 
%                                                      each tracked frame.
%    FPByFraction (:obj:`bool`):                       True (default): calculate
%                                                      faction of neighbours 
%                                                      with lower FP distance.
%                                                      False: calculate if 
%                                                      any neighbour has 
%                                                      lower FP distance
%    startIdx (:obj:`int`):                            Start index for the
%                                                      first frame tracking 
%                                                      started.
%    nData (:obj:`int`):                               Total length of the
%                                                      data columns.
%
% Returns
% -------
%     pLowerFPArray: :obj:`float`:          
%                                                      Cumultative probability
%                                                      for each assignment
%                                                      that a neighbour has
%                                                      a lower fingerprint 
%                                                      distance.
%
% :Authors:
%    Joerg Stelling - initial implementation of the idea
% :Authors:
%    Andreas P. Cuny - implementation for TracX evaluation

% initialize output array
pLowerFPArray = nan(nData, 1);
                      
nf = length(assignedFPdistTotalMovie);
% FPDistThreshold = nan(1,nf);
% FPDistCDF = nan(1,nf);

for currFrame = 1:nf   
    fingerprintDistance = assignedFPdistTotalMovie{currFrame};
    neighbourDeltaDist  = neighbourDeltaDistTotalMovie{currFrame};
    
    n = length(fingerprintDistance);      
    % number of neighbours, number neighbours with lower FP distance than
    % assigned object
    [nn, nnfp] = deal(n,1);
    
    % counts for neighbourhood objects
    for z = 1:n
        v = neighbourDeltaDist{z};
        v = v(~isnan(v));        % delta dist of neighbours of a certain cell no nan
        if FPByFraction
            nn(z)   = length(v); % number of neighbours of a certain cell
            nnfp(z) = sum(v < 0); % number of neighbours whith delta distance below 0. So assignment is not unique.
        else
            nn(z)   = any(v);
            nnfp(z) = any(v <0);
        end
    end
    
    pLowerFPOrigOrder = nnfp./nn;
        
    % identify FP threshold
    if currFrame == 1
        pLowerFPArray(startIdx:numel(pLowerFPOrigOrder), 1) = pLowerFPOrigOrder;
        sIdx = numel(pLowerFPOrigOrder) + 1;
        eIdx = numel(pLowerFPOrigOrder);
    else
        eIdx = eIdx + numel(pLowerFPOrigOrder);
        pLowerFPArray(sIdx:eIdx, 1) = pLowerFPOrigOrder;
        sIdx = sIdx + numel(pLowerFPOrigOrder);
    end
end
end
