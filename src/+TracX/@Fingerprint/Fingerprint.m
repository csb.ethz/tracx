%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef Fingerprint
    % Fingerprint class constructs a Fingerprint object to compute
    % the cell region fingerprint. Additionally it implements all the
    % methods to compute the fingerprint and delta distance.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
        
        utils % :class:`+TracX.@Utils` instance implementing utility methods

        io % :class:`+TracX.@IO` instance implementing read/write methods

        debugLevel % Debug level
        
    end
    
    methods
        
        % Constructor
        function obj = Fingerprint(utils, io)
            % Fingerprint constructs an empty object of
            % type Fingerprint. This class implements the fingerprint and
            % related functionalities.
            %
            % Args:
            %    utils (:obj:`object`):            Utils instance.
            %    io (:obj:`object`):               IO instance.
            %    configuration (:obj:`object`):    ImageProcessing instance.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                                      Returns a Fingerprint instance
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            
            obj.utils = utils;
            obj.io = io;
        end
        
        [ realTrackFingerprintDistance ] = ...
            computeFingerprintDistance(this, realTrackFingerprintOldFrame, ...
            realTrackFingerprintNewFrame)
        % COMPUTEFINGERPRINTDISTANCE Computes the fingerprint distance
        % between two typically subsequent image frames (oldFrame and newFrame)
        % based on real fingerprints. For real 
        % fingerprints the least square distance is computed. The DC
        %  component (0,0) of the real fingerprints are removed.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprintDistance`
        
        [ realFingerprintTrackDistance ] = ...
            computeAssignmentFingerprintDistanceArray(this, oldFrame, newFrame)
        %  COMPUTEASSIGNMENTFINGERPRINTDISTANCEARRAY Computes the
        %     fingerprint distance for real fingerprints for all
        %     assigned cells between oldFrame and newFrame.
        %
        % See also:
        %   `+TracX.fingerprint.computeAssignmentFingerprintDistanceArray`
        
        [ retFq ] = computeFingerprintForFrame(this, fromFrame, toFrame,  ...
            fingerprintHalfWindowSideLength, fingerprintResizeFactor, ...
            fingerprintMaxConsideredFrequencies, imageDir, ...
            imageFingerprintFileArray, imageCropCoordinateArray, ...
            cellFrameArray, cellCenterXArray, cellCenterYArray)
        % COMPUTEFINGERPRINTFORFRAME Computes a unique cell quality
        % control feature based on an out-of-focus Brightfield image
        % using the DCT transform. Here the fingerprints are computed
        % for each frame using DCT and binarization of the frequencies
        % containing the structural information of the cell resulting in
        % a unique string. It is possible to only calculate the fingerprint
        % for a number of specified image frames.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprintForFrame`
        
        [  retFq ] = computeFingerprintForFrame3D(this, fromFrame, toFrame,  ...
            fingerprintHalfWindowSideLength, fingerprintResizeFactor, ...
            fingerprintMaxConsideredFrequencies, imageDir, imageFingerprintFileArray, ...
            imageCropCoordinateArray, cellFrameArray, cellCenterXArray, cellCenterYArray, ...
            cellCenterZArray, PixelPerZplane, active3DFingerprint)
        % COMPUTEFINGERPRINTFORFRAME Computes a unique cell quality
        % control feature based on an out-of-focus Brightfield 3D image
        % using the DCT transform. Here the fingerprints are computed
        % for each frame using DCT and binarization of the frequencies
        % containing the structural information of the cell resulting in
        % a unique string. It is possible to only calculate the fingerprint
        % for a number of specified image frames.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprintForFrame3D`
        
        [ retFq ] = computeFingerprint(this, img, ...
            fingerprintResizeFactor, fingerprintMaxConsideredFrequencies )
        % COMPUTEFINGERPRINT Computes a unique cell quality
        % control featured based on an image using the DCT transform.
        % Here the fingerprint is computed for the image using DCT and
        % the frequencies containing the structural
        % information of the cell resulting in a unique string.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprint`
        
        [ retFq ] = computeFingerprint3D(this, img, ...
            fingerprintResizeFactor, fingerprintMaxConsideredFrequencies )
        % COMPUTEFINGERPRINT Computes a unique cell quality
        % control featured based on an image using the DCT transform.
        % Here the fingerprint is computed for the image using DCT and
        % the frequencies containing the structural
        % information of the cell resulting in a unique string.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprint3D`
        
        [ fpDistThreshold, fpDistThresholdMadCorr, fpDistanceSubset] = ...
            computeFPDistThreshold(~, xFPDistAssigned, xFPDistAssignedToCompare, ...
            yDeltaDistAssigned, yDeltaDistToCompare, lowerQuantile)
        % COMPUTEFPDISTTHRESHOLD Computes the fingerprint distance
        % threshold based on the lower quantile of negative delta distances.
        %
        % See also:
        %   `+TracX.fingerprint.computeFPDistThreshold`
        
        [deltaDistance, deltaDistanceNeighbours, fingerprintDistance, ...
            fingerprintDistanceNeighboursSize, deltaDistanceLowestNeighbour, ...
            deltaDistance2ndLowest, deltaDistance2ndLowestNeighbour, ...
            assignedFPdistTotalMovie, neighbourDeltaDistTotalMovie] = ...
            computeDeltaDistance(this, data, startFrame, endFrame, ...
            useAssignment)
        % COMPUTEDELTADISTANCE Computes the delta distance for non or
        % tracked data. The delta distance is defined as the difference in
        % fingerprint distance among two image frames of the lowest to the second
        % lowest value. In case the data was tracked. it is the difference between
        % the fingerprint distance of the assignment and the lowest one from either
        % all other cells, the neighbourhood of the assigned one or the lowest one
        % thereof.
        %
        % See also:
        %   `+TracX.fingerprint.computeDeltaDistance`
        
        [ pLowerFPArray ] = computeNeighbourProbFPBelowAss(this, ...
            assignedFPdistTotalMovie, neighbourDeltaDistTotalMovie, ...
            FPByFraction, startIdx, nData)
        % COMPUTENEIGHBOURPROBFPBELOWASS Computes the cumultative
        % probability for each assigned cell that a neighbouring cells has
        % lower fingerprint distance (negative delta distance) and would
        % a more similar cell according to the fingerprint measure alone.
        %
        % See also:
        %   `+TracX.fingerprint.computeNeighbourProbFPBelowAss`
        
        [FFPa, FFP, NNFPa, NNFP, nNhbra, nNhbr] = ...
            computeFingerprintAsgmtFrac(this, crfO, crfN, dCRFAsgmt, ...
            nhbrhdM, maxNNgbr)
        % COMPUTEFINGERPRINTASGMTFRAC Computes the faction for each assigned
        % cell that a neighbouring cells has lower fingerprint distance (dCRF)
        % as the difference of dCRF(assignment) - dCRF(neighbour). So there is a
        % more similar cell according to the fingerprint (CRF) measure.
        %
        % See also:
        %   `+TracX.fingerprint.computeFingerprintAsgmtFrac`
        
    end
end