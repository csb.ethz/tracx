%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ retFq ] = computeFingerprint3D(~, img, ...
    fingerprintResizeFactor, fingerprintMaxConsideredFrequencies )
% COMPUTEFINGERPRINT3D Computes a unique cell quality 
% control featured based on an image using the DCT transform.
% Here the fingerprint is computed for the image using DCT and
% binarization of the frequencies containing the structural
% information of the cell resulting in a unique string. 
%
% Args:
%    ignoredArg (:obj:`object`):                       :class:`Fingerprint` instance 
%    img (:obj:`uint8` NxNx3 uint8, 16, 32, 64):       Image matrix         
%    fingerprintResizeFactor (:obj:`int`):             +TracX.ParameterConfiguration.fingerprintResizeFactor
%    fingerprintMaxConsideredFrequencies(:obj:`int`):  +TracX.ParameterConfiguration.fingerprintMaxConsideredFrequencies
%
% Returns
% ------- 
%    retFq: :obj:`float` 1xM                     
%                                                       [1x64] Real frequency 
%                                                       fingerprint array.
%
% :Authors:
%    Tomas Kuendig - initial implementation

if all(size(img)) == 0
    error('Image is empty.');
end
% Resize image
imgResized = imresize3(img, 1/(size(img,1)/fingerprintResizeFactor));
% Cosine transform
imgResizedDCTT = TracX.ExternalDependencies.mirt_dctn(imgResized);
% Take lowest MAX_FREQUENCIES^2 frequencies
DCTComponents = imgResizedDCTT(1:fingerprintMaxConsideredFrequencies, ...
    1:fingerprintMaxConsideredFrequencies,1:fingerprintMaxConsideredFrequencies);
% Retrun the lowest MAX_FREQUENCIES^2 (with DC at (1,1))
retFq =  {DCTComponents(:)'};

end