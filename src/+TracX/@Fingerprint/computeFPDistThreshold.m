%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ fpDistThreshold, fpDistThresholdMadCorr, fpDistanceSubset ] =...
    computeFPDistThreshold(~, xFPDistAssigned, xFPDistAssignedToCompare, ...
    yDeltaDistAssigned, yDeltaDistToCompare, lowerQuantile)
% COMPUTEFPDISTTHRESHOLD Computes the fingerprint distance threshold 
% based on the lower quantile of negative delta distances.
%
% Args:
%    ignoredArg (:obj:`object`):                  :class:`Fingerprint` instance 
%    xFPDistAssigned (:obj:`array` 1xN):          Fingerprint distance of 
%                                                 all assignments         
%    xFPDistAssignedToCompare (:obj:`array` 1xM): Fingerprint distances of
%                                                 neighbouring cells for 
%                                                 each assigned cell.
%    yDeltaDistAssigned (:obj:`array` 1xN):       Delta distance of all
%                                                 assignments to the
%                                                 minimal FPdist of all
%                                                 possible assignments for
%                                                 each cell.
%    yDeltaDistToCompare (:obj:`array` 1xM):      Delta distance of all
%                                                 assignments to their 
%                                                 comparator (i.e. its direct
%                                                 neighbours or all other
%                                                 cells)
%    lowerQuantile (:obj:`float` 1x1):            Lower quantile in the 
%                                                 range [0,1];
%
% Returns
% -------
%    fpDistThreshold: :obj:`float` 1x1:           
%                                                 Fingerprint distance
%                                                 threshold.
%    fpDistThresholdMadCorr: :obj:`float` 1x1:  
%                                                 Fingerprint distance
%                                                 threshold outlier corrected.
%    fpDistanceSubset: :obj:`float` 1x1:          
%                                                 Data used for
%                                                 fingerprint threshold
%                                                 determination.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

fpDistanceSubsetA = xFPDistAssigned(...
    yDeltaDistAssigned < 0)';
fpDistanceSubsetN = xFPDistAssignedToCompare(...
    yDeltaDistToCompare < 0)';
fpDistanceSubset = [fpDistanceSubsetA, ...
    fpDistanceSubsetN];
if isempty(fpDistanceSubset)
    fpDistanceSubset = max([max(xFPDistAssigned), ...
        max(xFPDistAssignedToCompare)]);
end

lowT = mean(fpDistanceSubset) - 2* mad(fpDistanceSubset);
idxBelowLowT = fpDistanceSubset < lowT;
fpDistThresholdMadCorr = quantile(fpDistanceSubset(~idxBelowLowT), lowerQuantile);
[fpDistThreshold] = quantile(fpDistanceSubset, lowerQuantile);
fpDistThreshold = fpDistThreshold(1);

end
