%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [retFq] = computeFingerprintForFrame(this, fromFrame, toFrame,  ...
    fingerprintHalfWindowSideLength, fingerprintResizeFactor, ...
    fingerprintMaxConsideredFrequencies, imageDir, imageFingerprintFileArray, ...
    imageCropCoordinateArray, cellFrameArray, cellCenterXArray, cellCenterYArray)
% COMPUTEFINGERPRINTFORFRAME Computes a unique cell quality control feature
% based on an out-of-focus Brightfield image using the DCT transform.
% Here the fingerprints are computed for each frame using DCT and
% the frequencies containing the structural
% information of the cell resulting in a unique string. It is possible
% to only calculate the fingerprint for a number of specified image frames.
%
% Args:
%    this (:obj:`object`):                              :class:`Fingerprint` instance 
%    fromFrame (:obj:`int`):                            Starting frame for
%                                                       calculation
%    toFrame (:obj:`int`):                              End frame for calculation
%    fingerprintHalfWindowSideLength  (:obj:`int`):     +TracX.ParameterConfiguration.fingerprintHalfWindowSideLength
%    fingerprintResizeFactor  (:obj:`int`):             +TracX.ParameterConfiguration.fingerprintResizeFactor
%    fingerprintMaxConsideredFrequencies (:obj:`int`):  +TracX.ParameterConfiguration.fingerprintMaxConsideredFrequencies
%    imageDir (:obj:`str`):                             +TracX.ProjectConfiguration.imageDir
%    imageFingerprintFileArray (:obj:`cell array` 1xK): +TracX.ProjectConfiguration.imageFingerprintFileArray
%    imageCropCoordinateArray (:obj:`double` 1x4):      +TracX.ProjectConfiguration.imageCropCoordinateArray
%    cellFrameArray (:obj:`int`):                       Array with all image
%                                                       frame numbers.
%    cellCenterXArray (:obj:`int`):                     Array with centroid x
%                                                       coordinates of segmented objects.
%    cellCenterYArray (:obj:`int`):                     Array with centroid x
%                                                       coordinates of segmented objects.
%
% Returns
% -------
%    retFq: :obj:`double`                               Array with the real
%                                                       fingerprints for each
%                                                       segmented object.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

tic

if isequal(fromFrame,toFrame)
    startFrame = fromFrame;
    endFrame = fromFrame;
else
    startFrame = fromFrame;
    endFrame = toFrame;
end

retFq = cell(numel(cellFrameArray), 1);
for nFrame = startFrame:endFrame
    % Read image of current frame
    if isequal(fromFrame,toFrame)
        imgRaw = this.io.readImageToGrayScale(fullfile(imageDir, ...
            imageFingerprintFileArray{fromFrame}), imageCropCoordinateArray);
    else
        imgRaw = this.io.readImageToGrayScale(fullfile(imageDir, ...
            imageFingerprintFileArray{nFrame}), imageCropCoordinateArray);
    end
    
    % Has to be wide formated!
    cellCenterX = cellCenterXArray(cellFrameArray == nFrame);
    cellCenterY = cellCenterYArray(cellFrameArray == nFrame);
    
    fromCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'first');
    toCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'last');
    toCellIdx = toCellIdx - (fromCellIdx-1);
    fromCellIdx = fromCellIdx - (fromCellIdx-1);
    
    retIdx = find(cellFrameArray == nFrame, 1, 'first');
    for mCellIdx = fromCellIdx:toCellIdx
        % Crop image to specified region around cell of current track
        windowCropCoordinates = [cellCenterX(mCellIdx) - fingerprintHalfWindowSideLength, ...
            cellCenterY(mCellIdx) - fingerprintHalfWindowSideLength, 2 * ...
            fingerprintHalfWindowSideLength, 2 * fingerprintHalfWindowSideLength];
        if numel(windowCropCoordinates) == 4
            img = this.utils.padImageCrop(imgRaw, windowCropCoordinates );
            [ retFq(retIdx) ] = this.computeFingerprint(...
                img, fingerprintResizeFactor, fingerprintMaxConsideredFrequencies );
        else
            retFq(retIdx)  = nan;
        end
        retIdx = retIdx + 1;
    end
end
if isequal(fromFrame,toFrame)
    startIdx = find(cellFrameArray == nFrame, 1, 'first');
    endIdx = find(cellFrameArray == nFrame, 1, 'last');
    retFq  = retFq(startIdx:endIdx);
    
    if this.debugLevel > 1
        this.utils.printToConsole(...
            sprintf(['All fingerprints computed in %4.4f seconds on'...
            ' frame %d.'], toc, fromFrame))
    end
else
    if this.debugLevel > 1
        this.utils.printToConsole(...
            printf(['All fingerprints computed in %4.4f seconds on'...
            ' frames %d - %d.'], toc, startFrame, endFrame))
    end
end
end