%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [FFPa, FFP, NNFPa, NNFP, nNhbra, nNhbr] = ...
    computeFingerprintAsgmtFrac(this, crfO, crfN, dCRFAsgmt, nhbrhdM)
% COMPUTEFINGERPRINTASGMTFRAC Computes the faction for each assigned cell
% that a neighbouring cells has lower fingerprint distance (dCRF)
% as the difference of dCRF(assignment) - dCRF(neighbour). So there is a
% more similar cell according to the fingerprint (CRF) measure.
%
% Args:
%    this (:obj:`object`):       :class:`Fingerprint` instance 
%    crfO (:obj:`array`):        Fingerprint (CRF) array for oldFrame.
%    crfN (:obj:`array`):        Fingerprint (CRF) array fpr newFrame.
%    dCRFAsgmt (:obj:`array`):   Fingerprint distance (dCRF) of all assignments
%                                between old and newFrame
%    nhbrhdM (:obj:`array`):     Neighbourhood indices locial matrix
%
% Returns
% -------
%    FFPa: :obj:`array`         
%                                Fraction of neighbouring cells with lower 
%                                dCRF for each assigned cell
%    FFP: :obj:`array`          
%                                Fraction of neighbouring cells with lower 
%                                dCRF (size of # neighbour)
%    NNFPa: :obj:`array`        
%                                Sum of dCRFDiff > 0 for each assigned cell
%    NNFP: :obj:`array`         
%                                Sum of dCRFDiff > 0 (size of # neighbour)
%    nNhbra: :obj:`array`       
%                                Total number of neighbouring cells for 
%                                each assigned cell
%    nNhbr: :obj:`array`        
%                                Total number of neighbouring cells 
%                                (size of # neighbour)
%
% :Authors:
% 	Andreas P. Cuny - initial implementation

% Case no neighbours at all
if all(nhbrhdM(:)==0)
    FFPa = zeros(size(dCRFAsgmt));
    FFP = []; % no neighbours
    NNFPa = zeros(size(dCRFAsgmt));
    NNFP = []; % no neighbours
    nNhbra = zeros(size(dCRFAsgmt));
    nNhbr = []; % no neighbours
% Case at least 1 neighbour
else
    % Calculated all vs all cell region fingerprint distances (dCRF) among
    % consecutive image frames.
    [dCRFM] = this.computeFingerprintDistance(crfO, crfN);
    % Convert to column major format
    dCRFAllcm = dCRFM';
    % dCRF for neighbouring tracks on frame frameNumber
    dCRFNhbrhd = dCRFAllcm(nhbrhdM');
    
    % % dCRF for neighbouring tracks on frame frameNumber
    % dCRFNhbrhd = dCRFAllcm(nhbrhdM(1:size(dCRFAllcm, 1), 1:size(dCRFAllcm, 2)));
    % % Number of neighbours for each assignment (col major)
    % nNhbra = sum(nhbrhdM(1:size(dCRFAllcm, 1), 1:size(dCRFAllcm, 2)), 2);
    nNhbra = sum(nhbrhdM, 2);
    
    % Convert the dCRF for the assigned tracks array to the shape of the
    % neighbourhood array (dCRFNhbrhd)
    selIdx = nNhbra > 0;
    eLoc = cumsum(nNhbra(selIdx));
    stIdx = zeros(1, eLoc(end));
    stIdx(eLoc - nNhbra(selIdx) + 1) = 1;
    selDCRFAsgmt = dCRFAsgmt(selIdx);
    dCRFAsgmtSNhbrhd = selDCRFAsgmt(cumsum(stIdx));
    
    % (should we include the assigned cell itself too to the neighbourhood?
    % Would result to allways 0 for dCRFDiff)
    dCRFDiff = dCRFAsgmtSNhbrhd - dCRFNhbrhd;
    NNFP   = sum(dCRFDiff>0 & ~isnan(dCRFDiff), 2);
    
    % Fraction of neighboring cells with lower FP distance for each neighbour
    % (size of dCRFNhbrhd)
    nNhbr = nNhbra(cumsum(stIdx));
    FFP = NNFP./nNhbr;
    
    % Fraction of neighbouring cells with lower FP distance for each assigned
    % cell
    NNFPa = zeros(size(nNhbra));
    NNFPa(selIdx) = accumarray(cumsum(stIdx)',NNFP);
    FFPa = NNFPa./nNhbra;
    % Replace assignments with no neighbours (nan) with 0. 
    FFPa(nNhbra == 0) = 0;
end
end