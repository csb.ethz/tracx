%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function  [realFingerprintTrackDistance] = ...
    computeAssignmentFingerprintDistanceArray(this, oldFrame, newFrame)
% COMPUTEASSIGNMENTFINGERPRINTDISTANCEARRAY Computes the
% fingerprint distance for real fingerprints for all assigned
% cells between oldFrame and newFrame.
%
% Args:
%    this (:obj:`object`):                           :class:`Fingerprint` instance 
%    oldFrame (:obj:`object`):                       :class:`~+TracX.@TrackerData.oldFrame`
%    newFrame (:obj:`object`):                       :class:`~+TracX.@TrackerData.mewFrame` 
%
% Returns
% -------
%    realFingerprintTrackDistance: :obj:`double`
%                                                  Array with fingerprint 
%                                                  distances from real 
%                                                  fingerprints.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

[realFingerprintDist] = this.computeFingerprintDistance(...
    oldFrame.track_fingerprint_real, newFrame.track_fingerprint_real);

[~, oldFrameLocIdx, newFrameLocIdx] = intersect(oldFrame.track_index, ...
    newFrame.track_index, 'stable');
locIdxAssignedTracks = sub2ind(size(realFingerprintDist), oldFrameLocIdx, ...
    newFrameLocIdx);

realFingerprintTrackDistanceTmp = realFingerprintDist(locIdxAssignedTracks);

realFingerprintTrackDistance = nan(numel(oldFrame.track_index), 1);

realFingerprintTrackDistance(oldFrameLocIdx) ...
    = realFingerprintTrackDistanceTmp;
end
