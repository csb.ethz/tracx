%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ wdX, wdY, wdZ ] = filterVectorField(~, X, Y, Z, dX, dY, dZ, radiusSupportX, ...
    radiusSupportY, radiusSupportZ)
% FILTERVECTORFIELD Filters a vector field by calculating a
% Gaussian-weighted average of neighboring vector components
% at each position in the vector field.
%
% The support of the Gaussian weighting kernel is defined as the radius 
% of the 2D Gaussian from the center to the location where the support 
% falls to a 1% of the central value.
%
% Args:
%    ignoredArg (:obj:`object`):   :class:`Motion` instance
%    X (:obj:`array`, Nx1):        Array of X coordinates of the vector
%                                  field.
%    Y (:obj:`array`, Nx1):        Array of Y coordinates of the vector 
%                                  field.
%    dX (:obj:`array`, Nx1):       Array of X components at each 
%                                  position in the vector field.
%    dY (:obj:`array`, Nx1):       Array of Y components at each 
%                                  position in the vector field.
%    radiusSupportX (:obj:`int`):  Radius of the Gaussian weighting 
%                                  function support in X direction
%    radiusSupportY (:obj:`int`):  Radius of the Gaussian weighting 
%                                  function support in Y direction
%
% Returns
% -------
%    wdX: :obj:`array` Nx1           
%                                  Filtered components ate the X positions 
%                                  with calculated Gaussian weights
%    wdY: :obj:`array` Nx1             
%                                  Filtered components ate the Y positions 
%                                  with calculated Gaussian weights.
%
% :Authors:
%    Aaron Ponti     - initial implementation in Python 
% :Authors:
%    Andreas P. Cuny - Translation to Matlab 

% Make sure the dimensions are correct
X = reshape(X, length(X), 1);
Y = reshape(Y, length(Y), 1);
Z = reshape(Z, length(Z), 1);
dX = reshape(dX, length(dX), 1);
dY = reshape(dY, length(dY), 1);
dZ = reshape(dZ, length(dZ), 1);


% Prepare the output
wdX = zeros(size(X,1), 1);
wdY = zeros(size(Y,1), 1);
wdZ = zeros(size(Z,1), 1);


% Calculate sigma from the support
sigmaX = ((radiusSupportX)^2 / sqrt(2.0 * log(100))/2 );
sigmaY = ((radiusSupportY)^2 / sqrt(2.0 * log(100))/2 );
sigmaZ = ((radiusSupportZ)^2 / sqrt(2.0 * log(100))/2 );


for i = 1:length(X)
    % Calculate relative displacements around current position
    x0 = X - X(i);
    y0 = Y - Y(i);
    z0 = Z - Z(i);
    
    % Gaussian weight function - integral set to 1
    p = [x0, y0, z0];
    w = mvnpdf(p, [0, 0, 0], [sigmaX, sigmaY, sigmaZ]);
    w = w / sum(w);
    
    % Calculate weighted components wdX(i), wdY(i)
    wdX(i) = dot(dX', w);
    wdY(i) = dot(dY', w);
    wdZ(i) = dot(dZ', w);
end

end
