%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef Motion
    % MOTION class to deal with cell motion and filter vectorns on
    % consecutive images.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
    end
    
    methods
        
        % Constructor
        function obj = Motion()
            % MOTION Constructs a :class:`Motion` object to deal with cell
            % motion and filter vectorns on consecutive images.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                               Returns a :class:`Motion` instance
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
        end
        
        
        [ wdX, wdY, wdZ ] = interpolateVectorComponents(~, x, y, fdX, fdY, ...
            fdZ, Lia, neighbourLocIdxMatrix )
        % INTERPLOATEVECTORCOMPONENTS Averages vector components
        %  by its neighbourhood.
        
        [ wdX, wdY, wdZ ] = filterVectorField(~, X, Y, Z, dX, dY, dZ, ...
            radiusSupportX, radiusSupportY, radiusSupportZ)
        % FILTERVECTORFIELD Filters a vector field by calculating a
        %  Gaussian-weighted average of neighboring vector components
        %  at each position in the vector field.
        %  The support of the Gaussian weighting kernel is defined as the
        %  radius of the 2D Gaussian from the center to the location
        %  where the support falls to a 1% of the central value.
        
        [ invalidVectorsIdx, vecLength ] = getInvalidVectors(~, x, y, z, dx, ...
            dy, dz, neighbourMatrix, meanCellDiameter, nStd)
        % GETINVALIDVECTORS Tries to find invalid vectors based on an
        %  exceptional long vector distance (due to miss assignment). Vector
        %  lengths above n times the standard deviation from the mean are
        %  identified as invalid.
        
    end
end