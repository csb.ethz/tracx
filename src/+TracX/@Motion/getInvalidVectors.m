%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ invalidVectorsIdx, vecLength ] = getInvalidVectors(~, x, y, z, dx, ...
    dy, dz, neighbourMatrix, meanCellDiameter, nStd)
% GETINVALIDVECTORS Tries to find invalid vectors based on an
% exceptional long vector distance (due to miss assignment). Vector
% lengths above n times the standard deviation from the mean are
% identified as invalid.
%
% Args:
%    ignoredArg (:obj:`int`):              :class:`Motion` instance
%    x (:obj:`array`, Nx1):                Array of X coordinates of the 
%                                          vectors.
%    y (:obj:`array`, Nx1):                Array of Y coordinates of the
%                                          vectors.
%    dx (:obj:`array`, Nx1):               Array of dX coordinates of the
%                                          vectors.
%    dy (:obj:`array`, Nx1):               Array of dY coordinates of the
%                                          vectors.
%    neighbourMatrix (:obj:`bool`, NxN):   Logical matrix of neighbours of 
%                                          x and y.
%    meanCellDiameter (:obj:`float`, 1x1): TracX.ParameterConfiguration.meanCellDiameter
%    nStd (:obj:`int`, 1x1): :             Nr of standard deviations to keep 
%                                          from the mean vector length of the 
%                                          vector field.
%
% Returns
% -------
%    invalidVectorsIdx: :obj:`array` Nx1 
%                                         Logical indicies of invalid vector
%                                         lengths.
%    vecLength: :obj:`array` Nx1 
%                                         Array of common vector lengths.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

vecLength = pdist2([x, y, z], [dx, dy, dz] , 'euclidean');
vecLength = vecLength(logical(eye(length(vecLength))));
invalidVectorsIdx = zeros(numel(vecLength), 1);

for iCell = 1:size(neighbourMatrix, 1)
    localVecLength = vecLength(neighbourMatrix(iCell, :));
    meanVecLength = mean(localVecLength);
    stdVecLength = std(localVecLength);
    invalidVectorsIdx(iCell) = and(vecLength(iCell) > meanCellDiameter, ...
        vecLength(iCell) > (meanVecLength + nStd * stdVecLength));
end

invalidVectorsIdx = logical(invalidVectorsIdx);

end