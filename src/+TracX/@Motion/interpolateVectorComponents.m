%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ wdX, wdY, wdZ ] = interpolateVectorComponents(~, x, y, fdX, fdY, fdZ, Lia, ...
    neighbourLocIdxMatrix )
% INTERPOLATEVECTORCOMPONENTS Averages vector components by its
% neighbourhood.
%
% Args:
%    ignoredArg (:obj:`object`):                :class:`Motion` instance
%    x (:obj:`array`, Nx1):                     Array of X coordinates of all
%                                               cells.
%    y (:obj:`array`, Nx1):                     Array of Y coordinates of all 
%                                               cells.
%    fdX (:obj:`array`, Nx1):                   Array of Gaussian filtered X 
%                                               components 
%                                               at matching  position (identical
%                                               cells) in the vector field.
%    fdY (:obj:`array`, Nx1):                   Array of Gaussian filtered Y
%                                               components  at matching
%                                               position (identical cells)
%                                               in the vector field.
%    Lia (:obj:`array`, Nx1):                   Location indicies for dfX and  
%                                               dfY into X and Y.
%    neighbourLocIdxMatrix (:obj:`array`, Nx1): Location indicies of X,Y 
%                                               neighbourhood 
% 
% Returns
% -------
%    wdX: :obj:`array` Nx1                      
%                                               Filtered components ate the X  
%                                               positions with averaged components.
%    wdY: :obj:`array` Nx1                    
%                                               Filtered components ate the Y  
%                                               positions with averaged components.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

if numel(x) ~= numel(y) || all(size(neighbourLocIdxMatrix) ~= [numel(x), ...
        numel(y)])
    wdX = 0;
    wdY = 0;
    wdZ = 0;
    return
end

N = numel(x);
wdX = nan(N, 1);
wdY = nan(N, 1);
wdZ = nan(N, 1);
wdX(Lia) = fdX;
wdY(Lia) = fdY;
wdZ(Lia) = fdZ;
for iRow = 1:N
    if isnan(wdX(iRow)) == 1
        iRowNeighboursLocIdx = neighbourLocIdxMatrix(iRow, Lia);
        % Average only for coordinates with at least one neighbour. If 
        % there is only one neighbour take those vector components.
        if ~isempty(fdX(iRowNeighboursLocIdx))
            wdX(iRow) = mean(fdX(iRowNeighboursLocIdx));
            wdY(iRow) = mean(fdY(iRowNeighboursLocIdx));
            wdZ(iRow) = mean(fdZ(iRowNeighboursLocIdx));
        % Set other vector components to 0.
        else
            wdX(iRow) = 0;
            wdY(iRow) = 0;
            wdZ(iRow) = 0;
        end
    else
        % Use Gaussian weighted / filtered X, Y components directly
    end
end