%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function inspectNewRoots(this,varargin)

if isempty(varargin)
    roots = unique(this.data.SegmentationData.track_index(...
        this.data.getFieldArray('track_parent')==0));
else
    roots = varargin{1};
end

for i = 1:length(roots)
    iRoot = roots(i);
    firstFrame = min(this.data.getFieldArrayForTrackIndex('cell_frame',iRoot));
    
    if firstFrame <= 1
        continue;
    end
    
    images = {};
    images{1} = this.imageVisualization.visualise3DCells(...
        'PropertyToVisualise','track_index','Frames',[firstFrame-1 firstFrame],...
        'ReturnImage',1,'OutputType','none','Track',iRoot);
    images{2} = this.imageVisualization.visualise3DCells(...
        'PropertyToVisualise','track_parent','Frames',[firstFrame-1 firstFrame],...
        'ReturnImage',1,'OutputType','none');
    images{3} = this.imageVisualization.visualise3DCells(...
        'PropertyToVisualise','track_lineage_tree','Frames',[firstFrame-1 firstFrame],...
        'ReturnImage',1,'OutputType','none');
    
    figure;
    montage(cat(1,images{:}),'Size',[nan 2],'ThumbnailSize', [])
    title(sprintf("Root track %d",iRoot))
    
end
end