%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function printTrackerResultsForCellTrackingChallenge(this)

parts = strsplit(this.configuration.ProjectConfiguration.segmentationResultDir,'_ST');
outpath = [parts{1},'_RES'];

if ~isfolder(outpath)
    mkdir(outpath);
end

f = fopen(fullfile(outpath,...
    'res_track.txt'),'w');

    for track = unique(this.data.getFieldArray('track_index'))'
        frames = this.data.getFieldArrayForTrackIndex('cell_frame',track);
        parents = this.data.getFieldArrayForTrackIndex('track_parent',track);
        assert(length(unique(parents))==1)
        fprintf(f,'%d %d %d %d\n',track,min(frames)-1,max(frames)-1,parents(1));
    end
    %fprintf("File generation failed. Closing file\n");

fclose(f);
end