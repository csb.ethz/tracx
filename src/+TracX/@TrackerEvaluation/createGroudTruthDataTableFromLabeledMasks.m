%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = createGroudTruthDataTableFromLabeledMasks( this, ...
    imageFilePath, fileNameIdenfifier, varargin)
%> @brief CREATEGROUDTRUTHDATATABLEFROMLABELEDMASKS Creates a ground truth 
%>  table in the csv format form labeled mask files which form a ground truth.
%>  The columns are 'Frame_number', number of the mask frame, 'Cell_number',
%>  Unique idnetifier of a cell 'Cell_colour', 0 if a mandatory ground truth
%>  point, 1 if the presence of the cell is facultative, 'Position_x' and
%>  'Position_y' describe the position of the cell on the labeled mask in
%>  respct to the size of the labeled mask file.
%>
% Input:
%> @param this:                [Object] TrackerEvaluation instance.
%> @param imageFilePath:       [String] Path to the labeled masks
%> @param fileNameIdenfifier:  [String] Regular expression to identify
%>                                      the labeled mask files. I.e. 'Mask_*'.
%>
% Output:
%> @retval None:                        Writes ground truth table do disk.
%>
% /*********************************************************************
%> @copyright Copyright (c) 2016-2020  Andreas P. Cuny, andreas.cuny@bsse.ethz.ch
%> Computational Systems Biology group, ETH Zurich
%> All rights reserved. @todo Add Licence!
%>
%  Contributors:
%>    @contributors
%>    @contributor{Andreas P. Cuny - initial implementation}
%  *********************************************************************/

p = inputParser;
addRequired(p, 'this', @isobject);
addRequired(p,'imageFilePath', @isfolder);
addRequired(p,'fileNameIdenfifier', @ischar);

PixelsPerPlane  = this.configuration.ParameterConfiguration.pixelsPerZPlaneInterval * ...
    this.configuration.ParameterConfiguration.data3D;

parse(p, this, imageFilePath, fileNameIdenfifier, varargin{:});
p.Results;

imageFileArray = dir(fullfile(p.Results.imageFilePath, sprintf('*%s', ...
    p.Results.fileNameIdenfifier)));
imageFileArray = {imageFileArray.name}';

data = [];
for fn = 1:numel(imageFileArray)
    if PixelsPerPlane
        for i = 1:length(imfinfo(fullfile(p.Results.imageFilePath, imageFileArray{fn})))
            img(:,:,i) = imread(fullfile(p.Results.imageFilePath, imageFileArray{fn}),i);
        end
        features = regionprops3(img);
        centroid = vertcat(features.Centroid(features.Volume > 0,:));
    else
        img = imread(fullfile(p.Results.imageFilePath, imageFileArray{fn}));
        features = regionprops(img);
        centroid = vertcat(features([features.Area] > 0).Centroid);
    end
    
    labels = double(unique(img));
    labels(1) = []; 
    
    if PixelsPerPlane
        dataCurrentFrame = [repmat(fn, numel(labels), 1), labels,  ...
            zeros(numel(labels), 1), centroid(:, 1), centroid(:, 2), centroid(:, 3)*PixelsPerPlane];
    else
        dataCurrentFrame = [repmat(fn, numel(labels), 1), labels,  ...
            zeros(numel(labels), 1), centroid(:, 1), centroid(:, 2)];
    end
    data = [data; dataCurrentFrame];
end

if PixelsPerPlane
    header={'Frame_number', 'Cell_number', 'Cell_colour', 'Position_x', ...
    'Position_y', 'Position_z'};
else
    header={'Frame_number', 'Cell_number', 'Cell_colour', 'Position_x', ...
        'Position_y'};
end
groundTruth = array2table(data);
groundTruth.Properties.VariableNames = header;

if ~exist(fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, 'GroundTruth'), 'dir')
    mkdir(fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, 'GroundTruth'))
end
this.io.writeToCSV(groundTruth, fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, 'GroundTruth'), 'GroundTruth', ',')
end
