%> @brief EVALUATEFINGERPRINTDISTTHRESHOLDNORMALITY Tests if the data used
%> for the fingerpint distance threshold determination are distributed
%> normally using the one-sample Kolmogorov-Smirnov test.
%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = evaluateFingerprintDistThresholdNormality(this, figurePath )

% Normalization test:
[this.deltaDistance, this.deltaDistanceNeighbours, ...
    this.fingerprintDistance, ...
    this.fingerprintDistanceNeighboursSize, ~, ~, ~] = ...
    this.fingerprint.computeDeltaDistance(this.data, ...
    this.configuration.ProjectConfiguration.trackerStartFrame, ...
    this.configuration.ProjectConfiguration.trackerEndFrame);
[t, t2, dd] = this.fingerprint.computeFPDistThreshold(...
    this.fingerprintDistance, ...
    this.fingerprintDistanceNeighboursSize, ...
    this.deltaDistance, ...
    this.deltaDistanceNeighbours, 0.01);

figH = this.imageVisualization.plotFPVsDeltaDistPlot(...
    this.fingerprintDistance, this.fingerprintDistanceNeighboursSize, this.deltaDistance, ...
    this.deltaDistanceNeighbours, t, 0.01, 'plotTitle', false); % 'upXLim', 1.65, 'upYLim', 1.8, 'lowYLim', -0.8,
figH.PaperOrientation = 'landscape';
figH.PaperUnits ='centimeters';
figH.Children(2).Title.String = sprintf('curr threshold: %g, mad corr threshold: %g', t, t2);
print(figH, fullfile(figurePath, sprintf('DeltaDistVSNeighbours_NormCheck_%s', ...
    this.configuration.ProjectConfiguration.projectName)),'-dpng')

if numel(dd) > 2
    x = (dd-mean(dd))/std(dd);
    h = kstest(x);
    fh = figure;
    subplot(1,3,1)
    histogram(dd)
    xlabel('FPdist')
    ylabel('Counts')
    title('Selected FPdist < deltaDist == 0')
    subplot(1,3,2)
    histogram(x)
    xlabel('FPdist centered + scaled')
    title('Data centered and scaleed by mean and std')
    subplot(1,3,3)
    cdfplot(x)
    hold on
    x_values = linspace(min(x),max(x));
    plot(x_values,normcdf(x_values,0,1),'r-')
    if h == 0
        isNormal = 1;
    else
        isNormal = 0;
    end
    legend('Empirical CDF','Standard Normal CDF','Location','best')
    title(sprintf('Distribution is normal == %d', isNormal))
    
    fh.PaperOrientation = 'landscape';
    fh.PaperUnits ='centimeters';
    fh.PaperPosition =[0 0 30 20];
    print(fh, fullfile(figurePath, sprintf('NormalityTest_FPDistComputation_MadCorr_%s',...
        this.configuration.ProjectConfiguration.projectName)),'-dpng')
end
end
