%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************

function [truthCounter, divisionCounter] = ScoreLineageWithGT(this, GtFilePath)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

divisionCounter = 0;
truthCounter = 0; 

disp(GtFilePath)
f = fopen(GtFilePath, 'r');

if ~isprop(this.data.SegmentationData,'lineageMistake')
 this.data.SegmentationData.addprop('lineageMistake')
end
this.data.SegmentationData.lineageMistake = zeros(length(this.data.SegmentationData.track_index),1);

GT = struct();
GT.track = [];
GT.firstFrame = [];
GT.lastFrame = [];
GT.parent = [];
while true
    thisline = fgetl(f);
    if ~ischar(thisline); break; end
    
    line = double(strsplit(string(thisline)));
    GT.track(end+1)=line(1);
    GT.firstFrame(end+1)=line(2);
    GT.lastFrame(end+1)=line(3);
    GT.parent(end+1)=line(4);
end
fclose(f);

this.utils.printToConsole(sprintf("GT: %d division events", length(unique(GT.parent))-1))

for i = 1:length(GT.track)
    if GT.parent(i) == 0
        daughterFrame = fullfile(this.configuration.ProjectConfiguration.segmentationResultDir, ...
            'RawImages', sprintf('man_seg%03d.tif',GT.firstFrame(i)));
        daughterMask = this.io.readSegmentationMask(daughterFrame);
        daughter_cell_idx = unique(daughterMask(daughterMask == GT.track(i)));
        if (length(daughter_cell_idx)~=1); continue; end
        daughter_cell_trackerparent = this.data.SegmentationData.track_parent( ...
            this.data.SegmentationData.cell_frame == GT.firstFrame(i)+1 & ...
            this.data.SegmentationData.cell_index == daughter_cell_idx);
        parent_cell_trackerTrack = 0;
    else
        
        
        parentIdx = find(GT.track == GT.parent(i));
        
        daughterFrame = fullfile(this.configuration.ProjectConfiguration.segmentationResultDir, ...
            'RawImages', sprintf('man_seg%03d.tif',GT.firstFrame(i)));
        parentFrame = fullfile(this.configuration.ProjectConfiguration.segmentationResultDir, ...
            'RawImages', sprintf('man_seg%03d.tif',GT.lastFrame(parentIdx)));
        
        daughterMask = this.io.readSegmentationMask(daughterFrame);
        parentMask = this.io.readSegmentationMask(parentFrame);
        
        daughter_cell_idx = unique(daughterMask(daughterMask == GT.track(i)));
        parent_cell_idx = unique(parentMask(parentMask == GT.track(parentIdx)));
        assert(length(parent_cell_idx)==1 && length(daughter_cell_idx)==1);
        
        daughter_cell_trackerparent = this.data.SegmentationData.track_parent( ...
            this.data.SegmentationData.cell_frame == GT.firstFrame(i)+1 & ...
            this.data.SegmentationData.cell_index == daughter_cell_idx);
        parent_cell_trackerTrack = this.data.SegmentationData.track_index( ...
            this.data.SegmentationData.cell_frame == GT.firstFrame(parentIdx)+1 & ...
            this.data.SegmentationData.cell_index == parent_cell_idx);
        
    end
    
    divisionCounter = divisionCounter + 1;
    if daughter_cell_trackerparent==parent_cell_trackerTrack
        truthCounter = truthCounter + 1;
    else
            this.data.SegmentationData.lineageMistake( ...
            this.data.SegmentationData.cell_frame == GT.firstFrame(i)+1 & ...
            this.data.SegmentationData.cell_index == daughter_cell_idx) = 1;
    end
    
end
end
