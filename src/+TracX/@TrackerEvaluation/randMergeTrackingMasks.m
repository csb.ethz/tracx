function [ permutations ] = randMergeTrackingMasks(this, permPercentage, nConsErr)
% RANDMERGETRACKINGMASKS Randomly selects x percent tracks to be merged for
% n consecutive frames to simulate merge errors and evaluate the
% fingerprint.

% Reset seed generator such that for each evaluation the same 'random'
% selection is made to obtain comparable results and reproducibiltiy of
% figures.
rng(1)

% 
[trackArray, uIdx] = unique(this.data.getFieldArray('track_index'));
stFrames = this.data.getConditionalFieldArray('track_start_frame', 'track_index', trackArray);
trackStFrameArray = stFrames(uIdx);
stFrames = this.data.getConditionalFieldArray('track_end_frame', 'track_index', trackArray);
trackEndFrameArray = stFrames(uIdx);
validLength = (trackEndFrameArray-(nConsErr+1)) - (trackStFrameArray + 5);
selNTracks = round(numel(trackArray) * permPercentage);
fprintf('%d tracks will be randomly selected.', selNTracks);
% 
se1 = strel('sphere', 7);

selTracks = randsample(trackArray(validLength>=nConsErr), selNTracks);

permutations = cell(numel(selTracks), 9);
for k = 1:numel(selTracks)
    
   permutations{k,1} = selTracks(k);
   trackFrameArray = this.data.getFieldArrayForTrackIndex('cell_frame', selTracks(k));
   cellCenterXArray = this.data.getFieldArrayForTrackIndex('cell_center_x', selTracks(k));
   cellCenterYArray = this.data.getFieldArrayForTrackIndex('cell_center_y', selTracks(k));
   rngToChoose = 5:numel(trackFrameArray)-(nConsErr+1);
   if ~isempty(rngToChoose)
       stFrameIdx = randsample(5:numel(trackFrameArray)-(nConsErr+1), 1);
       
       framesToPerm = trackFrameArray(stFrameIdx:(stFrameIdx-1)+nConsErr);
       permutations{k,2} = [min(framesToPerm)-1; framesToPerm; max(framesToPerm)+1];
       permutations{k,3} = repmat(selTracks(k), numel(permutations{k,2}), 1);
       permutations{k,4} = cellCenterXArray(stFrameIdx-1:(stFrameIdx-1)+nConsErr+1);
       permutations{k,5} = cellCenterYArray(stFrameIdx-1:(stFrameIdx-1)+nConsErr+1);
       dist = nan(numel(framesToPerm), 1);
       for f = 1:numel(framesToPerm)
           ngbhr = this.data.SegmentationData.cell_close_neighbour(framesToPerm(f)).Indicies;
           trackOnFrameArray = this.data.getFieldArrayForFrame('track_index', framesToPerm(f));
           ngbhrArray = trackOnFrameArray(ngbhr(ismember(trackOnFrameArray, selTracks(k)), :));
           
           % For each randomly selected track we select a random neighbour to
           % be merged. We make sure to use the same merge for nConsErr times.
           if f == 1
               trackToMerge = randsample(ngbhrArray, 1);
           end
           
           mask = this.io.readSegmentationMask(fullfile(...
               this.configuration.ProjectConfiguration.segmentationResultDir, ...
               this.configuration.ProjectConfiguration. ...
               trackerResultMaskFileArray{framesToPerm(f)}));
           
           maskSel = mask;
           maskSel(~ismember(mask, [selTracks(k), trackToMerge])) = 0;
           
           maskSelD = imdilate(maskSel, se1);
           maskSelE = imerode(maskSelD, se1);
                                
           mask(logical(maskSelE)) = selTracks(k);
           
           p = regionprops(logical(maskSelE), 'Centroid');
           if size(p, 1) == 1
               dist(f) = pdist2(p.Centroid, [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
           else
               % In this case the neighbours are not in direct contact
               unmergedCoords = sum(vertcat(p.Centroid))./size(p, 1);
               dist(f) = pdist2(unmergedCoords, [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
           end
           
           % Save mask
           this.io.writeToMAT(mask, this.configuration.ProjectConfiguration. ...
               segmentationResultDir, ...
               this.configuration.ProjectConfiguration.trackerResultMaskFileArray{...
               framesToPerm(f)});
           
       end
       trackFrameArray2 = this.data.getFieldArrayForTrackIndex('cell_frame', trackToMerge);
       cellCenterXArray2 = this.data.getFieldArrayForTrackIndex('cell_center_x', trackToMerge);
       cellCenterYArray2 = this.data.getFieldArrayForTrackIndex('cell_center_y', trackToMerge);
       permutations{k,6} = repmat(trackToMerge, numel(permutations{k,2}), 1);
       xx = nan(size(repmat(trackToMerge, numel(permutations{k,2}), 1)));
       xx(ismember(permutations{k,2}, trackFrameArray2)) = cellCenterXArray2(...
           ismember(trackFrameArray2, permutations{k,2}));
       permutations{k,7} = xx;
       yy = nan(size(repmat(trackToMerge, numel(permutations{k,2}), 1)));
       yy(ismember(permutations{k,2}, trackFrameArray2)) = cellCenterYArray2(...
           ismember(trackFrameArray2, permutations{k,2}));
       permutations{k,8} = yy;
       permutations{k,9} = dist;
   else
        permutations{k,2} = nan;
   end
end

end

