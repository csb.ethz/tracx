%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ this ] = randTrackIndexPermutation( this, permutationPercentage )
%> @brief RANDTRACKINDEXPERMUTATION Randomly permutes the track indicies of
%>  x % of the data in the current project. 
%>  Used for tracker performance evaluation against a ground truth to
%>  artificially generate tracking errors / assignment errors to test the
%>  fingerprint (thresholding) method.
%>
%> Input:
%> @param this:                      [Object] TrackerEvaluation instance.
%> @param permutationPercentage: [1x1 Double] Percentage of data to remove
%>                                            [0-100].
%>
%> Output:
%> @retval this:                     [Object] Returns modified 
%>                                            TrackerEvaluation instance.
%>/*************************************************************
%> @copyright Copyright (c) 2017-19 Andreas P. Cuny, andreas.cuny@bsse.ethz.ch
%> Computational Systems Biology group, ETH Zurich
%> All rights reserved. @todo Add Licence!
%>
% Contributors:
%>    @contributors
%>    @contributor{Andreas P. Cuny - initial implementation}
%>*************************************************************/

% Handle case of no permutation -> return direclty
if permutationPercentage == 0
    return
end

trB = this.data.getFieldArray('track_index');

% Reset seed generator such that for each evaluation the same 'random'
% selection is made to obtain comparable results and reproducibiltiy of
% figures.
rng(1)

data = this.data.joinedTrackerDataTable;
uniqueFrames = unique(data.cell_frame);

% Randomly select x percent of the data to be permuted
dsPermutionIdx = randsample(size(data, 1), round((permutationPercentage/100) * ...
    size(data, 1)));
framesWithPermutation = data.cell_frame(dsPermutionIdx);
% Write permuted dataset to disk in frame by frame manner
for fn  = 1:numel(uniqueFrames)
    
    dsPermutionIdxOnFrame = dsPermutionIdx(framesWithPermutation == fn);
    trackIndexSel = data.track_index(dsPermutionIdxOnFrame);
    neighbourIdx = this.data.SegmentationData.cell_close_neighbour(fn).Indicies;
    trackIndiciesOnFrame = data.track_index(data.cell_frame == fn);
    
    for trackToModify = trackIndexSel'
        % Get track neighbourhood
        trackNeighbours = trackIndiciesOnFrame(neighbourIdx(...
            trackIndiciesOnFrame == trackToModify, :)); 
        
        % Permute only tracks which have neighbouring cells
        if ~isempty(trackNeighbours)
            % Rand select a neighbour
            selectedNeighbourIdx = randsample(size(trackNeighbours, 1), 1);
            %
            neighbourIdxToReplace = trackIndiciesOnFrame == trackNeighbours(...
                selectedNeighbourIdx);
            trackIndexIdxToReplace = trackIndiciesOnFrame == trackToModify;
            % Switch label in trackIndex array
            trackIndiciesOnFrame(neighbourIdxToReplace) = trackToModify;
            trackIndiciesOnFrame(trackIndexIdxToReplace) = trackNeighbours(...
                selectedNeighbourIdx);
        end
    end
    
    % Save permuted data of current frame back to TrackerData.
    this.data.setFieldArrayForFrame('track_index', fn, trackIndiciesOnFrame)
end

trA = this.data.getFieldArray('track_index');

end
