%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [summaryData] = evaluateTrackingResultProb(this, FPassigned, ...
    FFP, permutationPercentage, evaluationResultsDir, testSetToEvaluate)

%% Old way of getting FFP and assignments (by comparison to GT and permutation of track_assignments)

[FPassigned, fpDistAssignmentNeighbourSizePermTotalMovie, wrongAssIdxTotalMovie, ...
    corrNTotalMovie, dDTotal, dDNTotal] = this.permuteTracks(...
    permutationPercentage, evaluationResultsDir, testSetToEvaluate, true);

na = numel(FPassigned);
nn = numel(fpDistAssignmentNeighbourSizePermTotalMovie);

NIdx = nan(nn,1); % neighbor index
NN   = nan(na,1); % number neighbors
for z = 1:na
    idx = fpDistAssignmentNeighbourSizePermTotalMovie== ...
        FPassigned(z);
    NIdx(idx) = z;
    NN(z) = sum(idx);
end

if any(isnan(NIdx))
    error('Not all neighbors assigned!');
end

FPassigned = FPassigned(:);
AssCorrect = ~wrongAssIdxTotalMovie(:);
NeiCorrect = corrNTotalMovie(:);
FPneighbor = nan(na,max(NN));
MNeiCorrect = false(size(FPneighbor));
FPneighborV = zeros(nn,1);
FPassignedV = zeros(nn,1);

for z = 1:na
    fp = fpDistAssignmentNeighbourSizePermTotalMovie(NIdx==z) + dDNTotal(NIdx==z);
    FPneighbor(z,1:NN(z))  = fp;
    FPneighborV(NIdx==z) = fp';
    FPassignedV(NIdx==z) = fpDistAssignmentNeighbourSizePermTotalMovie(NIdx==z);
    MNeiCorrect(z,1:NN(z)) = NeiCorrect(NIdx==z);
end

FPDiff = FPassigned*ones(1,max(NN)) - FPneighbor;
NNFP   = sum(FPDiff>0 & ~isnan(FPDiff),2);
FFP    = NNFP./NN; % fraction of neighboring cells with lower FP distance
FFP(isnan(FFP)) = 0;


%% evaluation metrics

% [TN FP TP FN]'
fconfusion = @(PredCorrect,AssCorrect) [sum(PredCorrect & AssCorrect); ...
    sum(~PredCorrect & AssCorrect); ...
    sum(~PredCorrect & ~AssCorrect); ...
    sum(PredCorrect & ~AssCorrect)];

ftpr    = @(v) v(3)/(v(3)+v(4));
ffpr    = @(v) v(2)/(v(2)+v(1));
fppv    = @(v) v(3)/(v(3)+v(2));
ffscore = @(tpr,ppv) 2*ppv*tpr/(ppv+tpr);

% Normalized FPassigned
FPassignedN = FPassigned/max(FPassigned);

n = 100;

[tpr,fpr,ppv,fscore] = deal(nan(n,1));
x = linspace(0,1,n);
for z = 1:n
   PredCorrect = FPassigned <= x(z);
   tpr(z)    = ftpr(fconfusion(PredCorrect,AssCorrect));
   fpr(z)    = ffpr(fconfusion(PredCorrect,AssCorrect));
   ppv(z)    = fppv(fconfusion(PredCorrect,AssCorrect));
   fscore(z) = ffscore(tpr(z),ppv(z));
end

idx = fscore==max(fscore);
idx = find(idx==1, 1, 'last');
if isempty(idx)
    idx = 1;
end
% fprintf('\nOptimal tau (%5.3f): TPR = %5.3f, PPV = %5.3f, F-Score = %5.3f\n', ...
%     x(idx),tpr(idx),ppv(idx),fscore(idx));
xOT = x(idx);
tprOT = tpr(idx);
ppvOT = ppv(idx);
fscoreOT = fscore(idx);

% Normalized
[tpr,fpr,ppv,fscore] = deal(nan(n,1));
x = linspace(0,1,n);
for z = 1:n
   PredCorrect = FPassignedN <= x(z);
   tpr(z)    = ftpr(fconfusion(PredCorrect,AssCorrect));
   fpr(z)    = ffpr(fconfusion(PredCorrect,AssCorrect));
   ppv(z)    = fppv(fconfusion(PredCorrect,AssCorrect));
   fscore(z) = ffscore(tpr(z),ppv(z));
end

idx = fscore==max(fscore);
idx = find(idx==1, 1, 'last');
if isempty(idx)
    idx = 1;
end
% fprintf('\nOptimal tau (%5.3f): TPR = %5.3f, PPV = %5.3f, F-Score = %5.3f\n', ...
%     x(idx),tpr(idx),ppv(idx),fscore(idx));
xOTNorm = x(idx);
tprOTNorm = tpr(idx);
ppvOTNorm = ppv(idx);
fscoreOTNorm = fscore(idx);

% Fraction
[tpr,fpr,ppv,fscore] = deal(nan(n,1));
x = linspace(0,1,n);
for z = 1:n
   PredCorrect = FFP <= x(z);
   tpr(z)    = ftpr(fconfusion(PredCorrect,AssCorrect));
   fpr(z)    = ffpr(fconfusion(PredCorrect,AssCorrect));
   ppv(z)    = fppv(fconfusion(PredCorrect,AssCorrect));
   fscore(z) = ffscore(tpr(z),ppv(z));
end

idx = fscore==max(fscore);
idx = find(idx==1, 1, 'last');
if isempty(idx)
    idx = 1;
end
xFFP = x(idx);
tprFFP = tpr(idx);
ppvFFP = ppv(idx);
fscoreFFP = fscore(idx);

testN = {'Optimal tau'; 'Optimal tau normalized'; 'Fraction neighbour lower CRF' };
parameter = [xOT; xOTNorm; xFFP];
truePR = [tprOT; tprOTNorm; tprFFP];
positivePV = [ppvOT; ppvOTNorm; ppvFFP];
fScore = [fscoreOT; fscoreOTNorm; fscoreFFP];
% Create table
summaryData = table(testN, parameter, truePR, positivePV, fScore);
summaryData.Properties.VariableNames = {'Test', 'Parameter', 'TPR', 'PPV', 'F_Score'};

end