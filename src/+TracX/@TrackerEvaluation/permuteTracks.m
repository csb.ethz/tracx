%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [track_fingerprint_real_distancePermTotalMovie, ...
    fpDistAssignmentNeighbourSizePermTotalMovie, wrongAssIdxTotalMovie, ...
    corrNTotalMovie, dDTotal, dDNTotal] = permuteTracks(this, ...
    permutationPercentage, evaluationResultsDir, testSetToEvaluate, qc)
%PERMUTETRACKS Summary of this function goes here
% Define the percentage of track label permutations
% permutationPercentage = 20;

% Reset seed generator such that for each evaluation the same 'random'
% selection is made to obtain comparable results and reproducibiltiy of
% figures.
rng(1)

%% Get all relevant data

nFrames = this.configuration.ProjectConfiguration.trackerEndFrame;

[track_index,neigh,fpDistsM] = deal(cell(nFrames,1));
for fn = 1:nFrames
   track_index{fn} = this.data.getFieldArrayForFrame('track_index', fn);
   neigh{fn}       = this.data.SegmentationData.cell_close_neighbour(fn).Indicies;
   if fn > 1
       [fpDistsM{fn} ] = this.fingerprint.computeFingerprintDistance(...
        this.data.getFieldArrayForFrame('track_fingerprint_real', fn-1), ...
        this.data.getFieldArrayForFrame('track_fingerprint_real', fn));
   end
end

nCells   = cellfun(@numel,track_index);
sumCells = sum(nCells);

% vectors: track and frame numbers
vTrack  = cell2mat(track_index);
vFrame  = deal(nan(sumCells,1));

% compile frame index vector
idx = [0;cumsum(nCells)];
for fn = 1:nFrames
    vFrame(idx(fn)+1:idx(fn+1))  = fn;
end

% display tracks, original
[trMatrixOrg,trMatrixNew] = deal(false(max(vTrack),nFrames));

trMatrixOrg(sub2ind(size(trMatrixOrg),vTrack,vFrame)) = true;

%% Select assignments to swap

nSwaps = ceil(sumCells * permutationPercentage/200);
[swapIdx1,swapIdx2] = deal(nan(nSwaps,1));
ns     = 1;

while ns <= nSwaps
    % select first cell randomly
    [~,sidx] = sort(rand(sumCells,1));
    idx1     = sidx(1);
    
    % avoid duplicates
    if ~any(swapIdx1 == idx1) && ~any(swapIdx2 == idx1)        
        fr    = vFrame(idx1);
        fidx  = find(vFrame==fr);
        
        % get random neighbor
        idx = find(neigh{fr}(vTrack(idx1) == vTrack(fidx),:));
        if ~isempty(idx)
            [~,sidx] = sort(rand(length(idx),1));
            idx2     = fidx(idx(sidx(1)));
            swapIdx1(ns) = idx1;
            swapIdx2(ns) = idx2;
%             fprintf('%i: %i: %i - %i (%i - %i)\n', ...
%                 ns,fr,idx1,idx2,vTrack(idx1),vTrack(idx2));
            ns = ns+1;
        end
    end
end

[~,sidx] = sort(vFrame(swapIdx1));
swapIdx1 = swapIdx1(sidx);
swapIdx2 = swapIdx2(sidx);
  
%% Execute swaps 

vTrackOrg = vTrack;

for zs = 1:nSwaps
    idx1   = swapIdx1(zs);
    idx2   = swapIdx2(zs);
    track1 = vTrack(idx1);
    track2 = vTrack(idx2);
    fr     = vFrame(idx1);
    
    % fprintf('%i / %i: %i,%i - %i,%i\n',zs,fr,idx1,track1,idx2,track2);
    
    % swap tracks
    tmpvTrack = vTrack;
    fidx      = vFrame >= fr;
    tmpvTrack(fidx & vTrack == track1) = track2;
    tmpvTrack(fidx & vTrack == track2) = track1;
    vTrack    = tmpvTrack;
end

% display tracks, permuted
trMatrixNew(sub2ind(size(trMatrixNew),vTrack,vFrame)) = true;

%% Map to Andreas' output variables

wrongAssIdxTotalMovie           = false(sumCells,1);
wrongAssIdxTotalMovie(swapIdx1) = true;
wrongAssIdxTotalMovie(swapIdx2) = true;

track_fingerprint_real_distancePermTotalMovie = nan(sumCells,1);
[fpDistAssignmentNeighbourSizePermTotalMovie, ...
    dDNTotal,corrNTotalMovie] = deal([]);

for z = 1:sumCells
    fr   = vFrame(z);
    % index in current frame
    idx = vTrack(vFrame == fr) == vTrack(z);
    
    if fr > 1
        % index in previous frame
        idx0 = vTrack(vFrame == fr-1) == vTrack(z);
        
        if any(idx0)
            fp = fpDistsM{fr}(idx0,idx);
            track_fingerprint_real_distancePermTotalMovie(z) = fp;
            idxn = neigh{fr}(idx,:);
            fpDistAssignmentNeighbourSizePermTotalMovie = ...
                [fpDistAssignmentNeighbourSizePermTotalMovie, fp*ones(1, ...
                sum(idxn))];
            dDNTotal = [dDNTotal, fpDistsM{fr}(idx0,idxn)-fp];
            corrNTotalMovie = [corrNTotalMovie, false(1,sum(idxn))];
        end        
    end           
end

idx = ~isnan(track_fingerprint_real_distancePermTotalMovie);
track_fingerprint_real_distancePermTotalMovie = ...
    track_fingerprint_real_distancePermTotalMovie(idx);
wrongAssIdxTotalMovie = wrongAssIdxTotalMovie(idx);
dDTotal = zeros(sum(idx),1);

if qc == true
    % Set to real black
    set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'}, ...
    {'k','k','k'})
    fh1 = figure('Renderer', 'painters', 'Position', [10 10 1550 900], ...
        'visible','off');
    clf;
    
    subplot(2,1,1)
    imagesc(double(trMatrixOrg));
    hold on
    xlabel('Frame'); ylabel('Cell ID'); title('Original');
    % display permuted assignments
    plot(vFrame(swapIdx1),vTrack(swapIdx1),'r.');
    hold off
    
    subplot(2,1,2)
    imagesc(double(trMatrixNew));
    xlabel('Frame'); ylabel('Cell ID'); title('Permuted');
    % Save figure
    print(fh1, fullfile(evaluationResultsDir, sprintf(...
        '%s_DataPermutation_ppermutedLabels_%g.png', ...
                testSetToEvaluate, permutationPercentage)),'-dpng')
end

end