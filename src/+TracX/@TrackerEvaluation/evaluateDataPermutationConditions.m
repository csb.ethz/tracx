%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [  ] = evaluateDataPermutationConditions( this, ...
    permuteNPercentOfData, thresholdRangeToTest, ...
    lowerQuantile, threshold, errorQuantification)
%> @brief EVALUATEDATAPERMUTATIOCONDITIONS Evaluates different conditions.
%> The conditions are datasets with n % of the resulting track indicies
%>  permuted for the evaluation of the fingerprint and tracker algorithm.
%>
% Input:
%>  @param this:                       [Object] TrackerEvaluation instance
%>  @param permuteNPercentOfData:  [1xn, Array] Array specifying the
%>                                              percentage of track indicies
%>                                              to be permuted [0-100].
%>  @param thresholdRangeToTest:   [1xk, Array] Range of different
%>                                              Fingerprint distance thresholds
%>                                              to be tested during the
%>                                              error cathegorization.
%>  @param lowerQuantile:         [1x1, Double] Lower quantile for
%>                                              fingerprint distance
%>                                              threshold determination
%>                                              [0-100]
%>  @param threshold:              [1xn, Array] Fingerprint distance
%>                                              thresholds which should be
%>                                              used for tracking modified
%>                                              data sets. (deprecated,
%>                                              remove)
%>  @param errorQuantification:     [1x1, Str] Error Quantification source
%>                                              'fpDistLUTTS' for fingerprint
%>                                              distance threshold evaluation.
%>                                              'pLowerFPLUTTS' for cum. prob
%>                                              that neighbour has lower fpDist.
%>
% Output:
%>  @retval none:                           Acts on TrackerEvaluation
%>                                          instance directly.
%>
%   /*************************************************************
%>  @copyright Copyright (c) 2017-19 Andreas P. Cuny, andreas.cuny@bsse.ethz.ch
%>  Computational Systems Biology group, ETH Zurich
%>  All rights reserved. @todo Add Licence!
%>
%   Contributors:
%>    @contributors
%>    @contributor{Andreas P. Cuny - initial implementation}
%   *************************************************************/

testSetToEvaluate = this.configuration.ProjectConfiguration.projectName;
pathToGroundTruth = fullfile(this.configuration.ProjectConfiguration. ...
    segmentationResultDir, 'GroundTruth\GroundTruth.csv');
evaluationResultsDir = fullfile(this.configuration.ProjectConfiguration. ...
    imageDir, 'EvaluationsNew');
dataRemovalReplicates = 1; % As random function would reset for every run
%and give the same result.

counter = 1;

% Create evaluations dir if it does not exist
if( ~exist(evaluationResultsDir, 'dir') )
    mkdir(evaluationResultsDir);
end

for n = permuteNPercentOfData
    
    this.utils.printToConsole(sprintf(['COPYING: Start copying data for', ...
        ' evaluation %d.'], n))
    
    % Create condition subdir if not existent
    if( ~exist(fullfile(evaluationResultsDir, ...
            sprintf('%dPercentDataPermuted', n)), 'dir'))
        mkdir(fullfile(evaluationResultsDir, ...
            sprintf('%dPercentDataPermuted', n)));
    end
    
    % Create replicate subdir
    for k = 1:dataRemovalReplicates
        
        if( ~exist(fullfile(evaluationResultsDir, ...
                sprintf('%dPercentDataPermuted', n), sprintf('Replicate%d', ...
                k)), 'dir'))
            mkdir(fullfile(evaluationResultsDir, ...
                sprintf('%dPercentDataPermuted', n), sprintf('Replicate%d', ...
                k)));
        end
        
        %         % Save project config
        %         this.configuration.exportTrackerConfiguration(fullfile( ...
        %             evaluationResultsDir, sprintf('%dPercentDataPermuted', n), ...
        %             sprintf('Replicate%d', k), sprintf(...
        %             '%s_Project_%s_Configuration.xml',datetime('now', 'Format', ...
        %             'yMd'), testSetToEvaluate)))
        %
        %         % Copy raw data into new folders
        %         [status(k), reason] = system(sprintf('copy "%s" "%s"',fullfile(this.configuration. ...
        %             ProjectConfiguration.segmentationResultDir), fullfile(evaluationResultsDir, ...
        %             sprintf('%dPercentDataPermuted', n), sprintf('Replicate%d', k))));
        %         if status(k) == 0
        %             this.utils.printToConsole('COPYING: Data sucesssfully copied.')
        %         else
        %             this.utils.printToConsole(sprintf('COPYING: Data copying FAILED. %s', reason))
        %         end
        
        
        %         % Remove randomly x percent of the data from the dataset and save
        %         % permuted data in same structure on a per frame basis
        %         this.randDataRemoval(fullfile(evaluationResultsDir, ...
        %             sprintf('%dPercentDataPermuted', n), sprintf('Replicate%d', k)), n)
        
        %         % Create new tracker instance to track permuted dataset. Load the
        %         % project from a config file.
        %         newTracker = TracX.Tracker();
        %         newTracker.loadTrackingProject(fullfile(...
        %             evaluationResultsDir, sprintf('%dPercentDataPermuted', n), ...
        %             sprintf('Replicate%d', k)), sprintf(...
        %             '%s_Project_%s_Configuration.xml',datetime('now', 'Format', ...
        %             'yMd'), testSetToEvaluate))
        %         % Update segmentation & tracking result directory
        %         newTracker.configuration.ProjectConfiguration.setSegmentationResultDir(fullfile(...
        %             evaluationResultsDir, sprintf('%dPercentDataPermuted', n), ...
        %             sprintf('Replicate%d', k)))
        %         newTracker.configuration.ProjectConfiguration.setTrackerResultsDirectory(fullfile(...
        %             evaluationResultsDir, sprintf('%dPercentDataPermuted', n), ...
        %             sprintf('Replicate%d', k)))
        %         newTracker.configuration.ParameterConfiguration.setFingerprintDistThreshold(threshold);
        fileIdentifierFingerprintImages = strsplit(this.configuration. ...
            ProjectConfiguration.imageFingerprintFileArray{1}, '_');
        newTracker = TracX.Tracker();
        newTracker.configuration.ParameterConfiguration = this.configuration.ParameterConfiguration;
        newTracker.createNewTrackingProject(this.configuration. ...
            ProjectConfiguration.projectName, this.configuration. ...
            ProjectConfiguration.imageDir, ...
            this.configuration.ProjectConfiguration.segmentationResultDir, ...
            fileIdentifierFingerprintImages{1}, [], [], ...
            this.configuration.ProjectConfiguration.imageCropCoordinatePriorReversionArray, ...
            this.configuration.ProjectConfiguration.nFramesToImport)
        newTracker.revertSegmentationImageCrop()  
        newTracker.runTracker()
        newTracker.saveCurrentTrackerState()
        
        % Join data of current project for track indices permutation
        if isempty(newTracker.data.joinedTrackerDataTable)
            newTracker.data.joinTrackerDataAsTable();
        end
        
        trB = newTracker.data.getFieldArray('track_index');
        % Permute randomly x percent of the track indices from the dataset
        % and save permuted data in same structure on a per frame basis
        newTracker.evaluation.randTrackIndexPermutation( n );
        
        %>@todo TEST IF PERMUTED DATA IS RETURNED AND THE REST ACTOS ON
        %THAT!
        trA = newTracker.data.getFieldArray('track_index');
        
        % Compute delta distances
        [newTracker.evaluation.deltaDistance, newTracker.evaluation.deltaDistanceNeighbours, newTracker.evaluation.fingerprintDistance, ...
            newTracker.evaluation.fingerprintDistanceNeighboursSize, newTracker.evaluation.deltaDistanceLowestNeighbour, ...
            newTracker.evaluation.deltaDistance2ndLowest, newTracker.evaluation.deltaDistance2ndLowestNeighbour, ...
            assignedFPdistTotalMovie, neighbourDeltaDistTotalMovie]...
            = newTracker.fingerprint.computeDeltaDistance(newTracker.data, ...
            newTracker.configuration.ProjectConfiguration. ...
            trackerStartFrame, newTracker.configuration. ...
            ProjectConfiguration.trackerEndFrame);
        
        pLowerFP = newTracker.fingerprint.computeNeighbourProbFPBelowAss(...
            assignedFPdistTotalMovie, neighbourDeltaDistTotalMovie, true, ...
            find(newTracker.data.SegmentationData. ...
            cell_frame == newTracker.configuration.ProjectConfiguration. ...
            trackerStartFrame, 1, 'first'), numel(newTracker.data. ...
            SegmentationData.cell_frame));
        
        newTracker.evaluation.pLowerFP = pLowerFP;
        
        % Commpute predicted fingerprint distance threshold based on lower
        % quantile of negative delta distances.
        fpDistThresholdPred = newTracker.fingerprint.computeFPDistThreshold( ...
            newTracker.evaluation.fingerprintDistance, newTracker.evaluation.fingerprintDistanceNeighboursSize, ...
            newTracker.evaluation.deltaDistance, newTracker.evaluation.deltaDistanceNeighbours, lowerQuantile);
        
        % Map test data to ground truth data
        [ newTracker.evaluation.idLUTGT, newTracker.evaluation.idLUTTS, ~, ~, newTracker.evaluation.occuranceGT, ...
            newTracker.evaluation.fpDistLUTTS, newTracker.evaluation.deltaDistLUTTS, ...
            newTracker.evaluation.pLowerFPLUTTS] = newTracker. ...
            evaluation.mapTestDataToGroundTruth(pathToGroundTruth, 0);
        
        % Cathegorize mapping results: the header is condition, replicate,
        % TP, TN, FP, FN
        %  Error matrix. FP (corr assign, above FP dist) == 1
        %                TN (corr assign, below FP dist) == 2
        %                FN (incorr assign, below FP dist) == 3
        %                TP (incorr assign, above FP dist) == 4
        % thresholdRangeToTest, FPR, TPR, FP, TN, FN, TP
        rocTmp = nan(numel(thresholdRangeToTest), 7);
        for iThreshold = 1:numel(thresholdRangeToTest)
            errorCategorisation = newTracker.evaluation.createErrorMatrix(errorQuantification,...
                thresholdRangeToTest(iThreshold));
            rocTmp(iThreshold,:) = [thresholdRangeToTest(iThreshold), errorCategorisation(1)/ ...
                (errorCategorisation(1)+errorCategorisation(2)),  errorCategorisation(4)/ ...
                (errorCategorisation(4)+errorCategorisation(3)), errorCategorisation(1), ...
                errorCategorisation(2), errorCategorisation(3), errorCategorisation(4)];
        end
        
        [~,evalSummary] = newTracker.evaluation.evaluateTrackingResultProb(evaluationResultsDir, 2, 0.1);
        
        % Save threshold to csv as well as error cathegorisation for every
        % run in the loop!
        evalSummaryTable = table();
        evalSummaryTable.condition = repmat(n, numel(thresholdRangeToTest), 1);
        evalSummaryTable.replicate = repmat(k, numel(thresholdRangeToTest), 1);
        evalSummaryTable.lowerQuantile = repmat(lowerQuantile, numel(thresholdRangeToTest), 1);
        evalSummaryTable.thresholdSetForTracking = repmat(threshold, numel(thresholdRangeToTest), 1);
        evalSummaryTable.thresholdPredicted = repmat(fpDistThresholdPred, numel(thresholdRangeToTest), 1);
        evalSummaryTable.thresholdTested = rocTmp(:,1);
        evalSummaryTable.FPR = rocTmp(:,2);
        evalSummaryTable.TPR = rocTmp(:,3);
        evalSummaryTable.FP = rocTmp(:,4);
        evalSummaryTable.TN = rocTmp(:,5);
        evalSummaryTable.FN = rocTmp(:,6);
        evalSummaryTable.TP = rocTmp(:,7);
        evalSummaryTable.optTau = evalSummary.Parameter(1);
        
        if counter == 1
            this.io.writeToXLS(evalSummaryTable, evaluationResultsDir, ...
                sprintf('ROC_AutoThreshold_EvaluationSummary_RandPermutation_eQuant_%s_%s.xls', ...
                errorQuantification, testSetToEvaluate), 'rangeStartRow', counter)
        else
            this.io.writeToXLS(evalSummaryTable, evaluationResultsDir, ...
                sprintf('ROC_AutoThreshold_EvaluationSummary_RandPermutation_eQuant_%s_%s.xls', ...
                errorQuantification, testSetToEvaluate), 'rangeStartRow', counter + 1, ...
                'writeVariableNames', false)
        end
        
        counter = counter + size(evalSummaryTable, 1);
        
    end
end

this.io.convertXLSToCSV(fullfile(evaluationResultsDir, ...
    sprintf('ROC_AutoThreshold_EvaluationSummary_RandPermutation_eQuant_%s_%s.xls', ...
    errorQuantification, testSetToEvaluate)), ',')
end
