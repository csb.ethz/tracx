%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef TrackerEvaluation < handle
    % TrackerEvaluation Handles all tracker performance evalution
    % steps.
    
    properties
        %> Tracker object
        Tracker = []
        %> Lookup table ground truth id (col) vs frames (rows) elements are
        %> test set ids.
        idLUTGT = []
        %> Lookup table test set id (col) vs frames (rows) elements are
        %> ground truth ids.
        idLUTTS = []
        %> Probability that neighbour of assignment has a lower fp dist (temp -> move to trackerdata!) 
        pLowerFP = []
        %> fpdists table for ground truth
        fpDistLUTGT = [];
        %> fpDists table for test set
        fpDistLUTTS = [];
        %> deltaDists table for test set
        deltaDistLUTTS = [];
        %> Probability that neighbour of assignment has a lower fp dist for
        %> test set
        pLowerFPLUTTS = []
        %> Ground truth ids occurance binary table
        occuranceGT = []
        %> Test set ids occurance binary table
        occuranceTS = []
        %> Error matrix. FP (corr assign, above FP dist) == 1
        %                TN (corr assign, below FP dist) == 2
        %                TN (incorr assign, below FP dist) == 3
        %                TP (incorr assign, above FP dist) == 4
        errorMatrix = [];
        % DeltaDistance of all tracked cells as the minimal FPdist - the
        % assigned FP dist.
        deltaDistance = [];
        % Fingerprint distance of all tracked cells.
        fingerprintDistance = [];
        % Fingerprint distance array resized to match all neighbours
        fingerprintDistanceNeighboursSize = [];
        % Delta distance for the second lowest fpDist value
        deltaDistance2ndLowest = [];
        % Delta distance for the neighbours
        deltaDistanceNeighbours = [];
        % Delta distance of lowest neighbour fp distance in the neighbourhood
        % (assigned cell + its direct neighbours)
        deltaDistanceLowestNeighbour = [];
        % Delta distance of second lowest fp distance in the neighbourhood
        % (assigned cell + its direct neighbours)
        deltaDistance2ndLowestNeighbour = [];   
        %> TrackerConfiguration instance with the tracking parameters and project metadata
        configuration
        %> TrackData instance stores all segmentation, quantification and lineage data.
        data
        %> IO instance implementing read/write methods
        io
        %> Utils instance implementing utility methods
        utils
        %> ImageVisualization instance implementing image visualization methods
        imageVisualization
        %> Fingerprint instance implementing cell fingerprinting methods
        fingerprint
        %> LAP instance implementing linear assignment methods
    end
    
    methods
        
        % Constructor
        %> @brief TrackerEvaluation Constructs a TrackerEvaluation object 
        %>    to evaluate the tracker performace with and without a ground 
        %>    truth.
        %>
        % Output:
        %> @retval obj:                 [Object] Returns a TrackerEvaluation 
        %>                                       objectinstance.
        function obj = TrackerEvaluation(configuration, data, io, utils, ...
                fingerprint, imageVisualization)
            obj.configuration = configuration;
            obj.data = data;
            obj.io = io;
            obj.utils = utils;
            obj.fingerprint = fingerprint;
            obj.imageVisualization = imageVisualization;
            
        end
        
        [truthCounter, division_counter] = ScoreLineageWithGT(this, GtFilePath);
        
        function createGroudTruthDataTableFromTrackingResult(this, varargin)
            
            parser = inputParser;
            parser.addParameter('invertX', false, @(x) islogical(x));
            parser.addParameter('invertY', false, @(x) islogical(x));
            
            parser.parse(varargin{:});
            invertX    = parser.Results.invertX;
            invertY    = parser.Results.invertY;
            
            evaluationResultsDir = fullfile(this.configuration. ...
                ProjectConfiguration.trackerResultsDir, 'GroundTruth');
            
            headerNames = {'Frame_number', 'Cell_number', 'Cell_colour', ...
                'Position_X', 'Position_Y'};
            
            % Sort by frame and append
            dataAllFrames = nan(numel( ...
                this.data.SegmentationData.cell_frame), 5);
            counter = 1;
            for fn = 1:numel(unique(this.data.SegmentationData.cell_frame))
                frameData = [this.data.getFieldArrayForFrame('cell_frame', fn), ...
                    this.data.getFieldArrayForFrame('track_index', fn), ...
                    zeros(numel(this.data.getFieldArrayForFrame('cell_frame', fn)), 1), ...
                    this.data.getFieldArrayForFrame('cell_center_x', fn), ...
                    this.data.getFieldArrayForFrame('cell_center_y', fn)];
                % Sort by track_index
                [~, sortIdx] = sort(frameData(:,2));
                frameData = frameData(sortIdx,:);
                dataAllFrames(counter : counter + size(frameData, 1)-1, :) ...
                    = frameData;
                counter = counter + size(frameData, 1);
            end
            
            if invertX == 1
                dataAllFrames(:,4) = this.configuration. ...
                    ProjectConfiguration.imageWidth -  dataAllFrames(:,4);
            end
            
            if invertY == 1
                dataAllFrames(:,5) = this.configuration. ...
                    ProjectConfiguration.imageHeight - dataAllFrames(:,5);
            end
            
            dataAllFramesTable = array2table(dataAllFrames, 'VariableNames',...
                headerNames);
            
            % Check if dir exists otherwise make it
            if ~exist(evaluationResultsDir, 'file')
                mkdir(evaluationResultsDir)
            end
            
            this.io.writeToCSV(dataAllFramesTable, ...
                evaluationResultsDir, 'GroundTruth', ',')
            
        end
        
        function [idLUTGT, idLUTTS, fpDistLUT, deltaDistLUT, occuranceGT, fpDistLUTTS, deltaDistLUTTS, pLowerFPLUTTS] = ...
                mapTestDataToGroundTruth(this, pathToGroundTruth, ...
                removeFacultativeTracks)
            gtImport = importdata(pathToGroundTruth);
            % Create ground truth struct
            groundTruth = struct('cell_frame', gtImport.data(:,1), ...
                'cell_id', gtImport.data(:,2), 'cell_col', ...
                gtImport.data(:,3), 'cell_center_x', gtImport.data(:,4), ...
                'cell_center_y', gtImport.data(:,5));
            
            if removeFacultativeTracks == 1
                facultativeTrackStartsIdx = ismember(groundTruth.cell_col, 1);
                groundTruth.cell_center_x(facultativeTrackStartsIdx) = [];
                groundTruth.cell_center_y(facultativeTrackStartsIdx) = [];
                groundTruth.cell_frame(facultativeTrackStartsIdx) = [];
                groundTruth.cell_id(facultativeTrackStartsIdx) = [];
            end
            %%
            if isempty(this.deltaDistance)
                
                [this.deltaDistance, this.deltaDistanceNeighbours, this.fingerprintDistance, ...
                    this.fingerprintDistanceNeighboursSize, this.deltaDistanceLowestNeighbour, ...
                    this.deltaDistance2ndLowest, this.deltaDistance2ndLowestNeighbour]...
                    = this.fingerprint.computeDeltaDistance(this.data, ...
                    this.configuration.ProjectConfiguration. ...
                    trackerStartFrame, this.configuration. ...
                    ProjectConfiguration.trackerEndFrame);
            end
            testDataset = struct('cell_frame', this.data. ...
                SegmentationData.cell_frame, 'cell_id', ...
                this.data.SegmentationData.track_index, ...
                'cell_center_x', this.data.SegmentationData. ...
                cell_center_x, 'cell_center_y', this.data. ...
                SegmentationData.cell_center_y, 'track_contained_in_track', ...
                this.data.SegmentationData.track_contained_in_track, ...
                'track_fingerprint_real_distance', this.fingerprintDistance, ...
                'track_fingerprint_delta_distance', this.deltaDistance);
            % Remove track contained in other tracks
            testDataset.cell_center_x(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            testDataset.cell_center_y(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            testDataset.cell_frame(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            testDataset.cell_id(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            testDataset.track_fingerprint_real_distance(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            testDataset.track_fingerprint_delta_distance(~isnan(testDataset. ...
                track_contained_in_track)) = [];
            
            % Do actual mapping between testDataset tracking ID and
            % groundTruth cell ID
            
            idLUTGT = nan(max(unique(groundTruth.cell_frame)), ...
                max(unique(groundTruth.cell_id)));
            idLUTTS = nan(max(unique(groundTruth.cell_frame)), ...
                max(unique(testDataset.cell_id)));
            occuranceTS = nan(max(unique(groundTruth.cell_frame)), ...
                max(unique(testDataset.cell_id)));
            fpDistLUTTS = nan(numel(unique(groundTruth.cell_frame)), ...
                max(unique(testDataset.cell_id)));
            deltaDistLUTTS = nan(numel(unique(groundTruth.cell_frame)), ...
                max(unique(testDataset.cell_id)));
            pLowerFPLUTTS = nan(numel(unique(groundTruth.cell_frame)), ...
                max(unique(testDataset.cell_id)));
            fpDistLUT = nan(max(unique(groundTruth.cell_frame)), ...
                max(unique(groundTruth.cell_id)));
            deltaDistLUT = nan(max(unique(groundTruth.cell_frame)), ...
                max(unique(groundTruth.cell_id)));
            occuranceGT = zeros(max(unique(groundTruth.cell_frame)), ...
                numel(unique(groundTruth.cell_id)));
            for fn = 1:max(unique(groundTruth.cell_frame))
                gt = [groundTruth.cell_center_x(groundTruth.cell_frame == fn), ...
                    groundTruth.cell_center_y(groundTruth.cell_frame == fn)];
                td = [testDataset.cell_center_x(testDataset.cell_frame == fn), ...
                    testDataset.cell_center_y(testDataset.cell_frame == fn)];
                gtID = groundTruth.cell_id(groundTruth.cell_frame == fn);
                tsID = testDataset.cell_id(testDataset.cell_frame == fn);
                deltaDist = testDataset.track_fingerprint_delta_distance(...
                    testDataset.cell_frame == fn);
                fpDist = testDataset.track_fingerprint_real_distance(...
                    testDataset.cell_frame == fn);
                distancesRaw = pdist2(td, gt);
                distances = pdist2(td, gt) <= 10;
                for k = 1:numel(gtID)
                    lutIdx = find(gtID == gtID(k));
                    occuranceGT(fn, gtID(k)) = 1;
                    if ~isempty(lutIdx)
                        idx = find(distances(:, lutIdx)); % min(distances(:,lutIdx)));
                        if ~isempty(idx)
                            id = tsID(idx);
                            if numel(id) > 1
                                % Multiple tracks within close distance to
                                % gtID refine by actual distance
                                idLUTGT(fn, gtID(k)) = id(ismember(...
                                    distancesRaw(idx, lutIdx), ...
                                    min(distancesRaw(idx, lutIdx))));
                                fpDistLUT(fn, gtID(k)) = fpDist(ismember(...
                                    distancesRaw(idx, lutIdx), ...
                                    min(distancesRaw(idx, lutIdx))));
                                deltaDistLUT(fn, gtID(k)) = deltaDist(ismember(...
                                    distancesRaw(idx, lutIdx), ...
                                    min(distancesRaw(idx, lutIdx))));
                            else
                                idLUTGT(fn, gtID(k)) = id;
                                fpDistLUT(fn, gtID(k)) = fpDist(idx);
                                deltaDistLUT(fn, gtID(k)) = deltaDist(idx);
                            end
                        end
                    end
                end
            end
            for fn = 1:max(unique(groundTruth.cell_frame))
                tsID = testDataset.cell_id(testDataset.cell_frame == fn);
                fpDist = testDataset.track_fingerprint_real_distance(...
                    testDataset.cell_frame == fn);
                deltaDist = testDataset.track_fingerprint_delta_distance(...
                    testDataset.cell_frame == fn);
%                 pLowerFP = this.pLowerFP(testDataset.cell_frame == fn);
                for l = 1:numel(tsID)
                    fpDistLUTTS(fn, tsID(l)) = fpDist(l);
                    deltaDistLUTTS(fn, tsID(l)) = deltaDist(l);
%                     pLowerFPLUTTS(fn, tsID(l))  = pLowerFP(l);
                    
                    if ~isempty(find(idLUTGT(fn,:) == tsID(l), 1))
                        %--------------------------
                        % Dirty fix needed if multiple outputs are given!
                        if numel(find(idLUTGT(fn,:) == tsID(l))) > 1
                            res = find(idLUTGT(fn,:) == tsID(l));
                            idLUTTS(fn, tsID(l)) = res(1);
                            occuranceTS(fn, tsID(l)) = occuranceGT(fn, res(1));
                        else
                            idLUTTS(fn, tsID(l)) = find(idLUTGT(fn,:) == tsID(l));
                            occuranceTS(fn, tsID(l)) = occuranceGT(fn, find(idLUTGT(fn,:) == tsID(l)));
                        end
                        %-----------------------
                    else
                        %                        % idLUTTS{fn, tsID(l)} = nan;
                    end
                    
                end
            end
            this.fpDistLUTTS = fpDistLUTTS;
            this.deltaDistLUTTS = deltaDistLUTTS;
            this.idLUTTS = idLUTTS;
            this.idLUTGT = idLUTGT;
            this.occuranceGT = occuranceGT;
            this.occuranceTS = occuranceTS;
            
            
        end
        
                
        function errors = createErrorMatrix(this, errorSource, threshold)
            this.errorMatrix = nan(size(this.idLUTTS));
            for fn = 1:size(this.idLUTTS, 1)-1
                for el = 1:size(this.idLUTTS, 2)
                    difference = this.idLUTTS(fn, el) - this.idLUTTS(fn+1, el);
                    if ~isnan(difference)
                        if strcmp(errorSource,'deltaDistLUTTS')
                            if (difference) == 0 && this.(errorSource)(fn, el) >= threshold
                                this.errorMatrix(fn, el) = 1;
                            elseif (difference) == 0 && this.(errorSource)(fn, el) < threshold
                                this.errorMatrix(fn, el) = 2;
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) < threshold
                                this.errorMatrix(fn, el) = 3;
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) >= threshold
                                this.errorMatrix(fn, el) = 4;
                            end
                        elseif strcmp(errorSource, 'fpDistLUTTS')
                            % Above or equal threshold & track correct
                            if (difference) == 0 && this.(errorSource)(fn, el) >= threshold
                                this.errorMatrix(fn, el) = 1;
                            % Below threshold & track correct
                            elseif (difference) == 0 && this.(errorSource)(fn, el) < threshold
                                this.errorMatrix(fn, el) = 2;
                            % Below threshold & track incorrect
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) < threshold
                                this.errorMatrix(fn, el) = 3;
                            % Above or equal threshold & track incorrect
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) >= threshold
                                this.errorMatrix(fn, el) = 4;
                            end
                        elseif strcmp(errorSource, 'pLowerFPLUTTS')
                            %  Error matrix. FP (corr assign, above FP dist) == 1
                            %                TN (corr assign, below FP dist) == 2
                            %                FN (incorr assign, below FP dist) == 3
                            %                TP (incorr assign, above FP dist) == 4
                            % Above threshold & track correct
                            if (difference) == 0 && this.(errorSource)(fn, el) > threshold
                                this.errorMatrix(fn, el) = 1;
                                % Below or equal threshold & track correct
                            elseif (difference) == 0 && this.(errorSource)(fn, el) <= threshold
                                this.errorMatrix(fn, el) = 2;
                                % Below or equal threshold & track incorrect
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) <= threshold
                                this.errorMatrix(fn, el) = 3;
                                % Above threshold & track incorrect
                            elseif (difference) ~= 0 && this.(errorSource)(fn, el) > threshold
                                this.errorMatrix(fn, el) = 4;
                            end
                            
                        end
                    end
                end
            end
            % Error matrix. FP (corr assign, above FP dist) == 1
            %               TN (corr assign, below FP dist) == 2
            %               FN (incorr assign, below FP dist) == 3
            %               TP (incorr assign, above FP dist) == 4
            errors = [numel(find(this.errorMatrix == 1)), ...
                numel(find(this.errorMatrix == 2)), ...
                numel(find(this.errorMatrix == 3)), ...
                numel(find(this.errorMatrix == 4))];
            
            sanityFigure = 0;
            if sanityFigure == 1
                
                yDeltaDistToCompare = this.deltaDistanceNeighbours';
                predictionITolerance = 0.99;
                xFPDistAssignedToCompare = this.fingerprintDistanceNeighboursSize';
                [~, yci] = predict(fitlm(xFPDistAssignedToCompare, yDeltaDistToCompare), xFPDistAssignedToCompare, ...
                    'Prediction', 'observation', 'alpha', 1-predictionITolerance);
                lowerPredIFit = fitlm(xFPDistAssignedToCompare, yci(:,1));
                fpDistThreshold = -lowerPredIFit.Coefficients{1,1}/lowerPredIFit. ...
                    Coefficients{2, 1};
                
                [row,col] = find(this.errorMatrix == 3);
                falseNegX = this.fpDistLUTTS(sub2ind(size(this.errorMatrix), row, col));
                falseNegY = this.deltaDistLUTTS(sub2ind(size(this.errorMatrix), row, col));
                xFPDistAssigned = this.fingerprintDistance;
                yDeltaDistAssigned = this.deltaDistance;
                
                figure(1)
                set(gcf,'color','w');
                clf
                
                ah1 = subplot(2, 2,  4);
                h1 = histogram(yDeltaDistToCompare, 'Orientation','horizontal');
                h1.Normalization = 'probability';
                h1.BinWidth = 0.05;
                h1.FaceColor = [253/255, 190/255, 60/255];
                xlabel('Counts')
                
                ah2 = subplot(2, 2, 1);
                h2 = histogram(xFPDistAssigned);
                h2.Normalization = 'probability';
                h2.BinWidth = 0.05;
                h2.FaceColor = [34/255, 152/255, 140/255];
                ylabel('Counts')
                title(sprintf('FPdist threshold %f for a %d%% prediction inverval', ...
                    fpDistThreshold, predictionITolerance*100))
                
                subplot(2, 2, 3);
                sh1 = scatter(xFPDistAssigned, yDeltaDistAssigned, 35, [34/255, 152/255, 140/255], ...
                    'filled');
                sh1.MarkerFaceAlpha = 0.2;
                sh1.MarkerEdgeAlpha = 0.2;
                hold on
                sh2 = scatter(xFPDistAssignedToCompare, yDeltaDistToCompare, 35, [253/255, 190/255, 60/255], ...
                    'filled');
                sh2.MarkerFaceAlpha = 0.2;
                sh2.MarkerEdgeAlpha = 0.2;
                ha = annotation('textarrow', 'String','Threshold');
                ha.Parent = gca;
                ha.X = [fpDistThreshold fpDistThreshold];
                ha.Y = [round(min([yDeltaDistToCompare; yDeltaDistAssigned]), 1)/2 0];
                hold on
                sh3 = scatter(falseNegX, falseNegY, 35, 'red', ...
                    'filled');
                sh3.MarkerFaceAlpha = 1;
                sh3.MarkerEdgeAlpha = 1;
                %                 hold on
                %                 line([fpDistThreshold, fpDistThreshold], [-2,3], 'LineStyle', '-', 'Color', [70/255,130/255,180/255])
                %                 hold on
                %                 line([min(xFPDistAssignedToCompare), max(xFPDistAssignedToCompare)], [max(yci(:,1)), min(yci(:,1))], ...
                %                     'LineStyle', '--', 'Color', [0.35, 0.35, 0.35])
                %                 hold on
                %                 line([min(xFPDistAssignedToCompare), max(xFPDistAssignedToCompare)], [max(yci(:,2)), min(yci(:,2))], ...
                %                     'LineStyle', '-.', 'Color', [0.35, 0.35, 0.35])
                xlabel('Fingerprint distance [a.u.]')
                ylabel('Delta distance [a.u.]')
                ah1.Box = 'off';
                ah1.YAxis.Visible = 'off';
                ah2.Box = 'off';
                ah2.XAxis.Visible = 'off';
                hFig    = gcf;
                hAxes   = findobj(allchild(hFig), 'flat', 'Type', 'axes');
                set(hAxes(3), 'Position', [0.7128, 0.0718, 0.2533, 0.5821]);
                set(hAxes(1), 'Position', [0.0542, 0.0687, 0.6398, 0.5845]);
                set(hAxes(2), 'Position', [0.0542, 0.68, 0.6398, 0.29]);
            end
        end
        
        function [correctness, correctnessAllFrames] = evaluateTrackCorrectness(this)
            
            correctness = nan(1, size(this.idLUTGT, 2));
            correctnessAllFrames = nan(1, size(this.idLUTGT, 2));
            % 1 != Correct
            % 2 != Correct but split into track fragments
            % 3 != Incorrect, not tracked or track jumps
            for c = 1:size(this.idLUTGT, 2)
                gtRIdx = this.occuranceGT(:, c);
                gtRIdx(isnan(gtRIdx)) = 0;
                gtTrackIDs = this.idLUTGT(logical(gtRIdx), c);
                uniqueGTTrackIDs = unique(gtTrackIDs);
                gtTrackIDs(isnan(gtTrackIDs)) = 0;
                uniqueGTTrackIDsTmp = uniqueGTTrackIDs;
                uniqueGTTrackIDsTmp(isnan(uniqueGTTrackIDs)) = 0;
                uniqueGTTrackIDsTmp = sort(uniqueGTTrackIDsTmp);
                uniqueGTTrackIDsOcc = hist(gtTrackIDs, uniqueGTTrackIDsTmp);
                uniqueGTTrackIDsOcc = uniqueGTTrackIDsOcc(uniqueGTTrackIDsTmp ~= 0);
                
                uniqueGTTrackIDs(isnan(uniqueGTTrackIDs)) = [];
                if numel(uniqueGTTrackIDs) == 1 && ~isnan(uniqueGTTrackIDs)
                    correctness(1, c) = 1;
                    correctnessAllFrames(1, c) = uniqueGTTrackIDsOcc/sum(gtRIdx);
                elseif numel(uniqueGTTrackIDs) == 1 && isnan(uniqueGTTrackIDs)
                    correctness(1, c) = 3;
                    correctnessAllFrames(1, c) = 0/sum(gtRIdx);
                elseif  numel(uniqueGTTrackIDs) > 1
                    columns = zeros(numel(uniqueGTTrackIDs), 1);
                    for ti = 1:numel(uniqueGTTrackIDs)
                        [~, co] = find(this.idLUTGT == uniqueGTTrackIDs(1));
                        columns(ti) = numel(unique(co));
                    end
                    if all(columns == 1)
                        correctness(1, c) = 2;
                        correctnessAllFrames(1, c) = sum(uniqueGTTrackIDsOcc./sum(gtRIdx));
                    else
                        correctness(1, c) = 3;
                        correctnessAllFrames(1, c) = sum(uniqueGTTrackIDsOcc./sum(gtRIdx));
                    end
                end
            end
            
        end
        
        %> @brief RANDDATAREMOVAL Randomly removes x % of the data in the current
        %>  project. Used for tracker performance evaluation against a ground truth.
        [ ] = randDataRemoval( this, path, removalPercentage )
           
        %> @brief RANDTRACKINDEXPERMUTATION Randomly permutes the track 
        %>  indicies of x % of the data in the current project. Used for 
        %>  tracker performance evaluation against a ground truth to
        %>  artificially generate tracking errors / assignment errors to 
        %>  test the fingerprint (thresholding) method.
        [ trackIndexSel ] = randTrackIndexPermutation( this, permutationPercentage )
        
        %> @brief EVALUATEDATAREMOVALCONDITIONS Evaluates different conditions. 
        %>  The conditions are datasets with n % of its overall data removed 
        %>  for the evaluation of the fingerprint and tracker algorithm.
        [  ] = evaluateDataRemovalConditions( this, removeNPercentOfData, ...
            dataRemovalReplicates, confidenceIntervalPrediction, ...
            threshold, thresholdRangeToTest, testSetToEvaluate)
        
        [ ] = evaluateFingerprint( this, lowerQuantile )
        
        %> @brief EVALUATEFINGERPRINTDISTTHRESHOLDNORMALITY Tests if the data used
        %> for the fingerpint distance threshold determination are distributed
        %> normally using the one-sample Kolmogorov-Smirnov test.
        [  ] = evaluateFingerprintDistThresholdNormality(this, figurePath )
        
        [ ] = exportForEPEvaluation( this, varargin )
        
        %> @brief CREATEGROUDTRUTHDATATABLEFROMLABELEDMASKS Creates a ground truth
        %>  table in the csv format form labeled mask files which form a ground truth.
        %>  The columns are 'Frame_number', number of the mask frame, 'Cell_number',
        %>  Unique idnetifier of a cell 'Cell_colour', 0 if a mandatory ground truth
        %>  point, 1 if the presence of the cell is facultative, 'Position_x' and
        %>  'Position_y' describe the position of the cell on the labeled mask in
        %>  respct to the size of the labeled mask file.
        [  ] = createGroudTruthDataTableFromLabeledMasks(this, imageFilePath, ...
            fileNameIdenfifier, varargin)
        
        [ ret ] = randMergeTrackingMasks(this, permPercentage, nConsErr)
        % RANDMERGETRACKINGMASKS Randomly selects x percent tracks to be merged for
        % n consecutive frames to simulate merge errors and evaluate the
        % fingerprint.
        
        [ ret ] = randSplitTrackingMasks(this, permPercentage, nConsErr, isUseBoth)
        % RANDSPLITETRACKINGMASKS Randomly selects x percent tracks to be split for
        % n consecutive frames to simulate split errors and evaluate the
        % fingerprint.
        
        
        [ ] = computeRotatedDCRF(this, selectedFrameArray, expName, ...
            outDir, varargin)
        % COMPUTEROTATEDDCRF Computes the cell region fingerprint distance for each
        % cell rotated by an angle in degree (-90 : +90).
        
        [track_fingerprint_real_distancePermTotalMovie, ...
            fpDistAssignmentNeighbourSizePermTotalMovie, wrongAssIdxTotalMovie, ...
            corrNTotalMovie, dDTotal, dDNTotal] = permuteTracks(this, ...
            permutationPercentage, evaluationResultsDir, testSetToEvaluate, ...
            qc)
                
        [summaryData] = evaluateTrackingResultProb(this, FPassigned, ...
            FFP, permutationPercentage, evaluationResultsDir, testSetToEvaluate)
        
        printTrackerResultsForCellTrackingChallenge(this)
        
        inspectNewRoots(this,varargin)
        
        createMasksForChallenge(this);
    end
    
end

