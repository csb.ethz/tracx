function [ permutations ] = randSplitTrackingMasks(this, permPercentage, ...
    nConsErr, isUseBoth)
% RANDSPLITTRACKINGMASKS Randomly selects x percent tracks to be merged for
% n consecutive frames to simulate merge errors and evaluate the
% fingerprint.

% Reset seed generator such that for each evaluation the same 'random'
% selection is made to obtain comparable results and reproducibiltiy of
% figures.
rng(1)

%
[trackArray, uIdx] = unique(this.data.getFieldArray('track_index'));
stFrames = this.data.getConditionalFieldArray('track_start_frame', 'track_index', trackArray);
trackStFrameArray = stFrames(uIdx);
stFrames = this.data.getConditionalFieldArray('track_end_frame', 'track_index', trackArray);
trackEndFrameArray = stFrames(uIdx);
validLength = (trackEndFrameArray-(nConsErr+1)) - (trackStFrameArray + 5);
selNTracks = round(numel(trackArray) * permPercentage);
fprintf('%d tracks will be randomly selected.', selNTracks);

newLabelIds = max(trackArray) + 100; % Use new label ids with some safety marging.
%
selTracks = randsample(trackArray(validLength>=nConsErr), selNTracks);

permutations = cell(numel(selTracks), 9);
for k = 1:numel(selTracks)
    
    permutations{k,1} = selTracks(k);
    trackFrameArray = this.data.getFieldArrayForTrackIndex('cell_frame', selTracks(k));
    cellCenterXArray = this.data.getFieldArrayForTrackIndex('cell_center_x', selTracks(k));
    cellCenterYArray = this.data.getFieldArrayForTrackIndex('cell_center_y', selTracks(k));
    rngToChoose = 5:numel(trackFrameArray)-(nConsErr+1);
    if ~isempty(rngToChoose)
        stFrameIdx = randsample(5:numel(trackFrameArray)-(nConsErr+1), 1);
        
        framesToPerm = trackFrameArray(stFrameIdx:(stFrameIdx-1)+nConsErr);
        permutations{k,2} = [min(framesToPerm)-1; framesToPerm; max(framesToPerm)+1];
        permutations{k,3} = repmat(selTracks(k), numel(permutations{k,2}), 1);
        permutations{k,4} = cellCenterXArray(stFrameIdx-1:(stFrameIdx-1)+nConsErr+1);
        permutations{k,5} = cellCenterYArray(stFrameIdx-1:(stFrameIdx-1)+nConsErr+1);
        dist = nan(numel(framesToPerm), 1);
        for f = 1:numel(framesToPerm)
            
            mask = this.io.readSegmentationMask(fullfile(...
                this.configuration.ProjectConfiguration.segmentationResultDir, ...
                this.configuration.ProjectConfiguration. ...
                trackerResultMaskFileArray{framesToPerm(f)}));
            
            maskSel = mask;
            maskOut = mask;
            maskSel(~ismember(mask, selTracks(k))) = 0;
            
            skelMask = bwmorph(maskSel, 'skel',inf);
            endPointsMask = bwmorph(skelMask, 'endpoints');
            branchPointMask = bwmorph(skelMask, 'branchpoints');
            
            idxE  = find(endPointsMask);
            [yE, xE] = ind2sub(size(endPointsMask), idxE);
            idxB  = find(branchPointMask);
            [yB, xB] = ind2sub(size(branchPointMask), idxB);
            
            % Remove too close ends
            [~, distB] = dsearchn([yB, xB], [yE, xE]);
            yE = yE(distB>1);
            xE = xE(distB>1);
            % Get closest branch for each remaining end
            [closeB, ~] = dsearchn([yB, xB], [yE, xE]);
            
            d= 10;
            A3x = [];
            A3y = [];
            skelMask2 = skelMask;
            fh = figure;
            imagesc(skelMask2)
            for j = 1:numel(yE)
                distance = sqrt((xB(closeB(j)) - xE(j))^2 + (yB(closeB(j)) - yE(j))^2);
                
                dx = xE(j) - xB(closeB(j));
                dy = yE(j) - yB(closeB(j));
                coef = (distance + d) / distance;
                
                A3x(j) = xB(closeB(j)) + dx * coef;
                A3y(j) = yB(closeB(j)) + dy *coef;
                
                hLine = images.roi.Line('Parent', gca, 'Position', ...
                    [xB(closeB(j)),yB(closeB(j)); A3x(j), A3y(j)]);
                singleLineBinaryImage = hLine.createMask();
                skelMask2 = skelMask2 | singleLineBinaryImage;
            end
            close(fh)
            
            newMask = logical(maskSel)-skelMask2;
            newMask(newMask < 0) = 0;
            newMaskL = bwlabel(newMask,4);
            %
            uLabels = unique(newMaskL);
            uLabels(1) = [];
            
            % Here we introduce the final split by selecting 2 labels to be merge after
            % the split
            nLabelsToMerge = 2;
            maskSplit = ismember(newMaskL, uLabels(1:nLabelsToMerge));
            maskSplitRemainder = ismember(newMaskL, uLabels(nLabelsToMerge+1:end));
            
            se1 = strel('sphere', 5);
            maskSplitN = imdilate(maskSplit, se1);
            maskSplitN = imerode(maskSplitN, se1);
            maskSplitN = bwmorph(maskSplitN, 'clean');
            maskSplitN = imerode(maskSplitN, strel('sphere', 1));
       
            % Remove the initial segmentation
            maskOut(logical(maskSel) == 1) = 0;
            % Add the split segmentation (~half of initial) and set to
            % initial label
            maskOut(logical(maskSplitN) == 1) = selTracks(k);
            if isUseBoth == 1
                maskSplitRemainderN = imdilate(maskSplitRemainder, se1);
                maskSplitRemainderN = imerode(maskSplitRemainderN, se1);
                maskSplitRemainderN = bwmorph(maskSplitRemainderN, 'clean');
                maskSplitRemainderN = imerode(maskSplitRemainderN, strel('sphere', 1));
                maskOut(logical(maskSplitRemainderN) == 1) = newLabelIds;
            end
            % figure; imshowpair(mask, maskSplitN)
            
            p = regionprops(logical(or(maskSplitN, maskSplitRemainderN)), 'Centroid');
            if size(p, 1) == 1
                dist(f) = pdist2(p.Centroid, [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
            else
                % In this case the neighbours are not in direct contact
                unmergedCoords = sum(vertcat(p.Centroid))./size(p, 1);
                distToGT = pdist2(unmergedCoords, [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
                if distToGT < 2
                    % In this case we have two splits and we report the
                    % dist of the closer one
                    distSplits = pdist2(vertcat(p.Centroid), [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
                    dist(f) = min(distSplits);
                else
                    % In this case the neighbours are not in direct contact
                    dist(f) = pdist2(unmergedCoords, [permutations{k,4}(f+1), permutations{k,5}(f+1)]);
                end
            end
            
            % Save mask
            this.io.writeToMAT(maskOut, this.configuration.ProjectConfiguration. ...
                segmentationResultDir, ...
                this.configuration.ProjectConfiguration.trackerResultMaskFileArray{...
                framesToPerm(f)});
        end
        permutations{k,9} = dist;
    end
    newLabelIds = newLabelIds + 1;
end

end

