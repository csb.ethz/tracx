%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = randDataRemoval( this, path, removalPercentage )
%> @brief RANDDATAREMOVAL Randomly removes x % of the data in the current
%>  project. Used for tracker performance evaluation against a ground truth. 
%>
%> Input:
%> @param this:                     [Object] TrackerEvaluation instance.
%> @param path:                     [String] Path to where the permuted data
%>                                           will be stored
%> @param removalPercentage:    [1x1 Double] Percentage of data to remove
%>                                           in the range [0-100].
%>
%> Output:
%> @retval none:                             Resulting files will be
%>                                           written to disk directly.
%>/*************************************************************
%> @copyright Copyright (c) 2017-19 Andreas P. Cuny, andreas.cuny@bsse.ethz.ch
%> Computational Systems Biology group, ETH Zurich
%> All rights reserved. @todo Add Licence!
%>
%> Contributors:
%>    @contributors
%>    @contributor{Andreas P. Cuny - initial implementation}
%>*************************************************************/

data = this.data.joinedTrackerDataTable;
uniqueFrames = unique(data.cell_frame);

% Remove x percent data from the whole experiment.
removalIdx = randsample(size(data, 1), round((removalPercentage/100) * ...
    size(data, 1)));
data(removalIdx, :) = [];

% Write permuted dataset to disk in frame by frame manner
for fn  = 1:numel(uniqueFrames)
    
    dataSubsetCurrentFrame = data(data.cell_frame == fn, :);
    
    this.io.writeToTXT(dataSubsetCurrentFrame, path, this.configuration. ...
        ProjectConfiguration.segmentationResultFileArray{fn}, '\t')

end
end
