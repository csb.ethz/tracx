%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function createMasksForChallenge(this)

parts = strsplit(this.configuration.ProjectConfiguration.segmentationResultDir,'_S');
outpath = [parts{1},'_RES'];

parts = strsplit(this.configuration.ProjectConfiguration.segmentationResultDir,'\Cell');
pathseg = fullfile(parts{1},'*seg*.tif');

if ~isfolder(outpath)
    mkdir(outpath);
end

files =  dir(pathseg);
filesnames = {files.name};
parfor f = 1:length(filesnames)
    filepath = files.folder;
    currentFile = fullfile(filepath,filesnames{f});
    
    [filepath,currentfilename,fileext] = fileparts(currentFile);
    timepoint = str2double(regexp(currentfilename,'(?<=seg)...', 'match'))+1;
    
    originalMask = uint16([]);
    for i = 1:length(imfinfo(currentFile))
        originalMask(:,:,i) = uint16(imread(currentFile,i));
    end
    labels = unique(originalMask);
    labels = labels(labels~=0);
    
    cellIndices = this.data.getFieldArrayForFrame('cell_index',timepoint);
    trackIndices = this.data.getFieldArrayForFrame('track_index',timepoint);
    
    [check, onFrameIdx] = ismember(labels,cellIndices);
    assert(all(check), sprintf('%d',f));
    trackedLables = trackIndices(onFrameIdx);
    
    trackedMask = uint16(zeros(size(originalMask)));
    for i = 1:length(labels)
        trackedMask(originalMask==labels(i)) = uint16(trackedLables(i));
    end
    
    outputPath = [fullfile(outpath, sprintf('mask%03d',timepoint-1)),fileext];
    this.io.writeToTIF(trackedMask,outputPath);
    fprintf('%d\n',timepoint);

end

end