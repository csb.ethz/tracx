function [ ] = computeRotatedDCRF(this, selectedFrameArray, expName, ...
    outDir, varargin)
% COMPUTEROTATEDDCRF Computes the cell region fingerprint distance for each
% cell rotated by an angle in degree (-90 : +90).
%
% Args:
%    this (:obj:`object`):                         :class:`Tracker` instance
%    selectedFrameArray (:obj:`double` Nx1):       Array with one or two frame numbers 
%    expName (:obj:`str`):                         Experiment name
%    outDir (:obj:`str`):                          Full path to result folder.
%    varargin (:obj:`str varchar`):
%
%        * **testAngles** (:obj:`int`): Test angles in degree.
%        * **isSubsequentFrameCmp** (:obj:`int`): flag if rotation is tested on same frame (0) or with subsequent one (1, requires tracking GT).
%        * **fingerprintMaxConsideredFrequencies** (:obj:`int`): Use custom fingerprintMaxConsideredFrequencies.
%        * **fingerprintHalfWindowSideLength** (:obj:`int`): Use custom fingerprintHalfWindowSideLength.
%        * **fingerprintResizeFactor** (:obj:`int`): Use custom fingerprintResizeFactor.
%
% Returns
% -------
%    void (-)                       
%                                                       Writes results
%                                                       to outDir.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultTestAngles = -90:1:90; % test angles in degree
defaultIsSubsequentFrameCmp = 0; % flag if rotation is tested on same frame (0) 
                                 % or with subsequent one (1, requires tracking GT)
defaultFingerprintMaxConsideredFrequencies = this.configuration.ParameterConfiguration. ...
    fingerprintMaxConsideredFrequencies;
defaultFingerprintHalfWindowSideLength = this.configuration.ParameterConfiguration. ...
    fingerprintHalfWindowSideLength;
defaultFingerprintResizeFactor = this.configuration.ParameterConfiguration. ...
    fingerprintResizeFactor;

p = inputParser;
p.addRequired('selectedFrameArray', @(x) validateattributes(x,{'numeric'}, {'vector'}));
p.addRequired('expName', @(x) validateattributes(x,{'char'},{'nonempty'}));
p.addRequired('outDir', @(x) validateattributes(x,{'char'},{'nonempty'}));
p.addParameter('testAngles', defaultTestAngles,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('isSubsequentFrameCmp', defaultIsSubsequentFrameCmp,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('fingerprintMaxConsideredFrequencies', defaultFingerprintMaxConsideredFrequencies,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));
p.addParameter('fingerprintHalfWindowSideLength', defaultFingerprintHalfWindowSideLength,  @(x)validateattributes(x, ...
    {'numeric'},  {'vector'}));
p.addParameter('fingerprintResizeFactor', defaultFingerprintResizeFactor,  @(x)validateattributes(x, ...
    {'numeric'},  {'scalar'}));

parse(p, selectedFrameArray, expName, outDir, varargin{:})

testAngles = p.Results.testAngles;
isSubsequentFrameCmp = p.Results.isSubsequentFrameCmp;
fingerprintMaxConsideredFrequencies = p.Results.fingerprintMaxConsideredFrequencies;
fingerprintHalfWindowSideLength = p.Results.fingerprintHalfWindowSideLength;
fingerprintResizeFactor = p.Results.fingerprintResizeFactor;

crfDeltaErr = nan(numel(testAngles), numel(fingerprintHalfWindowSideLength));
crfDelta = cell(numel(testAngles), numel(fingerprintHalfWindowSideLength));

% Read image of current frame
imgRaw = this.io.readImageToGrayScale(fullfile(this.configuration.ProjectConfiguration.imageDir, ...
    this.configuration.ProjectConfiguration.imageFingerprintFileArray{selectedFrameArray(1)}), ...
    this.configuration.ProjectConfiguration.imageCropCoordinateArray);

ngbhrCell = nan(numel(fingerprintHalfWindowSideLength), 1);
for j = 1:numel(fingerprintHalfWindowSideLength)
    cnt = 1;
    
    ret_fq = {};
    for k = 1:1 % numel(selectedFrameArray)
        [~, ret_fq{k}] = this.fingerprint.computeFingerprintForFrame(...
            selectedFrameArray(k), selectedFrameArray(k), ...
            fingerprintHalfWindowSideLength(j), ...
            fingerprintResizeFactor, ...
            fingerprintMaxConsideredFrequencies, ...
            this.configuration.ProjectConfiguration.imageDir, ...
            this.configuration.ProjectConfiguration.imageFingerprintFileArray, ...
            this.configuration.ProjectConfiguration.imageCropCoordinateArray, ...
            this.data.SegmentationData.cell_frame, ...
            this.data.SegmentationData.cell_center_x, ...
            this.data.SegmentationData.cell_center_y);
    end
    
    if isSubsequentFrameCmp == 1
        % Read image of subsequent frame
        imgRawSub = this.io.readImageToGrayScale(fullfile(this.configuration.ProjectConfiguration.imageDir, ...
            this.configuration.ProjectConfiguration.imageFingerprintFileArray{selectedFrameArray(k + isSubsequentFrameCmp)}), ...
            this.configuration.ProjectConfiguration.imageCropCoordinateArray);
    else
        imgRawSub = imgRaw;
    end
    
    for r = testAngles
        
        retFq2 = cell(1, 1);
        for nFrame = selectedFrameArray(k + isSubsequentFrameCmp)
            
            cellFrameArray = this.data.SegmentationData.cell_frame;
            cellCenterXArray = this.data.SegmentationData.cell_center_x;
            cellCenterYArray = this.data.SegmentationData.cell_center_y;
            
            % Has to be wide formated!
            cellCenterX = cellCenterXArray(cellFrameArray == nFrame);
            cellCenterY = cellCenterYArray(cellFrameArray == nFrame);
            
            fromCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'first');
            toCellIdx = find((cellFrameArray == nFrame) == 1, 1, 'last');
            toCellIdx = toCellIdx - (fromCellIdx-1);
            fromCellIdx = fromCellIdx - (fromCellIdx-1);
            
            retIdx = find(cellFrameArray == nFrame, 1, 'first');
            for mCellIdx =  fromCellIdx:toCellIdx
                % Crop image to specified region around cell of current track
                windowCropCoordinates = [cellCenterX(mCellIdx) - fingerprintHalfWindowSideLength(j), ...
                    cellCenterY(mCellIdx) - fingerprintHalfWindowSideLength(j), 2 * ...
                    fingerprintHalfWindowSideLength(j), 2 * fingerprintHalfWindowSideLength(j)];
                if numel(windowCropCoordinates) == 4
                    rotated = rotateAround(imgRawSub, cellCenterY(mCellIdx), cellCenterX(mCellIdx), r);
                    % fgure; imagesc(rotated)
                    img = this.utils.padImageCrop(rotated, windowCropCoordinates );
                    % figure; imagesc(img)
                    % img2 = this.utils.padImageCrop(imgRaw, windowCropCoordinates );
                    %figure; imagesc(img2)
                    [ ~, retFq2(retIdx) ] = this.fingerprint.computeFingerprint(...
                        img, fingerprintResizeFactor, fingerprintMaxConsideredFrequencies );
                else
                    retFq2(retIdx)  = nan;
                end
                retIdx = retIdx + 1;
            end
            startIdx = find(cellFrameArray == nFrame, 1, 'first');
            endIdx = find(cellFrameArray == nFrame, 1, 'last');
            retFq2{k}  = retFq2(startIdx:endIdx);
        end
        
        nCells = size(ret_fq{k}, 1);
        [realTrackFingerprintDistance] = ...
            this.fingerprint.computeFingerprintDistance(ret_fq{k}(1:nCells), ...
            retFq2{k}(1:nCells)');
        crfDelta{cnt, j} = min(realTrackFingerprintDistance, [], 2) - diag(realTrackFingerprintDistance);
        crfDeltaErr(cnt, j) = sum(crfDelta{cnt, j} < 0) / size(realTrackFingerprintDistance, 1);
        cnt = cnt + 1;
    end
    ngbhrCell(j) = mean(sum(this.data.getCloseCellNeighbourIdx(cellCenterX, ...
        cellCenterY, fingerprintHalfWindowSideLength(j)), 2));
    fprintf('Done with window %d\n', j)
end
%%
windows = strsplit(num2str(fingerprintHalfWindowSideLength));
T = array2table([testAngles', 1-crfDeltaErr], 'VariableNames', {'RotationAngle', windows{:}});
writetable(T, fullfile(outDir, sprintf('RotationAngleEvaluation_%s_withSub_%d.csv', ...
    expName, isSubsequentFrameCmp)))

label = zeros(6, 1);
label(1:2:end) = fingerprintHalfWindowSideLength;
label(2:2:end) = ngbhrCell;
lgndLabel = strsplit(sprintf('w = %d (px) %g (cells),', label(:)), ',');
lgndLabelT = cell2table(lgndLabel','VariableNames', {'Labels'});
% Write the table to a CSV file
writetable(lgndLabelT, fullfile(outDir, sprintf('RotationAngleEvaluationLabels_%s_withSub_%d.csv', ...
    expName, isSubsequentFrameCmp)))

end

