%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ] = exportForEPEvaluation( this, varargin )
%EXPORTFOREPEVALUATION Summary of this function goes here
%   Detailed explanation goes here

parser = inputParser;
parser.addParameter('invertX', false, @(x) islogical(x));
parser.addParameter('invertY', false, @(x) islogical(x));
parser.addParameter('fileName', '', @(x) ischar(x));

parser.parse(varargin{:});
invertX    = parser.Results.invertX;
invertY    = parser.Results.invertY;
fileName = parser.Results.fileName;

this.data.joinTrackerDataAsTable()

tmpResults = [this.data.joinedTrackerData.cell_frame, ...
    this.data.joinedTrackerData.cell_index,...
    this.data.joinedTrackerData.cell_center_x, ...
    this.data.joinedTrackerData.cell_center_y, ...
    this.data.joinedTrackerData.track_index];

if invertX == 1
    tmpResults(:,3) = this.configuration. ...
        ProjectConfiguration.imageWidth -  tmpResults(:,3);
end

if invertY == 1
    tmpResults(:,4) = this.configuration. ...
        ProjectConfiguration.imageHeight - tmpResults(:,4);
end

results = array2table(tmpResults);
header={'Frame_number', 'Cell_number', 'Position_X', 'Position_Y', ...
    'Unique_cell_number'};
results.Properties.VariableNames = header;

if ~isempty(fileName)
    this.io.writeToCSV(results,  this.configuration.ProjectConfiguration. ...
        segmentationResultDir, sprintf('TracX_%s_%s', this.configuration. ...
        ProjectConfiguration.projectName, fileName), ...
        ',')
    this.utils.printToConsole(sprintf('SAVE: %s/TracX_%s_%s', ...
        this.configuration.ProjectConfiguration. ...
        segmentationResultDir, this.configuration. ...
        ProjectConfiguration.projectName, fileName))
else
    this.io.writeToCSV(results,  this.configuration.ProjectConfiguration. ...
        segmentationResultDir, sprintf('TracX_%s', this.configuration. ...
        ProjectConfiguration.projectName), ...
        ',')
    this.utils.printToConsole(sprintf('SAVE: %s/TracX_%s', ...
        this.configuration.ProjectConfiguration. ...
        segmentationResultDir, this.configuration. ...
        ProjectConfiguration.projectName))
end

end
