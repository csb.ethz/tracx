%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [logicalA, logicalAOnlyB] = getCommonLogical(logicalA, ...
    logicalB )
% GETCOMMONLOGICAL Get common logical of the subset of the 
% logical array A and a logical array B of the size of the subset of 
% logical A.
%
% Args:
%    logicalA (:obj:`array`, Nx1):  Logical array A.
%    logicalB (:obj:`array`, Mx1):  Logical array B.
%
% Returns
% -------
%    logicalA: :obj:`array` Nx1   
%                                   Logical array including logicalB but
%                                   of size logicalA.
%    logicalAOnlyB: :obj:`array` Nx1 
%                                   Logical array of size logicalA but
%                                   only containing logicalB.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

logicalAAll = ones(length(logicalA), 1);
logicalAOnlyB = zeros(length(logicalA), 1);
logicalAAllSubset = logicalAAll(logicalA);
locIdxA = find(logicalA == 1);
logicalAAllSubsetToKeep = and(logicalAAllSubset, logicalB);
logicalA(locIdxA(~logicalAAllSubsetToKeep)) = 0;
logicalAOnlyB(locIdxA(~logicalB)) = 1;
logicalAOnlyB = logical(logicalAOnlyB);

end