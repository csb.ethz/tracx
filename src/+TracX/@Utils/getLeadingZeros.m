%>  ***********************************************************************
%>   Copyright © 2017-2024 ETH Zurich, Andreas P. Cuny, Tomas Kuendig,
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group
%>   All rights reserved. This program and the accompanying materials
%>   are made available under the terms of the BSD-3 Clause License
%>   which accompanies this distribution, and is available at
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst
%>
%>  ***********************************************************************
function nLeadingZeros = getLeadingZeros(str)
% GETLEADINGZEROS Get the number of leading zeros of a string.
%
% Args:
%    str (:obj:`str`):  String.
%
% Returns
% -------
%    nLeadingZeros: :obj:`int`  The number of leading zeros.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Extract the numeric part of the string
numStr = regexp(str, '\d+', 'match');

if isempty(numStr)
    nLeadingZeros = 0;
    return;
end

% Use regexp to match leading zeros in the numeric part
matches = regexp(numStr{1}, '^0*', 'match');

% Get the length of the matched leading zeros
if ~isempty(matches)
    nLeadingZeros = length(matches{1});
else
    nLeadingZeros = 0;
end
end