%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [bx, by, bw, bh] = getSegAreaBoundingBox(xc, yc, offset)
% GETSEGAREABOUNDINGBOX Returns the bounding box of an image area that has 
% been segmented to focus/zoom on this particular area for image
% visualization purpose.
%
% Args:
%    xc (:obj:`array` Nx1): Array of x components of segmented objects.         
%    yc (:obj:`array` Nx1): Array of y components of segmented objects. 
%    offset (:obj:`int`):   Offset from true corner points of segmented
%                           area.
%
% Returns
% -------
%    bx: :obj:`int`, 1x1
%                           X component of bounding box.  
%    by: :obj:`int`, 1x1                               
%                           Y component of bounding box.
%    bw: :obj:`int`, 1x1                             
%                           Width component of bounding box.
%    bh: :obj:`int`, 1x1                             
%                           Height component of bounding box.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

offset = offset * 4;

pts = [xc, yc];
ptsMax = max(pts);
ptsMin = min(pts);
extrp = [ptsMax; ptsMin];
p1 = reshape(extrp(:,1), [2 1]);
p2 = reshape(extrp(:,2), [1 2]);
p1 = p1(:, ones(2, 1));
p2 = p2(ones(2, 1), :);
extrp = [p1(:) p2(:)];
extrp = extrp + 0.001 * rand(size(extrp));
chull = convhulln(extrp);

node1 = chull(:, 1);
node2 = chull(:, 2);

x1 = extrp(node1, 1);
x2 = extrp(node2, 1);
y1 = extrp(node1, 2);
y2 = extrp(node2, 2);

X1 = [x1, x2]';
Y1 = [y1, y2]';

xE = unique(round(X1(1, :)));
yE = unique(round(Y1(1, :)));
height = diff(unique(round(X1(1, :))));
width = diff(unique(round(Y1(1, :))));

bx = xE(1) - offset;
by = yE(1) - offset;
bw = width + 2 * offset;
bh = height + 2 * offset;

end