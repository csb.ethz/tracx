%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ cmap ] = getColorMap(intendedSize, varargin)
% GETCOLORMAP Returns a colormap of a user defined size
% with well distingushable colors.
%
% Args:
%   intendedSize (:obj:`int`, 1x1): Size of final colormap
%   varargin (:obj:`array`, Nx3):   [Optional] :obj:`customColorMap` which
%                                   will be resized to intended size 
%                                   (color pattern will be repeated 
%                                   until final size is  reached).
%
% Returns
% -------
%   cmap: (:obj:`array` Nx3)        
%                                   Color map with user defined size based
%                                   on colors defined in colorScheme1 (repetitions).
%
% :Authors:
%    Andreas P. Cuny - initial implementation

defaultCustomColorMap = [];
defaultIndices = [];
p = inputParser;
p.addRequired('intendedSize', @(x)validateattributes(x, ...
    {'double'}, {'>', 0}, 'Rankings'));
p.addParameter('customColorMap', defaultCustomColorMap, @isnumeric)
p.addParameter('indices', defaultIndices, @isnumeric)

parse(p, intendedSize, varargin{:})

if isempty(intendedSize)
    intendedSize = 1;
end    

if ~isempty(p.Results.customColorMap)
    colorScheme = p.Results.customColorMap;
else
    colorScheme = [ ...
        31,119,180; ...
        174,199,232; ...
        255,127,14; ...
        255,187,120; ...
        44,160,44; ...
        152,223,138; ...
        214,39,40; ...
        255,152,150; ...
        148,103,189; ...
        197,176,213; ...
        188,189,34; ...
        219,219,141; ...
        23,190,207; ...
        158,218,229; ...
        140,86,75; ...
        196,156,148];
end
% Here we repeate the colors defined in the colorScheme to fit the final
% size. Colors could be permuted. Or function could be extended to other
% color shemes.
reshapedColorScheme = repmat(colorScheme, ceil(intendedSize / ...
    size(colorScheme, 1)) , 1);

cmap = zeros(intendedSize, 3);
cmap(1:intendedSize, :) = reshapedColorScheme(1:intendedSize, :);

% Rescale color map values between 0 and 1; if given as 8bit values.
if any(unique(cmap) > 1)
    cmap = cmap ./ 255;
end

if ~isempty(p.Results.indices)
    cmapT = cmap;
    cmap = zeros(max(p.Results.indices), 3);
    cmap(p.Results.indices, :) = cmapT;
end

end
