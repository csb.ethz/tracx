%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [X, ndx] = natSortNames(X, varargin)
% NATSORT Wrapper to alphanumeric / Natural-Order sort of a cell array of 
% filenames/filepaths. See TracX.ExternalDependencies.natsortfiles for 
% detailed information.
%
% Args:
%    X (:obj:`double` 1xN): Array to be sorted      
%    varargin (:obj:`str`): Additional input arguments. See 
%                           TracX.ExternalDependencies.natsortfiles 
%                           for detailed information.
%
% Returns
% -------
%    X: :obj:`array` 1xN
%                           Cell of Strings, <X> with all strings sorted into natural-order.  
%    ndx: :obj:`arrax` 1xN                               
%                           Numeric Array, such that Y = X(ndx). The same size as <X>.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

[X, ndx] = TracX.ExternalDependencies.natsortfiles(X, varargin{:});

end