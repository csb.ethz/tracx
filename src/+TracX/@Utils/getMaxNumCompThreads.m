%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ nThreads ] = getMaxNumCompThreads()
% GETMAXNUMCOMPTHREADS Returns the maximal number of compute threads.
%
% Args:
%   ignoredArg (:obj:`obj`):        :class:`+TracX.@Utils`
%
% Returns
% -------
%   ret: (:obj:`int`)        
%                                   Color map with user defined size based
%                                   on colors defined in colorScheme1 (repetitions).
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Supposed to work with > R2007b
nThreads = maxNumCompThreads;

end

