%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function printToConsole(message)
% PRINTTOCONSOLE Outputs logging/status information to the console
% with a time stamp. A static function in :class:`Utils`.
%
% Args:
%    message (:obj:`str`): Message to be printed to the console.
% Returns
% -------
%    void (:obj:`str`):      
%                         Prints message to console directly.
% Example:
%
% >>> printToConsole('My message')
% 06-Jun-2017 11:30:30:  My message
%
% :Authors:
%    Andreas P. Cuny - initial implementation

fprintf('%s:  %s\n', char(datetime('now')), message)

end