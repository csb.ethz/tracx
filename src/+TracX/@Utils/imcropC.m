%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [crop] = imcropC(image, imageCropCoordinateArray)
% IMCROPC Crop image. Custom imcrop function. Faster (~3.5-6x) 
% than native MATLAB interactive imcrop function. 
%
% Args:
%    image (:obj:`double`, NxM):                  Image matrix
%    imageCropCoordinateArray (:obj:`int`, 1x4):  Array with coordinates
%                                                 to crop an image. 
%                                                 :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`
% Returns
% -------
%    crop: (:obj:`double` NxM)
%                                                 Croped image matrix.
%
% :Authors:
%    Andreas P. Cuny - initial implementation

cS = imageCropCoordinateArray(1);
if cS < 1
    cS = 1; % clip
end
cE = imageCropCoordinateArray(1) + imageCropCoordinateArray(3);
if cE  > size(image, 2)
   cE = size(image, 2);
end
rS = imageCropCoordinateArray(2);
if rS < 1
    rS = 1; % clip
end
rE = imageCropCoordinateArray(2) + imageCropCoordinateArray(4);
if rE  > size(image, 1)
   rE = size(image, 1);
end

crop = image(rS:rE, cS:cE, :);
if isempty(crop)
    crop = [];
end

end