%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ status ] = checkToolboxStatus(this)
% CHECKTOOLBOXSTATUS Checks if all required Matlab toolboxes are installed.
% Returns 1 if all installed, 0 otherwise.
%
% Args:
%    this (:obj:`obj`):           :class:`Tracker` object     
%
% Returns
% -------
%    status: (:obj:`array` Nx3)
%                                 Returns 1 if all installed, 0 otherwise. 
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Current toolbox dependencies
%-------------------------------------------------------------------------%
% Image Processing (for i/o)
% Signal Processing Toolbox (for i.e resample)
% Statistics and Machine Learning Toolbox (for i.e pdist)
% Parallel computing toolbox (for i.e parfor)

% Optional
% Fuzzy Logic Toolbox (for i.e sigmf)
% Mapping Toolbox (for i.e changem)

requiredT = {'Image Processing Toolbox', 'Signal Processing Toolbox', ...
    'Parallel Computing Toolbox'};
isTInstalled = zeros(size(requiredT));

for k = 1:numel(requiredT)
    isTInstalled(k) = any(any(contains(struct2cell(ver), requiredT{k})));
end

if any(isTInstalled(:) == 0) == 1
   this.printToConsole('WARNING: Missing Matlab Toolbox detected. TracX might not work properly.')
   this.printToConsole('WARNING: Please install the following toolboxes:')
   this.printToConsole(sprintf('%s', strjoin(requiredT(logical(~isTInstalled)), ', ')))
   status = 0;
else 
   this.printToConsole(sprintf('CHECK: %s', strjoin(requiredT, ', ')))
   this.printToConsole('DONE: Checking required Matlab Toolbox') 
   status = 1;
end

end

