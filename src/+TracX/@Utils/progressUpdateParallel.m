%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ pattern ] = progressUpdateParallel(stepSize, workerDirName)
% UPDATEPARALLEL Wrapper around update function when ProgressBar is 
% used in parallel setup. For details see
% TracX.ExternalDependencies.matlabProgressBar
%
% Args:
%    stepSize (:obj:`int`):      The size of the progress step when the function is
%                                called. This can be used to pass the number of
%                                processed bytes when using 'Bytes' as units. If
%                                bytes are used be sure to pass only integer values.
%                                [default: stepSize = 1]
%    workerDirName (:obj:`str`): Directory where the worker aux. files will be
%                                saved. This can be specified for debug purposes
%                                or if multiple progress bars in a parallel
%                                setup would get in each other's way since all
%                                have the same file pattern and would distract
%                                each progress bar's progress state.
%                                [default: workerDirName = tempdir()]
%
% Returns
% -------
%    filePattern: (:obj:`str`)
%                                The common beginning of every file name before the
%                                unique part begins. This is an auxiliary function
%                                output which is used by the ProgressBar() class.
%                                Typically not be of interest for the user. The
%                                variable is only returned if no input arguments were
%                                passed!
%
% Example:
%
% >>> [pattern] = updateParallel(1, pwd)
%
% :Authors:
%       - J.-A. Adrian (JA) [jensalrik.adrian AT gmail.com] - initial implementation 28-Jun-2016 16:52
%       - Andreas P. Cuny - Wrapper around updateParallel for TracX

if ~nargin
    pattern = TracX.ExternalDependencies.matlabProgressBar.updateParallel(...
        stepSize, workerDirName);
else
    TracX.ExternalDependencies.matlabProgressBar.updateParallel(...
        stepSize, workerDirName);
end

end