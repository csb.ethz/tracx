%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function crop = imcrop3C(s, cuboidWindow)
% IMCROP3C Custom fast implementation on Matlabs imcrop3 to crop a 3D image.
%
% Args:
%    s (:obj:`double` NxNx3):         3D image stack
%    cuboidWindow (:obj:`array` 1x6): Crop coordinates in the form of
%                                     [x, y, z, w, h, l]
%
% Returns
% -------
%    ret: (:obj:`double` NxNx3)
%                                     Croped 3D image stack.
%
% :Authors:
%    Thomas Kuendig - initial implementation

%X, Y and Z limits stored as - [lowIndex highIndex]
xLimits = [];
yLimits = [];
zLimits = [];

% Clip upper and lower limits to the image bounds.
width  = cuboidWindow(4);
height = cuboidWindow(5);
depth  = cuboidWindow(6);
xLimits(1) = round(max(cuboidWindow(1),1));
xLimits(2) = round(min(cuboidWindow(1)+width,size(s,2)));

yLimits(1) = round(max(cuboidWindow(2),1));
yLimits(2) = round(min(cuboidWindow(2)+height,size(s,1)));

zLimits(1) = round(max(cuboidWindow(3),1));
zLimits(2) = round(min(cuboidWindow(3)+depth,size(s,3)));

% Crop the input volume
crop =  s(yLimits(1):yLimits(2), xLimits(1):xLimits(2), zLimits(1):zLimits(2),:);

end