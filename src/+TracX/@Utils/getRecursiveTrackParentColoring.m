%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ colormap ] = getRecursiveTrackParentColoring(this, root, color, ...
    colormap, trackIndex, trackParent)
% GETRECURSIVETRACKPARENTCOLORING Modifies an existing colormap such that
% track parents are colored at a distance to the input color proportional
% to the length of the track. 
%
% Args:
%    this (:obj:`object`):            :class:`Utils` instance.
%    root (:obj:`int`):               Root track index         
%    color (:obj:`int`):              Color
%    colormap (:obj:`int`, Nx3):      Color map
%    trackIndex (:obj:`array`, Nx1):  Track index array
%    trackParent (:obj:`array`, Nx1): Track parent array
%
% Returns
% -------
%    colormap: :obj:`array` Nx3
%                                     Color map  
%
% :Authors:
%    Thomas Kuending - initial implementation

if root == 0
    % for the select the root origin 0 and start them with random colors
    children = trackParent==root;
    children_tracks = unique(trackIndex(children));
        for t = children_tracks'
        colormap = this.getRecursiveTrackParentColoring(t, rand(1,3), ...
            colormap, trackIndex, trackParent);
        end
else
    % for a root track: set a goal color at the right distance
    currenttrack = trackIndex==root;
    children = trackParent==root;
    nextcolor = randomizer(color, sum(currenttrack));
    
    % interpolate the steps between the start color and the goal color
    inbetween = zeros(sum(currenttrack),3);
    for i = 1:3
        inbetween(:,i) = linspace(color(i),nextcolor(i),sum(currenttrack))';
    end
    
    % assign the color gradient to the object that belong to the track
    colormap(currenttrack,:) = inbetween;
    children_tracks = unique(trackIndex(children));
    
    for t = children_tracks'
        
        % for each child find the last cell belonging to the parent before
        % the beginning of the child
        k = find(trackIndex==t,1);
        while ~currenttrack(k)
            k = k-1;
            if k == 0
                break
            end
        end
        
        % start with child as root and the color of the last parent cell
        % before the division
        if k~=0
        colormap = this.getRecursiveTrackParentColoring(t, colormap(k,:), ...
            colormap, trackIndex, trackParent);
        end
    end
end
end

function color = randomizer(color, number)

    % generate a random color at a distance to the input color  
    % proportional to the length of the track
    rand = randn(1,3);
    rand = rand./norm(rand)/30*number;
    color = color + rand;
    
    % floor and ceil impossible colors
    color(color > 1) = 1;
    color(color < 0) = 0;
end