%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ dest ] = downloadFile(this, url, fileName)
% DOWNLOADFILE Downloads a file and returns the path.
%
% Args:
%    this (:obj:`obj`):             :class:`Tracker` object 
%    url (:obj:`str`):              URL to the file to be downloaded
%    fileName (:obj:`str`):         Name of the downloaded file.
%
% Returns
% -------
%    dest: (:obj:`str`)        
%                                   Returns the file url/location on the system.
%
% :Authors:
%       - Andreas P. Cuny - Initial implementation

% Check if the repository exists
try
    this.printToConsole('INFO: Download started');
    dest = urlwrite(url, fileName);
    this.printToConsole(sprintf('INFO: Download successfull %s', url));
catch ME
    if strcmp(ME.identifier, 'MATLAB:urlwrite:FileNotFound')
        this.printToConsole(sprintf('ERROR: %s does not exist.', url));
    else
        rethrow(ME);
    end
end
end

