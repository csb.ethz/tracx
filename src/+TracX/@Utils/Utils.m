%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef Utils
    % UTILS Defines general utility functions used for TracX.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties
    end
    
    methods
        
        % Constructor
        function obj = Utils()
            % UTILS Constructor of TracX.Utils class.
            %
            % Returns
            % -------
            %    obj: :obj:`object`
            %                         Returns a :class:`Utils` instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
        end
        
    end
    
    methods
        
        % Function signature definition. Function implementation in
        % invididual files.
        
        [ colormap ] = getRecursiveTrackParentColoring(this, root, color, ...
            colormap, track_index, track_parent)
        % GETRECURSIVETRACKPARENTCOLORING Modifies an existing colormap
        % such that track parents are colored at a distance to the input
        % color proportional to the length of the track.
        
        [ status ] = checkToolboxStatus(this)
        % CHECKTOOLBOXSTATUS Checks if all required Matlab toolboxes are installed.
        
        [ dest ] = downloadFile(this, url, fileName)
        % DOWNLOADFILE Downloads a file and returns the path.
        
    end
    
    methods (Static)
        
        % Function signature definition. Function implementation in
        % invididual files.
        
        printToConsole(message)
        % PRINTTOCONSOLE Outputs logging/status information to the console
        % with a time stamp. A static function in :class:`Utils`.
        
        printTimeLeft(taskName, actStep, totStep)
        % PRINTTIMELEFT printTimeLeft(taskName, actStep, totStep) prints the
        % remaining time in a computation when actStep steps out of totSteps
        % have been made.
        
        [ ret ] = isPointInsideEllipse(x, y, x0, y0, theta, minorAxis, ...
            majorAxis )
        % ISPOINTINSIDEELLIPSE Determines if a point P(x,y) lays inside
        % or on the ellipse boundaries given by E(x0,y0, theta, minorAxis
        % majorAxis).
        
        [ logicalA, logicalAOnlyB ] = getCommonLogical(logicalA, ...
            logicalB )
        % GETCOMMONLOGICAL Get common logical of the subset of the
        % logical array A and a logical array B of the size of the subset of
        % logical A.
        
        [ nLeadingZeros ] = getLeadingZeros(str)
        % GETLEADINGZEROS Get the number of leading zeros of a string.
        
        [ arrOut ] = insertValueAtIdx(arrIn, val, idx)
        % INSERTVALUEATIDX Inserts an array of values a at a
        % certain position within array b.
        
        [ cmap ] = getColorMap(size, varargin)
        % GETCOLORMAP Returns a colormap of a user defined size
        % with well distingushable colors.
        
        [ obj ] = progressBar(total, varargin)
        % PROGRESSBAR Wrapper around a class to provide a
        % convenient and useful progress bar
        
        [ pattern ] = progressUpdateParallel(stepSize, workerDirName)
        % UPDATEPARALLEL Wrapper around update function when
        % ProgressBar is used in parallel setup.
        
        [ bx, by, bw, bh ] = getSegAreaBoundingBox(xc, yc, offset)
        % GETSEGAREABOUNDINGBOX Returns the bounding box of an image area
        % that has been segmented to focus/zoom on this particular area for
        % image visualization purpose.
        
        [ X, ndx ] = natSortNames(X, varargin)
        % NATSORT Wrapper to alphanumeric / Natural-Order sort of a cell
        % array of filenames/filepaths.
        
        [ paddedImage ] = padImageCrop(image, imageCropCoordinateArray )
        % PADIMAGECROP creates an image crop of the 2D input image
        % (NxM) applying the crop coordinates. It fills crops at the
        % image border with zeros to always keep the same imgOut image size
        % (image padding).
        
        [ crop ] = imcropC(image, imageCropCoordinateArray )
        % IMCROPC Crop image. Custom imcrop function. Faster (~3.5-6x)
        % than native MATLAB interactive imcrop function.
        
        [ color ] = getBWLabelColor(colorMap)
        % GETBWLABELCOLOR Returns either a black or white colormap that
        % should be well readable on an given input colorMap.
        
        [ crop ] = imcrop3C(s, cuboidWindow)
        % IMCROP3C Custom fast implementation on Matlabs imcrop3 to crop a 3D image.
        
        [ nThreads ] = getMaxNumCompThreads()
        % GETMAXNUMCOMPTHREADS Returns the maximal number of compute threads.
        
        [ image ] = rgb2java(rgb)
        % RGB2JAVA Convert uint8 rgb Matlab image to Java BufferedImage.
                
    end
end