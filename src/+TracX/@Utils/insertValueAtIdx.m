%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function arrOut = insertValueAtIdx(arrIn, val, idx)
% INSERTVALUEATIDX Inserts an array of values a at a
% certain position within array b.
%
% Args:
%    arrIn (:obj:`array` 1xM):     Array where values have
%                                  to be inserted.
%    val (:obj:`array` 1xN):       Array with values to insert.
%    idx (:obj:`int` 1x1):         Index withing arrIn where.
%
% Returns
% -------
%    arrOut: (:obj:`array` 1x(M+N)) 
%                                  Final array with inserts
%
% :Authors:
%    Andreas P. Cuny - initial implementation

% Check size of arrays
if min(size(arrIn)) > 1
    sprintf(['wrong arrIn size [%dx%d], should be either',...
        '1 x M or M x 1'], size(arrIn))
    return
end
if min(size(val)) > 1
    sprintf(['wrong val size [%dx%d], should be either 1 x M', ...
        'or M x 1'], size(val))
    return
end
% Transpose array if needed
if size(arrIn,1) > size(arrIn,2)
    arrIn = arrIn';
end
if size(val,1) > size(val,2)
    val = val';
end

if idx == numel(arrIn)+1
    arrOut = [arrIn val];
else
    arrOut = [arrIn(1:idx-1) val arrIn(idx:end)];
end

end