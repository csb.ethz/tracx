%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ ret ] = isPointInsideEllipse(x, y, x0, y0, theta, minorAxis,...
    majorAxis )
% ISPOINTINSIDEELLIPSE Determines if a point P(x,y) lays inside
% or on the ellipse boundaries given by E(x0,y0, theta,
% minorAxis, majorAxis).
%
% Args:
%    x (:obj:`float`, Nx1):                x coordinate of query point.
%    y (:obj:`float`, Nx1):                y coordinate of query point.
%    x0 (:obj:`float`, 1x1):               x coordinate of ellipse center.
%    y0 (:obj:`float`, 1x1):               y coordinate of ellipse center.
%    theta (:obj:`float`, 1x1):            angle of ellipse rotaion in degrees.
%    minorAxis (:obj:`float`, 1x1):        minorAxis length of ellipse.
%    majorAxis (:obj:`float`, 1x1):        majorAxis length of ellipse.
%
% Returns
% -------
%    ret (Nx1, :obj:`float`): 
%                                 Result array. If ret < 1 := inside ellipse
%                                 else if ret == 1 := on ellpise else ret > 
%                                 1 := outside ellipse.
%
% Example:
%
% >>> printToConsole('My message')
% 06-Jun-2017 11:30:30:  My message
%
% :Authors:
%    Andreas P. Cuny - initial implementation

cosAngle = cos(deg2rad(180-theta));
sinAngle = sin(deg2rad(180-theta));

xc = x - x0;
yc = y - y0;

xct = xc * cosAngle - yc * sinAngle;
yct = xc * sinAngle + yc * cosAngle;

ret = (xct.^2/(minorAxis/2)^2) + (yct.^2/(majorAxis/2)^2);

end