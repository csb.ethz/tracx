%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function printTimeLeft(taskName, actStep, totStep)
% PRINTTIMELEFT printTimeLeft(taskName, actStep, totStep) prints the
% remaining time in a computation when actStep steps out of totSteps have
% been made. taskName is a String describing what will be computed
% A "tic" must have been called at the beginnig of the computation. This
% code must be called at the end of the step act_step (and not at the
% beginning).
% To reduce the computaton overhead, the code will only be active if
% floor(percentage) has changed in the last step (this can easy be
% removed by deleting the first 'if' condition).
%
% Note:
% 	Adopted from inital implementation from Nicolas Le Roux.
% 	https://ch.mathworks.com/matlabcentral/fileexchange/8076-ascii-progress-bar?s_tid=prof_contriblnk
%
% Args:
%    taskName (:obj:`str`, 1x1): Name of the task.
%    actStep (:obj:`int`, 1x1):  Current step of the evaluation
% 	 totStep (:obj:`int`, 1x1):  Total number of steps in this task/loop.
%
% Returns
% -------
%    void (:obj:`str`) 
%                               Prints to console directly
%
% Example:
%
% >>> tic
% >>> for i = 1:10
% >>> 	printTimeLeft('MyTask', i, 10)
% >>> end
%
% :Authors:
%       - Nicolas Le Roux [lerouxni@iro.umontreal.ca] - initial implementation July, 20th, 2005
%       - Andreas P. Cuny - Restructured code and modified design of waitbar for TracX
TracX.ExternalDependencies.printTimeLeft(taskName, actStep, totStep);

end