%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ image ] = rgb2java(rgb)
% RGB2JAVA Convert uint8 rgb Matlab image to Java BufferedImage.
%
% Args:
%   rgb (:obj:`array`, NxM):        RGB Matlab image
%
% Returns
% -------
%   image: (:obj:`array` NxM)        
%                                   Java BufferedImage
%
% Note:
% 	Adopted from original im2java2d.m (Matlab) that fails to run with
% 	parfor / parpool due to inexistent 'swing' check.
%
% :Authors:
%    Andreas P. Cuny - initial implementation
%    Aaron Ponti - initial implementation

% @todo might be removed in future releases and be added directly

import com.mathworks.toolbox.images.util.ImageFactory;

narginchk(1,1);
validateattributes(rgb,{'uint8'},{'real', 'nonsparse'}, mfilename, 'Image', 1)

if ndims(rgb)==3 && size(rgb, 3)==3
    width  = size(rgb, 2);
    height = size(rgb, 1);
    rgbImg = permute(rgb, [3 2 1]);
    
    image = ImageFactory.createInterleavedRGBImage(width, height, rgbImg(:));
else
    error(message('rgb2java:invalidImage')) 
end

end
