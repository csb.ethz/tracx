%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [obj] = progressBar(total, varargin)
% PROGRESSBAR Wrapper around a class to provide a convenient and 
% useful progress bar
%
% Args:
%    total (:obj:`int`):      The total number of iterations [default: []] matrix         
%    varargin (:obj:`array`): Title - the progress bar's title shown in front [default: 'Processing']
%                             Unit - the unit of the update process. Can either be 'Iterations' or
%                             'Bytes' [default: 'Iterations']
%                             UpdateRate - the progress bar's update rate in Hz. Defines the printing
%                             update interval [default: 5 Hz]
% Returns
% -------
%    retBin: (:obj:`object`)
%                             Returns a progressBar instance. 
% :Authors:
%       - J.-A. Adrian (JA) [jensalrik.adrian AT gmail.com] - initial implementation 28-Jun-2016 16:52
%       - Andreas P. Cuny - Wrapper around ProgressBar for TracX

% Delete potential old timers
keep = false;
idx = find(cellfun(@(x) strcmpi(x, 'KeepTimer') , varargin));
if idx
  keep = varargin{idx+1};
  varargin(idx:idx+1) = [];
end


if keep == false
    TracX.ExternalDependencies.matlabProgressBar.ProgressBar.deleteAllTimers()
end
% Delete potential temp files
idx = find(cellfun(@(x) strcmpi(x, 'WorkerDirectory') , varargin));
if idx
  workerDirectory = varargin{idx+1};
  delete(fullfile(workerDirectory, 'progbarworker_*'))
end

obj = TracX.ExternalDependencies.matlabProgressBar.ProgressBar(total, ...
    varargin{:});

end