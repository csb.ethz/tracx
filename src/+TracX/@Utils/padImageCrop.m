%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ paddedImage ] = padImageCrop(image, imageCropCoordinateArray )
% PADIMAGECROP creates an image crop of the 2D input image
% (NxM) applying the crop coordinates. It fills crops at the
% image border with zeros to always keep the same imgOut image size
% (image padding).
%
% Args:
%    image (:obj:`uint16` NxN):                 Image matrix
%    imageCropCoordinateArray (:obj:`int` 1x4): Array with coordinates to crop an image.
%                                               :class:`~+TracX.@ProjectConfiguration.ProjectConfiguration.imageCropCoordinateArray`
%
% Returns
% -------
%     paddedImage: (:obj:`uint16`, NxM)                  
%                                               Image matrix with exact
%                                               size indicated by
%                                               imageCropCoordinateArray
%                                               where as regions outside
%                                               the input images are set
%                                               to 0 but the size given
%                                               by imageCropCoordinateArray
%                                               is maintained.
% 
% :Authors:
%   Andreas P. Cuny - initial implementation

cropCoordinateArray = imageCropCoordinateArray;

if length(cropCoordinateArray) == 4
    %% 2D
    if imageCropCoordinateArray(1) == 0
        imageCropCoordinateArray(1) = 1;
    elseif imageCropCoordinateArray(1) > 0
        cropCoordinateArray(3) = imageCropCoordinateArray(3) -1;
    end
    if imageCropCoordinateArray(2) == 0
        imageCropCoordinateArray(2) = 1;
    elseif imageCropCoordinateArray(2) > 0
        cropCoordinateArray(4) = imageCropCoordinateArray(4) - 1;
    end
    
    crop = TracX.Utils.imcropC(image, cropCoordinateArray);
    
    if isempty(crop)
        % If crop is empty return
        paddedImage = [];
        return
    end
    
    % handle x
    % xLeft case
    if imageCropCoordinateArray(1) < 1 % or 0 we have to check
        xLeft = (0 - imageCropCoordinateArray(1));
    else
        xLeft = 0;
    end
    if xLeft < 0
        xLeft = abs(xLeft) + 1; % to correct for 0 not being an index.
    end
    % xRight case
    if imageCropCoordinateArray(1) + imageCropCoordinateArray(3) > size(image, 2)
        xRight = (size(image, 2) - (imageCropCoordinateArray(1) + imageCropCoordinateArray(3))) + 1;
    else
        xRight = 0;
    end
    if xRight < 0
        xRight = abs(xRight) + 1; % to correct for 0 not being an index.
    end
    % handle y
    % yTop case
    if imageCropCoordinateArray(2) < 1 % or 0 we have to check
        yTop= (0 - imageCropCoordinateArray(2));
    else
        yTop = 0;
    end
    if yTop < 0
        yTop = abs(yTop) + 1; % to correct for 0 not being an index.
    end
    % yBottom case
    if imageCropCoordinateArray(2) + imageCropCoordinateArray(4) > size(image, 1)
        yBtm = (size(image, 1) - (imageCropCoordinateArray(2) + imageCropCoordinateArray(4))) + 1;
    else
        yBtm = 0;
    end
    if yBtm < 0
        yBtm = abs(yBtm) + 1; % to correct for 0 not being an index.
    end
    
    % Pad only if needed to speed up calc
    if all([xLeft, xRight, yTop, yBtm] == 0)
        paddedImage = crop;
    else
        paddingFill = sum(sum(image))/numel(image); % faster than mean(mean(image)) or mean2(image);
        paddedImage = zeros(imageCropCoordinateArray(4), ...
            imageCropCoordinateArray(3)) + paddingFill;
        if isempty(paddedImage)
            % If crop is 1 due to zero width and height return 1
            paddedImage = 1;
            return
        end
        
        % Handle offset to end of indices sequence
        if abs(yBtm) > 0 && abs(yTop) == 0
            zeroOffsetY = 1;
        elseif  abs(yBtm) == 0 && ...
                imageCropCoordinateArray(2) + imageCropCoordinateArray(4) > size(image, 1) && ...
                (size(paddedImage, 1) - abs(yBtm)) ~= size(crop, 1)
            zeroOffsetY = -1;
        else
            zeroOffsetY = 0;
        end
        if abs(xRight) > 0 && abs(xLeft) == 0
            zeroOffsetX = 1;
        elseif abs(xRight) == 0 && ...
                imageCropCoordinateArray(1) + imageCropCoordinateArray(3) > size(image, 2) && ...
                (size(paddedImage, 2) - abs(xRight)) ~= size(crop, 2)
            zeroOffsetX = -1;
        else
            zeroOffsetX = 0;
        end
        
        paddedImage(1 + abs(yTop):(size(paddedImage, 1) - abs(yBtm)) + zeroOffsetY, ...
            1 + abs(xLeft):(size(paddedImage, 2) - abs(xRight)) + zeroOffsetX, :) = crop;
        % Inverted y:
        %     paddedImage((abs(yBtm):(size(paddedImage, 1)-abs(yTop))+zeroOffset), ...
        %         abs(xLeft):(size(paddedImage, 2)-abs(xRight))+zeroOffset) = crop;
        
        % Factor 7 slower but potentially more robust. final size is +1
        % for width and height.
        %         paddedImage = horzcat(repmat(paddingFill, size(crop, 1), abs(xLeft)), crop);
        %         paddedImage = horzcat(paddedImage, repmat(paddingFill, size(crop, 1), abs(xRight)));
        %         paddedImage = vertcat(repmat(paddingFill, abs(yTop), size(paddedImage, 2)), paddedImage);
        %         paddedImage = vertcat(paddedImage, repmat(paddingFill, abs(yBtm), size(paddedImage, 2)));
        
    end
    
    
elseif length(cropCoordinateArray) == 6
    %% 3d
    
    if imageCropCoordinateArray(1) == 0
        imageCropCoordinateArray(1) = 1;
    elseif imageCropCoordinateArray(1) > 0
        cropCoordinateArray(4) = imageCropCoordinateArray(4) -1;
    end
    if imageCropCoordinateArray(2) == 0
        imageCropCoordinateArray(2) = 1;
    elseif imageCropCoordinateArray(2) > 0
        cropCoordinateArray(5) = imageCropCoordinateArray(5) - 1;
    end
    if imageCropCoordinateArray(3) == 0
        imageCropCoordinateArray(3) = 1;
    elseif imageCropCoordinateArray(3) > 0
        cropCoordinateArray(6) = imageCropCoordinateArray(6) - 1;
    end
    
    crop = TracX.Utils.imcrop3C(image, cropCoordinateArray);
    
    if isempty(crop)
        % If crop is empty return
        paddedImage = [];
        return
    end
    
    % handle x
    % xLeft case
    if imageCropCoordinateArray(1) < 1 % or 0 we have to check
        xLeft = (0 - imageCropCoordinateArray(1));
    else
        xLeft = 0;
    end
    if xLeft < 0
        xLeft = abs(xLeft) + 1; % to correct for 0 not being an index.
    end
    % xRight case
    if imageCropCoordinateArray(1) + imageCropCoordinateArray(4) > size(image, 2)
        xRight = (size(image, 2) - (imageCropCoordinateArray(1) + imageCropCoordinateArray(4))) + 1;
    else
        xRight = 0;
    end
    if xRight < 0
        xRight = abs(xRight) + 1; % to correct for 0 not being an index.
    end
    % handle y
    % yTop case
    if imageCropCoordinateArray(2) < 1 % or 0 we have to check
        yTop= (0 - imageCropCoordinateArray(2));
    else
        yTop = 0;
    end
    if yTop < 0
        yTop = abs(yTop) + 1; % to correct for 0 not being an index.
    end
    % yBottom case
    if imageCropCoordinateArray(2) + imageCropCoordinateArray(5) > size(image, 1)
        yBtm = (size(image, 1) - (imageCropCoordinateArray(2) + imageCropCoordinateArray(5))) + 1;
    else
        yBtm = 0;
    end
    if yBtm < 0
        yBtm = abs(yBtm) + 1; % to correct for 0 not being an index.
    end
    % handle z
    % zFront case
    if imageCropCoordinateArray(3) < 1 % or 0 we have to check
        zFront= (0 - imageCropCoordinateArray(3));
    else
        zFront = 0;
    end
    if zFront < 0
        zFront = abs(zFront) + 1; % to correct for 0 not being an index.
    end
    % zBack case
    if imageCropCoordinateArray(3) + imageCropCoordinateArray(6) > size(image, 3)
        zBack = (size(image, 3) - (imageCropCoordinateArray(3) + imageCropCoordinateArray(6))) + 1;
    else
        zBack = 0;
    end
    if zBack < 0
        zBack = abs(zBack) + 1; % to correct for 0 not being an index.
    end
    
    % Pad only if needed to speed up calc
    if all([xLeft, xRight, yTop, yBtm, zFront, zBack] == 0)
        paddedImage = crop;
    else
        paddingFill = sum(image(:))/numel(image); % faster than mean(mean(image)) or mean2(image);
        paddedImage = zeros(imageCropCoordinateArray(5), ...
            imageCropCoordinateArray(4), imageCropCoordinateArray(6)) + paddingFill;
        if isempty(paddedImage)
            % If crop is 1 due to zero width and height return 1
            paddedImage = 1;
            return
        end
        
        % Handle offset to end of indices sequence
        if abs(yBtm) > 0 && abs(yTop) == 0
            zeroOffsetY = 1;
        elseif  abs(yBtm) == 0 && ...
                imageCropCoordinateArray(2) + imageCropCoordinateArray(4) > size(image, 1) && ...
                (size(paddedImage, 1) - abs(yBtm)) ~= size(crop, 1)
            zeroOffsetY = -1;
        else
            zeroOffsetY = 0;
        end
        if abs(xRight) > 0 && abs(xLeft) == 0
            zeroOffsetX = 1;
        elseif abs(xRight) == 0 && ...
                imageCropCoordinateArray(1) + imageCropCoordinateArray(3) > size(image, 2) && ...
                (size(paddedImage, 2) - abs(xRight)) ~= size(crop, 2)
            zeroOffsetX = -1;
        else
            zeroOffsetX = 0;
        end
        if abs(zBack) > 0 && abs(zFront) == 0
            zeroOffsetZ = 1;
        elseif abs(zBack) == 0 && ...
                imageCropCoordinateArray(3) + imageCropCoordinateArray(6) > size(image, 3) && ...
                (size(paddedImage, 3) - abs(zBack)) ~= size(crop, 3)
            zeroOffsetZ = -1;
        else
            zeroOffsetZ = 0;
        end
        
        paddedImage(1 + abs(yTop):(size(paddedImage, 1) - abs(yBtm)) + zeroOffsetY, ...
            1 + abs(xLeft):(size(paddedImage, 2) - abs(xRight)) + zeroOffsetX, ...
            1 + abs(zFront):(size(paddedImage, 3) - abs(zBack)) + zeroOffsetZ) = crop;
        % Inverted y:
        %     paddedImage((abs(yBtm):(size(paddedImage, 1)-abs(yTop))+zeroOffset), ...
        %         abs(xLeft):(size(paddedImage, 2)-abs(xRight))+zeroOffset) = crop;
        
        % Factor 7 slower but potentially more robust. final size is +1
        % for width and height.
        %         paddedImage = horzcat(repmat(paddingFill, size(crop, 1), abs(xLeft)), crop);
        %         paddedImage = horzcat(paddedImage, repmat(paddingFill, size(crop, 1), abs(xRight)));
        %         paddedImage = vertcat(repmat(paddingFill, abs(yTop), size(paddedImage, 2)), paddedImage);
        %         paddedImage = vertcat(paddedImage, repmat(paddingFill, abs(yBtm), size(paddedImage, 2)));
        
    end
else
    error("imageCropCoordinateArray must have 4 elements for 2D or 6 elements for 3D")
    
end