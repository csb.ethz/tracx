%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
function [ color ] = getBWLabelColor(colorMap)
% GETBWLABELCOLOR Returns either a black or white colormap that should be
% well readable on an given input colorMap.
%
% Args:
%    colorMap (:obj:`array` Nx3): Colormap         
%
% Returns
% -------
%    color: (:obj:`array` Nx3)
%                                 BW colormap 
%
% :Authors:
%    Thomas Kuendig - initial implementation

hsv = rgb2hsv(colorMap);
if all(size(hsv) == [1, 3]) % colormap of size 1
    
    if  hsv(2) < 0.5 && hsv(3) > 0.5
        color =  [1,1,1].*0;
    elseif  hsv(2) < 0.5 && hsv(3) < 0.5
        color = [1,1,1].*1;
    elseif  hsv(2) > 0.5 && hsv(3) > 0.5
        if hsv(1) > 0.5 || hsv(1) < 0.1
            color = [1,1,1].*1;
        else
            color = [1,1,1].*0;
        end
    elseif  hsv(2) > 0.5 && hsv(3) < 0.5
        color = [1,1,1].*1;
    end
    
else % Handle case of colormap with size > 1
    
    n = size(hsv, 1);
    color = zeros(n, 3);
    
    for j = 1:n
        
        if  hsv(j, 2) < 0.5 && hsv(j, 3) > 0.5
            color(j, :) =  [1,1,1].*0;
        elseif  hsv(j, 2) < 0.5 && hsv(j, 3) < 0.5
            color(j, :) = [1,1,1].*1;
        elseif  hsv(j, 2) > 0.5 && hsv(j, 3) > 0.5
            if hsv(j, 1) > 0.5 || hsv(j, 1) < 0.1
                color(j, :) = [1,1,1].*1;
            else
                color(j, :) = [1,1,1].*0;
            end
        elseif  hsv(j, 2) > 0.5 && hsv(j, 3) < 0.5
            color(j, :) = [1,1,1].*1;
        end
       
    end
end

end