%>  *********************************************************************** 
%>   Copyright © 2017-2021 ETH Zurich, Andreas P. Cuny, Tomas Kuendig, 
%>   Aaron Ponti, Joerg Stelling; D-BSSE; CSB Group 
%>   All rights reserved. This program and the accompanying materials 
%>   are made available under the terms of the BSD-3 Clause License 
%>   which accompanies this distribution, and is available at 
%>   https://gitlab.com/csb.ethz/tracx/-/blob/master/LICENSE.rst 
%>    
%>  ***********************************************************************
classdef SegmentationData < dynamicprops
    % SegmentationData Creates an object to store imported cell
    % segmentation data. It is structured based on the CellX output,
    % however other segmentation software outputs can be stored as well
    % once converted.
    %
    % :Authors:
    %    Andreas P. Cuny - initial implementation
    
    properties (SetAccess=private)
        uuid % Primary key
    end
    
    properties
        % Segmentation data
        %
        
        cell_frame
        % Image frame number of segmented cell.
        cell_index
        % Unique segmentation cell index for each frame.
        cell_center_x
        % X component of segmented cell center.
        cell_center_y
        % Y component of segmented cell center
        cell_center_z
        % Z component of segmented cell center
        cell_majoraxis
        % Major axis of segmented cell.
        cell_minoraxis
        % Minor axis of segmented cell.
        cell_orientation
        % Orientation of segmented cell.
        cell_area
        % Area of segmented cell.
        cell_volume
        % Volume of segmented cell.
        cell_perimeter
        % Perimeter of segmented cell.
        cell_eccentricity
        % Eccentricity of segmented cell.
        cell_fractionOfGoodMembranePixels
        % Fraction of good membrane pixel of segmented cell.
        cell_mem_area
        % Membrane area of segmented cell.
        cell_mem_volume
        % Membrane volume of segmented cell..
        cell_nuc_radius
        % Nucleus radius of segmented cell
        cell_nuc_area
        % Nucleus area of segmented cell.
        cell_close_neighbour
        % Array of closest cell neighbours of segmented cell.
        cell_dif_x
        % X component difference between two subsequent frames of segmented cell.
        cell_dif_y
        % Y component difference between two subsequent frames of segmented cell.
        cell_dif_z
        % Z component difference between two subsequent frames of segmented cell.
        cell_filtered_dif_x
        % Filtered X component difference between two subsequent frames of segmented cell.
        cell_filtered_dif_y
        % Filtered Y component difference between two subsequent frames of segmented cell.
        cell_filtered_dif_z
        % Filtered Z component difference between two subsequent frames of segmented cell.
        cell_pole1_x
        % Cell pole 1 x component (optionally, for rod shaped object lineage reconstruction)
        cell_pole1_y
        % Cell pole 1 y component (optionally, for rod shaped object lineage reconstruction)
        cell_pole2_x
        % Cell pole 2 x component (optionally, for rod shaped object lineage reconstruction)
        cell_pole2_y
        % Cell pole 2 y component  (optionally, for rod shaped object lineage reconstruction)
        cell_pole1_age
        % Cell pole 1 age  (optionally, for rod shaped object lineage reconstruction)
        cell_pole2_age
        % Cell pole 2 age  (optionally, for rod shaped object lineage reconstruction)
        cell_timepoint
        % Cell timpoint (3D data)
        cell_zplane
        % Cell zplane (3D data)
        
        % Tracking Data
        %
        
        track_index
        % Track index of segmented cell.
        track_index_qc
        % Track index of segmented cell after qc verification
        track_fingerprint_real
        % Track real frequency based fingerprint of segmented cell.
        track_fingerprint_real_distance
        % Track fingerprint distance of segmented cell based on track_fingerprint_real.
        track_age
        % Track age of segmented cell.
        track_parent
        % Track parent of segmented cell.
        track_parent_frame
        % Track parent frame of segmented cell.
        track_parent_prob
        % Track parent probabliity of parent assignment.
        track_parent_score
        % Track parent score of parent assignment.
        track_generation
        % Track generation of segmented cell.
        track_lineage_tree
        % Track lineage of segmented cell.
        track_cell_cycle_phase
        % Track cell cycle phase
        track_assignment_fraction
        % Track assignment fraction if there is a better alternative assignment.
        track_total_assignment_cost
        % Track total assignment costs of segmented cell.
        track_position_assignment_cost
        % Track positional assignment costs of segmented cell.
        track_area_assignment_cost
        % Track area assignment costs of segmented cell determined by penalty function.
        track_rotation_assignment_cost
        % Track rotation assignment costs of segmented cell determined by penalty function.
        track_frame_skipping_cost
        % Track frame skipping assignment costs of segmented cell determined by penalty function.
        track_start_frame
        % Track start frame number of segmented cell determined by penalty function.
        track_end_frame
        % Track end frame number of segmented cell.
        track_index_corrected
        % Track index corrected of segmented cell.
        track_contained_in_track
        % Track cointained in other track of segmented cell.
        track_has_bud
        % Track has bud indicates whether a track in a certain frame is budding
        track_budneck_total
        % Budneck total fluo signal for the given track
        cell_mask_boundary_points
        % Point on Outline of Segmentation mask for mesh
    end
    
    methods
        
        % Constructor
        function obj = SegmentationData()
            % SEGMENTATIONDATA Constructs an empty :class:`SegmentationData` 
            % object to save segmentation data as well as tracking results.
            % for a TracX project.
            %
            % Returns
            % -------
            %    obj (:obj:`object`)
            %                            :class:`SegmentationData` instance.
            %
            % :Authors:
            %    Andreas P. Cuny - initial implementation
            
            % Primary key
            obj.uuid = [];
            
            % Segmentation data
            obj.cell_frame = [];
            obj.cell_index = [];
            obj.cell_center_x = [];
            obj.cell_center_y = [];
            obj.cell_center_z = [];
            obj.cell_majoraxis = [];
            obj.cell_minoraxis = [];
            obj.cell_orientation = [];
            obj.cell_area = [];
            obj.cell_volume = [];
            obj.cell_perimeter = [];
            obj.cell_eccentricity = [];
            obj.cell_fractionOfGoodMembranePixels = [];
            obj.cell_mem_area = [];
            obj.cell_mem_volume = [];
            obj.cell_nuc_radius = [];
            obj.cell_nuc_area = [];
            obj.cell_close_neighbour =struct('Indicies', []);
            obj.cell_dif_x = [];
            obj.cell_dif_y = [];
            obj.cell_dif_z = [];
            obj.cell_filtered_dif_x = [];
            obj.cell_filtered_dif_y = [];
            obj.cell_filtered_dif_z = [];
            obj.cell_pole1_x = [];
            obj.cell_pole1_y = [];
            obj.cell_pole2_x = [];
            obj.cell_pole2_y = [];
            obj.cell_pole1_age = [];
            obj.cell_pole2_age = [];
            
            % Tracking
            obj.track_index = [];
            obj.track_index_qc = [];
            obj.track_fingerprint_real = [];
            obj.track_fingerprint_real_distance = [];
            obj.track_age = [];
            obj.track_parent = [];
            obj.track_parent_frame = [];
            obj.track_parent_prob = [];
            obj.track_parent_score = [];
            obj.track_generation = [];
            obj.track_lineage_tree = [];
            obj.track_cell_cycle_phase = [];
            obj.track_assignment_fraction = [];
            obj.track_total_assignment_cost = [];
            obj.track_position_assignment_cost = [];
            obj.track_area_assignment_cost = [];
            obj.track_rotation_assignment_cost = [];
            obj.track_frame_skipping_cost = [];
            obj.track_start_frame = [];
            obj.track_end_frame = [];
            obj.track_index_corrected = [];
            obj.track_contained_in_track = [];
            obj.track_has_bud = [];
            obj.track_budneck_total = [];
            obj.cell_timepoint = [];
            obj.cell_zplane = [];
            
            obj.cell_mask_boundary_points = {};
            
        end
        
        function this = initNewEntry(this, uuid, cell_frame, ...
                cell_index, cell_center_x,...
                cell_center_y, cell_majoraxis, cell_minoraxis, ...
                cell_orientation, cell_area, cell_volume, cell_perimeter,...
                cell_eccentricity, cell_fractionOfGoodMembranePixels, ...
                cell_mem_area, cell_mem_volume, cell_nuc_radius, cell_nuc_area,...
                cell_close_neighbour, cell_dif_x, cell_dif_y, cell_filtered_dif_x, ...
                cell_filtered_dif_y, cell_pole1_x, cell_pole1_y, ...
                cell_pole2_x, cell_pole2_y, cell_pole1_age, cell_pole2_age, ...
                track_index, track_index_qc, ...
                track_fingerprint_real, ...
                track_fingerprint_real_distance,...
                track_age, track_parent, track_parent_frame, track_parent_prob,...
                track_parent_score, track_generation, ...
                track_lineage_tree, track_cell_cycle_phase, track_assignment_fraction, ...
                track_total_assignment_cost, ...
                track_position_assignment_cost, track_area_assignment_cost, ...
                track_rotation_assignment_cost, track_frame_skipping_cost, ...
                track_start_frame, track_end_frame, ...
                track_index_corrected, track_contained_in_track, ...
                track_has_bud, track_budneck_total)
            % INITNEWENTRY Adds new segmentation data to the object.
            % Empty parameters  should be a NaN array of the same size as the
            % other parameter data arrays.
            %
            % Args:
            %    this (:obj:`object`):                          SegmentationData instance
            %    uuid (:obj:`int`):                             Primary key
            %    cell_frame (:obj:`int`):                       Image frame number
            %    cell_index (:obj:`int`):                       Unique segmentation cell
            %                                                   index for each frame
            %    cell_center_x (:obj:`int`):                    X coordinate of segmented
            %                                                   object
            %    cell_center_y (:obj:`int`):                    Y coordinate of segmented
            %                                                   object
            %    cell_majoraxis (:obj:`float`):                 Major axis
            %    cell_minoraxis(:obj:`float`):                  Minor axis
            %    cell_orientation (:obj:`float`):               Orienation
            %    cell_area(:obj:`float`):                       Area
            %    cell_volume (:obj:`float`):                    Volume
            %    cell_perimeter(:obj:`float`):                  Perimeter
            %    cell_eccentricity (:obj:`float`):              Eccentricity
            %    cell_fractionOfGoodMembranePixels (:obj:`float`): Fraction of
            %                                                   good membrane
            %                                                   pixels
            %    cell_mem_area (:obj:`float`):                  Membrane area
            %    cell_mem_volume (:obj:`float`):                Membrane volume
            %    cell_nuc_radius (:obj:`float`):                Nucleus radius
            %    cell_nuc_area (:obj:`float`):                  Nucleus area
            %    cell_close_neighbour (:obj:`array`):           Array of closest cell
            %                                                   neighbours
            %    cell_dif_x (:obj:`float`):                     Frame to frame
            %                                                   difference of X coordinate
            %    cell_dif_y (:obj:`float`):                     Frame to frame
            %                                                   difference of Y coordinate
            %    cell_filtered_dif_x (:obj:`float`):            Filtered frame to frame difference
            %                                                   difference of X coordinate
            %    cell_filtered_dif_y (:obj:`float`):            Filtered frame to frame difference
            %                                                   difference of Y coordinate
            %    cell_pole1_x (:obj:`float`):                   Cell pole1 x coordinate
            %                                                   (rod shaped objects);
            %    cell_pole1_y (:obj:`float`):                   Cell pole1 y coordinate
            %                                                   (rod shaped objects);
            %    cell_pole2_x (:obj:`float`):                   Cell pole2 x coordinate
            %                                                   (rod shaped objects);
            %    cell_pole2_y (:obj:`float`):                   Cell pole2 y coordinate
            %                                                   (rod shaped objects);
            %    cell_pole1_age (:obj:`int`):                   Cell pole1 age (rod
            %                                                   shaped objects);
            %    cell_pole2_age (:obj:`int`):                   Cell pole2 age (rod
            %                                                   shaped objects);
            %    track_index (:obj:`int`):                      Unique track index
            %    track_index_qc (:obj:`int`):                   Unique qc verified
            %                                                   track index
            %    track_fingerprint_real (:obj:`array`):         Track fingerprint real
            %    track_fingerprint_real_distance (:obj:`float`): Track fingerprint
            %                                                   real distance
            %    track_age (:obj:`int`):                        Track age
            %    track_parent (:obj:`ont`):                     Track parent
            %    track_parent_frame (:obj:`int`):               Track parent frame
            %    track_parent_prob (:obj:`float`):              Track parent assignemt
            %                                                   probability
            %    track_parent_score (:obj:`float`):             Track parent assignemt
            %                                                   score
            %    track_generation (:obj:`int`):                 Track generation
            %    track_lineage_tree (:obj:`int`):               Track lineage
            %    track_assigmnent_fraction (:obj:`float`):      Assignment fraction
            %    track_total_assignment_cost (:obj:`float`):    Track total
            %                                                   assignment costs
            %    track_position_assignment_cost (:obj:`float`): Track positional
            %                                                   costs
            %    track_area_assignment_cost (:obj:`float`):     Track area costs
            %    track_rotation_assignment_cost (:obj:`float`): Track orientation
            %                                                   costs
            %    track_frame_skipping_cost (:obj:`float`):      Track frame
            %                                                   skipping costs
            %    track_start_frame (:obj:`int`):                Track start frame
            %    track_end_frame (:obj:`int`):                  Track end frame
            %    track_index_corrected (:obj:`int`):            Track index corrected
            %    track_contained_in_track (:obj:`int`):         Track contained
            %                                                   in other track
            %    track_has_bud (:obj:`int`):                    Track has bud
            %                                                   indicates if a
            %                                                   track is
            %                                                   budding in a
            %                                                   certain cell_frame
            %    track_budneck_total (:obj:`float`):            Total budneck signal
            %
            % Returns
            % -------
            %    this void (:obj:`object`)
            %                                                   :class:`SegmentationData` instance.
            %                                                   updated with new data
            
            % Primary key
            this.uuid = uuid;
            % Segmentation data
            this.cell_frame = cell_frame;
            this.cell_index = cell_index;
            this.cell_center_x = cell_center_x;
            this.cell_center_y = cell_center_y;
            this.cell_majoraxis = cell_majoraxis;
            this.cell_minoraxis = cell_minoraxis;
            this.cell_orientation = cell_orientation;
            this.cell_area = cell_area;
            this.cell_volume = cell_volume;
            this.cell_perimeter = cell_perimeter;
            this.cell_eccentricity = cell_eccentricity;
            this.cell_fractionOfGoodMembranePixels = cell_fractionOfGoodMembranePixels;
            this.cell_mem_area = cell_mem_area;
            this.cell_mem_volume = cell_mem_volume;
            this.cell_nuc_radius = cell_nuc_radius;
            this.cell_nuc_area = cell_nuc_area;
            this.cell_close_neighbour = struct('Indicies', cell_close_neighbour);
            this.cell_dif_x = cell_dif_x;
            this.cell_dif_y = cell_dif_y;
            this.cell_filtered_dif_x = cell_filtered_dif_x;
            this.cell_filtered_dif_y = cell_filtered_dif_y;
            this.cell_pole1_x = cell_pole1_x;
            this.cell_pole1_y = cell_pole1_y;
            this.cell_pole2_x = cell_pole2_x;
            this.cell_pole2_y = cell_pole2_y;
            this.cell_pole1_age = cell_pole1_age;
            this.cell_pole2_age = cell_pole2_age;
            
            % Tracking
            this.track_index = track_index;
            this.track_index_qc = track_index_qc;
            this.track_fingerprint_real = track_fingerprint_real;
            this.track_fingerprint_real_distance = track_fingerprint_real_distance;
            this.track_age = track_age;
            this.track_parent = track_parent;
            this.track_parent_frame = track_parent_frame;
            this.track_parent_prob = track_parent_prob;
            this.track_parent_score = track_parent_score;
            this.track_generation = track_generation;
            this.track_lineage_tree = track_lineage_tree;
            this.track_cell_cycle_phase = track_cell_cycle_phase;
            this.track_assignment_fraction = track_assignment_fraction;
            this.track_total_assignment_cost = track_total_assignment_cost;
            this.track_position_assignment_cost = track_position_assignment_cost;
            this.track_area_assignment_cost = track_area_assignment_cost;
            this.track_rotation_assignment_cost = track_rotation_assignment_cost;
            this.track_frame_skipping_cost = track_frame_skipping_cost;
            this.track_start_frame = track_start_frame;
            this.track_end_frame = track_end_frame;
            this.track_index_corrected = track_index_corrected;
            this.track_contained_in_track = track_contained_in_track;
            this.track_has_bud = track_has_bud;
            this.track_budneck_total = track_budneck_total;
            
        end
        
        function setUUID(this, value)
            % SETUUID Sets a unique id for each segmented cell per
            % image frame such that the +TracX.QuantificationData is mapped to the
            % +TracX.SegmentationData object via a primary key stored in the
            % uuid property.
            %
            % Args:
            %    this (:obj:`object`):   :class:`SegmentationData` instance.
            %    value (:obj:`int`):     Sets value for uuid.
            this.uuid = value;
        end
        
    end
end